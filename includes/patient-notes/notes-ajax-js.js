// MednCures Ajax Call for Patient Notes Crud Functions
jQuery(document).on('click','#submit-note',function(e) {
	e.preventDefault();
	jQuery('#feedback').remove();
	jQuery('#patient-message').show();
	//Get necessary details for posting Notes Data Properly
			var note_data = jQuery('#notescomments').val();
			var old_note = jQuery('#old_comments').val();
			var old_note_id = jQuery('#old_comment_id').val();
			var created_by = jQuery('#created_by').val();
			var creater_first_name = jQuery('#creater_first_name').val();
			var creater_last_name = jQuery('#creater_last_name').val();
			var note_type = jQuery('#note_type').val();
			var site_url = jQuery('#site_url').val();
			var patientid = jQuery(this).data('patient');
			var data_type = jQuery(this).data('type');
			var data = {
				'action': 'notes_crud_ajax_call',
				'patientid': patientid,
				'createdby': created_by,
				'cfirstname': creater_first_name,
				'clastname': creater_last_name,
				'notetype': note_type,
				'siteurl': site_url,
				'datatype': data_type,
				'notedata': note_data,
				'oldnotedata':  old_note,
				'oldnotedataid':  old_note_id
			};
			jQuery.post(wc_add_to_cart_params.ajax_url, data, function(response) {
				$('#list-patient-notes').html('<img style="display:inherit;max-width:100px;width: 100px;margin: 5px auto;" src="'+cdrmedajax.img_url+'/loading-secure-animation.gif" alt="Loading" />');
				jQuery('#submit-note').attr('data-type','new_note');
				jQuery('#notescomments').attr("value", "");
				jQuery("#patient-message").append('<div id="feedback">'+response+'</div>');
				jQuery(".previous-listed-notes").fadeOut();
				setTimeout( function() {
					jQuery("#patient-message").hide();
				}, 2000);
				// Second Ajax Call for Refreshing Notes Data Once Again
				var data = {
				'action': 'print_notes_on_page',
				'patientid': patientid,
				'note_type': note_type
				};
				
				jQuery.post(wc_add_to_cart_params.ajax_url, data, function(response) {
				jQuery("#list-patient-notes").html('<div class="previous-listed-notes">'+response+'</div>');
				});
				
			});
		
});

// Submit Functionality Function of Edit Note
jQuery('.sub2').live('click', function (event) {
	var id = event.target.id;
	jQuery('#'+id).hide();
	var old_comment_id = id.replace('update', "");
	jQuery('#old_comment_id').val(old_comment_id);
	id =  id.replace('update', "#commentsnote");
	jQuery('#notescomments').val(jQuery(id).html());
	jQuery('#notes-form').submit();
	jQuery('#submit-note').trigger('click');
});

// Live Edit Functionality for Notes
jQuery('.edit').live('click', function (event) {
	var id = event.target.id;
	id =  id.replace('edit', "");
	jQuery('#submit-note').attr('data-type','edit_note');
	jQuery('#old_comments').val(jQuery('#commentsnote'+id).text());

	var attr = jQuery('.commentsnote').attr('contenteditable');
	var value = jQuery('#commentsnote'+id).attr('contenteditable');

	if (value == 'false' || typeof value === "undefined") {
		 jQuery('#commentsnote'+id).css("background-color", "#FFFFCC");
		jQuery('#commentsnote'+id).attr('contenteditable','true');
		jQuery("#previousnotes"+id).css({ 'margin-bottom': '65px' });
		jQuery('#update'+id).show();
	}
	else {
		jQuery('#commentsnote'+id).css("background-color", "#ffffff");
		jQuery('#commentsnote'+id).attr('contenteditable','false');
		 jQuery("#previousnotes"+id).css({ 'margin-bottom': '15px' });
		 jQuery('#submit-note').attr('data-type','new_note');
		jQuery('#update'+id).hide();		
	}       
});


// Delete Call Functionality of Note
jQuery('.delete-patient-note').live('click', function (event) {
	var answer = confirm ("Are you sure you want to delete the note?");
	if (answer) {
	jQuery('#submit-note').attr('data-type','delete_note');
	var id = event.target.id;
	jQuery('#old_comment_id').val(id);
	jQuery('#notes-form').submit();
	jQuery('#submit-note').trigger('click');
	}
});

// Model Box for Notes History
jQuery(".mytooltop").live("click",function(){
		//jQuery("#notesalla"+this.id).show();
		jQuery('#user_notes').html('');
		jQuery('#user_notes').html(jQuery("#notesalla"+this.id).html());
		jQuery('#notesalla').modal('show');
	});
	
	jQuery('#notesalla').on('hidden.bs.modal', function () {
		//jQuery(".notesalla").hide();
	});


// Function for Printing Any HTML Area Enclosed in a Div.
jQuery(document).on('click','#printDiv',function() {
	var getPrintEl = jQuery(this).data('print');
	var printContents = document.getElementById(getPrintEl).innerHTML;
	 var originalContents = document.body.innerHTML;
	 document.body.innerHTML = printContents;
	 window.print();
	 document.body.innerHTML = originalContents;
});