<?php

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

add_action('wp_ajax_notes_crud_ajax_call','notes_crud_ajax_call');
add_action('wp_ajax_nopriv_notes_crud_ajax_call','notes_crud_ajax_call');

function notes_crud_ajax_call() {
	global $wpdb, $wp;
	// Get Parameters from Ajax Call Here
	$pid = $_POST['patientid'];
	$created_by = $_POST['createdby'];
	$creater_first_name = $_POST['cfirstname'];
	$creater_last_name = $_POST['clastname'];
	// Note Type Represents Intake, Pre-Registration Or General
	$note_type = $_POST['notetype'];
	$site_url = $_POST['siteurl'];
	// Data Type Represents new_note,edit_note,delete_note
	$data_type = $_POST['datatype'];
	
	/*$old_note = nl2br(htmlentities($_POST['oldnotedata'], ENT_QUOTES, 'UTF-8'));
	if($old_note){
	$note = $_POST['notedata'];
	} else {
	$note = nl2br(htmlentities($_POST['notedata'], ENT_QUOTES, 'UTF-8'));
	}*/
	$old_note = $_POST['oldnotedata'];
	$note = $_POST['notedata'];
	
	$old_note_id = $_POST['oldnotedataid'];
	
	$feedback_message = '';
	
	if($data_type == 'edit_note' && $old_note && $old_note_id && $note){
		// Update Notes and Maintain History
		$myentry = $wpdb->query("update {$wpdb->prefix}cdrmed_notes set value='".$note."' where id=$old_note_id");
			$wpdb->query( $wpdb->prepare(
				"INSERT INTO {$wpdb->prefix}notes_history (patient_id, `date`,note_id, comments,note_type) VALUES ( %s, %s, %s, %s, %s )",
				array(
					$pid,
					date("Y-m-d H:i:s"),
					$old_note_id,
					$old_note,
					$note_type,
				)
			));
		$feedback_message = 'Note updated successfully!';
		
		cdrmed_save_activity_log('Patient Notes Updated!', $pid);
		
	}
	
	if($data_type == 'new_note' && $note){
		//nl2r convert \n into <br>
		$note = nl2br($note);
		// Save Note to Database
		$myentry = $wpdb->query( $wpdb->prepare(
				"INSERT INTO {$wpdb->prefix}cdrmed_notes (patient_id, user_name, user_id, value, note_type,date_created) VALUES ( %s, %s, %s, %s, %s, %s )",
					array(
						$pid,
						$creater_first_name,
						$created_by,
						$note,
						$note_type,
						date("Y-m-d H:i:s")
						//date("2016-05-02 12:51:33")
					)
				));
		$feedback_message = 'Note saved successfully!';
		
		cdrmed_save_activity_log('Patient Notes Added!', $pid);
	}
	
	if($data_type == 'delete_note' && $old_note_id){			
		// Delete Note from Database
		$deletecomment = $wpdb->query($wpdb->prepare("DELETE FROM {$wpdb->prefix}cdrmed_notes WHERE id = %d",$old_note_id));
		$feedback_message = 'Note deleted successfully!';
	}
	echo $feedback_message;
	die();

}

?>