<?php

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly 

add_shortcode( 'patient-notes', 'patient_notes' );
function patient_notes() {

	$pid = isset($_GET['pid']) ? $_GET['pid'] : (isset($_GET['poid']) ? $_GET['poid'] : '');
	$url = site_url();
   	$user_ID = get_current_user_id();
	$user_info = get_userdata($user_ID);
	$first_name = $user_info->first_name; 
	$last_name = $user_info->last_name; 
	
	if(is_page(array('view-complete-intake','new-notes-test'))){
		$note_type = 'intake';
	} elseif(is_page('pre-registration-data')){
		$note_type = 'pre-reg';
	} else {
		$note_type =  'general';
	}
	
	// Note Form Starts Here
	$html1 = '';
	$html1 .= '<div id="patient-message1"></div>';
	$html1 .= '<div class="sort-text">Patient and Consultation Notes</div>';
	$html1 .= '<form action="" name="notes-form" id="notes-form" method="post">
	<textarea name="comment" id="notescomments" class="notes-area-text" rows="4" cols="150" placeholder="Enter your note here..."></textarea>
  	<input type="hidden" name="old_comments" id="old_comments" value="" />
	<input type="hidden" name="old_comment_id" id="old_comment_id" value="" />
	<input type="hidden" name="created_by" id="created_by" value="'.$user_ID.'" />
	<input type="hidden" name="creater_first_name" id="creater_first_name" value="'.$first_name.'" />
	<input type="hidden" name="creater_last_name" id="creater_last_name" value="'.$last_name.'" />
	<input type="hidden" name="note_type" id="note_type" value="'.$note_type.'" />
	<input type="hidden" name="site_url" id="site_url" value="'.$url.'" />
  	<input type="submit" data-type="new_note" data-patient="'.$pid.'" value="Save Note" id="submit-note" class="sub-but submit-note" name="submit">
	</form>';
	
	
	//$html1 .= print_notes_on_page($pid,$note_type);
	
	$html1 .= '<div id="list-patient-notes"><div class="previous-listed-notes">';
	$html1 .= do_shortcode('[list-patient-notes patient_id="'.$pid.'" note_type="'.$note_type.'"]');
	$html1 .= '</div></div>';
	
	return $html1;
	// Notes Printing Ends Here
	
}


add_action('wp_ajax_print_notes_on_page','print_notes_on_page');
add_action('wp_ajax_nopriv_print_notes_on_page','print_notes_on_page');
add_shortcode( 'list-patient-notes', 'print_notes_on_page' );
function print_notes_on_page($atts) {
	global $wpdb, $wp;
	
	$atts = shortcode_atts(
		array(
			'patient_id' => '',
			'note_type' => '',
		),
		$atts
	);
	$html1 = '';
	
	
	$pid = ($atts['patient_id'] ? $atts['patient_id'] : $_POST['patientid']);
	$note_type = ($atts['note_type'] ? $atts['note_type'] : $_POST['note_type']);
	
	// Start Printing of Notes Here
	$pnotes = "SELECT * FROM {$wpdb->prefix}cdrmed_notes where patient_id = $pid and note_type = '$note_type' ORDER BY id DESC";
	$getnotes = $wpdb->get_results($pnotes);

	if($getnotes) {
	$html1 .= '<div class="notesall">';
  		foreach($getnotes as $key => $value) {
			$id = $value->id;
			$username = $value->user_name;
			$datecreated = $value->date_created;
			//$comments = stripslashes($value->value);
			$comments = $value->value;
			$newDate = date("m-d-Y", strtotime($datecreated));
			
	// History Notes Here
	$historynotes2 = "SELECT * FROM {$wpdb->prefix}notes_history
				where patient_id = $pid AND note_id=$id ORDER BY id DESC";
				
	$historynotes = $wpdb->get_results($historynotes2);
	if($historynotes){
		$old_id = $id;

		$html1 .= '<div class="notesalla notesalla'.$id.'" id="notesalla'.$id.'" style="display:none;">';
		foreach($historynotes as $key => $mvalue) {
			
			$datecreated = $mvalue->date;
			$mcomments = $mvalue->comments;
			$newDate = date("m-d-Y", strtotime($datecreated));
		
			$html1 .= '<div class="previousnotes"><div class="createdby">Created By '.$username.'<span class="createddate">'.$datecreated.'</span></div>';	
			$html1 .= '<div class="commentsnote" id="commentsnote">'.$mcomments.'</div></div>';	
		}
		$html1 .= '</div>';
	} else {
			$html1 .= '<p class="notesalla notesalla'.$id.'" style="display:none;" id="notesalla'.$id.'">There are no History notes available at the moment</p>';
			//$i++;
	}
	// History Notes End Here
	
	$html1 .= '<div class="previousnotes" data-prevnote="'.$id.'" id="previousnotes'.$id.'">
				<div class="createdby">Created By '.$username.'
				
				<a data-target="#doc" id ="'.$id.'" class="load-cdrmed-modal-box mytooltop"  modal-title="Notes History" modal-action="view-note-history" modal-footer="false" href="javascript:void(0);">
				<span class="fa fa-question round-info" data-toggle="tooltip" title="click to see History"></span></a>
				<span class="notesddate" style="float:right;">'.$newDate.' 
				<a href="javascript:void(0)" id="'.$id.'" class="delete-patient-note">Delete</a> | <a href="javascript:void(0)" class="edit" id="edit'.$id.'">Edit</a>
				</span>
				</div>';	
	$html1 .= '<div class="commentsnote" id="commentsnote'.$id.'">'.$comments.'</div>
				<input type="button" id="update'.$id.'" style="display:none;" value="Update Note" class="sub-but sub2" name="submit'.$id.'">
				';	
	$html1 .= '</div>';
	
		}
		} else {
	
	$html1 = '<div class="feedback">No Notes Found</div>';	
		
	}
	
	if(isset($_POST['patientid'])) {
	echo $html1;
	die();	
	} else {
	return $html1;
	}
}
?>