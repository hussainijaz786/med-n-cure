<?php 
if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly 
// Include Functions for Physician Dashboard
include('child-physician-dispensary-list.php');
include('edit-patient-intake.php');
include('user-profile-detail-view.php');
include('add-new-patient-physician-side.php');
include('create-new-patient-ajax-call.php');
include('view-all-patients-ajax-call.php');
include('view-all-patients.php');
include('view-pending-registrations.php');
include('view-short-intake-detail.php');
include('view-complete-intake-detail.php');
include('view-complete-prereg-detail.php');
include('azimport-csv.php');
include('action-on-child-users-ajax-call.php');
?>