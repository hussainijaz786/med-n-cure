<?php

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly 


add_action( 'wp_enqueue_scripts', 'azimport_csv' );
function azimport_csv() {
	
   $wp_upload_dir = wp_upload_dir();

    
$target_dir = $wp_upload_dir['path']."/";
$target_file = $target_dir . basename(@$_FILES["fileToUpload"]["name"]);
 
// Check if image file is a actual image or fake image
if(isset($_POST["submit-csv"])) {
 if ( ! function_exists( 'wp_handle_upload' ) ) require_once( ABSPATH . 'wp-admin/includes/file.php' );
$uploadedfile = $_FILES['fileToUpload'];
$upload_overrides = array( 'test_form' => false );
$movefile = wp_handle_upload( $uploadedfile, $upload_overrides );
if ( $movefile ) {
    
    //var_dump( $movefile);

     	$csv = array();
$lines = file($target_file, FILE_IGNORE_NEW_LINES);
$my_post = array(
                'post_type'  => 'shoops',
                'post_title' => 'Shoops Dosage CSV Import',
                'post_content' => 'Shoops Dosage CSV Import',
                'post_status' => 'private',
                'comment_status' => 'closed',   // if you prefer
                'ping_status' => 'closed',
                'post_author'  => get_current_user_id()
             
);

// Insert the post into the database
$postid = wp_insert_post( $my_post );
$i = 0;
$check = 0;
foreach ($lines as $key => $value)
{
	
	
	$csv[$key] = str_getcsv($value);
	

	if ($postid) {
   
     	//$postid = $_POST['post_id'];
      if($i == 0){
         if(strtolower($csv[$key][1]) == 'values'){
        $check = 0;
     }else{
        $check = 1;
     }
   }
     if($check == 0  && $i > 0){
       switch ($i) {
         case '1':
          update_post_meta($postid, 'medicine1_cannabinoid_1',$csv[$key][1]);
           break;
        case '2':
         update_post_meta($postid,'medicine1_target_dosage_1'  ,$csv[$key][1]);
           break;
        case '3':
         update_post_meta($postid, 'medicine1_frequency_1',$csv[$key][1]);
          break; 
      case '4':
          update_post_meta($postid, 'medicine1_cannabinoid_2',$csv[$key][1]);
           break;
      case '5':
          update_post_meta($postid, 'medicine1_target_dosage_2',$csv[$key][1]);
           break;
      case '6':
          update_post_meta($postid, 'medicine1_frequency_2',$csv[$key][1]);
           break;
       case '7':
          update_post_meta($postid, 'medicine1_terpene', $csv[$key][1]);
           break;
       case '8':
          update_post_meta($postid, 'medicine1_ingestion_method',$csv[$key][1]);
           break;
       case '9':
          update_post_meta($postid, 'notes',$csv[$key][1]);
           break;
         default:
           update_post_meta($postid, 'patient_id', $_GET['pid']);
           break;
       }
       
        update_post_meta($postid, 'patient_id', $_GET['pid']);
  
      }else{
        update_post_meta($postid, '_azcl_'.strtolower($csv[$key][0]),$csv[$key][1]);
        update_post_meta($postid, '_azcl_'.strtolower($csv[$key][0])."_test",$csv[$key][2]);
        update_post_meta($postid, '_azcl_'.strtolower($csv[$key][0])."_ppm",$csv[$key][3]);
        update_post_meta($postid, 'patient_id', $_GET['pid']);
   
      }
       
     }
     $i++;
 }
  $urlofpage = $url.'/shoop-for-patient/';
  echo "<h2><center>Shoops imported Successfully<br/>You will be redirected shortly</center></h2>";
         header('refresh:5;url='.$urlofpage.'?'.$_SERVER['QUERY_STRING']);
        exit;
}else{

  echo "Some error in CSV file.Couldn't upload.";
}
}
   $html = '<div class="row"><div class="col-md-12">
	<form role = "form" name="doc-form" method="post" enctype="multipart/form-data">
 
    

  <div class="bdr-csv"> 
    <label for = "inputfile">Import CSV File</label>
    <br/>
  <center>
            Browse <input type="file" id="fileToUpload" name="fileToUpload">
    
            <input type="submit" name="submit-csv" id="gform_submit_button_102" class="gform_button3 button3" value="Import CSV" >
</center>
 

   </div>
   
</form></div>';
	
	
  
  return $html;
}
add_shortcode( 'azimport-csv', 'azimport_csv' );
?>