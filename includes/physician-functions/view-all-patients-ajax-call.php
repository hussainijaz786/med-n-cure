<?php

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly 

add_action( 'wp_ajax_view_all_patient_ajax_request', 'view_all_patient_ajax_request');
add_action( 'wp_ajax_nopriv_view_all_patient_ajax_request', 'view_all_patient_ajax_request');

function view_all_patient_ajax_request() {
	global $wpdb, $add_my_script;
	$add_my_script= true;
	$siteurl = site_url();
	$preg = site_url( '/pre-registration-data/', 'http' );
	$cin = site_url( '/view-complete-intake/', 'http' );
	$gurl = site_url( '/general-notes/', 'http' );
	$shoopurl = site_url( '/shoop-for-patient/', 'http' );
	$createshoop = site_url( '/shoop-for-patient/create-new-shoop/', 'http' );
	$docurl = site_url( '/patient-documents/', 'http' );
	$current_usid = get_current_user_id();
	$userrole = get_user_role();
	
	$meta_key = 'patient_physician_id';
	$hide_personal_info = true;
	if(	$userrole == 'dispensary'){
		$meta_key = 'patient_dispensary_id';
		$hide_personal_info = null;
	}
	
	$meta_value = $current_usid;
	
	$child_user = get_user_meta($meta_value, 'user_parent', true);
	if($child_user != ''){
		$meta_value = get_user_meta($meta_value, 'user_parent_id', true);
	}
	if( $userrole == 'administrator'){
		$meta_value = 'orphan';
	}
	
	$offset = isset($_GET['offset']) ? $_GET['offset'] : 0;
	$limit = isset($_GET['limit']) ? $_GET['limit'] : 10;
	
	
	$search = '';
	
	$patient_ids = array();
	
	if(isset($_GET['search']) && $_GET['search'] != ''){
		//get search keyword
		$search = $_GET['search'];
		
		//pass arguments with search keywords
		//get total patients
		$users = new WP_User_Query(array(
			'role' => "patient",
			'orderby' => 'ID',
			'order' => 'DESC',
			'meta_query' => array(
				'relation' => 'AND',
				array(
					'key' => 'user_full_name',
					'value' => $search,
					'compare' => 'LIKE'
				),
				array(
					'key' => $meta_key,
					'value' => $meta_value,
					'compare' => 'LIKE'
				),
			)
		));
		
		$patient_id_count = array();
		$total_patients1 = $users->get_results();		
		foreach ($total_patients1 as $key => $value) {      // Loop all order Items
			$patient_id = $value->ID;
			if(!in_array($patient_id, $patient_id_count)){
				$patient_id_count[] = $value->ID;
			}
		}
		
		$users = new WP_User_Query(array(
			'role' => "patient",
			'orderby' => 'ID',
			'order' => 'DESC',
			'search'         => "*{$search}*",
			'meta_query' => array(
				'relation' => 'AND',
				array(
					'key' => $meta_key,
					'value' => $meta_value,
					'compare' => 'LIKE'
				),
			)
		));
		$total_patients2 = $users->get_results();
		foreach ($total_patients2 as $key => $value) {      // Loop all order Items
			$patient_id = $value->ID;
			if(!in_array($patient_id, $patient_id_count)){
				$patient_id_count[] = $patient_id;
			}
		}
		
		$product_json['total'] = sizeof($patient_id_count);
		
		//echo "<pre>";print_r($patient_id_count);echo "</pre>";
		
		$loop_limit = $offset + $limit;
		for($i = $offset; $i < $loop_limit; $i++){
			if($i == $product_json['total'])
				break;
			$patient_id = $patient_id_count[$i];
			$patient_ids[] = $patient_id;
		}
		
	}
	else{
		
		//get total patients
		$users = new WP_User_Query(array(
			'role' => "patient",
			'orderby' => 'ID',
			'order' => 'DESC',
			'meta_query' => array(
				'relation' => 'AND',
				array(
					'key' => $meta_key,
					'value' => $meta_value,
					'compare' => 'LIKE'
				),
			)
		));
		
		$patient_id_count = array();
		$total_patients = $users->get_results();		
		foreach ($total_patients as $key => $value) {     // Loop all order Items
			if(!in_array($value->ID, $patient_id_count))
				$patient_id_count[] = $value->ID;
		}
		
		$product_json['total'] = sizeof($patient_id_count);
		
		$loop_limit = $offset + $limit;
		for($i = $offset; $i < $loop_limit; $i++){
			if($i == $product_json['total'])
				break;
			$patient_id = $patient_id_count[$i];
			$patient_parents = json_decode(get_user_meta($patient_id, $meta_key, true), true);
			if(in_array($meta_value, $patient_parents)){
				$patient_ids[] = $patient_id;
			}
		}
		
		//show patients with offset and limit
		/* $users = new WP_User_Query(array(
			'role' => "patient",
			'number' => $limit,
			'offset' => $offset,
			'orderby' => 'ID',
			'order' => 'DESC',
			'meta_query' => array(
				'relation' => 'AND',
				array(
					'key' => $meta_key,
					'value' => $meta_value,
					'compare' => 'LIKE'
				),
			)
		));
		
		
		
		$getpatients = $users->get_results();			
		foreach ($getpatients as $key => $value) {      // Loop all order Items
			if(!in_array($value->ID, $patient_ids))
				$patient_ids[] = $value->ID;
		} */
		
	}
	
	foreach ($patient_ids as $key => $value) {      // Loop all order Items
		//$patient_id = $value->user_id;
		$patient_id = $value;
		$user_info = get_userdata($patient_id);
		$first_name = $user_info->first_name; 
		$last_name = $user_info->last_name; 
		$email = $user_info->user_email; 
		
		$user_doc_count = count_user_posts( $patient_id , 'document' );
		
		$user_latest_doc = gh_get_latest_doc($patient_id);
		if($user_latest_doc != '') {
			$user_latest_doc_date = get_the_date( "m-d-Y", $user_latest_doc );
			$user_latest_doc_date_print = get_the_date( "m-d-Y", $user_latest_doc );
		}
		else{
			$user_latest_doc_date ='01-17-2000';
			$user_latest_doc_date_print ='';
		}
		if($user_doc_count > 0) {
			$doc = '<a href="'.$docurl.'?pid='.$patient_id.'">'.$user_doc_count.' Docs | Add Docs</a> <span style="width:100%; float: left; font-size: 10px;">Last Document: '.$user_latest_doc_date_print.'</span>';
		}
		else{
			$doc = 'No Documents  | <a href="'.$docurl.'?pid='.$patient_id.'"> Add Docs</a>';
		}
		
		
		$user_latest_shoop = gh_get_latest_shoop($patient_id);
		if(!empty($user_latest_shoop)) {
			$user_latest_shoop_date = get_the_date( "m-d-Y", $user_latest_shoop );
			$user_latest_shoop_date_print = "<span style='width:100%; float: left; font-size: 10px;'>Last Shoop: {$user_latest_shoop_date}</span>";
		}
		else {
			$user_latest_shoop_date ='01-17-2000';
			$user_latest_shoop_date_print = '';
		}
		
		if($hide_personal_info){
			$product_json['rows'][] = array(
				  'usid' => $patient_id,
				  'id' => '<div data-short="'.strtolower($first_name).' '.strtolower($last_name).'"><a href="'.$cin.'?pid='.$patient_id.'" data-check=""><i class="fa fa-file-text-o"></i>  '.ucfirst(strtolower($first_name)).' '.ucfirst(strtolower($last_name)).'</a></div>',
				  'email' => '<div data-short="'.strtolower($email).'"><span style="text-transform:lowercase;cursor: pointer;" class="load-cdrmed-modal-box send_email_to_patient_modal" data-call_action="send_email_to_patient_modal" modal-title="Send Email" modal-action="send-email-to-patient" modal-footer="true" data-email="'.$email.'">'.$email.'</span></div>',
				  'doc' => '<div data-short="'.$user_latest_doc_date.'">'.$doc.'</div>',
				  'date' => '<div data-short="'.$user_latest_shoop_date.'"><a href="'.$shoopurl.'?&pid='.$patient_id.'">Launch SHOOP</a>'.$user_latest_shoop_date_print.'</div>',
				  'notes' => '<a href="'.$gurl.'?pid='.$patient_id.'">Patient Notes</a>',
				  'action' => '<a title="View Health Journey" href="'.$siteurl.'/patient-health-journey/?pid='.$patient_id.'" class="patient-action-td-child"><i class="fa fa-info-circle"></i></a><span class="load-cdrmed-modal-box edit-child-profile-modal patient-action-td-child" data-call_for="patient" data-call_action="edit_child_profile_modal" modal-title="Edit '.$first_name.' Profile" modal-action="edit-child-profile" modal-footer="true" data-user_id="'.$patient_id.'" data-first_name="'.$first_name.'" data-last_name="'.$last_name.'" data-email="'.$email.'" data-action="by_patient" style="cursor:pointer;"><i class="fa fa-edit"></i></span><span class="user-remove-custom-btn patient-action-td-child" data-call-action="remove-user" data-action-permission="allow" data-user_id="'.$patient_id.'" style="cursor:pointer;"><i class="fa fa-remove"></i></span>'
				); 
		}
		else{
			$product_json['rows'][] = array(
				  'usid' => $patient_id,
				  'id' => '<div data-short="'.strtolower($first_name).' '.strtolower($last_name).'"><a href="'.$gurl.'?pid='.$patient_id.'" data-check=""><i class="fa fa-file-text-o"></i>  '.ucfirst(strtolower($first_name)).' '.ucfirst(strtolower($last_name)).'</a></div>',
				  'email' => '<div data-short="'.strtolower($email).'"><span style="text-transform:lowercase;cursor: pointer;" class="load-cdrmed-modal-box send_email_to_patient_modal" data-call_action="send_email_to_patient_modal" modal-title="Send Email" modal-action="send-email-to-patient" modal-footer="true" data-email="'.$email.'">'.$email.'</span></div>',
				  'date' => '<div data-short="'.$user_latest_shoop_date.'"><a href="'.$shoopurl.'?&pid='.$patient_id.'">Launch SHOOP</a>'.$user_latest_shoop_date_print.'</div>',
				  //'notes' => '<a href="'.$gurl.'?pid='.$patient_id.'">Patient Notes</a>',
				  'action' => '<a title="View Health Journey" href="'.$siteurl.'/patient-health-journey/?pid='.$patient_id.'" class="patient-action-td-child"><i class="fa fa-info-circle"></i></a><span class="load-cdrmed-modal-box  edit-child-profile-modal patient-action-td-child" data-call_for="patient" data-call_action="edit_child_profile_modal" modal-title="Edit '.$first_name.' Profile" modal-action="edit-child-profile" modal-footer="true" data-user_id="'.$patient_id.'" data-first_name="'.$first_name.'" data-last_name="'.$last_name.'" data-email="'.$email.'" data-action="by_patient" style="cursor:pointer;"><i class="fa fa-edit"></i></span><span class="user-remove-custom-btn patient-action-td-child" data-call-action="remove-user" data-action-permission="allow" data-user_id="'.$patient_id.'" style="cursor:pointer;"><i class="fa fa-remove"></i></span>'
				); 
		}
			 
				
				
	}
	
	if(sizeof($patient_ids) < 1){
		$product_json['rows'][] = array();
	}
	
	$sort = isset($_GET['sort']) ? $_GET['sort']: 'usid';
	$order = ($_GET['order']=='asc') ? SORT_ASC : SORT_DESC;
	if(!isset($_GET['sort'])){
			$order = SORT_DESC;
	}
	$product_json['rows'] = array_sort($product_json['rows'], $sort, $order);
	$product_json = json_encode($product_json);
	echo $product_json;
	die;
}

function array_sort($array, $on, $order=SORT_ASC){

    $new_array = array();
    $sortable_array = array();

    if (count($array) > 0) {
        foreach ($array as $k => $v) {
            if (is_array($v)) {
                foreach ($v as $k2 => $v2) {
                    if ($k2 == $on) {
                        $sortable_array[$k] = $v2;
                    }
                }
            } else {
                $sortable_array[$k] = $v;
            }
        }

        switch ($order) {
            case SORT_ASC:
                asort($sortable_array);
                break;
            case SORT_DESC:
                arsort($sortable_array);
                break;
        }

        foreach ($sortable_array as $k => $v) {
            $new_array[] = $array[$k];
        }
    }

    return $new_array;
}


?>