<?php

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly 

// Add Shortcode
function patient_doc_list() {
	global $wpdb, $add_my_script;
	$add_my_script= true;
	$patient_id = $_GET['pid'];
	
	$patient_fname = get_the_author_meta( 'first_name', $patient_id );
	$patient_lname = get_the_author_meta( 'last_name', $patient_id );
	
	// WP_Query arguments
	$args = array (
		'post_type'              => array( 'document' ),
		'post_status'            => array( 'private' ),
		'author'                 => $patient_id,
		//'pagination'             => true,
		//'posts_per_page'         => '10',
	);
	
	// The Query
	$query = new WP_Query( $args );
	//echo "<pre>"; print_r($query); echo "</pre>";
			
			$html .= '<h2>'.$patient_fname.' '.$patient_lname.'`s Documents</h2>';
			$html .= '<table data-toggle="table" data-show-toggle="false" data-classes="table table-hover stripped" data-striped="true" data-show-columns="true" data-id-field="id" data-pagination="true" data-search="true" data-page-size="10" data-show-export="true" data-page-list="[25, 50, 100, 250, 500, 1000, 5000]" data-smart-display="true" data-toolbar="#filter-bar" data-show-filter="true" data-mobile-responsive="true">';
			$html .= '<thead><tr>';
			$html .= '<th>Document Number</th>';
			$html .= '<th>Document Name</th>';
			$html .= '<th>Date Added</th>';
			$html .= '</thead></tr><tbody>';
	
	// The Loop
	if ( $query->have_posts() ) {
		while ( $query->have_posts() ) {
			$query->the_post();
			// do something
			$postid = get_the_ID();
			$date = get_the_date();
			$title = get_the_title();
			$link = get_the_permalink();
			
			$html .= '<tr>';
			$html .= '<td>'.$postid.'</td>';
			$html .= '<td><a href="'.$link.'">'.$title.'</a></td>';
			$html .= '<td>'.$date.'</td>';
			$html .= '</tr>';

		}
	} else {
		// no posts found
	}
	
	// Restore original Post Data
	wp_reset_postdata();
		$html .= '</tbody></table>';
		return $html;
	
}
add_shortcode( 'patient-docs', 'patient_doc_list' );


?>