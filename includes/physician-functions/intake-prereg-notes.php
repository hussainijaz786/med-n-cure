<?php

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly 

function userpost_notes() {
	
	global $wpdb, $wp;

	$current_url = home_url( add_query_arg( NULL, NULL ) );
	$pid = $_GET['pid'];
	$url = site_url();
	$html1 = '';
   	$user_ID = get_current_user_id();
	$user_info = get_userdata($user_ID);
	$cid = $_POST['cid'];
	$first_name = $user_info->first_name; 
	$last_name = $user_info->last_name; 
	$email = $user_info->user_email;
	$notefromform = $_POST['comment'];
  	$note = nl2br(htmlentities($notefromform, ENT_QUOTES, 'UTF-8'));
	
	$note_type = "pre-reg";
	
	if(isset($_POST['edit'])){
		$myentry = $wpdb->query("update {$wpdb->prefix}cdrmed_notes set value='".$note."' where id=$cid");
		$wpdb->query( $wpdb->prepare(
      "INSERT INTO {$wpdb->prefix}notes_history (patient_id, `date`,note_id, comments,note_type) VALUES ( %s, %s, %s, %s, %s )",
      array(
          $pid,
          date("Y-m-d H:i:s"),
          $cid,
		  $_POST['old_comments'],
		  $note_type,
		  )
		)); 
		header('Location: '.$_SERVER['REQUEST_URI']);exit(); 
	}else{
	//if($form_id) {
	$html1 .= '<form action="" id="usrform" method="post">
	<textarea name="comment" id="notescomments" class="notes-area-text" form="usrform" rows="4" cols="150" placeholder="Enter your note here..."></textarea>
  	<input type="hidden" name="old_comments" id="old_comments" value="" />
  	<input type="submit" value="Save Note" id="submit" class="sub-but" name="submit">
	</form>';
	//}
	if(isset($_POST['submit']) && isset($_POST['comment'])) {
     $success = false;

    $myentry = $wpdb->query( $wpdb->prepare(
      "INSERT INTO {$wpdb->prefix}cdrmed_notes (patient_id, user_name, user_id, value, note_type,date_created) VALUES ( %s, %s, %s, %s, %s, %s)",
      array(
          $pid,
          $first_name,
          $user_ID,
		  $note,
		  $note_type,
		  date("Y-m-d H:i:s")
		  )
		));
		if($myentry) {
		$success = true;
			}
    
		}
		}
		
		
	//Delete Comment Starts Here
	if(isset($_GET['delete']) && $_GET['delete'] == '1') {
		if(isset($_GET['cid'])) {
		$comid = $_GET['cid'];
	$deletecomment = $wpdb->query($wpdb->prepare("DELETE FROM {$wpdb->prefix}cdrmed_notes WHERE id = %d",$comid));
		}
	}
  
  $pnotes = "SELECT * FROM {$wpdb->prefix}cdrmed_notes where patient_id = $pid and note_type='pre-reg' ORDER BY id DESC";
				
	$getnotes = $wpdb->get_results($pnotes);
  $html1 .= '<div class="notesall">';
  foreach($getnotes as $key => $value) {
		$id = $value->id;
		$username = $value->user_name;
		$datecreated = $value->date_created;
		$comments = stripslashes($value->value); 
		$newDate = date("m-d-Y", strtotime($datecreated));
	// History code start from here
	$pid = !isset($_GET['pid'])?null:$_GET['pid'];
		if($pid) {
			$user = $pid;
		} else {
			$pid = get_current_user_id();
		}
	$muser_info = get_userdata($pid);
	$cid = $_POST['cid'];
	$first_name = $muser_info->first_name; 
	$last_name = $muser_info->last_name; 
	
$historynotes2 = "SELECT * FROM {$wpdb->prefix}notes_history
				where patient_id = $pid AND note_id=$id ORDER BY id DESC";				
	$historynotes = $wpdb->get_results($historynotes2);
	if($historynotes){
		$old_id = $id;

		$html1 .= '<div class="notesalla" id="notesalla'.$id.'" style="display:none;">';
		foreach($historynotes as $key => $mvalue) {
			$datecreated = $mvalue->date;
			$mcomments = $mvalue->comments;
			$newDate = date("m-d-Y", strtotime($datecreated));
		
		$html1 .= '<div class="previousnotes"><div class="createdby">Created By '.$first_name.'<span class="createddate">'.$datecreated.'</span></div>';	
		$html1 .= '<div class="commentsnote" id="commentsnote">'.$mcomments.'</div></div>';	
		}
		$html1 .= '</div>';
	} else {
			$html1 .= '<p class="notesalla" style="display:none;" id="notesalla'.$id.'">There are no History notes available at the moment</p>';
			$i++;
	}
	
//Ends here
	$html1 .= '<div class="previousnotes" id="previousnotes'.$id.'"><div class="createdby">Created By '.$username.'<a data-target="#doc" id ="'.$id.'" class="mytooltop" href="javascript:void(0);"><span class="fa fa-question round-info" data-toggle="tooltip" title="click to see History"></span><a/><span class="notesddate">'.$newDate.' <a class="delete-comment confirmationcall" href="'.$current_url.'&delete=1&cid='.$id.'">Delete</a>
	 | <a href="javascript:void(0)" class="edit" id="edit'.$id.'">Edit</a></span></div>';	
	$html1 .= '<div class="commentsnote" id="commentsnote'.$id.'">'.$comments.'</div> <input type="button" id="update'.$id.'" style="display:none;" value="Update Note" class="sub-but sub2" name="submit'.$id.'"></div>';	
	}
	$html1 .= '<input type="hidden" name="cide" id="cid" value="" /></div>';
	return $html1;

	
}
add_shortcode( 'prenotes', 'userpost_notes' );

// Intake Notes

function intakepost_notes() {
	
	global $wpdb, $wp;

	$current_url = home_url( add_query_arg( NULL, NULL ) );
	$pid = isset($_GET['pid']) ? $_GET['pid'] : '';
	$url = site_url();
	$html1 = '';
   	$user_ID = get_current_user_id();
	$user_info = get_userdata($user_ID);
	$cid = isset($_POST['cid']) ? $_POST['cid'] : '';
	$oldnotes =  isset($_POST['old_comments']) ? $_POST['old_comments'] : '';
	$first_name = $user_info->first_name; 
	$last_name = $user_info->last_name; 
	$email = $user_info->user_email;
	$notefromform = isset($_POST['comment']) ? $_POST['comment'] : '';
	//echo $notefromform;die();
	
	$note_type = "intake";

	if(isset($_POST['edit'])){
		//print_r($_POST);exit();
		/*this<br />
is<br />
note
*/
	$note = $notefromform;

		$myentry = $wpdb->query("update {$wpdb->prefix}cdrmed_notes set value='".$note."' where id=$cid");
		$wpdb->query( $wpdb->prepare(
			"INSERT INTO {$wpdb->prefix}notes_history (patient_id, `date`,note_id, comments,note_type) VALUES ( %s, %s, %s, %s, %s )",
			array(
				$pid,
				date("Y-m-d H:i:s"),
				$cid,
				$oldnotes,
				$note_type,
			)
		));
		header('Location: '.$_SERVER['REQUEST_URI']);exit(); 	
	}else{
		
		$note = nl2br(htmlentities($notefromform, ENT_QUOTES, 'UTF-8'));

		$html1 .= '<form action="" id="usrform" method="post"><div class="sort-text">Patient and Consultation Notes</div>
		<textarea name="comment" id="notescomments" class="notes-area-text" form="usrform" rows="4" cols="150" placeholder="Enter your note here..."></textarea>
		<input type="hidden" name="old_comments" id="old_comments" value="" />
		<input type="submit" value="Save Note" id="submit" class="sub-but" name="submit">
		</form>';

		if(isset($_POST['submit']) && isset($_POST['comment'])) {
			$success = false;


			$myentry = $wpdb->query( $wpdb->prepare(
			"INSERT INTO {$wpdb->prefix}cdrmed_notes (patient_id, user_name, user_id, value, note_type,date_created) VALUES ( %s, %s, %s, %s, %s, %s )",
				array(
					$pid,
					$first_name,
					$user_ID,
					$note,
					$note_type,
					date("Y-m-d H:i:s")
				)
			));
   
			if($myentry) {
				$success = true;
			}
		}
	}
		//Delete Comment Starts Here
	if(isset($_GET['delete']) && $_GET['delete'] == '1') {
		if(isset($_GET['cid'])) {
		$comid = $_GET['cid'];
	$deletecomment = $wpdb->query($wpdb->prepare("DELETE FROM {$wpdb->prefix}cdrmed_notes WHERE id = %d",$comid));
		}
	}
 
	$pnotes = "SELECT * FROM {$wpdb->prefix}cdrmed_notes
				where patient_id = $pid and note_type='intake'
				ORDER BY id DESC
				";
				
	$getnotes = $wpdb->get_results($pnotes);
	$html1 .= '<div class="notesall">';
	foreach($getnotes as $key => $value) {
		$id = $value->id;
		$username = $value->user_name;
		$datecreated = $value->date_created;
		$comments = $value->value; 
		$newDate = date("m-d-Y", strtotime($datecreated));
	// History code start from here
	$pid = !isset($_GET['pid'])?null:$_GET['pid'];
		if($pid) {
			$user = $pid;
		} else {
			$pid = get_current_user_id();
		}
	$muser_info = get_userdata($pid);
	$cid = !isset($_GET['cid'])?null:$_GET['cid'];
	$first_name = $muser_info->first_name; 
	$last_name = $muser_info->last_name; 
	
	$historynotes2 = "SELECT * FROM {$wpdb->prefix}notes_history
				where patient_id = $pid AND note_id=$id ORDER BY id DESC";
				
	$historynotes = $wpdb->get_results($historynotes2);
	if($historynotes){
		$old_id = $id;

	$html1 .= '<div class="notesalla" id="notesalla'.$id.'" style="display:none;">';
	foreach($historynotes as $key => $mvalue) {
		
		$datecreated = $mvalue->date;
		$mcomments = $mvalue->comments;
		$newDate = date("m-d-Y", strtotime($datecreated));
	
		$html1 .= '<div class="previousnotes"><div class="createdby">Created By '.$first_name.'<span class="createddate">'.$datecreated.'</span></div>';	
		$html1 .= '<div class="commentsnote" id="commentsnote">'.$mcomments.'</div></div>';	
	}
		$html1 .= '</div>';
	} else {
			$html1 .= '<p class="notesalla" style="display:none;" id="notesalla'.$id.'">There are no History notes available at the moment</p>';
			$i++;
	}
	
	//Ends here
	$html1 .= '<div class="previousnotes" id="previousnotes'.$id.'">
		<div class="createdby"> 
			Created By '.$username.' <a data-target="#doc" id ="'.$id.'" class="mytooltop" href="javascript:void(0);"><span class="fa fa-question round-info" data-toggle="tooltip" title="click to see History"></span></a>
			<span class="notesddate">'.$newDate.' 
			<a class="delete-comment confirmationcall" href="'.$current_url.'&delete=1&cid='.$id.'">Delete</a>
			| <a href="javascript:void(0)" class="edit" id="edit'.$id.'">Edit</a></span></div>
			<div class="commentsnote" id="commentsnote'.$id.'">
				'.$comments.'
			</div>
			<input type="button" id="update'.$id.'" style="display:none;" value="Update Note" class="sub-but sub2" name="submit'.$id.'">
		</div>';
	}
	$html1 .= '<input type="hidden" name="cide" id="cid" value="" /></div>';
	return $html1;

	
}
add_shortcode( 'intakenotes', 'intakepost_notes' );

?>