<?php 

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly 

// Add Shortcode

function intake_edit_mbox() {
 if(is_page( 'view-complete-intake' )) {   
?>  
    <div class="modal fade" id="intakeEditMbox" role="dialog">
            <div class="modal-dialog multi-select-modal navyblue modal-lg">
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close fa fa-close" data-dismiss="modal"></button>
                        <h4 class="intake-modal-title" id="intake-title">Edit Patient Intake</h4>
                            <div id="myhealth">
                            	<div class="intake-patient-form">
                            <?php
                            echo do_shortcode('[intake_form]');
                             ?>
                             	</div>
                            </div>
                    </div>
                </div>
            
            </div>
        </div>
        
 <?php }  // Complete Intake Condition Ends
}

add_action( 'wp_footer', 'intake_edit_mbox');
?>