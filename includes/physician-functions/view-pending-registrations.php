<?php

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly 

add_shortcode( 'pending-registrations' , 'pending_users_wpd' );

function pending_users_wpd($atts){
	global $wpdb, $add_my_script;
	$url = site_url();
	$current_usid = get_current_user_id();
	$current_usid = is_dispensary_physician_child($current_usid);
	$url = site_url();
	//$physician_id = $wpdb->get_var( $wpdb->prepare( "select user_id from $wpdb->usermeta where meta_key = 'url' and meta_value = '%s' LIMIT 1", $url ) );
	
	$add_my_script= true;
	$siteurl = site_url( '/pre-registration-data/', 'http' );			
	$html = '';
	if(isset($_GET['action']) && $_GET['action'] == "delete-pen-user") {
		$html = '<div class="alert alert-success">Patient Deleted Successfully.</div>';
	}
    $html .= '<div class="pre_reg_feedback feedback"></div>
	<div id="filter-bar"></div>
	<table data-toggle="table" data-show-toggle="false" data-classes="table table-hover stripped" data-striped="true" data-show-columns="true" data-id-field="id" data-pagination="true" data-search="true" data-page-size="10" data-show-export="true" data-page-list="[25, 50, 100, 250, 500, 1000, 5000]" data-smart-display="true" data-toolbar="#filter-bar" data-show-filter="true" data-mobile-responsive="true">';
	$html .= '<thead><tr>';
	//$html .= '<th>ID</th>';
	//$html .= '<th>User Login</th>';
	$html .= '<th data-field="first_name">First Name</th>';
	$html .= '<th data-field="last_name">Last Name</th>';
	$html .= '<th data-field="user_email">User Email</th>';
	$html .= '<th data-field="details">Details</th>';
	//$html .= '<th>Signed Up</th>';
	$html .= '<th data-field="activation">Activation</th>';
	$html .= '<th data-field="deny">Deny</th>';
	$html .= '</tr></thead><tbody>';

	$exi = 0;
	$nexi = 0;
	$count = 0;
	// WP_Query arguments
	$args = array (
		'post_type'              => array( 'pre-registrations' ),
		'posts_per_page' => -1,
		'meta_query' => array(
			array(
				'key' => 'patient_parent_user_id',
				'value' => $current_usid,
				'compare' => '='
			),
		),
	);
		
	// The Query
	$query = new WP_Query( $args );

	if ( $query->have_posts() ) {
		while ( $query->have_posts() ) {
			$query->the_post();
			//echo "<pre>";print_r($query);echo "</pre>";			
			$postid = get_the_ID();
			
			$first_name = get_field( 'first_name', $postid ); 
			$last_name = get_field( 'last_name', $postid );
			$email = get_field( 'best_email', $postid );
			$date = $pfx_date = get_the_date( 'Y-m-d', $postid );
			$mmjreco = get_post_meta( $postid, 'is-mmj-reco', true );
			//$alreadyactive = get_post_meta( $postid, 'user-id-prereg', true );
			//$user = get_user_by( 'email', $email );
			$exists = email_exists($email);
						
			if($mmjreco != 'yes') {
				if(!$exists){
					$html .= '<tr>';
					//$html .= '<td>'.$signupid.'</td>';
					//$html .= '<td>'.$login.'</td>';
					if($first_name) {
						$html .= '<td>'.$first_name.'</td>';
					} else {
						$html .= '<td>N/A</td>';
					}
					if($last_name) {
						$html .= '<td>'.$last_name.'</td>';
					} else {
						$html .= '<td>N/A</td>';
					}
					if($email) {
						$html .= '<td>'.$email.'</td>';
					} else {
						$html .= '<td>N/A</td>';
					}
					$html .= '<td><a href="'.$siteurl.'?poid='.$postid.'">View</a></td>';
					//$html .= '<td>'.$date.'</td>';
					$html .= '<td><a class="active_patient_account" data-post_id="'.$postid.'" data-email_id="'.$email.'">Activate</a></td>';
					//$html .= '<td><a class="pre-regis-delete" href="'.$url.'/physicians-dashboard/?action=delete-pen-user111#menu1">Delete</a></td>';
					$html .= '<td><a class="pre-regis-delete" href="'.$url.'/physicians-dashboard/?action=delete-pen-user&emailid='.$email.'&post='.$postid.'#menu1">Delete</a></td>';
					$html .= '</tr>';
					$count++;
				}
			}
		}
	}
	/*else {
		 $html .= '<tr>';
		 $html .= '<td colspan="6">No Latest Signups</td>';
		 $html .= '</tr>';
	}*/
	// Restore original Post Data
		wp_reset_postdata();

    $html .= '</tbody></table>';
	$html .= '<input id="regicounts" type="hidden" name="pending-registrations-count" value="'.$count.'">';
	return $html;

}

add_action ('init','delete_signup1');
function delete_signup1() {
	global $wpdb;
	$act = isset($_GET['action']) ? $_GET['action'] : '';
	$postid = isset($_GET['post']) ? $_GET['post'] : '';
if($act == "delete-pen-user" && $act != '') {
		wp_delete_post( $postid );
	}
			
}

function get_user_id_by_url($url){
	global $wpdb;
	$user_id = $wpdb->get_var( $wpdb->prepare( "select user_id from $wpdb->usermeta where meta_key = 'url' and meta_value = '%s' LIMIT 1", $url ) );
	if ( $user_id ) { 
	return $user_id;
	} else {
	  return null;
	}
}

?>