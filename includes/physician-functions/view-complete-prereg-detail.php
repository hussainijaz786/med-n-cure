<?php
if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly 

function load_prereg_data() {

	$post_id = $_GET['poid'];
	
	$first_name = get_field('first_name',$post_id);
	$last_name = get_field('last_name',$post_id);
	$email = get_field('email',$post_id);
	$phone = get_field('phone',$post_id);
	$dob = get_field('dater_of_birth',$post_id);
	$street_address = get_field('street_address',$post_id);
	$address2 = get_field('address2',$post_id);
	$city = get_field('city',$post_id);
	$state = get_field('state',$post_id);
	$zip_code = get_field('zip_code',$post_id);
	$country = get_field('country',$post_id);
	$gender = get_field('gender',$post_id);
	$interest = get_field('interested In',$post_id);
	$physician_name = get_field('physician_name',$post_id);
	$physician_phone = get_field('physician_phone',$post_id);
	$reco_number = get_field('medical_marijuana_recommendation_number',$post_id);
	$mdoe = get_field('marijuana_date_of_expiration',$post_id);
	$physician_website = get_field('physician_or_clinic_verification_website',$post_id);
	$physician_recomendation = get_field('scanphoto_of_physician_recommendation',$post_id);
	$driving_license_number = get_field('drivers_license_or_id_number',$post_id);
	$scanned_license = get_field('scanphoto_of_drivers_license_or_id',$post_id);
	$hear_about_us = get_field('how_did_you_hear_about_us',$post_id);
	$contact_you = get_field('when_is_the_best_time_to_contact_you',$post_id);
	$contact_you = get_field('when_is_the_best_time_to_contact_you',$post_id);
	$current_medicines = get_field('please_list_your_current_pharmaceuticals_and_supplements',$post_id);
	$objective = get_field('what_is_your_objective',$post_id);
	$medications = get_field('please_list_your_current_pharmaceuticals_and_supplements',$post_id);
	$anything_else = get_field('tell_us_anything_else_you_would_like_us_to_know',$post_id);

	$info = '';
	$info .= '&nbsp;';
	$info .= '&nbsp;';
	$info .= '<div class="patient-details-outerwrap">';
	$info .= '<div class="row">';
	$info .= '<div class="col-md-6">';
	if($first_name){
	$info .='<div class="gfve-entry-field">';
    $info .='<span class="gfve-field-label">First Name</span>';
    $info .='<div class="rowval">'. $first_name . '</div>';
    $info .='</div>';
	}
	
	if($last_name){
	$info .='<div class="gfve-entry-field">';
    $info .='<span class="gfve-field-label">Last Name</span>';
    $info .='<div class="rowval">'. $last_name . '</div>';
    $info .='</div>';
	}
	
	if($email){
	$info .='<div class="gfve-entry-field">';
    $info .='<span class="gfve-field-label">Email</span>';
    $info .='<div class="rowval">'. $email . '</div>';
    $info .='</div>';
	}
	
	if($phone){
	$info .='<div class="gfve-entry-field">';
    $info .='<span class="gfve-field-label">Phone</span>';
    $info .='<div class="rowval">'. $phone . '</div>';
    $info .='</div>';
	}
	
	if($dob){
	$info .='<div class="gfve-entry-field">';
    $info .='<span class="gfve-field-label">Date Of Birth</span>';
    $info .='<div class="rowval">'. $dob . '</div>';
    $info .='</div>';
	}
	
	if($street_address){
	$info .='<div class="gfve-entry-field">';
    $info .='<span class="gfve-field-label">Address Line 1:</span>';
    $info .='<div class="rowval">'. $street_address . '</div>';
    $info .='</div>';
	}
	if($address2){
	$info .='<div class="gfve-entry-field">';
    $info .='<span class="gfve-field-label">Address Line2:</span>';
    $info .='<div class="rowval">'. $address2 . '</div>';
    $info .='</div>';
	}
	
	if($city){
	$info .='<div class="gfve-entry-field">';
    $info .='<span class="gfve-field-label">City</span>';
    $info .='<div class="rowval">'. $city . '</div>';
    $info .='</div>';
	}
	if($state){
	$info .='<div class="gfve-entry-field">';
    $info .='<span class="gfve-field-label">State / Province / Region</span>';
    $info .='<div class="rowval">'. $state . '</div>';
    $info .='</div>';
	}
	if($zip_code){
	$info .='<div class="gfve-entry-field">';
    $info .='<span class="gfve-field-label">ZIP / Postal Code</span>';
    $info .='<div class="rowval">'. $zip_code . '</div>';
    $info .='</div>';
	}
	if($country){
	$info .='<div class="gfve-entry-field">';
    $info .='<span class="gfve-field-label">Country</span>';
    $info .='<div class="rowval">'. $country . '</div>';
    $info .='</div>';
	}
	
	if($gender){
	$info .='<div class="gfve-entry-field">';
    $info .='<span class="gfve-field-label">Gender</span>';
    $info .='<div class="rowval">'. $gender . '</div>';
    $info .='</div>';
	}
	
	if($interest){
	$info .='<div class="gfve-entry-field">';
    $info .='<span class="gfve-field-label">Interested In</span>';
    $info .='<div class="rowval">'. $interest . '</div>';
    $info .='</div>';
	}
	
	if($physician_name){
	$info .='<div class="gfve-entry-field">';
    $info .='<span class="gfve-field-label">Physician Name</span>';
    $info .='<div class="rowval">'. $physician_name . '</div>';
    $info .='</div>';
	}
	
	if($physician_phone){
	$info .='<div class="gfve-entry-field">';
    $info .='<span class="gfve-field-label">Physician Phone</span>';
    $info .='<div class="rowval">'. $physician_phone . '</div>';
    $info .='</div>';
	}
	
	if($reco_number){
	$info .='<div class="gfve-entry-field">';
    $info .='<span class="gfve-field-label">Medical Marijuana Recommendation Number</span>';
    $info .='<div class="rowval">'. $reco_number . '</div>';
    $info .='</div>';
	}
	
	if($mdoe){
	$info .='<div class="gfve-entry-field">';
    $info .='<span class="gfve-field-label">Recommendation Date of Expiration</span>';
    $info .='<div class="rowval">'. $mdoe . '</div>';
    $info .='</div>';
	}
	
	if($physician_website){
	$info .='<div class="gfve-entry-field">';
    $info .='<span class="gfve-field-label">Physician or Clinic Verification Website</span>';
    $info .='<div class="rowval">'. $physician_website . '</div>';
    $info .='</div>';
	}
	
	if($physician_recomendation){ 

	$info .='<div class="gfve-entry-field">';
    $info .='<span class="gfve-field-label">Scan/Photo of Physician Recommendation</span>';
    $supported_image = array('gif','jpg','jpeg','png','bmp');
	$ext = strtolower(pathinfo($physician_recomendation, PATHINFO_EXTENSION));
     
  if (in_array($ext, $supported_image)) { 
   		 $info .='<div class="rowval"><img src="'. $physician_recomendation . '"></div>';
    }else{
    	$info .= ' <div class="download-button-pp rowval" id="ppt"><a href="'. $physician_recomendation . '" id="fdlinks" download><img src="'.CDRMED.'/includes/assets/img/download-file-btn.png" class="dfb" width="90"/></a></div>';
    }
    $info .='</div>';
	}
	
	if($driving_license_number){
	$info .='<div class="gfve-entry-field">';
    $info .='<span class="gfve-field-label">Driver License or I.D. Number</span>';
    $info .='<div class="rowval">'. $driving_license_number . '</div>';
    $info .='</div>';
	}
	
	if($scanned_license){
	$info .='<div class="gfve-entry-field">';
    $info .='<span class="gfve-field-label">Scan/Photo of Drivers License or I.D</span>';
    $supported_image = array('gif','jpg','jpeg','png','bmp');
	$ext = strtolower(pathinfo($scanned_license, PATHINFO_EXTENSION));
     
  if (in_array($ext, $supported_image)) { 
   		 $info .='<div class="rowval"><img src="'. $scanned_license . '"></div>';
    }else{
    	$info .= ' <div class="download-button-pp rowval" id="ppt"><a href="'. $scanned_license . '" id="fdlinks" download><img src="'.CDRMED.'/includes/assets/img/download-file-btn.png" class="dfb" width="90"/></a></div>';
    }
   
    $info .='</div>';
	}
	
	if($hear_about_us){
	$info .='<div class="gfve-entry-field">';
    $info .='<span class="gfve-field-label">How did you hear about us?</span>';
    $info .='<div class="rowval">'. $hear_about_us . '</div>';
    $info .='</div>';
	}
	
	if($contact_you){
	$info .='<div class="gfve-entry-field">';
    $info .='<span class="gfve-field-label">When is the best time to contact you?</span>';
    $info .='<div class="rowval">'. $contact_you . '</div>';
    $info .='</div>';
	}
	
	if($current_medicines){
	$info .='<div class="gfve-entry-field">';
    $info .='<span class="gfve-field-label">Please list your current pharmaceuticals and supplements</span>';
	$info .= '<div class="rowval">';
	$info .='<table>';
	$info .='<thead>';
		$info .='<tr>';
			$info .='<th>Medication Name</th>';
			$info .='<th>Dose</th>';
			$info .='<th>Frequency</th>';
		$info .='</tr>';
	$info .='</thead>';
	$info .='<tbody>';
	
	foreach($medications as $value){

		$info .='<tr>';
			$info .= '<td>'.$value['medication_name'].'<td><td>'.$value['dose'].'<td><td>'.$value['frequency'].'<td>';
			$info .='</tr>';
	}
	
	$info .='</tbody>';
	$info .='</table>';
	$info .='</div>';
    $info .='</div>';
	}
	
	if($objective){
	$info .='<div class="gfve-entry-field">';
    $info .='<span class="gfve-field-label">What is your objective?</span>';
	$info .= '<div class="rowval">';
	$info .= '<ol>';
	foreach($objective as $val) {
    $info .='<li>'. $val . '</li>';
	}
	$info .='</ol>';
	$info .='</div>';
    $info .='</div>';
	}
	
	if($anything_else){
	$info .='<div class="gfve-entry-field">';
    $info .='<span class="gfve-field-label">Tell us anything else you would like us to know:</span>';
    $info .='<div class="rowval">'. $anything_else . '</div>';
    $info .='</div>';
	}

	
	
	$info .= '</div>';
	$info .= '<div class="col-md-6">';
	$info .= do_shortcode('[patient-notes]');
	$info .= '</div>';
	$info .= '</div>';
	$info .= '</div>';
	
	
    return $info;
}
add_shortcode( 'prereg_patient_data', 'load_prereg_data' );

?>