<?php
if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly 


/*
*
*	Add New Child Physician-dispensary
*
*/
add_action( 'wp_ajax_add_child_physician_dispensary_ajax', 'add_child_physician_dispensary_ajax');
add_action( 'wp_ajax_nopriv_add_child_physician_dispensary_ajax', 'add_child_physician_dispensary_ajax');

function add_child_physician_dispensary_ajax() {
	$current_user = wp_get_current_user();
	$current_usid = get_current_user_id();
	$userrole = get_user_role();
	
	$user_name = $_POST["email"];
	$user_email = $user_name;
	//$user_id = username_exists( $user_name );
	$user_id = email_exists( $user_name );
	if($user_id) {
		$response = array('action' => false, 'refresh' => '', 'feedback' => 'This is USER ID: '.$user_id.'<br>'. $user_email. ' User already exists!<br>');
		echo json_encode($response);
		die();
	}
	
	if($current_user->user_parent == 0 && $current_user->user_parent !== ''){
		$response = array('action' => false, 'refresh' => '', 'feedback' => 'You have no access to add '.ucwords($userrole));
		echo json_encode($response);
		die();
	}
	
	$first_name = $_POST['first_name'];
	$last_name = $_POST['last_name'];
	$password = $_POST['password'];
	
	$notify = "admin";
	
	$user_id = wp_create_user( $user_name, $password, $user_email );
	
	$user_id_role = new WP_User($user_id);
	
	$userrole = get_user_role();
	$role = '';
	if(	$userrole == 'physician' || $userrole == 'caregiver' || $userrole == 'administrator' ) {
		$role = 'physician';
		cdrmed_save_activity_log('New Child Physician added!', $user_id);
	}
	else if($userrole == 'dispensary'){
		$role = 'dispensary';
		cdrmed_save_activity_log('New Child Dispensary added!', $user_id);
	}
	
	$user_id_role->set_role($role);
	$now = date("Y-m-d H:i:s");
	$setuserid = wp_update_user( array( "ID" => $user_id, "first_name" => $first_name, "last_name" => $last_name, "display_name" => $first_name, "nickname" => $first_name ) );
	
	if ( is_wp_error( $setuserid ) ) {
		$response = array('action' => false, 'refresh' => '', 'feedback' => 'Some Error has Occured!');
		echo json_encode($response);
		die();
	}
	else{
		//wp_new_user_notification( $user_id, '', $notify );
		
		$password_reset_url = password_reset_link($user_id);
		
		$args = array(
			'call_by' => 'post-registeration-'.$role,
			'receiver' => $user_email, 
			'subject_name' => $first_name.', Your MednCures Instance is Ready!', 
			'body_name' => $first_name, 
			'body_part' => '', 
			'password_reset_url' => $password_reset_url,
		);	
		cdrmed_send_email($args);
		
		update_user_meta( $user_id, 'user_parent',  0);
		update_user_meta( $user_id, 'user_parent_id', $current_usid );
		$child_count++;
		update_user_meta( $current_usid, 'user_child_count', $child_count );
		
		$response = array('action' => true, 'refresh' => true, 'feedback' => 'New '.ucwords($user_role).' added Successfully!');
		echo json_encode($response);
		die();
	}
}


/*
*
*	Remove New Child Physician-dispensary
*
*/
add_action( 'wp_ajax_remove_child_physician_dispensary_ajax', 'remove_child_physician_dispensary_ajax');
add_action( 'wp_ajax_nopriv_remove_child_physician_dispensary_ajax', 'remove_child_physician_dispensary_ajax');

function remove_child_physician_dispensary_ajax() {
	$current_usid = get_current_user_id();
	$userrole = get_user_role();
	
	$user_id = $_POST['user_id'];
	$user_role = cdrmed_get_user_role($user_id);
	if($user_role != 'patient'){
		$child_count = get_user_meta($current_usid, 'user_child_count', true);
		$child_count--;
		update_user_meta( $current_usid, 'user_child_count', $child_count );
	}
	wp_delete_user($user_id);
	echo ucwords($userrole).' remove Successfully';
	die();
}


/*
*
*	Send email To patient
*
*/
add_action( 'wp_ajax_send_email_to_patient_ajax', 'send_email_to_patient_ajax');
add_action( 'wp_ajax_nopriv_send_email_to_patient_ajax', 'send_email_to_patient_ajax');

function send_email_to_patient_ajax() {
	
	$current_user = wp_get_current_user();
	$current_usid = get_current_user_id();
	
	$patient_email = $_POST['to'];
	$subject = $_POST['subject'];
	$message = ereg_replace( "\n",'<br>', $_POST['message']);
	
	$args = array(
		'call_by' => 'email-to-patient',
		'receiver' => $patient_email,
		'subject_name' => $subject, 
		'body_name' => $message, 
		'body_part' => '', 
		'password_reset_url' => '',
	);
	cdrmed_send_email($args);
	$patient_id = email_exists( $patient_email );
	cdrmed_save_activity_log('Sent Email to Patient', $patient_id);
	
	$response = array('action' => true, 'refresh' => '', 'output' => 'Your email has been sent successfully!');
	echo json_encode($response);
	die();
}


/*
*
*	Edit Child User Profile
*
*/

add_action( 'wp_ajax_edit_child_profile_ajax', 'edit_child_profile_ajax');
add_action( 'wp_ajax_nopriv_edit_child_profile_ajax', 'edit_child_profile_ajax');

function edit_child_profile_ajax() {
	
	$current_user = wp_get_current_user();
	$current_usid = get_current_user_id();
	
	//Update Patient Profile by Physician
	$user_id = $_POST['user_id'];
	$first_name = $_POST['first_name'];
	$last_name = $_POST['last_name'];
	$email = $_POST['email'];
	$password = $_POST['password'];
	//get child user's user role
	$user_role = cdrmed_get_user_role($user_id);
	if($password != ''){
		wp_set_password( $password, $user_id );
		if($user_role == 'patient'){
			//Password is being updated forcefully by Physician
			update_user_meta( $user_id, 'who_updated_password', $current_usid );
		}
	}
	
	//$old_email = get_user_meta($user_id, '');
	$user = get_user_by( 'ID', $user_id );
	$old_email = $user->user_email;
	if($old_email != $email){
		global $wpdb;
		$sql = "UPDATE {$wpdb->users} SET user_login = %s WHERE ID = %d";
		$sql = $wpdb->prepare($sql, $email, $user_id);
		$wpdb->query($sql);
		$setuserid = wp_update_user( array( "ID" => $user_id, "first_name" => $first_name, "last_name" => $last_name, "display_name" => $first_name, "nickname" => $email, 'user_email' => $email  ) );
	}
	else{
		$setuserid = wp_update_user( array( "ID" => $user_id, "first_name" => $first_name, "last_name" => $last_name, "display_name" => $first_name  ) );
	}
	
	if( $user_role == 'patient' ){
		
		$post_id = get_user_meta($user_id, 'patient_intake', true);
		
		$date_of_birth = $_POST['date_of_birth'];
		$address1 = $_POST['address1'];
		$address2 = $_POST['address2'];
		$city = $_POST['city'];
		$state = $_POST['state'];
		$zip_code = $_POST['zip_code'];
		$country = $_POST['country'];
		
		update_field('field_56c2b5c142848', $date_of_birth, $post_id);
		update_field('field_56c2b5c140932',$address1, $post_id);
		update_field('field_56c2b5c140933',$address2, $post_id);
		update_field('field_56c2b5c140d17',$city , $post_id);
		update_field('field_56c2b5c141109', $state, $post_id);
		update_field('field_56c2b5c1414e8',$zip_code, $post_id);
		update_field('field_56c2b5c222236',$country, $post_id);
		$db = date("M-d-Y", strtotime($date_of_birth));
		$age = date_diff(date_create($db), date_create('now'))->y;
		update_post_meta($post_id, 'patient_calculated_age', $age);
		
		cdrmed_save_activity_log('Patient Profile Updated!', $user_id);
		
	}
	else{
		cdrmed_save_activity_log('Child Profile Updated!', $user_id);
	}
	
	$response = array('action' => true, 'output' => ucwords($user_role).' Profile updated successfully!');
	echo json_encode($response);
	die();
			
}