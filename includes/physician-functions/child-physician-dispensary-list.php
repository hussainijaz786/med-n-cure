<?php

/* 
File conatins the [edit_child_profile] shortcode for add new Child users
A popup window for Child User Update profile
*/

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly 

// Add Shortcode
function child_physician_dispensary_profile() {
	// Bail if not logged in or able to post
	if ( ! ( is_user_logged_in() ) ) {
		echo '<p>You Must Be Logged In To Add User</p>';
		return;
	}
	
	$current_usid = get_current_user_id();
	$child_count = get_user_meta($current_usid, 'user_child_count', true);
	$userrole = get_user_role();
	$html = '';
	$html .= '<div class="journey"><h3>Create New '.ucwords($userrole).'</h3><br></div>';
	if($child_count < 3){
		$html .= '<div class="user-registration-phy child-user-setting">
		
		<form method="post" class="user-custom-profile-form" id="user-custom-profile-form">
			<table class="form-table" id="user-custom-profile">
			<tr>
				<th>Email: <span>*</span></th>
				<td><input type="email" name="nu_email" id="nu_email" required></td>
			</tr>
			<tr>
				<th>First Name: <span>*</span></th>
				<td><input type="text" name="nu_firstname" id="nu_firstname" required></td>
			</tr>
			<tr>
				<th>Last Name: <span>*</span></th>
				<td><input type="text" name="nu_lastname" id="nu_lastname" required></td>
			</tr>
			<tr>
				<th>Password: <span>*</span></th>
				<td class="relative_position"><input type="password" name="nu_password" data-pass="password-input" id="nu_password" required>
				<input type="checkbox" name="show_password" id="show_password" class="show-password fa fa-eye" title="Show Password">
				<div id="nu_password_feedback" class="nu_password_feedback"></div>
				<input type="hidden" id="nu_password_health" class="nu_password_health"></td>
			</tr>
			<tr>
				<th>Confirm Password</th>
				<td class="relative_position"><input type="password" name="nu_c_password" data-pass="password-input" id="nu_c_password" class="password" />
				<div id="nu_c_password_feedback"></div></td>
			</tr>
			<tr><span class="feedback default-feedback"></span></tr>
			</table>
			<div class="submit-form-btn"><input name="submit" Value="Add New User" id="nu-btn" type="submit"></div>
		</form></div>';
	}


	$html .= '<div class="display-child-users">
		<table>
			<thead>
				<tr><th>Email</th><th>First Name</th><th>Last Name</th><th>Edit</th><th>Remove</th></tr>
			</thead>
			<tbody>';
			
			$user_flag = false;
			$child_users = get_users( 'meta_key=user_parent_id&meta_value='.$current_usid );
			// Array of WP_User objects.
			foreach ( $child_users as $user ) {
				$user_flag = true;
				$html .= '<tr>
				<td>'.$user->user_email.'</td>
				<td>'.$user->first_name.'</td>
				<td>'.$user->last_name.'</td>
				<td>
					
					<span class="load-cdrmed-modal-box edit-child-profile-modal patient-action-td-child" data-call_action="edit_child_profile_modal" modal-title="Edit '.$user->first_name.' Profile" modal-action="edit-child-profile" modal-footer="true" data-user_id="'.$user->id.'" data-first_name="'.$user->first_name.'" data-last_name="'.$user->last_name.'" data-email="'.$user->user_email.'" data-action="by_patient" style="cursor:pointer;"><i class="fa fa-edit"></i></span>
					
				</td>
				<td><span id="remove-user-profile-btn" class="user-remove-custom-btn" data-call-action="remove-user" data-action-permission="notallow" data-user_id="'.$user->id.'" style="cursor:pointer;"><i class="fa fa-remove"></i></span></td></tr>';
			}
			
			if(!$user_flag){
				$html .= '<tr><td>No User Found!</td></tr>';
			}
			
			$html .= '</tbody>
		</table>
		</div>';
	
	return $html;
	
}

add_shortcode( 'child_physician_dispensary_profile', 'child_physician_dispensary_profile');