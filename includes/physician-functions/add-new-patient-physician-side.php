<?php
/*
Plugin Name: User registeration Form
Description: Description
Version: 1.0
Author: wordpress
*/

add_shortcode( 'user-registeration-form', 'user_registeration_form');
function user_registeration_form() {
	$return_string = '';
	
	if(isset($_POST["submit"])) {

		$user_name = $_POST["emailid"];
		$user_email = $user_name;
		$user_id = username_exists( $user_name );
		
		if($user_id) {
			$return_string .= 'This is USER ID: '.$user_id.'<br><br>';
			$return_string .= $user_email. ' User already exists!<br>';
		}
		
		$notify = "admin";
		// User Data
		$first_name = $_POST["firstname"];
		$last_name = $_POST["lastname"];
		$password = $_POST["password"];

		if ( !$user_id and email_exists($user_email) == false ) {

			$user_id = wp_create_user( $user_name, $password, $user_email );
			$user_id_role = new WP_User($user_id);
			$user_id_role->set_role("subscriber");
			$now = date("Y-m-d H:i:s");
			$setuserid = wp_update_user( array( "ID" => $user_id, "first_name" => $first_name, "last_name" => $last_name, "display_name" => $first_name, "nickname" => $first_name ) );
			if ( is_wp_error( $setuserid ) ) {
				// There was an error, probably that user doesnt exist.	
				$return_string .= 'Some Error has Occured!';		
			} else {
				wp_new_user_notification( $user_id, '', $notify );
				$return_string .= '<h1>New User Created Successfully</h1>';
			}
			if(!empty($user_id)) {
			  // Some Action which we can perform with the user id.
			}
		} else {
			$setuserid = wp_update_user( array( "ID" => $user_id, "first_name" => $first_name, "last_name" => $last_name, "display_name" => $first_name, "nickname" => $first_name ) );
			$random_password = __("User already exists.  Password inherited.");
		}
		$return_string .= 'User Updated Successfully.';
	}
		$return_string .= '<div class="journey"><h3>Create New User</h3><br></div>';
		$return_string .= '<div class="user-registration-phy"><form method="post" class="user-reg-forms">';
		$return_string .= '<table>';
		$return_string .= '<tr>';
		$return_string .= '<td>Email:</td> <td><input type="email" name="emailid" required></td>';
		$return_string .= '</tr>';
		$return_string .= '<tr>';
		$return_string .= '<td>First Name:</td> <td><input type="text" name="firstname" required></td>';
		$return_string .= '</tr>';
		$return_string .= '<tr>';
		$return_string .= '<td>Last Name:</td> <td><input type="text" name="lastname" required></td>';
		$return_string .= '</tr>';
		$return_string .= '<tr>';
		$return_string .= '<td>password:</td> <td><input type="password" name="password" required></td>';
		$return_string .= '</tr>';
		$return_string .= '<tr>';
		$return_string .= '<td> </td><td><input type="submit" name="submit" Value="Submit"></td>';
		$return_string .= '</tr>';
		$return_string .= '</table>';
	$return_string .= '</form></div>';
	
	return $return_string;
	
}