<?php 

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly 

// Add Shortcode
function user_profile() {
	
	
	error_reporting(E_ALL);
	//ini_set('display_errors', 0);
	$html = '';
	
	
    global $wpdb;
	$gurl = site_url( '/', 'http' );
    $pid = !isset($_GET['pid'])?null:$_GET['pid'];
    $userId = '';
	
	$current_usid = get_current_user_id();
	$current_usid = is_dispensary_physician_child($current_usid);
	$page_type = isset($_GET['page_type'])? $_GET['page_type'] : '';
	
	$userrole = get_user_role();
	
	$search_key = 'patient_physician_id';
	if( $userrole == 'dispensary'){
		$search_key = 'patient_dispensary_id';
	}
	$hide_personal_info = true;
	if($userrole == 'dispensary' ){
		$hide_personal_info = null; 
	}
	
	$patient_parent_ids = json_decode(get_user_meta($pid, $search_key, true), true);
	
	if(sizeof($patient_parent_ids) > 0 && in_array($current_usid, $patient_parent_ids)){
		
	
		if($pid){ 
			$user_info = get_userdata($pid);
			$userId = $pid;
			 
		}else{
			$user_info = get_userdata($current_usid);
			 $userId   = get_current_user_id();
		}
		$posts = get_posts(array(
			'posts_per_page' => 1,
			'post_type'    => 'pre-registrations',
			'post_author'  => $userId
		));

		$user_post = get_user_meta( $userId, 'patient_intake', true ); 
		$user_life_status = get_user_meta( $userId, 'patient_life_status', true );
		$isdead = '';
		if(!empty($user_life_status) && $user_life_status == "dead") {
			$deadtrue = 'checked="checked"';
			$isdead = "| Deceased";
		} else {
			$deadtrue = '';
		}
		
		$mobile = get_field( "phone", $posts[0]->ID );
		//$db = get_field( "date_of_birth", $posts[0]->ID );
		//$email = get_field( "best_email_address", $posts[0]->ID );
		$email = $user_info->user_email;
		//$gender = get_field( "gender", $posts[0]->ID );

		$home = get_field( "physician_or_clinic_phone_number", $posts[0]->ID );

		$intake_posts = get_posts(array(
			'posts_per_page' => 1,
			'post_type'    => 'intake',
			'author'  => $userId
		));	
		
		
		
		if($intake_posts){
			
			//$intake_posts = get_post( $user_post );
		  
			$gender = get_field( "gender", $intake_posts[0]->ID );
			$db1 = get_field( "date_of_birth", $intake_posts[0]->ID );


			$db = date("M-d-Y", strtotime($db1));

			$age = date_diff(date_create($db), date_create('now'))->y;
			$diagnosis = get_field( "principal_primary_diagnosis", $intake_posts[0]->ID );
			$hphone = get_field( "home_phone",$intake_posts[0]->ID );
			$cellphone = get_field( "cell_phone",$intake_posts[0]->ID );
			$otherhc = get_field( "other_health_concerns", $intake_posts[0]->ID );
			
			$medication = get_field( "please_list_all_prescribed_and_over-the-counter_medications_taken_regularly_or_as_needed", $intake_posts[0]->ID );
		}
		else{
			
			$gender = '';
			$db = '';
			$age = '';
			$diagnosis = '';
			$hphone = '';
			$cellphone = '';
			$otherhc = '';
			$medication = null;
		}
		
		$healthcon = array();
		
		if($otherhc){
			foreach($otherhc as $value) {
				//$other .= ''.$value['concerns'].', ';
				$healthcon[] = $value['concerns'];
			}
		}
		if(sizeof($healthcon) < 1) {
			$health_concerns = "N/A";
		} else {
			$health_concerns = implode(", ", $healthcon);
		}
		//$other = "Diabetes, Epilepsy, High Blood Pressure";
		$photo = $user_info->user_avatar;
		//$purl = wp_get_attachment_url( $photo );
		if($photo !=''){
			$img = '<img src="'.$photo.'" class="round-image">';
		}else{
			$img = '<img src="'. CDRMED.'/includes/assets/img/avatar.png" alt="Avatar" class="round-image"/>';
		}
		if($diagnosis != ''){
			$diagnosis = $diagnosis;
		}else{
			$diagnosis = 'NA';
		}
		/* if($other != ''){
			$other = $other;
		}else{
			$other = 'NA';
		} */


		
		
		$arr = array();
		if($medication){
			foreach($medication as $row){
				$arr[] = $row['medication_name'];
			}
		}


		$count1 = 0;
		$count2 = 0;
		$count3 = 0;
		$medi = array();

		$allposts = $wpdb->get_results("SELECT * FROM wp_medication WHERE supplement IN ('".implode("','",$arr)."') and status = '0'");
		$inducers = array();
		$substrates = array();
		$prodrug = array();
		foreach ($allposts as $singlepost) {				// For Each to Start Comparison of Medicines
			if(in_array($singlepost->supplement, $arr)){ 	//	If To check for Single Medications
		
				if($singlepost->category == 'Inducers'){
					$inducers[] = $singlepost->supplement;
					$count1++;
				}
				if($singlepost->alert_category == 'Inducers'){
					$inducers[] = $singlepost->supplement; 
					$count1++;
				}
			
				if($singlepost->category == 'Substrates'){
					$substrates[] = $singlepost->supplement; 
					$count2++;
				}
				if($singlepost->alert_category == 'Substrates'){
					$substrates[] = $singlepost->supplement; 
					$count2++;
				}
			
				if($singlepost->category == 'Pro-drugs'){
					$prodrug[] = $singlepost->supplement; 
					$count3++;
				}
				if($singlepost->alert_category == 'Pro-drugs'){
					$prodrug[] = $singlepost->supplement; 
					$count3++;
				}
			}												//	End If To check for Single Medications
		}													// For Each End for Comparison of Medicines

		$msg1 = '<strong>INDUCERS DETECTED!</strong> <br/> '.implode(",", $inducers).'<br/> These drugs induce or enhance the activity of the cytochrome P450 system. Cannabinoids are metabolized in the cytochrome P450 system, and these drugs may facilitate or accelerate cannabinoid metabolism. When appropriate, consider increasing dosage of prescribed cannabinoids.';
		
		$msg2 = '<strong>SUBSTRATES DETECTED!</strong> <br/>'.implode(",", $substrates).'<br/>These drugs are metabolized by the cytochrome P450 system. Cannabinoids partially inhibit the cytochrome P450 system, and may delay the metabolism of these drugs, and, hence, enhance their pharmacologic activity. When appropriate, consider the possibility of lowering the dosage of these drugs.';
		
		$msg3 = '<strong>PRO-DRUGS DETECTED!</strong> <br/>'.implode(",", $prodrug).' <br/>These drugs require activation by the cytochrome P450 system. Cannabinoids partially inhibit the cytochrome P450 system and may prevent these drugs from being activated. This may compromise or completely block their effectiveness.
		   When appropriate, consider replacing with another drug.';
				   

		$html .= '<section class="clinte-detail">';
					
			if($count1 > 0 || $count2 > 0 || $count3 > 0) {
				if(!is_page('patient-documents')){
					$html .='<div class="max-internal-width darkbluec client-alert-content-bar">';
						$html .='<div class="client-alert-content">
							<div class="container-fluid">
								<div class="row">
									<div class="col-sm-12">
										<div class="row">
											<div class="col col1">';
												if($count1 > 0 || $count2 > 0 || $count3 > 0) {	
													$html .='<div class="red-alert-box alert-box"><h3 class="blink_me"><i class="fa fa-exclamation-triangle"></i>Attention</h3>
													<span id="close-alert-bar2" class="close-alert-bar2"><i class="fa fa-remove"></i></span>
													</div>';
												}							
											$html .='</div>
											<div class="col col2">';
												if($count1 > 0 || $count2 > 0 || $count3 > 0) {
													$html .= '<div class="medication-alert">';
														//$html .= '<h3 class="blink_me" style="color:#A22D31;">ALERT!</h3>';
														if ($count1 > 0) {
															$html .= '<div class="alert-child">';
															$html .= '<span class="inducer"><span class="mihead">Inducers:<span class="fa fa-question round-info"></span></span><span class="mtext"> '.implode(" ", $inducers).'</span><span> ';
															$html .= '<span class="induc">'.$msg1.'</span>';
															$html .= '</div>';
														}
														if ($count2 > 0) {
															$html .= '<div class="alert-child">';
															$html .= '<span class="substrates"><span class="mihead">Substrates:<span class="fa fa-question round-info"></span></span><span class="mtext"> '.implode(" ", $substrates).'</span></span> ';
															$html .= '<span class="subs">'.$msg2.'</span>';
															$html .= '</div>';
														}
														if ($count3 > 0) {
															$html .= '<div class="alert-child">';
															$html .= '<span class="prodrug"><span class="mihead">Pro-Drugs:<span class="fa fa-question round-info"></span></span><span class="mtext"> '.implode(" ", $prodrug).'</span></span> ';
															$html .= '<span class="prodr">'.$msg3.'</span>';
															$html .= '</div>';
														}				
													$html .= '</div>';				
												}	
											$html .= '</div>';
											$html .= '<div class="col col3">';
												if($count1 > 0 || $count2 > 0 || $count3 > 0) {
													$html .= '<span id="close-alert-bar" class="close-alert-bar"><i class="fa fa-remove"></i></span>';
												}
											$html .= '</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>';
				}
			}
					
			$html .='<div class="container">
				<div class="row">
					<div class="col-sm-12">
						<div class="row">
							<div class="sec-heading">
								<span class="left_name">'.$user_info->first_name.' '.$user_info->last_name.'</span>
								<span class="right-dob">'.$age.' Yo | '.$gender.' <span style="color:maroon;">'.$isdead.'</span></span>
									<span class="dob-tip">DOB<br>'.$db.'</span>
								</div>
								<div class="col-sm-2">'.$img.'</div>
								<div class="col-sm-4">
									<div class="clint-summery">
										<h3>Health Intake Comments</h3>
										<b>Primary Diagnosis:</b>
										<p>'.$diagnosis.' </p>
										<b>Other Health Concerns: </b>
										<p>'.$health_concerns.'</p>
									</div>
								</div>';
								$dispensary_login = '';
								$userrole = get_user_role();
								
								$html .= '<div class="col-sm-3">';
								
								$html .= '<div class="clint-summery right-sided">
									<h3>Contact Detail</h3>';
									if($hide_personal_info){
										$html .= '<p><b>Home Phone: </b>'.$cellphone.'</p>
										<p><b>Cell Phone: </b> '.$hphone.'</p>';
									}
									$html .= '<p><b>Email:</b> <a href="mailto:'.$email.'">'.$email.'</a></p>';
									$html .= '<form action="#" method="post">
										<label><input type="checkbox" name="patientlifestatus" id="patientlifestatus" data-patient="'.$pid.'" value="Deceased" '.$deadtrue.'> Patient Deceased</label><br>
										<div class="mdates patient-death-date"><span class="acf-input"><input type="text" name="deathdate" id="deathdate" placeholder="Enter Date of Death"></span></div>
										<a href="#" class="btn bordered" id="update-death" data-patient="'.$pid.'">Update Patient</a>
									</form><br>
									<div id="patient-message"></div>
								</div>
							</div>';
							if(	$userrole == 'physician' || $userrole == 'caregiver' || $userrole == 'administrator' || $userrole == 'dispensary' ){
								$html .= '<div class="col-sm-3">';
									if(is_page( 'view-complete-intake' )) {
										if($hide_personal_info){
											$html .= '<a href="#" class="btn bordered" data-target="#intakeEditMbox" data-toggle="modal">EDIT HEALTH INTAKE</a>';
										}
										$html .= '<a href="'.$gurl.'shoop-for-patient/?pid='.$pid.'" class="btn bordered">VIEW PATIENT SHOOP</a>';										
										$patient_id = intval($_GET['pid']);
										
									} elseif($page_type == 'dosing_history' && $userrole != 'dispensary'){
										$html .= '<a href="'.$gurl.'view-complete-intake/?pid='.$pid.'" class="btn bordered">View Full Intake</a>
										<a href="'.$gurl.'shoop-for-patient/?pid='.$pid.'" class="btn bordered">VIEW PATIENT SHOOP</a>';
									}
									else {
											$html .= '<a href="'.$gurl.'view-complete-intake/?pid='.$pid.'" class="btn bordered">View Full Intake</a>';
											if($userrole == 'dispensary')
											$html .= do_shortcode('[shoop-products]');	
									}
									//if(is_page( 'view-complete-intake' )) {								
									
									
									$html .= '<a style="cursor: pointer;" class="btn bordered load-cdrmed-modal-box show-mmj-recommendation" data-call_action="show_mmj_recommendation" modal-title="Patient MMJ Recommendation" modal-action="patient-mmj-recommendation" modal-footer="false" data-patient-id="'.$userId.'">MMJ Recommendation</a>';
									
									
									if(!is_page('patient-documents') && $hide_personal_info){
										$html .= '<a href="'.$gurl.'patient-documents/?pid='.$pid.'" class="btn bordered" target="_blank">View Patient Documents</a>';
									}
									//				}	
								$html .='</div>';
							}
						$html .='</div>
					</div>
				</div>
			</div>
		</section>';
	}
	return $html;
  
}  // function ends here
add_shortcode( 'user_profile_dispaly', 'user_profile' );