<?php
/*
* User Registration Call Back with Intake Data Population
* Author: Hassan
* Package @ MednCures
*/

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly 

add_action( 'wp_ajax_populate_intake_data', 'create_user_reg_intake' );

function create_user_reg_intake(){
	// Get data from Ajax Post and Assign for Later Use
	$first_name = $_POST['firstname'];
	$last_name = $_POST['lastname'];
	$email = $_POST['email'];
	$password = $_POST['password'];
	$diagnosis = $_POST['diagnosis'];
	$experience = $_POST['experience'];
	$medications = $_POST['medications'];
	$objective = $_POST['objective'];
	$cannabis = $_POST['cannabis'];
	
	
	if(isset($_POST["submit"])) {

		$user_name = $_POST["emailid"];
		$user_email = $user_name;
		$user_id = username_exists( $user_name );
		
		if($user_id) {
			$return_string .= 'This is USER ID: '.$user_id.'<br><br>';
			$return_string .= $user_email. ' User already exists!<br>';
		}
		
		$notify = "admin";
		// User Data
		$first_name = $_POST["firstname"];
		$last_name = $_POST["lastname"];
		$password = $_POST["password"];

		if ( !$user_id and email_exists($user_email) == false ) {

			$user_id = wp_create_user( $user_name, $password, $user_email );
			$user_id_role = new WP_User($user_id);
			$user_id_role->set_role("subscriber");
			$now = date("Y-m-d H:i:s");
			$setuserid = wp_update_user( array( "ID" => $user_id, "first_name" => $first_name, "last_name" => $last_name, "display_name" => $first_name, "nickname" => $first_name ) );
			if ( is_wp_error( $setuserid ) ) {
				// There was an error, probably that user doesnt exist.	
				$return_string .= 'Some Error has Occured!';		
			} else {
				wp_new_user_notification( $user_id, '', $notify );
				$return_string .= '<h1>New User Created Successfully</h1>';
			}
			if(!empty($user_id)) {
			  // Some Action which we can perform with the user id.
			}
		} else {
			$setuserid = wp_update_user( array( "ID" => $user_id, "first_name" => $first_name, "last_name" => $last_name, "display_name" => $first_name, "nickname" => $first_name ) );
			$random_password = __("User already exists.  Password inherited.");
		}
		$return_string .= 'User Updated Successfully.';
	}
	
	$user_id = intval( $_POST['patientsid'] );
	if($user_id) {
	$lifestats = get_user_meta( $user_id, 'patient_life_status', true );
	$deathdate = get_user_meta( $user_id, 'patient_death_date', true );
	echo "<span style='width: 100%; float: left; color: #39b56a;'>Patient Updated Successfully.</span>";
	}
	die();
	
}