<?php

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly 

// Add Shortcode
function new_all_patients_data_shortcode() {
	
	$html = '';
	$html .= '<table data-toggle="table"
       data-url="'.get_admin_url().'/admin-ajax.php?action=custom_geniii_callback"
       data-pagination="true"
       data-side-pagination="server"
       data-page-list="[5, 10, 20, 50, 100, 200]"
       data-search="true"
       data-height="300">
    <thead>
    <tr>
        <th data-field="fname" data-align="right" data-sortable="true">Item ID</th>
        <th data-field="lname" data-align="center" data-sortable="true">Item Name</th>
        <th data-field="email" data-sortable="true">Item Price</th>
    </tr>
    </thead>
</table>';

	return $html;
}
add_shortcode( 'new-all-patients', 'new_all_patients_data_shortcode' );

?>