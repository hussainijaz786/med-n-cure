<?php
if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly 


function load_intake_complete_data() {
	global $wpdb;
    $posts = get_posts(array(
   'numberposts' => 1,
    'post_type' => 'intake',
    'author'  => @$_GET['pid']
	));
	
	$userrole = get_user_role();
	$search_key = 'patient_physician_id';
	$hide_personal_info = true;
	if( $userrole == 'dispensary'){
		$search_key = 'patient_dispensary_id';
		$hide_personal_info = null;
	}
	$current_usid = get_current_user_id();
	$current_usid = is_dispensary_physician_child($current_usid);
	$patient_parent_ids = json_decode(get_user_meta(@$_GET['pid'], $search_key, true), true);
	$post_id = '';
	if($posts)
		$post_id = $posts[0]->ID;

	//$tab_title_a = get_field('tab_title_a',$post_id);
	
	if(sizeof($patient_parent_ids) > 0 && in_array($current_usid, $patient_parent_ids)){
		$prefix = get_field('prefix_suffix',$post_id);
		$first_name = get_field('first_name',$post_id);
		$last_name = get_field('last_name',$post_id);
	}
	
	$occupations = get_field('occupations',$post_id);
	$highest_level_education = get_field('highest_level_of_education',$post_id);
	$doctrate_type = get_field('select_doctrate_type',$post_id);
	if(sizeof($patient_parent_ids) > 0 && in_array($current_usid, $patient_parent_ids) && $hide_personal_info){
		$street_address = get_field('street_address',$post_id);
		$address2 = get_field('address2',$post_id);
	}
	$city = get_field('city',$post_id);
	$state = get_field('state',$post_id);
	$zip_code = get_field('zip_code',$post_id);
	$country = get_field('country',$post_id);
	$gender = get_field('gender',$post_id);
	if(sizeof($patient_parent_ids) > 0 && in_array($current_usid, $patient_parent_ids) && $hide_personal_info){
		$home_phone = get_field('home_phone',$post_id);
		$cell_phone = get_field('cell_phone',$post_id);
	}
	$date_of_birth = get_field('date_of_birth',$post_id);
	$ethnic_heritage = get_field('ethnic_heritage',$post_id);
	$height = get_field('height',$post_id);
	$height_inch = get_field('height_inches',$post_id);
	$weight = get_field('weight',$post_id);
	$blood_type = get_field('blood_type',$post_id);
	
	if(sizeof($patient_parent_ids) > 0 && in_array($current_usid, $patient_parent_ids) && $hide_personal_info){
		//$tab_title_b = get_field('tab_title_b',$post_id);
		$emergency_contact_firstname = get_field('emergency_contact_firstname',$post_id);
		$emergency_contact_lastname = get_field('emergency_contact_lastname',$post_id);
		$emergency_contact_phone = get_field('emergency_contact_phone',$post_id);
		$emergency_contact_relationship = get_field('emergency_contact_relationship',$post_id);
		$physician_contact_firstname = get_field('physician_contact_firstname',$post_id);
		$physician_contact_lastname = get_field('physician_contact_lastname',$post_id);
		$primary_care_physician_phone = get_field('primary_care_physician_phone',$post_id);
		$using_cannabis = get_field('does_your_physician_know_you_will_be_using_cannabis',$post_id);
		$referred_by = get_field('referred_by',$post_id);
		$minor_dependent_relationship = get_field('if_a_minor_or_dependent_adult_name_and_relationship_of_caretaker_or_guardian_completing_this_form',$post_id);
	}
	
	
	//$tab_title_c = get_field('tab_title_c',$post_id);
	$principal_primary_diagnosis = get_field('principal_primary_diagnosis',$post_id);
	$stage_cancer = get_field('stage_cancer',$post_id);
	$icd_code = get_field('icd-9_or_icd-10_code_if_known',$post_id);
	$date_of_onset = get_field('date_of_onset',$post_id);
	$duration2 = get_field('duration2',$post_id);
	$other_health_concerns = get_field('other_health_concerns',$post_id);
	
	
	//$tab_title_d = get_field('tab_title_d',$post_id);
	$list_any_past_surgeries = get_field('list_any_past_surgeries_and_treatments:',$post_id);
	
	
	//$tab_title_e = get_field('tab_title_e',$post_id);
	$list_all_prescribed = get_field('please_list_all_prescribed_and_over-the-counter_medications_taken_regularly_or_as_needed',$post_id);
	
	
	$medication_allergies = get_field('do_you_have_any_medication_allergies',$post_id);
	$describe_medication_allergies = get_field('please_describe_medication_allergies',$post_id);
	
	
	//$tab_title_f = get_field('tab_title_f',$post_id);
	$family_members_following_conditions = get_field('have_any_of_your_immediate_family_members_had_any_of_the_following_conditions',$post_id);
	$alcoholism = get_field('alcoholism',$post_id);
	$relative_alcoholism = get_field('which_relative_alcoholism',$post_id);
	$anemia = get_field('anemia',$post_id);
	$relative_anemia = get_field('which_relative_anemia',$post_id);
	$anesthesia_problems = get_field('anesthesia_problems',$post_id);
	$relative_anesthesia = get_field('which_relative_anesthesia',$post_id);
	$arthritis = get_field('arthritis',$post_id);
	$relative_arthritis = get_field('which_relative_arthritis',$post_id);
	$asthma = get_field('asthma',$post_id);
	$relative_asthama = get_field('which_relative_asthama',$post_id);
	$birth_defects = get_field('birth_defects',$post_id);
	$relative_birth_defects = get_field('which_relative_birth_defects',$post_id);
	$bleeding_problems = get_field('bleeding_problems',$post_id);
	$relative_bleeding_problems = get_field('which_relative_bleeding_problems',$post_id);
	$cancer = get_field('cancer',$post_id);
	$relative_cancer = get_field('which_relative_cancer',$post_id);
	$colon_problems_polyps_colitis = get_field('colon_problems_polyps_colitis',$post_id);
	$relative_colon_problems = get_field('which_relative_colon_problems',$post_id);
	$crohn_disease = get_field('crohn’s_disease',$post_id);
	$relative_crohons = get_field('which_relative_crohons',$post_id);
	$depression = get_field('depression',$post_id);
	$relative_depression = get_field('which_relative_depression',$post_id);
	$diabetes_type_1_child = get_field('diabetes_type_1_child',$post_id);
	$relative_diabetes_t1 = get_field('which_relative_diabetes_t1',$post_id);
	$diabetes_type_2_adult = get_field('diabetes_type_2_adult',$post_id);
	$relative_diabetes_t2 = get_field('which_relative_diabetes_t2',$post_id);
	$eczema_other = get_field('eczema_other',$post_id);
	$relative_eczema = get_field('which_relative_eczema',$post_id);
	$epilepsy_seizures = get_field('epilepsy_seizures',$post_id);
	$relative_epilepsy = get_field('which_relative_epilepsy',$post_id);
	$genetic_diseases = get_field('genetic_diseases',$post_id);
	$relative_genetic = get_field('which_relative_genetic',$post_id);
	$glaucoma = get_field('glaucoma',$post_id);
	$relative_glaucoma = get_field('which_relative_glaucoma',$post_id);
	$hay_fever_allergies = get_field('hay_fever_allergies',$post_id);
	$relative_hayfever = get_field('which_relative_hayfever',$post_id);
	$heart_attack_cad = get_field('heart_attack_cad',$post_id);
	$relative_heartattack = get_field('which_relative_heartattack',$post_id);
	$high_blood_pressure = get_field('high_blood_pressure',$post_id);
	$relative_high_blood_pressure = get_field('which_relative_high_blood_pressure',$post_id);
	$high_cholesterol = get_field('high_cholesterol',$post_id);
	$relative_high_cholesterol = get_field('which_relative_high_cholesterol',$post_id);
	$kidney_diseases = get_field('kidney_diseases',$post_id);
	$relative_kidney_disease = get_field('which_relative_kidney_disease',$post_id);
	$lupus_sle = get_field('lupus_sle',$post_id);
	$relative_lupus = get_field('which_relative_lupus',$post_id);
	$mental_retardation = get_field('mental_retardation',$post_id);
	$relative_mental_retardation = get_field('which_relative_mental_retardation',$post_id);
	$migraine_headaches = get_field('migraine_headaches',$post_id);
	$relative_migraine = get_field('which_relative_migraine',$post_id);
	$mitral_valve_prolapse = get_field('mitral_valve_prolapse',$post_id);
	$relative_mitral = get_field('which_relative_mitral',$post_id);
	$osteoporosis = get_field('osteoporosis',$post_id);
	$relative_osteoporosis = get_field('which_relative_osteoporosis',$post_id);
	$stroke_cva = get_field('stroke_cva',$post_id);
	$relative_stroke = get_field('which_relative_stroke',$post_id);
	$substance_abuse = get_field('substance_abuse',$post_id);
	$relative_substance_abuse = get_field('which_relative_substance_abuse',$post_id);
	$thyroid_disorders = get_field('thyroid_disorders',$post_id);
	$relative_thyroid_disorders = get_field('which_relative_thyroid_disorders',$post_id);
	$tuberculosis = get_field('tuberculosis',$post_id);
	$relative_tuberculosis = get_field('which_relative_tuberculosis',$post_id);
	
	
	//$tab_title_g = get_field('tab_title_g',$post_id);
	$problems_in_the_past_year = get_field('have_you_had_any_of_the_following_problems_in_the_past_year',$post_id);
	$abdominal_painbloating = get_field('abdominal_painbloating',$post_id);
	$acid_reflux_heartburn = get_field('acid_reflux_heartburn',$post_id);
	$alcoholism = get_field('alcoholism',$post_id);
	$allergies = get_field('allergies',$post_id);
	$anxiety = get_field('anxiety',$post_id);
	$asthma = get_field('asthma',$post_id);
	$atrial_fibrillation = get_field('atrial_fibrillation',$post_id);
	$bladder_problems = get_field('bladderkidneyurinary_problems',$post_id);
	$breast_problems = get_field('breast_problems',$post_id);
	$cancer = get_field('cancer',$post_id);
	$coagulation_bleeding = get_field('coagulation_bleeding_or_clotting_problems',$post_id);
	$cholesterol_problem = get_field('cholesterol_problem',$post_id);
	$chronic_low_back_pain = get_field('chronic_low_back_pain',$post_id);
	$constipation = get_field('constipationdiarrhea',$post_id);
	$coordination_problems = get_field('coordination_problems',$post_id);
	$depression = get_field('depression',$post_id);
	$diabetes_mellitus = get_field('diabetes_mellitus',$post_id);
	$dizziness = get_field('dizziness',$post_id);
	$erectile_dysfunction = get_field('erectile_dysfunction',$post_id);
	$fatigue = get_field('fatigueweakness',$post_id);
	$headaches = get_field('headaches',$post_id);
	$heart_disease = get_field('heart_diseasechest_painrhythm_disturbance',$post_id);
	$hypertension_high_blood_pressure = get_field('hypertension_high_blood_pressure',$post_id);
	$irritable_bowel_syndrome = get_field('irritable_bowel_syndrome',$post_id);
	$memory_loss = get_field('memory_loss',$post_id);
	$migraines = get_field('migraines',$post_id);
	$muscle_pain_or_swelling = get_field('musclejoint_pain_or_swelling',$post_id);
	$numbness = get_field('numbness',$post_id);
	$osteopenia= get_field('osteopenia_or_osteoporosis',$post_id);
	$general_pain= get_field('general_pain',$post_id);
	$prostate_problem= get_field('prostate_problem',$post_id);
	$sexual_function_problems= get_field('sexual_function_problems',$post_id);
	$skin_problems= get_field('skin_problems',$post_id);
	$sleep_problems= get_field('sleep_problems',$post_id);
	$substance_abuse= get_field('substance_abuse',$post_id);
	$thyroid_problem= get_field('thyroid_problem',$post_id);
	$vision_hearing_problems= get_field('visionhearing_problems',$post_id);
	$weight_lossgain= get_field('weight_lossgain',$post_id);
	$other_problems= get_field('other_problems',$post_id);
	
	
	
	//$tab_title_g2= get_field('tab_title_g2',$post_id);
	$lansky_score= get_field('lansky_score:',$post_id);
	$karnofsky_score= get_field('karnofsky_score:',$post_id);
	$ecog_score= get_field('ecog_score:',$post_id);
	
	
	
	
	//$tab_title_h= get_field('tab_title_h',$post_id);
	$your_cannabis_use= get_field('using_the_scale_below_what_best_describes_your_cannabis_use',$post_id);
	$methods_of_administration= get_field('what_methods_of_administration_have_you_tried_select_all_the_apply',$post_id);
	$feel_when_using_cannabis= get_field('please_describe_in_a_few_words_how_you_feel_when_using_cannabis',$post_id);
	$favorite_strain_of_cannabis= get_field('do_you_have_a_favorite_strain_of_cannabis',$post_id);
	$strain_name= get_field('strain_name',$post_id);
	$like_this_strain= get_field('why_do_you_like_this_strain',$post_id);
	
	
	
	
	
	
	// getline
	// 1
	$cannabis_for_your_symptoms = get_field('are_you_currently_using_cannabis_for_your_symptoms',$post_id);
	
	// 1-1
	$your_condition_previousaly = get_field('did_you_used_cannabis_products_for_your_condition_previousaly11',$post_id);
	
	// 1-1-1
	// no data
	
	// 1-1-2
	$repeater1 = get_field('did_you_used_cannabis_products_for_your_condition_previousaly_repeater',$post_id);
	
	// 1-2
	$are_you_currently_using_az_productss = get_field('are_you_currently_using_az_productss12',$post_id);
	
	// 1-2-1
	$under_our_supervision = get_field('are_you_using_the_products_under_our_supervision121',$post_id);
	
	// 1-2-1-1
	$repeater2 = get_field('are_you_using_the_products_under_our_supervision_repeater1',$post_id);
	
	// 1-2-1-2
	$repeater3 = get_field('are_you_using_the_products_under_our_supervision_repeater2',$post_id);
	
	
	// 1-2-2
	$currently_under_our_supervision = get_field('are_you_currently_using_the_products_under_our_supervision122',$post_id);
	
	// 1-2-2-1	
	$repeater4 = get_field('currently_products_under_our_supervision_repeater1',$post_id);
	// 1-2-2-2	
	$repeater5 = get_field('date_you_started_treatment_repeater2',$post_id);
	
	$rate_symptoms_level= get_field('rate_your_original_symptoms_level',$post_id);
	$rate_symptoms_level_after_cannabis= get_field('rate_your_symptoms_level_after_using_cannabis',$post_id);
	$objective_with_cannabis= get_field('what_is_your_objective_with_cannabis',$post_id);
	$committed_cannabis_regimen= get_field('how_committed_are_you_to_the_cannabis_regimen',$post_id);
	
	
	//$tab_title_i= get_field('tab_title_i',$post_id);
	$rate_diet_scale_of_1= get_field('how_do_you_rate_your_diet_on_a_scale_of_1',$post_id);
	$top_10_foods_eaten= get_field('please_list_the_top_10_foods_eaten_on_a_regular_basis',$post_id);
	$current_dietary_restrictions= get_field('please_describe_any_current_dietary_restrictions_that_you_may_have_veganvegetarianceliac_etc',$post_id);
	$food_allergies= get_field('do_you_have_any_food_allergies',$post_id);
	$describe_food_allergies= get_field('please_describe_food_allergies',$post_id);
	$recent_changes_diet= get_field('have_you_made_any_recent_changes_to_your_diet',$post_id);
	$describe_diet_changes= get_field('please_describe_diet_changes',$post_id);
	$five_foods_crave= get_field('what_top_five_foods_do_you_crave',$post_id);
	$five_foods_avoid= get_field('what_top_five_foods_do_you_avoid',$post_id);
	$snack_during_day= get_field('do_you_snack_during_the_day',$post_id);
	$snack_on= get_field('what_do_you_snack_on',$post_id);
	$days_week_eat= get_field('how_many_days_per_week_do_you_eat',$post_id);
	$cook_own_meals= get_field('do_you_generally_cook_your_own_meals',$post_id);
	$how_often= get_field('how_often',$post_id);
	$usually_eating_meals= get_field('how_are_you_usually_eating_your_meals',$post_id);
	$eat_variety_foods= get_field('do_you_eat_a_wide_variety_of_foods',$post_id);
	$consume_sugar= get_field('how_often_do_you_consume_sugar',$post_id);
	$consider_yourself= get_field('do_you_consider_yourself',$post_id);
	$html_block= get_field('html_block',$post_id);
	$alcohol= get_field('alcohol',$post_id);
	$coffee= get_field('coffee',$post_id);
	$decaf_coffee= get_field('decaf_coffee',$post_id);
	$diet_drinksaids= get_field('diet_drinksaids',$post_id);
	$soft_drinks= get_field('soft_drinks',$post_id);
	$black_tea= get_field('black_tea',$post_id);
	$green_tea= get_field('green_tea',$post_id);
	$herbal_tea= get_field('herbal_tea',$post_id);
	$sport_drinks= get_field('sport_drinks',$post_id);
	$fruit_juice= get_field('fruit_juice',$post_id);
	$water= get_field('water',$post_id);
	
	$months_week_exercise= get_field('in_the_past_3_months_how_many_days_per_week_did_you_exercise',$post_id);
	$participate_cardiovascular_exercise= get_field('do_you_participate_in_regular_cardiovascular_exercise',$post_id);
	$type_cardiovascular_exercise= get_field('what_type_regular_cardiovascular_exercise',$post_id);
	$often_cardiovascular_exercise= get_field('how_often_regular_cardiovascular_exercise',$post_id);
	$duration_cardiovascular_exercise= get_field('duration_regular_cardiovascular_exercise',$post_id);
	$intensity= get_field('how_would_you_describe_your_intensity',$post_id);
	$participate_strength_training_exercise= get_field('do_you_participate_in_regular_strength_training_exercise',$post_id);
	$intensity_strength_training_exercise= get_field('how_would_you_describe_the_intensity_regular_strength_training_exercise',$post_id);
	$kind_strength_training_exercise= get_field('what_kind_regular_strength_training_exercise',$post_id);
	$participate_flexibility_exercises= get_field('do_you_participate_in_flexibility_exercises_and_stretching',$post_id);
	$kind_excercises_stretching= get_field('what_kind_excercises_stretching',$post_id);
	$often_excercises_stretching= get_field('how_often_excercises_stretching',$post_id);
	$intensity_excercises_stretching= get_field('how_would_you_describe_the_intensity_excercises_stretching',$post_id);
	
	if(sizeof($patient_parent_ids) > 0 && in_array($current_usid, $patient_parent_ids) && $hide_personal_info){
		//$tab_title_j= get_field('tab_title_j',$post_id);
		$served_in_military= get_field('do_you_or_have_you_served_in_the_military',$post_id);
		$branch_of_service= get_field('branch_of_service',$post_id);
		$highest_rank= get_field('highest_rank',$post_id);
		$exposed_to_chemicals= get_field('are_you_aware_if_you_have_been_exposed_to_any_chemicals',$post_id);
		$chemicals_were_exposed= get_field('please_list_which_chemicals_you_were_exposed_to',$post_id);
		$war_zone_or_combat= get_field('have_you_been_exposed_to_a_war_zone_or_combat',$post_id);
		$war_zone_or_combat_where = get_field('war_zone_or_combat_where',$post_id);
		
		
		//$tab_title_k= get_field('tab_title_k',$post_id);
		$patient_intake_form= get_field('what_would_you_like_to_tell_us_that_was_not_included_in_this_patient_intake_form',$post_id);
	}
	
	
	
	$info = '';				// Initialize
	
	
	//$info = do_shortcode('[physicians-tab-nav]');
	//$info .= '&nbsp;';
	//$info .= '&nbsp;';
	
	$info .= '<div class="patient-details-outerwrap">';
	//$info .= do_shortcode('[user_profile_dispaly]');
	
	if($post_id) {
		$info .= '<div class="row"><div class="col-md-12">';
		$info .= '<div class="intake-view"><div class="sort-text">Sort Intake View:</div><select class="intake-code  col-md-4">
            <option>Entire Intake</option>
            <option value="patient-info">Patient Information</option>
            <option value="contacts">Contacts</option>
            <option value="diagnosis">Diagnosis</option>
            <option value="treatment">Treatment History</option>
            <option value="medications">Medications &amp; Supplements</option>
            <option value="family">Family History</option>
            <option value="cannabis">Cannabis Use</option>
            <option value="nutrition">Nutrition and Exercise</option>
            <option value="military">Military Service</option>
            <option value="comments">Comments</option>
        </select></div>';
		$info .= '</div></div>';
	}
	$info .= '<div class="row">';
	$info .= '<div class="col-md-7">';
	
	if($post_id) {
		//if(!empty($first_name) || !empty($date_of_birth)  || !empty($occupations)){
		$info .= '<div id="patient-info" class="box"><h2 class="noborder">Patient Information</h2>';
		if(sizeof($patient_parent_ids) > 0 && in_array($current_usid, $patient_parent_ids)){
			if($prefix){
				$info .='<div class="gfve-entry-field">';
				$info .='<span class="gfve-field-label">Prefix / Suffix</span>';
				$info .='<div class="rowval">'. $prefix . '</div>';
				$info .='</div>';
			}
			if($first_name){
				$info .='<div class="gfve-entry-field">';
				$info .='<span class="gfve-field-label">First Name</span>';
				$info .='<div class="rowval">'. $first_name . '</div>';
				$info .='</div>';
			}
			if($last_name){
				$info .='<div class="gfve-entry-field">';
				$info .='<span class="gfve-field-label">Last Name</span>';
				$info .='<div class="rowval">'. $last_name . '</div>';
				$info .='</div>';
			}
		}
		if($occupations){
			$info .='<div class="gfve-entry-field">';
			$info .='<span class="gfve-field-label">Occupation</span>';
			$info .='<div class="rowval">'. $occupations . '</div>';
			$info .='</div>';
		}
		if($highest_level_education){
			$info .='<div class="gfve-entry-field">';
			$info .='<span class="gfve-field-label">Highest Level of Education</span>';
			$info .='<div class="rowval">'. $highest_level_education . '</div>';
			$info .='</div>';
		}
		if($doctrate_type && $doctrate_type != "No"){
			$info .='<div class="gfve-entry-field">';
			$info .='<span class="gfve-field-label">Select Doctrate Type</span>';
			$info .='<div class="rowval">'. $doctrate_type . '</div>';
			$info .='</div>';
		}
		if(sizeof($patient_parent_ids) > 0 && in_array($current_usid, $patient_parent_ids) && $hide_personal_info){
			if($street_address){
				$info .='<div class="gfve-entry-field">';
				$info .='<span class="gfve-field-label">Address Line 1:</span>';
				$info .='<div class="rowval">'. $street_address . '</div>';
				$info .='</div>';
			}
			if($address2){
				$info .='<div class="gfve-entry-field">';
				$info .='<span class="gfve-field-label">Address Line2:</span>';
				$info .='<div class="rowval">'. $address2 . '</div>';
				$info .='</div>';
			}
		}
		
	
		if($city){
		$info .='<div class="gfve-entry-field">';
		$info .='<span class="gfve-field-label">City</span>';
		$info .='<div class="rowval">'. $city . '</div>';
		$info .='</div>';
		}
		if($state){
		$info .='<div class="gfve-entry-field">';
		$info .='<span class="gfve-field-label">State / Province / Region</span>';
		$info .='<div class="rowval">'. $state . '</div>';
		$info .='</div>';
		}
		if($zip_code){
		$info .='<div class="gfve-entry-field">';
		$info .='<span class="gfve-field-label">ZIP / Postal Code</span>';
		$info .='<div class="rowval">'. $zip_code . '</div>';
		$info .='</div>';
		}
		if($country){
		$info .='<div class="gfve-entry-field">';
		$info .='<span class="gfve-field-label">Country</span>';
		$info .='<div class="rowval">'. $country . '</div>';
		$info .='</div>';
		}
		if($gender){
		$info .='<div class="gfve-entry-field">';
		$info .='<span class="gfve-field-label">Gender</span>';
		$info .='<div class="rowval">'. $gender . '</div>';
		$info .='</div>';
		}
		if(sizeof($patient_parent_ids) > 0 && in_array($current_usid, $patient_parent_ids) && $hide_personal_info){
			if($home_phone){
			$info .='<div class="gfve-entry-field">';
			$info .='<span class="gfve-field-label">Home Phone</span>';
			$info .='<div class="rowval">'. $home_phone . '</div>';
			$info .='</div>';
			}
			if($cell_phone){
			$info .='<div class="gfve-entry-field">';
			$info .='<span class="gfve-field-label">Cell Phone</span>';
			$info .='<div class="rowval">'. $cell_phone . '</div>';
			$info .='</div>';
			}
		}
		
		if($date_of_birth){
		$info .='<div class="gfve-entry-field">';
		$info .='<span class="gfve-field-label">Date of Birth</span>';
		$info .='<div class="rowval">'. $date_of_birth . '</div>';
		$info .='</div>';
		}
		if($ethnic_heritage){
		$info .='<div class="gfve-entry-field">';
		$info .='<span class="gfve-field-label">Which of the following best represents your racial or ethnic heritage?</span>';
		$info .='<div class="rowval">';
		foreach($ethnic_heritage as $ethnic){
			$info .= $ethnic.'<br>';
		}
		$info .='</div>';
		$info .='</div>';
		}
		if($height){
		$info .='<div class="gfve-entry-field">';
		$info .='<span class="gfve-field-label">Height</span>';
		$info .='<div class="rowval">'. $height."'". $height_inch. '"</div>';
		$info .='</div>';
		}
		/*if($height_inch){
		$info .='<div class="gfve-entry-field">';
		$info .='<span class="gfve-field-label">Height inch</span>';
		$info .='<div class="rowval">'. $height_inch . '</div>';
		$info .='</div>';
		}*/
		if($weight){
		$info .='<div class="gfve-entry-field">';
		$info .='<span class="gfve-field-label">Weight</span>';
		$info .='<div class="rowval">'. $weight . ' lbs</div>';
		$info .='</div>';
		}
		if($blood_type){
		$info .='<div class="gfve-entry-field">';
		$info .='<span class="gfve-field-label">Blood Type</span>';
		$info .='<div class="rowval">'. $blood_type . '</div>';
		$info .='</div>';
		}
		$info .='</div>';    //Patient Information TAB Closed.
		
		
		if(sizeof($patient_parent_ids) > 0 && in_array($current_usid, $patient_parent_ids) && $hide_personal_info){


			if(!empty($emergency_contact_firstname) || !empty($emergency_contact_lastname)){
				$info .= '<div id="contacts" class="box"><h2>Contacts</h2>';
				if($emergency_contact_firstname){
				$info .='<div class="gfve-entry-field">';
				$info .='<span class="gfve-field-label">Emergency Contact First Name</span>';
				$info .='<div class="rowval">'. $emergency_contact_firstname . '</div>';
				$info .='</div>';
				}
				if($emergency_contact_lastname){
				$info .='<div class="gfve-entry-field">';
				$info .='<span class="gfve-field-label">Emergency Contact Last Name</span>';
				$info .='<div class="rowval">'. $emergency_contact_lastname . '</div>';
				$info .='</div>';
				}
				if($emergency_contact_phone){
				$info .='<div class="gfve-entry-field">';
				$info .='<span class="gfve-field-label">Emergency Contact Phone</span>';
				$info .='<div class="rowval">'. $emergency_contact_phone . '</div>';
				$info .='</div>';
				}
				if($emergency_contact_relationship){
				$info .='<div class="gfve-entry-field">';
				$info .='<span class="gfve-field-label">Emergency Contact Relationship</span>';
				$info .='<div class="rowval">'. $emergency_contact_relationship . '</div>';
				$info .='</div>';
				}
				if($physician_contact_firstname){
				$info .='<div class="gfve-entry-field">';
				$info .='<span class="gfve-field-label">Physician First Name</span>';
				$info .='<div class="rowval">'. $physician_contact_firstname . '</div>';
				$info .='</div>';
				}
				if($physician_contact_lastname){
				$info .='<div class="gfve-entry-field">';
				$info .='<span class="gfve-field-label">Physician Last Name</span>';
				$info .='<div class="rowval">'. $physician_contact_lastname . '</div>';
				$info .='</div>';
				}
				if($primary_care_physician_phone){
				$info .='<div class="gfve-entry-field">';
				$info .='<span class="gfve-field-label">Primary Care Physician Phone</span>';
				$info .='<div class="rowval">'. $primary_care_physician_phone . '</div>';
				$info .='</div>';
				}
				if($using_cannabis && $using_cannabis != "No"){
				$info .='<div class="gfve-entry-field">';
				$info .='<span class="gfve-field-label">Does your physician know you will be using cannabis?</span>';
				$info .='<div class="rowval">'. $using_cannabis . '</div>';
				$info .='</div>';
				}
				if($referred_by){
				$info .='<div class="gfve-entry-field">';
				$info .='<span class="gfve-field-label">Referred By</span>';
				$info .='<div class="rowval">'. $referred_by . '</div>';
				$info .='</div>';
				}
				if($minor_dependent_relationship){
				$info .='<div class="gfve-entry-field">';
				$info .='<span class="gfve-field-label">If a minor or dependent adult, name and relationship of caretaker or guardian completing this form:</span>';
				$info .='<div class="rowval">'. $minor_dependent_relationship . '</div>';
				$info .='</div>';
				}
				$info .='</div>';    //Contact TAB Closed.
			}else{
				$info .= '<div id="comments" class="box"><h2>No Contacts</h2></div>';
			}
		}
	
	
	
		if(!empty($principal_primary_diagnosis) || !empty($icd_code)){
			$info .= '<div id="diagnosis" class="box"><h2>Diagnosis</h2>';
			if($principal_primary_diagnosis){
			$info .='<div class="gfve-entry-field">';
			$info .='<span class="gfve-field-label">Principal / Primary Diagnosis</span>';
			$info .='<div class="rowval">'. $principal_primary_diagnosis . '</div>';
			$info .='</div>';
			}
			if($stage_cancer){
			$info .='<div class="gfve-entry-field">';
			$info .='<span class="gfve-field-label">Stage of Cancer</span>';
			$info .='<div class="rowval">'. $stage_cancer . '</div>';
			$info .='</div>';
			}
			if($icd_code){
			$info .='<div class="gfve-entry-field">';
			$info .='<span class="gfve-field-label">ICD-9 or ICD-10 Code (if known)</span>';
			$info .='<div class="rowval">'. $icd_code . '</div>';
			$info .='</div>';
			}
			if($date_of_onset){
			$info .='<div class="gfve-entry-field">';
			$info .='<span class="gfve-field-label">Date of Onset</span>';
			$info .='<div class="rowval">'. $date_of_onset . '</div>';
			$info .='</div>';
			}
			if($duration2){
			$info .='<div class="gfve-entry-field">';
			$info .='<span class="gfve-field-label">Duration</span>';
			$info .='<div class="rowval">'. $duration2 . '</div>';
			$info .='</div>';
			}
			if($other_health_concerns){
			$info .='<div class="gfve-entry-field">';
			$info .='<span class="gfve-field-label">OTHER HEALTH CONCERNS - e.g. Diabetes, epilepsy, depression, anxiety, ADHD, etc.</span>';
			$info .='<div class="rowval">';
			foreach($other_health_concerns as $value){
				$info .= $value['concerns'].'<br>';
			}
			$info .='</div>';
			$info .='</div>';
			}
			$info .='</div>';    //Diagnosis TAB Closed.
		}else{
			$info .= '<div id="comments" class="box"><h2>No Diagnosis</h2></div>';
		}
	
	
	
		if(!empty($list_any_past_surgeries)){
			$info .= '<div id="treatment" class="box"><h2>Treatment History</h2>';
			if($list_any_past_surgeries){
			$info .='<div class="gfve-entry-field">';
			$info .='<span class="gfve-field-label">List any past surgeries and treatments:</span>';
		   // $info .='<div class="rowval">'. $list_any_past_surgeries . '</div>';
			$info .='<table>';
			$info .='<thead>';
				$info .='<tr>';
					$info .='<th>Surgery/Treatment</th>';
					$info .='<th>Month </th>';
					$info .='<th>Year</th>';
				$info .='</tr>';
			$info .='</thead>';
			$info .='<tbody>';
			$loop_check = '<tr><td colspan="2">No Treatment</td></tr>';
			foreach($list_any_past_surgeries as $value){
				$loop_check = '';
				$info .='<tr>';
					$info .= '<td>'.$value['surgerytreatment'].'<td>
					<td>'.$value['month'].'<td><td>'.$value['year'].'<td>';
					$info .='</tr>';
			}
			$info .= $loop_check;
			$loop_check = '';
			$info .='</tbody>';
			$info .='</table>';
			$info .='</div>';
			}
			$info .='</div>';    //Treatment History TAB Closed.
		}else{
			$info .= '<div id="treatment" class="box"><h2>No Treatment History</h2></div>';
		}
	
	
	
		if(!empty($list_all_prescribed) || !empty($medication_allergies)){
			$info .= '<div id="medications" class="box"><h2>Medications & Supplements</h2>';
			if($list_all_prescribed){
				$info .='<div class="gfve-entry-field">';
				$info .='<span class="gfve-field-label">Please list ALL prescribed and over-the-counter medications taken regularly or as needed.</span>';
				$info .='<div class="rowval">';
				
				$info .='<table>';
				$info .='<thead>';
					$info .='<tr>';
						$info .='<th>Medication Name</th>';
						$info .='<th>Dose</th>';
						$info .='<th>Frequency</th>';
					$info .='</tr>';
				$info .='</thead>';
				$info .='<tbody>';
				$loop_check = '<tr><td colspan="3">No Medication</td></tr>';
				foreach($list_all_prescribed as $value){
					$loop_check = '';
					$info .='<tr>';
						$info .= '<td>'.$value['medication_name'].'<td><td>'.$value['dose'].'<td><td>'.$value['frequency'].'<td>';
					$info .='</tr>';
				}
				$info .= $loop_check;
				$loop_check = '';
			
		
				$info .='</tbody>';
				$info .='</table>';
				$info .='</div>';
				$info .='</div>';
			}
			if($medication_allergies){
			$info .='<div class="gfve-entry-field">';
			$info .='<span class="gfve-field-label">Do you have any medication allergies?</span>';
			$info .='<div class="rowval">'. $medication_allergies . '</div>';
			$info .='</div>';
			}
			if($describe_medication_allergies){
			$info .='<div class="gfve-entry-field">';
			$info .='<span class="gfve-field-label">Please Describe</span>';
			$info .='<div class="rowval">'. $describe_medication_allergies . '</div>';
			$info .='</div>';
			}
			$info .='</div>';    //Medications & Supplements TAB Closed.
		}else{
			$info .= '<div id="medications" class="box"><h2>No Medications & Supplements</h2></div>';
		}
	
	// Functioning Scales
	if(sizeof($patient_parent_ids) > 0 && in_array($current_usid, $patient_parent_ids) && $hide_personal_info){
		if(!empty($lansky_score) || !empty($karnofsky_score) || !empty($ecog_score)){
			$info .= '<div id="medications" class="box"><h2>Functioning Scales</h2>';
			if(!empty($lansky_score)){
				$info .='<div class="gfve-entry-field">';
				$info .='<span class="gfve-field-label">Lansky Score</span>';
				$info .='<div class="rowval">';
				
				$info .='<table>';
				$info .='<thead>';
					$info .='<tr>';
						$info .='<th>Score</th>';
						$info .='<th>Date</th>';
					$info .='</tr>';
				$info .='</thead>';
				$info .='<tbody>';
				$loop_check = '<tr><td colspan="2">No Score</td></tr>';
				foreach($lansky_score as $value){
					$loop_check = '';
					$info .='<tr>';
					$info .= '<td>'.$value['score'].'<td><td>'.$value['date'].'<td>';
					$info .='</tr>';
				}
				$info .= $loop_check;
				$loop_check = '';
				$info .='</tbody>';
				$info .='</table>';
				$info .='</div>';
				$info .='</div>';
			}
			if(!empty($karnofsky_score)){
				$info .='<div class="gfve-entry-field">';
				$info .='<span class="gfve-field-label">Karnofsky Score</span>';
				$info .='<div class="rowval">';
				
				$info .='<table>';
				$info .='<thead>';
					$info .='<tr>';
						$info .='<th>Score</th>';
						$info .='<th>Date</th>';
					$info .='</tr>';
				$info .='</thead>';
				$info .='<tbody>';
				$loop_check = '<tr><td colspan="2">No Score</td></tr>';
				foreach($karnofsky_score as $value){
					$loop_check = '';
					$info .='<tr>';
					$info .= '<td>'.$value['score'].'<td><td>'.$value['date'].'<td>';
					$info .='</tr>';
				}
				$info .= $loop_check;
				$loop_check = '';
				$info .='</tbody>';
				$info .='</table>';
				$info .='</div>';
				$info .='</div>';
			}
			if(!empty($ecog_score)){
				$info .='<div class="gfve-entry-field">';
				$info .='<span class="gfve-field-label">ECOG Score</span>';
				$info .='<div class="rowval">';
				
				$info .='<table>';
				$info .='<thead>';
					$info .='<tr>';
						$info .='<th>Score</th>';
						$info .='<th>Date</th>';
					$info .='</tr>';
				$info .='</thead>';
				$info .='<tbody>';
				$loop_check = '<tr><td colspan="2">No Score</td></tr>';
				foreach($ecog_score as $value){
					$loop_check = '';
					$info .='<tr>';
					$info .= '<td>'.$value['score'].'<td><td>'.$value['date'].'<td>';
					$info .='</tr>';
				}
				$info .= $loop_check;
				$loop_check = '';
				$info .='</tbody>';
				$info .='</table>';
				$info .='</div>';
				$info .='</div>';
			}
			
			$info .='</div>';  
		}
		else{
			$info .= '<div id="medications" class="box"><h2>No Functioning Scales</h2></div>';
		}
	} // Functionaing Scales Closed
	
	// Patient History
	if(sizeof($patient_parent_ids) > 0 && in_array($current_usid, $patient_parent_ids) && $hide_personal_info){
	
		if(!empty($abdominal_painbloating) || !empty($acid_reflux_heartburn)){
			$info .= '<div id="patient" class="box"><h2>Patient History</h2>';
			if($problems_in_the_past_year){
			$info .='<div class="gfve-entry-field">';
			$info .='<span class="gfve-field-label">Have you had any of the following problems in the past year?</span>';
			$info .='<div class="rowval">'. $problems_in_the_past_year . '</div>';
			$info .='</div>';
			}
			if($abdominal_painbloating && $abdominal_painbloating != "No"){
			$info .='<div class="gfve-entry-field">';
			$info .='<span class="gfve-field-label">Abdominal pain/bloating</span>';
			$info .='<div class="rowval">'. $abdominal_painbloating . '</div>';
			$info .='</div>';
			}
			if($acid_reflux_heartburn && $acid_reflux_heartburn != "No"){
			$info .='<div class="gfve-entry-field">';
			$info .='<span class="gfve-field-label">Acid reflux (heartburn)</span>';
			$info .='<div class="rowval">'. $acid_reflux_heartburn . '</div>';
			$info .='</div>';
			}
			if($alcoholism && $alcoholism != "No"){
			$info .='<div class="gfve-entry-field">';
			$info .='<span class="gfve-field-label">Alcoholism</span>';
			$info .='<div class="rowval">'. $alcoholism . '</div>';
			$info .='</div>';
			}
			if($allergies && $allergies != "No"){
			$info .='<div class="gfve-entry-field">';
			$info .='<span class="gfve-field-label">Allergies</span>';
			$info .='<div class="rowval">'. $allergies . '</div>';
			$info .='</div>';
			}
			if($anxiety && $anxiety != "No"){
			$info .='<div class="gfve-entry-field">';
			$info .='<span class="gfve-field-label">Anxiety</span>';
			$info .='<div class="rowval">'. $anxiety . '</div>';
			$info .='</div>';
			}
			if($asthma && $asthma != "No"){
			$info .='<div class="gfve-entry-field">';
			$info .='<span class="gfve-field-label">Asthma</span>';
			$info .='<div class="rowval">'. $asthma . '</div>';
			$info .='</div>';
			}
			if($atrial_fibrillation && $atrial_fibrillation != "No"){
			$info .='<div class="gfve-entry-field">';
			$info .='<span class="gfve-field-label">Atrial fibrillation</span>';
			$info .='<div class="rowval">'. $atrial_fibrillation . '</div>';
			$info .='</div>';
			}
			if($bladder_problems && $bladder_problems != "No"){
			$info .='<div class="gfve-entry-field">';
			$info .='<span class="gfve-field-label">Bladder/Kidney/Urinary problems</span>';
			$info .='<div class="rowval">'. $bladder_problems . '</div>';
			$info .='</div>';
			}
			if($breast_problems && $breast_problems != "No"){
			$info .='<div class="gfve-entry-field">';
			$info .='<span class="gfve-field-label">Breast problems</span>';
			$info .='<div class="rowval">'. $breast_problems . '</div>';
			$info .='</div>';
			}
			if($cancer && $cancer != "No"){
			$info .='<div class="gfve-entry-field">';
			$info .='<span class="gfve-field-label">Cancer</span>';
			$info .='<div class="rowval">'. $cancer . '</div>';
			$info .='</div>';
			}
			if($coagulation_bleeding && $coagulation_bleeding != "No"){
			$info .='<div class="gfve-entry-field">';
			$info .='<span class="gfve-field-label">Coagulation (bleeding or clotting) problems</span>';
			$info .='<div class="rowval">'. $coagulation_bleeding . '</div>';
			$info .='</div>';
			}
			if($cholesterol_problem && $cholesterol_problem != "No"){
			$info .='<div class="gfve-entry-field">';
			$info .='<span class="gfve-field-label">Cholesterol problem</span>';
			$info .='<div class="rowval">'. $cholesterol_problem . '</div>';
			$info .='</div>';
			}
			if($chronic_low_back_pain && $chronic_low_back_pain != "No"){
			$info .='<div class="gfve-entry-field">';
			$info .='<span class="gfve-field-label">Chronic low back pain</span>';
			$info .='<div class="rowval">'. $chronic_low_back_pain . '</div>';
			$info .='</div>';
			}
			if($constipation && $constipation != "No"){
			$info .='<div class="gfve-entry-field">';
			$info .='<span class="gfve-field-label">Constipation/diarrhea</span>';
			$info .='<div class="rowval">'. $constipation . '</div>';
			$info .='</div>';
			}
			if($coordination_problems && $coordination_problems != "No"){
			$info .='<div class="gfve-entry-field">';
			$info .='<span class="gfve-field-label">Coordination problems</span>';
			$info .='<div class="rowval">'. $coordination_problems . '</div>';
			$info .='</div>';
			}
			if($depression && $depression != "No"){
			$info .='<div class="gfve-entry-field">';
			$info .='<span class="gfve-field-label">Depression</span>';
			$info .='<div class="rowval">'. $depression . '</div>';
			$info .='</div>';
			}
			if($diabetes_mellitus && $diabetes_mellitus != "No"){
			$info .='<div class="gfve-entry-field">';
			$info .='<span class="gfve-field-label">Diabetes mellitus</span>';
			$info .='<div class="rowval">'. $diabetes_mellitus . '</div>';
			$info .='</div>';
			}
			if($dizziness && $dizziness != "No"){
			$info .='<div class="gfve-entry-field">';
			$info .='<span class="gfve-field-label">Dizziness</span>';
			$info .='<div class="rowval">'. $dizziness . '</div>';
			$info .='</div>';
			}
			if($erectile_dysfunction && $erectile_dysfunction != "No"){
			$info .='<div class="gfve-entry-field">';
			$info .='<span class="gfve-field-label">Erectile dysfunction</span>';
			$info .='<div class="rowval">'. $erectile_dysfunction . '</div>';
			$info .='</div>';
			}
			if($fatigue && $fatigue != "No"){
			$info .='<div class="gfve-entry-field">';
			$info .='<span class="gfve-field-label">Fatigue/weakness</span>';
			$info .='<div class="rowval">'. $fatigue . '</div>';
			$info .='</div>';
			}
			if($headaches && $headaches != "No"){
			$info .='<div class="gfve-entry-field">';
			$info .='<span class="gfve-field-label">Headaches</span>';
			$info .='<div class="rowval">'. $headaches . '</div>';
			$info .='</div>';
			}
			if($heart_disease && $heart_disease != "No"){
			$info .='<div class="gfve-entry-field">';
			$info .='<span class="gfve-field-label">Heart disease/chest pain/rhythm disturbance</span>';
			$info .='<div class="rowval">'. $heart_disease . '</div>';
			$info .='</div>';
			}
			if($hypertension_high_blood_pressure && $hypertension_high_blood_pressure != "No"){
			$info .='<div class="gfve-entry-field">';
			$info .='<span class="gfve-field-label">Hypertension (high blood pressure)</span>';
			$info .='<div class="rowval">'. $hypertension_high_blood_pressure . '</div>';
			$info .='</div>';
			}
			if($irritable_bowel_syndrome && $irritable_bowel_syndrome != "No"){
			$info .='<div class="gfve-entry-field">';
			$info .='<span class="gfve-field-label">Irritable bowel syndrome</span>';
			$info .='<div class="rowval">'. $irritable_bowel_syndrome . '</div>';
			$info .='</div>';
			}
			if($memory_loss && $memory_loss != "No"){
			$info .='<div class="gfve-entry-field">';
			$info .='<span class="gfve-field-label">Memory loss</span>';
			$info .='<div class="rowval">'. $memory_loss . '</div>';
			$info .='</div>';
			}
			if($migraines && $migraines != "No"){
			$info .='<div class="gfve-entry-field">';
			$info .='<span class="gfve-field-label">Migraines</span>';
			$info .='<div class="rowval">'. $migraines . '</div>';
			$info .='</div>';
			}
			if($muscle_pain_or_swelling && $muscle_pain_or_swelling != "No"){
			$info .='<div class="gfve-entry-field">';
			$info .='<span class="gfve-field-label">Muscle/joint pain or swelling</span>';
			$info .='<div class="rowval">'. $muscle_pain_or_swelling . '</div>';
			$info .='</div>';
			}
			if($numbness && $numbness != "No"){
			$info .='<div class="gfve-entry-field">';
			$info .='<span class="gfve-field-label">Numbness</span>';
			$info .='<div class="rowval">'. $numbness . '</div>';
			$info .='</div>';
			}
			if($osteopenia && $osteopenia != "No"){
			$info .='<div class="gfve-entry-field">';
			$info .='<span class="gfve-field-label">Osteopenia or Osteoporosis</span>';
			$info .='<div class="rowval">'. $osteopenia . '</div>';
			$info .='</div>';
			}
			if($general_pain && $general_pain != "No"){
			$info .='<div class="gfve-entry-field">';
			$info .='<span class="gfve-field-label">Pain</span>';
			$info .='<div class="rowval">'. $general_pain . '</div>';
			$info .='</div>';
			}
			if($prostate_problem && $prostate_problem != "No"){
			$info .='<div class="gfve-entry-field">';
			$info .='<span class="gfve-field-label">Prostate problem</span>';
			$info .='<div class="rowval">'. $prostate_problem . '</div>';
			$info .='</div>';
			}
			if($sexual_function_problems && $sexual_function_problems != "No"){
			$info .='<div class="gfve-entry-field">';
			$info .='<span class="gfve-field-label">Sexual function problems</span>';
			$info .='<div class="rowval">'. $sexual_function_problems . '</div>';
			$info .='</div>';
			}
			if($skin_problems && $skin_problems != "No"){
			$info .='<div class="gfve-entry-field">';
			$info .='<span class="gfve-field-label">Skin problems</span>';
			$info .='<div class="rowval">'. $skin_problems . '</div>';
			$info .='</div>';
			}
			if($sleep_problems && $sleep_problems != "No"){
			$info .='<div class="gfve-entry-field">';
			$info .='<span class="gfve-field-label">Sleep problems</span>';
			$info .='<div class="rowval">'. $sleep_problems . '</div>';
			$info .='</div>';
			}
			if($substance_abuse && $substance_abuse != "No"){
			$info .='<div class="gfve-entry-field">';
			$info .='<span class="gfve-field-label">Substance abuse</span>';
			$info .='<div class="rowval">'. $substance_abuse . '</div>';
			$info .='</div>';
			}
			if($thyroid_problem && $thyroid_problem != "No"){
			$info .='<div class="gfve-entry-field">';
			$info .='<span class="gfve-field-label">Thyroid problem</span>';
			$info .='<div class="rowval">'. $thyroid_problem . '</div>';
			$info .='</div>';
			}
			if($vision_hearing_problems && $vision_hearing_problems != "No"){
			$info .='<div class="gfve-entry-field">';
			$info .='<span class="gfve-field-label">Vision/hearing problems</span>';
			$info .='<div class="rowval">'. $vision_hearing_problems . '</div>';
			$info .='</div>';
			}
			if($weight_lossgain && $weight_lossgain != "No"){
			$info .='<div class="gfve-entry-field">';
			$info .='<span class="gfve-field-label">Weight loss/gain</span>';
			$info .='<div class="rowval">'. $weight_lossgain . '</div>';
			$info .='</div>';
			}
			if($other_problems && $other_problems != "No"){
			$info .='<div class="gfve-entry-field">';
			$info .='<span class="gfve-field-label">Other problems</span>';
			$info .='<div class="rowval">'. $other_problems . '</div>';
			$info .='</div>';
			}
			$info .='</div>';    //Patient History TAB Closed.
		}else{
			$info .= '<div id="patient" class="box"><h2>No Patient History</h2></div>';
		}
	}//Patient History Closed
	
	// Family History
	if(sizeof($patient_parent_ids) > 0 && in_array($current_usid, $patient_parent_ids) && $hide_personal_info){

		if(!empty($family_members_following_conditions) || $alcoholism != "No"){
			$info .= '<div id="family" class="box"><h2>Family History</h2>';
			if($family_members_following_conditions){
			$info .='<div class="gfve-entry-field">';
			$info .='<span class="gfve-field-label">Have any of your immediate family members had any of the following conditions?</span>';
			$info .='<div class="rowval">'. $family_members_following_conditions . '</div>';
			$info .='</div>';
			}
			if($alcoholism && $alcoholism != "No"){
			$info .='<div class="gfve-entry-field">';
			$info .='<span class="gfve-field-label">Alcoholism</span>';
			$info .='<div class="rowval">'. $alcoholism . '</div>';
			$info .='</div>';
			}
			if($relative_alcoholism){
			$info .='<div class="gfve-entry-field">';
			$info .='<span class="gfve-field-label">Which Relative</span>';
			$info .='<div class="rowval">'. $relative_alcoholism . '</div>';
			$info .='</div>';
			}
			if($anemia && $anemia != "No"){
			$info .='<div class="gfve-entry-field">';
			$info .='<span class="gfve-field-label">Anemia</span>';
			$info .='<div class="rowval">'. $anemia . '</div>';
			$info .='</div>';
			}
			if($relative_anemia){
			$info .='<div class="gfve-entry-field">';
			$info .='<span class="gfve-field-label">Which Relative</span>';
			$info .='<div class="rowval">'. $relative_anemia . '</div>';
			$info .='</div>';
			}
			if($anesthesia_problems && $anesthesia_problems != "No"){
			$info .='<div class="gfve-entry-field">';
			$info .='<span class="gfve-field-label">Anesthesia problems</span>';
			$info .='<div class="rowval">'. $anesthesia_problems . '</div>';
			$info .='</div>';
			}
			if($relative_anesthesia){
			$info .='<div class="gfve-entry-field">';
			$info .='<span class="gfve-field-label">Which Relative</span>';
			$info .='<div class="rowval">'. $relative_anesthesia . '</div>';
			$info .='</div>';
			}
			if($arthritis && $arthritis != "No"){
			$info .='<div class="gfve-entry-field">';
			$info .='<span class="gfve-field-label">Arthritis</span>';
			$info .='<div class="rowval">'. $arthritis . '</div>';
			$info .='</div>';
			}
			if($relative_arthritis){
			$info .='<div class="gfve-entry-field">';
			$info .='<span class="gfve-field-label">Which Relative</span>';
			$info .='<div class="rowval">'. $relative_arthritis . '</div>';
			$info .='</div>';
			}
			if($asthma && $asthma != "No"){
			$info .='<div class="gfve-entry-field">';
			$info .='<span class="gfve-field-label">Asthma</span>';
			$info .='<div class="rowval">'. $asthma . '</div>';
			$info .='</div>';
			}
			if($relative_asthama){
			$info .='<div class="gfve-entry-field">';
			$info .='<span class="gfve-field-label">Which Relative</span>';
			$info .='<div class="rowval">'. $relative_asthama . '</div>';
			$info .='</div>';
			}
			if($birth_defects && $birth_defects != "No"){
			$info .='<div class="gfve-entry-field">';
			$info .='<span class="gfve-field-label">Birth Defects</span>';
			$info .='<div class="rowval">'. $birth_defects . '</div>';
			$info .='</div>';
			}
			if($relative_birth_defects){
			$info .='<div class="gfve-entry-field">';
			$info .='<span class="gfve-field-label">Which Relative</span>';
			$info .='<div class="rowval">'. $relative_birth_defects . '</div>';
			$info .='</div>';
			}
			if($bleeding_problems && $bleeding_problems != "No"){
			$info .='<div class="gfve-entry-field">';
			$info .='<span class="gfve-field-label">Bleeding problems</span>';
			$info .='<div class="rowval">'. $bleeding_problems . '</div>';
			$info .='</div>';
			}
			if($relative_bleeding_problems){
			$info .='<div class="gfve-entry-field">';
			$info .='<span class="gfve-field-label">Which Relative</span>';
			$info .='<div class="rowval">'. $relative_bleeding_problems . '</div>';
			$info .='</div>';
			}
			if($cancer && $cancer != "No"){
			$info .='<div class="gfve-entry-field">';
			$info .='<span class="gfve-field-label">Cancer</span>';
			$info .='<div class="rowval">'. $cancer . '</div>';
			$info .='</div>';
			}
			if($relative_cancer){
			$info .='<div class="gfve-entry-field">';
			$info .='<span class="gfve-field-label">Which Relative</span>';
			$info .='<div class="rowval">'. $relative_cancer . '</div>';
			$info .='</div>';
			}
			if($colon_problems_polyps_colitis && $colon_problems_polyps_colitis != "No"){
			$info .='<div class="gfve-entry-field">';
			$info .='<span class="gfve-field-label">Colon Problems (polyps, colitis)</span>';
			$info .='<div class="rowval">'. $colon_problems_polyps_colitis . '</div>';
			$info .='</div>';
			}
			if($relative_colon_problems){
			$info .='<div class="gfve-entry-field">';
			$info .='<span class="gfve-field-label">Which Relative</span>';
			$info .='<div class="rowval">'. $relative_colon_problems . '</div>';
			$info .='</div>';
			}
			if($crohn_disease && $crohn_disease != "No"){
			$info .='<div class="gfve-entry-field">';
			$info .='<span class="gfve-field-label">Crohn’s Disease</span>';
			$info .='<div class="rowval">'. $crohn_disease . '</div>';
			$info .='</div>';
			}
			if($relative_crohons){
			$info .='<div class="gfve-entry-field">';
			$info .='<span class="gfve-field-label">Which Relative</span>';
			$info .='<div class="rowval">'. $relative_crohons . '</div>';
			$info .='</div>';
			}
			if($depression && $depression != "No"){
			$info .='<div class="gfve-entry-field">';
			$info .='<span class="gfve-field-label">Depression</span>';
			$info .='<div class="rowval">'. $depression . '</div>';
			$info .='</div>';
			}
			if($relative_depression){
			$info .='<div class="gfve-entry-field">';
			$info .='<span class="gfve-field-label">Which Relative</span>';
			$info .='<div class="rowval">'. $relative_depression . '</div>';
			$info .='</div>';
			}
			if($diabetes_type_1_child && $diabetes_type_1_child != "No"){
			$info .='<div class="gfve-entry-field">';
			$info .='<span class="gfve-field-label">Diabetes, Type 1 (child)</span>';
			$info .='<div class="rowval">'. $diabetes_type_1_child . '</div>';
			$info .='</div>';
			}
			if($relative_diabetes_t1){
			$info .='<div class="gfve-entry-field">';
			$info .='<span class="gfve-field-label">Which Relative</span>';
			$info .='<div class="rowval">'. $relative_diabetes_t1 . '</div>';
			$info .='</div>';
			}
			if($diabetes_type_2_adult && $diabetes_type_2_adult != "No"){
			$info .='<div class="gfve-entry-field">';
			$info .='<span class="gfve-field-label">Diabetes, Type 2 (adult)</span>';
			$info .='<div class="rowval">'. $diabetes_type_2_adult . '</div>';
			$info .='</div>';
			}
			if($relative_diabetes_t2){
			$info .='<div class="gfve-entry-field">';
			$info .='<span class="gfve-field-label">Which Relative</span>';
			$info .='<div class="rowval">'. $relative_diabetes_t2 . '</div>';
			$info .='</div>';
			}
			if($eczema_other && $eczema_other != "No"){
			$info .='<div class="gfve-entry-field">';
			$info .='<span class="gfve-field-label">Eczema Other</span>';
			$info .='<div class="rowval">'. $eczema_other . '</div>';
			$info .='</div>';
			}
			if($relative_eczema){
			$info .='<div class="gfve-entry-field">';
			$info .='<span class="gfve-field-label">Which Relative</span>';
			$info .='<div class="rowval">'. $relative_eczema . '</div>';
			$info .='</div>';
			}
			if($epilepsy_seizures && $epilepsy_seizures != "No"){
			$info .='<div class="gfve-entry-field">';
			$info .='<span class="gfve-field-label">Epilepsy (Seizures)</span>';
			$info .='<div class="rowval">'. $epilepsy_seizures . '</div>';
			$info .='</div>';
			}
			if($relative_epilepsy){
			$info .='<div class="gfve-entry-field">';
			$info .='<span class="gfve-field-label">Which Relative</span>';
			$info .='<div class="rowval">'. $relative_epilepsy . '</div>';
			$info .='</div>';
			}
			if($genetic_diseases && $genetic_diseases != "No"){
			$info .='<div class="gfve-entry-field">';
			$info .='<span class="gfve-field-label">Genetic diseases</span>';
			$info .='<div class="rowval">'. $genetic_diseases . '</div>';
			$info .='</div>';
			}
			if($relative_genetic){
			$info .='<div class="gfve-entry-field">';
			$info .='<span class="gfve-field-label">Which Relative</span>';
			$info .='<div class="rowval">'. $relative_genetic . '</div>';
			$info .='</div>';
			}
			if($glaucoma && $glaucoma != "No"){
			$info .='<div class="gfve-entry-field">';
			$info .='<span class="gfve-field-label">Glaucoma</span>';
			$info .='<div class="rowval">'. $glaucoma . '</div>';
			$info .='</div>';
			}
			if($relative_glaucoma){
			$info .='<div class="gfve-entry-field">';
			$info .='<span class="gfve-field-label">Which Relative</span>';
			$info .='<div class="rowval">'. $relative_glaucoma . '</div>';
			$info .='</div>';
			}
			if($hay_fever_allergies && $hay_fever_allergies != "No"){
			$info .='<div class="gfve-entry-field">';
			$info .='<span class="gfve-field-label">Hay fever (Allergies)</span>';
			$info .='<div class="rowval">'. $hay_fever_allergies . '</div>';
			$info .='</div>';
			}
			if($relative_hayfever){
			$info .='<div class="gfve-entry-field">';
			$info .='<span class="gfve-field-label">Which Relative</span>';
			$info .='<div class="rowval">'. $relative_hayfever . '</div>';
			$info .='</div>';
			}
			if($heart_attack_cad && $heart_attack_cad != "No"){
			$info .='<div class="gfve-entry-field">';
			$info .='<span class="gfve-field-label">Heart Attack (CAD)</span>';
			$info .='<div class="rowval">'. $heart_attack_cad . '</div>';
			$info .='</div>';
			}
			if($relative_heartattack){
			$info .='<div class="gfve-entry-field">';
			$info .='<span class="gfve-field-label">Which Relative</span>';
			$info .='<div class="rowval">'. $relative_heartattack . '</div>';
			$info .='</div>';
			}
			if($high_blood_pressure && $high_blood_pressure != "No"){
			$info .='<div class="gfve-entry-field">';
			$info .='<span class="gfve-field-label">High Blood Pressure</span>';
			$info .='<div class="rowval">'. $high_blood_pressure . '</div>';
			$info .='</div>';
			}
			if($relative_high_blood_pressure){
			$info .='<div class="gfve-entry-field">';
			$info .='<span class="gfve-field-label">Which Relative</span>';
			$info .='<div class="rowval">'. $relative_high_blood_pressure . '</div>';
			$info .='</div>';
			}
			if($high_cholesterol && $high_cholesterol != "No"){
			$info .='<div class="gfve-entry-field">';
			$info .='<span class="gfve-field-label">High cholesterol</span>';
			$info .='<div class="rowval">'. $high_cholesterol . '</div>';
			$info .='</div>';
			}
			if($relative_high_cholesterol){
			$info .='<div class="gfve-entry-field">';
			$info .='<span class="gfve-field-label">Which Relative</span>';
			$info .='<div class="rowval">'. $relative_high_cholesterol . '</div>';
			$info .='</div>';
			}
			if($kidney_diseases && $kidney_diseases != "No"){
			$info .='<div class="gfve-entry-field">';
			$info .='<span class="gfve-field-label">Kidney diseases</span>';
			$info .='<div class="rowval">'. $kidney_diseases . '</div>';
			$info .='</div>';
			}
			if($relative_kidney_disease){
			$info .='<div class="gfve-entry-field">';
			$info .='<span class="gfve-field-label">Which Relative</span>';
			$info .='<div class="rowval">'. $relative_kidney_disease . '</div>';
			$info .='</div>';
			}
			if($lupus_sle && $lupus_sle != "No"){
			$info .='<div class="gfve-entry-field">';
			$info .='<span class="gfve-field-label">Lupus (SLE)</span>';
			$info .='<div class="rowval">'. $lupus_sle . '</div>';
			$info .='</div>';
			}
			if($relative_lupus){
			$info .='<div class="gfve-entry-field">';
			$info .='<span class="gfve-field-label">Which Relative</span>';
			$info .='<div class="rowval">'. $relative_lupus . '</div>';
			$info .='</div>';
			}
			if($mental_retardation && $mental_retardation != "No"){
			$info .='<div class="gfve-entry-field">';
			$info .='<span class="gfve-field-label">Mental retardation</span>';
			$info .='<div class="rowval">'. $mental_retardation . '</div>';
			$info .='</div>';
			}
			if($relative_mental_retardation){
			$info .='<div class="gfve-entry-field">';
			$info .='<span class="gfve-field-label">Which Relative</span>';
			$info .='<div class="rowval">'. $relative_mental_retardation . '</div>';
			$info .='</div>';
			}
			if($migraine_headaches && $migraine_headaches != "No"){
			$info .='<div class="gfve-entry-field">';
			$info .='<span class="gfve-field-label">Migraine headaches</span>';
			$info .='<div class="rowval">'. $migraine_headaches . '</div>';
			$info .='</div>';
			}
			if($relative_migraine){
			$info .='<div class="gfve-entry-field">';
			$info .='<span class="gfve-field-label">Which Relative</span>';
			$info .='<div class="rowval">'. $relative_migraine . '</div>';
			$info .='</div>';
			}
			if($mitral_valve_prolapse && $mitral_valve_prolapse != "No"){
			$info .='<div class="gfve-entry-field">';
			$info .='<span class="gfve-field-label">Mitral Valve Prolapse</span>';
			$info .='<div class="rowval">'. $mitral_valve_prolapse . '</div>';
			$info .='</div>';
			}
			if($relative_mitral){
			$info .='<div class="gfve-entry-field">';
			$info .='<span class="gfve-field-label">Which Relative</span>';
			$info .='<div class="rowval">'. $relative_mitral . '</div>';
			$info .='</div>';
			}
			if($osteoporosis && $osteoporosis != "No"){
			$info .='<div class="gfve-entry-field">';
			$info .='<span class="gfve-field-label">Osteoporosis</span>';
			$info .='<div class="rowval">'. $osteoporosis . '</div>';
			$info .='</div>';
			}
			if($relative_osteoporosis){
			$info .='<div class="gfve-entry-field">';
			$info .='<span class="gfve-field-label">Which Relative</span>';
			$info .='<div class="rowval">'. $relative_osteoporosis . '</div>';
			$info .='</div>';
			}
			if($stroke_cva && $stroke_cva != "No"){
			$info .='<div class="gfve-entry-field">';
			$info .='<span class="gfve-field-label">Stroke (CVA)</span>';
			$info .='<div class="rowval">'. $stroke_cva . '</div>';
			$info .='</div>';
			}
			if($relative_stroke){
			$info .='<div class="gfve-entry-field">';
			$info .='<span class="gfve-field-label">Which Relative</span>';
			$info .='<div class="rowval">'. $relative_stroke . '</div>';
			$info .='</div>';
			}
			if($substance_abuse && $substance_abuse != "No"){
			$info .='<div class="gfve-entry-field">';
			$info .='<span class="gfve-field-label">Substance abuse</span>';
			$info .='<div class="rowval">'. $substance_abuse . '</div>';
			$info .='</div>';
			}
			if($relative_substance_abuse){
			$info .='<div class="gfve-entry-field">';
			$info .='<span class="gfve-field-label">Which Relative</span>';
			$info .='<div class="rowval">'. $relative_substance_abuse . '</div>';
			$info .='</div>';
			}
			if($thyroid_disorders && $thyroid_disorders != "No"){
			$info .='<div class="gfve-entry-field">';
			$info .='<span class="gfve-field-label">Thyroid disorders</span>';
			$info .='<div class="rowval">'. $thyroid_disorders . '</div>';
			$info .='</div>';
			}
			if($relative_thyroid_disorders){
			$info .='<div class="gfve-entry-field">';
			$info .='<span class="gfve-field-label">Which Relative</span>';
			$info .='<div class="rowval">'. $relative_thyroid_disorders . '</div>';
			$info .='</div>';
			}
			if($tuberculosis && $tuberculosis != "No"){
			$info .='<div class="gfve-entry-field">';
			$info .='<span class="gfve-field-label">Tuberculosis</span>';
			$info .='<div class="rowval">'. $tuberculosis . '</div>';
			$info .='</div>';
			}
			if($relative_tuberculosis){
			$info .='<div class="gfve-entry-field">';
			$info .='<span class="gfve-field-label">Which Relative</span>';
			$info .='<div class="rowval">'. $relative_tuberculosis . '</div>';
			$info .='</div>';
			}
			$info .='</div>';    //Family History TAB Closed.
		}else{
			$info .= '<div id="family" class="box"><h2>No Family History</h2></div>';
		}
	
	}// Family History Closed
	
	
		if(!empty($methods_of_administration) || !empty($feel_when_using_cannabis)){
			$info .= '<div id="cannabis" class="box"><h2>Cannabis Use</h2>';
			if($your_cannabis_use && $your_cannabis_use != "No"){
			$info .='<div class="gfve-entry-field">';
			$info .='<span class="gfve-field-label">What best describes your cannabis use?</span>';
			$info .='<div class="rowval">'. $your_cannabis_use . '</div>';
			$info .='</div>';
			}
			if($methods_of_administration){

				if($methods_of_administration[0] != ''){
					$lst = '';
					$lst .= '<ul>';
					foreach ($methods_of_administration as $value) {
						$lst .= '<li>'.$value.'</li>';
					}
					$lst .= '</ul>';
				}
			
				$info .='<div class="gfve-entry-field">';
				$info .='<span class="gfve-field-label">What methods of administration have you tried? (Select all the apply)</span>';
				$info .='<div class="rowval">'.$lst. '</div>';
				$info .='</div>';
			}
			if($feel_when_using_cannabis){
			$info .='<div class="gfve-entry-field">';
			$info .='<span class="gfve-field-label">Please describe in a few words how you feel when using cannabis.</span>';
			$info .='<div class="rowval">'. $feel_when_using_cannabis . '</div>';
			$info .='</div>';
			}
			if($favorite_strain_of_cannabis && $favorite_strain_of_cannabis != "No"){
			$info .='<div class="gfve-entry-field">';
			$info .='<span class="gfve-field-label">Do you have a favorite strain of cannabis?</span>';
			$info .='<div class="rowval">'. $favorite_strain_of_cannabis . '</div>';
			$info .='</div>';
			}
			if($strain_name){
			$info .='<div class="gfve-entry-field">';
			$info .='<span class="gfve-field-label">Strain Name</span>';
			$info .='<div class="rowval">'. $strain_name . '</div>';
			$info .='</div>';
			}
			if($like_this_strain){
			$info .='<div class="gfve-entry-field">';
			$info .='<span class="gfve-field-label">Why do you like this strain?</span>';
			$info .='<div class="rowval">'. $like_this_strain . '</div>';
			$info .='</div>';
			}
		
		
		
		
		
		
		
		
			// printline
			if($cannabis_for_your_symptoms && $cannabis_for_your_symptoms == "No"){
				$info .='<div class="gfve-entry-field">';
				$info .='<span class="gfve-field-label">Are you currently using cannabis for your symptoms?</span>';
				$info .='<div class="rowval">'. $cannabis_for_your_symptoms . '</div>';
				$info .='</div>';
				
				$info .='<div class="gfve-entry-field">';
				$info .='<span class="gfve-field-label">Did you used cannabis Product(s) for your condition previousaly</span>';
				$info .='<div class="rowval">'. $your_condition_previousaly . '</div>';
				$info .='</div>';
				
				if($your_condition_previousaly == 'Yes'){
					
					$info .='<div class="gfve-entry-field">';
					$info .='<span class="gfve-field-label">Cannabis Product(s) for your condition previousaly</span>';
					$info .='<table>';
					$info .='<thead>';
						$info .='<tr>';
							$info .='<th>Treatment Start Date</th>';
							$info .='<th>Products</th>';
							$info .='<th>Daily Amount</th>';
							$info .='<th>Administration Route</th>';
							$info .='<th>Treatment Stop Date</th>';
						$info .='</tr>';
					$info .='</thead>';
					$info .='<tbody>';
					$i = 0;
					$loop_check = '<tr><td colspan="5">No Product</td></tr>';
					foreach($repeater1 as $rep){
						$loop_check = '';
						$info .='<tr>';
							$info .= '<td>'.$rep['date_you_started_treatment1'].'</td>
							<td>'.$rep['products_you_used1'].'</td>
							<td>'.$rep['daily_amount_you_used1'].'</td>
							<td>'.$rep['route_of_administration1'].'</td>
							<td>'.$rep['date_you_stopped_treatment1'].'</td>';
							$info .='</tr>';
							$i++;
					}
					$info .=$loop_check;
					$loop_check = '';
					$info .='</tbody>';
					$info .='</table>';
					$info .='</div>'; 
					
				}
			}
			elseif($cannabis_for_your_symptoms && $cannabis_for_your_symptoms == "Yes"){
				$info .='<div class="gfve-entry-field">';
				$info .='<span class="gfve-field-label">Are you currently using cannabis for your symptoms?</span>';
				$info .='<div class="rowval">'. $cannabis_for_your_symptoms . '</div>';
				$info .='</div>';
				
				
				$info .='<div class="gfve-entry-field">';
				$info .='<span class="gfve-field-label">Are you currently using cannabis for your symptoms?</span>';
				$info .='<div class="rowval">'. $are_you_currently_using_az_productss . '</div>';
				$info .='</div>';
				
				if($are_you_currently_using_az_productss == 'No'){
					$info .='<div class="gfve-entry-field">';
					$info .='<span class="gfve-field-label">Are you currently using cannabis for your symptoms?</span>';
					$info .='<div class="rowval">'. $under_our_supervision . '</div>';
					$info .='</div>';
					
					if($under_our_supervision == 'No'){
						
						$info .='<div class="gfve-entry-field">';
						$info .='<span class="gfve-field-label">Your Symptoms</span>';
						$info .='<table>';
						$info .='<thead>';
							$info .='<tr>';
								$info .='<th>Treatment Start Date</th>';
								$info .='<th>Products</th>';
								$info .='<th>Daily Amount</th>';
								$info .='<th>Administration Route</th>';
							$info .='</tr>';
						$info .='</thead>';
						$info .='<tbody>';
						$i = 0;
						$loop_check = '<tr><td colspan="4">No Symptoms</td></tr>';
						foreach($repeater2 as $rep){
							$loop_check = '';
							$info .='<tr>';
								$info .= '<td>'.$rep['date_you_started_treatment2'].'</td>
								<td>'.$rep['products_currently_using2'].'</td>
								<td>'.$rep['daily_amount_currently_using2'].'</td>
								<td>'.$rep['route_of_administration2'].'</td>';
								$info .='</tr>';
								$i++;
						}
						$info .=$loop_check;
						$loop_check = '';
						$info .='</tbody>';
						$info .='</table>';
						$info .='</div>'; 
						
					}
					elseif($under_our_supervision == 'Yes'){
						
						$info .='<div class="gfve-entry-field">';
						$info .='<span class="gfve-field-label">Your Symptoms</span>';
						$info .='<table>';
						$info .='<thead>';
							$info .='<tr>';
								$info .='<th>Treatment Start Date</th>';
								$info .='<th>Products</th>';
								$info .='<th>Daily Amount</th>';
								$info .='<th>Administration Route</th>';
							$info .='</tr>';
						$info .='</thead>';
						$info .='<tbody>';
						$i = 0;
						$loop_check = '<tr><td colspan="4">No Symptoms</td></tr>';
						foreach($repeater3 as $rep){
							$loop_check = '';
							$info .='<tr>';
								$info .= '<td>'.$rep['date_you_started_treatment3'].'</td>
								<td>'.$rep['products_currently_using3'].'</td>
								<td>'.$rep['daily_amount_currently_using3'].'</td>
								<td>'.$rep['route_of_administration3'].'</td>';
								$info .='</tr>';
								$i++;
						}
						$info .=$loop_check;
						$loop_check = '';
						$info .='</tbody>';
						$info .='</table>';
						$info .='</div>'; 
						
					}
				}
				elseif($are_you_currently_using_az_productss == 'Yes'){
					
					$info .='<div class="gfve-entry-field">';
					$info .='<span class="gfve-field-label">Are you currently using cannabis for your symptoms?</span>';
					$info .='<div class="rowval">'. $currently_under_our_supervision . '</div>';
					$info .='</div>';
					
					if($currently_under_our_supervision == 'No'){
						
						$info .='<div class="gfve-entry-field">';
						$info .='<span class="gfve-field-label">Your Symptoms</span>';
						$info .='<table>';
						$info .='<thead>';
							$info .='<tr>';
								$info .='<th>Treatment Start Date</th>';
								$info .='<th>Products</th>';
								$info .='<th>Daily Amount</th>';
								$info .='<th>Administration Route</th>';
							$info .='</tr>';
						$info .='</thead>';
						$info .='<tbody>';
						$i = 0;
						$loop_check = '<tr><td colspan="4">No Symptoms</td></tr>';
						foreach($repeater4 as $rep){
							$loop_check = '';
							$info .='<tr>';
								$info .= '<td>'.$rep['date_you_started_treatment4'].'</td>
								<td>'.$rep['products_currently_using4'].'</td>
								<td>'.$rep['daily_amount_currently_using4'].'</td>
								<td>'.$rep['route_of_administration4'].'</td>';
								$info .='</tr>';
								$i++;
						}
						$info .=$loop_check;
						$loop_check = '';
						$info .='</tbody>';
						$info .='</table>';
						$info .='</div>'; 
					}
					elseif($currently_under_our_supervision == 'Yes'){
						
						$info .='<div class="gfve-entry-field">';
						$info .='<span class="gfve-field-label">Your Symptoms</span>';
						$info .='<table>';
						$info .='<thead>';
							$info .='<tr>';
								$info .='<th>Treatment Start Date</th>';
								$info .='<th>Products</th>';
								$info .='<th>Daily Amount</th>';
								$info .='<th>Administration Route</th>';
							$info .='</tr>';
						$info .='</thead>';
						$info .='<tbody>';
						$i = 0;
						$loop_check = '<tr><td colspan="4">No Symptoms</td></tr>';
						foreach($repeater5 as $rep){
							$loop_check = '';
							$info .='<tr>';
								$info .= '<td>'.$rep['date_you_started_treatment5'].'</td>
								<td>'.$rep['products_currently_using5'].'</td>
								<td>'.$rep['daily_amount_currently_using5'].'</td>
								<td>'.$rep['route_of_administration5'].'</td>';
								$info .='</tr>';
								$i++;
						}
						$info .=$loop_check;
						$loop_check = '';
						$info .='</tbody>';
						$info .='</table>';
						$info .='</div>';
					}
					
				}
				
			}
		

		
		
		
			if($rate_symptoms_level){
			$info .='<div class="gfve-entry-field">';
			$info .='<span class="gfve-field-label">Rate your ORIGINAL symptoms level</span>';
			$info .='<div class="rowval">'. $rate_symptoms_level . '</div>';
			$info .='</div>';
			}
			if($rate_symptoms_level_after_cannabis){
			$info .='<div class="gfve-entry-field">';
			$info .='<span class="gfve-field-label">Rate your symptoms level AFTER using cannabis:</span>';
			$info .='<div class="rowval">'. $rate_symptoms_level_after_cannabis . '</div>';
			$info .='</div>';
			}
			if($objective_with_cannabis){
			

			   if($objective_with_cannabis[0] != ''){
				   $lst2 = '';
					$lst2 .= '<ul>';
				foreach ($objective_with_cannabis as $value) {
					$lst2 .= '<li>'.$value.'</li>';
				}
				$lst2 .= '</ul>';
			}
			$info .='<div class="gfve-entry-field">';
			$info .='<span class="gfve-field-label">What is your objective with cannabis?</span>';
			$info .='<div class="rowval">'.$lst2. '</div>';
			$info .='</div>';
			}
			if($committed_cannabis_regimen){
			$info .='<div class="gfve-entry-field">';
			$info .='<span class="gfve-field-label">How committed are you to the cannabis regimen?</span>';
			$info .='<div class="rowval">'. $committed_cannabis_regimen . '</div>';
			$info .='</div>';
			}
			$info .='</div>';    //Cannabis TAB Closed.
		
		}else{
			$info .= '<div id="cannabis" class="box"><h2>No Cannabis Use</h2></div>';
		}
	
	// Nutrition and Excercise
	if(sizeof($patient_parent_ids) > 0 && in_array($current_usid, $patient_parent_ids) && $hide_personal_info){
		if(!empty($rate_diet_scale_of_1) || !empty($top_10_foods_eaten)){
			$info .= '<div id="nutrition" class="box"><h2>Nutrition and Exercise</h2>';
			if($rate_diet_scale_of_1){
			$info .='<div class="gfve-entry-field">';
			$info .='<span class="gfve-field-label">How do you rate your diet? On a scale of 1-10 (1-very unhealthy and 10-extremely healthy).</span>';
			$info .='<div class="rowval">'. $rate_diet_scale_of_1 . '</div>';
			$info .='</div>';
			}
			if($top_10_foods_eaten){
			$info .='<div class="gfve-entry-field">';
			$info .='<span class="gfve-field-label">Please list the top 10 foods eaten on a regular basis:</span>';
			$info .='<div class="rowval">'. $top_10_foods_eaten . '</div>';
			$info .='</div>';
			}
			if($current_dietary_restrictions){
			$info .='<div class="gfve-entry-field">';
			$info .='<span class="gfve-field-label">Please describe any current dietary restrictions that you may have (vegan/vegetarian/celiac etc.)</span>';
			$info .='<div class="rowval">'. $current_dietary_restrictions . '</div>';
			$info .='</div>';
			}
			if($food_allergies && $food_allergies != "No"){
			$info .='<div class="gfve-entry-field">';
			$info .='<span class="gfve-field-label">Do you have any food allergies?</span>';
			$info .='<div class="rowval">'. $food_allergies . '</div>';
			$info .='</div>';
			}
			if($describe_food_allergies){
			$info .='<div class="gfve-entry-field">';
			$info .='<span class="gfve-field-label">Please Describe</span>';
			$info .='<div class="rowval">'. $describe_food_allergies . '</div>';
			$info .='</div>';
			}
			if($recent_changes_diet && $recent_changes_diet != "No"){
			$info .='<div class="gfve-entry-field">';
			$info .='<span class="gfve-field-label">Have you made any recent changes to your diet?</span>';
			$info .='<div class="rowval">'. $recent_changes_diet . '</div>';
			$info .='</div>';
			}
			if($describe_diet_changes){
			$info .='<div class="gfve-entry-field">';
			$info .='<span class="gfve-field-label">Please Describe</span>';
			$info .='<div class="rowval">'. $describe_diet_changes . '</div>';
			$info .='</div>';
			}
			if($five_foods_crave){
			$info .='<div class="gfve-entry-field">';
			$info .='<span class="gfve-field-label">What top 5 foods do you crave?</span>';
			$info .='<div class="rowval">'. $five_foods_crave . '</div>';
			$info .='</div>';
			}
			if($five_foods_avoid){
			$info .='<div class="gfve-entry-field">';
			$info .='<span class="gfve-field-label">What top 5 foods do you avoid?</span>';
			$info .='<div class="rowval">'. $five_foods_avoid . '</div>';
			$info .='</div>';
			}
			if($snack_during_day && $snack_during_day != "No"){
			$info .='<div class="gfve-entry-field">';
			$info .='<span class="gfve-field-label">Do you snack during the day?</span>';
			$info .='<div class="rowval">'. $snack_during_day . '</div>';
			$info .='</div>';
			}
			if($snack_on){
			$info .='<div class="gfve-entry-field">';
			$info .='<span class="gfve-field-label">What do you snack on?</span>';
			$info .='<div class="rowval">'. $snack_on . '</div>';
			$info .='</div>';
			}
			if($days_week_eat){
			$info .='<div class="gfve-entry-field">';
			$info .='<span class="gfve-field-label">How many days per week do you eat:</span>';
			$info .='<div class="rowval">'. $days_week_eat . '</div>';
			$info .='</div>';
			}
			if($cook_own_meals && $cook_own_meals != "No"){
			$info .='<div class="gfve-entry-field">';
			$info .='<span class="gfve-field-label">Do you generally cook your own meals?</span>';
			$info .='<div class="rowval">'. $cook_own_meals . '</div>';
			$info .='</div>';
			}
			if($how_often){
			$info .='<div class="gfve-entry-field">';
			$info .='<span class="gfve-field-label">How often?</span>';
			$info .='<div class="rowval">'. $how_often . '</div>';
			$info .='</div>';
			}
			if($usually_eating_meals){
			$info .='<div class="gfve-entry-field">';
			$info .='<span class="gfve-field-label">How are you usually eating your meals?</span>';
			foreach($usually_eating_meals as $ethnic2){
			$info .= $ethnic2.'<br>';
			}
			//$info .='<div class="rowval">'. $usually_eating_meals . '</div>';
			$info .='</div>';
			}
			if($eat_variety_foods && $eat_variety_foods != "No"){
			$info .='<div class="gfve-entry-field">';
			$info .='<span class="gfve-field-label">Do you eat a wide variety of foods?</span>';
			$info .='<div class="rowval">'. $eat_variety_foods . '</div>';
			$info .='</div>';
			}
			if($consume_sugar && $consume_sugar != "No"){
			$info .='<div class="gfve-entry-field">';
			$info .='<span class="gfve-field-label">How often do you consume sugar?</span>';
			$info .='<div class="rowval">'. $consume_sugar . '</div>';
			$info .='</div>';
			}
			if($consider_yourself && $consider_yourself != "No"){
			$info .='<div class="gfve-entry-field">';
			$info .='<span class="gfve-field-label">Do you consider yourself:</span>';
			$info .='<div class="rowval">'. $consider_yourself . '</div>';
			$info .='</div>';
			}
			if($html_block){
			$info .='<div class="gfve-entry-field">';
			$info .='<span class="gfve-field-label">How many of the following do you drink per week?</span>';
			$info .='<div class="rowval">'. $html_block . '</div>';
			$info .='</div>';
			}
			if($alcohol){
			$info .='<div class="gfve-entry-field">';
			$info .='<span class="gfve-field-label">Alcohol</span>';
			$info .='<div class="rowval">'. $alcohol . '</div>';
			$info .='</div>';
			}
			if($coffee){
			$info .='<div class="gfve-entry-field">';
			$info .='<span class="gfve-field-label">Coffee</span>';
			$info .='<div class="rowval">'. $coffee . '</div>';
			$info .='</div>';
			}
			if($decaf_coffee){
			$info .='<div class="gfve-entry-field">';
			$info .='<span class="gfve-field-label">Decaf Coffee</span>';
			$info .='<div class="rowval">'. $decaf_coffee . '</div>';
			$info .='</div>';
			}
			if($diet_drinksaids){
			$info .='<div class="gfve-entry-field">';
			$info .='<span class="gfve-field-label">Diet drinks/aids</span>';
			$info .='<div class="rowval">'. $diet_drinksaids . '</div>';
			$info .='</div>';
			}
			if($soft_drinks){
			$info .='<div class="gfve-entry-field">';
			$info .='<span class="gfve-field-label">Soft drinks</span>';
			$info .='<div class="rowval">'. $soft_drinks . '</div>';
			$info .='</div>';
			}
			if($black_tea){
			$info .='<div class="gfve-entry-field">';
			$info .='<span class="gfve-field-label">Please Describe</span>';
			$info .='<div class="rowval">'. $black_tea . '</div>';
			$info .='</div>';
			}
			if($green_tea){
			$info .='<div class="gfve-entry-field">';
			$info .='<span class="gfve-field-label">Green tea</span>';
			$info .='<div class="rowval">'. $green_tea . '</div>';
			$info .='</div>';
			}
			if($herbal_tea){
			$info .='<div class="gfve-entry-field">';
			$info .='<span class="gfve-field-label">Herbal tea</span>';
			$info .='<div class="rowval">'. $herbal_tea . '</div>';
			$info .='</div>';
			}
			if($sport_drinks){
			$info .='<div class="gfve-entry-field">';
			$info .='<span class="gfve-field-label">Sport Drinks</span>';
			$info .='<div class="rowval">'. $sport_drinks . '</div>';
			$info .='</div>';
			}
			if($fruit_juice){
			$info .='<div class="gfve-entry-field">';
			$info .='<span class="gfve-field-label">Fruit juice</span>';
			$info .='<div class="rowval">'. $fruit_juice . '</div>';
			$info .='</div>';
			}
			if($water){
			$info .='<div class="gfve-entry-field">';
			$info .='<span class="gfve-field-label">Water</span>';
			$info .='<div class="rowval">'. $water . '</div>';
			$info .='</div>';
			}
			
			if($months_week_exercise){
			$info .='<div class="gfve-entry-field">';
			$info .='<span class="gfve-field-label">In the past 3 months how many days per week did you exercise?</span>';
			$info .='<div class="rowval">'. $months_week_exercise . '</div>';
			$info .='</div>';
			}
			if($participate_cardiovascular_exercise && $participate_cardiovascular_exercise != "No"){
			$info .='<div class="gfve-entry-field">';
			$info .='<span class="gfve-field-label">Do you participate in regular Cardiovascular exercise?</span>';
			$info .='<div class="rowval">'. $participate_cardiovascular_exercise . '</div>';
			$info .='</div>';
			}
			if($type_cardiovascular_exercise){
			$info .='<div class="gfve-entry-field">';
			$info .='<span class="gfve-field-label">What type?</span>';
			$info .='<div class="rowval">'. $type_cardiovascular_exercise . '</div>';
			$info .='</div>';
			}
			if($often_cardiovascular_exercise){
			$info .='<div class="gfve-entry-field">';
			$info .='<span class="gfve-field-label">How often?</span>';
			$info .='<div class="rowval">'. $often_cardiovascular_exercise . '</div>';
			$info .='</div>';
			}
			if($duration_cardiovascular_exercise){
			$info .='<div class="gfve-entry-field">';
			$info .='<span class="gfve-field-label">Duration?</span>';
			$info .='<div class="rowval">'. $duration_cardiovascular_exercise . '</div>';
			$info .='</div>';
			}
			if($intensity && $intensity != "No"){
			$info .='<div class="gfve-entry-field">';
			$info .='<span class="gfve-field-label">How would you describe your intensity?</span>';
			$info .='<div class="rowval">'. $intensity . '</div>';
			$info .='</div>';
			}
			if($participate_strength_training_exercise && $participate_strength_training_exercise != "No"){
			$info .='<div class="gfve-entry-field">';
			$info .='<span class="gfve-field-label">Do you participate in regular strength training exercise?</span>';
			$info .='<div class="rowval">'. $participate_strength_training_exercise . '</div>';
			$info .='</div>';
			}
			if($intensity_strength_training_exercise && $intensity_strength_training_exercise){
			$info .='<div class="gfve-entry-field">';
			$info .='<span class="gfve-field-label">How would you describe the intensity?</span>';
			$info .='<div class="rowval">'. $intensity_strength_training_exercise . '</div>';
			$info .='</div>';
			}
			if($kind_strength_training_exercise){
			$info .='<div class="gfve-entry-field">';
			$info .='<span class="gfve-field-label">What kind?</span>';
			$info .='<div class="rowval">'. $kind_strength_training_exercise . '</div>';
			$info .='</div>';
			}
			if($participate_flexibility_exercises && $participate_flexibility_exercises != "No"){
			$info .='<div class="gfve-entry-field">';
			$info .='<span class="gfve-field-label">Do you participate in flexibility exercises and stretching?</span>';
			$info .='<div class="rowval">'. $participate_flexibility_exercises . '</div>';
			$info .='</div>';
			}
			if($kind_excercises_stretching){
			$info .='<div class="gfve-entry-field">';
			$info .='<span class="gfve-field-label">What kind?</span>';
			$info .='<div class="rowval">'. $kind_excercises_stretching . '</div>';
			$info .='</div>';
			}
			if($often_excercises_stretching){
			$info .='<div class="gfve-entry-field">';
			$info .='<span class="gfve-field-label">How often?</span>';
			$info .='<div class="rowval">'. $often_excercises_stretching . '</div>';
			$info .='</div>';
			}
			if($intensity_excercises_stretching && $intensity_excercises_stretching != "No"){
			$info .='<div class="gfve-entry-field">';
			$info .='<span class="gfve-field-label">How would you describe the intensity?</span>';
			$info .='<div class="rowval">'. $intensity_excercises_stretching . '</div>';
			$info .='</div>';
			}
			$info .='</div>';    //Nutrition and Exercise TAB Closed.
		}else{
			$info .= '<div id="nutrition" class="box"><h2> No Nutrition and Exercise</h2></div>';
		}
		
	}// Nutrition Closed
			
		if(sizeof($patient_parent_ids) > 0 && in_array($current_usid, $patient_parent_ids) && $hide_personal_info){
			if(!empty($served_in_military) || !empty($branch_of_service)){
				$info .= '<div id="military" class="box"><h2>Military Service</h2>';
				if($served_in_military){
				$info .='<div class="gfve-entry-field">';
				$info .='<span class="gfve-field-label">Do you or have you served in the Military?</span>';
				$info .='<div class="rowval">'. $served_in_military . '</div>';
				$info .='</div>';
				}
				if($branch_of_service){
				$info .='<div class="gfve-entry-field">';
				$info .='<span class="gfve-field-label">Branch of Service:</span>';
				$info .='<div class="rowval">'. $branch_of_service . '</div>';
				$info .='</div>';
				}
				if($highest_rank){
				$info .='<div class="gfve-entry-field">';
				$info .='<span class="gfve-field-label">Highest Rank</span>';
				$info .='<div class="rowval">'. $highest_rank . '</div>';
				$info .='</div>';
				}
				if($exposed_to_chemicals){
				$info .='<div class="gfve-entry-field">';
				$info .='<span class="gfve-field-label">Are you aware if you have been exposed to any chemicals?</span>';
				$info .='<div class="rowval">'. $exposed_to_chemicals . '</div>';
				$info .='</div>';
				}
				if($chemicals_were_exposed){
				$info .='<div class="gfve-entry-field">';
				$info .='<span class="gfve-field-label">Please list which chemicals you were exposed to:</span>';
				$info .='<div class="rowval">'. $chemicals_were_exposed . '</div>';
				$info .='</div>';
				}
				if($war_zone_or_combat){
					$info .='<div class="gfve-entry-field">';
					$info .='<span class="gfve-field-label">Have you been exposed to a war zone or combat?</span>';
					$info .='<div class="rowval">'. $war_zone_or_combat . '</div>';
					$info .='</div>';
				}
				if($war_zone_or_combat_where){
					$info .='<div class="gfve-entry-field">';
					$info .='<span class="gfve-field-label">Where?</span>';
					$info .='<div class="rowval">'. $war_zone_or_combat_where . '</div>';
					$info .='</div>';
				}
				$info .='</div>';    //Military Service TAB Closed.
			}else{
				$info .= '<div id="military" class="box"><h2>No Military Service</h2></div>';
			}
	
	
	
			if(!empty($patient_intake_form)){
				$info .= '<div id="comments" class="box"><h2>Comments</h2>';
				if($patient_intake_form){
					$info .='<div class="gfve-entry-field">';
					$info .='<span class="gfve-field-label">Please tell us in your own words what you feel is most important.</span>';
					$info .='<div class="rowval">'. $patient_intake_form . '</div>';
					$info .='</div>';
				}
				$info .='</div>';    //Comments TAB Closed.
			}else{
				$info .= '<div id="comments" class="box"><h2>No Comments</h2></div>';
			}
		}
	} else {   // If condition for Details of Patient
	
		$info .= '<div class="col-md-12"><div class="journey"><h2>Patient hasn\'t filled the intake yet</h2></div></div>';
	
	}
	$info .= '</div>';
	$info .= '<div class="col-md-5">';
	$info .= do_shortcode('[patient-notes]');
	$info .= '</div>';
	$info .= '</div>';
	$info .= '</div>';
	
	
    return $info;
}
add_shortcode( 'intake_complete_patient_data', 'load_intake_complete_data' );

?>