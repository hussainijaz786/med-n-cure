<?php

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly 

function patient_intake_short_detail() {
	
	$current_url = home_url( add_query_arg( NULL, NULL ) );
	$url = site_url();
	$pid = isset($_GET['pid']) ? $_GET['pid'] : '';
	
	//Get patient Intake ID
	$post_id = get_user_meta($pid, 'patient_intake', true);
	//Get patient Pre-Registeration ID
	$post_id2 = get_user_meta($pid, 'pre-registration-id', true);
	$principal_primary_diagnosis = get_field('principal_primary_diagnosis',$post_id);
	$html1 = '';
	$html1 .= '<div class="col-md-12" style="margin-bottom:20px;">
		<div class="row">';
			$info = '<div class="col-md-6">';
				if(!empty($principal_primary_diagnosis) || !empty($icd_code)){
					$stage_cancer = get_field('stage_cancer',$post_id);
					$icd_code = get_field('icd-9_or_icd-10_code_if_known',$post_id);
					$date_of_onset = get_field('date_of_onset',$post_id);
					$duration2 = get_field('duration2',$post_id);
					$other_health_concerns = get_field('other_health_concerns',$post_id);
				}
				else{
					$principal_primary_diagnosis = get_field('principal_primary_diagnosis',$post_id2);
					$stage_cancer = get_field('stage_cancer',$post_id);
					$icd_code = get_field('icd-9_or_icd-10_code_if_known',$post_id);
					$date_of_onset = get_field('date_of_onset',$post_id);
					$duration2 = get_field('duration2',$post_id);
					$other_health_concerns = get_field('other_health_concerns',$post_id);
				}
	
				if(!empty($principal_primary_diagnosis) || !empty($icd_code)){
					$info .= '<div id="diagnosis" class="box"><h2>Diagnosis</h2>';
					if($principal_primary_diagnosis){
						$info .='<div class="gfve-entry-field">';
						$info .='<span class="gfve-field-label">Principal / Primary Diagnosis</span>';
						$info .='<div class="rowval">'. $principal_primary_diagnosis . '</div>';
						$info .='</div>';
					}
					if($stage_cancer){
						$info .='<div class="gfve-entry-field">';
						$info .='<span class="gfve-field-label">Stage of Cancer</span>';
						$info .='<div class="rowval">'. $stage_cancer . '</div>';
						$info .='</div>';
					}
					if($icd_code){
						$info .='<div class="gfve-entry-field">';
						$info .='<span class="gfve-field-label">ICD-9 or ICD-10 Code (if known)</span>';
						$info .='<div class="rowval">'. $icd_code . '</div>';
						$info .='</div>';
					}
					if($date_of_onset){
						$info .='<div class="gfve-entry-field">';
						$info .='<span class="gfve-field-label">Date of Onset</span>';
						$info .='<div class="rowval">'. $date_of_onset . '</div>';
						$info .='</div>';
					}
					if($duration2){
						$info .='<div class="gfve-entry-field">';
						$info .='<span class="gfve-field-label">Duration</span>';
						$info .='<div class="rowval">'. $duration2 . '</div>';
						$info .='</div>';
					}
					if($other_health_concerns){
						$info .='<div class="gfve-entry-field">';
						$info .='<span class="gfve-field-label">OTHER HEALTH CONCERNS - e.g. Diabetes, epilepsy, depression, anxiety, ADHD, etc.</span>';
						$info .='<div class="rowval">';
						foreach($other_health_concerns as $value){
							$info .= $value['concerns'].'<br>';
						}
						$info .='</div>';
						$info .='</div>';
					}
					$info .='</div>';    //Diagnosis TAB Closed.
				}else{
					$info .= '<br><div id="comments" class="box"><h2>No Diagnosis</h2></div>';
				}
			$info .='</div>';
			$html1 .= $info;
	
			$your_cannabis_use = get_field('using_the_scale_below_what_best_describes_your_cannabis_use',$post_id);
			$objective_with_cannabis = get_field('what_is_your_objective_with_cannabis',$post_id);
			if($your_cannabis_use && $your_cannabis_use != "No"){
			}
			else{
				$your_cannabis_use = get_field('using_the_scale_below_what_best_describes_your_cannabis_use',$post_id2);
			}
	
			$info = '<div class="col-md-6">';
				$info .= '<div id="diagnosis" class="box"><h2>Cannabis</h2>';
					if($your_cannabis_use && $your_cannabis_use != "No"){
						$info .='<div class="gfve-entry-field">';
						$info .='<span class="gfve-field-label">What best describes your cannabis use?</span>';
						$info .='<div class="rowval">'. $your_cannabis_use . '</div>';
						$info .='</div>';
					}
					if($objective_with_cannabis){
						$lst2 = '';
						if($objective_with_cannabis[0] != ''){
							$lst2 .= '<ul>';
							foreach ($objective_with_cannabis as $value) {
								$lst2 .= '<li>'.$value.'</li>';
							}
							$lst2 .= '</ul>';
						}
						$info .='<div class="gfve-entry-field">';
						$info .='<span class="gfve-field-label">What is your objective with cannabis?</span>';
						$info .='<div class="rowval">'.$lst2. '</div>';
						$info .='</div>';
					}
				$info .='</div>';
			$info .= '</div>';
			$html1 .= $info;
	
			$list_any_past_surgeries = get_field('list_any_past_surgeries_and_treatments:',$post_id);
			if(empty($list_any_past_surgeries)){
				$list_any_past_surgeries = get_field('list_any_past_surgeries_and_treatments:',$post_id2);
			}
	
			$info = '<div class="col-md-6">';
				$info .= '<div id="treatment" class="box"><h2>Treatment History</h2>';
					if(sizeof($list_any_past_surgeries) > 0 ){
						if($list_any_past_surgeries){
							$info .='<div class="gfve-entry-field">';
							$info .='<span class="gfve-field-label">List any past surgeries and treatments:</span>';
							$info .='<table>';
							$info .='<thead>';
								$info .='<tr>';
									$info .='<th>Surgery/Treatment</th>';
									$info .='<th>Month </th>';
									$info .='<th>Year</th>';
								$info .='</tr>';
							$info .='</thead>';
							$info .='<tbody>';
							foreach($list_any_past_surgeries as $value){
								$info .='<tr>';
									$info .= '<td>'.$value['surgerytreatment'].'<td><td>'.$value['month'].'<td><td>'.$value['year'].'<td>';
								$info .='</tr>';
							}
							$info .='</tbody>';
							$info .='</table>';
							$info .='</div>';
						}
					}else{
						$info .= '<br><div id="treatment" class="box"><h6>No Treatment History</h6></div>';
					}
				$info .='</div>';    //Treatment History TAB Closed.
			$info .= '</div>';
			$html1 .= $info;
	
			$list_all_prescribed = get_field('please_list_all_prescribed_and_over-the-counter_medications_taken_regularly_or_as_needed',$post_id);
			
			//echo '<pre>'; print_r($list_all_prescribed); echo '</pre>';
			
			if(!$list_all_prescribed){
				$list_all_prescribed = get_field('please_list_all_prescribed_and_over-the-counter_medications_taken_regularly_or_as_needed',$post_id2);
			}
			
			$medication_allergies = get_field('do_you_have_any_medication_allergies',$post_id);
			$describe_medication_allergies = get_field('please_describe_medication_allergies',$post_id);
			if(!$medication_allergies){
				$medication_allergies = get_field('do_you_have_any_medication_allergies',$post_id2);
				$describe_medication_allergies = get_field('please_describe_medication_allergies',$post_id2);
			}
			
			
			$info = '<div class="col-md-6">';
				$info .= '<div id="medications" class="box"><h2>Medications & Supplements</h2>';
					if(!empty($list_all_prescribed) || !empty($medication_allergies)){
						if($list_all_prescribed){
							
							$info .='<div class="gfve-entry-field">';
							$info .='<span class="gfve-field-label">Please list ALL prescribed and over-the-counter medications taken regularly or as needed.</span>';
							$info .='<div class="rowval">';
							$info .='<table>';
							$info .='<thead>';
								$info .='<tr>';
									$info .='<th>Medication Name</th>';
									$info .='<th>Dose</th>';
									$info .='<th>Frequency</th>';
								$info .='</tr>';
							$info .='</thead>';
							$info .='<tbody>';
							
							foreach($list_all_prescribed as $value){
								$info .='<tr>';
									$info .= '<td>'.$value['medication_name'].'<td><td>'.$value['dose'].'<td><td>'.$value['frequency'].'<td>';
								$info .='</tr>';
							}
							$info .='</tbody>';
							$info .='</table>';
							$info .='</div>';
							$info .='</div>';
						}
						if($medication_allergies){
							$info .='<div class="gfve-entry-field">';
							$info .='<span class="gfve-field-label">Do you have any medication allergies?</span>';
							$info .='<div class="rowval">'. $medication_allergies . '</div>';
							$info .='</div>';
						}
						if($describe_medication_allergies){
							$info .='<div class="gfve-entry-field">';
							$info .='<span class="gfve-field-label">Please Describe</span>';
							$info .='<div class="rowval">'. $describe_medication_allergies . '</div>';
							$info .='</div>';
						}
					}else{
						$info .= '<br><div id="medications" class="box"><h6>No Medications & Supplements</h6></div>';
					}
				$info .='</div>';    //Medications & Supplements TAB Closed.
			$info .= '</div>';
			$html1 .= $info;
		$html1 .= '</div>
	</div><br><br>';
	return $html1;
}
add_shortcode( 'patient-intake-short-details', 'patient_intake_short_detail' );

?>