<?php

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly 

function wpd_show_patients_list() {
	
	$userrole = get_user_role();
    $hide_personal_info = true;
	if($userrole == 'dispensary' ){
		$hide_personal_info = null; 
	}
	$html = '<a class="button cdrmed-primary-button disdash" href="#add-patient" data-toggle="tab">Add Patient</a>
	<a class="button cdrmed-primary-button disdash import_patient_show_group0 import_patient_show_group">Import Patient</a>
	<div class="import-patient-group">
		<input type="text" class="regular-text import-patient-id" placeholder="Enter Patient ID" />
		<a class="button cdrmed-primary-button disdash import_patient_send">Import</a>
		<a class="button cdrmed-primary-button disdash import_patient_cancel">Cancel</a>
	</div>
	
	';
	 $html .= '<div id="filter-bar"> </div>';
     $html .= '<table data-toggle="table"
		data-url="'.get_admin_url().'/admin-ajax.php?action=view_all_patient_ajax_request"
		data-pagination="true"
		data-side-pagination="server"
		data-page-list="[10, 25, 50, 100, 250, 500, 1000, 5000]"
		data-search="true"
		data-height="300"

		data-show-toggle="false"
		data-classes="table table-hover stripped"
		data-striped="true"
		data-show-columns="true"
		data-id-field="id"
		data-smart-display="true"
		data-toolbar="#filter-bar"
		data-show-filter="false"
		data-mobile-responsive="true">';
      $html .= '      <thead>';
      $html .= '          <tr>';
      //$html .= '              <th data-field="ID" data-switchable="false" data-sortable="true">ID</th>';
      $html .= '              <th data-field="id" data-sortable="true" data-sorter="starsSorter" data-sort-name="_id_data">Name</th>';
      //$html .= '              <th data-field="lastname">Last Name</th>';
      $html .= '              <th data-field="email" data-sortable="true">Email</th>';
	  if( $hide_personal_info){
		  $html .= '              <th data-field="doc" data-sortable="true" data-sort-name="_doc_data" data-sorter="docSorter" data-sort-order="desc">Documents</th>';
	  }
	  $html .= '              <th data-field="date" data-sortable="true" data-sort-name="_date_data" data-sorter="monthSorter">Cannabis Dosing</th>';
	  if( $hide_personal_info){
	  $html .= '              <th data-field="notes">Patient Notes</th>';
	  }
	  $html .= '              <th data-field="action" class="patient-action-th"><i class="fa fa-info"></i></th>';
	  /*$html .= '              <th>Pre Registration</th>';*/
      $html .= '          </tr>';
      $html .= '      </thead>';
      
	$html .= '</table>';
	
	return $html;

}

add_shortcode( 'cdrmed_patients_list', 'wpd_show_patients_list' );

// Get Latest Document Date
function gh_get_latest_doc($user_id){
	global $wpdb;
	$lastrowId=$wpdb->get_col( "SELECT ID FROM wp_posts where post_type='document' AND post_author={$user_id} ORDER BY post_date DESC" );
	if(sizeof($lastrowId) > 0)
		$lastPropertyId=$lastrowId[0];
	else
		$lastPropertyId = '';
	return $lastPropertyId;
}

//Get Latest SHOOP Date
function gh_get_latest_shoop($user_id){
	global $wpdb;
	$lastrowId=$wpdb->get_col( "SELECT ID FROM wp_posts where post_author={$user_id} AND post_type='strain_shoops' ORDER BY post_date DESC" );
	
	if(sizeof($lastrowId) > 0)
		$lastPropertyId=$lastrowId[0];
	else
		$lastPropertyId = '';
	return $lastPropertyId;
}

function pid() {
	global $current_screen;
	$pid = isset($_GET['pid'])? $_GET['pid'] : '';
	if(is_page('view-complete-intake')){
	$deathdate = get_user_meta( $pid, 'patient_death_date', true );
    ?>
    <script type="text/javascript">
	var dtdate;
    var dtdate = '<?php echo $deathdate; ?>';
    </script>
    <?php

	    } else {
	 ?>
    <script type="text/javascript">
	var dtdate;
    </script>
    <?php	
			
		}
} 
add_action('wp_head','pid');

?>