<?php 
if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly 
//set_time_limit(0);
//error_reporting(E_ALL);
////ini_set('display_errors', 0);
if( !wp_next_scheduled( 'update_all_dob' ) ) {
   wp_schedule_event( time(), 'daily', 'update_all_dob' );
}

add_action( 'wp_ajax_update_dob_of_patients','update_dob_of_patients');
add_action( 'wp_ajax_nopriv_update_dob_of_patients','update_dob_of_patients');
//add_action( 'update_all_dob', 'update_dob_of_patients' );

function update_dob_of_patients() {
	global $wpdb,$post;
	$site_url = get_site_url();
	$n= 0;
	//echo "Started Running Function";

	$query = "SELECT ID FROM wp_posts
			WHERE post_type = 'intake'";
 
	$posts_array = $wpdb->get_results($query, ARRAY_A);
	//echo "<br>Posts Array Fetched<br>";
	//echo "<br>This SHould Be Query Object:<br><pre>";print_r($posts_array);echo "</pre>";
	if($posts_array) {
		foreach($posts_array as $key => $value) {
			// Get Each Post ID
			$post_id = $value['ID'];	
			$already_exist = get_post_meta( $post_id, 'patient_calculated_age', true );
			if($already_exist) {
				echo '<br>Date of Birth # '.$n.' = '.$dob.' <strong>Already Exists</strong>';
				// Do Nothing....
			} else {
				$dob = get_post_meta( $post_id, 'date_of_birth', true );
				echo '<br>Date of Birth # '.$n.' = '.$dob;
				$age = ageCalculator($dob);
				update_post_meta($post_id, 'patient_calculated_age', $age);

				//
				//echo '<br>Age of Entry # '.$n.' = '.$age;
				//
				/*$last_name = get_post_meta( $post_id, 'last_name', true );
				$post_author = get_post_field( 'post_author', $post_id );
				
				$user_info = get_userdata($post_author);
				$ul_name = $user_info->last_name;

				if($ul_name) {
					// Do Nothing If Last Name Exists Already
				} else {
					wp_update_user( array( "ID" => $post_author, "last_name" => $last_name) );
				}*/
				$n++;
			}
		//echo "<br>Iteration is made: ".$n;
		} // For Each Loop
		unset($value);
	}
	if($n > 0) {
		$email = "genius.hassan@gmail.com";
		$to = $email;
		$headers = 'From: Customer Support <support@medncures.com>' . "\r\n";
		 $headers .= "Content-type: text/html\r\n";
		$subject = 'Date Of Birth Has Been Updated...';
		$body = 'Dear Hassan
		<br>
		<br>
		It is to report that Date Of Birth of All Patients in the system has been added to Meta as Well. Everything should be working fine.
		<br>
		<br>
		Website Updated was: '.$site_url.'.
		<br>
		<br>
		Total Number of Updated Entries was: '.$n.'
		<br>
		<br>
		-MednCures\'s';

		// send email
		wp_mail($to, $subject, $body, $headers );
	}
	
	//echo '<strong><br>Number of Posts are: '.$n.'</strong>';
	//echo 'All Done!';
	
}

//Reference of Regex: http://screencast.com/t/lJ2yWf0Q and http://stackoverflow.com/questions/13194322/php-regex-to-check-date-is-in-yyyy-mm-dd-format
function ageCalculator($dob){
    if(!empty($dob)){
		if(preg_match("/^((((19|[2-9]\d)\d{2})\-(0[13578]|1[02])\-(0[1-9]|[12]\d|3[01]))|(((19|[2-9]\d)\d{2})\-(0[13456789]|1[012])\-(0[1-9]|[12]\d|30))|(((19|[2-9]\d)\d{2})\-02\-(0[1-9]|1\d|2[0-8]))|(((1[6-9]|[2-9]\d)(0[48]|[2468][048]|[13579][26])|((16|[2468][048]|[3579][26])00))\-02\-29))$/g", $dob) || preg_match("/^((((19|[2-9]\d)\d{2})\/(0[13578]|1[02])\/(0[1-9]|[12]\d|3[01]))|(((19|[2-9]\d)\d{2})\/(0[13456789]|1[012])\/(0[1-9]|[12]\d|30))|(((19|[2-9]\d)\d{2})\/02\/(0[1-9]|1\d|2[0-8]))|(((1[6-9]|[2-9]\d)(0[48]|[2468][048]|[13579][26])|((16|[2468][048]|[3579][26])00))\/02\/29))$/g", $dob) || preg_match("/^(((0[13578]|1[02])\-(0[1-9]|[12]\d|3[01])\-((19|[2-9]\d)\d{2}))|((0[13456789]|1[012])\-(0[1-9]|[12]\d|30)\-((19|[2-9]\d)\d{2}))|(02\-(0[1-9]|1\d|2[0-8])\-((19|[2-9]\d)\d{2}))|(02\-29\-((1[6-9]|[2-9]\d)(0[48]|[2468][048]|[13579][26])|((16|[2468][048]|[3579][26])00))))$/g", $dob) || preg_match("/^(((0[13578]|1[02])\/(0[1-9]|[12]\d|3[01])\/((19|[2-9]\d)\d{2}))|((0[13456789]|1[012])\/(0[1-9]|[12]\d|30)\/((19|[2-9]\d)\d{2}))|(02\/(0[1-9]|1\d|2[0-8])\/((19|[2-9]\d)\d{2}))|(02\/29\/((1[6-9]|[2-9]\d)(0[48]|[2468][048]|[13579][26])|((16|[2468][048]|[3579][26])00))))$/g", $dob) || preg_match("/^(((0[1-9]|[12]\d|3[01])\/(0[13578]|1[02])\/((19|[2-9]\d)\d{2}))|((0[1-9]|[12]\d|30)\/(0[13456789]|1[012])\/((19|[2-9]\d)\d{2}))|((0[1-9]|1\d|2[0-8])\/02\/((19|[2-9]\d)\d{2}))|(29\/02\/((1[6-9]|[2-9]\d)(0[48]|[2468][048]|[13579][26])|((16|[2468][048]|[3579][26])00))))$/g", $dob) || preg_match("/^(((0[1-9]|[12]\d|3[01])\-(0[13578]|1[02])\-((19|[2-9]\d)\d{2}))|((0[1-9]|[12]\d|30)\-(0[13456789]|1[012])\-((19|[2-9]\d)\d{2}))|((0[1-9]|1\d|2[0-8])\-02\-((19|[2-9]\d)\d{2}))|(29\-02\-((1[6-9]|[2-9]\d)(0[48]|[2468][048]|[13579][26])|((16|[2468][048]|[3579][26])00))))$/g", $dob) || preg_match("/\d{4}\-\d{2}-\d{2}/", $dob) ) {
			$birthdate = new DateTime($dob);
			$today   = new DateTime('today');
			$age = $birthdate->diff($today)->y;
			return $age;
		}
    }else{
        return 0;
    }
}

?>
