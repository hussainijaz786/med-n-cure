<?php 

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly 
// Add Shortcode
function cdrmed_search_advanced_function_gen() {
	
	ob_start();
	
	$search = new WP_Advanced_Search('my-form');

	$search->the_form();

	$advsearch = ob_get_contents();
  	ob_end_clean();
  	return $advsearch;

}

add_shortcode( 'cdrmed-adv-search', 'cdrmed_search_advanced_function_gen' );

// Add Shortcode for Show and Hide Filter
function cdrmed_search_advanced_fields_function() {
	
	$return_data = '';
	/* $userrole = get_user_role();
	if(	$userrole == 'physician' || $userrole == 'caregiver' || $userrole == 'administrator' ) {
		$return_data .= do_shortcode('[physicians-tab-nav]');
	}
	else if($userrole == 'dispensary'){
		$return_data .= do_shortcode('[dispensar-tab-nav]');
	} */
	
	$return_data .= '<div class="cdrmed-filter-selection-class">
		<h4>Select search fields</h4>
		<div class="search-fileds">
		<span class="advance_search_filter active" data-selected="selected" data-filter_id="wpas-meta_occupations">Occuptions</span>
		<span class="advance_search_filter active" data-selected="selected" data-filter_id="wpas-meta_highest_level_of_education">Highest Level of Education</span>
		<span class="advance_search_filter active" data-selected="selected" data-filter_id="wpas-meta_city">City</span>
		<span class="advance_search_filter active" data-selected="selected" data-filter_id="wpas-meta_state">State</span>
		<span class="advance_search_filter active" data-selected="selected" data-filter_id="wpas-meta_zip_code">Zip</span>
		<span class="advance_search_filter" data-selected="" data-filter_id="wpas-meta_country">Country</span>
		<span class="advance_search_filter" data-selected="" data-filter_id="wpas-meta_gender">Gender</span>
		<span class="advance_search_filter" data-selected="" data-filter_id="wpas-meta_patient_calculated_age1">Age From</span>
		<span class="advance_search_filter" data-selected="" data-filter_id="wpas-meta_patient_calculated_age2">Age To</span>
		<span class="advance_search_filter" data-selected="" data-filter_id="wpas-meta_ethnic_heritage">Ethnic Heritage</span>
		<span class="advance_search_filter" data-selected="" data-filter_id="wpas-meta_blood_type">Blood Type</span>
		<span class="advance_search_filter" data-selected="" data-filter_id="wpas-meta_principal_primary_diagnosis">Principle Primary Diagnosis</span>
		<span class="advance_search_filter active" data-selected="selected" data-filter_id="wpas-meta_icd-9_or_icd-10_code_if_known">ICD-9 or ICD-10 Code</span>
		<span class="advance_search_filter" data-selected="" data-filter_id="wpas-meta_stage_cancer">Stage of Cancer</span>
		<span class="advance_search_filter" data-selected="" data-filter_id="wpas-meta_other_health_concerns">Health Concern</span>
		<span class="advance_search_filter" data-selected="" data-filter_id="wpas-meta_medication_name">Medication Name</span>
		</div>
	</div>';

return $return_data;

}

add_shortcode( 'cdrmed_search_advanced_fields', 'cdrmed_search_advanced_fields_function' );

?>
