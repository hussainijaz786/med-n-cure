<?php 

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly 

// Add Shortcode
function geneate_patient_data() {
	
	global $wpdb;
	
	$query = "SELECT DISTINCT user_id FROM {$wpdb->prefix}usermeta
				WHERE `meta_value` LIKE '%subscriber%' or  `meta_value` LIKE '%patient%'
				ORDER BY user_id DESC
				";
				
	$getpatients = $wpdb->get_results($query);
	
	 $html = '<div id="genius-bar" style="display: block !important;"> </div>';
	 $html .= '<style>.columns.columns-right.btn-group.pull-right{display: block !important;}</style>';
     $html .= '<table data-toggle="table" data-show-toggle="false" data-classes="table table-hover stripped" data-striped="true" data-show-columns="true" data-id-field="id" data-pagination="true" data-search="true" data-page-size="10" data-show-export="true" data-page-list="[25, 50, 100, 250, 500, 1000, 5000]" data-smart-display="true" data-toolbar="#genius-bar" data-show-filter="true" data-mobile-responsive="true">';
      $html .= '      <thead>';
      $html .= '          <tr>';
      $html .= '              <th>First Name</th>';
      $html .= '              <th>Last Name</th>';
      $html .= '              <th>Email</th>';
	  $html .= '              <th>Phone</th>';
	  $html .= '              <th>SHOOP Exists</th>';
	  $html .= '              <th>Intake Exists</th>';
	  $html .= '              <th>Patient Info</th>';
	  $html .= '              <th>Contacts Info</th>';
	  $html .= '              <th>Diagnosis</th>';
	  $html .= '              <th>Treatment</th>';
	  $html .= '              <th>Medications</th>';
      $html .= '          </tr>';
      $html .= '      </thead>';
      $html .= '      <tbody>';
				
	foreach ($getpatients as $key => $value) {
	$patient_id = $value->user_id;
	$user_info = get_userdata($patient_id);
	$first_name = $user_info->first_name; 
	$last_name = $user_info->last_name; 
	$email = $user_info->user_email;
	$intake_id = get_user_meta($patient_id, 'patient_intake', true );
	$phone = get_post_meta($intake_id, 'cell_phone', true );

	
	$html .= '<tr>';
		$html .= '<td>'.$first_name.'</td>';
		$html .= '<td>'.$last_name.'</td>';
		$html .= '<td>'.$email.'</td>';
		if($phone) {
		$html .= '<td>'.$phone.'</td>';
		} else {
		$html .= '<td>N/A</td>';	
		}
		$html .= '<td>'.check_patient_shoops($patient_id).'</td>';	// Check SHOOP Existance
		if($intake_id) {
			$html .= '<td>Yes</td>';
		} else {
			$html .= '<td>No Intake</td>';
		}
		if($intake_id) {				// Check Patient Information Such as First and Last Name
			$html .= '<td>'.check_patient_det($intake_id).'</td>';
		} else {
			$html .= '<td>N/A</td>';
		}
		if($intake_id) {				// Check Emergency Contacts Such as Physician and Contact Name
			$html .= '<td>'.check_contacts_det($intake_id).'</td>';
		} else {
			$html .= '<td>N/A</td>';
		}
		if($intake_id) {				// Check Diagnosis Info
			$html .= '<td>'.check_diagnosis_det($intake_id).'</td>';
		} else {
			$html .= '<td>N/A</td>';
		}
		if($intake_id) {				// Check Treatment Info
			$html .= '<td>'.check_treatment_det($intake_id).'</td>';
		} else {
			$html .= '<td>N/A</td>';
		}
		if($intake_id) {				// Check Medication Info
			$html .= '<td>'.check_medications_det($intake_id).'</td>';
		} else {
			$html .= '<td>N/A</td>';
		}
	$html .= '</tr>';
	}
	
	$html .= '</tbody>';
	$html .= '</table>';
	
	return $html;

}
add_shortcode( 'generate-patients-data', 'geneate_patient_data' );


function check_patient_shoops($user_id) {
	global $wpdb;
	$lastrowId=$wpdb->get_col( "SELECT ID FROM wp_posts where post_author={$user_id} AND post_type='strain_shoops' ORDER BY post_date DESC" );
	if($lastrowId) {
		$genius = 'SHOOP Exists';
	} else {
		$genius = 'NO SHOOP';
	}
	return $genius;
}

function check_patient_det($intake_id) {
	$post_id = $intake_id;
	$first_name = get_field('first_name',$post_id);
	$last_name = get_field('last_name',$post_id);
	$occupations = get_field('occupations',$post_id);
	$highest_level_education = get_field('highest_level_of_education',$post_id);
	$doctrate_type = get_field('select_doctrate_type',$post_id);
	$street_address = get_field('street_address',$post_id);
	
	if(!empty($first_name) && !empty($last_name)) {
		$genius = 'Exists';
	} else {
		$genius = 'Not Exists';
	}
	return $genius;
}
function check_contacts_det($intake_id) {
	$post_id = $intake_id;
	$emergency_contact_firstname = get_field('emergency_contact_firstname',$post_id);
	$emergency_contact_lastname = get_field('emergency_contact_lastname',$post_id);
	$emergency_contact_phone = get_field('emergency_contact_phone',$post_id);
	$emergency_contact_relationship = get_field('emergency_contact_relationship',$post_id);
	$physician_contact_firstname = get_field('physician_contact_firstname',$post_id);
	$physician_contact_lastname = get_field('physician_contact_lastname',$post_id);
	if(!empty($emergency_contact_firstname) && !empty($emergency_contact_lastname) || !empty($physician_contact_firstname) && !empty($physician_contact_lastname)  ) {
		$genius = 'Exists';
	} else {
		$genius = 'Not Exists';
	}
	return $genius;
}
function check_diagnosis_det($intake_id) {
	$post_id = $intake_id;
	$principal_primary_diagnosis = get_field('principal_primary_diagnosis',$post_id);
	if(!empty($principal_primary_diagnosis)) {
		$genius = 'Exists';
	} else {
		$genius = 'Not Exists';
	}
	return $genius;
}
function check_treatment_det($intake_id) {
	$post_id = $intake_id;
	$list_any_past_surgeries = get_field('list_any_past_surgeries_and_treatments:',$post_id);
	foreach($list_any_past_surgeries as $value){
		$checktreatment = $value['surgerytreatment'];
	}
	if(!empty($checktreatment)) {
		$genius = 'Exists';
	} else {
		$genius = 'Not Exists';
	}
	return $genius;
}
function check_medications_det($intake_id) {
	$post_id = $intake_id;
	$list_all_prescribed = get_field('please_list_all_prescribed_and_over-the-counter_medications_taken_regularly_or_as_needed',$post_id);
	foreach($list_all_prescribed as $value){
		$medications = $value['medication_name'];
	}
	if(!empty($medications)) {
		$genius = 'Exists';
	} else {
		$genius = 'Not Exists';
	}
	return $genius;
}
function check_family_det($intake_id) {
	
}
function check_patient_previous_det($intake_id) {
	
}
function check_cannabis_det($intake_id) {
	
}
function check_nutrition_det($intake_id) {
	
}
function check_military_det($intake_id) {
	
}
function check_comments_det($intake_id) {
	
}
?>