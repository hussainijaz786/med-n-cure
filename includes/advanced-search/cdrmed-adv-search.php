<?php 

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly 

function cdrmed_adv_search_form() {
	
	// error_reporting(E_ALL);
	// //ini_set('display_errors', 0);
	
    $args = array();
	
	$current_usid = get_current_user_id();
	$current_usid = is_dispensary_physician_child($current_usid);
	$siteurl = site_url();
	$userrole = get_user_role();
	/* $search_key = 'patient_physician_id';
	if( $userrole == 'dispensary'){
		$search_key = 'patient_dispensary_id';
	} */
	
	/**
	* Error was found in wp_query argument where code wasn't executing on local system and when disabled code worked flawlessly.
	* Dated: 18/06/2016
	* Author: Hassan
	**/
	
	// How many Posts Per Page?
  	$args['wp_query'] = array('post_type' => 'intake',
							  'posts_per_page' => '100',
							  'order' => 'DESC');
						  
	// Here is where we specify the page where results will be shown
	$args['form'] = array( 'action' => get_bloginfo('url') . '/advanced-search-results' );
	
	/* $args['form'] = array( 'auto_submit' => true );
	
	$args['form']['ajax'] = array( 'enabled' => true,
                                   'show_default_results' => true,
                                   'results_template' => get_bloginfo('url') . '/advanced-search-results', // This file must exist in your theme root
                                   'button_text' => 'Load More Results'); */
	
	// Search Type
	$args['fields'][] = array(	'label' => 'Search Type',
								'format' => 'select',
								'type' => 'generic',
								'values' => array (
										'' => 'All Patients',
										$current_usid  => 'My Patients',
									),
								'class' => 'col-3m'
								);
	
	// First Meta Key Type Field
	$args['fields'][] = array(	'label' => 'Occupation',
								'format' => 'select',
								'compare' => 'LIKE',
								'meta_key' => 'occupations',
								'data_type' => 'ARRAY<CHAR>',
								'type' => 'meta_key',
								'values' => array (
										'0' => '--Select--',
										'Accounting/Finance' => 'Accounting/Finance',
										'Advertising/Public Relations' => 'Advertising/Public Relations',
										'Aerospace/Aviation' => 'Aerospace/Aviation',
										'Arts/Entertainment/Publishing' => 'Arts/Entertainment/Publishing',
										'Automotive' => 'Automotive',
										'Banking/Mortgage' => 'Banking/Mortgage',
										'Business Development' => 'Business Development',
										'Business Opportunity' => 'Business Opportunity',
										'Clerical/Administrative' => 'Clerical/Administrative',
										'Construction/Facilities' => 'Construction/Facilities',
										'Consumer Goods' => 'Consumer Goods',
										'Customer Service' => 'Customer Service',
										'Education/Training' => 'Education/Training',
										'Energy/Utilities' => 'Energy/Utilities',
										'Engineering' => 'Engineering',
										'Government/Military' => 'Government/Military',
										'Green' => 'Green',
										'Healthcare' => 'Healthcare',
										'Hospitality/Travel' => 'Hospitality/Travel',
										'Human Resources' => 'Human Resources',
										'Installation/Maintenance' => 'Installation/Maintenance',
										'Insurance' => 'Insurance',
										'Internet' => 'Internet',
										'Job Search Aids' => 'Job Search Aids',
										'Law Enforcement/Security' => 'Law Enforcement/Security',
										'Legal' => 'Legal',
										'Management/Executive' => 'Management/Executive',
										'Manufacturing/Operations' => 'Manufacturing/Operations',
										'Marketing' => 'Marketing',
										'Non-Profit/Volunteer' => 'Non-Profit/Volunteer',
										'Pharmaceutical/Biotech' => 'Pharmaceutical/Biotech',
										'Professional Services' => 'Professional Services',
										'QA/Quality Control' => 'QA/Quality Control',
										'Real Estate' => 'Real Estate',
										'Restaurant/Food Service' => 'Restaurant/Food Service',
										'Retail' => 'Retail',
										'Sales' => 'Sales',
										'Science/Research' => 'Science/Research',
										'Skilled Labor' => 'Skilled Labor',
										'Technology' => 'Technology',
										'Telecommunications' => 'Telecommunications',
										'Transportation/Logistics' => 'Transportation/Logistics',
										'Other' => 'Other',
									),
								'class' => 'col-3m'
								);
	
	// Highest Level of Education
	$args['fields'][] = array(	'label' => 'Highest Level of Education',
								'format' => 'select',
								'compare' => 'LIKE',
								'meta_key' => 'highest_level_of_education',
								'data_type' => 'ARRAY<CHAR>',
								'type' => 'meta_key',
								'values' => array (
										'0' => '--Select--',
										'Haven\'t graduated high school' => 'Haven\'t graduated high school',
										'GED' => 'GED',
										'High school graduate' => 'High school graduate',
										'First year of college' => 'First year of college',
										'Third year of college' => 'Third year of college',
										'Fourth year of college' => 'Fourth year of college',
										'Bachelors' => 'Bachelors',
										'Masters' => 'Masters',
										'Doctorate' => 'Doctorate',
									),
								'class' => 'col-3m'
								);	
								
	// City
	$args['fields'][] = array(	'label' => 'City',
								'placeholder' => 'Enter Name of City...',
								'format' => 'text',
								'compare' => 'LIKE',
								'meta_key' => 'city',
								'data_type' => 'ARRAY<CHAR>',
								'type' => 'meta_key',
								'class' => 'col-3m'
								);
								
	// State
	$args['fields'][] = array(	'label' => 'State',
								'placeholder' => 'Enter Name of State...',
								'format' => 'text',
								'compare' => 'LIKE',
								'meta_key' => 'state',
								'data_type' => 'ARRAY<CHAR>',
								'type' => 'meta_key',
								'class' => 'col-3m'
								);													  
	// Zip
	$args['fields'][] = array(	'label' => 'Zip',
								'placeholder' => 'Enter Zip Code...',
								'format' => 'text',
								'compare' => 'LIKE',
								'meta_key' => 'zip_code',
								'data_type' => 'ARRAY<CHAR>',
								'type' => 'meta_key',
								'class' => 'col-3m'
								);	
								
	// Country
	$args['fields'][] = array(	'label' => 'Country',
								'format' => 'select',
								'compare' => 'LIKE',
								'meta_key' => 'country',
								'data_type' => 'ARRAY<CHAR>',
								'type' => 'meta_key',
								'values' => array (
										'0' => '--Select--',
										'Abkhazia' => 'Abkhazia',
										'Afghanistan' => 'Afghanistan',
										'Akrotiri and Dhekelia' => 'Akrotiri and Dhekelia',
										'Aland' => 'Aland',
										'Albania' => 'Albania',
										'Algeria' => 'Algeria',
										'American Samoa' => 'American Samoa',
										'Andorra' => 'Andorra',
										'Angola' => 'Angola',
										'Anguilla' => 'Anguilla',
										'Antigua and Barbuda' => 'Antigua and Barbuda',
										'Argentina' => 'Argentina',
										'Armenia' => 'Armenia',
										'Aruba' => 'Aruba',
										'Ascension Island' => 'Ascension Island',
										'Australia' => 'Australia',
										'Austria' => 'Austria',
										'Azerbaijan' => 'Azerbaijan',
										'Bahamas, The' => 'Bahamas, The',
										'Bahrain' => 'Bahrain',
										'Bangladesh' => 'Bangladesh',
										'Barbados' => 'Barbados',
										'Belarus' => 'Belarus',
										'Belgium' => 'Belgium',
										'Belize' => 'Belize',
										'Benin' => 'Benin',
										'Bermuda' => 'Bermuda',
										'Bhutan' => 'Bhutan',
										'Bolivia' => 'Bolivia',
										'Bosnia and Herzegovina' => 'Bosnia and Herzegovina',
										'Botswana' => 'Botswana',
										'Brazil' => 'Brazil',
										'Brunei' => 'Brunei',
										'Bulgaria' => 'Bulgaria',
										'Burkina Faso' => 'Burkina Faso',
										'Burundi' => 'Burundi',
										'Cambodia' => 'Cambodia',
										'Cameroon' => 'Cameroon',
										'Canada' => 'Canada',
										'Cape Verde' => 'Cape Verde',
										'Cayman Islands' => 'Cayman Islands',
										'Central Africa Republic' => 'Central Africa Republic',
										'Chad' => 'Chad',
										'Chile' => 'Chile',
										'China' => 'China',
										'Christmas Island' => 'Christmas Island',
										'Cocos (Keeling) Islands' => 'Cocos (Keeling) Islands',
										'Colombia' => 'Colombia',
										'Comoros' => 'Comoros',
										'Congo' => 'Congo',
										'Cook Islands' => 'Cook Islands',
										'Costa Rica' => 'Costa Rica',
										'Cote d\'lvoire' => 'Cote d\'lvoire',
										'Croatia' => 'Croatia',
										'Cuba' => 'Cuba',
										'Cyprus' => 'Cyprus',
										'Czech Republic' => 'Czech Republic',
										'Denmark' => 'Denmark',
										'Djibouti' => 'Djibouti',
										'Dominica' => 'Dominica',
										'Dominican Republic' => 'Dominican Republic',
										'East Timor Ecuador' => 'East Timor Ecuador',
										'Egypt' => 'Egypt',
										'El Salvador' => 'El Salvador',
										'Equatorial Guinea' => 'Equatorial Guinea',
										'Eritrea' => 'Eritrea',
										'Estonia' => 'Estonia',
										'Ethiopia' => 'Ethiopia',
										'Falkland Islands' => 'Falkland Islands',
										'Faroe Islands' => 'Faroe Islands',
										'Fiji' => 'Fiji',
										'Finland' => 'Finland',
										'France' => 'France',
										'French Polynesia' => 'French Polynesia',
										'Gabon' => 'Gabon',
										'Cambia, The' => 'Cambia, The',
										'Georgia' => 'Georgia',
										'Germany' => 'Germany',
										'Ghana' => 'Ghana',
										'Gibraltar' => 'Gibraltar',
										'Greece' => 'Greece',
										'Greenland' => 'Greenland',
										'Grenada' => 'Grenada',
										'Guam' => 'Guam',
										'Guatemala' => 'Guatemala',
										'Guemsey' => 'Guemsey',
										'Guinea' => 'Guinea',
										'Guinea-Bissau' => 'Guinea-Bissau',
										'Guyana' => 'Guyana',
										'Haiti' => 'Haiti',
										'Honduras' => 'Honduras',
										'Hong Kong' => 'Hong Kong',
										'Hungary' => 'Hungary',
										'Iceland' => 'Iceland',
										'India' => 'India',
										'Indonesia' => 'Indonesia',
										'Iran' => 'Iran',
										'Iraq' => 'Iraq',
										'Ireland' => 'Ireland',
										'Isle of Man' => 'Isle of Man',
										'Israel' => 'Israel',
										'Italy' => 'Italy',
										'Jamaica' => 'Jamaica',
										'Japan' => 'Japan',
										'Jersey' => 'Jersey',
										'Jordan' => 'Jordan',
										'Kazakhstan' => 'Kazakhstan',
										'Kenya' => 'Kenya',
										'Kiribati' => 'Kiribati',
										'Korea, N' => 'Korea, N',
										'Korea, S' => 'Korea, S',
										'Kosovo' => 'Kosovo',
										'Kuwait' => 'Kuwait',
										'Kyrgyzstan' => 'Kyrgyzstan',
										'Laos' => 'Laos',
										'Latvia' => 'Latvia',
										'Lebanon' => 'Lebanon',
										'Lesotho' => 'Lesotho',
										'Liberia' => 'Liberia',
										'Libya' => 'Libya',
										'Liechtenstein' => 'Liechtenstein',
										'Lithuania' => 'Lithuania',
										'Luxembourg' => 'Luxembourg',
										'Macao' => 'Macao',
										'Macedonia' => 'Macedonia',
										'Madagascar' => 'Madagascar',
										'Malawi' => 'Malawi',
										'Malaysia' => 'Malaysia',
										'Maldives' => 'Maldives',
										'Mali' => 'Mali',
										'Malta' => 'Malta',
										'Marshall Islands' => 'Marshall Islands',
										'Mauritania' => 'Mauritania',
										'Mauritius' => 'Mauritius',
										'Mayotte' => 'Mayotte',
										'Mexico' => 'Mexico',
										'Micronesia' => 'Micronesia',
										'Moldova' => 'Moldova',
										'Monaco' => 'Monaco',
										'Mongolia' => 'Mongolia',
										'Montenegro' => 'Montenegro',
										'Montserrat' => 'Montserrat',
										'Morocco' => 'Morocco',
										'Mozambique' => 'Mozambique',
										'Myanmar' => 'Myanmar',
										'Nagorno-Karabakh' => 'Nagorno-Karabakh',
										'Namibia' => 'Namibia',
										'Nauru' => 'Nauru',
										'Nepal' => 'Nepal',
										'Netherlands' => 'Netherlands',
										'Netherlands Antilles' => 'Netherlands Antilles',
										'New Caledonia' => 'New Caledonia',
										'New Zealand' => 'New Zealand',
										'Nicaragua' => 'Nicaragua',
										'Niger' => 'Niger',
										'Nigeria' => 'Nigeria',
										'Niue' => 'Niue',
										'Norfolk Island' => 'Norfolk Island',
										'Northern Cyprus' => 'Northern Cyprus',
										'Northern Mariana Islands' => 'Northern Mariana Islands',
										'Norway' => 'Norway',
										'Oman' => 'Oman',
										'Pakistan' => 'Pakistan',
										'Palau' => 'Palau',
										'Palestine' => 'Palestine',
										'Panama' => 'Panama',
										'Papua New Guinea' => 'Papua New Guinea',
										'Paraguay' => 'Paraguay',
										'Peru' => 'Peru',
										'Philippines' => 'Philippines',
										'Pitcaim Islands' => 'Pitcaim Islands',
										'Poland' => 'Poland',
										'Portugal' => 'Portugal',
										'Puerto Rico' => 'Puerto Rico',
										'Qatar' => 'Qatar',
										'Romania' => 'Romania',
										'Russia' => 'Russia',
										'Rwanda' => 'Rwanda',
										'Sahrawi Arab Democratic Republic' => 'Sahrawi Arab Democratic Republic',
										'Saint-Barthelemy' => 'Saint-Barthelemy',
										'Saint Helena' => 'Saint Helena',
										'Saint Kitts and Nevis' => 'Saint Kitts and Nevis',
										'Saint Lucia' => 'Saint Lucia',
										'Saint Martin' => 'Saint Martin',
										'Saint Pierre and Miquelon' => 'Saint Pierre and Miquelon',
										'Saint Vincent and Grenadines' => 'Saint Vincent and Grenadines',
										'Samos' => 'Samos',
										'San Marino' => 'San Marino',
										'Sao Tome and Principe' => 'Sao Tome and Principe',
										'Saudi Arabia' => 'Saudi Arabia',
										'Senegal' => 'Senegal',
										'Serbia' => 'Serbia',
										'Seychelles' => 'Seychelles',
										'Sierra Leone' => 'Sierra Leone',
										'Singapore' => 'Singapore',
										'Slovakia' => 'Slovakia',
										'Slovenia' => 'Slovenia',
										'Solomon Islands' => 'Solomon Islands',
										'Somalia' => 'Somalia',
										'Somaliland' => 'Somaliland',
										'South Africa' => 'South Africa',
										'South Ossetia' => 'South Ossetia',
										'Spain' => 'Spain',
										'Sri Lanka' => 'Sri Lanka',
										'Sudan' => 'Sudan',
										'Suriname' => 'Suriname',
										'Svalbard' => 'Svalbard',
										'Swaziland' => 'Swaziland',
										'Sweden' => 'Sweden',
										'Switzerland' => 'Switzerland',
										'Syria' => 'Syria',
										'Tajikistan' => 'Tajikistan',
										'Tanzania' => 'Tanzania',
										'Thailand' => 'Thailand',
										'Togo' => 'Togo',
										'Tokelau' => 'Tokelau',
										'Tonga' => 'Tonga',
										'Transnistria' => 'Transnistria',
										'Trinidad and Tobago' => 'Trinidad and Tobago',
										'Tristan da Cunha' => 'Tristan da Cunha',
										'Tunisia' => 'Tunisia',
										'Turkey' => 'Turkey',
										'Turkmenistan' => 'Turkmenistan',
										'Turks and Caicos Islands' => 'Turks and Caicos Islands',
										'Tuvalu' => 'Tuvalu',
										'Uganda' => 'Uganda',
										'Ukraine' => 'Ukraine',
										'United Arab Emirates' => 'United Arab Emirates',
										'United Kingdom' => 'United Kingdom',
										'United States' => 'United States',
										'Uruguay' => 'Uruguay',
										'Uzbekistan' => 'Uzbekistan',
										'Vanuatu' => 'Vanuatu',
										'Vatican City' => 'Vatican City',
										'Venezuela' => 'Venezuela',
										'Vietnam' => 'Vietnam',
										'Virgin Islands, British' => 'Virgin Islands, British',
										'Virgin Islands, U.S.' => 'Virgin Islands, U.S.',
										'Wallis and Futuna' => 'Wallis and Futuna',
										'Yemen' => 'Yemen',
										'Zambia' => 'Zambia',
										'Zimbabwe' => 'Zimbabwe',
									),
								'class' => 'col-3m'
								);	
								
	// Gender
	$args['fields'][] = array(	'label' => 'Gender',
								'format' => 'select',
								'compare' => 'LIKE',
								'meta_key' => 'gender',
								'data_type' => 'ARRAY<CHAR>',
								'type' => 'meta_key',
								'values' => array (
										'0' => '--Select--',
										'Male' => 'Male',
										'Female' => 'Female',
									),
								'class' => 'col-3m'
								);	
															
	// Age
	$args['fields'][] = array(	//'label' => 'Age',
								'type' => 'meta_key',
								'meta_key' => 'patient_calculated_age',
								'compare' => 'BETWEEN',
								'data_type' => 'NUMERIC',
								'inputs' => array(
									array(
										'label' => 'Age From',
										'format' => 'number'
									),
									array(
										'label' => 'Age To',
										'format' => 'number'
									)
								),
								'class' => 'col-3m smallfield'
							);	
							
	// Ethnic Heritage
	$args['fields'][] = array(	'label' => 'Ethnic Heritage',
								'format' => 'select',
								'compare' => 'LIKE',
								'meta_key' => 'ethnic_heritage',
								'data_type' => 'ARRAY<CHAR>',
								'type' => 'meta_key',
								'values' => array (
										'0' => '--Select--',
										'Non-Hispanic White or Euro-American' => 'Non-Hispanic White or Euro-American',
										'Black, Afro-Caribbean, or African American' => 'Black, Afro-Caribbean, or African American',
										'Latino or Hispanic American' => 'Latino or Hispanic American',
										'East Asian or Asian American' => 'East Asian or Asian American',
										'South Asian or Indian American' => 'South Asian or Indian American',
										'Middle Eastern or Arab American' => 'Middle Eastern or Arab American',
										'Native American or Alaskan Native' => 'Native American or Alaskan Native',
										'Other' => 'Other',
									),
								'class' => 'col-3m'
								);
								
	// Blood Type
	$args['fields'][] = array(	'label' => 'Blood Type',
								'format' => 'select',
								'compare' => 'LIKE',
								'meta_key' => 'blood_type',
								'data_type' => 'ARRAY<CHAR>',
								'type' => 'meta_key',
								'values' => array (
										'0' => '--Select--',
										'A' => 'A',
										'B' => 'B',
										'AB' => 'AB',
										'O' => 'O',
										'I don\'t know' => 'I don\'t know',
									),
								'class' => 'col-3m'
								);	
								
	// Principle Primary Diagnosis
	$args['fields'][] = array(	'label' => 'Principle Primary Diagnosis',
								'placeholder' => 'Type Primary Diagnosis',
								'format' => 'text',
								'compare' => 'LIKE',
								'meta_key' => 'principal_primary_diagnosis',
								'data_type' => 'ARRAY<CHAR>',
								'type' => 'meta_key',
								'class' => 'col-3m cannbi'
								);	
								
	// ICD-9 Or ICD-10 Code
	$args['fields'][] = array(	'label' => 'ICD-9 Or ICD-10 Code',
								'placeholder' => 'Enter ICD Code (If Known)',
								'format' => 'text',
								'compare' => 'LIKE',
								'meta_key' => 'icd-9_or_icd-10_code_if_known',
								'data_type' => 'ARRAY<CHAR>',
								'type' => 'meta_key',
								'class' => 'col-3m'
								);
								
	// Cancer Stage
	$args['fields'][] = array(	'label' => 'Stage of Cancer',
								'format' => 'select',
								'compare' => 'LIKE',
								'meta_key' => 'stage_cancer',
								'data_type' => 'ARRAY<CHAR>',
								'type' => 'meta_key',
								'values' => array (
										'0' => '--Select--',
										'Stage 0' => 'Stage 0',
										'Stage 1' => 'Stage 1',
										'Stage 2' => 'Stage 2',
										'Stage 3' => 'Stage 3',
										'Stage 4' => 'Stage 4',
									),
								'class' => 'col-3m icd'
								);		
								
	// Health Cocern
	$args['fields'][] = array(	'label' => 'Health Concern',
								'placeholder' => 'Enter Health Concern',
								'format' => 'text',
								'compare' => 'LIKE',
								'meta_key' => 'other_health_concerns',
								'data_type' => 'ARRAY<CHAR>',
								'type' => 'meta_key',
								'class' => 'col-3m'
								);							
	// Medication Name
	$args['fields'][] = array(	'label' => 'Medication Name',
								'placeholder' => 'Start Typing Medication Name',
								'format' => 'text',
								'compare' => 'LIKE',
								'meta_key' => 'medication_name',
								'data_type' => 'ARRAY<CHAR>',
								'type' => 'meta_key',
								'class' => 'col-3m medication'
								);																																																				

	// Search Button
	 $args['fields'][] = array( 'type' => 'submit',
                               'class' => 'button',
                               'value' => 'Locate Patients' );
							   
	// Set Debug to True for Initial Setup
	//$args['debug'] = 'true';
	//$args['debug_level'] = 'verbose';
	
							  
    register_wpas_form('my-form', $args);
}
add_action('init','cdrmed_adv_search_form');