<?php 

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly 

function display_current_shoop_dispensar(){
	
	$pid = !isset($_GET['pid'])?null:$_GET['pid'];
	$current_shoop = intval(get_user_meta($pid, 'latest_patient',true));
	
	if($current_shoop) {
		$post_id = $current_shoop;
	}

      $primary_dosage = get_post_meta( $post_id, 'primary_dosage', true );
      $daily_dosage = get_post_meta( $post_id, 'daily_dosage', true );
      $frequency = get_post_meta( $post_id, 'frequency', true );
      $secornday_cann = get_post_meta( $post_id, 'secornday_cann', true );   
       $medicine1_dosage_frequency = explode(",",get_post_meta( $post_id, 'medicine'.($i+1).'_dosage_frequency'.($i+1), true ));
      $medicine1_dosage_mg = explode(",",get_post_meta( $post_id, 'medicine'.($i+1).'_dosage_mg'.($i+1), true ));
      $medicine1_dosage_timing = explode(",",get_post_meta( $post_id, 'medicine'.($i+1).'_dosage_timing'.($i+1), true ));
      
	  // Variable for counting number of Dosages values in it.
	  $ki = 0;
	  
      $tbl2 ='';
      foreach ($medicine1_dosage_frequency as  $value) { 
       		$tbl2 .= '<li><b>Dosage: </b> '.$value.' '.$medicine1_dosage_mg[$ki].' '.ucwords($medicine1_dosage_timing[$ki]).'</li>';
      		$ki++;
	  }
      $terpene = get_post_meta( $post_id, 'terpene', true );

      $tbl = '';
      $ingestion_method = get_post_meta( $post_id, 'ingestion_method', true );
      $notes = get_post_meta( $post_id, 'notes', true );
   
			
	 for($i=0; $i < 4; $i++) {
	   
	  $medicine_cannabinoid_1 = get_post_meta( $post_id, 'medicine'.($i+1).'_cannabinoid_1', true );
	  $medicine1_target_dosage_1 = get_post_meta( $post_id, 'medicine'.($i+1).'_target_dosage_1', true );
	  $medicine1_frequency_1 = get_post_meta( $post_id, 'medicine'.($i+1).'_frequency_1', true );
	  $medicine1_cannabinoid_2 = get_post_meta( $post_id, 'medicine'.($i+1).'_cannabinoid_2', true );   
	  $medicine1_dosage_frequency = explode(",",get_post_meta( $post_id, 'medicine'.($i+1).'_dosage_frequency'.($i+1), true ));
	  $medicine1_dosage_mg = explode(",",get_post_meta( $post_id, 'medicine'.($i+1).'_dosage_mg'.($i+1), true ));
	  $medicine1_dosage_timing = explode(",",get_post_meta( $post_id, 'medicine'.($i+1).'_dosage_timing'.($i+1), true ));
	  $notes = get_post_meta( $post_id, 'medicine_notes'.($i+1), true ); 
      $ki = 0;

      $tbl2 ='';
       if($medicine1_dosage_frequency[0] != '' && strpos($medicine1_frequency_1, "hourly") === false){
			  foreach ($medicine1_dosage_frequency as  $value) { 
			   $tbl2 .= '<li><b>Dosage '.($ki+1).': </b> '.$value.' '.$medicine1_dosage_mg[$ki].' '.ucwords($medicine1_dosage_timing[$ki]).'</li>';
			
			  $ki++;
			  }
			  } else {
			  $tbl ='';
		} 
		$medicine1_terpene = get_post_meta( $post_id, 'medicine'.($i+1).'_terpene', true );
		$medicine1_ingestion_method = get_post_meta( $post_id, 'medicine'.($i+1).'_ingestion_method', true );
		$fdprimary_dosage = get_post_meta( $post_id, 'medicine'.($i+2).'_cannabinoid_1', true );
		  $fdingestion_method = get_post_meta( $post_id, 'medicine'.($i+2).'_ingestion_method', true );
		  if($fdprimary_dosage == '' && $fdingestion_method ==''){
			$last = 'last';
		  }else if($i == 3){
			$last = 'last';
		  }
		  if(trim($medicine1_ingestion_method) == 'Ingested (edible)'){
			 $eat = '';
		  }else{
		   $eat = '';
		  }
		if($medicine_cannabinoid_1 != '' && $medicine1_ingestion_method !=''){
		   $post = get_post($postid);
		 if($medicine_cannabinoid_1 != '' && $medicine1_ingestion_method !=''){
					 $data .= '
                        <div class="col-sm-12">
                          <div class="shoop_review-content'.($i+1).' '.$last.'">
                          <div id="shoop_review-content'.($i+1).'">
                              <section class="shoop-hd-crcl'.($i+1).'"><b>Medicine : '.($i+1).'  </b><b class="txt-right"> '.$post_id.' Date Created : '.get_the_date( "Y-m-d", $post_id ).' </b></section>
                               <ul class="shoop_list'.($i+1).'">
                                 <li><b>Target Cannabinoid: </b> '.$medicine_cannabinoid_1.'</li>';
                                 if($medicine1_terpene !=''){
                                 $data .= ' <li><b>TERPENES: </b> <ul>'. $medicine1_terpene.' </ul></li>';
                                  }
                                   $data .= ' <li><b>Target Dose: </b> '.str_replace("X","",$medicine1_target_dosage_1).'</li>
                                    <li><b>Frequency: </b> '.str_replace("X","x",$medicine1_frequency_1).'</li>
                                     ';
                                 /*if($medicine1_cannabinoid_2 !='' && $medicine1_cannabinoid_2 !='Select Cannabinoid 2...'){    
                                     $data .= '<li><b>Cannabinoid 2: </b> '.$medicine1_cannabinoid_2.'</li>';
                                    }*/
                                  $data .= $tbl2.'
                                    <li class="with-method">
                                      <p><b>Ingestion Method: </b> '.$medicine1_ingestion_method.'</p>
                                      <div class="method-text">
                                          <span>'.$medicine1_ingestion_method." ".$eat.'</span>';
                                   if($i == 0){
                                  switch ($medicine1_ingestion_method) {
                                    case 'Inhaled':
                                      $data .=' <img src="'.CDRMED.'/includes/assets/img/icon5.png" alt=""/>';
                                      break;
                                   case 'Ingested (edible)':
                                      $data .=' <img src="'.CDRMED.'/includes/assets/img/icon1.png" alt=""/>';
                                      break;
                                  case 'Sublingual':
                                      $data .=' <img src="'.CDRMED.'/includes/assets/img/icon2.png" alt=""/>';
                                      break;
                                case 'Suppository':
                                      $data .=' <img src="'.CDRMED.'/includes/assets/img/icon3.png" alt=""/>';
                                      break; 
                                case 'Topical':
                                      $data .=' <img src="'.CDRMED.'/includes/assets/img/icon4.png" alt=""/>';
                                      break;     
                                  } }else if($i == 1){
                                    switch ($medicine1_ingestion_method) {
                                    case 'Inhaled':
                                      $data .=' <img src="'.CDRMED.'/includes/assets/img/1-1.png" alt=""/>';
                                      break;
                                   case 'Ingested (edible)':
                                      $data .=' <img src="'.CDRMED.'/includes/assets/img/1-2.png" alt=""/>';
                                      break;
                                  case 'Sublingual':
                                      $data .=' <img src="'.CDRMED.'/includes/assets/img/1-3.png" alt=""/>';
                                      break;
                                case 'Suppository':
                                      $data .=' <img src="'.CDRMED.'/includes/assets/img/1-4.png" alt=""/>';
                                      break; 
                                case 'Topical':
                                      $data .=' <img src="'.CDRMED.'/includes/assets/img/1-5.png" alt=""/>';
                                      break;     
                                  }
                                }else if($i == 2){
                                    switch ($medicine1_ingestion_method) {
                                    case 'Inhaled':
                                      $data .=' <img src="'.CDRMED.'/includes/assets/img/2-1.png" alt=""/>';
                                      break;
                                   case 'Ingested (edible)':
                                      $data .=' <img src="'.CDRMED.'/includes/assets/img/2-2.png" alt=""/>';
                                      break;
                                  case 'Sublingual':
                                      $data .=' <img src="'.CDRMED.'/includes/assets/img/2-3.png" alt=""/>';
                                      break;
                                case 'Suppository':
                                      $data .=' <img src="'.CDRMED.'/includes/assets/img/2-4.png" alt=""/>';
                                      break; 
                                case 'Topical':
                                      $data .=' <img src="'.CDRMED.'/includes/assets/img/2-5.png" alt=""/>';
                                      break;     
                                  }
                                  } else if($i == 3){
                                    switch ($medicine1_ingestion_method) {
                                    case 'Inhaled':
                                      $data .=' <img src="'.CDRMED.'/includes/assets/img/3-1.png" alt=""/>';
                                      break;
                                   case 'Ingested (edible)':
                                      $data .=' <img src="'.CDRMED.'/includes/assets/img/3-2.png" alt=""/>';
                                      break;
                                  case 'Sublingual':
                                      $data .=' <img src="'.CDRMED.'/includes/assets/img/3-3.png" alt=""/>';
                                      break;
                                case 'Suppository':
                                      $data .=' <img src="'.CDRMED.'/includes/assets/img/3-4.png" alt=""/>';
                                      break; 
                                case 'Topical':
                                      $data .=' <img src="'.CDRMED.'/includes/assets/img/3-5.png" alt=""/>';
                                      break;     
                                  }
                                  } 
                                          
                                         
                                  $data .=' </div>
                                    </li>';
                                     if($notes != '') { 
                                   $data .=' <li><b>Dosage Notes:  </b> '.$notes.'</li>';
                                 }
                                 
                                $data .='</ul>
                                </div>
                            </div>
                        </div>';
                      }
                      } // if condition ends here
          }

          $data .='</div>';
  
	
	return  $data;
	
}


add_shortcode( 'display-current-shoop-dispensar', 'display_current_shoop_dispensar' );

 ?>
