<?php 

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly 
if (!session_id()) {
    session_start();
}
function current_patient_shoops(){
	global $wpdb;
	
	$userrole = get_user_role();
  
    $pid = !isset($_GET['pid'])?null:$_GET['pid'];
    $post_id = !isset($_GET['send_email'])?null:$_GET['send_email'];

    $send_email = !isset($_GET['send_email'])?null:$_GET['send_email'];
    if(isset($send_email)){

		$patient_id = get_post_meta( $post_id, 'patient_id', true );
		update_post_meta($post_id, 'sent_patient','1');
		update_user_meta($pid, 'latest_patient',$post_id);
		update_post_meta($post_id, 'send_patient_date',date("Y-m-d h:i:s"));
		//strain_send_message2($send_email,$pid);
		
		$patient_info = get_userdata( $pid );
		
		$args = array(
			'call_by' => 'shoop-alert-patient',
			'receiver' => $patient_info->user_email, 
			'subject_name' => '', 
			'body_name' => '', 
			'body_part' => '', 
			'password_reset_url' => ''
		);
		
		cdrmed_send_email($args);

		do_action('shoop_send_to_pateint', $patient_id, $post_id);
		$_SESSION["ses_id"] = "<b id='successMessage'><center>SHOOP sent Successfully.</center></b>"; 
		/* echo "<h2><center>Email sent Successfully<br/>You will be redirected shortly</center></h2>";
		header('refresh:5;url='.$urlofpage.'?pid='.$_GET['pid']);
		exit;*/
    }
	
	$main_data1 = '';
	$current_shoop = ''; 
	$latest_shoop_id = get_user_meta($pid, 'latest_patient', true);
	
	//$data .= '<div class="col-md-12 single-patient-shoops">';
	$args = array(
		'posts_per_page'   => '',
		'offset'           => 0,
		'category'         => '',
		'category_name'    => '',
		'orderby'          => 'ID',
		'order'            => 'DESC',
		'include'          => '',
		'exclude'          => '',
		'meta_key'         => '',
		'meta_value'       => '',
		'post_type'        => 'strain_shoops',
		'post_mime_type'   => '',
		'post_parent'      => '',
		'author'           => $pid,
		'post_status'      => 'private',
		'suppress_filters' => true 
	);
	$posts_array = get_posts( $args );
	$posts = array();
	$postlist = get_posts( 'post_type=strain_shoops&orderby=menu_order&sort_order=asc' );
	foreach ( $posts_array as $post ) : setup_postdata( $post );
		$post_id = $post->ID;
		$posts[] = $post_id;
	endforeach;

    if(count($posts) > 1){
		$querys = $wpdb->get_row( "SELECT * FROM $wpdb->postmeta WHERE post_id in (".implode(",",$posts).") AND $wpdb->postmeta.meta_key = 'send_patient_date'  ORDER BY $wpdb->postmeta.meta_value DESC limit 1");
		if ( $querys ){
			$last_id = $querys->post_id;
		}
	}
	else{
		$main_data1 .= '<h4>Current Patient have no SHOOP</h4>';
	}
	$j = 0;
	
	
	
	foreach ( $posts_array as $post ) : setup_postdata( $post );
		$jk = 0;
		
		$data = '';
		
		$post_id = $post->ID;
		$patient_id = get_post_meta( $post_id, 'patient_id', true );
		if($patient_id == $pid){
			for($i=0; $i < 4; $i++) {
				
				$medicine_cannabinoid_1 = get_post_meta( $post_id, 'medicine'.($i+1).'_cannabinoid_1', true );
				$medicine1_target_dosage_1 = get_post_meta( $post_id, 'medicine'.($i+1).'_target_dosage_1', true );
				$medicine1_frequency_1 = get_post_meta( $post_id, 'medicine'.($i+1).'_frequency_1', true );
				$medicine1_cannabinoid_2 = get_post_meta( $post_id, 'medicine'.($i+1).'_cannabinoid_2', true );   
				$medicine1_target_dosage_2 = get_post_meta( $post_id, 'medicine'.($i+1).'_target_dosage_2', true );
				$medicine1_frequency_2 = get_post_meta( $post_id,'medicine'.($i+1).'_frequency_2', true );
				$medicine1_terpene = get_post_meta( $post_id, 'medicine'.($i+1).'_terpene', true );
				$medicine1_ingestion_method = get_post_meta( $post_id, 'medicine'.($i+1).'_ingestion_method', true );
				$medicine1_dosage_frequency = explode(",",get_post_meta( $post_id, 'medicine'.($i+1).'_dosage_frequency'.($i+1), true ));
				$medicine1_dosage_mg = explode(",",get_post_meta( $post_id, 'medicine'.($i+1).'_dosage_mg'.($i+1), true ));
				$medicine1_dosage_timing = explode(",",get_post_meta( $post_id, 'medicine'.($i+1).'_dosage_timing'.($i+1), true ));
				$notes = get_post_meta( $post_id, 'medicine_notes'.($i+1), true ); 
				$ki = 0;
				//print_r($medicine1_dosage_frequency);exit();
				$tbl2 ='';
				if($medicine1_dosage_frequency[0] != '' && strpos($medicine1_dosage_frequency[0], "hourly") === false){
					foreach ($medicine1_dosage_frequency as  $value) { 
						$tbl2 .= '<li><b>Dosage '.($ki+1).': </b> '.$value.' '.$medicine1_dosage_mg[$ki].' '.ucwords($medicine1_dosage_timing[$ki]).'</li>';
						$ki++;
					}
				}else{
					$tbl ='';
				}
				if($medicine_cannabinoid_1 != '' && $medicine1_ingestion_method !=''){
					$jk = 1;
					/*if($j == 0){ $row = 11; }else{
					$row = 6;
					}
					if($j == 1){ $data .= "</div><div class='row spce'>";}*/

					if(trim($medicine1_ingestion_method) == 'Ingested (edible)'){
						$eat = '';
					}else{
						$eat = '';
					}
					if($_SESSION["ses_id"]){
						$data .= '<div class="alert alert-success">'.$_SESSION["ses_id"].'</div>';
						unset($_SESSION["ses_id"]);
					}
					$data .= '
                        <div class="col-md-12">
                          <div class="shoop_review-content'.($i+1).'">
                              <div id="shoop_review-content'.($i+1).'1">
                              <section class="shoop-hd-crcl'.($i+1).'"><b>Medicine : '.($i+1).'</b><b class="txt-right"> '.$post_id.' Date Created : '.get_the_date( "Y-m-d", $post_id ).' </b></section>
                                
                                <ul class="shoop_list'.($i+1).'">
                                 <li><b>Target Cannabinoid: </b> '.$medicine_cannabinoid_1.'</li>';
                                 if($medicine1_terpene !=''){
									$data .= ' <li><b>TERPENES: </b> <ul>'. $medicine1_terpene.' </ul></li>';
								}
								$data .= ' <li><b>Target Dose: </b> '.str_replace("X","",$medicine1_target_dosage_1).'</li>
                                    <li><b>Frequency: </b> '.str_replace("X","x",$medicine1_frequency_1).'</li>
                                     ';
									/*if($medicine1_cannabinoid_2 !='' && $medicine1_cannabinoid_2 !='Select Cannabinoid 2...'){    
                                     $data .= '<li><b>Cannabinoid 2: </b> '.$medicine1_cannabinoid_2.'</li>';
                                    }*/
                                  $data .= $tbl2.'
                                    <li class="with-method">
                                      <p><b>Ingestion Method: </b> '.$medicine1_ingestion_method.'</p>
                                      <div class="method-text">
                                          <span>'.$medicine1_ingestion_method." ".$eat.' </span>';
                                if($i == 0){
									switch ($medicine1_ingestion_method) {
										case 'Inhaled':
											$data .=' <img src="'.CDRMED.'/includes/assets/img/icon5.png" alt=""/>';
											break;
										case 'Ingested (edible)':
											$data .=' <img src="'.CDRMED.'/includes/assets/img/icon1.png" alt=""/>';
											break;
										case 'Sublingual':
											$data .=' <img src="'.CDRMED.'/includes/assets/img/icon2.png" alt=""/>';
											break;
										case 'Suppository':
											$data .=' <img src="'.CDRMED.'/includes/assets/img/icon3.png" alt=""/>';
										break; 
										case 'Topical':
											$data .=' <img src="'.CDRMED.'/includes/assets/img/icon4.png" alt=""/>';
										break;     
									}
								}else if($i == 1){
                                    switch ($medicine1_ingestion_method) {
										case 'Inhaled':
											$data .=' <img src="'.CDRMED.'/includes/assets/img/1-1.png" alt=""/>';
											break;
										case 'Ingested (edible)':
											$data .=' <img src="'.CDRMED.'/includes/assets/img/1-2.png" alt=""/>';
											break;
										case 'Sublingual':
											$data .=' <img src="'.CDRMED.'/includes/assets/img/1-3.png" alt=""/>';
											break;
										case 'Suppository':
											$data .=' <img src="'.CDRMED.'/includes/assets/img/1-4.png" alt=""/>';
											break; 
										case 'Topical':
											$data .=' <img src="'.CDRMED.'/includes/assets/img/1-5.png" alt=""/>';
											break;     
									}
                                }else if($i == 2){
                                    switch ($medicine1_ingestion_method) {
										case 'Inhaled':
											$data .=' <img src="'.CDRMED.'/includes/assets/img/2-1.png" alt=""/>';
											break;
										case 'Ingested (edible)':
											$data .=' <img src="'.CDRMED.'/includes/assets/img/2-2.png" alt=""/>';
											break;
										case 'Sublingual':
											$data .=' <img src="'.CDRMED.'/includes/assets/img/2-3.png" alt=""/>';
											break;
										case 'Suppository':
											$data .=' <img src="'.CDRMED.'/includes/assets/img/2-4.png" alt=""/>';
											break; 
										case 'Topical':
											$data .=' <img src="'.CDRMED.'/includes/assets/img/2-5.png" alt=""/>';
											break;     
									}
                                } else if($i == 3){
									switch ($medicine1_ingestion_method) {
										case 'Inhaled':
											$data .=' <img src="'.CDRMED.'/includes/assets/img/3-1.png" alt=""/>';
											break;
										case 'Ingested (edible)':
											$data .=' <img src="'.CDRMED.'/includes/assets/img/3-2.png" alt=""/>';
											break;
										case 'Sublingual':
											$data .=' <img src="'.CDRMED.'/includes/assets/img/3-3.png" alt=""/>';
											break;
										case 'Suppository':
											$data .=' <img src="'.CDRMED.'/includes/assets/img/3-4.png" alt=""/>';
											break; 
										case 'Topical':
											$data .=' <img src="'.CDRMED.'/includes/assets/img/3-5.png" alt=""/>';
											break;     
									}
                                }        
                                $data .=' </div>
								</li>';
                                if($notes != '') { 
									$data .=' <li><b>Dosage Notes:  </b> '.$notes.'</li>';
								}
                                 
                                $data .='</ul>
                               
                                </div></div>
                    </div>
					';
                    $j++;
				}
			} // if condition ends here
		}
		
		$selected_shoop = '';
		if($latest_shoop_id == $post_id){
			$selected_shoop = '<div class="shoop-select"><i class="fa fa-star"></i></div>';
			$_SESSION["current_shoop"] = $data;
		}
		
		if($jk == 1){
			if($userrole != 'dispensary'){
				$data .=' <section>
				<div class="text-center form-footer-btns">
				  <a href="'.get_home_url().'/preview-strain/?post_id='.$post_id.'&pid='.$pid.'&edit=1&cp='.($i+1).'"><button type="button" class="btn reset">EDIT</button></a>
				   <a href="'.$_SERVER['REQUEST_URI'].'&send_email='. $post_id.'"><button type="button" class="btn preview">SEND TO PATIENT</button></a>
				   <a href="'.get_delete_post_link( $post_id, '', true ).'"><button type="button" class="btn preview">DELETE</button></a>
					 </div>
				  </section>';
			}
        }
		
		$main_data1 .= $selected_shoop.$data;
		
	endforeach;
    //$main_data1 .= '';
   
	// Reset Post Data
	wp_reset_postdata();
    return $main_data1;
}
add_shortcode( 'current-patient-shoops', 'current_patient_shoops' );
?>
