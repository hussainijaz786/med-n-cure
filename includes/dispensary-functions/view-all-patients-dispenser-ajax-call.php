<?php

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly 


add_action( 'wp_ajax_view_all_patient_dispensary_ajax_request', 'view_all_patient_dispensary_ajax_request');
add_action( 'wp_ajax_nopriv_view_all_patient_dispensary_ajax_request', 'view_all_patient_dispensary_ajax_request');

function view_all_patient_dispensary_ajax_request() {
	global $wpdb, $add_my_script;
	$add_my_script= true;
	$gurl = site_url( '/general-notes/', 'http' );
	$cin = site_url( '/view-complete-intake/', 'http' );
	//$shoopurl = site_url( '/new-patient-created/', 'http' );
	$shoopurl = site_url( '/shoop-for-patient/', 'http' );
	$current_usid = get_current_user_id();
	
	$patient_dispensary_id = $current_usid;
	
	$child_user = get_user_meta($patient_dispensary_id, 'user_parent', true);
	
	if($child_user != ''){
		$patient_dispensary_id = get_user_meta($patient_dispensary_id, 'user_parent_id', true);
	}
	
	
	$search = '';

	if(isset($_GET['search']) && $_GET['search'] != ''){
		$search = $_GET['search'];
		
		$query1 = "SELECT DISTINCT user_id FROM {$wpdb->prefix}usermeta
			WHERE `meta_key` = 'patient_dispensary_id' AND `meta_value` like '%$patient_dispensary_id%'
			ORDER BY user_id DESC";
		
		$patient_ids = array();
		$getpatients = $wpdb->get_results($query1);				
		foreach ($getpatients as $key => $value) {      // Loop all order Items
			$patient_ids[] = $value->user_id;
		}
		
		$query2 = "SELECT {$wpdb->prefix}users.*, {$wpdb->prefix}usermeta.*
			FROM {$wpdb->prefix}usermeta
			LEFT JOIN {$wpdb->prefix}users
			ON {$wpdb->prefix}users.ID={$wpdb->prefix}usermeta.user_id
			WHERE {$wpdb->prefix}usermeta.meta_key = 'patient_dispensary_id' and {$wpdb->prefix}usermeta.meta_value like '%".$patient_dispensary_id."%' and {$wpdb->prefix}usermeta.meta_value like '%".$search."%' or {$wpdb->prefix}users.user_email like '%".$search."%'
			ORDER BY {$wpdb->prefix}usermeta.user_id;";
				
		$search_ids = array();
		$getpatients = $wpdb->get_results($query2);				
		foreach ($getpatients as $key => $value) {      // Loop all order Items
			$search_ids[] = $value->user_id;
		}
		
		$result = array_intersect($patient_ids, $search_ids);
		$result_id = join("','",$result);
		
		$query3 = "SELECT DISTINCT user_id FROM {$wpdb->prefix}usermeta
			WHERE `meta_key` = 'patient_dispensary_id' AND `meta_value` like '%$patient_dispensary_id%'
			ORDER BY user_id DESC";
		
		$wpdb->get_results($query3);
		$product_json['total'] = $wpdb->num_rows;
		wp_reset_query();
			
		$ofset = $_GET['offset'];
		$limit = $_GET['limit'];
		
		$query = "SELECT DISTINCT user_id FROM {$wpdb->prefix}usermeta
			WHERE `user_id` in ('".$result_id."')
			ORDER BY user_id DESC
			LIMIT {$ofset},{$limit}";

	}
	else{
		
		$query1 = "SELECT DISTINCT user_id FROM {$wpdb->prefix}usermeta
			WHERE `meta_key` = 'patient_dispensary_id' AND `meta_value` like '%$patient_dispensary_id%'
			ORDER BY user_id DESC";
					
		$wpdb->get_results($query1);
		$product_json['total'] = $wpdb->num_rows;
		wp_reset_query();
			
		$ofset = $_GET['offset'];
		$limit = $_GET['limit'];
		
		$query = "SELECT DISTINCT user_id FROM {$wpdb->prefix}usermeta
			WHERE `meta_key` = 'patient_dispensary_id' AND `meta_value` like '%$patient_dispensary_id%'
			ORDER BY user_id DESC
			LIMIT {$ofset},{$limit}";
	}
	$getpatients = $wpdb->get_results($query);
	
	foreach ($getpatients as $key => $value) {      // Loop all order Items
		$patient_id = $value->user_id;
		$user_info = get_userdata($patient_id);
		$first_name = $user_info->first_name; 
		$last_name = $user_info->last_name; 
		$email = $user_info->user_email;
		
		$user_latest_shoop = gh_get_latest_shoop($patient_id);
		if(!empty($user_latest_shoop)) {
			$user_latest_shoop_date = get_the_date( "m-d-Y", $user_latest_shoop );
			$user_latest_shoop_date_print = "<span style='width:100%; float: left; font-size: 10px;'>Last Shoop: {$user_latest_shoop_date}</span>";
		}
		else {
			$user_latest_shoop_date ='01-17-2000';
			$user_latest_shoop_date_print = '';
		}
		
		 $product_json['rows'][] = array(
			'usid' => $patient_id,
			'id' => '<div data-short="'.strtolower($first_name).' '.strtolower($last_name).'"><a href="'.$cin.'?pid='.$patient_id.'" data-check=""><i class="fa fa-file-text-o"></i>  '.ucfirst(strtolower($first_name)).' '.ucfirst(strtolower($last_name)).'</a></div>',
			'email' => '<div data-short="'.strtolower($email).'"><span style="text-transform:lowercase;cursor: pointer;" class="custom-user-modal-show" data-call-action="email" data-email="'.$email.'">'.$email.'</span></div>',
			'date' => '<div data-short="'.$user_latest_shoop_date.'"><a href="'.$shoopurl.'?&pid='.$patient_id.'">Launch SHOOP</a>'.$user_latest_shoop_date_print.'</div>',
			 //'notes' => '<a href="'.$gurl.'?pid='.$patient_id.'">Patient Notes</a>',
			  'action' => '<span id="show-update-user-popup" class="custom-user-modal-show patient-action-td-child" data-call-action="profile-update" data-user_id="'.$patient_id.'" data-first_name="'.$first_name.'" data-last_name="'.$last_name.'" data-email="'.$email.'" data-action="by_patient" style="cursor:pointer;"><i class="fa fa-edit"></i></span><span class="user-remove-custom-btn patient-action-td-child" data-call-action="remove-user" data-action-permission="allow" data-user_id="'.$patient_id.'" style="cursor:pointer;"><i class="fa fa-remove"></i></span>'
		);
	}
	
	$sort = isset($_GET['sort']) ? $_GET['sort']: 'usid';
	$order = ($_GET['order']=='asc') ? SORT_ASC : SORT_DESC;
	if(!isset($_GET['sort'])){
			$order = SORT_DESC;
	}
	$product_json['rows'] = array_sort($product_json['rows'], $sort, $order);
	$product_json = json_encode($product_json);
	echo $product_json;
	die;
}


?>