<?php

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly 

add_action( 'wp_ajax_add_patient_to_dispensary_request', 'add_patient_to_dispensary_request');
add_action( 'wp_ajax_nopriv_add_patient_to_dispensary_request', 'add_patient_to_dispensary_request');

function add_patient_to_dispensary_request() {
	$current_usid = get_current_user_id();
	$patient_id = $_POST['patient_id'];
	$dispensary_arr = json_decode(get_user_meta($patient_id, 'patient_dispensary_id', true), true);
	$dispensary_arr[] = $current_usid;
	update_user_meta($patient_id, 'patient_dispensary_id', json_encode($dispensary_arr));
	echo true;
	die();
}

//Verification for Dispensary
//Dispensary can access only child Patients
add_action ('init','verfity_dispensary_patient');
function verfity_dispensary_patient() {
	$userrole = get_user_role();
	if($userrole == 'dispensary'){
		$current_usid = get_current_user_id();
		
		$patient_dispensary_id = $current_usid;
	
		$child_user = get_user_meta($patient_dispensary_id, 'user_parent', true);
		if($child_user != ''){
			$patient_dispensary_id = get_user_meta($patient_dispensary_id, 'user_parent_id', true);
		}
		
		$url = site_url();
		if(isset($_GET['pid'])){
			$patient_id = $_GET['pid'];
			
			$patient_dispensary_arr = json_decode(get_user_meta($patient_id, 'patient_dispensary_id', true), true);
			
			/* if(!in_array($patient_dispensary_id, $patient_dispensary_arr)){
				header('Location:'.$url.'/dispensary-dashboard/#patients');
				die();
			} */
		}
		elseif(isset($_GET['proid'])){
			$product_id = $_GET['proid'];
			$product_dispensary_id = get_post_meta($product_id, 'product_dispensary_id', true);
			
			if($product_dispensary_id != $patient_dispensary_id){
				header('Location:'.$url.'/inventory-list');
				die();
			}
		}
	}
}

function wpd_show_patients_list_dispenser() {
	
	$html = '<a class="button cdrmed-primary-button disdash" href="#add-patient" data-toggle="tab">Add Patient</a>';
	$html .= '<table data-toggle="table"
		data-url="'.get_admin_url().'/admin-ajax.php?action=view_all_patient_dispensary_ajax_request"
		data-pagination="true"
		data-side-pagination="server"
		data-page-list="[10, 25, 50, 100, 250, 500, 1000, 5000]"
		data-search="true"
		data-height="300"

		data-show-toggle="false"
		data-classes="table table-hover stripped"
		data-striped="true"
		data-show-columns="true"
		data-id-field="id"
		data-smart-display="true"
		data-toolbar="#filter-bar"
		data-show-filter="true"
		data-mobile-responsive="true">';
		
		$html .= '<thead>
		<tr>
		<th data-field="id" data-sortable="true" data-sorter="starsSorter" data-sort-name="_id_data">Name</th>
		<th data-field="email" data-sortable="true">Email</th>
		<th data-field="date" data-sortable="true" data-sort-name="_date_data" data-sorter="monthSorter">Cannabis Dosing</th>
		<!--<th data-field="notes">Patient Notes</th>-->
		<th data-field="action" class="patient-action-th"><i class="fa fa-info"></i></th>
		</tr>
		</thead>';

	$html .= '</table>';
	return $html;	

}

add_shortcode( 'cdrmed_patients_dispenser_list', 'wpd_show_patients_list_dispenser' );

?>