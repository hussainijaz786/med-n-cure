<?php 

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly 

// Add Shortcode
add_action( 'wp_head', 'terpenes_model_boxes');
function terpenes_model_boxes() {
    if(is_page(array('create-new-shoop-dis','preview-strain'))){

?>  	<div class="modal fade" id="myModal1" role="dialog">
            <div class="modal-dialog multi-select-modal purple">
			
			<?php
				if(is_page('preview-strain')){
					$post_id = !isset($_GET['post_id']) ? null : $_GET['post_id'];
					$terpene = get_post_meta( $post_id, 'medicine1_terpene', true );
					$terpenes1 = explode(',', $terpene);
				}
				
			?>
            
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close fa fa-close" data-dismiss="modal"></button>
                        <h4 class="modal-title">Please select TERPENE </h4>
                    </div>
                    <div class="modal-body">
                    <ul class="mCustomScrollbar testlist" data-mcs-theme="dark">
                            <li>
                                <input type="checkbox" value="Alpha-humulene" name="a1" id="a12" <?php if(in_array('Alpha-humulene',$terpenes1 )){ echo 'checked'; } ?>>
                                <label for="a12">Alpha-humulene</label>
                            </li>
                            <li>
                                <input type="checkbox" value="Alpha-pinene" name="s1" id="a13" <?php if(in_array('Alpha-pinene',$terpenes1 )){ echo 'checked'; } ?>>
                                <label for="a13">Alpha-pinene</label>
                            </li>
                            <li>
                                <input type="checkbox" value="Beta-caryophyllene" name="df1" id="a14" <?php if(in_array('Beta-caryophyllene',$terpenes1 )){ echo 'checked'; } ?>>
                                <label for="a14">Beta-caryophyllene</label>
                            </li>
                            <li>
                                <input type="checkbox" value="Beta-pinene" name="g1" id="a15" <?php if(in_array('Beta-pinene',$terpenes1 )){ echo 'checked'; } ?>>
                                <label for="a15">Beta-pinene</label>
                            </li>
                            <li>
                                <input type="checkbox" value="Bisabolol" name="h1" id="a16" <?php if(in_array('Bisabolol',$terpenes1 )){ echo 'checked'; } ?>>
                                <label for="a16">Bisabolol</label>
                            </li>
                            <li>
                                <input type="checkbox" value="Caryophyllene Oxide" name="j1" id="a17" <?php if(in_array('Caryophyllene Oxide',$terpenes1 )){ echo 'checked'; } ?>>
                                <label for="a17">Caryophyllene Oxide</label>
                            </li>
                            <li>
                                <input type="checkbox" value="Geraniol" name="k1" id="a18" <?php if(in_array('Geraniol',$terpenes1 )){ echo 'checked'; } ?>>
                                <label for="a18">Geraniol</label>
                            </li><li>
                                <input type="checkbox" value="Limonene" name="l1" id="a19" <?php if(in_array('Limonene',$terpenes1 )){ echo 'checked'; } ?>>
                                <label for="a19">Limonene</label>
                            </li>
                            <li>
                                <input type="checkbox" value="Linalool" name="i1" id="a20" <?php if(in_array('Linalool',$terpenes1 )){ echo 'checked'; } ?>>
                                <label for="a20">Linalool</label>
                            </li>
                            <li>
                                <input type="checkbox" value="Myrcene" name="u1" id="a21" <?php if(in_array('Myrcene',$terpenes1 )){ echo 'checked'; } ?>>
                                <label for="a21">Myrcene</label>
                            </li>
                            <li>
                                <input type="checkbox" value="Terpinolene" name="y1" id="a22" <?php if(in_array('Terpinolene',$terpenes1 )){ echo 'checked'; } ?>>
                                <label for="a22">Terpinolene</label>
                            </li>
                           
                        </ul>
                        
                        <div class="selected-terpens">
                            <b>Selected TERPENES:</b>
                            <p></p>
                        </div>
                        <div class="text-center">
                         <button type="button" id="mode1" name="ok" class="btn preview">OK</button>
                            </div>
                    </div>
                    
                </div>
            
            </div>
        </div>

		<!--   Red Color model box -->
		<div class="modal fade" id="myModal2" role="dialog">
            <div class="modal-dialog multi-select-modal reded">
			
				<?php
					if(is_page('preview-strain')){
						$post_id = !isset($_GET['post_id']) ? null : $_GET['post_id'];
						$terpene = get_post_meta( $post_id, 'medicine2_terpene', true );
						$terpenes2 = explode(',', $terpene);
					}
					
				?>
            
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close fa fa-close" data-dismiss="modal"></button>
                        <h4 class="modal-title">Please select TERPENE </h4>
                    </div>
                    <div class="modal-body">
                    <ul class="mCustomScrollbar testlist2" data-mcs-theme="dark">
                            <li>
                                <input type="checkbox" value="Alpha-humulene" name="a" id="a122"<?php if(in_array('Alpha-humulene',$terpenes2 )){ echo 'checked'; } ?>>
                                <label for="a122">Alpha-humulene</label>
                            </li>
                            <li>
                                <input type="checkbox" value="Alpha-pinene" name="s" id="a132"<?php if(in_array('Alpha-pinene',$terpenes2 )){ echo 'checked'; } ?>>
                                <label for="a132">Alpha-pinene</label>
                            </li>
                            <li>
                                <input type="checkbox" value="Beta-caryophyllene" name="df" id="a142"<?php if(in_array('Beta-caryophyllene',$terpenes2 )){ echo 'checked'; } ?>>
                                <label for="a142">Beta-caryophyllene</label>
                            </li>
                            <li>
                                <input type="checkbox" value="Beta-pinene" name="g" id="a152"<?php if(in_array('Beta-pinene',$terpenes2 )){ echo 'checked'; } ?>>
                                <label for="a152">Beta-pinene</label>
                            </li>
                            <li>
                                <input type="checkbox" value="Bisabolol" name="h" id="a162"<?php if(in_array('Bisabolol',$terpenes2 )){ echo 'checked'; } ?>>
                                <label for="a162">Bisabolol</label>
                            </li>
                            <li>
                                <input type="checkbox" value="Caryophyllene Oxide" name="j" id="a172"<?php if(in_array('Caryophyllene Oxide',$terpenes2 )){ echo 'checked'; } ?>>
                                <label for="a172">Caryophyllene Oxide</label>
                            </li>
                            <li>
                                <input type="checkbox" value="Geraniol" name="k" id="a182"<?php if(in_array('Geraniol',$terpenes2 )){ echo 'checked'; } ?>>
                                <label for="a182">Geraniol</label>
                            </li><li>
                                <input type="checkbox" value="Limonene" name="l" id="a192"<?php if(in_array('Limonene',$terpenes2 )){ echo 'checked'; } ?>>
                                <label for="a192">Limonene</label>
                            </li>
                            <li>
                                <input type="checkbox" value="Linalool" name="i" id="a202"<?php if(in_array('Linalool',$terpenes2 )){ echo 'checked'; } ?>>
                                <label for="a202">Linalool</label>
                            </li>
                            <li>
                                <input type="checkbox" value="Myrcene" name="u" id="a212"<?php if(in_array('Myrcene',$terpenes2 )){ echo 'checked'; } ?>>
                                <label for="a212">Myrcene</label>
                            </li>
                            <li>
                                <input type="checkbox" value="Terpinolene" name="y" id="a222" <?php if(in_array('Terpinolene',$terpenes2 )){ echo 'checked'; } ?>>
                                <label for="a222">Terpinolene</label>
                            </li>
                           
                        </ul>
                        
                        <div class="selected-terpens2" id="red">
                            <b>Selected TERPENES:</b>
                            <p></p>
                        </div>
                         <div class="text-center">
                         <button type="button" id="mode2" name="ok" class="btn preview">OK</button>
                            </div>
                    </div>
                    
                </div>
            
            </div>
        </div>
		
		<!--   lightblue Color model box -->
		<div class="modal fade" id="myModal3" role="dialog">
            <div class="modal-dialog multi-select-modal lightblue">
			
				<?php
					if(is_page('preview-strain')){
						$post_id = !isset($_GET['post_id']) ? null : $_GET['post_id'];
						$terpene = get_post_meta( $post_id, 'medicine3_terpene', true );
						$terpenes3 = explode(',', $terpene);
					}
					
				?>
            
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close fa fa-close" data-dismiss="modal"></button>
                        <h4 class="modal-title">Please select TERPENE </h4>
                    </div>
                    <div class="modal-body">
                    <ul class="mCustomScrollbar testlist3" data-mcs-theme="dark">
                            <li>
                                <input type="checkbox" value="Alpha-humulene" name="a" id="a121" <?php if(in_array('Alpha-humulene',$terpenes3 )){ echo 'checked'; } ?>>
                                <label for="a121">Alpha-humulene</label>
                            </li>
                            <li>
                                <input type="checkbox" value="Alpha-pinene" name="s" id="a131" <?php if(in_array('Alpha-pinene',$terpenes3 )){ echo 'checked'; } ?>>
                                <label for="a131">Alpha-pinene</label>
                            </li>
                            <li>
                                <input type="checkbox" value="Beta-caryophyllene" name="df" id="a141" <?php if(in_array('Beta-caryophyllene',$terpenes3 )){ echo 'checked'; } ?>>
                                <label for="a141">Beta-caryophyllene</label>
                            </li>
                            <li>
                                <input type="checkbox" value="Beta-pinene" name="g" id="a151" <?php if(in_array('Beta-pinene',$terpenes3 )){ echo 'checked'; } ?>>
                                <label for="a151">Beta-pinene</label>
                            </li>
                            <li>
                                <input type="checkbox" value="Bisabolol" name="h" id="a161" <?php if(in_array('Bisabolol',$terpenes3 )){ echo 'checked'; } ?>>
                                <label for="a161">Bisabolol</label>
                            </li>
                            <li>
                                <input type="checkbox" value="Caryophyllene Oxide" name="j" id="a171" <?php if(in_array('Caryophyllene Oxide',$terpenes3 )){ echo 'checked'; } ?>>
                                <label for="a171">Caryophyllene Oxide</label>
                            </li>
                            <li>
                                <input type="checkbox" value="Geraniol" name="k" id="a181" <?php if(in_array('Geraniol',$terpenes3 )){ echo 'checked'; } ?>>
                                <label for="a181">Geraniol</label>
                            </li><li>
                                <input type="checkbox" value="Limonene" name="l" id="a191" <?php if(in_array('Limonene',$terpenes3 )){ echo 'checked'; } ?>>
                                <label for="a191">Limonene</label>
                            </li>
                            <li>
                                <input type="checkbox" value="Linalool" name="i" id="a201" <?php if(in_array('Linalool',$terpenes3 )){ echo 'checked'; } ?>>
                                <label for="a201">Linalool</label>
                            </li>
                            <li>
                                <input type="checkbox" value="Myrcene" name="u" id="a211" <?php if(in_array('Myrcene',$terpenes3 )){ echo 'checked'; } ?>>
                                <label for="a211">Myrcene</label>
                            </li>
                            <li>
                                <input type="checkbox" value="Terpinolene" name="y" id="a221" <?php if(in_array('Terpinolene',$terpenes3 )){ echo 'checked'; } ?>>
                                <label for="a221">Terpinolene</label>
                            </li>
                           
                        </ul>
                        
                        <div class="selected-terpens3" id="lightblue">
                            <b>Selected TERPENES:</b>
                            <p></p>
                        </div>
                     <div class="text-center">
                         <button type="button" id="mode3" name="ok" class="btn preview">OK</button>
                            </div>
                    </div>
                    
                </div>
            
            </div>
        </div>
		
		<!--   lightgreen Color model box -->
		<div class="modal fade" id="myModal4" role="dialog">
            <div class="modal-dialog multi-select-modal lightgreen ">
				
				<?php
					if(is_page('preview-strain')){
						$post_id = !isset($_GET['post_id']) ? null : $_GET['post_id'];
						$terpene = get_post_meta( $post_id, 'medicine4_terpene', true );
						$terpenes4 = explode(',', $terpene);
					}
					
				?>
            
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close fa fa-close" data-dismiss="modal"></button>
                        <h4 class="modal-title">Please select TERPENE </h4>
                    </div>
                    <div class="modal-body">
                    <ul class="mCustomScrollbar testlist4" data-mcs-theme="dark">
                            <li>
                                <input type="checkbox" value="Alpha-humulene" name="a" id="a123" <?php if(in_array('Alpha-humulene',$terpenes4 )){ echo 'checked'; } ?>>
                                <label for="a123">Alpha-humulene</label>
                            </li>
                            <li>
                                <input type="checkbox" value="Alpha-pinene" name="s" id="a133" <?php if(in_array('Alpha-pinene',$terpenes4 )){ echo 'checked'; } ?>>
                                <label for="a133">Alpha-pinene</label>
                            </li>
                            <li>
                                <input type="checkbox" value="Beta-caryophyllene" name="df" id="a143" <?php if(in_array('Beta-caryophyllene',$terpenes4 )){ echo 'checked'; } ?>>
                                <label for="a143">Beta-caryophyllene</label>
                            </li>
                            <li>
                                <input type="checkbox" value="Beta-pinene" name="g" id="a153" <?php if(in_array('Beta-pinene',$terpenes4 )){ echo 'checked'; } ?>>
                                <label for="a153">Beta-pinene</label>
                            </li>
                            <li>
                                <input type="checkbox" value="Bisabolol" name="h" id="a163" <?php if(in_array('Bisabolol',$terpenes4 )){ echo 'checked'; } ?>>
                                <label for="a163">Bisabolol</label>
                            </li>
                            <li>
                                <input type="checkbox" value="Caryophyllene Oxide" name="j" id="a173" <?php if(in_array('Caryophyllene Oxide',$terpenes4 )){ echo 'checked'; } ?>>
                                <label for="a173">Caryophyllene Oxide</label>
                            </li>
                            <li>
                                <input type="checkbox" value="Geraniol" name="k" id="a183" <?php if(in_array('Geraniol',$terpenes4 )){ echo 'checked'; } ?>>
                                <label for="a183">Geraniol</label>
                            </li><li>
                                <input type="checkbox" value="Limonene" name="l" id="a193" <?php if(in_array('Limonene',$terpenes4 )){ echo 'checked'; } ?>>
                                <label for="a193">Limonene</label>
                            </li>
                            <li>
                                <input type="checkbox" value="Linalool" name="i" id="a203" <?php if(in_array('Linalool',$terpenes4 )){ echo 'checked'; } ?>>
                                <label for="a203">Linalool</label>
                            </li>
                            <li>
                                <input type="checkbox" value="Myrcene" name="u" id="a213" <?php if(in_array('Myrcene',$terpenes4 )){ echo 'checked'; } ?>>
                                <label for="a213">Myrcene</label>
                            </li>
                            <li>
                                <input type="checkbox" value="Terpinolene" name="y" id="a223" <?php if(in_array('Terpinolene',$terpenes4 )){ echo 'checked'; } ?>>
                                <label for="a223">Terpinolene</label>
                            </li>
                           
                        </ul>
                        
                        <div class="selected-terpens4" id="lightgreen">
                            <b>Selected TERPENES:</b>
                            <p></p>
                        </div>
                         <div class="text-center">
                         <button type="button" id="mode4" name="ok" class="btn preview">OK</button>
                            </div>
                    </div>
                    
                </div>
            
            </div>
        </div>
	<?php
    }?>
	
		<div class="modal fade" id="myModalmg" role="dialog">
            <div class="modal-dialog multi-select-modal purple">
            
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close fa fa-close" data-dismiss="modal"></button>
                        <h4 class="modal-title">Document Image</h4>
                    </div>
                    <div class="modal-body" id="img">
                    
                     <img src="" id="myimg" />
                        <div class="text-center">
                         <button type="button" id="mode1" name="ok" class="btn preview">OK</button>
                       </div>
                    </div>
                    
                </div>
            
            </div>
        </div>
	<?php }  // function ends here
// add_shortcode( 'strain_popup_dispaly', 'model_boxes' );
?>