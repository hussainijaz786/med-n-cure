<?php
if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly 

function view_after_adding_patient() {
$url = get_site_url();
$pid = $_GET['pid'];
$html = '';
$html .= do_shortcode('[dispensar-tab-nav]');
// Load Patient Snapshot
$html .= do_shortcode('[user_profile_dispaly]');

// Show the Button to Create New SHOOP
$html .= '<center><a href="'.$url.'/dispensary-dashboard/create-new-shoop-dis/?pid='.$pid.'" class="btn bordered">Create New SHOOP</a></center>';

$html .='<div class="col-md-12">';
// Show All The SHOOPS Created Previously
$html .= do_shortcode('[show-all-patient-shoops]');
$html .='</div>';
// Close Col MD 12
$html .='</div>';

return $html;

}

add_shortcode('dispensar-after-patient-add','view_after_adding_patient');

?>