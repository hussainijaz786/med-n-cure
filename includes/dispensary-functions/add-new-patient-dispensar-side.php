<?php
/*
Plugin Name: User registeration Form
Description: Description
Version: 1.0
Author: wordpress
*/

add_shortcode( 'user-registeration-form-physician', 'user_registeration_form_physician');
function user_registeration_form_physician() {
	acf_form_head();
	ob_start();
// Bail if not logged in or able to post
	if ( ! ( is_user_logged_in() ) ) {
		echo '<p>You Must Be Logged In To Fill Your Intake Form</p>';
		return;
	}
	$current_user_id = get_current_user_id();
	acf_form(array(
					'post_id'		=> 'new_post',
					'field_groups' => array('group_56da79ad4bcdrmed'),
					'new_post'		=> array(
						'post_type'		=> 'pre-registrations',
						'post_status'		=> 'publish',
						'post_title'	=> '5 Questions',
						'post_author'	=> 	$current_user_id
					),
					'label_placement' => 'left',
					'submit_value'		=> 'Add New Patient'
				));
				
	$mmjreco = ob_get_contents();
		ob_get_clean();
		ob_end_flush();
		return $mmjreco;
	
}

add_action('acf/save_post', 'add_new_patient_by_dispensary');

function add_new_patient_by_dispensary( $post_id ) {
	$url = get_site_url();
	// bail early if not a contact_form post
	if( get_post_type($post_id) !== 'pre-registrations' ) {
		return;
	}
	// bail early if editing in admin
	if( is_admin() ) {
		return;
	}
	// vars
	$postid = get_post( $post_id );
	
	$userrole = get_user_role();
	
	if(is_page(array('add-new-patient','dispensary-dashboard','physicians-dashboard'))){			// Only Execute Functions if User is on Current Page of Add New Patient
	
		// Time to Get Saved Fields Back and Process the User and Intake
		
		

		$fname  = get_field( "first_name", $postid );
		$lname 	= get_field( "last_name", $postid );
		$email  = get_field( "best_email", $postid );
		$date_of_birth  = get_field( "date_of_birth", $postid );
		$gender = get_field( "gender", $postid );
		$medications = get_field('please_list_your_current_pharmaceuticals_and_supplements',$postid);
		$diagnosis  = get_field( "principal_primary_diagnosis", $postid );
		$stages 	= get_field( "stage_cancer", $postid );
		$icd_code = get_field('icd-9_or_icd-10_code_if_known',$post_id);
		$cannibus  = get_field( "what_is_your_previous_experience_with_cannabis", $postid );
		$objective = get_field( "what_is_your_objective", $postid );
		
		$user_name = $user_email = $email;
		$user_id = username_exists( $user_name );
		$intake_id_exists = null;
		if($user_id){
			$intake_id_exists = get_user_meta($user_id, 'patient_intake', true);
		}
		
		$notify = "both";
		
		// Double Check User Doesn't Exists and No Email Id Is associated with Other User
		if ( !$user_id and email_exists($user_email) == false ) {
		
			$random_password = wp_generate_password( $length=12, $include_standard_special_chars=false );
			
			$user_id = wp_create_user( $user_name, $random_password, $user_email );
			
			$user_id_role = new WP_User($user_id);
			
			$user_id_role->set_role('patient');
			$now = date('Y-m-d H:i:s');
			$setuserid = wp_update_user( array( 'ID' => $user_id, 'first_name' => $fname, 'last_name' => $lname, 'display_name' => $fname, 'nickname' => $fname ) );
			
			if ( is_wp_error( $setuserid ) ) {
				// There was an error, probably that user doesn't exist.
			} else {
				wp_new_user_notification( $user_id, '', $notify );
			}
			
		}	// End User Existence Check
		
		if($user_id) {
			update_user_meta( $user_id, 'pre-registration-id', $post_id );
			// Create Intake post object
			if($intake_id_exists) {				// Check if Intake Already EXISTS for that User
				$post_id = $intake_id_exists;
			} else {
				$my_post = array(
				  'post_title'    => $user_email,
				  'post_content'  => 'Intake Form details',
				  'post_type'		=> 'intake',
				  'post_status'		=> 'publish',
				  'post_author'   => $user_id,
				);
				 
				// Insert the post into the database
				$post_id = wp_insert_post( $my_post );
			}
			update_user_meta($user_id, 'patient_intake', $post_id);
			update_user_meta($user_id, 'patient_cannabis_objective', $objective);
			// Insert the Values to Intake Post We are Going to Create For User
			update_field('field_56c2b5c140152',$fname , $post_id);
			update_field('field_56c2b5c140566', $lname, $post_id);
			update_field('field_56c47071c3827',$email, $post_id);
			update_field('field_56c2eb9ec5f99',$diagnosis , $post_id);
			update_field('field_56c2b5c1112ec', $stages, $post_id);
			update_field('field_56c2ebaac5f9a', $icd_code, $post_id);
			update_field('field_56c47071c3827',$cannibus, $post_id);
			update_field('field_56c4717ac3831',$objective, $post_id);
			update_field('field_56c2b5c142848',$date_of_birth, $post_id);
			update_field('field_56c2b5c1418e7',$gender, $post_id);
			
			// Scroll Through the List of Medications and Insert Them in 
			if($medications)
			{
				foreach($medications as $medicine)
				{ 
					$row = array(
								'field_56c2ed2038706' => $medicine['medication_name'],
								'field_56c2ed4138707'   => $medicine['dose'],
								'field_56c2ed5038708'  => $medicine['frequency']
								);
					$i = add_row('field_56c2ecf138705', $row, $post_id);
				}
			}
			update_user_meta( $user_id, 'pre-registration-id', $postid );
			
			$patient_id = $user_id;
			
			$parent_id = get_current_user_id();
			//$current_usid = get_current_user_id();
			$child_user = get_user_meta($parent_id, 'user_parent', true);
			if($child_user != ''){
				$parent_id = get_user_meta($parent_id, 'user_parent_id', true);
			}
			
			$patient_parent = array($parent_id);
			if($userrole == 'physician'){
				update_user_meta($patient_id, 'patient_physician_id', json_encode($patient_parent));
			}
			else{
				update_user_meta($patient_id, 'patient_dispensary_id', json_encode($patient_parent));
				update_user_meta($patient_id, 'patient_dispenary_products_id', json_encode($patient_parent));
			}
			
			update_user_meta( $patient_id, 'user_full_name', $fname.' '.$lname );
			
			cdrmed_save_activity_log('New Patient added!', $patient_id);
			
			update_post_meta($post_id, 'patient_parent_user_id', $parent_id);
			

		}	// Add above Data When User Is created and We Have User ID To link Posts.
		
		wp_redirect($url.'/general-notes/?pid='.$user_id);
		exit;
	
	}
	
	
	
}