<?php 
if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly 
global $post_id;




function cdrmed_shoop_patient_products2() {
	
	$current_user = wp_get_current_user();
	$current_usid = get_current_user_id();
	
	ob_start();
	$patient_id = !isset($_GET['pid'])?null:$_GET['pid'];
	// get patient's latest shoop post id
	$latest_shoop_id = get_user_meta(  $patient_id, 'latest_patient', true);
	if(!$latest_shoop_id || FALSE === get_post_status( $latest_shoop_id ) ) return; // there is no shoop for patient, get the hell out of here
	$shoop_data = get_orgnized_shoop_data($latest_shoop_id);
	//echo "<pre>";
	//print_r($shoop_data);
	//echo "</pre>";
  

	?>
	<div class="col-md-12 shoop-img">
	<!--<h3 class="p-shoops">Select from the following MednCures's Products to Match your SHOOP. </h3>-->
		<?php 
		$medicine_number = 0;
		$medicine_number_args = array();
		foreach ($shoop_data as $medicine_number => $medicine) {
			$medicine_number++;
			$products_args_arr = array(
				'post_type' => 'product',
				'author ' => $current_usid,
			);
			$product_tax = 'product_cat';
			$target_dose = explode('X', $medicine['target_dosage']);
			global $wpdb;

			/**
			 * 1
			 * a. Exact match to prescription 
			 */
			if($medicine['ingestion_method'] != 'Sublingual' && $medicine['ingestion_method'] != 'Topical' && $medicine['ingestion_method'] != 'Suppository'){
				$get_cats = get_terms($product_tax, array( 'search' => $medicine['ingestion_method'] ) );
				$term_ids = array();
				if($get_cats){
					foreach ($get_cats as $term) {
						$term_ids[] = $term->term_id;
					}
					$products_args_arr['tax_query'][] = array( 
						'taxonomy' => $product_tax,
						'field'    => 'id',
						'terms'     => $term_ids
					);
				}
			}

			// Local: 
			$category_ids = array('sublingual', 'oral');
			//$category_ids = array(9, 22);
			/**
			 * 3. Concentration of cannabinoid:
			 * a. If target dose is = or < 50mg cannabinoid per day, select infused oil -> ID:19,18 or tincture for sublingual and oral
			 */
			if(intval($target_dose[0]) <= 50 && $target_dose[1] == 'mg'){
			  $category_ids = array('cannabis-infused-mct-oil', 'cannabis-infused-olive-oil' ,'edible', 'tincture');
			}

			/**
			 * 3. Concentration of cannabinoid:
			 * b. If target dose is > 50mg cannabinoid per day, select extract -> ID:17 or infused -> ID:18,19,20 product with smallest volume of medicine. - PROBABLY NOT NECESSARY
			 */
			if(intval($target_dose[0]) > 50 && $target_dose[1] == 'mg'){
			  // Local: 
			  $category_ids = array('sublingual', 'oral', 'edible', 'cannabis-extract', 'cannabis-infused-olive-oil' , 'cannabis-infused-mct-oil', 'cannabis-infused-coconut-topical');
			  //$category_ids = array(9, 22, 21, 17, 18, 19, 20);
			}

			/**
			 * Rule one: b. IF “sublingual” selected, THEN choose products from inventory in the following order:
			 * i. Sublingual -> ID: 12
			 * ii. Oral -> ID: 22
			 * iii. Edible -> ID: 21
			 */
			if( 'Sublingual' == $medicine['ingestion_method'] ){
			  //$products_args_arr['tax_query']['relation'] = "OR";
			  $category_ids[] = 'edible';
			  $products_args_arr['tax_query'][] = array( 
				'taxonomy' => $product_tax,
				'field'    => 'slug',
				'terms'     => $category_ids,
				'operator' => 'IN'
				);
			}

			/**
			 * Rule two: c. IF “topical” selected, THEN choose products from inventory in the following order:
			 * i.  Topical -> ID: 14
			 * ii. Transdermal -> ID: 15
			 */
			if ( 'Topical' == $medicine['ingestion_method'] ) {
			  $products_args_arr['tax_query'][] = array( 
				'taxonomy' => $product_tax,
				'field'    => 'slug',
				'terms'     => array('topical', 'transdermal'),
				'operator' => 'IN'
				);
			}

			/**
			 * d. IF “suppository”, THEN choose products from inventory in the following order:
			 * i.  Suppository -> ID: 13
			 * ii. extract -> ID: 17
			 */
			if ( 'Suppository' == $medicine['ingestion_method'] ) {
			  $products_args_arr['tax_query'][] = array( 
				'taxonomy' => $product_tax,
				'field'    => 'slug',
				'terms'     => array('suppository', 'cannabis-extract'),
				'operator' => 'IN'
			  );
			}

			/**
			 * 2. Major cannabinoid matches 
			 */
			preg_match_all("/\(([^)]+)\)/", $medicine['cannabinoid'], $matches);
			$meta_key_part = strtolower($matches[1][0]);
			$products_args_arr['meta_query'][] = array(
				'key' => "_azcl_{$meta_key_part}_tests",
				'value' => $target_dose[0],
				'compare' => '>='
			);
		
			$secondary_cannabinoid = 'thc';
			if($meta_key_part == 'thc'){
				$secondary_cannabinoid = 'cbd';
			}
			$products_args_arr['meta_query'][] = array(
				'key' => "_azcl_{$secondary_cannabinoid}_tests",
				'value' => '',
				'compare' => '!='
			);

			/**
			* 4. Select those that have listed terpenes that match the prescription.
			*/
			$terpenes_raw = $medicine['terpene'];
		
			/*
			* Fixed Array (!) exclamation mark By Hassan on 02-June-2016
			*/
			if(!array_empty($terpenes_raw) && is_array($terpenes_raw)){
				foreach ($terpenes_raw as $terpene) {
					$terpen_part = strtolower($terpene);
					$terpen_part = str_replace("-", "_", $terpen_part);
					$terpen_part = str_replace(" ", "_", $terpen_part);
					$products_args_arr['meta_query'][] = array(
					  'key' => "_azcl_{$terpen_part}_tests",
					  'value' => '',
					  'compare' => '!='
					);
				}
			}
       
			if(!empty($terpenes_raw) && !is_array($terpenes_raw)){
				  $terpen_part = strtolower($terpenes_raw);
				  $terpen_part = str_replace("-", "_", $terpen_part);
				  $terpen_part = str_replace(" ", "_", $terpen_part);
				  $products_args_arr['meta_query'][] = array(
						'key' => "_azcl_{$terpen_part}_tests",
						'value' => '',
						'compare' => '!='
				  );
			}

			/**
			 * 5. IF ADD or ADHD is “diagnosis”, AND no terpenes are selected, THEN choose 4a (Sativa)*
			 */
			$user_intek_id = get_user_meta($patient_id, 'patient_intake', true);
			$diagnosis_patient = get_field('principal_primary_diagnosis', $user_intek_id);

			if($diagnosis_patient == 'ADD' || $diagnosis_patient == 'ADHD' && !empty($terpenes_raw)){
				$terpenes_to_look = array('alpha_pinene', 'beta_pinene', 'limonene', 'daytime', 'awake', 'uplifting', 'stimulating');
				foreach ($terpenes_to_look as $terpene) {
					$products_args_arr['meta_query'][] = array(
						'key' => "_azcl_{$terpene}_tests",
						'value' => '',
						'compare' => '!='
					);
				}
			}

			/**
			 * 6.  IF “insomnia” is “diagnosis”, AND no terpenes are selected, THEN choose 4b (Indica)*
			 */
			if ($diagnosis_patient == 'insomnia' && !array_empty($terpenes_raw)){
				$terpenes_to_look = array('myrcene', 'linalool', 'sleepy', 'bedtime', 'nighttime', 'calming');
				foreach ($terpenes_to_look as $terpene) {
					$products_args_arr['meta_query'][] = array(
					'key' => "_azcl_{$terpene}_tests",
					'value' => '',
					'compare' => '!='
					);
				}
			}

			/**
			 * 4
			 * a. IF term ‘Sativa” is used in description, AND terpenes are NOT available, THEN return with the following prescription terpenes: 
			 * i.  Alpha pinene
			 * ii. Beta pinene
			 * iii.  Limonene
			 * iv. Daytime
			 * v.  Awake
			 * vi. Uplifting
			 * vii.  Stimulating
			 */
			if (contains($medicine['notes'], 'Sativa') !== FALSE && array_empty($terpenes_raw) ){
			  $terpenes_to_look = array('alpha_pinene', 'beta_pinene', 'limonene', 'daytime', 'awake', 'uplifting', 'stimulating');
			  foreach ($terpenes_to_look as $terpene) {
					$products_args_arr['meta_query'][] = array(
						'key' => "_azcl_{$terpene}_tests",
						'value' => '',
						'compare' => '!='
					);
				}
			}

			/**
			 * 4
			 * b. IF “Indica” is used in description, AND terpenes are NOT available, THEN return with the following prescription terpenes: 
			 * i.  Myrcene
			 * ii. Linalool
			 * iii.Sleepy
			 * iv. Bedtime
			 * v.  Nighttime
			 * vi. Calming
			 */
			if (contains($medicine['notes'], 'Indica') !== FALSE && array_empty($terpenes_raw)){
				$terpenes_to_look = array('myrcene', 'linalool', 'sleepy', 'bedtime', 'nighttime', 'calming');
				foreach ($terpenes_to_look as $terpene) {
					$products_args_arr['meta_query'][] = array(
						'key' => "_azcl_{$terpene}_tests",
						'value' => '',
						'compare' => '!='
					);
				}
			}
        
			if(count($products_args_arr['meta_query']) > 1){
				$products_args_arr['meta_query']['relation'] = 'AND';
			}
			//print_r($products_args_arr);
			//$products = get_posts($products_args_arr);
			$medicine_number_args[$medicine_number] = $products_args_arr;

		} // End foreeach
		?>
	</div>
	
	<style type="text/css">
	.modal-backdrop.fade.in{ display: none; }
	.modal.fade.in { background: rgba(0, 0, 0, 0.61) none repeat scroll 0 0; }
	</style>
	<?php //if($products){ ?>
   <a href="#" class="btn bordered" data-target="#ProductsModal" data-toggle="modal">Select Products</a>
	<?php //} ?>
	<div class="modal fade" id="ProductsModal" role="dialog" >
		<div class="modal-dialog multi-select-modal navypurple modal-lg">
            <!-- Modal content-->
            <div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close fa fa-close" data-dismiss="modal"></button>
				</div>

                <?php if($medicine_number_args){
                    $step_number = 0;
                    foreach ($medicine_number_args as $medicine_numer => $medicine_args) { 
                        $number_eng = array('first', 'second', 'third', 'fourth');
                        $retVal = ($step_number > 0) ? 'Now' : 'First' ; ?>
                        <div <?php if($step_number > 0){ ?> style="display: none;" <?php } ?> data-step="<?php echo $step_number; ?>" 
                        class="modal-contendddt cdrmed-step step_<?php echo $step_number; ?> medicine-steps">
                            <div id="heading-text_<?php echo $step_number; ?>" class="cdrm-header">
                                <h4 data-cannabinoid="<?php echo $shoop_data[$medicine_numer-1]['cannabinoid']; ?>">
                                <?php echo $retVal; ?>, select a product to match your <?php echo $number_eng[$step_number] ?> target cannabinoid of <?php echo $shoop_data[$medicine_numer-1]['cannabinoid']; ?></h4>
                            </div>
                            <div class="cdrmed_modal_slider">
								<?php 
                                $products = get_posts($medicine_args);
                                if($products){
									$next_secondry_medicine = '';
									foreach ($products as $product) { 
										$product_id = $product->ID;
										$shoop_id = $latest_shoop_id;
										$prod_name = get_the_title( $product_id );

										$product_url = get_permalink($product_id);
										$see_full_profile_link = '<a href='.$product_url.'>See Full Profile</a>';
										//print_r($shoop_data[$medicine_numer-1]);
										// get medicine target dosage
										$target_dosage_arr = explode('X', $shoop_data[$medicine_numer-1]['target_dosage']);
										$target_dosage = $target_dosage_arr[0];

										// Number of Doses per day.
										$number_of_doses_arr = explode('X', $shoop_data[$medicine_numer-1]['frequency']);
										$number_of_doses = $number_of_doses_arr[0];

										preg_match_all("/\(([^)]+)\)/", $shoop_data[$medicine_numer-1]['cannabinoid'], $matches);
										$primary_cannabinoid = strtolower($matches[1][0]);
										$primary_cannabinoid_percent_value = get_post_meta( $product_id, '_azcl_'.$primary_cannabinoid, true );
										$primary_cannabinoid_value = get_post_meta( $product_id, '_azcl_'.$primary_cannabinoid.'_tests', true );
										$secondary_cannabinoid = '';

										if($primary_cannabinoid == 'thc'){ 
										  $secondary_cannabinoid = 'cbd';
										}else{
										  $secondary_cannabinoid = 'thc';
										}

										/**
										 * IF THERE IS MORE THAN ONE MEDICINE BEING RECOMMENDED, THE PRIMARY CANNABINOID IN BOTH MEDICINES NEEDS TO BE THE SECONDARY CANNABINOID IN THE OTHER.
										 * For example, recommended primary cannabinoid1 is THC, and primary cannabinoid2 is THCA, then BOTH medicines would need to account for both values from lab reports.
										 */
										// if(!empty($next_secondry_medicine)){
										//   $secondary_cannabinoid = $next_secondry_medicine;
										// }

										// secondary cannabinoid value prom product
										$secondary_cannabinoid_percent_value = get_post_meta( $product_id, '_azcl_'.$secondary_cannabinoid, true );
										$secondary_cannabinoid_value = get_post_meta( $product_id, '_azcl_'.$secondary_cannabinoid.'_tests', true );
										
										$frequency_always = $shoop_data[0]['frequency'];	// Number of Doses Each with Daily or Hourly Etc
										$extracted_frequency = preg_replace("/[^0-9]/","",$frequency_always);  // This is number of doses per day from SHOOP extracted number only
										
										//echo 'This is SECOND Cannabinoid '.$secondary_cannabinoid;
										//echo '<br>This is PRIMARY Cannabinoid % value '.$primary_cannabinoid_percent_value;
										//echo '<br>This is SECOND Cannabinoid % value '.$secondary_cannabinoid_percent_value;
										//echo '<br>This is SECOND Cannabinoid value '.$secondary_cannabinoid_value;

										// v1 = Take Target Dosage Value and Divide it by 1000. GOT: 0.1
										$v1 =  $target_dosage / 1000;

										// v2 = Take Secondary Cannabinoid Percentage Value and Divide it by 100. got: 0.126
										$v2 = $primary_cannabinoid_percent_value / 100;
										
										// Now divide v1 by v2 and you will have This Much Extract Daily Value.
										$v3 = $v1 / $v2;
										$tmed = round($v3,2);
										//echo 'USE THIS MUCH EXTRACT DAILY '.round($v3,2);
										// v3(this much extract daily) should be divided by Number of Doses per day.
										$epd = $v3 / $number_of_doses;
										
										// This Much Per Dose
										$tmdpd = $tmed/$extracted_frequency;
										$dose_per_day_round = round($tmdpd,2);
										
										//var_dump($secondary_cannabinoid_value);
										/**
										 * To calculate Amount Primary Cannabinoid(mg) per day:
										 * h1 = Primary Cannabinoid % Value from Product divided by 100.
										 */
										$h1 = $primary_cannabinoid_value / 100;

										// h2 = h1 multiplied by v3(this much extract daily).
										$h2 = $h1 * $v3;

										// h3 = h2 multiplies by 1000
										$h3 = $h2 * 1000;
										
										
										/**
										 * To calculate Amount Secondary Cannabinoid(mg) per day:
										 * h1 = Primary Cannabinoid % Value from Product divided by 100.
										 */
										$hg1 = $secondary_cannabinoid_percent_value / 100;

										// h2 = h1 multiplied by v3(this much extract daily).
										$hg2 = $hg1 * $tmed;

										// h3 = h2 multiplies by 1000
										$hg3 = $hg2 * 1000;
										
										$next_secondry_medicine = $primary_cannabinoid;

										$product_json = array(
											'p_product_title' => $prod_name,
											'p_cannabinoid' => strtoupper($primary_cannabinoid), 
											'p_cannabinoid_value' => $primary_cannabinoid_value,
											'p_cannabinoid_percent_value' => $primary_cannabinoid_percent_value,
											's_cannabinoid' => strtoupper($secondary_cannabinoid), 
											's_cannabinoid_value' => $secondary_cannabinoid_value,
											's_cannabinoid_percent_value' => $secondary_cannabinoid_percent_value,
											'this_much_value' => $tmed,
											'dose_per_day' => $dose_per_day_round,
											'secondary_calculations' => $hg3,
											'frequency_per_day' => $frequency_always,
											'full_profile_link' => $see_full_profile_link,
											'calculations' => $h3
										);
										$product_json = json_encode($product_json);
										?>
										<div>
											<div data-product-json='<?php echo $product_json; ?>' data-product-id="<?php echo $product_id; ?>" class="cdrmed_slide_item">
												<?php 
												$thumb_id = get_post_thumbnail_id($product_id);
												$thumb_url_array = wp_get_attachment_image_src($thumb_id, 'medium', true);
												$thumb_url = $thumb_url_array[0];
												?>
												<div class="cdrm_item">
													<img src="<?php echo $thumb_url; ?>" alt="image">
													<button type="button" id="select_cdrm_item" class="select_cdrm_item btn">Select</button>
													<h3 class="cdrm_title"><?php echo $product->post_title; ?></h3>
													<a href="<?php echo get_permalink($product_id); ?>">More info</a>
												</div><!-- end .cdrm_item -->
											</div><!-- end .cdrmed_slide_item -->
										</div><!-- end div holding .cdrmed_slide_item -->
										<!--<a href="<?php echo get_permalink($product->ID); ?>"><?php echo $h3.' :- '.$product->post_title.' -- '.$medicine_number; ?></a>
										<hr />-->
									<?php }
								}
								?>
                            </div><!-- end .cdrmed_modal_slider -->

                            <div class="cdrmed_slid_footer ccdrmf_slider">
								<?php 
								if($products){
									foreach ($products as $product) { 
										?>
										<div>
											<div class="selected_cdrm_items">
												<?php 
												$thumb_id = get_post_thumbnail_id($product_id);
												$thumb_url_array = wp_get_attachment_image_src($thumb_id, 'large', true);
												$thumb_url = $thumb_url_array[0];
												//$thumb_url = str_replace("http://localhost/cdrmed","http://beta2.cdrmed.org/", $thumb_url);
												?>
												<img style="width: 60px;" src="<?php echo $thumb_url; ?>" alt="image">
											</div>
										</div>
									<?php }
								} ?>
                            </div><!-- end .cdrmed_slid_footer -->
                        </div><!-- end .modal-contendddt -->
                      <?php $step_number++;
					}
                }?>
                      
				<div style="display: none;" class="modal-contendddt cdrmed-step step_<?php echo $step_number++; ?> step-show-selected-products">
					<div class="cdrm-header">
						<h4>Here are your selected products. <br />Click the button below to calculate your daily dosing guide.</h4>
					</div>
					<div id="selected_products_markup" class="step_2_items"> </div>
					<div class="buttons">
						<button style="width:auto" id="calculate-daily-dosage" type="button">Calculate my daily dose</button>
					</div>
					<div id="selected_products_markup_thumb" class="cdrmed_slid_footer"></div>
				</div><!-- end .modal-content -->
					
				<div style="display: none;" class="modal-contendddt cdrmed-step step_<?php echo $step_number; ?> step-view-therapy">
					<div class="cdrm-header">
						<h4>Here's your daily dose. Click the button below to save this therapy.</h4>
						<div class="buttons">
							<button data-details="<?php echo $latest_shoop_id.',',$patient_id; ?>" id="save-therapy-btn2" type="button">save therapy</button>
							<img id="save-therapy-loader" src="<?php echo CDRMED ?>/includes/assets/img/ajax-loader.png" style="width: 20px; display: none;">
						</div>
					</div>
					<div id="calucations_items" class="step_3_slider"></div><!-- end step_3slider -->
					<div id="calucations_items_thumbs" class="cdrmed_slid_footer"></div>
				</div>
                      
            </div>
				
		</div>
    </div>
  <?php
  $data_output = ob_get_contents();
  ob_end_clean();
  return $data_output;
}
add_shortcode( 'cdrmed_shoop_patient_products2', 'cdrmed_shoop_patient_products2');