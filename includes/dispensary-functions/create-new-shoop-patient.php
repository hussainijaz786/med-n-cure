<?php
if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly 
if (!session_id()) {
    session_start();
}
function create_new_shoop_dispensar_form(){
	global $wpdb;
	
	$html = '';

	if(isset($_POST['submit'])){
		/*  echo "<pre/>";
		print_r($_POST);exit();*/
		if(isset($_GET['edit'])){ // If edit form submitted
			global $wpdb;
			$postid = $_POST['post_id'];
			update_post_meta($postid, 'primary_dosage',$_POST['primary_dosage']);
			update_post_meta($postid,'daily_dosage'  ,$_POST['daily_dosage']." X ".$_POST['daily_dosage_mg']);
			update_post_meta($postid,'frequency' ,$_POST['frequency']." X ".$_POST['primary_daily_frequency']);
			update_post_meta($postid, 'secornday_cann',$_POST['secornday_cann']);
			update_post_meta($postid,'secornday_daily_dosage'  ,$_POST['secornday_daily_dosage']." X ".$_POST['secornday_dosage_mg']);
			update_post_meta($postid,'secornday_frequency' ,$_POST['secornday_frequency']." X ".$_POST['secornday_daily']);
			update_post_meta($postid, 'terpene',  $_POST['terpene']);
			update_post_meta($postid, 'ingestion_method', $_POST['ingestion_method']);
			//update_post_meta($postid, 'notes', $_POST['notes']);
			if(isset($_POST['cdrmed_shoop_date']) && !empty($_POST['cdrmed_shoop_date'])){
				$date_d = strtotime($_POST['cdrmed_shoop_date']);
				$post_date = date('Y-m-d H:i:s', $date_d);
				$wpdb->update( 
					'wp_posts', 
					array( 
						'post_date' => $post_date,  // string
						'post_date_gmt' => $post_date // integer (number) 
					), 
					array( 'ID' => $postid )
				);
			}
			
			$_SESSION["ses_id"] = "<h2 id='successMessage'><center>Medicines updated Successfully</center></h2>";

			$update = 2;
			$postid = '';
			$urlofpage = $url.'/new-patient-created/';
			header('Location:'.$urlofpage.'?'.$_SERVER['QUERY_STRING']);
			//echo '<meta http-equiv="refresh" content="1; url='.$urlofpage.'?'.$_SERVER['QUERY_STRING'].'" />';
			exit;
		}else{
			$i = 0;
			foreach ($_POST['primary_dosage'] as  $row) {
				if($row  != 'Select Target Cannabinoid...'  && $_POST['daily_dosage'][$i] !=''){
					if($i == 0){
						$user_id = '';
						if(isset($_GET['pid'])){
							$user_id = $_GET['pid'];
						}else{
							$user_id = get_current_user_id();
						}
						$my_post = array(
							'post_type'  => 'strain_shoops',
							'post_title' => 'Strain Shoops Dosage',
							'post_content' => $row,
							'post_status' => 'private',
							'comment_status' => 'closed',   // if you prefer
							'ping_status' => 'closed',
							'post_author'  => $user_id
						);
						if(isset($_POST['cdrmed_shoop_date']) && !empty($_POST['cdrmed_shoop_date'])){
							$date_d = strtotime($_POST['cdrmed_shoop_date']);
							$post_date = date('Y-m-d H:i:s', $date_d);
							$my_post['post_date'] = $post_date;
						}

						// Insert the post into the database
						$postid = wp_insert_post( $my_post );
					}
					if ($postid) {  
						// insert post meta
						add_post_meta($postid, 'medicine'.($i+1).'_cannabinoid_1',$row);
						add_post_meta($postid,'medicine'.($i+1).'_target_dosage_1',$_POST['daily_dosage'][$i]." X ".$_POST['daily_dosage_mg'][$i]);
						add_post_meta($postid,'medicine'.($i+1).'_frequency_1' ,$_POST['frequency'][$i]." X ".$_POST['primary_daily_frequency'][$i]);
						add_post_meta($postid, 'medicine'.($i+1).'_cannabinoid_2',$_POST['secornday_cann'][$i]);
						add_post_meta($postid,'medicine'.($i+1).'_dosage_frequency'.($i+1)  ,@implode(",",$_POST['daily_dosage_frequncy'.($i+1)]));
						add_post_meta($postid,'medicine'.($i+1).'_dosage_mg'.($i+1) ,@implode(",",$_POST['frequency_daily_dosage'.($i+1)]));
						add_post_meta($postid,'medicine'.($i+1).'_dosage_timing'.($i+1) ,@implode(",",$_POST['primary_dosage_timing'.($i+1)]));
						/*add_post_meta($postid,'medicine'.($i+1).'_target_dosage_2'  ,$_POST['secornday_daily_dosage'][$i]." X ".$_POST['secornday_dosage_mg'][$i]);
						add_post_meta($postid,'medicine'.($i+1).'_frequency_2' ,$_POST['secornday_frequency'][$i]." X ".$_POST['secornday_daily'][$i]);*/
						add_post_meta($postid, 'medicine'.($i+1).'_terpene',  $_POST['terpene'][$i]);
						add_post_meta($postid, 'medicine'.($i+1).'_ingestion_method', $_POST['ingestion_method'][$i]);
						add_post_meta($postid, 'medicine_notes'.($i+1), $_POST['notes'][$i]);
						add_post_meta($postid, 'patient_id', $_POST['patient_id']);
						update_post_meta($postid, 'physician_id', get_current_user_id());
					} //if post inserted
				} // else condition ends here
				$i++;
			}// Check if primary cannibonide not empty
			$_SESSION["ses_id"] = "<h2 id='successMessage'><center>Medicines added Successfully</center></h2>";
			do_action('cdrmed_shoop_added', $postid);
			$urlofpage = site_url().'/new-patient-created';
			header('Location:'.$urlofpage.'?pid='.$_GET['pid'].'&post_id='.$postid);
			//echo '<meta http-equiv="refresh" content="1; url='.$urlofpage.'?pid='.$_GET['pid'].'&post_id='.$postid.'" />';
			/* wp_redirect($urlofpage.'?pid='.$_GET['pid'].'&post_id='.$postid);
			die();*/
			exit;
		}
	}

    $lst = '';
        
	$arr = array('Inhaled','Ingested (edible)','Sublingual','Suppository','Topical');

	foreach ($arr as $value) { 
		// $value = strtolower($value);
		$lst .="<option " .(($ingestion_method==$value)? ' selected="selected"' : '')."value='".$value."'>$value</option>";
	}
	if($_SESSION["ses_id"]){
		$html .= '<div class="alert alert-success">'.$_SESSION["ses_id"].'</div>';
		unset($_SESSION["ses_id"]);
	}
	$html .= '   <section class="strains_section_holder">
        <form class="" method="post" id="formId">
            	<div class="container-fluid">
                	<div class="row">
                    	<div class="col-sm-12">
                        	<section class="main-heading">
                            	<h2>Please Complete the following Information and Preview the Patients SHOOP before continuing.<br>
Ingredients are color coded throughout the system from Input to Output of the SHOOP.</h2>
                                  <div class="col-sm-12">
                                  <!--<input type="date" id="cdrmed_shoop_date" class="cdrmed_shoop_date" name="cdrmed_shoop_date" value="" placeholder="mm/dd/yyyy"/>-->
								  <input type="text" id="cdrmed_shoop_date" class="cdrmed_shoop_date" name="cdrmed_shoop_date" value="" placeholder="mm/dd/yyyy"/>
                                  </div>
                            </section>
                        	<section class="strain purple last">
                          <input type="hidden" class="myterpene" name="terpene[]" value="" />
                            	<input type="hidden" name="patient_id" value="'.@$_GET['pid'].'" />
                                	<div class="col-sm-12"><b>Medicine 1</b></div>
                                  <div class="col-sm-12"><b>Target Cannabinoid</b><span class="fa fa-question round-info" data-toggle="tooltip" title="Choose the first cannabinoid to include in this medicine"></span></div>
                                	<div class="col-sm-6">
                                    	<div class="form-group"><div clsss="form-control">
                                    	 <select name="primary_dosage[]" id="primary_dosage" class="selectpicker full-width">
               <option value="">Select Target Cannabinoid...</option>
                <option '.(($primary_dosage=='Tetrahydrocannabinol (THC)')? ' selected="selected"' : '').' value="Tetrahydrocannabinol (THC)">Tetrahydrocannabinol (THC)</option>
               <option '.(($primary_dosage=='Cannabidiol (CBD)')? ' selected="selected"' : '').' value="Cannabidiol (CBD)">Cannabidiol (CBD)</option>
               <option '.(($primary_dosage=='Cannabichromene (CBC)')? ' selected="selected"' : '').' value="Cannabichromene (CBC)">Cannabichromene (CBC)</option>
               <option '.(($primary_dosage=='Cannabichromenic acid (CBCA)')? ' selected="selected"' : '').' value="Cannabichromenic acid (CBCA)">Cannabichromenic acid (CBCA)</option>
               <option '.(($primary_dosage=='Cannabichromivarin (CBCV)')? ' selected="selected"' : '').' value="Cannabichromivarin (CBCV)">Cannabichromivarin (CBCV)</option>
               <option '.(($primary_dosage=='Cannabichromivarin acid (CBCA)')? ' selected="selected"' : '').' value="Cannabichromivarin acid (CBCA)">Cannabichromivarin acid (CBCA)</option>

               <option '.(($primary_dosage=='Cannabidiolic acid (CBDA)')? ' selected="selected"' : '').' value="Cannabidiolic acid (CBDA)">Cannabidiolic acid (CBDA)</option>
               <option '.(($primary_dosage=='Cannabidivarin (CBDV)')? ' selected="selected"' : '').' value="Cannabidivarin (CBDV)">Cannabidivarin (CBDV)</option>
               <option '.(($primary_dosage=='Cannabidivarin acid (CBDVA)')? ' selected="selected"' : '').' value="Cannabidivarin acid (CBDVA)">Cannabidivarin acid (CBDVA)</option>
               <option '.(($primary_dosage=='Cannabigerol (CBG)')? ' selected="selected"' : '').' value="Cannabigerol (CBG)">Cannabigerol (CBG)</option>
               <option '.(($primary_dosage=='Cannabigerolic acid (CBGA)')? ' selected="selected"' : '').' value="Cannabigerolic acid (CBGA)">Cannabigerolic acid (CBGA)</option>
               <option '.(($primary_dosage=='Cannabigevarin (CBGV)')? ' selected="selected"' : '').' value="Cannabigevarin (CBGV)">Cannabigevarin (CBGV)</option>
               <option '.(($primary_dosage=='Cannabigevarin acid (CBGVA)')? ' selected="selected"' : '').' value="Cannabigevarin acid (CBGVA)">Cannabigevarin acid (CBGVA)</option>
               <option '.(($primary_dosage=='Cannabinol (CBN)')? ' selected="selected"' : '').' value="Cannabinol (CBN)">Cannabinol (CBN)</option>
               
               <option '.(($primary_dosage=='Tetrahydrocannabinolic acid (THCA)')? ' selected="selected"' : '').' value="Tetrahydrocannabinolic acid (THCA)">Tetrahydrocannabinolic acid (THCA)</option>
               <option '.(($primary_dosage=='Tetrahydrocannabivarin (THCV)')? ' selected="selected"' : '').' value="Tetrahydrocannabivarin (THCV)">Tetrahydrocannabivarin (THCV)</option>
               <option '.(($primary_dosage=='Tetrahydrocannabivarin acid (THCVA)')? ' selected="selected"' : '').' value="Tetrahydrocannabivarin acid (THCVA)">Tetrahydrocannabivarin acid (THCVA)</option>
               </select>
                                        </div></div>
                                    </div>
                                    <div class="col-sm-6">
                                    	<a href="#" class="btn bordered" data-toggle="modal" data-target="#myModal1">Select TERPENES</a>
                                        <span class="fa fa-question round-info" data-toggle="tooltip" title="Choose the TERPENES you want to include in this medicine"></span><div class="selected-terpens11" style="display:none;"><b>Selected TERPENES:</b><span></span></div>
                                    </div>
                                    
                                    <div class="clearfix"></div>
                                    
                                    <div class="col-sm-6">
                                    	<div class="form-group">
                                        	<label>Target Daily Dose</label>
                                            <span class="fa fa-question round-info" data-toggle="tooltip" title="Target Daily Dose"></span>
										</div>
                                    	<div class="form-group">
                                        	<input name="daily_dosage[]" id="daily_dosage" type="text" value="'.trim(@$daily_dosage[0]).'" class="two-third form-control" tabindex="2">
                                            <select class="selectpicker one-third" name="daily_dosage_mg[]">
                                                <option value="mg">mg</option>
                                                 <option value="g">g</option>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="col-sm-6">
                                    	<div class="form-group">
                                        	<label>Frequency</label>
                                            <span class="fa fa-question round-info" data-toggle="tooltip" title="Enter the frequency for the medicine, i.e., two times daily"></span>
										</div>
                                    	<div class="form-group">
                                      <select name="frequency[]" id="frequency1" class="selectpicker two-third">
                                           <option value="">Select Frequency...</option>
                                           <option value="1">1</option>
                                           <option value="2">2</option>
                                           <option value="3">3</option>
                                           <option value="4">4</option>
                                           <option value="5">5</option>
                                           <option value="6">6</option>
                                           <option value="7">7</option>
                                           <option value="8">8</option>
                                           <option value="9">9</option>
                                           <option value="10">10</option>
                                           <option value="11">11</option>
                                           <option value="12">12</option>
                                           <option value="13">13</option>
                                           <option value="14">14</option>
                                           <option value="15">15</option>
                                           </select>
                                      <span class="in-to">x</span>
                                            <select class="selectpicker one-third frequency1" name="primary_daily_frequency[]">
                                                  <option value="">-Select-</option><option value="daily">daily</option>
                                                <option value="hourly">hourly</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="new">
                                    </div>
                                    <div class="clearfix"></div>
                 
                                    <div class="col-sm-6">
                                    	<div class="form-group">
                                        	<label>Ingestion Method</label>
                                            <span class="fa fa-question round-info" data-toggle="tooltip" title="Select the ingestion method you want the patient to use"></span>
										</div>
                                    	<div class="form-group">
                                          <select name="ingestion_method[]" class="selectpicker full-width" id="ingestion_method">
						               		<option value="Select Ingestion Method...">Select Ingestion Method...</option>
						                '.$lst.'
						               </select>
                                        </div>
                                    </div>

                        <div class="col-sm-6">
                                      <div class="form-group">
                                          <label>Notes</label>
                                            <span class="fa fa-question round-info" data-toggle="tooltip" title="Notes"></span>
                    </div>
                                      <div class="form-group">
                                          <textarea name="notes[]" id="notes" class="full-width notes form-control"></textarea>
                                        </div>
                                    </div> 
                              <div class="clearfix"></div>
                                    <div class="col-md-6 col-sm-12 ">
                                    	<div class="form-buttons">
                                            <a href="javascript:void(0)" class="btn bordered with-icon" id="purple_add" onClick="showpurple();"><span class="fa fa-plus-circle"></span>Add Medicine</a>
                                            <span class="fa fa-question round-info" data-toggle="tooltip" title="Add medicine to SHOOP Panel"></span>
                                          
										</div>
                                    </div>
                                    
                              
                            </section>
                            
                            <section class="strain reded" style="display:none;">
                            	<div class="col-sm-12"><b>Medicine 2</b></span></div>
                                <div class="col-sm-12"><b>Target Cannabinoid</b><span class="fa fa-question round-info" data-toggle="tooltip" title="Choose the first cannabinoid to include in this medicine"></span></div>
                                  <div class="col-sm-6">
                                      <div class="form-group">
                                      <input type="hidden" class="myterpene2" name="terpene[]" value="" /><div clsss="form-control">
                                       <select name="primary_dosage[]" id="primary_dosage" class="selectpicker full-width">
               <option value="">Select Target Cannabinoid...</option>
                <option '.(($primary_dosage=='Tetrahydrocannabinol (THC)')? ' selected="selected"' : '').' value="Tetrahydrocannabinol (THC)">Tetrahydrocannabinol (THC)</option>
               <option '.(($primary_dosage=='Cannabidiol (CBD)')? ' selected="selected"' : '').' value="Cannabidiol (CBD)">Cannabidiol (CBD)</option>
               <option '.(($primary_dosage=='Cannabichromene (CBC)')? ' selected="selected"' : '').' value="Cannabichromene (CBC)">Cannabichromene (CBC)</option>
               <option '.(($primary_dosage=='Cannabichromenic acid (CBCA)')? ' selected="selected"' : '').' value="Cannabichromenic acid (CBCA)">Cannabichromenic acid (CBCA)</option>
               <option '.(($primary_dosage=='Cannabichromivarin (CBCV)')? ' selected="selected"' : '').' value="Cannabichromivarin (CBCV)">Cannabichromivarin (CBCV)</option>
               <option '.(($primary_dosage=='Cannabichromivarin acid (CBCA)')? ' selected="selected"' : '').' value="Cannabichromivarin acid (CBCA)">Cannabichromivarin acid (CBCA)</option>
               <option '.(($primary_dosage=='Cannabidiolic acid (CBDA)')? ' selected="selected"' : '').' value="Cannabidiolic acid (CBDA)">Cannabidiolic acid (CBDA)</option>
               <option '.(($primary_dosage=='Cannabidivarin (CBDV)')? ' selected="selected"' : '').' value="Cannabidivarin (CBDV)">Cannabidivarin (CBDV)</option>
               <option '.(($primary_dosage=='Cannabidivarin acid (CBDVA)')? ' selected="selected"' : '').' value="Cannabidivarin acid (CBDVA)">Cannabidivarin acid (CBDVA)</option>
               <option '.(($primary_dosage=='Cannabigerol (CBG)')? ' selected="selected"' : '').' value="Cannabigerol (CBG)">Cannabigerol (CBG)</option>
               <option '.(($primary_dosage=='Cannabigerolic acid (CBGA)')? ' selected="selected"' : '').' value="Cannabigerolic acid (CBGA)">Cannabigerolic acid (CBGA)</option>
               <option '.(($primary_dosage=='Cannabigevarin (CBGV)')? ' selected="selected"' : '').' value="Cannabigevarin (CBGV)">Cannabigevarin (CBGV)</option>
               <option '.(($primary_dosage=='Cannabigevarin acid (CBGVA)')? ' selected="selected"' : '').' value="Cannabigevarin acid (CBGVA)">Cannabigevarin acid (CBGVA)</option>
               <option '.(($primary_dosage=='Cannabinol (CBN)')? ' selected="selected"' : '').' value="Cannabinol (CBN)">Cannabinol (CBN)</option>
               <option '.(($primary_dosage=='Tetrahydrocannabinolic acid (THCA)')? ' selected="selected"' : '').' value="Tetrahydrocannabinolic acid (THCA)">Tetrahydrocannabinolic acid (THCA)</option>
               <option '.(($primary_dosage=='Tetrahydrocannabivarin (THCV)')? ' selected="selected"' : '').' value="Tetrahydrocannabivarin (THCV)">Tetrahydrocannabivarin (THCV)</option>
               <option '.(($primary_dosage=='Tetrahydrocannabivarin acid (THCVA)')? ' selected="selected"' : '').' value="Tetrahydrocannabivarin acid (THCVA)">Tetrahydrocannabivarin acid (THCVA)</option>
               </select></div>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                      <a href="#" class="btn bordered" data-toggle="modal" data-target="#myModal2">Select TERPENES</a>
                                        <span class="fa fa-question round-info" data-toggle="tooltip" title="Choose the TERPENES you want to include in this medicine"></span><div class="selected-terpens12" style="display:none;"><b>Selected TERPENES:</b><span></span></div>
                                    </div>
                                    
                                    <div class="clearfix"></div>
                                    
                                    <div class="col-sm-6">
                                      <div class="form-group">
                                          <label>Target Daily Dose</label>
                                            <span class="fa fa-question round-info" data-toggle="tooltip" title="Target Daily Dose"></span>
                    </div>
                                      <div class="form-group">
                                          <input name="daily_dosage[]" id="daily_dosage" type="text" value="'.trim(@$daily_dosage[0]).'" class="two-third form-control" tabindex="2">
                                            <select class="selectpicker one-third" name="daily_dosage_mg[]">
                                                 <option value="mg">mg</option>
                                                 <option value="g">g</option>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="col-sm-6">
                                      <div class="form-group">
                                          <label>Frequency</label>
                                            <span class="fa fa-question round-info" data-toggle="tooltip" title="Enter the frequency for the medicine, i.e., two times daily"></span>
                    </div>
                                      <div class="form-group">
									  <div clsss="form-control">
                                            <select name="frequency[]" id="frequency2" class="selectpicker two-third">
               <option value="">Select Frequency...</option>
               <option value="1">1</option>
               <option value="2">2</option>
               <option value="3">3</option>
               <option value="4">4</option>
               <option value="5">5</option>
               <option value="6">6</option>
               <option value="7">7</option>
               <option value="8">8</option>
               <option value="9">9</option>
               <option value="10">10</option>
               <option value="11">11</option>
               <option value="12">12</option>
               <option value="13">13</option>
               <option value="14">14</option>
               <option value="15">15</option>
               </select>  <span class="in-to">x</span>
                                            <select class="selectpicker one-third frequency2" name="primary_daily_frequency[]">
                                                 <option value="">-Select-</option><option value="daily">daily</option>
                                                <option value="hourly">hourly</option>
                                            </select>
                                        </div>
										</div>
                                    </div>
                                  <div class="new2">
                                    </div>  
                                    <div class="clearfix"></div>
                       
                                    <div class="col-sm-6">
                                      <div class="form-group">
                                          <label>Ingestion Method</label>
                                            <span class="fa fa-question round-info" data-toggle="tooltip" title="Select the ingestion method you want the patient to use"></span>
                    </div>
                                      <div class="form-group">
                                          <select name="ingestion_method[]" class="selectpicker full-width" id="ingestion_method">
                              <option value="Select Ingestion Method...">Select Ingestion Method...</option>
                            '.$lst.'
                           </select>
                                        </div>
                                    </div>
                     <div class="col-sm-6">
                                      <div class="form-group">
                                          <label>Notes</label>
                                            <span class="fa fa-question round-info" data-toggle="tooltip" title="Notes"></span>
                    </div>
                                      <div class="form-group">
                                          <textarea name="notes[]" id="notes" class="full-width notes form-control"></textarea>
                                        </div>
                                    </div>                  
                         <div class="clearfix"></div> 
                                    <div class="col-md-6 col-sm-12 ">
                                      <div class="form-buttons">
                                            <a href="javascript:void(0)" class="btn bordered with-icon" id="red_add" onClick="red_add();"><span class="fa fa-plus-circle"></span>Add Medicine</a>
                                            <span class="fa fa-question round-info" data-toggle="tooltip" title="Add medicine to SHOOP Panel"></span>
                                            <a href="javascript:void(0)" class="btn bordered with-icon" id="red_remove" onClick="red_remove();"><span class="fa fa-minus-circle"></span>Remove Medicine</a>
                                            <span class="fa fa-question round-info" data-toggle="tooltip" title="Remove medicine from SHOOP Panel"></span>
                    </div>
                                    </div>
                            </section>
                            
                            
                            <section class="strain lightblue" style="display:none;">
                            	<div class="col-sm-12"><b>Medicine 3</b></div>
                               <div class="col-sm-12"><b>Target Cannabinoid</b><span class="fa fa-question round-info" data-toggle="tooltip" title="Choose the first cannabinoid to include in this medicine"></span></div>
                                    <div class="col-sm-6">
                                      <div class="form-group">
                                      <input type="hidden" class="myterpene3" name="terpene[]" value="" />
                                       <select name="primary_dosage[]" id="primary_dosage" class="selectpicker full-width">
               <option value="">Select Target Cannabinoid...</option>
                <option '.(($primary_dosage=='Tetrahydrocannabinol (THC)')? ' selected="selected"' : '').' value="Tetrahydrocannabinol (THC)">Tetrahydrocannabinol (THC)</option>
               <option '.(($primary_dosage=='Cannabidiol (CBD)')? ' selected="selected"' : '').' value="Cannabidiol (CBD)">Cannabidiol (CBD)</option>
               <option '.(($primary_dosage=='Cannabichromene (CBC)')? ' selected="selected"' : '').' value="Cannabichromene (CBC)">Cannabichromene (CBC)</option>
               <option '.(($primary_dosage=='Cannabichromenic acid (CBCA)')? ' selected="selected"' : '').' value="Cannabichromenic acid (CBCA)">Cannabichromenic acid (CBCA)</option>
               <option '.(($primary_dosage=='Cannabichromivarin (CBCV)')? ' selected="selected"' : '').' value="Cannabichromivarin (CBCV)">Cannabichromivarin (CBCV)</option>
               <option '.(($primary_dosage=='Cannabichromivarin acid (CBCA)')? ' selected="selected"' : '').' value="Cannabichromivarin acid (CBCA)">Cannabichromivarin acid (CBCA)</option>
               <option '.(($primary_dosage=='Cannabidiolic acid (CBDA)')? ' selected="selected"' : '').' value="Cannabidiolic acid (CBDA)">Cannabidiolic acid (CBDA)</option>
               <option '.(($primary_dosage=='Cannabidivarin (CBDV)')? ' selected="selected"' : '').' value="Cannabidivarin (CBDV)">Cannabidivarin (CBDV)</option>
               <option '.(($primary_dosage=='Cannabidivarin acid (CBDVA)')? ' selected="selected"' : '').' value="Cannabidivarin acid (CBDVA)">Cannabidivarin acid (CBDVA)</option>
               <option '.(($primary_dosage=='Cannabigerol (CBG)')? ' selected="selected"' : '').' value="Cannabigerol (CBG)">Cannabigerol (CBG)</option>
               <option '.(($primary_dosage=='Cannabigerolic acid (CBGA)')? ' selected="selected"' : '').' value="Cannabigerolic acid (CBGA)">Cannabigerolic acid (CBGA)</option>
               <option '.(($primary_dosage=='Cannabigevarin (CBGV)')? ' selected="selected"' : '').' value="Cannabigevarin (CBGV)">Cannabigevarin (CBGV)</option>
               <option '.(($primary_dosage=='Cannabigevarin acid (CBGVA)')? ' selected="selected"' : '').' value="Cannabigevarin acid (CBGVA)">Cannabigevarin acid (CBGVA)</option>
               <option '.(($primary_dosage=='Cannabinol (CBN)')? ' selected="selected"' : '').' value="Cannabinol (CBN)">Cannabinol (CBN)</option>
               <option '.(($primary_dosage=='Tetrahydrocannabinolic acid (THCA)')? ' selected="selected"' : '').' value="Tetrahydrocannabinolic acid (THCA)">Tetrahydrocannabinolic acid (THCA)</option>
               <option '.(($primary_dosage=='Tetrahydrocannabivarin (THCV)')? ' selected="selected"' : '').' value="Tetrahydrocannabivarin (THCV)">Tetrahydrocannabivarin (THCV)</option>
               <option '.(($primary_dosage=='Tetrahydrocannabivarin acid (THCVA)')? ' selected="selected"' : '').' value="Tetrahydrocannabivarin acid (THCVA)">Tetrahydrocannabivarin acid (THCVA)</option>
               </select>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                      <a href="#" class="btn bordered" data-toggle="modal" data-target="#myModal3">Select TERPENES</a>
                                        <span class="fa fa-question round-info" data-toggle="tooltip" title="Choose the TERPENES you want to include in this medicine"></span><div class="selected-terpens13" style="display:none;"><b>Selected TERPENES:</b><span></span></div>
                                    </div>
                                    
                                    <div class="clearfix"></div>
                                    
                                    <div class="col-sm-6">
                                      <div class="form-group">
                                          <label>Target Daily Dose</label>
                                            <span class="fa fa-question round-info" data-toggle="tooltip" title="Target Daily Dose"></span>
                    </div>
                                      <div class="form-group">
                                          <input name="daily_dosage[]" id="daily_dosage" type="text" value="'.trim(@$daily_dosage[0]).'" class="two-third form-control" tabindex="2">
                                            <select class="selectpicker one-third" name="daily_dosage_mg[]">
                                                  <option value="mg">mg</option>
                                                 <option value="g">g</option>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="col-sm-6">
                                      <div class="form-group">
                                          <label>Frequency</label>
                                            <span class="fa fa-question round-info" data-toggle="tooltip" title="Enter the frequency for the medicine, i.e., two times daily"></span>
                    </div>
                                      <div class="form-group">
                                           <select name="frequency[]" id="frequency3" class="selectpicker two-third">
               <option value="">Select Frequency...</option>
               <option value="1">1</option>
               <option value="2">2</option>
               <option value="3">3</option>
               <option value="4">4</option>
               <option value="5">5</option>
               <option value="6">6</option>
               <option value="7">7</option>
               <option value="8">8</option>
               <option value="9">9</option>
               <option value="10">10</option>
               <option value="11">11</option>
               <option value="12">12</option>
               <option value="13">13</option>
               <option value="14">14</option>
               <option value="15">15</option>
               </select> <span class="in-to">x</span>
                                            <select class="selectpicker frequency3 one-third" name="primary_daily_frequency[]">
                                                   <option value="">-Select-</option><option value="daily">daily</option>
                                                <option value="hourly">hourly</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="new3">
                                    </div>
                                    <div class="clearfix"></div>
             
                                    <div class="col-sm-6">
                                      <div class="form-group">
                                          <label>Ingestion Method</label>
                                            <span class="fa fa-question round-info" data-toggle="tooltip" title="Select the ingestion method you want the patient to use"></span>
                    </div>
                                      <div class="form-group">
                                          <select name="ingestion_method[]" class="selectpicker full-width" id="ingestion_method">
                              <option value="Select Ingestion Method...">Select Ingestion Method...</option>
                            '.$lst.'
                           </select>
                                        </div>
                                    </div>
                    <div class="col-sm-6">
                                      <div class="form-group">
                                          <label>Notes</label>
                                            <span class="fa fa-question round-info" data-toggle="tooltip" title="Notes"></span>
                    </div>
                                      <div class="form-group">
                                          <textarea name="notes[]" id="notes" class="full-width notes form-control"></textarea>
                                        </div>
                                    </div> 
                                     <div class="clearfix"></div>
                                    <div class="col-md-6 col-sm-12 ">
                                      <div class="form-buttons">
                                            <a href="javascript:void(0)" class="btn bordered with-icon" id="lightblue_add" onClick="lightblue_add();"><span class="fa fa-plus-circle"></span>Add Medicine</a>
                                            <span class="fa fa-question round-info" data-toggle="tooltip" title="Add medicine to SHOOP Panel"></span>
                                            <a href="javascript:void(0)" class="btn bordered with-icon" id="lightblue_remove" onClick="lightblue_remove();"><span class="fa fa-minus-circle"></span>Remove Medicine</a>
                                            <span class="fa fa-question round-info" data-toggle="tooltip" title="Remove medicine from SHOOP Panel"></span>
                    </div>
                                    </div>
                            </section>
                          
                            
                            <section class="strain lightgreen" style="display:none;">
                            	<div class="col-sm-12"><b>Medicine 4</b></div>
                               <div class="col-sm-12"><b>Target Cannabinoid</b><span class="fa fa-question round-info" data-toggle="tooltip" title="Choose the first cannabinoid to include in this medicine"></span></div>
                                   <div class="col-sm-6">
                                      <div class="form-group">
                                      <input type="hidden" class="myterpene4" name="terpene[]" value="" />
                                       <select name="primary_dosage[]" id="primary_dosage" class="selectpicker full-width">
               <option value="">Select Target Cannabinoid...</option>
                <option '.(($primary_dosage=='Tetrahydrocannabinol (THC)')? ' selected="selected"' : '').' value="Tetrahydrocannabinol (THC)">Tetrahydrocannabinol (THC)</option>
               <option '.(($primary_dosage=='Cannabidiol (CBD)')? ' selected="selected"' : '').' value="Cannabidiol (CBD)">Cannabidiol (CBD)</option>
               <option '.(($primary_dosage=='Cannabichromene (CBC)')? ' selected="selected"' : '').' value="Cannabichromene (CBC)">Cannabichromene (CBC)</option>
               <option '.(($primary_dosage=='Cannabichromenic acid (CBCA)')? ' selected="selected"' : '').' value="Cannabichromenic acid (CBCA)">Cannabichromenic acid (CBCA)</option>
               <option '.(($primary_dosage=='Cannabichromivarin (CBCV)')? ' selected="selected"' : '').' value="Cannabichromivarin (CBCV)">Cannabichromivarin (CBCV)</option>
               <option '.(($primary_dosage=='Cannabichromivarin acid (CBCA)')? ' selected="selected"' : '').' value="Cannabichromivarin acid (CBCA)">Cannabichromivarin acid (CBCA)</option>
               <option '.(($primary_dosage=='Cannabidiolic acid (CBDA)')? ' selected="selected"' : '').' value="Cannabidiolic acid (CBDA)">Cannabidiolic acid (CBDA)</option>
               <option '.(($primary_dosage=='Cannabidivarin (CBDV)')? ' selected="selected"' : '').' value="Cannabidivarin (CBDV)">Cannabidivarin (CBDV)</option>
               <option '.(($primary_dosage=='Cannabidivarin acid (CBDVA)')? ' selected="selected"' : '').' value="Cannabidivarin acid (CBDVA)">Cannabidivarin acid (CBDVA)</option>
               <option '.(($primary_dosage=='Cannabigerol (CBG)')? ' selected="selected"' : '').' value="Cannabigerol (CBG)">Cannabigerol (CBG)</option>
               <option '.(($primary_dosage=='Cannabigerolic acid (CBGA)')? ' selected="selected"' : '').' value="Cannabigerolic acid (CBGA)">Cannabigerolic acid (CBGA)</option>
               <option '.(($primary_dosage=='Cannabigevarin (CBGV)')? ' selected="selected"' : '').' value="Cannabigevarin (CBGV)">Cannabigevarin (CBGV)</option>
               <option '.(($primary_dosage=='Cannabigevarin acid (CBGVA)')? ' selected="selected"' : '').' value="Cannabigevarin acid (CBGVA)">Cannabigevarin acid (CBGVA)</option>
               <option '.(($primary_dosage=='Cannabinol (CBN)')? ' selected="selected"' : '').' value="Cannabinol (CBN)">Cannabinol (CBN)</option>
               <option '.(($primary_dosage=='Tetrahydrocannabinolic acid (THCA)')? ' selected="selected"' : '').' value="Tetrahydrocannabinolic acid (THCA)">Tetrahydrocannabinolic acid (THCA)</option>
               <option '.(($primary_dosage=='Tetrahydrocannabivarin (THCV)')? ' selected="selected"' : '').' value="Tetrahydrocannabivarin (THCV)">Tetrahydrocannabivarin (THCV)</option>
               <option '.(($primary_dosage=='Tetrahydrocannabivarin acid (THCVA)')? ' selected="selected"' : '').' value="Tetrahydrocannabivarin acid (THCVA)">Tetrahydrocannabivarin acid (THCVA)</option>
               </select>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                      <a href="#" class="btn bordered" data-toggle="modal" data-target="#myModal4">Select TERPENES</a>
                                        <span class="fa fa-question round-info" data-toggle="tooltip" title="Choose the TERPENES you want to include in this medicine"></span><div class="selected-terpens14" style="display:none;"><b>Selected TERPENES:</b><span></span></div>
                                    </div>
                                    
                                    <div class="clearfix"></div>
                                    
                                    <div class="col-sm-6">
                                      <div class="form-group">
                                          <label>Target Daily Dose</label>
                                            <span class="fa fa-question round-info" data-toggle="tooltip" title="Target Daily Dose"></span>
                    </div>
                                      <div class="form-group">
                                          <input name="daily_dosage[]" id="daily_dosage" type="text" value="'.trim(@$daily_dosage[0]).'" class="two-third form-control" tabindex="2">
                                            <select class="selectpicker one-third" name="daily_dosage_mg[]">
                                                  <option value="mg">mg</option>
                                                 <option value="g">g</option>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="col-sm-6">
                                      <div class="form-group">
                                          <label>Frequency</label>
                                            <span class="fa fa-question round-info" data-toggle="tooltip" title="Enter the frequency for the medicine, i.e., two times daily"></span>
                    </div>
                                      <div class="form-group">
                                            <select name="frequency[]" id="frequency4" class="selectpicker two-third">
               <option value="">Select Frequency...</option>
               <option value="1">1</option>
               <option value="2">2</option>
               <option value="3">3</option>
               <option value="4">4</option>
               <option value="5">5</option>
               <option value="6">6</option>
               <option value="7">7</option>
               <option value="8">8</option>
               <option value="9">9</option>
               <option value="10">10</option>
               <option value="11">11</option>
               <option value="12">12</option>
               <option value="13">13</option>
               <option value="14">14</option>
               <option value="15">15</option>
               </select><span class="in-to">x</span>
                                            <select class="selectpicker frequency4 one-third" name="primary_daily_frequency[]">
                                                    <option value="">-Select-</option><option value="daily">daily</option>
                                                <option value="hourly">hourly</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="new4">
                                    </div>
                                    <div class="clearfix"></div>
                                    
              
                                    <div class="col-sm-6">
                                      <div class="form-group">
                                          <label>Ingestion Method</label>
                                            <span class="fa fa-question round-info" data-toggle="tooltip" title="Select the ingestion method you want the patient to use"></span>
                    </div>
                                      <div class="form-group">
                                          <select name="ingestion_method[]" class="selectpicker full-width" id="ingestion_method">
                              <option value="Select Ingestion Method...">Select Ingestion Method...</option>
                            '.$lst.'
                           </select>
                                        </div>
                                    </div>
                             <div class="col-sm-6">
                                      <div class="form-group">
                                          <label>Notes</label>
                                            <span class="fa fa-question round-info" data-toggle="tooltip" title="Notes"></span>
                    </div>
                                      <div class="form-group">
                                          <textarea name="notes[]" id="notes" class="full-width notes form-control"></textarea>
                                        </div>
                                    </div>
                         <div class="clearfix"></div> 
                                    <div class="col-md-6 col-sm-12 ">
                                      <div class="form-buttons">
                                            <a href="javascript:void(0)" class="btn bordered with-icon" id="lightgreen_add"><span class="fa fa-plus-circle"></span>Add Medicine</a>
                                            <span class="fa fa-question round-info" data-toggle="tooltip" title="Add medicine to SHOOP Panel"></span>
                                            <a href="javascript:void(0)" class="btn bordered with-icon" id="lightgreen_remove" onClick="lightgreen_remove();"><span class="fa fa-minus-circle"></span>Remove Medicine</a>
                                            <span class="fa fa-question round-info" data-toggle="tooltip" title="Remove medicine from SHOOP Panel"></span>
                    </div>
                                    </div>
                            </section>
                            
                            
                            
                        </div>
                        <div class="clearfix"></div>
                        <div class="col-sm-12">
                        	<div class="text-right form-footer-btns">
                          <button type="submit" onClick="return submitDetailsForm();" name="submit" class="btn preview">Save</button>
                            	<button type="button" class="btn reset">Reset</button>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            </form>';
	// model box code

	return $html;
}// function ends here
add_shortcode( 'create-new-shoop-dispensar', 'create_new_shoop_dispensar_form' );
?>