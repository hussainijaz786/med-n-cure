<?php

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly 


/*
*
*		medical conditions ajax call
*		
*
*/

add_action( 'wp_ajax_educational_medical_condition_ajax', 'educational_medical_condition_ajax');
add_action( 'wp_ajax_nopriv_educational_medical_condition_ajax', 'educational_medical_condition_ajax');

function educational_medical_condition_ajax() {
	
	$patient_id = $_POST['patient_id'];
	$medical_condition = $_POST['medical_condition'];
	die();
}

/*
*
*		Educational topics Load into Modal Box
*		
*
*/

add_action( 'wp_ajax_educational_topics_load_ajax', 'educational_topics_load_ajax');
add_action( 'wp_ajax_nopriv_educational_topics_load_ajax', 'educational_topics_load_ajax');

function educational_topics_load_ajax() {
	
	
	$siteurl = get_site_url();
	$html = '';
	
	$taxonomy = 'educational_medical_condition';
	$orderby = 'name';
	$show_count = 0; // 1 for yes, 0 for no
	$pad_counts   = 0; // 1 for yes, 0 for no 
	$hierarchical = 1;      // 1 for yes, 0 for no  
	$title = ''; 
	$empty = 0;
	
	$args = array( 'taxonomy' => $taxonomy, 'orderby' => $orderby, 'pad_counts'   => $pad_counts, 'title_li'  => $title, 'hide_empty' => $empty
	);
	
	$all_categories = get_categories( $args );
	/* echo '<pre>';
	print_r($all_categories);
	echo '</pre>'; */
	
	$loop_check = null;
	$html_part = '';
	foreach ($all_categories as $cat) {
		$loop_check = true;
		$html_part .= '<li><a href="'.get_term_link($cat->term_id).'" target="_blank">'.$cat->name.'</a></li>';
	}
	
	if($loop_check){
		$html .= '<div class="educational-topic">
			<div class="topic-title"><h2>Medical Conditions</h2></div>
			<ul>'.$html_part.'</ul>
		</div>';
	}
	
	
	
	$taxonomy = 'educational_cannabinoid';
	$args = array( 'taxonomy' => $taxonomy, 'orderby' => $orderby, 'pad_counts'   => $pad_counts, 'title_li'  => $title, 'hide_empty' => $empty
	);
	$all_categories = get_categories( $args );
	$loop_check = null;
	$html_part = '';
	foreach ($all_categories as $cat) {
		$loop_check = true;
		$html_part .= '<li><a href="'.get_term_link($cat->term_id).'" target="_blank">'.$cat->name.'</a></li>';
	}
	
	if($loop_check){
		$html .= '<div class="educational-topic">
		<div class="topic-title"><h2>Cannabinoids</h2></div>
			<ul>'.$html_part.'</ul>
		</div>';
	}
	
	$taxonomy = 'educational_terpenses';
	$args = array( 'taxonomy' => $taxonomy, 'orderby' => $orderby, 'pad_counts'   => $pad_counts, 'title_li'  => $title, 'hide_empty' => $empty
	);
	$all_categories = get_categories( $args );
	$loop_check = null;
	$html_part = '';
	foreach ($all_categories as $cat) {
		$loop_check = true;
		$html_part .= '<li><a href="'.get_term_link($cat->term_id).'" target="_blank">'.$cat->name.'</a></li>';
	}
	
	if($loop_check){
		$html .= '<div class="educational-topic">
		<div class="topic-title"><h2>Terpenses</h2></div>
			<ul>'.$html_part.'</ul>
		</div>';
	}
	
	echo $html;
	die();
}
?>