<?php

// Patient Code

function cdrmed_educational_system() {
	
	

	$labels = array(
		'name'                => _x( 'Educational Topics', 'Post Type General Name', 'wordpress' ),
		'singular_name'       => _x( 'Educational Topic', 'Post Type Singular Name', 'wordpress' ),
		'menu_name'           => __( 'Educational Topics', 'wordpress' ),
		'name_admin_bar'      => __( 'Educational Topic', 'wordpress' ),
	);
	$args = array(
		'label'               => __( 'Educational Topics', 'wordpress' ),
		'description'         => __( 'MednCures Educational Topics', 'wordpress' ),
		'labels'              => $labels,
		'supports'            => array( 'title', 'editor', 'author', 'custom-fields', 'thumbnail'),
		'hierarchical'        => true,
		'public'              => true,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'menu_position'       => 5,
		'menu_icon'           => 'dashicons-welcome-learn-more',
		'show_in_admin_bar'   => true,
		'show_in_nav_menus'   => true,
		'show_in_rest'   => true,
	);
	register_post_type( 'educational', $args );
	
}
add_action( 'init', 'cdrmed_educational_system', 0 );

// Register Custom Taxonomy
function cdrmed_educational_custom_tax() {
	
	register_educational_custom_tax('Medical Conditions', 'Medical Condition', 'Medical Conditions', 'educational_medical_condition');
	register_educational_custom_tax('Cannabinoids', 'Cannabinoid', 'Cannabinoids', 'educational_cannabinoid');
	register_educational_custom_tax('Terpenes', 'Terpenes', 'Terpene', 'educational_terpenses');
	
}
add_action( 'init', 'cdrmed_educational_custom_tax', 0 );


function register_educational_custom_tax($name, $singular_name, $menu_name, $slug){
	
	$labels = array(
		'name'                       => _x( $name, 'Taxonomy General Name', 'wordpress' ),
		'singular_name'              => _x( $singular_name, 'Taxonomy Singular Name', 'wordpress' ),
		'menu_name'                  => __( $menu_name, 'wordpress' ),
		'all_items'                  => __( 'All Items', 'wordpress' ),
		'parent_item'                => __( 'Parent Item', 'wordpress' ),
		'parent_item_colon'          => __( 'Parent Item:', 'wordpress' ),
		'new_item_name'              => __( 'New Item Name', 'wordpress' ),
		'add_new_item'               => __( 'Add New Item', 'wordpress' ),
		'edit_item'                  => __( 'Edit Item', 'wordpress' ),
		'update_item'                => __( 'Update Item', 'wordpress' ),
		'view_item'                  => __( 'View Item', 'wordpress' ),
		'separate_items_with_commas' => __( 'Separate items with commas', 'wordpress' ),
		'add_or_remove_items'        => __( 'Add or remove items', 'wordpress' ),
		'choose_from_most_used'      => __( 'Choose from the most used', 'wordpress' ),
		'popular_items'              => __( 'Popular Items', 'wordpress' ),
		'search_items'               => __( 'Search Items', 'wordpress' ),
		'not_found'                  => __( 'Not Found', 'wordpress' ),
		'no_terms'                   => __( 'No items', 'wordpress' ),
		'items_list'                 => __( 'Items list', 'wordpress' ),
		'items_list_navigation'      => __( 'Items list navigation', 'wordpress' ),
	);
	$args = array(
		'labels'                     => $labels,
		'hierarchical'               => true,
		'public'                     => true,
		'show_ui'                    => true,
		'show_admin_column'          => true,
		'show_in_nav_menus'          => true,
		'show_tagcloud'              => false,
		'rewrite'                    => true,
	);
	register_taxonomy( $slug, array( 'educational' ), $args );
	
}

?>