<?php 

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly 
/* Adds all user meta to the /wp-json/wp/v2/user/[id] endpoint */
function sb_user_meta( $data, $field_name, $request ) {
	if( $data['id'] ){
		$user_meta = get_user_meta( $data['id'] );
	}
	if ( !$user_meta ) {
    return new WP_Error( 'No user meta found', 'No user meta found', array( 'status' => 404 ) );
  }
  foreach ($user_meta as $key => $value) {
		$data[$key] = $value;
	}
	return $data;
}
add_action( 'rest_api_init', function () {
    register_api_field( 'user',
        'meta',
        array(
            'get_callback'    => 'sb_user_meta',
            'update_callback' => null, // add callback here for POST/PUT requests to update user meta
            'schema'          => null,
        )
    );
} );
?>
