<?php 

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly 
// Include Functions for Physician Dashboard

//Diagnosis for admin side.inlcuding List and adding new
include('diagnosis/index-inc.php');
//Medications for admin side.inlcuding List and adding new
include('medications/index-inc.php');
include('export/index-inc.php');
include('add-new-physician.php');
include('add-new-dispensary.php');
include('patient-user-profile.php');
//include('remove-admin-menus.php');
//Cron Function for Reminders
include('reminders/index-inc.php');
//User Activity Log
include('activity-log/index-inc.php');
//Export Patients
include('export-patients.php');
?>