<?php
function export_list () {
	?>
	<div class="wrap">
		<h2>Exports List</h2>
		<?php
		global $wpdb;
		$args = array(
			'posts_per_page'   => '',
			'post_type'        => 'export_list',
			'post_mime_type'   => '',
			'post_parent'      => '',
			'author'           => get_current_user_id(),
			'post_status'      => 'private',
			'suppress_filters' => true 
		);
		$posts_array = get_posts( $args );
		echo "<table class='wp-list-table widefat fixed'>";
		echo "<tr><th>File Name</th><th>Number of Intake Entries</th><th>Duration</th><th>Uploaded Status</th><th>Download Link to File</th></tr>";
		foreach ($posts_array as  $row) {
		//	$cpunt = unserialize(get_post_meta( $row->ID, 'number_entries', true ));
			echo "<tr>";
			echo "<td>".get_post_meta( $row->ID, 'name', true )."</td>";
			echo "<td>".get_post_meta( $row->ID, 'number_entries', true )."</td>";
			echo "<td>".get_post_meta( $row->ID, 'start_date', true )." - ".get_post_meta( $row->ID, 'end_date', true )."</td>";
			echo "<td>".get_post_meta( $row->ID, 'status', true )."</td>";	
			echo "<td><a href='".get_post_meta( $row->ID, 'link', true )."'>Download Link</a></td>";
			echo "</tr>";}
		echo "</table>";
		?>
	</div>
	<?php
}