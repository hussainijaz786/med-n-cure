<?php
function export_intakes () {
	
	//error_reporting(E_ALL);
	////ini_set('display_errors', 0);
	
	global $wpdb;
	if(isset($_POST['save']) || isset($_POST['all_export'])){
		
		$query_part = '';
		if(isset($_POST['save'])){
			
			$start_date = str_replace('/', '-', $_POST['start_date']);
			$start_date = date("Y-m-d", strtotime($start_date));
			$end_date 	= str_replace('/', '-', $_POST['end_date']);
			$end_date 	= date("Y-m-d", strtotime($end_date));
			
			
			$post_ids = $wpdb->get_col("SELECT ID FROM `wp_posts` WHERE `post_date` BETWEEN '".$start_date." 00:00:00' AND '".$end_date." 00:00:00' AND `post_type` LIKE 'intake'");
		}
		else{
			$post_ids = $wpdb->get_col( "SELECT distinct(ID) FROM {$wpdb->posts} JOIN {$wpdb->postmeta} WHERE post_type ='intake' AND post_status='publish' ");
		}
		
		
		
		
		$siteurl = get_site_url();
		
		//Export Data If
		$uploads = wp_upload_dir();
		if (!file_exists($uploads['basedir'].'/intake/')) {
			mkdir($uploads['basedir'].'/intake/', 0777, true);
		}
		
		// output headers so that the file is downloaded rather than displayed
		$output  = @fopen($uploads['basedir'].'/intake/'."Export_intake_".date("Y-m-d").".csv", "w");

		fputcsv($output, array('Gender','Which of the following best represents your racial or ethnic heritage?','Address Line1','Address Line2','City','State','Postal Code','Country',
		'Height', 'Weight','Referred By','Primary Diagnosis','Date of Onset','Duration','ICD-9 or ICD-10',
		'OTHER HEALTH CONCERNS', 'List any past surgeries and treatments', 'Please list ALL prescribed and over-the-counter medications taken regularly or as needed.',
		'Do you have any medication allergies?', 'Alcoholism','Which Relative','Anemia','Which Relative','Anesthesia problems','Which Relative',
		'Arthritis', 'Which Relative?','Asthma','Which Relative?','Birth Defects','Which Relative?','Bleeding problems','Which Relative?','Cancer','Which Relative?',
		'Colon Problems (polyps, colitis)', 'Which Relative?','Crohn\’s Disease','Which Relative?','Depression','Which Relative?','Diabetes, Type 1 (child)','Which Relative?','Diabetes, Type 2 (adult)','Which Relative?',
		'Eczema Other','Which Relative?', 'Epilepsy (Seizures)','Which Relative?','Genetic diseases','Which Relative?','Glaucoma','Which Relative?','Hay fever (Allergies)','Which Relative?','Heart Attack (CAD)',
		'Which Relative?','High Blood Pressure','Which Relative?','High cholesterol','Which Relative?',
		'Kidney diseases','Which Relative?','Lupus (SLE)','Which Relative?','Mental retardation',
		'Which Relative?', 'Migraine headaches','Which Relative?','Mitral Valve Prolapse','Which Relative?','Osteoporosis',
		'Which Relative?', 'Stroke (CVA)','Which Relative?','Substance abuse','Which Relative?',
		'Thyroid disorders','Which Relative?','Tuberculosis','Which Relative?',
		'Abdominal pain/bloating', 'Acid reflux (heartburn)','Alcoholism','Allergies','Anxiety','Asthma','Atrial fibrillation','Bladder/Kidney/Urinary problems',
		'Breast problems', 'Cancer','Coagulation (bleeding or clotting) problems','Cholesterol problem','Chronic low back pain','Constipation/diarrhea','Coordination problems','Depression','Diabetes mellitus','Dizziness',
		'Erectile dysfunction', 'Fatigue/weakness','Headaches','Heart disease/chest pain/rhythm disturbance','Hypertension (high blood pressure)','Irritable bowel syndrome','Memory loss','Migraines','Muscle/joint pain or swelling','Numbness',	
		'Osteopenia or Osteoporosis', 'Pain','Prostate problem','Sexual function problems','Skin problems','Sleep problems','Substance abuse','Thyroid problem','Vision/hearing problems','Weight loss/gain',
		'Other problems', 'Using the scale below, what best describes your cannabis use?','What methods of administration have you tried?','Please describe in a few words how you feel when using cannabis.','Do you have a favorite strain of cannabis?','Strain Name','Why do you like this strain?','Are you currently using cannabis for your symptoms?','Rate your ORIGINAL symptoms level','Rate your symptoms level AFTER using cannabis:',
		'Visual Explanation', 'What is your objective with cannabis?','How committed are you to the cannabis regimen?','How do you rate your diet? On a scale of 1-10 (1-very unhealthy and 10-extremely healthy)','Please list the top 10 foods eaten on a regular basis','Please describe any current dietary restrictions that you may have (vegan/vegetarian/celiac etc.)','Do you have any food allergies?','Please Describe','Have you made any recent changes to your diet?',
		'Please Describe', 'What top five foods do you crave?','What top five foods do you avoid?','Do you snack during the day?','What do you snack on?','How many days per week do you eat:','Do you generally cook your own meals?','How often?','How are you usually eating your meals?','Do you eat a wide variety of foods?',	
		'How often do you consume sugar?', 'Do you consider yourself:','Alcohol','Coffee','Decaf Coffee','Diet drinks/aids','Soft drinks','Black tea','Green tea','Herbal tea',
		'Sport Drinks', 'Fruit juice','Water','In the past 3 months how many days per week did you exercise?','Do you participate in regular Cardiovascular exercise?','What type?','How often?','Duration?','How would you describe your intensity?','Do you participate in regular strength training exercise?',
		'How would you describe the intensity?', 'What kind?','Do you participate in flexibility exercises and stretching?','What kind?','How often?','How would you describe the intensity?','Do you or have you served in the Military?',
		'Branch of Service:', 'Highest Rank','Are you aware if you have been exposed to any chemicals?','Please list which chemicals you were exposed to:','Have you been exposed to a war zone or combat?','What would you like to tell us that was not included in this patient intake form?','Date'));
		
		$html = "";
        // loop through all the returned results
		$i=0;
		$na = 0;
		foreach ($post_ids as  $value) {
			$fields = get_field_objects($value);	
        	
			$i++;
			// field 1.3 is the first name. I upper cased the first letter for consistency
			$gender       	 	 = get_field( "gender", $value );
			
			
			$ethnic_heritage = get_field('ethnic_heritage',$value);
			$heritage0 = '';
			if($ethnic_heritage){
				foreach($ethnic_heritage as $ethnic){
					$heritage0 .= $ethnic.',';
				}
			}
			$heritage0 = rtrim($heritage0, ',');
			
			
			//$heritage0 = get_field('ethnic_heritage',$value);
			
			/* $heritage0 = json_decode(get_field( "ethnic_heritage", $value ), true);
			if (json_last_error() != JSON_ERROR_NONE) {
				$heritage0   		 = implode(",",get_field( "ethnic_heritage", $value ));
			} */
			
			$street_address		 = get_field('street_address',$value);
			$address2 			 = get_field('address2',$value);
			$city 				 = get_field('city',$value);
			$state  			 = get_field('state',$value);
			$zip_code 			 = get_field('zip_code',$value);
			$country 			 = get_field('country',$value);
			$height       		 = get_field( "height", $value );
			$weight       		 = get_field( "weight", $value );
			$referred_by  		 = get_field( "referred_by", $value );
			$primary_diagnosis   = get_field( "principal_primary_diagnosis", $value );
			$date_onset  		 = get_field( "date_of_onset", $value );
			$duration   		 = get_field( "duration2", $value );
			$icd  				 = get_field( "icd-9_or_icd-10_code_if_known", $value );				
			$a81        		 = get_field( "concerns", $value );
			$cerge = get_field('other_health_concerns');
			if($cerge){
				foreach($cerge as $row){
					$a81 .=  $row['concerns']."\n";
				}
			}
			$rows = get_field('list_any_past_surgeries_and_treatments');
			if($rows){

				foreach($rows as $row){
					$a82 .=  $row['Surgery/treatment'] . ' - ' . $row['Month'] . ' - ' . $row['Year']."\n";
				}

			}
				//$past_treat2   		 = get_field( "list_any_past_surgeries_and_treatments", $value );
				/*if(is_array($past_treat2)):
				foreach ($past_treat2 as $key => $value) {
					$a82 .= $value['Surgery/treatment']." - ".$value['Month']." - ".$value['Year'];
					$a82 .= "\n";
				}
				endif;*/
			$row2 = get_field('please_list_all_prescribed_and_over');
			if($row2){
				foreach($row2 as $row){
					$a83 .=  $row['medication_name'] . ' - ' . $row['dose'] . ' - ' . $row['frequency']."\n";
				}
			}
				/*$a83a  		 = get_field( "please_list_all_prescribed_and_over-the-counter_medications_taken_regularly_or_as_needed", $value );
				if(is_array($a83a)):
				foreach ($a83a as $key => $value) {
					$a83 .= $value['medication_name']." - ".$value['dose']." - ".$value['frequency'];
					$a83 .= "\n";
				}
				endif;*/
			$a84  	     = get_field( "do_you_have_any_medication_allergies", $value );
			$a85  		 = get_field( "alcoholism", $value );
			$a86 		 = get_field( "which_relative_alcoholism", $value );
			$a87  		 = get_field( "anemia", $value );
			$a88 		 = get_field( "which_relative_anemia", $value );
			$a90 		 = get_field( "anesthesia_problems", $value );
			$a91 		 = get_field( "which_relative_anesthesia", $value );

			$a92 		 = get_field( "arthritis", $value );
			$a93  		 = get_field( "which_relative_arthritis", $value );
			$a94  		 = get_field( "asthma", $value );
			$a95 	 	 = get_field( "which_relative_asthama", $value );

			$a96  		 = get_field( "birth_defects", $value );
			$a97 	 	 = get_field( "which_relative_birth_defects", $value );
			$a98	 	 = get_field( "bleeding_problems", $value );
			$a99 	 	 = get_field( "which_relative_bleeding_problems", $value );

			$a100	  	 = get_field( "cancer", $value );
			$a101 	 	 = get_field( "which_relative_cancer", $value );
			$a102	 	 = get_field( "colon_problems_polyps_colitis", $value );
			$a103		 = get_field( "which_relative_colon_problems", $value );
			
			$a104		 = get_field( "crohn’s_disease", $value );
			$a105		 = get_field( "which_relative_crohons", $value );	
			$a106		 = get_field( "depression", $value );
			$a107		 = get_field( "which_relative_depression", $value );

			$a108		 = get_field( "diabetes_type_1_child", $value );
			$a109		 = get_field( "which_relative_diabetes_t1", $value );
			$a114		 = get_field( "diabetes_type_2_adult", $value );
			$a115	 	 = get_field( "which_relative_diabetes_t2", $value );

			$a110	 	 = get_field( "heart_diseasechest_painrhythm_disturbance", $value );
				//$a111	 	 = get_field( "medications_supplements", $value );
				/*$a112 		 = get_field( "high_blood_pressure", $value );
				$a113	 	 = get_field( "which_relative_high_blood_pressure", $value );*/
				//$age_diagnosis12 	 = get_field( "medications_supplements", $value );
				
			$a116	 	 = get_field( "eczema_other", $value );
			$a117	 	 = get_field( "which_relative_eczema", $value );
			$a118		 = get_field( "epilepsy_seizures", $value );
			$a119	 	 = get_field( "which_relative_epilepsy", $value );
			$a120		 = get_field( "genetic_diseases", $value );
			$a121	 	 = get_field( "which_relative_genetic", $value );
			$a122     		 = get_field( "glaucoma", $value );
			$a123 	 	 = get_field( "which_relative_glaucoma", $value );

			$a124 		 = get_field( "hay_fever_allergies", $value );
			$a125 	 	 = get_field( "which_relative_hayfever", $value );
			$a126   	 = get_field( "heart_attack_cad", $value );
			$a127 	 	 = get_field( "which_relative_heartattack", $value );

			$a128 		 = get_field( "high_blood_pressure", $value );
			$a129 	 	 = get_field( "which_relative_high_blood_pressure", $value );
			$a130   	 = get_field( "high_cholesterol", $value );
			$a131 	 	 = get_field( "which_relative_high_cholesterol", $value );
			
			$a132     	 = get_field( "kidney_diseases", $value );
			$a133 	 	 = get_field( "which_relative_kidney_disease", $value );
			$a134    		 = get_field( "lupus_sle", $value );
			$a135 	 	 = get_field( "which_relative_lupus", $value );
			
			$a136 		 = get_field( "mental_retardation", $value );
			$a137 	 	 = get_field( "which_relative_mental_retardation", $value );
			$a138  		= get_field( "migraine_headaches", $value );
			$a139 	 	 = get_field( "which_relative_migraine", $value );

			$a1381 		 = get_field( "mitral_valve_prolapse", $value );
			$a1391 	 	 = get_field( "which_relative_mitral", $value );
			$a140  		 = get_field( "osteoporosis", $value );
			$a141 	 	 = get_field( "which_relative_osteoporosis", $value );
			$a142		 = get_field( "stroke_cva", $value );
			$a143 	 	 = get_field( "which_relative_stroke", $value );
			$a144  	 	 = get_field( "substance_abuse", $value );
			$a145 	 	 = get_field( "which_relative_substance_abuse", $value );
			$a146  		 = get_field( "thyroid_disorders", $value );
			$a147 	 	 = get_field( "which_relative_thyroid_disorders", $value );
			$a148   	 = get_field( "tuberculosis", $value );
			$a149 	 	 = get_field( "which_relative_tuberculosis", $value );
			
			//Patient start from here
			$a150   			 = get_field( "abdominal_painbloating", $value );
			$a151	 			 = get_field( "acid_reflux_heartburn", $value );
			$a152	 			 = get_field( "alcoholism", $value );
			$a153	 			 = get_field( "allergies", $value );
			$a154	 			 = get_field( "anxiety", $value );
			$a155	 			 = get_field( "asthma", $value );
			$a156	 			 = get_field( "atrial_fibrillation", $value );
			$a157	 			 = get_field( "bladderkidneyurinary_problems", $value );
			$a158	 			 = get_field( "breast_problems", $value );
			$a159	 			 = get_field( "cancer", $value );
			$a160	 			 = get_field( "coagulation_bleeding_or_clotting_problems", $value );
			$a161	 			 = get_field( "cholesterol_problem", $value );
			$a162	 			 = get_field( "chronic_low_back_pain", $value );
			$a163	 			 = get_field( "constipationdiarrhea", $value );
			$a164	 			 = get_field( "coordination_problems", $value );
			$a165	 			 = get_field( "depression", $value );
			$a166	 			 = get_field( "diabetes_mellitus", $value );
			$a167	 			 = get_field( "dizziness", $value );
			$a168	 			 = get_field( "erectile_dysfunction", $value );
			$a169	 			 = get_field( "fatigueweakness", $value );
			$a170	 			 = get_field( "headaches", $value );
			$a171	 			 = get_field( "heart_diseasechest_painrhythm_disturbance", $value );
			$a172	 			 = get_field( "hypertension_high_blood_pressure", $value );
			$a173	 			 = get_field( "irritable_bowel_syndrome", $value );
			$a174	 			 = get_field( "memory_loss", $value );
			$a175	 			 = get_field( "migraines", $value );
			$a176	 			 = get_field( "musclejoint_pain_or_swelling", $value );
			$a177	 			 = get_field( "numbness", $value );
			$a178	 			 = get_field( "osteopenia_or_osteoporosis", $value );
			$a179	 			 = get_field( "general_pain", $value );
			$a180	 			 = get_field( "sexual_function_problems", $value );
			$a181	 			 = get_field( "skin_problems", $value );
			$a182	 			 = get_field( "sleep_problems", $value );
			$a183	 			 = get_field( "substance_abuse", $value );
			$a184	 			 = get_field( "thyroid_problem", $value );
			$a185	 			 = get_field( "visionhearing_problems", $value );
			$a186	 			 = get_field( "weight_lossgain", $value );
			$a187	 			 = get_field( "other_problems", $value );
			//Cannibi code
			$a188	 			 = get_field( "using_the_scale_below_what_best_describes_your_cannabis_use", $value );
			if(get_field( "what_methods_of_administration_have_you_tried_select_all_the_apply", $value ) != ''){
				
				$methods_of_administration = get_field( "what_methods_of_administration_have_you_tried_select_all_the_apply", $value );
				$a189 = '';
				if($methods_of_administration){
					foreach($methods_of_administration as $methods_of_a){
						$a189 .= $methods_of_a.',';
					}
				}
				$a189 = rtrim($a189, ',');
				/* $a189 = json_decode(get_field( "what_methods_of_administration_have_you_tried_select_all_the_apply", $value ), true);
				if (json_last_error() != JSON_ERROR_NONE) {
					$a189   		 = implode(",",get_field( "what_methods_of_administration_have_you_tried_select_all_the_apply", $value ));
				} */
				
			}else{ $a189	 		 = '';}
				$a190	 			 = get_field( "please_describe_in_a_few_words_how_you_feel_when_using_cannabis", $value );
				$a191	 			 = get_field( "do_you_have_a_favorite_strain_of_cannabis", $value );
				$a192	 			 = get_field( "strain_name", $value );
				$a193	 			 = get_field( "why_do_you_like_this_strain", $value );
				$a194	 			 = get_field( "are_you_currently_using_cannabis_for_your_symptoms", $value );
				$a195	 			 = get_field( "rate_your_original_symptoms_level", $value );
				$a196	 			 = get_field( "rate_your_symptoms_level_after_using_cannabis", $value );
				$a197	 			 = get_field( "visual_explanation", $value );
				
			if(get_field( "what_is_your_objective_with_cannabis", $value ) != ''){
				//$a198	 			 = implode(",",get_field( "what_is_your_objective_with_cannabis", $value ));
				
				$objective_with_cannabis = get_field( "what_is_your_objective_with_cannabis", $value );
				$a198 = '';
				if($objective_with_cannabis){
					foreach($objective_with_cannabis as $objective_with_c){
						$a198 .= $objective_with_c.',';
					}
				}
				$a198 = rtrim($a198, ',');
				
				/* $a198 = json_decode(get_field( "what_is_your_objective_with_cannabis", $value ), true);
				if (json_last_error() != JSON_ERROR_NONE) {
					$a198   		 = implode(",",get_field( "what_is_your_objective_with_cannabis", $value ));
				} */
				
			}else{ $a198	 		 = '';}
				
				$a199	 			 = get_field( "how_committed_are_you_to_the_cannabis_regimen", $value );
				$a200	 			 = get_field( "how_do_you_rate_your_diet_on_a_scale_of_1-10_1-very_unhealthy_and_10-extremely_healthy", $value );
				$a202 				 = get_field( "please_list_the_top_10_foods_eaten_on_a_regular_basis", $value );
				$a203 				 = get_field( "please_describe_any_current_dietary_restrictions_that_you_may_have_veganvegetarianceliac_etc", $value );
				$a204 				 = get_field( "do_you_have_any_food_allergies", $value );
				$a205 				 = get_field( "please_describe_food_allergies", $value );
				$a207 				 = get_field( "have_you_made_any_recent_changes_to_your_diet", $value );
				$a208 				 = get_field( "please_describe_diet_changes", $value );
				$a209  				 = get_field( "what_top_five_foods_do_you_crave", $value );
				$a211  				 = get_field( "what_top_five_foods_do_you_avoid", $value );
				$a212 				 = get_field( "do_you_snack_during_the_day", $value );
				$a213  				 = get_field( "what_do_you_snack_on", $value );
				$row12 = get_field('how_many_days_per_week_do_you_eat:', $value );
			
			if($row12){
				foreach($row12 as $row){
					$a213 .=  $row['breakfast'] . ' - ' . $row['lunch'] . ' - ' . $row['dinner']."\n";
				}
			}
				//$a213  				 = get_field( "how_many_days_per_week_do_you_eat:", $value ); //repeater field
				$a215 				 = get_field( "do_you_generally_cook_your_own_meals", $value );
				$a216                = get_field( "how_often", $value );
			if(get_field( "how_are_you_usually_eating_your_meals", $value ) != ''){
				//$a217                = implode(",",get_field( "how_are_you_usually_eating_your_meals", $value ));
				
				$usually_eating_meals = get_field( "how_are_you_usually_eating_your_meals", $value );
				
				$a217 = '';
				if($usually_eating_meals){
					foreach($usually_eating_meals as $usually_eating){
						$a217 .= $usually_eating.',';
					}
				}
				$a217 = rtrim($a217, ',');
				
				/* $a217 = json_decode(get_field( "how_are_you_usually_eating_your_meals", $value ), true);
				if (json_last_error() != JSON_ERROR_NONE) {
					$a217   		 = implode(",",get_field( "how_are_you_usually_eating_your_meals", $value ));
				} */
				
			}else{ $a217	 		 = '';}
				
				$a220                = get_field( "do_you_eat_a_wide_variety_of_foods", $value );
				$a221  				 = get_field( "how_often_do_you_consume_sugar", $value );
				
				$a300  = get_field( "do_you_consider_yourself:", $value );
				$a297  = get_field( "alcohol", $value );
				$a298  = get_field( "coffee", $value );
				$a299  = get_field( "decaf_coffee", $value );
				$a311  = get_field( "diet_drinksaids", $value );
				$a312  = get_field( "soft_drinks", $value );
				$a313  = get_field( "black_tea", $value );
				$a226  = get_field( "green_tea", $value );
				$a228  = get_field( "herbal_tea", $value );
				$a229  = get_field( "sport_drinks", $value );
				$a230  = get_field( "fruit_juice", $value );
				$a231  = get_field( "water", $value );
				$a232  = get_field( "in_the_past_3_months_how_many_days_per_week_did_you_exercise", $value );
				$a233  = get_field( "do_you_participate_in_regular_cardiovascular_exercise", $value );
				$a234  = get_field( "what_type_regular_cardiovascular_exercise", $value );
				$a235  = get_field( "how_often_regular_cardiovascular_exercise", $value );
				$a236  = get_field( "duration_regular_cardiovascular_exercise", $value );
				$a237  = get_field( "how_would_you_describe_your_intensity", $value );
				$a240  = get_field( "do_you_participate_in_regular_strength_training_exercise", $value );
				$a241  = get_field( "how_would_you_describe_the_intensity_regular_strength_training_exercise", $value );
				$a242  = get_field( "what_kind_regular_strength_training_exercise", $value );
				$a243  = get_field( "do_you_participate_in_flexibility_exercises_and_stretching", $value );
				$a244  = get_field( "what_kind_excercises_stretching", $value );
				$a245  = get_field( "how_often_excercises_stretching", $value );
				$a246  = get_field( "how_would_you_describe_the_intensity_excercises_stretching", $value );
				$a247  = get_field( "do_you_or_have_you_served_in_the_military", $value );
				$a248  = get_field( "branch_of_service:", $value );
				$a249  = get_field( "highest_rank", $value );
				$a250  = get_field( "are_you_aware_if_you_have_been_exposed_to_any_chemicals", $value );
				$a251  = get_field( "please_list_which_chemicals_you_were_exposed_to:", $value );
				$a252  = get_field( "have_you_been_exposed_to_a_war_zone_or_combat", $value );
				$a253  = get_field( "what_would_you_like_to_tell_us_that_was_not_included_in_this_patient_intake_form", $value );
				$a296  = get_field( "date", $value );
			
				
				fputcsv($output, array($gender,$heritage0,$street_address,$address2,$city,$state,$zip_code,$country,$height,$weight,$referred_by,$primary_diagnosis,$date_onset,
				$duration,$icd,$a81,$a82,$a83,$a84,$a85,$a86,$a87,$a88,$a90,$a91,$a92,$a93,$a94,$a95,$a96,$a97,$a98,$a99,$a100,$a101,$a102,
				$a103,$a104,$a105,$a106,$a107,$a108,$a109,$a110,$a114,$a115,$a116,$a117,$a118,$a119,$a120,$a121,$a122,$a123,$a124,$a125,$a126,
				$a127,$a128,$a129,$a130,$a131,$a132,$a133,$a134,$a135,$a136,$a137,$a138,$a139,$a1381,$a1391,$a140,$a141,$a142,$a143,$a144,$a145,$a146,$a147,$a148,$a149,$a150,$a151,$a152,$a153,
				$a154,$a155,$a156,$a157,$a158,$a159,$a160,$a161,$a162,$a163,$a164,$a165,$a166,$a167,$a168,$a169,$a170,$a171,$a172,$a173,$a174,$a175,$a176,$a177,$a178,$a179,$a180,
				$a181,$a182,$a183,$a184,$a185,$a186,$a187,$a188,$a189,$a190,$a191,$a192,$a193,$a194,$a195,$a196,$a197,$a198,$a199,$a200,$a202,$a203,$a204,$a205,$a207,
				$a208,$a209,$a211,$a212,$a213,$a214,$a215,$a216,$a217,$a220,$a221,$a300,$a297,$a298,$a299,$a311,$a312,$a313,$a226,$a228,$a229,$a230,$a231,$a232,$a233,$a234,$a235,
				$a236,$a237,$a240,$a241,$a242,$a243,$a244,$a245,$a246,$a247,$a248,$a249,$a250,$a251,$a252,$a253,$a296));
			$i++;
        }
	}  // Ends of export method
	?>

	<script>
	jQuery(function() {
		jQuery( ".date" ).datepicker();
	});
	</script>

	<div class="wrap">
		<h2>Intake Export Here</h2>
		<form method="post" action="<?php echo $_SERVER['REQUEST_URI']; ?>">
			
			<input type="submit" name="all_export" id="all_export" class="button button-primary button-large" value="Export All Data">
			
			<div id="general_product_data" class="container text-center">
				<p class="form-field "><label for="start_date">Start Date:</label>
				<input type="text" class="date" style="width:300px; padding:7px;" name="start_date" id="start_date" value="" placeholder=""> </p>
				<p class="form-field "><label for="end_date">End Date:</label>
				<input type="text" class="date" style="width:300px; padding:7px; margin-left:5px;" name="end_date" id="end_date" value="" placeholder=""> </p>
				<br/>
				<div id="col-md-6">
					<span class="spinner"></span>
					<input name="original_publish" type="hidden" id="original_publish" value="Publish">
					<input type="submit" name="save" id="save" class="button button-primary button-large" value="Export Data">
				</div>
			</div>
		</form>
	</div>
	<?php
}