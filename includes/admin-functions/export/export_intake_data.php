<?php 
add_action('admin_menu','export_intake_forms');
function export_intake_forms() {
	
	//this is the main item for the menu
	add_menu_page('MednCures Export', //page title
	'MednCures Export', //menu title
	'manage_options', //capabilities
	'export_intakes', //menu slug
	'export_intakes' //function
	);
add_submenu_page('export_intakes', //parent slug
	'List Export Files', //page title
	'List Export Files', //menu title
	'manage_options', //capability
	'export_list', //menu slug
	'export_list'); //function	
	
}

?>