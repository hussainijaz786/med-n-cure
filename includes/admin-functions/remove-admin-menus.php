<?php

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly 

function cdrmed_remove_menu_items() {
        
	remove_menu_page( 'edit.php?post_type=appointment' );
	remove_menu_page( 'edit.php?post_type=document' );
	remove_menu_page( 'edit.php?post_type=pre-registrations' );
	remove_menu_page( 'edit.php?post_type=intake' );
	remove_menu_page( 'edit.php?post_type=portfolio' );
	remove_menu_page( 'edit.php?post_type=products' );
	remove_menu_page( 'edit.php?post_type=staff' );
	remove_menu_page( 'edit.php?post_type=testimonial' );
	remove_menu_page( 'edit.php?post_type=patient_feedback' );
	remove_menu_page( 'edit.php?post_type=symptoms_tracker' );
	remove_menu_page( 'edit.php?post_type=patient_therapy' );
	remove_menu_page( 'edit.php?post_type=strain_shoops' );
	remove_menu_page( 'edit.php?post_type=cdrmed_activity_log' );
	
	// WP Default Page Links
	remove_menu_page('link-manager.php'); // Links
	remove_menu_page('edit-comments.php'); // Comments
	remove_menu_page('themes.php'); // Appearance
	remove_menu_page('tools.php'); // Tools
	remove_menu_page('options-general.php'); // Settings
		
}
add_action( 'admin_menu', 'cdrmed_remove_menu_items' );

?>