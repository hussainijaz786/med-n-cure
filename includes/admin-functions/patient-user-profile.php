<?php 

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly 


function add_fields_patient_profile_fields( $user ) {
	$user_id = isset($_GET['user_id'])? $_GET['user_id'] : '';
	$user_role = cdrmed_get_user_role($user_id);
	if($user_role == 'patient'){
		?>
		<h3><?php _e('Select Patient Physicians', ''); ?></h3>
		<table class="form-table">
			<tr>
				<th>
					<label for="address"><?php _e('Physicians', ''); ?>
				</label></th>
				<td>
					<div class="admin-select-physician-backend">
					<?php
					$physician_arr = json_decode(get_user_meta($user_id, 'patient_physician_id', true), true);
					
					$get_physicians = get_users( 'orderby=user_id&role=physician' );
					foreach ($get_physicians as $key => $value) {
						$physician_id = $value->ID;
						$post_id = get_user_meta($physician_id, 'physician_post_id', true);
						$name = get_the_title($post_id);
						if(!$name){
							$name = get_user_meta($physician_id, 'first_name', true).' '.get_user_meta($physician_id, 'first_last', true);
						}
						if(!empty($name) && $name != ' '){
							$checked = '';
							if(sizeof($physician_arr) > 0 && in_array($physician_id,$physician_arr )){
								$checked ='checked ';
							}
							?>
							<span class="single-physician"><input type="checkbox" class="physician_ids" name="physician_ids[]" value="<?php echo $physician_id; ?>" <?php echo $checked; ?>><?php echo $name; ?></span>
							<?php
						}
					}
					?>
					</div>
				</td>
			</tr>
		</table>
		
				
	<?php }
}

function save_field_patient_profile_fields( $user_id ) {
	global $wpdb;
	if ( !current_user_can( 'edit_user', $user_id ) )
		return FALSE;
	$patient_id = $user_id;
	
	$physician_ids = $_POST['physician_ids'];
	
	$patient_intake = get_user_meta($patient_id, 'patient_intake', true);
	//get user parent id
	$patient_parent_user_id = $wpdb->get_var( $wpdb->prepare( "select meta_value from $wpdb->postmeta where meta_key = 'patient_parent_user_id' and post_id = '%s' LIMIT 1", $patient_intake ) );
	$parent_id = null;
	if($patient_parent_user_id == 'orphan' && sizeof($physician_ids) > 0){
		$parent_id = $physician_ids[0];
		$child_user = get_user_meta($parent_id, 'user_parent', true);
		if($child_user != ''){
			$parent_id = get_user_meta($parent_id, 'user_parent_id', true);
		}
		update_post_meta($patient_intake, 'patient_parent_user_id', $parent_id);
		update_user_meta( $patient_id, 'patient_physician_id', '' );
	}
	
	if(!in_array($patient_parent_user_id, $physician_ids) || $parent_id){
		$user_role = cdrmed_get_user_role($patient_parent_user_id);
		if('physician' == $user_role){
			$physician_ids[] = $patient_parent_user_id;
		}
	}
	update_user_meta( $patient_id, 'patient_physician_id', json_encode($physician_ids) );	
}

add_action( 'show_user_profile', 'add_fields_patient_profile_fields' );
add_action( 'edit_user_profile', 'add_fields_patient_profile_fields' );

add_action( 'personal_options_update', 'save_field_patient_profile_fields' );
add_action( 'edit_user_profile_update', 'save_field_patient_profile_fields' );


?>