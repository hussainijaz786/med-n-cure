<?php 

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly 

// create custom plugin settings menu
add_action('admin_menu', 'cdrmed_reminders_menu');

function cdrmed_reminders_menu() {

	//create new top-level menu
	add_menu_page('MednCures Patient Email Reminders', 'Reminders', 'administrator', __FILE__, 'cdrmed_reminders_settings_page' , 'dashicons-pressthis' );

	//call register settings function
	add_action( 'admin_init', 'register_cdrmed_reminders_settings' );
}


function register_cdrmed_reminders_settings() {
	//register our settings for Daily Email
	//register_setting( 'cdrmed-reminders-settings-group', 'turn_emailing' );
	
	//register our settings for Daily Email
	register_setting( 'cdrmed-reminders-settings-group', 'health_journey_form' );
	register_setting( 'cdrmed-reminders-settings-group', 'health_journey_subject' );
	register_setting( 'cdrmed-reminders-settings-group', 'health_journey_content' );
	
	//register our settings for Weekly Email
	register_setting( 'cdrmed-reminders-settings-group', 'from_name_weekly' );
	register_setting( 'cdrmed-reminders-settings-group', 'from_subject_weekly' );
	register_setting( 'cdrmed-reminders-settings-group', 'from_content_weekly' );
	
	//Register our settings for Monthly Email
	register_setting( 'cdrmed-reminders-settings-group', 'from_name_monthly' );
	register_setting( 'cdrmed-reminders-settings-group', 'from_subject_monthly' );
	register_setting( 'cdrmed-reminders-settings-group', 'from_content_monthly' );
}

function cdrmed_reminders_settings_page() {
?>
<div class="wrap">
<h2>MednCures Patient Reminders</h2>
<h4>Please configure all the emails which you want to send to the patients on the regular intervals.</h4>
<br><br>
<form method="post" action="options.php">
    <?php settings_fields( 'cdrmed-reminders-settings-group' ); ?>
    <?php do_settings_sections( 'cdrmed-reminders-settings-group' ); ?>
   
    <h4>Please Configure Patient Health Journey Email Settings</h4>
	<p>You can use the following tags while composing email: [patient-name] * [site-url] * [tbody]</p>
   	<table class="form-table">

		<tr valign="top">
		<th scope="row">From Name</th>
		<td><input type="text" name="health_journey_form" value="<?php echo esc_attr( get_option('health_journey_form') ); ?>" /></td>
		</tr>
		 
		<tr valign="top">
		<th scope="row">Email Subject</th>
		<td><input type="text" name="health_journey_subject" value="<?php echo esc_attr( get_option('health_journey_subject') ); ?>" /></td>
		</tr>

		 <tr valign="top">
		<th scope="row">Email Content</th>
		<td><textarea style="width: 450px; height: 300px;" name="health_journey_content"/><?php echo esc_attr( get_option('health_journey_content') ); ?></textarea></td>
		</tr>
       
    </table> 
	<br><br>
	  
     <h4>Please Configure Weekly Email Settings</h4>
      <table class="form-table">
        
        <tr valign="top">
        <th scope="row">From Name</th>
        <td><input type="text" name="from_name_weekly" value="<?php echo esc_attr( get_option('from_name_weekly') ); ?>" /></td>
        </tr>
         
        <tr valign="top">
        <th scope="row">Email Subject</th>
        <td><input type="text" name="from_subject_weekly" value="<?php echo esc_attr( get_option('from_subject_weekly') ); ?>" /></td>
        </tr>
        
         <tr valign="top">
        <th scope="row">Email Content</th>
        <td><textarea style="width: 450px; height: 300px;" name="from_content_weekly"/><?php echo esc_attr( get_option('from_content_weekly') ); ?></textarea></td>
        </tr>   
        
        </table>
        <h4>Please Configure Monthly Email Settings</h4>
		<table class="form-table">

        <tr valign="top">
        <th scope="row">From Name</th>
        <td><input type="text" name="from_name_monthly" value="<?php echo esc_attr( get_option('from_name_monthly') ); ?>" /></td>
        </tr>
         
        <tr valign="top">
        <th scope="row">Email Subject</th>
        <td><input type="text" name="from_subject_monthly" value="<?php echo esc_attr( get_option('from_subject_monthly') ); ?>" /></td>
        </tr>
        
         <tr valign="top">
        <th scope="row">Email Content</th>
        <td><textarea style="width: 450px; height: 300px;" name="from_content_monthly"/><?php echo esc_attr( get_option('from_content_monthly') ); ?></textarea></td>
        </tr>   
        
        </table>
    
    <?php submit_button(); ?>

</form>
</div>
<?php } ?>