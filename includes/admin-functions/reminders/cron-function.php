<?php 

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly 

// We declared New Intervals In WP Cron Job System
function add_new_intervals($schedules){
	
	$schedules['daily'] = array(
		'interval' => 86400,
		'display' => __('Daily')
	);
	
	// add weekly and monthly intervals
	$schedules['weekly'] = array(
		'interval' => 604800,
		'display' => __('Once Weekly')
	);

	$schedules['monthly'] = array(
		'interval' => 2635200,
		'display' => __('Once a month')
	);

	return $schedules;
}
add_filter( 'cron_schedules', 'add_new_intervals');

/*
*
*	Medicine Notification
*
*
*/
$timestamp_morning = strtotime( date( 'Y-m-d' ).' 08:00:00' );
if( !wp_next_scheduled( 'send_medicine_notification' ) ) {
   wp_schedule_event( $timestamp_morning, 'daily', 'send_medicine_notification', 'morning');
}
$timestamp_afternoon = strtotime( date( 'Y-m-d' ).' 14:00:00' );
if( !wp_next_scheduled( 'send_medicine_notification' ) ) {
   wp_schedule_event( $timestamp_afternoon, 'daily', 'send_medicine_notification', 'afternoon');
}
$timestamp_evening = strtotime( date( 'Y-m-d' ).' 20:00:00' );
if( !wp_next_scheduled( 'send_medicine_notification' ) ) {
   wp_schedule_event( $timestamp_evening, 'daily', 'send_medicine_notification', 'evening' );
}


// Temporary ajax call to Send Daily medicine notification Emails Class for ajax call => send-medicine-notification
add_action( 'wp_ajax_send_medicine_notification', 'send_medicine_notification');
add_action( 'wp_ajax_nopriv_send_medicine_notification', 'send_medicine_notification');

// action for send medicine notification
add_action('send_medicine_notification', 'send_medicine_notification');
function send_medicine_notification($current_time) {
//function send_medicine_notification() {
	
	//$current_time = 'morning';
	
	$args = array(
		'role' => "patient",
		'orderby' => 'ID',
		'meta_query' => array(
			'relation' => 'AND',
			array(
				'key' => 'medicine_notification',
				'value' => 'Email',
				'compare' => '='
			),
			array(
				'key' => '_cdrmed_therapy_id',
				'value' => '',
				'compare' => '!='
			),
		),
	);
	/*
	array(
				'key' => 'latest_patient',
				'value' => '',
				'compare' => '!='
			), */
			
	$users = new WP_User_Query($args);
	$patient_users = $users->get_results();
	
	foreach ( $patient_users as $user ) {
		$patient_id = $user->ID;
		
		$email_body = '';
		//get therapy id of every patient
		$therapy_id = get_user_meta($patient_id, '_cdrmed_therapy_id', true);
		
		//get order id by therapy
		$order_id = get_post_meta( $therapy_id, 'order_id', true );
		//count total medicine
		$therapy_total_medicine = get_post_meta( $therapy_id, '_total_medicine', true );
		
		//get latest shoop id from patient meta
		$shoop_id =  get_user_meta($patient_id, 'latest_patient',true);
		for($i=0; $i < $therapy_total_medicine; $i++) {
			//start email body
			$email_body .= '\r\n\n <table border="1" cellpadding="0" cellspacing="0" width="100%" style="border: none;"><tbody>';
			
			$medicine_frequency_1 = get_post_meta( $shoop_id, 'medicine'.($i+1).'_frequency_1', true );
			
			$medicine_dosage_frequency = explode(",",get_post_meta( $shoop_id, 'medicine'.($i+1).'_dosage_frequency'.($i+1), true ));
			// Check if Medicine Frquency is Daily and Its not Hourly
			if($medicine_dosage_frequency[0] != '' && strpos($medicine_frequency_1, "hourly") === false){
				//get medicine timeing from shoop
				$medicine1_dosage_timing = explode(",",get_post_meta( $shoop_id, 'medicine'.($i+1).'_dosage_timing'.($i+1), true ));
				
				//if dose time match with cron job time
				$match_dose = array_search($current_time, $medicine1_dosage_timing);
				
				if($match_dose > -1){
					//get therapy data in form of array
					$therapy_data = get_post_meta( $therapy_id, '_cdrmed_therapy_data', false );
					$ex_data_arr = $therapy_data[0];
					//print_r($therapy_data[0]);
					$therapy_counter = 0;
					foreach($ex_data_arr as $key => $val){
						
						$head_back = ' ';
						if($therapy_counter == $i){
							//define product heading color background base on no therapy no
							switch ($i) {
								case 0:
									$head_back = ' background: #39b56a; ';
									break;
								case 1:
									$head_back = ' background: #856ac7; ';
									break;
								case 2:
									$head_back = ' background: #f0894c; ';
									break;
								case 3:
									$head_back = ' background: #ecc543; ';
									break;
							}
							
							$therapy = json_decode($val);
							$product_name = $therapy->p_product_title;
							$pcan = $therapy->p_cannabinoid;
							$pcan_value = $therapy->p_cannabinoid_value;
							$pcan_percent_value = $therapy->p_cannabinoid_percent_value;
							$scan = $therapy->s_cannabinoid;
							$scan_value = $therapy->s_cannabinoid_value;
							$scan_percent_value = $therapy->s_cannabinoid_percent_value;
							$total_this_value = $therapy->this_much_value;
							$dpd = $therapy->dose_per_day;
							$sec_cal = $therapy->secondary_calculations;
							$freq = $therapy->frequency_per_day;
							$full_profile = $therapy->full_profile_link;
							$calculations = $therapy->calculations;
							//therapy rows bg color
							$row_first_bg = ' background: #e5e8ed; ';
							$row_second_bg = ' background: #cbd3de; ';
							
							// therapy show in table rows
							$email_body .= '<tr>
								<td style="padding: 5px;'.$head_back.' color: #fff; border-radius: 10px 10px 0 0;">'.$product_name.'</td>
							</tr>
							<tr>
								<td style="padding: 5px;'.$row_first_bg.'border-color: #fff;">'.$pcan_percent_value.' % <b>'.$pcan.'</b> | '.$scan_percent_value.' % <b>'.$scan.'</td>
							</tr>
							<tr>
								<td style="padding: 5px;'.$row_second_bg.'border-color: #fff;"><b>To Be Taken</b> '.$freq.'</td>
							</tr>
							<tr>
								<td style="padding: 5px;'.$row_first_bg.'border-color: #fff;">'.$pcan.' Per Day:</b> '.ROUND($total_this_value, 2).' (g)</td>
							</tr>
							<tr>
								<td style="padding: 5px;'.$row_second_bg.'border-color: #fff;"><b>Dose of</b> '.$pcan.' each time: '.$dpd.' (g)</td>
							</tr>';
							// for two medicine
							if(intval($therapy_total_medicine) == 2){
								$email_body .= '<tr>
									<td style="padding: 5px;'.$row_first_bg.'border-color: #fff;"><b>Total Amount of</b> '.$pcan.': '.ROUND($pcan_percent_value*10*$total_this_value, 2) .' (mg)</td>
								</tr>';
								$email_body .= '<tr>
									<td style="padding: 5px;'.$row_second_bg.'border-color: #fff;"><b>Total Amount of</b> '.$scan.': '.ROUND($scan_percent_value*10*$total_this_value, 2) .' (mg)</td>
								</tr>';
							}
							else{
								//other than two medicine
								$email_body .= '<tr>
									<td style="padding: 5px;'.$row_first_bg.'border-color: #fff;"><b>Total Amount of</b> '.$scan.': '.$sec_cal.' (mg)</td>
								</tr>';
							}
							//set therapy bottom row
							$email_body .= '<tr><td style="padding: 7px 0;'.$head_back.' border-radius: 0 0 10px 10px; "></td></tr>';
							$therapy_counter++;
						}
						else{
							$therapy_counter++;
						}
					}// close therapy loop
				}
			}
			$email_body .= '</tbody></table>';
			
		}//close medicine loop
		
		
		if($email_body != ''){
			$patient_email = $user->user_email;
			$patient_first_name = get_user_meta($patient_id, 'first_name', true);
			
			$args = array(
				'call_by' => 'medicine-notification-patient',
				'receiver' => $patient_email, 
				'subject_name' => $patient_first_name, 
				'body_name' => '', 
				'body_part' => '', 
				'password_reset_url' => $email_body,
			);
			cdrmed_send_email($args);
			//echo $email_body;
		}
	}
}



// Setting up Cron Job
// Daily Email Cron
if( !wp_next_scheduled( 'send_email_daily' ) ) {
   wp_schedule_event( time(), 'daily', 'send_email_daily' );
}
// Weekly Email Cron
if( !wp_next_scheduled( 'send_email_weekly' ) ) {
   wp_schedule_event( time(), 'weekly', 'send_email_weekly' );
}
// Monthly Email Cron
if( !wp_next_scheduled( 'send_email_monthly' ) ) {
   wp_schedule_event( time(), 'monthly', 'send_email_monthly' );
}

// Temporary ajax call to Send Daily Emails Class for ajax call => send-health-journey
add_action( 'wp_ajax_send_email_daily', 'send_email_daily');
add_action( 'wp_ajax_nopriv_send_email_daily', 'send_email_daily');

// Function to Run on Daily Basis
function send_email_daily() {
	send_patient_health_journey('Daily');
}

// Function to Run on Weekly Basis
function send_email_weekly() {
	send_patient_health_journey('Weekly');
}

// Function to Run on Monthly Basis
function send_email_monthly() {
	send_patient_health_journey('Monthly');
}


function send_patient_health_journey($health_report){
	
	$from_template = get_option('from_name_daily');
	$subject_template = get_option('from_subject_daily');
	$content_template = get_option('from_content_daily');
	
	/*
	* Define Variables to be Replaced
	* [patient-name]
	* [tbody]
	* [site-url]
	*/
	
	// [patient-name] health Journey
	
	/*
	<table border="1" cellpadding="0" cellspacing="0" width="100%">
	<thead>
	<tr>
	<th>Date, Time</th>
	<th>Status</th>
	<th>Comment</th>
	</tr>
	</thead>
	<tbody>
	
	[tbody]
	
	</tbody>
	</table>
	*/
	
	if($content_template){
		
		$siteurl = site_url();
	
		$args = array(
			'role' => "patient",
			'orderby' => 'ID',
			'meta_query' => array(
				'relation' => 'AND',
				array(
					'key' => 'user_lruh',
					'value' => 'Email',
					'compare' => '='
				),
				array(
					'key' => 'user_rhp',
					'value' => $health_report,
					'compare' => '='
				),
			),
		);
		
		$users = new WP_User_Query($args);
		$patient_users = $users->get_results();
		foreach ( $patient_users as $user ) {
			$patient_id = $user->ID;
			$patient_name = $user->first_name.' '.$user->last_name;
			$siteurl = site_url();
			$patient_email = $user->user_email;
			
			add_filter( 'posts_where', 'filter_where' );
			$args1 = array(
				'posts_per_page' => 5,
				'orderby'          => 'date',
				'order'            => 'DESC',
				'post_type'        => 'patient_feedback',
				'author'           => $patient_id,
				'post_status'      => 'private'
			);
			$query = new WP_Query( $args1 );
			$posts = $query->posts;
			$tr = '';
			foreach ($posts as  $row) {
				$postid 		= $row->ID;
				$status 		= get_post_meta($postid, 'feel',true);
				$comments 		= get_post_meta($postid, 'comments',true);
				$date 			= new DateTime($row->post_date);
				$bg_color = '';
				switch ($status) {
					case 'well':
						$nstatus = '<div class="bg_good">Well</div>';
						$bg_color = '#62a709';
						break;
					case 'v-well':
						$nstatus = '<div class="bg_vgood">Very Well</div>';
						$bg_color = '#a8ce38';
						break;
					case 'neutral':
						$nstatus = '<div class="bg_neutral">Neutral</div>';
						$bg_color = '#f5c300';
						break;
					case 'unwell':
						$nstatus = '<div class="bg_bad">Unwell</div>';
						$bg_color = '#ec2743';
						break;
					case 'v-unwell':
						$nstatus = '<div class="bg_vbad">Very Unwell</div>';
						$bg_color = '#d0112b';
						break;	
				}
				
				$tr .= '<tr>
					<td style="padding: 5px;">'.$date->format('D, F d y h:i:s A ').'</td>
					<td style="padding: 5px;" bgcolor="'.$bg_color.'"><a href="'.$siteurl.'/patient-dashboard/?post_id='.$postid.'" target="_blank" style="color: #fff; font-weight: bold; text-decoration: none;">'.$nstatus.'</a></td>
					<td style="padding: 5px;">'.$comments.'</td>
				</tr>';
			}
			$subject = str_replace('[patient-name]', $patient_name."'s", $subject_template);
			
			$email_body = 'Showing '.count($posts).' Health Check-Up Entries Between '.date('F Y', strtotime('-2 month')).' - '.date('F Y').'\r\n\n';
			$email_body .= str_replace('[tbody]', $tr, $content_template);
			//send email to patient parent parent
			/* $patient_intake_id = get_user_meta($patient_id, 'patient_intake', true);
			$patient_parent_id = get_post_meta($patient_intake_id, 'patient_parent_user_id', true);
			$receiver = cdrmed_get_user_email($patient_parent_id); */
			$args = array(
				'call_by' => 'health-journey-patient',
				'receiver' => $patient_email, 
				'subject_name' => $subject, 
				'body_name' => $email_body, 
				'body_part' => '', 
				'password_reset_url' => '',
			);
			cdrmed_send_email($args);
		}
	}
	
	return true;
}