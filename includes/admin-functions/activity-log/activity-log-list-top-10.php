<?php
function activity_log_list_top_10() {
	
	?>
	<div class="wrap">
		<h2>Top 10 Activity Performers</h2>
		<?php
		
		echo '<br><form method="post" action="'.$_SERVER['REQUEST_URI'].'">';
			?>
			<table data-toggle='table' data-striped='true' class='table table-hover fixed table-striped'
			id='admin_side_activity_log_table' data-sort-name='date'>
				<thead>
				<tr>
					<th>Sr #</th>
					<th>Name</th>
					<th>Email</th>
					<th>Role</th>
					<th>Activities</th>
				</tr>
				</head>
				<tbody>
					<?php
					$args = array(
						'post_type' => 'cdrmed_activity_log',
						'posts_per_page' => 10,
						'order' => 'DESC',
						'orderby' => 'meta_value_num',
						'meta_key' => 'total_activities',
						'meta_query' => array(
							array(
								'key' => 'total_activities',
								'value' => 0,
								'type' => 'numeric',
								'compare' => '>',
							),
						),
					);
					$post = new WP_Query( $args );
					
					$user_counter = 1;
					
					while ( $post->have_posts() ) : $post->the_post();
						
						//Get detail of Author
						$post_id = get_the_ID();
						$author_id = get_the_author_meta('ID');
						$author_email = get_the_author_meta('user_email');
						$author_name = get_the_author_meta('display_name');
						
						echo '<tr>
							<td>'.$user_counter.'</td>
							<td><a href="'.admin_url('admin.php?page=single_user_activity_log&user_id='.$author_id.'&name='.$author_name).'">'.$author_name.'</a></td>
							<td>'.$author_email.'</td>
							<td>'.ucfirst(cdrmed_get_user_role($author_id)).'</td>
							<td>'.get_post_meta($post_id, 'total_activities', true).'</td>
						</tr>';
						$user_counter++;
					endwhile;
				?>
				</body>
			</table>
		</form>
	</div>
	<?php
}?>