<?php
function single_user_activity_log () {
	
	global $wpdb;
	
	$search_id = !isset($_GET['user_id']) ? null : $_GET['user_id'];
	
	$search_name = !isset($_GET['name']) ? null : $_GET['name'].'\'s Activity Log';
	$action = !isset($_GET['action']) ? null : $_GET['action'];
	$search = 'upon';
	if($action == 'upon'){
		$search = 'performer';
		$search_name = !isset($_GET['name']) ? null : 'Activities Upon '.$_GET['name'];
	}
	
	
	?>
	<div class="wrap cdrmed-activity-log">
		<h2><?php echo $search_name; ?></h2>
		<?php
		
		echo '<br><br><form method="post" action="'.$_SERVER['REQUEST_URI'].'">';
			?>
			
			<div class="info">
				<span><b>Email:</b> <?php echo get_the_author_meta('user_email', $search_id); ?></span>
				<span><b>Role:</b> <?php echo ucfirst(cdrmed_get_user_role($search_id)); ?></span>
			</div>
			
			<div class="actions">
				<input type="text" class="activity-log-date-search" placeholder="Filter by Date" style="float: left;">&nbsp;&nbsp;&nbsp;
				<input type="text" class="activity-log-email-search2" placeholder="Filter: Type Email" style="float: left;">
			</div>
			<br><br>
			<?php
			// if view current perforper activites
			if($action == 'performer'){
			
				//$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
				$args = array(
					'post_type' => 'cdrmed_activity_log',
					'posts_per_page' => -1,
					'order' => 'DESC',
					'post_author'   => $search_id,
				);
				$post = new WP_Query( $args );
				//$activity_count = (intval($post->post_count) > 0)? intval($post->post_count) : null;
				?><div class="single-user-activity-log">
				
				<?php
				while ( $post->have_posts() ) : $post->the_post();
					$post_id = get_the_ID();
					$activity_arr = get_post_meta( $post_id );
					
					// run loop with totla activity logs keys againt user
					foreach($activity_arr as $activity_arr_key => $activity_arr_meta){
						if($activity_arr_key != 'total_activities' && $activity_arr_key != '_vc_post_settings'  && $activity_arr_key != '_edit_lock'){
							
							//fetch single meta key
							foreach($activity_arr_meta as $activity_key => $activity_value){
								//json decode current meta key
								$activity_log = json_decode($activity_value, true);
								
								foreach($activity_log as $key => $val){
									
									// get target user id
									$target_user_id = $val["target_user_id"];
									// get 5 days old date
									$date3_days_ago = Date('m/d/Y', strtotime("-5 days"));
									if($val["date"] >= $date3_days_ago){
										$upon = '-';
										$other = '-';
										$email = '';
										// if activity on any other user
										if($target_user_id){
											
											$name = get_user_meta($target_user_id, 'first_name', true).' '.get_user_meta($target_user_id, 'last_name', true);
											$email = cdrmed_get_user_email($target_user_id);
											$role = cdrmed_get_user_role($target_user_id);
											$upon = $name.' | '.$email.' ( '.$role.' )';
											if($email){
												$upon = '<a href="'.admin_url('admin.php?page=single_user_activity_log&action=upon&user_id='.$target_user_id.'&name='.$name).'">'.$name.' | '.$email.' ( '.ucfirst(cdrmed_get_user_role($target_user_id)).' )</a>';
											}
										}
										
										echo '<div class="single-activity" data-email="'.$email.'" data-date="'.$val["date"].'">
											<span>Activity: '.$val["activity"].'</span>
											<span>'.$val["date"].' '.$val["time"].'</span>
											<div class="activity">
												<span>'.ucfirst($search).': '.$upon.'</span>
											</div>
										</div>';
									}
								}
							}
						}
					}
				endwhile;
			}
			elseif($action == 'upon'){
				
				$query1 = "SELECT DISTINCT post_id FROM {$wpdb->prefix}postmeta
				WHERE `meta_key` like 'activity_log_%' AND `meta_value` like '%$search_id%' ORDER BY post_id DESC";
				
				$activity_log_post_ids = $wpdb->get_results($query1);
				/* echo '<pre>';
				print_r($activity_log_post_ids);
				echo '</pre>'; */
				?><div class="single-user-activity-log"><?php
				foreach($activity_log_post_ids as $activity_log_post_id){
					
					//Get detail of Author
					$post_id = $activity_log_post_id->post_id;
					//get author data
					$auth = get_post($post_id); // gets author from post
					//get author id
					$author_id = $auth->post_author;
					$author_email = get_the_author_meta('user_email', $author_id);
					$author_name = get_the_author_meta('display_name', $author_id);
					
					if($author_email){
						
						// get meta keys for a user
						//every date has a single meta key
						//meta key is today date
						$activity_arr = get_post_meta( $post_id );
						
						// run loop with totla activity logs keys againt user
						foreach($activity_arr as $activity_arr_key => $activity_arr_meta){
							if($activity_arr_key != 'total_activities' && $activity_arr_key != '_vc_post_settings'  && $activity_arr_key != '_edit_lock'){
								//fetch single meta key
								foreach($activity_arr_meta as $activity_key => $activity_value){
									
									//echo sizeof($activity_value).'<br>';
									
									//json decode current meta key
									$activity_log = json_decode($activity_value, true);
									
									/* echo '<pre>';
									print_r($activity_log);
									echo '</pre>'; */
									
									foreach($activity_log as $key => $val){
										
										// get target user id
										$target_user_id = $val["target_user_id"];
										$date3_days_ago = Date('m/d/Y', strtotime("-5 days"));
										if($target_user_id == $search_id && $val["date"] >= $date3_days_ago){
											
											//$role = cdrmed_get_user_role($target_user_id);
											//$performer = $author_name.' | '.$author_email.' ( '.$role.' )';
											
											$performer = '<a href="'.admin_url('admin.php?page=single_user_activity_log&action=performer&user_id='.$author_id.'&name='.$author_name).'">'.$author_name.' | '.$author_email.' ( '.ucfirst(cdrmed_get_user_role($author_id)).' )</a>';
											
											
											echo '<div class="single-activity" data-email="'.$author_email.'" data-date="'.$val["date"].'">
												<span>Activity: '.$val["activity"].'</span>
												<span>'.$val["date"].' '.$val["time"].'</span>
												<div class="activity">
													<span>'.ucfirst($search).': '.$performer.'</span>
												</div>
											</div>';
										}
									}
								}
							}
						}
					}
				}
				
			}
			
			//$total_pages = $post->max_num_pages;
			//$current_page = max(1, get_query_var('paged'));
			
			/* echo '<div class="activity-log-pag">'.paginate_links(array(
				'current' => $current_page,
				'total' => $total_pages,
				'type'         => 'list',
				'prev_text'    => 'Previous',
				'next_text'    => 'Next',
				'end_size'     => 3,
				'mid_size'     => 3
			)).'</div>'; */
			
			?>
			</div>
			
		</form>
	</div>
	<?php
}?>