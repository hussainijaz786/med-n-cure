<?php
function activity_log_list () {
	
	//delete all activity log
	if(isset($_POST['delete_all'])){
		if(isset($_POST['activity_log_check'])){
			foreach($_POST['activity_log_check'] as $post_id){
				// parse a string as post id from checkboxes
				$post_id___activity_id___date = explode("___", $post_id);
				$post_id = $post_id___activity_id___date[0];
				$activity_id = $post_id___activity_id___date[1];
				$date = $post_id___activity_id___date[2];
				
				$activity = array();
				// fetch meta key of current post id with date meta key
				$activity_json_decoded = json_decode(get_post_meta( $post_id, 'activity_log_'.$date, true), true);
				$total_activities = 0;
				foreach($activity_json_decoded as $key => $value){
					//activity_id is id of activity in json array
					if($value['activity_id'] != intval($activity_id)){
						$total_activities++;
						$activity[] = array('activity_id' => $total_activities, 'date' => $value['date'], 'time' => $value['time'], 'target_user_id' => $value['target_user_id'], 'activity' => $value['activity']);
					}
				}
				update_post_meta($post_id, 'activity_log_'.$date, json_encode($activity));
				update_post_meta($post_id, 'total_activities', $total_activities);
				//if user remove all activites
				if($total_activities > 1){
					//$author_id = get_the_author_meta('ID', $post_id);
					$auth = get_post($post_id); // gets author from post
					$author_id = $auth->post_author;
					update_user_meta($author_id, 'activity_log_id', '');
					wp_delete_post( $post_id, true );
				}
			}
		}
		echo '<div class="updated"><p>Activity Log deleted!</p></div>';
	}
	
	?>
	<div class="wrap cdrmed-activity-log">
		<h2>User Activity Log</h2>
		<?php
		
		echo '<br><br><form method="post" action="'.$_SERVER['REQUEST_URI'].'">';
			?>
			<div class="actions">
				<input type="submit" class="button button-primary button-large" name="delete_all" value="Delete Checked Data" onclick="return confirm('Do you want to delete the record?')"/>
				
				<input type="text" class="activity-log-email-search" placeholder="Filter: Type Email">
			</div>
			
			<table data-toggle="table"
			data-url="<?php echo get_admin_url().'/admin-ajax.php?action=view_all_activity_log_ajax'; ?>"
			data-pagination="true"
			data-side-pagination="server"
			data-page-list="[10, 20, 25, 50, 100, 250, 500, 1000, 5000]"
			data-search="false">
				<thead>
				<tr>
					<th data-field="check"><input type='checkbox' name='all_activity_log_select' class='all_activity_log_select' style='margin: 0;'></th>
					<th data-field='perform_by' data-sortable='true'>Perform By</th>
					<th data-field='activity_upon' data-sortable='true'>Activity Upon</th>
					<th data-field='activity' data-sortable='true'>Activity</th>
					<th data-field='date_time' data-sortable='true' data-sort-name='_date_data'>Date/Time</th>
				</tr>
				</thead>
				
			</table>
		</form>
	</div>
	<?php
}?>