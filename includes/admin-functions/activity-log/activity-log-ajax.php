<?php


/*
*	ajax call for load user activities with server side pagination
*/

add_action( 'wp_ajax_view_all_activity_log_ajax', 'view_all_activity_log_ajax');
add_action( 'wp_ajax_nopriv_view_all_activity_log_ajax', 'view_all_activity_log_ajax');
function view_all_activity_log_ajax() {
	
	
	/*
	read total activities
	*/
	$args = array(
		'post_type' => 'cdrmed_activity_log',
		'posts_per_page' => -1,
		'order' => 'DESC',
		'post_status' => 'publish',
	);
	$post = new WP_Query( $args );
	//$post0->post_count;
	$activities = array();
	$authors = array();
	
	while ( $post->have_posts() ) : $post->the_post();
		//Get detail of Author
		$post_id = get_the_ID();
		$author_id = get_the_author_meta('ID');
		$author_email = get_the_author_meta('user_email');
		$author_name = get_the_author_meta('display_name');
		
		$author_activity_index = sizeof($activities);
		$authors[$author_activity_index] = array('author_id' => $author_id, 'author_email' => $author_email, 'author_name' => $author_name);
		
		$activity_arr = get_post_meta( $post_id );
		foreach($activity_arr as $activity_arr_key => $activity_arr_meta){
			foreach($activity_arr_meta as $activity_key => $activity_value){
				$activity_log = json_decode($activity_value, true);
				if(is_array($activity_log)){
					$activities =  array_merge($activities, $activity_log);
				}
			}
		}
		
	endwhile;
	
	$activity_json['total'] = count($activities);
	
	//echo $activity_json['total'];
	//echo "<pre>";print_r($authors);echo "</pre>";
	//die();
	
	$author_id = $author_email = $author_name = '';
	
	$limit = isset($_GET['limit']) ? $_GET['limit'] : 10;
	$offset = isset($_GET['offset']) ? $_GET['offset'] : 0;
	$loop_limit = $offset + $limit;
	for($i = $offset; $i < $loop_limit; $i++){
		//break when loop reach at array limit
		if($i == $activity_json['total'])
			break;
		
		if(is_array($authors) && array_key_exists($i, $authors)){		
			$author_id = $authors[$i]['author_id'];
			$author_email = $authors[$i]['author_email'];
			$author_name = $authors[$i]['author_name'];
		}
		
		// get target user id
		$target_user_id = $activities[$i]["target_user_id"];
		$upon = '-';
		$other = '-';
		$email = '';
		// if activity on any other user
		if($target_user_id){
			
			$name = get_user_meta($target_user_id, 'first_name', true).' '.get_user_meta($target_user_id, 'last_name', true);
			$email = cdrmed_get_user_email($target_user_id);
			$role = cdrmed_get_user_role($target_user_id);
			//$upon = $name.' | '.$email.' ( '.$role.' )';
			if($email){
				$upon = '<a href="'.admin_url('admin.php?page=single_user_activity_log&action=upon&user_id='.$target_user_id.'&name='.$name).'">'.$name.' | <span class="email-search" data-email="'.$email.'" data-email2="'.$email.'">'.$email.'</span> ( '.ucfirst(cdrmed_get_user_role($target_user_id)).' )</a>';
			}
			
		}
		
		$performer = '<a href="'.admin_url('admin.php?page=single_user_activity_log&action=performer&user_id='.$author_id.'&name='.$author_name).'">'.$author_name.' | <span class="email-search" data-email="'.$author_email.'" data-email2="'.$email.'">'.$author_email.'</span> ( '.ucfirst(cdrmed_get_user_role($author_id)).' )</a>';
		
		$activity_json['rows'][] = array(
			'check' => '<input type="checkbox" name="activity_log_check[]" class="activity_log_check" value="'.$post_id.'___'.$activities[$i]["activity_id"].'___'.$activities[$i]["date"].'" />',
			'perform_by' => $performer,
			'activity_upon' => $upon ,
			'activity' => $activities[$i]["activity"],
			'date_time' => $activities[$i]["date"].' '.$activities[$i]["time"],
		);
		
	}
	
	$sort = isset($_GET['sort']) ? $_GET['sort']: 'date_time';
	$order = ($_GET['order']=='asc') ? SORT_ASC : SORT_DESC;
	if(!isset($_GET['sort'])){
			$order = SORT_DESC;
	}
	$activity_json['rows'] = array_sort($activity_json['rows'], $sort, $order);
	$activity_json = json_encode($activity_json);
	echo $activity_json;
	die;
	
}