<?php

/*
*
*	Function for save user activity log
*	every user have a single post
*	every post have meta keys by date
*/

/*
cdrmed_save_activity_log('Update Patient Intake', isset($_GET['pid']) ? $_GET['pid'] : '');
$patient_id = email_exists( $patient_email );
cdrmed_save_activity_log('Update Patient Intake', '');
*/

//target user means if activity on other users
function cdrmed_save_activity_log($current_activity, $target_user_id = '') {
	$current_user = wp_get_current_user();
	$current_usid = get_current_user_id();
	
	$date = date('m/d/Y');
	$time = date('H:i:s');
	
	//check if current user have already post of activity log
	$post_type = 'cdrmed_activity_log';
	$user_post_count = count_user_posts( $current_usid , $post_type );
	if($user_post_count < 1){
		//create new post if not exists
		$post = array(
			'post_title'    => $current_user->display_name.'\'s Activities',
			'post_content'  => '',
			'post_status'   => 'publish',
			'post_author'   => $current_usid,
			'post_type'	  => $post_type
		);
		$activity_log_id = wp_insert_post( $post );
		// save post id in user meta
		update_user_meta($current_usid, 'activity_log_id', $activity_log_id);
		update_post_meta($activity_log_id, 'total_activities', '');
		//update_user_meta($current_usid, 'user_email', $current_user->user_email);
	}
	else{
		//if already create a post 
		$activity_log_id = get_user_meta($current_usid, 'activity_log_id', true);
	}
	if($activity_log_id){
		// get old activites in json
		$activity_log = get_post_meta($activity_log_id, 'activity_log_'.$date, true);
		//count total activites
		$total_activities = get_post_meta($activity_log_id, 'total_activities', true);
		if(!$total_activities){
			$total_activities = 0;
		}
		//increase 1 in total activites
		$total_activities++;
		//save total activites
		update_post_meta($activity_log_id, 'total_activities', $total_activities);
		// if old activites exists
		if($activity_log){
			$activity = json_decode($activity_log, true);
		}
		else{
			$activity = array();
		}
		$activity[] = array('activity_id' => intval($total_activities), 'date' => $date, 'time' => $time, 'target_user_id' => $target_user_id, 'activity' => $current_activity);
		update_post_meta($activity_log_id, 'activity_log_'.$date, json_encode($activity));
	}
	return true;
}