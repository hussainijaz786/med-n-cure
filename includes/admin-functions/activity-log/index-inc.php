<?php 

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly 
require_once('init.php');
require_once('activity-log-ajax.php');
require_once('activity-log-list.php');
require_once('activity-log-list-top-10.php');
require_once('single-user-activity-log.php');
require_once('activity-log-save.php');
?>