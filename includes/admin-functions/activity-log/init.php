<?php

//menu items
add_action('admin_menu','admin_cdrmed_activity_log');
function admin_cdrmed_activity_log() {
	
	//this is the main item for the menu
	add_menu_page(
		'User Activity Log', //page title
		'Activity Log', //menu title
		'manage_options', //capabilities
		'activity_log_list', //menu slug
		'activity_log_list', //function
		'dashicons-list-view' //icon
	);
	
	//this is a submenu
	add_submenu_page(
		'activity_log_list', //parent slug
		'Top 10 Activity Performer', //page title
		'Top 10', //menu title
		'manage_options', //capability
		'activity_log_list_top_10', //menu slug
		'activity_log_list_top_10'
	); //function
	
	//this submenu is HIDDEN, however, we need to add it anyways
	add_submenu_page(null, //parent slug
	'Single user Activity Log', //page title
	'Single user Activity Log', //menu title
	'manage_options', //capability
	'single_user_activity_log', //menu slug
	'single_user_activity_log'); //function
	
	
}
?>