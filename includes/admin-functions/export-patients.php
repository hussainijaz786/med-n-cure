<?php 

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly 

add_action('admin_menu','admin_cdrmed_export_patients');
function admin_cdrmed_export_patients() {
	
	//this is the main item for the menu
	add_menu_page(
		'Export Patients', //page title
		'Export Patients', //menu title
		'manage_options', //capabilities
		'cdrmed_export_patients', //menu slug
		'cdrmed_export_patients', //function
		'dashicons-list-view' //icon
	);
}


function cdrmed_export_patients(){
	?>
	<div class="wrap">
		<h2>Exports List</h2>
		<?php
		$users = new WP_User_Query(array(
			'role' => "patient",
			'orderby' => 'ID',
			'order' => 'DESC',
		));
		$total_patients2 = $users->get_results();
		
		$uploads = wp_upload_dir();
		
		// output headers so that the file is downloaded rather than displayed
		$output  = @fopen($uploads['basedir'].'/'."Export_Patients_".date("m-d-Y").".csv", "w");
		$file_link = $uploads['baseurl'].'/'."Export_Patients_".date("m-d-Y").".csv";
		
		fputcsv($output, array('First Name', 'Last Name', 'Email'));
		foreach ($total_patients2 as $key => $value) {      // Loop all order Items
			$patient_id = $value->ID;
			$user_info = get_userdata($patient_id);
			if($user_info){
				$first_name = $user_info->first_name;
				$last_name = $user_info->last_name;
				$email = $user_info->user_email;
			
				fputcsv($output, array($first_name,$last_name,$email));
			}
			
		}
		?>
		<div class="updated"><p><?php echo 'List exported successfully!'?></p></div>
		<h3><a href="<?php echo $file_link; ?>" target="_blank"><h3>Download</h3></a>
	</div>
	<?php
}

?>