<?php 

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly 
// Include Custom Post Types Files
require_once('init.php');
require_once('medication-list.php');
require_once('medication-create.php');
require_once('medication-update.php');
?>