<?php
function medication_create () {
	global $wpdb;
	$id = !isset($_GET['id'])?null:$_GET['id'];
	$message = "";
	if(@isset($_POST['insert'])){
		$category 		=	$_POST["category"];
		if($category == '--New Category--'){
			$category 		=	$_POST["new_category"];
		}
		$supplement 	    = 	$_POST["supplement"];
		$alert_category 	    = 	isset($_POST["p450_alert_category"]) ? $_POST["p450_alert_category"] : null;
		$status 		=	$_POST["status"];
		$wpdb->query("INSERT INTO `wp_medication` SET `category` = '$category', supplement='$supplement', alert_category='$alert_category', status = '$status'");
		$message="Mediaction inserted!";
	}
	?>
	<div class="wrap">
		<h2>Add New Medication</h2>
		<?php if (isset($message) && !empty($message)): ?>
			<div class="updated"><p>
			<?php echo $message;?></p></div>
		<?php endif;?>
		<form method="post" action="<?php echo $_SERVER['REQUEST_URI']; ?>">
			<table class='wp-list-table widefat fixed'>
				<tr><th>Category</th><td>
					<select name="category" class="medication_select" data-id="1">
					<option value=""> Select Category</option>
					<option value="--New Category--">--New Category--</option>
					<?php  $categories = $wpdb->get_results("SELECT distinct(category) from wp_medication");
					
					if(sizeof($categories) > 0){
						foreach ($categories as $ct ){
							if($ct->category !=''){?>
								<option value="<?php echo $ct->category;?>"><?php echo $ct->category;?></option>
							<?php }
						}
					}
					?>
					</select>
				</td></tr>
				
				<tr id="medication_tr1"><th>New Category</th><td><input type="text" name="new_category" value=""/></td></tr>
				
				<tr><th>Medication</th><td><input type="text" name="supplement" value=""/></td></tr>
				<tr><th>Status</th><td>
					<select name="status">
						<option value="" selected='selected'> Select Status</option>
						<option value="0"> Approve</option>
						<option value="1"> Unapprove</option>
					</select>
				</td></tr>
				<tr><th>P450 Alert</th><td><input type="checkbox" class="p450alert-checkbox" name="p450alert" value="yes"/></td></tr>
				<tr id="p450-alert-category" style="display: none;">
					<th>P450 Alert Category</th>
					<td>
					<select name="p450_alert_category">
						<option value="Inducers">Inducers</option>
						<option value="Substrates">Substrates</option>
						<option value="Pro-drugs">Pro-drugs</option>
					</select>
				</td></tr>
			</table>
			<br/>
			<div class="wrap text-center">
				<input type='submit' name="insert" value='Save' class='button button-primary button-large'>
			</div>
		</form>
	</div>
	<?php
}?>