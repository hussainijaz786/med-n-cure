<?php
function medication_list () {
	?><div class="wrap">
	<h2>Medication List</h2>
	<p>
		<a href="<?php echo admin_url('admin.php?page=medication_create'); ?>" class="page-title-action">Add New</a>
	</p><br/>
	<?php
	global $wpdb;
	
	// Delete one by one
	if(isset($_GET['action']) && $_GET['action'] == 'delete'){
		$post_id = isset($_GET['id']) ? $_GET['id'] : '';
		if($post_id != ''){
			$wpdb->query($wpdb->prepare("DELETE FROM wp_medication WHERE id = %s",$post_id));
		}
		echo '<div class="updated"><p>Medication deleted!</p></div>';
	}
	// Delete Check Records
	if(isset($_POST['delete_all'])){
		if(isset($_POST['mediaction_check'])){
			foreach($_POST['mediaction_check'] as $mediaction_check){
				$wpdb->query($wpdb->prepare("DELETE FROM wp_medication WHERE id = %s",$mediaction_check));
			}
		}
		echo '<div class="updated"><p>Medications deleted!</p></div>';
	}
	
	//filter for active/inactive
	$query_part = '';
	if(isset($_GET['med_status'])){
		$query_part = ($_GET['med_status'] == 0) ? "where status='0'" : "where status='1'";
	}
	
	$rows = $wpdb->get_results("SELECT * from wp_medication ".$query_part);
	
	echo '<form method="post" action="'.$_SERVER['REQUEST_URI'].'">';
	
	echo '<a class="button button-primary button-large" href="'.admin_url('admin.php?page=medication_list').'">All</a>&nbsp;&nbsp;&nbsp;';
	echo '<a class="button button-primary button-large" href="'.admin_url('admin.php?page=medication_list&med_status=0').'">Active</a>&nbsp;&nbsp;&nbsp;';
	echo '<a class="button button-primary button-large" href="'.admin_url('admin.php?page=medication_list&med_status=1').'">Inactive</a>&nbsp;&nbsp;&nbsp;';
	?><input type="submit" class="button button-primary button-large" name="delete_all" value="Delete" onclick="return confirm('Do you want to delete the record?')"/><br><br><?php
	
	echo "<table
	data-toggle='table'
	data-striped='true'
	class='table table-hover fixed table-striped'
	id='admin_side_medications'
	data-sort-name='category'>";
	echo "<thead>
	<tr>
		<th><input type='checkbox' name='all_medication_select' class='all_medication_select' style='margin: 0;'></th>
		<th data-field='category' data-sortable='true'>Category</th>
		<th data-field='medications' data-sortable='true'>Medication</th>
		<th data-field='status' data-sortable='true'>Status</th>
		<th data-field='alert_category' data-sortable='true'>Alert category</th>
		<th>Action</th>
	</tr>
	</head>
	<tbody>";
	foreach ($rows as $row ){
		echo "<tr>";
		echo "<td><input type='checkbox' name='mediaction_check[]' class='mediaction_check' value='".$row->id."' /></td>";	
		echo "<td>$row->category</td>";
		echo "<td>$row->supplement</td>";
		if($row->status == 0){
			echo "<td>Approved</td>";	
		}else{
			echo "<td>Unapproved</td>";	
		}
		echo "<td>$row->alert_category</td>";
		echo "<td>
			<a href='".admin_url('admin.php?page=medication_update&id='.$row->id)."'>EDIT</a> / ";
			?><a href="<?php echo admin_url('admin.php?page=medication_list&action=delete&id='.$row->id); ?>" onclick="return confirm('Do you want to delete the record?')">DELETE</a><?php
		echo "</td>";
		echo "</tr>";
	}
	echo "</body>
	</table>";
	echo '</form>';
	
	?>
	</div>
	<?php
}?>