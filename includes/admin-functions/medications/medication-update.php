<?php
function medication_update () {
	global $wpdb;
	$id = !isset($_GET['id'])?null:$_GET['id'];
	
	//update
	if(@isset($_POST['update'])){	
		$category 		=	$_POST["category"];
		if($category == '--New Category--'){
			$category 		=	$_POST["new_category"];
		}
		$supplement 	    = 	$_POST["supplement"];
		$alert_category 	    = 	isset($_POST["p450_alert_category"]) ? $_POST["p450_alert_category"] : null;
		$status 		=	$_POST["status"];
		$wpdb->query("UPDATE `wp_medication` SET `category` = '$category', supplement='$supplement', alert_category='$alert_category', status = '$status' where id = $id");
	}
	
	//delete
	else if(@isset($_POST['delete'])){	
		$wpdb->query($wpdb->prepare("DELETE FROM wp_medication WHERE id = %s",$id));
	}
	else{//selecting value to update	
		$schools = $wpdb->get_results($wpdb->prepare("SELECT * from wp_medication where id=%s",$id));
		foreach ($schools as $s ){
			$category=$s->category;
			$supplement=$s->supplement;
			$alert_category = $s->alert_category;
			
			$status=$s->status;
			//echo "<h2>Medication updated!</h2>";
		}
	}
	?>
	<div class="wrap">
	<h2>Medication List</h2>

	<?php if(@$_POST['delete']){?>
		<div class="updated"><p>Medication deleted!</p></div>
		<a href="<?php echo admin_url('admin.php?page=medication_list')?>">&laquo; Back to Medication List</a>

	<?php } else if(@$_POST['update']) {?>
		<div class="updated"><p>Medication updated!</p></div>
		<a href="<?php echo admin_url('admin.php?page=medication_list')?>">&laquo; Back to Medication List</a>
	<?php } else {?>
		<form method="post" action="<?php echo $_SERVER['REQUEST_URI']; ?>">
			<table class='wp-list-table widefat fixed'>
			
				<tr><th>Category</th><td>
					<select name="category" class="medication_select" data-id="2">
					<option value="">Select Category</option>
					<option value="-1">--New Category--</option>
					<?php  $categories = $wpdb->get_results("SELECT distinct(category) from wp_medication");
					foreach ($categories as $ct ){
						if($ct->category !=''){?>
							<option value="<?php echo $ct->category;?>" <?php if($ct->category==$category){ echo "selected='selected'";} ?>><?php echo $ct->category;?></option>
						<?php }
					} ?>
					</select>
				</td></tr>
				
				<tr id="medication_tr2"><th>New Category</th><td><input type="text" name="new_category" value=""/></td></tr>
				
				<tr><th>Medication</th><td><input type="text" name="supplement" value="<?php echo $supplement;?>"/></td></tr>
				<tr><th>Status</th><td>
					<select name="status">
						<option value=""> Select Status</option>
						<option value="0" <?php if($status==0){ echo "selected='selected'";} ?>> Approve</option>
						<option value="1" <?php if($status==1){ echo "selected='selected'";} ?>> Unapprove</option>
					</select>
				</td></tr>
				
				<tr><th>P450 Alert</th><td><input type="checkbox" class="p450alert-checkbox" name="p450alert" value="yes" <?php if($alert_category){ echo 'checked'; } ?> /></td></tr>
				<tr id="p450-alert-category" <?php if(!$alert_category){ echo 'style="display:none;"'; } else{ echo 'style="display: table-row;"'; } ?>>
					<th>P450 Alert Category</th>
					<td>
					<select name="p450_alert_category">
						<option value="Inducers" <?php if('Inducers'==$alert_category){ echo "selected='selected'";} ?>>Inducers</option>
						<option value="Substrates" <?php if('Substrates'==$alert_category){ echo "selected='selected'";} ?>>Substrates</option>
						<option value="Pro-drugs" <?php if('Pro-drugs'==$alert_category){ echo "selected='selected'";} ?>>Pro-drugs</option>
					</select>
				</td></tr>
				
				
			</table>

			<div class="wrap text-center">
				<input type='submit' name="update" value='Save' class='button button-primary button-large'> &nbsp;&nbsp;
				<input type='submit' name="delete" value='Delete' class='button button-primary button-large' onclick="return confirm('Do you want to delete the record?')">
			</div>
		</form>
	<?php }?>
	</div>
<?php
} ?>