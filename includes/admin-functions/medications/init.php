<?php

//menu items
add_action('admin_menu','admin_medication_menu');
function admin_medication_menu() {
	
	//this is the main item for the menu
	add_menu_page('Medication List', //page title
	'Medication', //menu title
	'manage_options', //capabilities
	'medication_list', //menu slug
	'medication_list' //function
	);
	
	//this is a submenu
	add_submenu_page('medication_list', //parent slug
	'Add New Medication', //page title
	'Add New', //menu title
	'manage_options', //capability
	'medication_create', //menu slug
	'medication_create'); //function
	
	//this submenu is HIDDEN, however, we need to add it anyways
	add_submenu_page(null, //parent slug
	'Update Mediaction', //page title
	'Update', //menu title
	'manage_options', //capability
	'medication_update', //menu slug
	'medication_update'); //function
}
?>