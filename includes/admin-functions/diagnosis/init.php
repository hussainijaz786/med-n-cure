<?php

//menu items
add_action('admin_menu','admin_diagnosis_menu');
function admin_diagnosis_menu() {
	
	//this is the main item for the menu
	add_menu_page('Diagnosis LIST', //page title
	'Diagnosis', //menu title
	'manage_options', //capabilities
	'diagnosis_list', //menu slug
	'diagnosis_list' //function
	);
	
	//this is a submenu
	add_submenu_page('diagnosis_list', //parent slug
	'Add New Diagnosis', //page title
	'Add New', //menu title
	'manage_options', //capability
	'diagnosis_create', //menu slug
	'diagnosis_create'); //function
	
	//this submenu is HIDDEN, however, we need to add it anyways
	add_submenu_page(null, //parent slug
	'Update Diagnosis', //page title
	'Update', //menu title
	'manage_options', //capability
	'diagnosis_update', //menu slug
	'diagnosis_update'); //function
}
?>