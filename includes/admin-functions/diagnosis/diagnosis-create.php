<?php
function diagnosis_create () {
	global $wpdb;
	$id = !isset($_GET['id'])?null:$_GET['id'];
	$message = $category = "";
	if(@isset($_POST['insert'])){
		$category 		=	$_POST["category"];
		if($category == '--New Category--'){
			$category 		=	$_POST["new_category"];
		}
		$diagnosis 	    = 	$_POST["diagnosis"];
		$icd_code		=	$_POST["icd_code"];
		$additional_description  =	$_POST["additional_description"];
		$status 		=	$_POST["status"];
		$wpdb->query("INSERT INTO `wp_disease` SET `category` = '$category', diagnosis='$diagnosis', icd_code = '$icd_code' , additional_description = '$additional_description', status = '$status'");
		$message="Diagnosis inserted!";
	}
	?>
	<div class="wrap">
		<h2>Add New Diagnosis</h2>
		<p>
			<a href="<?php echo admin_url('admin.php?page=diagnosis_create'); ?>" class="page-title-action">Add New</a>
		</p><br/>
		<?php if (isset($message) && !empty($message)): ?>
			<div class="updated"><p><?php echo $message;?></p></div>
		<?php endif;?>
		<form method="post" action="<?php echo $_SERVER['REQUEST_URI']; ?>">
			<table class='wp-list-table widefat fixed'>
				<tr><th>Category</th><td>
					<select name="category" class="diagnosis_select" data-id="1">
					<option value=""> Select Category</option>
					<option value="--New Category--">--New Category--</option>
					<?php  $categories = $wpdb->get_results("SELECT distinct(category) from wp_disease");
					foreach ($categories as $ct ){
						if($ct->category !=''){?>
							<option value="<?php echo $ct->category;?>" <?php if($ct->category==$category){ echo "selected='selected'";} ?>><?php echo $ct->category;?></option>
						<?php } 
					} ?>
					</select>
				</td></tr>
				
				
				<tr id="diagnosis_tr1"><th>New Category</th><td><input type="text" name="new_category" value=""/></td></tr>
				
				<tr><th>Diagnosis</th><td><input type="text" name="diagnosis" value=""/></td></tr>
				<tr><th>Icd Code</th><td><input type="text" name="icd_code" value=""/></td></tr>
				<tr><th>Additional Description</th><td><input type="text" name="additional_description" value=""/></td></tr>
				<tr><th>Status</th><td>
					<select name="status">
					<option value="" selected='selected'> Select Status</option>
					<option value="0"> Approve</option>
					<option value="1"> Unapprove</option>
					</select>
				</td></tr>
			</table>
			<br/>
			<div class="wrap text-center">
				<input type='submit' name="insert" value='Save' class='button button-primary button-large'>
			</div>
		</form>
	</div>
	<?php
}?>