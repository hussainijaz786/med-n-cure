<?php
function diagnosis_update () {
	global $wpdb;
	
	$id = !isset($_GET['id'])?null:$_GET['id'];
	//update
	if(@isset($_POST['update'])){
		$category 		=	$_POST["category"];
		if($category == '--New Category--'){
			$category 		=	$_POST["new_category"];
		}
		$diagnosis 	    = 	$_POST["diagnosis"];
		$icd_code		=	$_POST["icd_code"];
		$additional_description  =	$_POST["additional_description"];
		$status 		=	$_POST["status"];
		$wpdb->query("UPDATE `wp_disease` SET `category` = '$category', diagnosis='$diagnosis', icd_code = '$icd_code' , additional_description = '$additional_description', status = '$status' where id = $id");
	}
	//delete
	else if(@isset($_POST['delete'])){
		$wpdb->query($wpdb->prepare("DELETE FROM wp_disease WHERE id = %s",$id));
	}
	else{//selecting value to update	
		$schools = $wpdb->get_results($wpdb->prepare("SELECT * from wp_disease where id=%s",$id));
		foreach ($schools as $s ){
			$category=$s->category;
			$diagnosis=$s->diagnosis;
			$icd_code=$s->icd_code;
			$additional_description=$s->additional_description;
			$status=$s->status;
			//echo "<h2>Diagnosis Updated</h2>";
		}
	}
	?>
	<div class="wrap">
	<h2>Diagnosis List</h2>
	<p>
		<a href="<?php echo admin_url('admin.php?page=diagnosis_create'); ?>" class="page-title-action">Add New</a>
	</p><br/>
	<?php if(@$_POST['delete']){?>
		<div class="updated"><p>Diagnosis deleted!</p></div>
		<a href="<?php echo admin_url('admin.php?page=diagnosis_list')?>">&laquo; Back to Diagnosis List</a>
	<?php } else if(@$_POST['update']) {?>
		<div class="updated"><p>Diagnosis updated!</p></div>
		<a href="<?php echo admin_url('admin.php?page=diagnosis_list')?>">&laquo; Back to Diagnosis List</a>
	<?php } else {?>
		<form method="post" action="<?php echo $_SERVER['REQUEST_URI']; ?>">
			<table class='wp-list-table widefat fixed'>
			
				<tr><th>Category</th><td>
					<select name="category" class="diagnosis_select" data-id="2">
					<option value=""> Select Category</option>
					<option value="--New Category--">--New Category--</option>
					<?php  $categories = $wpdb->get_results("SELECT distinct(category) from wp_disease");
					foreach ($categories as $ct ){
						if($ct->category !=''){?>
							<option value="<?php echo $ct->category;?>" <?php if($ct->category==$category){ echo "selected='selected'";} ?>><?php echo $ct->category;?></option>
						<?php } 
					} ?>
					</select>
				</td></tr>
				
				<tr id="diagnosis_tr2"><th>New Category</th><td><input type="text" name="new_category" value=""/></td></tr>
				
				<tr><th>Diagnosis</th><td><input type="text" name="diagnosis" value="<?php echo $diagnosis;?>"/></td></tr>
				<tr><th>Icd Code</th><td><input type="text" name="icd_code" value="<?php echo $icd_code;?>"/></td></tr>
				<tr><th>Additional Description</th><td><input type="text" name="additional_description" value="<?php echo $additional_description;?>"/></td></tr>
				<tr><th>Status</th><td>
					<select name="status">
						<option value=""> Select Status</option>
						<option value="0" <?php if($status==0){ echo "selected='selected'";} ?>> Approve</option>
						<option value="1" <?php if($status==1){ echo "selected='selected'";} ?>> Unapprove</option>
					</select>
				</td></tr>
			</table>
			<div class="wrap text-center">
				<input type='submit' name="update" value='Save' class='button button-primary button-large'> &nbsp;&nbsp;
				<input type='submit' name="delete" value='Delete' class='button button-primary button-large' onclick="return confirm('Do you want to delete the record?')">
			</div>
		</form>
	<?php }?>
	</div>
<?php
} ?>