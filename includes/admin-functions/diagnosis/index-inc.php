<?php 

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly 
// Include Custom Post Types Files
require_once('init.php');
require_once('diagnosis-list.php');
require_once('diagnosis-create.php');
require_once('diagnosis-update.php');
?>