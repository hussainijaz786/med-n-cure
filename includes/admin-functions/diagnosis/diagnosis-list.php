<?php
function diagnosis_list () {
	?><div class="wrap">
	<h2>Diagnosis List</h2>
	<p>
		<a href="<?php echo admin_url('admin.php?page=diagnosis_create'); ?>" class="page-title-action">Add New</a>
	</p><br/>
	<?php
	global $wpdb;
	
	// Delete one by one
	if(isset($_GET['action']) && $_GET['action'] == 'delete'){
		$post_id = isset($_GET['id']) ? $_GET['id'] : '';
		if($post_id != ''){
			$wpdb->query($wpdb->prepare("DELETE FROM wp_disease WHERE id = %s",$post_id));
		}
		echo '<div class="updated"><p>Diagnosis deleted!</p></div>';
	}
	
	// Delete Check Records
	if(isset($_POST['delete_all'])){
		if(isset($_POST['diagnosis_check'])){
			foreach($_POST['diagnosis_check'] as $diagnosis_check){
				$wpdb->query($wpdb->prepare("DELETE FROM wp_disease WHERE id = %s",$diagnosis_check));
			}
		}
		echo '<div class="updated"><p>Diagnosis deleted!</p></div>';
	}
	
	//filter for active/inactive
	$query_part = '';
	if(isset($_GET['diag_status'])){
		$query_part = ($_GET['diag_status'] == 0) ? "where status='0'" : "where status='1'";
	}
	$rows = $wpdb->get_results("SELECT * from wp_disease ".$query_part);
	
	echo '<form method="post" action="'.$_SERVER['REQUEST_URI'].'">';
	
	echo '<a class="button button-primary button-large" href="'.admin_url('admin.php?page=diagnosis_list').'">All</a>&nbsp;&nbsp;&nbsp;';
	echo '<a class="button button-primary button-large" href="'.admin_url('admin.php?page=diagnosis_list&diag_status=0').'">Active</a>&nbsp;&nbsp;&nbsp;';
	echo '<a class="button button-primary button-large" href="'.admin_url('admin.php?page=diagnosis_list&diag_status=1').'">Inactive</a>&nbsp;&nbsp;&nbsp;';
	?><input type="submit" class="button button-primary button-large" name="delete_all" value="Delete" onclick="return confirm('Do you want to delete the record?')"/><br><br><?php
	
	echo "<table data-toggle='table' data-striped='true' class='table table-hover fixed table-striped'>";
	echo "<thead><tr><th><input type='checkbox' name='all_diagnosis_select' class='all_diagnosis_select' style='margin: 0;' /></th><th>Category</th><th>Disease</th><th>Icd Code</th><th>Status</th><th>Action</th></tr></thead><tbody>";
	foreach ($rows as $row ){
		echo "<tr>";
		echo "<td><input type='checkbox' name='diagnosis_check[]' class='diagnosis_check' value='".$row->id."' /></td>";
		echo "<td>$row->category</td>";	
		echo "<td>$row->diagnosis</td>";
		echo "<td>$row->icd_code</td>";
		if($row->status == 0){
			echo "<td>Approved</td>";	
		}else{
			echo "<td>Unapproved</td>";	
		}
		echo "<td>
			<a href='".admin_url('admin.php?page=diagnosis_update&id='.$row->id)."'>EDIT</a> / ";
			?><a href="<?php echo admin_url('admin.php?page=diagnosis_list&action=delete&id='.$row->id); ?>" onclick="return confirm('Do you want to delete the record?')">DELETE</a><?php
		echo "</td>";
		echo "</tr>";
	}
	echo "</tbody></table>";
	echo '</form>';
	?>
	</div>
	<?php
}?>