<?php 

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly 


add_action('acf/save_post', 'add_new_physician');

function add_new_physician( $post_id ) {
	
	// bail early if not a contact_form post
	
	//$post = get_post( $post_id );
	
	if( get_post_type($post_id) == 'physicians' && is_admin()) {
		// bail early if editing in admin
		
		$first_name = get_field('first_name', $post_id);
		$last_name = get_field('last_name', $post_id);
		$url = get_field('url', $post_id);
		$logo = get_post_meta($post_id, 'logo', true);
		$password = get_field('password', $post_id);
		
		$user_name = get_field('email', $post_id);
		$user_email = $user_name;
		//$user_id = username_exists( $user_name );
		$user_id = email_exists( $user_name );
		if($user_id) {
			//$response = array('action' => false, 'refresh' => '', 'feedback' => 'This is USER ID: '.$user_id.'<br>'. $user_email. ' User already exists!<br>');
			
			$setuserid = wp_update_user( array( "ID" => $user_id, "first_name" => $first_name, "last_name" => $last_name, "display_name" => $first_name  ) );
			update_user_meta( $user_id, 'email', $user_name );
			update_user_meta( $user_id, 'physician_post_id', $post_id );
			update_post_meta( $post_id, 'physician_user_id', $user_id );
			wp_set_password( $password, $user_id );
		}
		else{
			$notify = "admin";
		
			$user_id = wp_create_user( $user_name, $password, $user_email );
			
			$user_id_role = new WP_User($user_id);
			
			//$userrole = get_user_role();
			$role = 'physician';
			
			$user_id_role->set_role($role);
			$now = date("Y-m-d H:i:s");
			$setuserid = wp_update_user( array( "ID" => $user_id, "first_name" => $first_name, "last_name" => $last_name, "display_name" => $first_name, "nickname" => $first_name ) );
			
			if ( is_wp_error( $setuserid ) ) {
				$response = array('action' => false, 'refresh' => '', 'feedback' => 'Some Error has Occured!');
			}
			else{
				//wp_new_user_notification( $user_id, $deprecated, $notify );
				
				update_user_meta( $user_id, 'user_child_count', 0 );
				update_user_meta( $user_id, 'physician_post_id', $post_id );
				update_post_meta( $post_id, 'physician_user_id', $user_id );
				
				$response = array('action' => true, 'refresh' => true, 'feedback' => 'New User added Successfully!');
				
				$password_reset_url = password_reset_link($user_id);
				
				$args = array(
					'call_by' => 'post-registeration-physician',
					'receiver' => $user_email, 
					'subject_name' => 'Dr.'.$first_name.', Your MednCures Instance is Ready!', 
					'body_name' => get_the_title($post_id), 
					'body_part' => '', 
					'password_reset_url' => $password_reset_url,
				);	
				cdrmed_send_email($args);
			}
		}
		
		update_user_meta( $user_id, 'email', $user_name );
		update_user_meta( $user_id, 'url', rtrim($url,"/") );
		update_user_meta( $user_id, 'logo', $logo );
		
	}
	else{
		return;
	}
	
}


?>