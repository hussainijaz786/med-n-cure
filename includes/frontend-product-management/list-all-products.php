<?php 
if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly 
/** 
Author: Hassan
Dated: 12-August-2016
Description: Show All Products on Physician or Dispenser Side so they may edit it on Frontend
Reference: https://www.gavick.com/blog/wp_query-woocommerce-products
**/



function list_all_wc_products(){
// Pass Parameters such as how much and which products to show
	$url = site_url( '/inventory-list', 'http' );
	$userrole = get_user_role();
	$siteurl = site_url();
	if( is_user_logged_in() && $userrole == 'dispensary'){
		if(isset($_GET['action']) && isset($_GET['page_type'])){
			return do_shortcode('[inventory-form]');
		}
		else{
			return do_shortcode('[inventory-list]');
		}
	}
	else{
		
		return do_shortcode('[patient-inventory-list]');
	}
}

add_shortcode('list-all-wc-products','list_all_wc_products');


function patient_inventory_list(){
	/* extract( shortcode_atts( array(
        'dispensar_id' => '',
    ), $atts ) );
	
	return $dispensar_id;
	die(); */
	
	$dispensar_id = isset($_GET['dis']) ? $_GET['dis'] : '';
	
	$html = '<div class="inventry-products">';
		$paged = ( get_query_var( 'paged' ) ) ? absint( get_query_var( 'paged' ) ) : 1;
		
		$params = array(
			'posts_per_page' => 20,
			'post_type' => 'product',
			'paged' => $paged,
			'meta_query' => array(
				array(
					'key' => 'product_dispensary_id',
					'value' => $dispensar_id,
					'compare' => '=',
				)
			)
		);
		$wc_query = new WP_Query($params);
		$html .= '<div class="grid-list">';
			$html .= '<div class="row">';
				$html .= '<div class="frontend-products-row">';
				
				// Outer If Condition if Posts Exists
				if ($wc_query->have_posts()) :
				
					// While Condition if There are posts then Display
					while ($wc_query->have_posts()) :
						$wc_query->the_post();
						$product_id = get_the_ID();
						//$product = new WC_Product($product_id);
						$product_title = get_the_title();
						$product_price = get_post_meta($product_id, '_price', TRUE);
						$product_price = (!empty($product_price) ? $product_price : 'N / A');
						$product_quantity = floor(get_post_meta($product_id, '_stock', TRUE));
						$product_sku = get_post_meta($product_id, '_sku', TRUE);
						$product_sku = (!empty($product_sku) ? $product_sku : 'N / A'); 
						$product_image_id = get_post_meta($product_id, '_thumbnail_id', TRUE);
						if($product_image_id) {
							$product_image = wp_get_attachment_image_src($product_image_id, 'inventry-list-thumb');
							$product_image = $product_image[0];
						} else {
							$product_image = "http://placehold.it/255x255";
						}
						// Actual HTML Starts Here
						$html .= '<div class="col-xs-6 col-sm-3">';
							$html .= '<div class="each-product-single" data-pro-id="'.$product_id.'">';
			
								// Product Image Displays Here
								$html .= '<div class="pro-single-image">';
									$html .= '<img src="'.$product_image.'">';
								$html .= '</div>';
			
								//Product Price and Other Meta Data
								$html .= '<div class="pro-single-meta">';
			
									// Product Title
									$html .= '<div class="pro-single-title">';
										$html .= '<a href="'.get_permalink().'">'.$product_title.'</a>';
									$html .= '</div>';
			
									$html .= '<div class="pro-single-price-sku">';
										// Product Price
										$html .= '<div class="pro-single-price">';
										$html .= 'Price: ($)<span>'.$product_price.'</span>';
										$html .= '</div>';
				
										// Product SKU
										$html .= '<div class="pro-single-sku">';
										$html .= 'Quantity: <span>'.$product_quantity.'</span>';
										$html .= '</div>';
									// End Price SKU Container
									$html .= '</div>';
								
									$html .= '<div class="pro-single-price-sku">';
										// Product Quantity
										$html .= '<div class="pro-single-price">';
										$html .= 'SKU: <span>'.$product_sku.'</span>';
										$html .= '</div>';
									// End Price SKU Container
									$html .= '</div>';
								// End Product Meta Class
								$html .= '</div>';
			
							// Each Product Single and 3 Grid Column Ends Here
							$html .= '</div>
						</div>';
					
					// End While Condition
					endwhile;
				
					// SHOW Pagination
					if ($wc_query->max_num_pages > 1) {
						$html .= '<div class="inventory-pag">';
						$html .= paginate_links( apply_filters( 'woocommerce_pagination_args', array(
							'base'         => esc_url_raw( str_replace( 999999999, '%#%', remove_query_arg( 'add-to-cart', get_pagenum_link( 999999999, false ) ) ) ),
							'format'       => '',
							'add_args'     => false,
							'current'      => max( 1, get_query_var( 'paged' ) ),
							'total'        => $wc_query->max_num_pages,
							'prev_text'    => 'Previous',
							'next_text'    => 'Next',
							'type'         => 'list',
							'end_size'     => 3,
							'mid_size'     => 3
						) ) );
						
						$html .= '</div>';
					}
					
					// Always reset post query so it may not conflict with anything else
					 wp_reset_postdata();
					
					// Else if there are no products at alls
				else: 
					$html .= _e( '<h4 style="text-align: center;">No Products</h4>' );
				// END outer IF Condition
				endif;
	
				$html .= '</div>';
			$html .= '</div>';
		$html .= '</div>';
	$html .= '</div>';
		
	return $html;
	
}
add_shortcode('patient-inventory-list','patient_inventory_list');

/*
*
*
*		Shortcode for all Inventory Products
*
*/

function inventory_list(){
	//$current_usid = get_current_user_id();
	$current_usid = (isset($_GET['did']) ? $_GET['did'] : get_current_user_id());
	$patient_dispensary_id = $current_usid;
	//echo $patient_dispensary_id;
	$child_user = get_user_meta($patient_dispensary_id, 'user_parent', true);
	if($child_user != ''){
		$patient_dispensary_id = get_user_meta($patient_dispensary_id, 'user_parent_id', true);
	}
	
	$html = '<div class="inventry-products">
		<span class="inventory-product-feedback feedback"></span>';
		$paged = ( get_query_var( 'paged' ) ) ? absint( get_query_var( 'paged' ) ) : 1;
		$post_per_page = (isset($_GET['layout-type']) && $_GET['layout-type'] == 'grid') ? 20 : -1;
		$params = array(
			'posts_per_page' => $post_per_page,
			'post_type' => 'product',
			'paged' => $paged,
			'meta_query' => array(
				array(
					'key' => 'product_dispensary_id',
					'value' => $patient_dispensary_id,
					'compare' => '=',
				)
			)
		);
		$wc_query = new WP_Query($params);

		//echo "<pre>";print_r($wc_query);echo "</pre>";
		$layout_type = '';
		//if(isset($_GET['layout-type']))
		$layout_type = (isset($_GET['layout-type']) && $_GET['layout-type'] == 'grid') ? '' : '?layout-type=grid';
		// Define Top row and an internal container for own use as parent
		$url = site_url( '/inventory-list', 'http' );
		
		$product_csv_template = CDRMED.'/includes/assets/sample-data/Product_Template.csv' ;
		
		$html .= '<div class="row"><div class="col-md-12"><div class="topbar">
		
		
		<div class="lab_result_group import_product_group">
			
			<a class="btn btn-primary new-product" style="float: left;" href="'.$product_csv_template.'">Product Template</a>
									
			<label for="product_cvs_import" id="lab_result_label" class="lab_result_label">
				<div class="new-file-group">
					<span class="button button-primary new-product product_select_import" id="product_select_import">Import CSV Product</span>
				</div>
			</label>
			<input name="lab_result_cvs_import" id="product_cvs_import" class="input-file prod-inp product_cvs_import" type="file" value="Import CVS Product" style="visibility: hidden; position: absolute;">
		</div>
		
		
		
		
		<a class="btn btn-primary new-product" href="'.$url.$layout_type.'"><i class="fa fa-bars" aria-hidden="true"></i></a> <a class="btn btn-primary new-product" href="'.$url.'?action=add-product&page_type=inventory-list" data-target="">Add New Item</a></div></div></div>';
		
		
		$html .= '<div class="grid-list">';
		
		
		$html .= '<div class="row">';
			$html .= '<div class="frontend-products-row">';
			
			
			
			if(isset($_GET['layout-type']) && $_GET['layout-type'] == 'grid'){
				// This is Grid Layout Setting	
				
				// Outer If Condition if Posts Exists
				if ($wc_query->have_posts()) :
				
				// While Condition if There are posts then Display
				while ($wc_query->have_posts()) :
					$wc_query->the_post();
					$product_id = get_the_ID();
					//$product = new WC_Product($product_id);
					$product_title = get_the_title();
					$product_price = get_post_meta($product_id, '_price', TRUE);
					$product_price = (!empty($product_price) ? $product_price : 'N / A');
					$product_quantity = floor(get_post_meta($product_id, '_stock', TRUE));
					$product_sku = get_post_meta($product_id, '_sku', TRUE);
					$product_sku = (!empty($product_sku) ? $product_sku : 'N / A'); 
					$product_image_id = get_post_meta($product_id, '_thumbnail_id', TRUE);
					if($product_image_id) {
						$product_image = wp_get_attachment_image_src($product_image_id, 'inventry-list-thumb');
						$product_image = $product_image[0];
					} else {
						$product_image = "http://placehold.it/255x255";
					}
					// Actual HTML Starts Here
					$html .= '<div class="col-xs-6 col-sm-3">';
						$html .= '<div class="each-product-single" data-pro-id="'.$product_id.'">';
			
							// Product Image Displays Here
							$html .= '<div class="pro-single-image">';
								$html .= '<img src="'.$product_image.'">';
							$html .= '</div>';
			
							//Product Price and Other Meta Data
							$html .= '<div class="pro-single-meta">';
			
								// Product Title
								$html .= '<div class="pro-single-title">';
									$html .= '<a href="'.get_permalink().'">'.$product_title.'</a>';
								$html .= '</div>';
			
								$html .= '<div class="pro-single-price-sku">';
									// Product Price
									$html .= '<div class="pro-single-price">';
									$html .= 'Price: ($)<span>'.$product_price.'</span>';
									$html .= '</div>';
			
									// Product SKU
									$html .= '<div class="pro-single-sku">';
									$html .= 'Quantity: <span>'.$product_quantity.'</span>';
									$html .= '</div>';
								// End Price SKU Container
								$html .= '</div>';
								
								
								$html .= '<div class="pro-single-price-sku">';
									// Product Quantity
									$html .= '<div class="pro-single-price">';
									$html .= 'SKU: <span>'.$product_sku.'</span>';
									$html .= '</div>';
								// End Price SKU Container
								$html .= '</div>';
			
								//$cin = site_url( '/view-complete-intake/', 'http' );
								// Action Buttons
								$html .= '<div class="pro-single-action-buttons">';
									$html .= '<a href="'.$url.'?action=edit-product&page_type=inventory-list&proid='.$product_id.'" data-target="" class="btn btn-primary">Edit Product</a>
									<a data-id="'.$product_id.'" class="btn btn-primary inventory-product-delete">Remove</a>';
								$html .= '</div>';
			
							// End Product Meta Class
							$html .= '</div>';
			
					// Each Product Single and 3 Grid Column Ends Here
					$html .= '</div></div>';
					
					// End While Condition
				endwhile;
				
				
				// SHOW Pagination
				if ($wc_query->max_num_pages > 1) {
					$html .= '<div class="inventory-pag">';
					$html .= paginate_links( apply_filters( 'woocommerce_pagination_args', array(
						'base'         => esc_url_raw( str_replace( 999999999, '%#%', remove_query_arg( 'add-to-cart', get_pagenum_link( 999999999, false ) ) ) ),
						'format'       => '',
						'add_args'     => false,
						'current'      => max( 1, get_query_var( 'paged' ) ),
						'total'        => $wc_query->max_num_pages,
						'prev_text'    => 'Previous',
						'next_text'    => 'Next',
						'type'         => 'list',
						'end_size'     => 3,
						'mid_size'     => 3
					) ) );
					
					$html .= '</div>';
				}
				
				// Always reset post query so it may not conflict with anything else
				 wp_reset_postdata();
				
				 // Else if there are no products at alls
				else: 
					$html .= _e( 'No Products' );
				// END outer IF Condition
				endif;
			}
			else{
				// Table Headers only to be shown if Layout Type Grid is not set
				 
				$html .= '<table
				data-toggle="table"
				data-url="'.get_admin_url().'admin-ajax.php?action=product_form_ajax_request&request=get-all"
				data-pagination="true"
				data-side-pagination="server"
				data-page-list="[10, 20, 25, 50, 100, 250, 500, 1000, 5000]"
				data-search="false"
				
				data-show-toggle="false"
				data-classes="table table-hover stripped"
				data-striped="true"
				data-show-columns="true"
				data-id-field="id"
				data-page-size="10"
				data-show-export="true"
				data-smart-display="true"
				data-mobile-responsive="true">';
				$html .= '<thead><tr>';
				$html .= '<th data-field="item" data-sortable="true">Item</th>';
				$html .= '<th data-field="category" data-sortable="true">Category</th>';
				$html .= '<th data-field="price" data-sortable="true">Price ($)</th>';
				$html .= '<th data-field="quantity" data-sortable="true">Quantity</th>';
				$html .= '<th data-field="sku" data-sortable="true">SKU</th>';
				 $html .= '<th data-field="action">Action</th>';
				$html .= '</tr></thead>';
				$html .= '</table>';
			}
			
			// Top Most Outer Row and Frontend Products Row Ends Here
			$html .= '</div>';
		$html .= '</div>';
		$html .= '</div>';
	$html .= '</div>';
		
	return $html;
	
}

add_shortcode('inventory-list','inventory_list');



/*
*
*
*		Shortcode for Add/Edit Inventory Product
*
*/

function inventory_page_title($title_parts) {
   if(is_page('inventory-list')){
	   
   }
}

add_filter( 'inventory_page_title', 'inventory_page_title');

function inventory_form(){
	
	ob_start();
	
	$title = $product_id = $product_title = $product_price = $product_sku = $product_description = $product_quantity = $product_image = '';
		
		$action = $_GET['action'];
		$page_type = $_GET['page_type'];
		
		
		$btn_text = 'Save Item';
		
		$lab_result_url = $lab_result_title = '';
		
		if($action == 'edit-product'){
			$btn_text = 'Update';
			$product_id = isset($_GET['proid']) ? $_GET['proid'] : '';
			$product_title = get_the_title($product_id);
			$title = $product_title;
			$product_price = get_post_meta($product_id, '_price', TRUE);
			$product_sku = get_post_meta($product_id, '_sku', TRUE);
			$product_description = get_post_field('post_content', $product_id);;//get_the_content($product_id);//get_post_meta($product_id, '_purchase_note', TRUE);
			$product_quantity = get_post_meta($product_id, '_stock', TRUE);
			$lab_result_attachment_id = get_post_meta($product_id, '_front_product_lab_result_file', TRUE);
			$lab_result_url = wp_get_attachment_url($lab_result_attachment_id);
			$lab_result_title = get_the_title($lab_result_attachment_id);
			
			$product_image_id = get_post_meta($product_id, '_thumbnail_id', TRUE);
			if($product_image_id) {
				$product_image = wp_get_attachment_image_src($product_image_id, 'single-post-thumbnail');
				$product_image = $product_image[0];
			} else {
				$product_image = "http://placehold.it/500x400";
			}
			
			//apply_filters('inventory_page_title', $atts);
			apply_filters('inventory_page_title', 'Edit Inventory');
		}
		elseif($action == 'add-product'){
			
			//add_filter( 'the_title', 'custom_title', 10, 1, 'New Inventory' );
			apply_filters('inventory_page_title', 'New Inventory');
			
			$title = 'New Item';
			$product_image = "http://placehold.it/500x400";
		}
		
		$form_submit_url = get_admin_url().'admin-ajax.php?action=product_form_ajax_request';
		
		?>
		<div class="product-inventory-form">
			<form class="form-horizontal" method="post" name="inventory-form" id="inventory-form" action="<?php echo $form_submit_url; ?>" enctype="multipart/form-data">
				<input type="hidden" id="product_id" name="product_id" value="<?php echo $product_id; ?>">
				<input type="hidden" id="featured_image_change" name="featured_image_change" value="">
				<input type="hidden" id="featured_image_src" name="featured_image_src" value="">
				<input type="hidden" id="lab_result_src" name="lab_result_src" value="">
				<fieldset>
					
					<!-- Form Name -->
					<legend><?php echo $title; ?></legend>
					
					<div class="inventory-form-default">
						<div class="col-xs-12 col-md-6 left-col">
							<div class="form-group">
								<div class="featured_image_group">
									<label for="featured_image" id="featured_image_label" class="featured_image_label">
										<div class="new-file-group">
											<span id="change_featured_image_btn " class="change_featured_image_btn"><i class="fa fa-edit"></i></span>
											<img id="featured_img" src="<?php echo $product_image; ?>" alt="<?php echo $product_title; ?>">
										</div>
									</label>
									<input id="featured_image" name="featured_image" id="featured_image" class="input-file prod-inp" type="file" value="Choose Image" style="visibility: hidden; position: absolute;">
								</div>
								<div class="lab_result_group">
									<label for="lab_result" id="lab_result_label" class="lab_result_label">
										<div class="new-file-group">
											<span class="button button-primary lab_result_select" id="lab_result_select">Upload Lab Results</span>
										</div>
									</label>
									<input name="lab_result" id="lab_result" class="input-file prod-inp" type="file lab_result" value="Upload Lab Results" style="visibility: hidden; position: absolute;">
									<div class="lab-result-show">
										<a target="_blank" href="<?php echo $lab_result_url ?>"><?php echo $lab_result_title; ?></a>
									</div>
								</div>
                                <div class="lab_result_download">
									<a class="button cdrmed-primary-button" href="<?php echo plugins_url( 'cdrmed/includes/assets/sample-data/Lab_Results_Template.csv' ) ?>">Lab Result Template</a>
                               
								 </div>
								 
								 <div class="lab_result_group">
										
									<label for="lab_result_cvs_import" id="lab_result_label" class="lab_result_label">
										<div class="new-file-group">
											<span class="button button-primary lab_result_select_import" id="lab_result_select_import">Import CSV Lab Results</span>
										</div>
									</label>
									<input name="lab_result_cvs_import" id="lab_result_cvs_import" class="input-file prod-inp lab_result_cvs_import" type="file" value="Import Import CVS Lab Results" style="visibility: hidden; position: absolute;">
									
								</div>
								
							</div>
						</div>
						
						<div class="col-xs-12 col-md-6 right-col">
							
							
							<!-- Text input-->
							<div class="form-group">
								<label class="col-md-3 control-label" for="product_title">Title</label>  
								<div class="col-md-9">
									<input id="product_title" name="product_title" type="text" placeholder="" class="form-control input-md prod-inp" required="" value="<?php echo $product_title; ?>">
								</div>
							</div>

							<!-- Text input-->
							<div class="form-group">
								<label class="col-md-3 control-label" for="product_price">Price ($)</label>  
								<div class="col-md-9">
									<input id="product_price" name="product_price" type="text" placeholder="" class="form-control input-md prod-inp" required="" value="<?php echo $product_price; ?>">
								</div>
							</div>

							<!-- Text input-->
							<div class="form-group">
								<label class="col-md-3 control-label" for="product_sku">SKU</label>  
								<div class="col-md-9">
									<input id="product_sku" name="product_sku" type="text" placeholder="" class="form-control input-md prod-inp" required="" value="<?php echo $product_sku; ?>">
								</div>
							</div>

							<!-- Text input-->
							<div class="form-group">
								<label class="col-md-3 control-label" for="product_quantity">Quantity</label>  
								<div class="col-md-9">
								<input id="product_quantity" name="product_quantity" type="text" placeholder="" class="form-control input-md prod-inp" required="" value="<?php echo floor($product_quantity); ?>">
								</div>
							</div>
							
							
							<div class="form-group">
								<label class="col-md-3 control-label" for="product_quantity">Category</label>  
								<div class="col-md-9">
								<?php
								$parent_cat_id_arr = array();
								if(!empty($product_id)){
									$terms = get_the_terms( $product_id, 'product_cat');
									if(sizeof($terms) > 0 && $terms != ''){
										foreach ($terms as $term) {
											$parent_cat_id_arr[] = $term->term_id;
										}
									}
								}
								$taxonomy = 'product_cat';
								$orderby = 'name';
								$show_count = 0; // 1 for yes, 0 for no
								$pad_counts   = 0; // 1 for yes, 0 for no 
								$hierarchical = 1;      // 1 for yes, 0 for no  
								$title = ''; 
								$empty = 0;
								$args = array(
									'taxonomy' => $taxonomy, 'orderby' => $orderby, 'show_count' => $show_count, 'pad_counts'   => $pad_counts, 'hierarchical' => $hierarchical, 'title_li'  => $title, 'hide_empty' => $empty
								);
								
								$all_categories = get_categories( $args );
				 
								$all_category_select = '<select name="all_category_select" id="all_category_select" class="all_category_select">';
								$all_category_select .= '<option value="">-- Parent Product Category --</option>';
								$product_category = '';
								foreach ($all_categories as $cat) {
									if($cat->category_parent == 0) {
										$category_id = $cat->term_id;
										$checked = '';
										if(in_array($category_id,$parent_cat_id_arr)){
											$checked = 'checked';
										}
										$all_category_select .= '<option value="'.$cat->term_id.'">'. $cat->name .'</option>';
										$product_category .= '<label class="category_label parent'.$cat->term_id.'"><input type="checkbox" name="product_category[]" '.$checked.' value="'. $cat->term_id .'">'. $cat->name .'</label>';
										$args2 = array(
												'taxonomy'     => $taxonomy,
												'child_of'     => 0,
												'parent'       => $category_id,
												'orderby'      => $orderby,
												'show_count'   => $show_count,
												'pad_counts'   => $pad_counts,
												'hierarchical' => $hierarchical,
												'title_li'     => $title,
												'hide_empty'   => $empty
										);
										$sub_cats = get_categories( $args2 );
										if($sub_cats) {
											foreach($sub_cats as $sub_category) {
												
												$sub_category_id = $sub_category->term_id;
												$checked = '';
												if(in_array($sub_category_id,$parent_cat_id_arr)){
													$checked = 'checked';
												}
												$all_category_select .=  '<option value="'.$sub_category->name.'">&nbsp&nbsp&nbsp'.$sub_category->name.'</option>';
												$product_category .= '<label class="category_label child"><input type="checkbox" name="product_category[]" '.$checked.' value="'. $sub_category->term_id .'">'. $sub_category->name .'</label>';
											}
										}
										//Fixed by Hassan due to warning being generated.
										// Dated: 14/10/2016 7:20PM
										//$parent++;
									}
								}
								$all_category_select .= '</select>';
								?>
									<div class="product_categories">
										<div class="all-category" id="all-category">
											<?php echo $product_category; ?>
										</div>
										<a id="product_cat_toogle">+ Add New Product Category</a>		
										<div class="new_category" id="new_category_group">
											<?php echo $all_category_select; ?>
											<input type="text" name="new_category" id="new_category" class="form-control input-md prod-inp" placeholder="New Category Name">
											<input type="button" id="new_category_btn" class="button new_category_btn" value="Add New Product Category">
										</div>
									</div>					 
								</div>
							</div>

							<!-- Textarea -->
							<div class="form-group">
								<label class="col-md-3 control-label" for="product_description">Description</label>
								<div class="col-md-9">                     
									<textarea class="form-control prod-inp" rows="5" id="product_description" name="product_description"><?php echo $product_description; ?></textarea>
								</div>
							</div>
							
						</div>
						
					</div>
					
					
					
					
					
					<div class="inventory-form-custom">
						<div class="col-xs-12 col-md-12">
							<div class="form-group">
								<?php
								/* $tab_data = maybe_unserialize( get_post_meta( $product_id, 'frs_woo_product_tabs', true ) );
								if ( empty( $tab_data ) ) {
									$tab_data[] = array( 'title' => '', 'content' => '' );
								}
								foreach ( $tab_data as $tab ) */
								{
								?>
								
								<div class="outer-wrap"><ul class="nav nav-pills patient-tabs phytb physician-side-tabs" id="inventory-tabs">
									<!--<li><a href="#tabs-0" data-toggle="tab">Test Details</a></li>-->
									<li class="active"><a href="#tabs-1" data-toggle="tab">Potency Test Results</a></li>
									<li><a href="#tabs-2" data-toggle="tab">Terpene Test Results</a></li>
									<li><a href="#tabs-3" data-toggle="tab">Residual Solvent Results</a></li>
								</ul></div>
								
								<div id="my-tab-content" class="tab-content">
									<!--<div id="tabs-0" class="tab-pane fade in active">text here 1</div>-->
									
									<div id="tabs-1" class="tab-pane fade in active">
										<table>
											<thead>
												<tr><th>Compounds</th><th>Tests</th><th>mg/grm</th><th>PPM</th></tr>
											</thead>
											<tbody>
											<?php
											
												$lab_result_counter = 0;
												
												$potency_arr = array('thc','thca','thcv','cbd','cbda','cbg','cbn','cbc');
												$tests = 'tests';
												$ppm = 'ppm';
												for($i = 0; $i < count($potency_arr);  $i++){
													$key1 = "_azcl_".$potency_arr[$i];
													$key2 = "_azcl_".$potency_arr[$i]."_".${$tests};
													$key3 = "_azcl_".$potency_arr[$i]."_".${$ppm};
													
													echo '<tr><td>'.ucwords($potency_arr[$i]).'</td>';
													echo '<td><input type="text" class="tp lb_'.$lab_result_counter.'"  name="'.$key1.'" id="'.$key1.'" value="'.get_post_meta( $product_id, $key1, true ).'" placeholder=""></td>';
													echo '<td><input type="text" class="tp lb_m_'.$lab_result_counter.'"  name="'.$key2.'" id="'.$key2.'" value="'.get_post_meta( $product_id, $key2, true ).'" placeholder=""></td>';
													echo '<td><input type="text" class="tp ppm "  name="'.$key3.'" id="'.$key3.'" value="'.((get_post_meta( $product_id, $key3, true ) == "")? "0.001" : get_post_meta( $product_id, $key3, true )).'" placeholder=""></td></tr>';
													$lab_result_counter++;
												}
												$array =   explode(",",str_replace(']','',str_replace('&quot;','',str_replace('[','',get_post_meta( $product_id, '_azcl_compunds', true )))));
												$arr2 =   explode(",",str_replace(']','',str_replace('&quot;','',str_replace('[','',get_post_meta( $product_id, '_azcl_potency', true )))));
												$arr3 =   explode(",",str_replace(']','',str_replace('&quot;','',str_replace('[','',get_post_meta( $product_id, '_azcl_potency_tests', true )))));
												$arr4 =   explode(",",str_replace(']','',str_replace('&quot;','',str_replace('[','',get_post_meta( $product_id, '_azcl_potency_ppm', true )))));
							
												$i = 0;
												if($array['0'] != 'null'  && $array['0'] != ''){
													foreach ($array as $value) {
														?>
														<tr><div class="form-field _azcl_cbc_field tp">
														<td><input type="text" class="form-control frst" name="_azcl_compunds[]" value="<?php echo trim($value, '"'); ?>"></td>
														<td><span class="scnd"><input type="text" class="form-control scnd2" name="_azcl_potency[]" value="<?php echo $arr2[$i]; ?>"><span></td>
														<td><input type="text" class="form-control tp2" name="_azcl_potency_tests[]" value="<?php echo $arr3[$i]; ?>"></td>
														<td><input type="text" class="form-control tp2" name="_azcl_potency_ppm[]" value="<?php echo $arr4[$i]; ?>"></td>
														</div></tr>
														<?php
														$i++;
													}
												}
											?>
												<tr  class="input_fields_wrap"><td colspan="4"><button id="add_field_button" class="add_field_button">Add More Fields</button></td></tr>
						
											</tbody>
										</table>
									</div>
									
									
									<div id="tabs-2" class="tab-pane fade">
										<table>
											<thead>
												<tr><th>Compounds</th><th>Tests</th><th>mg/grm</th><th>PPM</th></tr>
											</thead>
											<tbody>
											<?php
												$terpene_label_arr = array('Bisabolol','alpha-Bisabolol','Camphene','Delta-3-Carene','Beta-Caryophyllene','Caryophyllene Oxide','P-Cymene','Geraniol','Guaiol','Alpha-Humulene','Isopulegol','Delta-Limonene','Limonene','Linalool','Myrcene','Nerolidol 1','Nerolidol 2','Ocimene','alpha-Pinene','Beta-Pinene','alpha-Terpinene','Gamma-Terpinene','Terpinolene');
												$terpene_arr = array('bisabolol','alpha_bisabolol','camphene','delta_3_carene','beta_caryophyllene','caryophyllene_oxide','p_cymene','geraniol','guaiol','alpha_humulene','isopulegol','delta_limonene','limonene','linalool','myrcene','nerolidol_1','nerolidol_2','ocimene','alpha_pinene','beta_pinene','alpha_terpinene','gamma_terpinene','terpinolene');
												$tests = 'tests';
												$ppm = 'ppm';
												for($i = 0; $i < count($terpene_label_arr);  $i++){
													$key1 = "_azcl_".$terpene_arr[$i];
													$key2 = "_azcl_".$terpene_arr[$i]."_".${$tests};
													$key3 = "_azcl_".$terpene_arr[$i]."_".${$ppm};
													
													echo '<tr><td>'.$terpene_label_arr[$i].'</td>';
													echo '<td><input type="text" class="tp lb_'.$lab_result_counter.'"  name="'.$key1.'" id="'.$key1.'" value="'.get_post_meta( $product_id, $key1, true ).'" placeholder=""></td>';
													echo '<td><input type="text" class="tp lb_m_'.$lab_result_counter.'"  name="'.$key2.'" id="'.$key2.'" value="'.get_post_meta( $product_id, $key2, true ).'" placeholder=""></td>';
													echo '<td><input type="text" class="tp ppm "  name="'.$key3.'" id="'.$key3.'" value="'.((get_post_meta( $product_id, $key3, true ) == "")? "0.001" : get_post_meta( $product_id, $key3, true )).'" placeholder=""></td></tr>';
													$lab_result_counter++;
												}
												
												$array =   explode(",",str_replace(']','',str_replace('&quot;','',str_replace('[','',get_post_meta( $product_id, '_azcl_compunds_terpene', true )))));
												$arr2 =   explode(",",str_replace(']','',str_replace('&quot;','',str_replace('[','',get_post_meta( $product_id, '_azcl_terpene', true )))));
												$arr3 =   explode(",",str_replace(']','',str_replace('&quot;','',str_replace('[','',get_post_meta( $product_id, '_azcl_terpene_tests', true )))));
												$arr4 =   explode(",",str_replace(']','',str_replace('&quot;','',str_replace('[','',get_post_meta( $product_id, '_azcl_terpene_ppm', true )))));
							
												$i = 0;
												if($array['0'] != 'null'  && $array['0'] != ''){
													foreach ($array as $value) {
														?>
														<tr><div class="form-field _azcl_cbc_field tp">
														<td><input type="text" class="form-control frst" name="_azcl_compunds_terpene[]" value="<?php echo trim($value, '"'); ?>"></td>
														<td><span class="scnd"><input type="text" class="form-control scnd2" name="_azcl_terpene[]" value="<?php echo $arr2[$i]; ?>"><span></td>
														<td><input type="text" class="form-control tp2" name="_azcl_terpene_tests[]" value="<?php echo $arr3[$i]; ?>"></td>
														<td><input type="text" class="form-control tp2" name="_azcl_terpene_ppm[]" value="<?php echo $arr4[$i]; ?>"></td>
														</div></tr>
														<?php
														$i++;
													}
												}
											?>
												<tr  class="input_fields_wrap2"><td colspan="4"><button id="add_field_button2" class="add_field_button2">Add More Fields</button></td></tr>
											
											</tbody>
										</table>
									</div>
										
										
									<div id="tabs-3" class="tab-pane fade">
										<table>
											<thead>
												<tr><th>Compounds</th><th>Tests</th><th>mg/grm</th><th>PPM</th></tr>
											</thead>
											<tbody>
											<?php
												
												$residual_arr = array('methanol','pentane','ethanol','acetone','iospropyl','hexane','benzene','toluene');
												$tests = 'tests';
												$ppm = 'ppm';
												for($i = 0; $i < count($residual_arr);  $i++){
													$key1 = "_azcl_".$residual_arr[$i];
													$key2 = "_azcl_".$residual_arr[$i]."_".${$tests};
													$key3 = "_azcl_".$residual_arr[$i]."_".${$ppm};
													
													echo '<tr><td>'.ucwords($residual_arr[$i]).'</td>';
													echo '<td><input type="text" class="tp lb_'.$lab_result_counter.'"  name="'.$key1.'" id="'.$key1.'" value="'.get_post_meta( $product_id, $key1, true ).'" placeholder=""></td>';
													echo '<td><input type="text" class="tp lb_m_'.$lab_result_counter.'"  name="'.$key2.'" id="'.$key2.'" value="'.get_post_meta( $product_id, $key2, true ).'" placeholder=""></td>';
													echo '<td><input type="text" class="tp ppm "  name="'.$key3.'" id="'.$key3.'" value="'.((get_post_meta( $product_id, $key3, true ) == "")? "0.001" : get_post_meta( $product_id, $key3, true )).'" placeholder=""></td></tr>';
													$lab_result_counter++;
												}
												$array =   explode(",",str_replace(']','',str_replace('&quot;','',str_replace('[','',get_post_meta( $product_id, 'compunds_residual', true )))));
												$arr2 =   explode(",",str_replace(']','',str_replace('&quot;','',str_replace('[','',get_post_meta( $product_id, 'residual', true )))));
												$arr3 =   explode(",",str_replace(']','',str_replace('&quot;','',str_replace('[','',get_post_meta( $product_id, 'residual_tests', true )))));
												$arr4 =   explode(",",str_replace(']','',str_replace('&quot;','',str_replace('[','',get_post_meta( $product_id, 'residual_ppm', true )))));
							
												$i = 0;
												if($array['0'] != 'null'  && $array['0'] != ''){
													foreach ($array as $value) {
														?>
														<tr><div class="form-field _azcl_cbc_field tp">
														<td><input type="text" class="form-control frst" name="compunds_residual[]" value="<?php echo trim($value, '"'); ?>"></td>
														<td><span class="scnd"><input type="text" class="form-control scnd2" name="residual[]" value="<?php echo $arr2[$i]; ?>"><span></td>
														<td><input type="text" class="form-control tp2" name="residual_tests[]" value="<?php echo $arr3[$i]; ?>"></td>
														<td><input type="text" class="form-control tp2" name="residual_ppm[]" value="<?php echo $arr4[$i]; ?>"></td>
														</div></tr>
														<?php
														$i++;
													}
												}
											?>
												<tr  class="input_fields_wrap3"><td colspan="4"><button id="add_field_button3" class="add_field_button3">Add More Fields</button></td></tr>
											
											</tbody>
										</table>
									</div>
									
								</div>
								<?php } ?>	
							</div>
						</div>
					</div>
					
					
					<div class="inventory-form-footer">
						<div class="col-md-12">
							<div class="form-group">                
								<input type="submit" value="<?php echo $btn_text; ?>" name="update" id="inventory-form-submit">
							</div>
						</div>
					</div>
					
					
				</fieldset>
				
				
			</form>
		</div>
		
		<?php
	$html = ob_get_contents();
		ob_get_clean();
		ob_end_flush();
		return $html;
}

add_shortcode('inventory-form','inventory_form');


?>