<?php

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly 
/*
File conatins the ajax call for ADD/EDIT Product
*/

//inventry list custom thumbnail size
add_action( 'after_setup_theme', 'wpdocs_theme_setup' );
function wpdocs_theme_setup() {
    add_image_size( 'inventry-list-thumb', 255, 255, false );
}



/*
*
*	change order status ajax call
*
*/

add_action( 'wp_ajax_change_order_status_ajax', 'change_order_status_ajax' );
add_action( 'wp_ajax_nopriv_change_order_status_ajax', 'change_order_status_ajax');

function change_order_status_ajax(){
	
	$current_usid = get_current_user_id();
	$userrole = get_user_role();
	
	$call_type = $_POST['call_type'];
	$order_id = $_POST['order_id'];
	//get patient id for order
	$patient_id = get_post_meta($order_id, '_customer_user', true);
	//get dispensary id to order
	$dispensary_order_id = get_post_meta($order_id, 'dispensary_order_id', true);
	//get patient therapy id
	$therapy_id = get_user_meta($patient_id, '_cdrmed_therapy_id', true);
	//create order object
	$order = new WC_Order($order_id);
	if($call_type != 'remove'){
		if($call_type == 'cancelled'){
			foreach ($order->get_items() as $key => $item) {
				$product_id = $item['product_id'];
				$product_quantity = floor(get_post_meta($product_id, '_stock', TRUE));
				$product_quantity++;
				update_post_meta($product_id, '_stock', $product_quantity);
			}
			if($userrole == 'dispensary'){
				update_user_meta($patient_id, 'order_notification', 1);
			}
			else{
				update_user_meta($dispensary_order_id, 'order_notification', 1);
			}
		}
		
		if($call_type == 'completed' && $therapy_id){
			update_post_meta($therapy_id, 'order_id', $order_id);
		}
		elseif($therapy_id){
			update_post_meta($therapy_id, 'order_id', '');
		}
		$order->update_status( $call_type );
		echo 'Order status change successfully!';
		
		cdrmed_save_activity_log('Order status updated', '');
		
	}
	else{
		wp_delete_post($order_id,true);
		echo 'Order removed successfully!';
	}
	die();
}




add_action( 'wp_ajax_import_product_from_cvs_ajax', 'import_product_from_cvs_ajax' );
add_action( 'wp_ajax_nopriv_import_product_from_cvs_ajax', 'import_product_from_cvs_ajax');

function import_product_from_cvs_ajax(){
	
	$current_usid = get_current_user_id();
	$current_usid = is_dispensary_physician_child($current_usid);
	$userrole = get_user_role();
	
	$products = $_POST['products'];
	
	
	$product_counter = 1;
	$product_counter1 = 0;
	$product_counter2 = 0;
	
	$fail_products = '';
	
	foreach($products as $product){
		
		$product_title = $product['title'];
		$product_description = $product['description'];
		$sku = $product['sku'];
		
		if($product_title && $sku){
			
			$check_product = check_product_sku( $sku );
			if(!$check_product){
				
				$product_counter1++;
				
				$product_id = wp_insert_post( array(
					'post_title' => $product_title,
					'post_content' => $product_description,
					'post_status' => 'publish',
					'post_type' => "product",
				) );
				
				update_post_meta($product_id, 'product_dispensary_id', $current_usid);
			
				// Upload feature Image of Product
				$image_url = $product['url'];
				if($image_url){
					$upload_dir = wp_upload_dir(); // Set upload folder
					$image_data = file_get_contents($image_url); // Get image data
					$filename   = $product_title.'.jpg';
					// Check folder permission and define file location
					if( wp_mkdir_p( $upload_dir['path'] ) ) {
						$file = $upload_dir['path'] . '/' . $filename;
					} else {
						$file = $upload_dir['basedir'] . '/' . $filename;
					}
					// Create the image  file on the server
					file_put_contents( $file, $image_data );
					// Check image file type
					$wp_filetype = wp_check_filetype( $image_url, null );
					// Set attachment data
					$attachment = array(
						'post_mime_type' => $wp_filetype['type'],
						'post_title'     => sanitize_file_name( $filename ),
						'post_content'   => '',
						'post_status'    => 'inherit'
					);
					// Create the attachment
					$attach_id = wp_insert_attachment( $attachment, $file, $product_id );
					// Include image.php
					require_once(ABSPATH . 'wp-admin/includes/image.php');
					// Define attachment metadata
					$attach_data = wp_generate_attachment_metadata( $attach_id, $file );
					// Assign metadata to attachment
					wp_update_attachment_metadata( $attach_id, $attach_data );
					// And finally assign featured image to post
					set_post_thumbnail( $product_id, $attach_id );
				}
				
				
				
				// update meta values
				
				$price = $product['price'];
				update_post_meta( $product_id, '_price', $price );
				
				update_post_meta( $product_id, '_sku', $sku );
				
				$product_quantity = $product['qty'];
				update_post_meta( $product_id, '_stock', $product_quantity );
				
				
				// code for Product Category
				
				$product_cat_arr = array();
				
				$product_category = $product['category'];
				if($product_category){
					$term = get_term_by('name', $product_category, 'product_cat');
					
					$product_cat_arr[] = $term->term_id;
					
					if(count($product_cat_arr) > 0){
						wp_set_object_terms($product_id, $product_cat_arr, 'product_cat');
					}
				}
				
				// code for Lab Tabs
				$potency_arr = array('thc','thca','thcv','cbd','cbda','cbg','cbn','cbc');
				$terpene_arr = array('bisabolol','alpha_bisabolol','camphene','delta_3_carene','beta_caryophyllene','caryophyllene_oxide','p_cymene','geraniol','guaiol','alpha_humulene','isopulegol','delta_limonene','limonene','linalool','myrcene','nerolidol_1','nerolidol_2','ocimene','alpha_pinene','beta_pinene','alpha_terpinene','gamma_terpinene','terpinolene');
				$residual_arr = array('methanol','pentane','ethanol','acetone','iospropyl','hexane','benzene','toluene');
				$tests = 'tests';
				$ppm = 'ppm';
				foreach($potency_arr as $pot){
					
					$val1 = $product[$pot];
					$val2 = $val1 * 100;
					$val3 = 0.001;
					
					update_post_meta( $product_id, '_azcl_'.$pot, $val1 );
					update_post_meta( $product_id, '_azcl_'.$pot.'_'.${$tests}, $val2 );
					update_post_meta( $product_id, '_azcl_'.$pot.'_'.${$ppm}, $val3 );
				}
				
				foreach($terpene_arr as $ter){
					
					$val1 = $product[$ter];
					$val2 = $val1 * 100;
					$val3 = 0.001;
					
					update_post_meta( $product_id, '_azcl_'.$ter, $val1 );
					update_post_meta( $product_id, '_azcl_'.$ter.'_'.${$tests}, $val2 );
					update_post_meta( $product_id, '_azcl_'.$ter.'_'.${$ppm}, $val3 );
				}
				
				foreach($residual_arr as $res){
					
					$val1 = $product[$res];
					$val2 = $val1 * 100;
					$val3 = 0.001;
					
					update_post_meta( $product_id, '_azcl_'.$res, $val1 );
					update_post_meta( $product_id, '_azcl_'.$res.'_'.${$tests}, $val2 );
					update_post_meta( $product_id, '_azcl_'.$res.'_'.${$ppm}, $val3 );
				}
				
				cdrmed_save_activity_log('Inventory updated', '');
				
			}
		
		}
		else{
			$product_counter2++;
			$fail_products .= $product_counter.',';
		}
		
		$product_counter++;
		
	}
	if($product_counter2 > 0){
		echo $product_counter1.' Product(s) saved successfully and '.$product_counter2.' Product(s) not saved, Fail Product rows are '.$fail_products;
	}
	else{
		echo $product_counter1.' Product(s) saved successfully';
	}
	
	die();
}


/*
*
*	show order to dispensary/patient ajax call
*
*/


add_action( 'wp_ajax_show_order_items_ajax', 'show_order_items_ajax' );
add_action( 'wp_ajax_nopriv_show_order_items_ajax', 'show_order_items_ajax');

function show_order_items_ajax(){
	
	$current_usid = get_current_user_id();
	$userrole = get_user_role();
	
	$meta_key = 'dispensary_order_id';
	$order_for = '_customer_user';
	if($userrole == 'patient'){
		$meta_key = '_customer_user';
		$order_for = 'dispensary_order_id';
	}
	
	$order_id = $_POST['order_id'];	
	$order = new WC_Order($order_id);
	$html = '';
	$html = '<div class="order-info">';
		$status = str_replace('wc-','',get_post_status( $order_id ));	
		if($userrole == 'dispensary'){
			$email = get_post_meta($order_id, 'patient_email', true);
			$phone = get_post_meta($order_id, 'patient_phone', true);
			
			$html .= '<div class="col-md-4 col">
				<label for="email">Email</label><br>
				<span class="email">'.$email.'</span>
			</div>';
			if($phone){
				$html .= '<div class="col-md-4 col">
					<label for="phone">Phone</label><br>
					<span class="phone">'.$phone.'</span>
				</div>';
			}
		}
		$html .= '<div class="col-md-4 col">
			<label for="status">Status</label><br>
			<span class="status">'.$status.'</span>
		</div>
	</div>';
	
	
	$html .= '<table>
		<thead>
			<th>Product</th><th>SKU</th><th>Price ($)</th><th>Qty</th><th>Total ($)</th>
		</thead>
		<tbody>';
			foreach ($order->get_items() as $key => $item) {
				
				$product_id = $item['product_id'];
				$product_sku = get_post_meta($product_id, '_sku', TRUE);
				$product_sku = (!empty($product_sku) ? $product_sku : 'N / A');
				$product_price = get_post_meta($product_id, '_price', TRUE);
				$product_price = (!empty($product_price) ? $product_price : 'N / A');
				
				$html .= '<tr>
					<td>'.$item['name'].'</td>
					<td>'.$product_sku.'</td>
					<td>'.$product_price.'</td>
					<td>'.$item['qty'].'</td>
					<td>'.$item['line_total'].'</td>
				</tr>';
			}
			$html .= '<tr><td colspan="4" style="text-align: right;">Total ($)</td><td style="text-align: left;">'.$order->get_total().'</td></tr>';
		$html .= '</tbody>
	</table>';
	
	//$status = str_replace('wc-','',get_post_status( $order_id ));
	
	$html .= '
		<div class="action">';
			if($userrole == 'dispensary'){
				if($status != 'on-hold'){
					$html .= '<a class="order-action-btn" data-action="on-hold" data-class="on-hold" data-status="On Hold" data-id="'.$order_id.'"><span>On Hold</span><i class="fa fa-clock-o"></i></a>';
				}
				if($status != 'processing'){
					$html .= '<a class="order-action-btn" data-action="processing" data-class="processing" data-status="Processing" data-id="'.$order_id.'"><span>Processing</span><i class="fa fa-circle-o-notch"></i></a>';
				}
				if($status != 'completed'){
					$html .= '<a class="order-action-btn" data-action="completed" data-class="completed" data-status="Completed" data-id="'.$order_id.'"><span>Completed</span><i class="fa fa-check"></i></a>';
				}
				if($status != 'cancelled'){
					$html .= '<a class="order-action-btn" data-action="cancelled" data-class="cancelled" data-status="Cancelled" data-id="'.$order_id.'"><span>Cancelled</span><i class="fa fa-remove"></i></a>';
				}
				$html .= '<a class="order-action-btn" data-action="remove" data-id="'.$order_id.'"><span>Remove</span><i class="fa fa-minus-square"></i></a>';
			}
			else{
				$html .= '<a class="order-action-btn" data-action="cancelled" data-class="cancelled" data-status="Cancelled" data-id="'.$order_id.'"><span>Cancelled</span><i class="fa fa-remove"></i></a>';
			}
		$html .= '</div>';
	
	echo $html;
	die();
}

/*
*
*	When Patient Click Send Enquiry Now Button
*
*
*/

add_action( 'wp_ajax_send_enquiry_ajax_call', 'send_enquiry_ajax_call' );
add_action( 'wp_ajax_nopriv_send_enquiry_ajax_call', 'send_enquiry_ajax_call');

function send_enquiry_ajax_call(){
	
	global $woocommerce;
	$current_usid = get_current_user_id();
	$current_user = wp_get_current_user();
	$userrole = get_user_role();
	$dispensaries = array();
	
	$patient_id = isset($_POST['patient_id']) ? $_POST['patient_id'] : $current_usid;
	
	$by_therapy = isset($_POST['by_therapy']) ? $_POST['by_therapy'] : null;
	$product_ids = isset($_POST['product_ids']) ? $_POST['product_ids'] : null;
	
	$counter = 0;
	if($userrole == 'patient' && !$by_therapy){
		foreach ( WC()->cart->get_cart() as $cart_item_key => $cart_item ) {
			$product_id = apply_filters( 'woocommerce_cart_item_product_id', $cart_item['product_id'], $cart_item, $cart_item_key );
			$dispensary_id = get_post_meta($product_id, 'product_dispensary_id', true);
			
			$dispensaries[$dispensary_id]['product_id'][$counter] = $product_id;
			$counter++;
			
			$product_quantity = floor(get_post_meta($product_id, '_stock', TRUE));
			$product_quantity--;
			update_post_meta($product_id, '_stock', $product_quantity);
			
		}
	}
	else if($by_therapy){
		foreach($product_ids as $key => $value){
			$product_id = $key;
			$dispensary_id = get_post_meta($product_id, 'product_dispensary_id', true);
			$dispensaries[$dispensary_id]['product_id'][$counter] = $product_id;
			$counter++;
			
			$product_quantity = floor(get_post_meta($product_id, '_stock', TRUE));
			$product_quantity--;
			update_post_meta($product_id, '_stock', $product_quantity);
		}
	}
	
	$order_date = date('m/d/Y');
	foreach($dispensaries as $key => $value){
		
		$dispensary_id = $key;
		
		$order = wc_create_order();
		$order_id = $order->id;
		
		$args = array('ID'=>$order_id,'post_author'=>$dispensary_id);
		 // update the post, which calls save_post again
		wp_update_post( $args );
		$intake_posts = get_posts(array(
			'posts_per_page' => 1,
			'post_type'    => 'intake',
			'author'  => $patient_id
		));
		$patient_phone = get_field( "cell_phone",$intake_posts[0]->ID );
		//get patient info
		$user_info = get_userdata($patient_id);
		//save dispensary ids in order
		update_post_meta($order_id, 'dispensary_order_id', $dispensary_id);
		//order meta keys
		update_post_meta($order_id, '_customer_user', $patient_id);
		update_post_meta($order_id, 'patient_phone', $patient_phone);
		update_post_meta($order_id, 'patient_email', $user_info->user_email);
		update_post_meta($order_id, 'date', $order_date);
		
		foreach($value['product_id'] as $product_id){
			$product = wc_get_product($product_id);
			$order->add_product($product, 1, '');
		}
		
		$order->calculate_totals();
		$order->update_status( 'on-hold' );
		
		$dispensary_email = get_user_meta($dispensary_id, 'email', true);
		$dispensary_name = get_user_meta($dispensary_id, 'first_name', true).' '.get_user_meta($dispensary_id, 'last_name', true);
		
		if($userrole == 'patient'){
			update_user_meta($dispensary_id, 'order_notification', 1);
		}
		if($userrole == 'dispensary'){
			update_user_meta($patient_id, 'order_notification', 1);
		}
		else{
			update_user_meta($dispensary_id, 'order_notification', 1);
		}
		
		if($by_therapy){
			$therapy_id = get_user_meta($patient_id, '_cdrmed_therapy_id', true);
			update_post_meta($order_id, '_cdrmed_therapy_id', $therapy_id);
			update_post_meta($therapy_id, 'order_id', $order_id);
		}
		
		cdrmed_save_activity_log('Order sent to Dispensary', $dispensary_id);
		
	}
	if($userrole == 'patient' && !$by_therapy){
		$woocommerce->cart->empty_cart();
	}
	
	echo 'Enquiry sent to Dispensary successfully!';
	die();
}


/*
*
*	remove Product ajax call
*
*/

add_action( 'wp_ajax_remove_inventory_product_ajax', 'remove_inventory_product_ajax' );
add_action( 'wp_ajax_nopriv_remove_inventory_product_ajax', 'remove_inventory_product_ajax');

function remove_inventory_product_ajax(){
	$product_id = $_POST['product_id'];
	wp_delete_post($product_id,true);
	echo 'Inventory Product removed successfully!';
	die();
}



// AJAX Call for add new categy on inventry page

add_action( 'wp_ajax_product_new_category_request', 'product_new_category_request' );
add_action( 'wp_ajax_nopriv_product_new_category_request', 'product_new_category_request');
function product_new_category_request(){
	$new_category = ucwords($_POST['new_cat']);
	
	$new_category_slug = sanitize_title($_POST['new_cat']);
	
	$cat_parent = $_POST['cat_parent'];
	if(!empty($cat_parent)){
		
		wp_insert_term( $new_category, 'product_cat',
			array(
				'parent' => $cat_parent,
				'description'	=> '',
				'slug' 		=> $new_category_slug
			)
		);
		
		$parent_terms = get_term_children( $cat_parent, 'product_cat');
		
		foreach($parent_terms as $term){
			$name = get_term_by('id', $term, 'product_cat');
			if($name->name == $new_category){
				$term = get_term_by('name', $new_category, 'product_cat');
				echo '<label class="category_label child"><input type="checkbox" name="product_category[]" value="'. $name->term_id .'">'. $name->name .'</label>';
				
			}
		}
	}
	elseif(!term_exists($new_category)){
		wp_insert_term( $new_category, 'product_cat',
			array(
				'description'	=> '',
				'slug' 		=> $new_category_slug
			)
		);
		$term = get_term_by('name', $new_category, 'product_cat');
		echo '<label class="category_label"><input type="checkbox" name="product_category[]" value="'. $term->term_id .'">'. $new_category .'</label>';
	}
	die();
}

add_action( 'wp_ajax_product_form_ajax_request', 'product_form_ajax_request' );
add_action( 'wp_ajax_nopriv_product_form_ajax_request', 'product_form_ajax_request');

function product_form_ajax_request(){
	
	$url = site_url( '/inventory-list/', 'http' );
	$current_usid = get_current_user_id();
	
	$patient_dispensary_id = $current_usid;
	
	$child_user = get_user_meta($patient_dispensary_id, 'user_parent', true);
	if($child_user != ''){
		$patient_dispensary_id = get_user_meta($patient_dispensary_id, 'user_parent_id', true);
	}
	
	if(!isset($_GET['request']) && $_GET['request'] != 'get-all'){
	
		$product_id = $_POST['product_id'];
		
		$product_title = $_POST['product_title'];
		$product_description = $_POST['product_description'];
		
		// check For Edit product or New Product if Product id is empty it means that product is new
		if(empty($product_id)){
			
			$product_id = wp_insert_post( array(
				'post_title' => $product_title,
				'post_content' => $product_description,
				'post_status' => 'publish',
				'post_type' => "product",
			) );
			
			update_post_meta($product_id, 'product_dispensary_id', $patient_dispensary_id);
			
		}
		else{
			//update_post_meta( $product_id, '_purchase_note', $product_description );
			$post_update = array(
				'ID'         => $product_id,
				'post_title' => $product_title,
				'post_content' => $product_description
			);
			wp_update_post( $post_update );
			
			update_post_meta($product_id, 'product_dispensary_id', $patient_dispensary_id);
		}
		
		
		
		
		//code for uploading
		
		
		// Upload feature Image of Product
		$featured_image_change = $_POST['featured_image_change'];
		if($featured_image_change == 'changed'){
			$image_url  = $_POST['featured_image_src'];
			$upload_dir = wp_upload_dir(); // Set upload folder
			$image_data = file_get_contents($image_url); // Get image data
			$filename   = $_FILES['featured_image']['name'];//basename($image_url); // Create image file name
			// Check folder permission and define file location
			if( wp_mkdir_p( $upload_dir['path'] ) ) {
				$file = $upload_dir['path'] . '/' . $filename;
			} else {
				$file = $upload_dir['basedir'] . '/' . $filename;
			}
			// Create the image  file on the server
			file_put_contents( $file, $image_data );
			// Check image file type
			$wp_filetype = wp_check_filetype( $filename, null );
			// Set attachment data
			$attachment = array(
				'post_mime_type' => $wp_filetype['type'],
				'post_title'     => sanitize_file_name( $filename ),
				'post_content'   => '',
				'post_status'    => 'inherit'
			);
			// Create the attachment
			$attach_id = wp_insert_attachment( $attachment, $file, $product_id );
			// Include image.php
			require_once(ABSPATH . 'wp-admin/includes/image.php');
			// Define attachment metadata
			$attach_data = wp_generate_attachment_metadata( $attach_id, $file );
			// Assign metadata to attachment
			wp_update_attachment_metadata( $attach_id, $attach_data );
			// And finally assign featured image to post
			set_post_thumbnail( $product_id, $attach_id );
		}
		
		
		
		// upload lab result file
		$lab_result = $_FILES['lab_result'];
		$lab_result_src = $_POST['lab_result_src'];
		if(!empty($lab_result_src)){
			
			$file_url = $lab_result_src;
			$filename   = $_FILES['lab_result']['name'];
			$upload_dir = wp_upload_dir();
			$file_data = file_get_contents($file_url); // Get image data
			
			
			if( wp_mkdir_p( $upload_dir['path'] ) ) {
				$file = $upload_dir['path'] . '/' . $filename;
			} else {
				$file = $upload_dir['basedir'] . '/' . $filename;
			}
			
			file_put_contents( $file, $file_data );
			$wp_filetype = wp_check_filetype( $filename, null );
			$attachment = array(
				'post_mime_type' => $wp_filetype['type'],
				'post_title'     => sanitize_file_name( $filename ),
				'post_content'   => '',
				'post_status'    => 'inherit'
			);

			$attach_id = wp_insert_attachment( $attachment, $file, $product_id );
			require_once( ABSPATH . 'wp-admin/includes/image.php' );
			require_once( ABSPATH . 'wp-admin/includes/file.php' );
			$attach_data = wp_generate_attachment_metadata( $attach_id, $file );
			wp_update_attachment_metadata( $attach_id, $attach_data );

			//set_post_thumbnail( $product_id, $attach_id );
			
			update_post_meta( $product_id, '_front_product_lab_result_file', $attach_id );
		}
		
		
		// update meta values
		
		$price = $_POST['product_price'];
		update_post_meta( $product_id, '_price', $price );
		
		$product_sku = $_POST['product_sku'];
		update_post_meta( $product_id, '_sku', $product_sku );
		
		$product_quantity = $_POST['product_quantity'];
		update_post_meta( $product_id, '_stock', $product_quantity );
		
		
		// code for Product Category
		
		$product_cat_arr = array();
		$args = array(
			'taxonomy' => 'product_cat', 'orderby' => 'name', 'show_count' => 0, 'pad_counts'   => 0, 'hierarchical' => 1, 'title_li'  => '', 'hide_empty' => 0 
		);
		$all_categories = get_categories( $args );
		
		$all_terms_id_arr = array();
		foreach ($all_categories as $cat) {
			$all_terms_id_arr[] = $cat->term_id;
		}
		
		
		$product_category = $_POST['product_category'];
		foreach ($all_terms_id_arr as $term_id){
			if(!in_array($term_id, $product_category)){
				//$term = get_term_by('id', $term_id, 'product_cat');
				wp_remove_object_terms( $product_id, $term_id, 'product_cat' );
			}
			else{
				$product_cat_arr[] = $term_id;
			}
		}
		if(count($product_cat_arr) > 0){
			wp_set_object_terms($product_id, $product_cat_arr, 'product_cat');
		}
		
		// code for Lab Tabs
		$potency_arr = array('thc','thca','thcv','cbd','cbda','cbg','cbn','cbc');
		$terpene_arr = array('bisabolol','alpha_bisabolol','camphene','delta_3_carene','beta_caryophyllene','caryophyllene_oxide','p_cymene','geraniol','guaiol','alpha_humulene','isopulegol','delta_limonene','limonene','linalool','myrcene','nerolidol1','nerolidol2','ocimene','alpha_pinene','beta_pinene','alpha_terpinene','gamma_terpinene','terpinolene');
		$residual_arr = array('methanol','pentane','ethanol','acetone','iospropyl','hexane','benzene','toluene');
		$tests = 'tests';
		$ppm = 'ppm';
		foreach($potency_arr as $pot){
			
			$val1 = intval($_POST["_azcl_".$pot]);
			$val2 = intval($_POST["_azcl_".$pot."_".${$tests}]);
			$val3 = intval($_POST["_azcl_".$pot."_".${$ppm}]);
			
			update_post_meta( $product_id, '_azcl_'.$pot, $val1 );
			update_post_meta( $product_id, '_azcl_'.$pot.'_'.${$tests}, $val2 );
			update_post_meta( $product_id, '_azcl_'.$pot.'_'.${$ppm}, $val3 );
		}
		
		foreach($terpene_arr as $ter){
			
			$val1 = intval($_POST["_azcl_".$ter]);
			$val2 = intval($_POST["_azcl_".$ter."_".${$tests}]);
			$val3 = intval($_POST["_azcl_".$ter."_".${$ppm}]);
			
			update_post_meta( $product_id, '_azcl_'.$ter, $val1 );
			update_post_meta( $product_id, '_azcl_'.$ter.'_'.${$tests}, $val2 );
			update_post_meta( $product_id, '_azcl_'.$ter.'_'.${$ppm}, $val3 );
		}
		
		foreach($residual_arr as $res){
			
			$val1 = intval($_POST["_azcl_".$res]);
			$val2 = intval($_POST["_azcl_".$res."_".${$tests}]);
			$val3 = intval($_POST["_azcl_".$res."_".${$ppm}]);
			
			update_post_meta( $product_id, '_azcl_'.$res, $val1 );
			update_post_meta( $product_id, '_azcl_'.$res.'_'.${$tests}, $val2 );
			update_post_meta( $product_id, '_azcl_'.$res.'_'.${$ppm}, $val3 );
		}
		
		for($i = 0; $i < count($_POST['_azcl_compunds']); $i++){
			$key = str_replace(' ', '_', $_POST['_azcl_compunds'][$i]);
			update_post_meta( $product_id, '_azcl_'.$key, intval($_POST['_azcl_potency'][$i]) );
			update_post_meta( $product_id, '_azcl_'.$key.'_tests' , intval($_POST['_azcl_potency_tests'][$i]) );
			update_post_meta( $product_id, '_azcl_'.$key.'_ppm' , intval($_POST['_azcl_potency_ppm'][$i]) );
		}
		
		$compunds = json_encode($_POST['_azcl_compunds']);
		if( !empty( $compunds ) )
			update_post_meta( $product_id, '_azcl_compunds', esc_attr( $compunds ) );
		$compunds = json_encode($_POST['_azcl_potency']);
		if( !empty( $compunds ) )
			update_post_meta( $product_id, '_azcl_potency', esc_attr( $compunds ) );
		$compunds = json_encode($_POST['_azcl_potency_tests']);
		if( !empty( $compunds ) )
			update_post_meta( $product_id, '_azcl_potency_tests', esc_attr( $compunds ) );
		$compunds = json_encode($_POST['_azcl_potency_ppm']);
		if( !empty( $compunds ) )
			update_post_meta( $product_id, '_azcl_potency_ppm', esc_attr( $compunds ) );
		
		
		for($i = 0; $i < count($_POST['_azcl_compunds_terpene']); $i++){
			$key = str_replace(' ', '_', $_POST['_azcl_compunds_terpene'][$i]);
			update_post_meta( $product_id, '_azcl_'.$key , intval($_POST['_azcl_terpene'][$i]) );
			update_post_meta( $product_id, '_azcl_'.$key.'_tests' , intval($_POST['_azcl_terpene_tests'][$i]) );
			update_post_meta( $product_id, '_azcl_'.$key.'_ppm' , intval($_POST['_azcl_terpene_ppm'][$i]) );
		}
		
		$compunds = json_encode($_POST['_azcl_compunds_terpene']);
		if( !empty( $compunds ) )
			update_post_meta( $product_id, '_azcl_compunds_terpene', esc_attr( $compunds ) );
		$compunds = json_encode($_POST['_azcl_terpene']);
		if( !empty( $compunds ) )
			update_post_meta( $product_id, '_azcl_terpene', esc_attr( $compunds ) );
		$compunds = json_encode($_POST['_azcl_terpene_tests']);
		if( !empty( $compunds ) )
			update_post_meta( $product_id, '_azcl_terpene_tests', esc_attr( $compunds ) );
		$compunds = json_encode($_POST['_azcl_terpene_ppm']);
		if( !empty( $compunds ) )
			update_post_meta( $product_id, '_azcl_terpene_ppm', esc_attr( $compunds ) );
		
		
		for($i = 0; $i < count($_POST['compunds_residual']); $i++){
			$key = str_replace(' ', '_', $_POST['compunds_residual'][$i]);
			update_post_meta( $product_id, '_azcl_'.$key , intval($_POST['residual'][$i]) );
			update_post_meta( $product_id, '_azcl_'.$key.'_tests' , intval($_POST['residual_tests'][$i]) );
			update_post_meta( $product_id, '_azcl_'.$key.'_ppm' , intval($_POST['residual_ppm'][$i]) );
		}
		
		$compunds = json_encode($_POST['compunds_residual']);
		if( !empty( $compunds ) )
			update_post_meta( $product_id, 'compunds_residual', esc_attr( $compunds ) );
		$compunds = json_encode($_POST['residual']);
		if( !empty( $compunds ) )
			update_post_meta( $product_id, 'residual', esc_attr( $compunds ) );
		$compunds = json_encode($_POST['residual_tests']);
		if( !empty( $compunds ) )
			update_post_meta( $product_id, 'residual_tests', esc_attr( $compunds ) );
		$compunds = json_encode($_POST['residual_ppm']);
		if( !empty( $compunds ) )
			update_post_meta( $product_id, 'residual_ppm', esc_attr( $compunds ) );
		
		//echo 'update successfully ';
		
		cdrmed_save_activity_log('Inventory updated', '');
		
		header('Location:'.$url);
	}
	else{
		
		$params = array(
			'post_type' => 'product',
			'meta_query' => array(
				array(
					'key' => 'product_dispensary_id',
					'value' => $patient_dispensary_id,
					'compare' => '=',
				)
			)
		);
		$wc_query = new WP_Query($params);
		$product_json['total'] = $wc_query->found_posts;
		wp_reset_query();
		
		$ofset = $_GET['offset'];
		$limit = $_GET['limit'];
		
		$params = array(
			'posts_per_page' => $limit,
			'post_type' => 'product',
			'offset' => $ofset,
			'meta_query' => array(
				array(
					'key' => 'product_dispensary_id',
					'value' => $patient_dispensary_id,
					'compare' => '=',
				)
			)
		);
		$wc_query = new WP_Query($params);
		
		while ($wc_query->have_posts()) :
			$wc_query->the_post();
			$product_id = get_the_ID();
			$product_title = get_the_title();
			
			//$product_category = the_category( ', ', $product_id);
			$product_category = '';
			$terms = get_the_terms( $product_id, 'product_cat');
			$i = 1;
			foreach ($terms as $term) {
				if($i > 1){
					$product_category .= ', ';
				}
				if($i%3 == 0){
					$product_category .= '<br>';
				}
				$product_category .= $term->name;
				$i++;
			}
			
			//$product_category = implode(' ,',$parent_cat_id_arr);
			
			
			$product_price = get_post_meta($product_id, '_price', TRUE);
			$product_price = (!empty($product_price) ? $product_price : 'N / A');
			$product_sku = get_post_meta($product_id, '_sku', TRUE);
			$product_sku = (!empty($product_sku) ? $product_sku : 'N / A');
			$product_quantity = floor(get_post_meta($product_id, '_stock', TRUE));
			
			$product_json['rows'][] = array(
				'proname' => strtolower($product_title),
				'item' => '<a href="'.get_permalink().'">'.$product_title.'</a>',
				'category' => '<div data-short="'.strtolower($product_category).'">'.$product_category.'</div>',
				'price' => $product_price,
				'quantity' => $product_quantity,
				'sku' => '<div data-short="'.strtolower($product_sku).'">'.$product_sku.'</div>',
				'action' => '<a href="'.$url.'?action=edit-product&page_type=inventory-list&proid='.$product_id.'">Edit</a> / <a data-id="'.$product_id.'" class="inventory-product-delete">Remove</a>'
			);
		endwhile;
		
		$sort = isset($_GET['sort']) ? $_GET['sort']: 'proname';
		$order = ($_GET['order']=='asc') ? SORT_ASC : SORT_DESC;
		if(!isset($_GET['sort'])){
			$order = SORT_ASC;
		}
		$product_json['rows'] = array_sort($product_json['rows'], $sort, $order);
		$product_json = json_encode($product_json);
		echo $product_json;
	}
	
	die();
	
	
}


