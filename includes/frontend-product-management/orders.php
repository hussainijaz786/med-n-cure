<?php 
if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly 

function inventory_orders(){
	
	$current_usid = get_current_user_id();
	$current_usid = is_dispensary_physician_child($current_usid);
	$patient_id = isset($_GET['pid']) ? $_GET['pid'] : null;
	
	$userrole = get_user_role();
	
	$meta_key = 'dispensary_order_id';
	$order_for = '_customer_user';
	if($userrole == 'patient'){
		$meta_key = '_customer_user';
		$order_for = 'dispensary_order_id';
	}
	$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
	$args = array(
		'post_type' => 'shop_order',
		'posts_per_page' => 10,
		'order' => 'DESC',
		'paged' => $paged,
		'post_status' => array('wc-processing','wc-completed', 'wc-on-hold', 'wc-cancelled'),
	);
	
	if($patient_id){
		$args['meta_query'] = array(
			'relation' => 'AND',
			array(
				'key' => 'dispensary_order_id',
				'value' => $current_usid,
				'compare' => '='
			),
			array(
				'key' => '_customer_user',
				'value' => $patient_id,
				'compare' => '='
			),
		);
	}
	else{
		$args['meta_query'] = array(
			'relation' => 'AND',
			array(
				'key' => $meta_key,
				'value' => $current_usid,
				'compare' => '='
			),
		);
	}
	
	$post = new WP_Query( $args );
	
	$order_count = (intval($post->post_count) > 0)? intval($post->post_count) : null;
	$html = $first_html = $second_html = $last_html = '';
	
	$on_hold = 0;
	$processing = 0;
	$complete = 0;
	$cancel = 0;
	
	$html .= '<div class="inventory-orders">';
	$html .= '<div class="journey topspace"><h3>Medicine Request(s)</h3><br></div>';
		
		
		if($order_count){
			$first_html .= '<div class="col-md-3 filter">
				<input type="text" class="form-control order-search" placeholder="Type Order #" />
			</div>';
			$second_html .= '<div class="feedback"></div>';
		
			while ( $post->have_posts() ) : $post->the_post();
				
				$order_id = get_the_ID();
				$order_date = get_post_meta($order_id, 'date', true);
				
				$order_for_id = get_post_meta($order_id, $order_for, true);
				$name = get_user_meta($order_for_id, 'first_name', true).' '.get_user_meta($order_for_id, 'last_name', true);
				$status = str_replace('wc-','',get_post_status( $order_id ));
				
				$last_html .= '<div class="single-order order'.$order_id.'" data-search="'.$order_id.'">
					<div class="panel panel-default inventory-order-collapse order_id'.$order_id.'" data-status="'.$status.'">
						<div class="panel-heading panel-collapsed panel-opened '.$status.'" order-id="'.$order_id.'">
							<span class="pull-left time">Order #'.$order_id.'</span>
							<span class="info">'.$name.'</span>
							<span class="date">'.$order_date.'</span>
							<span class="pull-right detail">Show Products <i class="fa fa-chevron-down"></i></span>
							<span class="pull-right closed">Hide Products <i class="fa fa-remove"></i></span>
						</div>
						<div class="panel-body" style="display: none;"></div>
					</div>
				</div>';
				
				if($status == 'on-hold'){
					$on_hold++;
				}
				if($status == 'processing'){
					$processing++;
				}
				if($status == 'completed'){
					$complete++;
				}
				if($status == 'cancelled'){
					$cancel++;
				}
				
			endwhile;
			
			$html .= $first_html;
			$html .= '<div class="col-md-9 order-counter">
				<span class="col-md-3 on-hold">On-Hold: <span>'.$on_hold.'</span></span>
				<span class="col-md-3 processing">Processing: <span>'.$processing.'</span></span>
				<span class="col-md-3 completed">Completed: <span>'.$complete.'</span></span>
				<span class="col-md-3 cancelled">Cancelled: <span>'.$cancel.'</span></span>
			</div>';
			$html .= $second_html.$last_html.'<div class="no-order"></div>';
			$big = 999999999; 
			$total_pages = $post->max_num_pages;
			$current_page = max(1, get_query_var('paged'));
			
			$html .= '<div class="inventory-pag">'.paginate_links(array(
				'current' => $current_page,
				'total' => $total_pages,
				'type'         => 'list',
				'prev_text'    => 'Previous',
				'next_text'    => 'Next',
				'end_size'     => 3,
				'mid_size'     => 3
			)).'</div>';
			
		}
		else{
			$html .= '<div class="feedback error-message">You have No Orders!</div>';
		}
		
	$html .= '</div>';
	return $html;
}

add_shortcode('inventory_orders','inventory_orders');

?>