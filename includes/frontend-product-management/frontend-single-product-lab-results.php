<?php
if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly 

/*
*
*	WooCommerce single Product page Custom Tab
*
*/


add_filter( 'woocommerce_product_tabs', 'new_tab_lab_results' );

function new_tab_lab_results( $tabs ) {
	/* Adds the new tab */
	$tabs['lab_result_tab'] = array(
		'title' 	=> __( '<span class="fa fa-flask"></span> Lab Results', 'woocommerce' ),
		'priority' 	=> 50,  
		'callback' 	=> 'tab_lab_result_content'
	);
	//remove reviews tab
	unset( $tabs['reviews'] );
	
	return $tabs;  /* Return all  tabs including the new New Custom Product Tab  to display */
}

function tab_lab_result_content() {
	///$pid = $_GET['prodcut'];
	global $post;
	$pid=$post->ID;
	
	$attach_id = get_post_meta( $pid, '_front_product_lab_result_file', true );
	$lab_result_url = wp_get_attachment_url($attach_id);
	echo '<h2>Lab Results</h2>';
	if($lab_result_url != ''){
		echo '<p><a href="'.$lab_result_url.'" target="_blank">Click Here</a> to view Lab Result.</p><br><br>';
	}
	else{
		echo 'No Lab Result available yet!<br><br>';
	}
	
	$html = '';

 	$meta = get_post_meta($pid);
  	$meta_to_hide = array('_azcl_customer', '_azcl_report_no', '_azcl_analyzed_date', '_azcl_report_date', '_azcl_received_date', '_azcl_sample_type', '_azcl_sample_code', '_azcl_sample_id', '_azcl_issue_date', '_azcl_lab_kit');
 	
	$html .= '<div class="table-responsive">
					<table class="table table-bordered table-hover">
							<thead>
							  <tr>
								<th>Compounds</th>
								<th>Percentage</th>
								<th>Test</th>
								<th>PPM</th>
							  </tr>
							</thead>
							<tbody>';

	foreach ($meta as $key => $value) {
		if(in_array($key, $meta_to_hide)) continue;
		if(strpos($key, "_azcl") !== false && $value[0] > 0){

			$name = ucwords(trim(str_replace("_azcl", "", $key),"_"));
			if(strpos($name, "_test") === false && strpos($name, "_ppm") === false){
				if(get_post_meta($pid, $key.'_tests',true)){
					$tests = get_post_meta($pid, $key.'_tests',true);
				}elseif (get_post_meta($pid, $key.'_test',true)) {
					$tests = get_post_meta($pid, $key.'_test',true);
				}else{
					$tests = 0;
				}
				if (get_post_meta($pid, $key.'_ppm',true)) {
					$ppm = get_post_meta($pid, $key.'_ppm',true);
				}else{
					$ppm = 0.001;
				}
				$html .= '<tr>';
					$html .= '<td><b>'.$name.': </b></td>';
					$html .='<td>'.$value[0].' %</td>';
					$html .= '<td>'.$tests.'</td>';
					$html .= '<td>'.$ppm.'</td>';
				$html .= '<tr>';	
			}
		
		}
	}
							
	$html .= '</tbody></table></div>';

  echo $html;
}
?>