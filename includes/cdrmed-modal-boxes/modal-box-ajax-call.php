<?php
if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly 

/*
*
*	MednCures Modal Box
*
*/
add_action( 'wp_ajax_load_cdrmed_modal_ajax', 'load_cdrmed_modal_ajax');
add_action( 'wp_ajax_nopriv_load_cdrmed_modal_ajax', 'load_cdrmed_modal_ajax');
/*
main Parent Div
.cdrmed-modal-box-content

.modal-box-feedback
reset_modal_feedback('error-message', false);
reset_feedback('error-message', false);

data-call_action=""

*/
function load_cdrmed_modal_ajax() {
	ob_start();
	$html = '';
	$current_user = wp_get_current_user();
	$current_usid = get_current_user_id();
	//action define clicked button's data attribute for modal body shortcode
	$modal_action = isset($_POST['modal_action']) ? $_POST['modal_action'] : '';
	// if footer requre then pass true else false
	$modal_footer = ($_POST['modal_footer'] == 'false') ? null : true;
	?>
	
	<div class="cdrmed-modal-box modal fade in">
		<div class="modal-dialog">
			<div class="modal-content">
			
				<div class="modal-header">
					<button type="button" class="close fa fa-close" data-dismiss="modal"></button>
					<h4 class="modal-title"></h4>
				</div>
				
				<div class="modal-body">
					<div class="body-contents">
						<?php
							echo do_shortcode('['.$modal_action.'-modal-box-body-contents]');
						?>
					</div>
					<span class="feedback modal-box-feedback"></span>
				</div>
				<?php
				//Footer Depend on button data attribute
				if($modal_footer){
				?>
					<div class="modal-footer"></div>
				<?php } ?>
			</div>
		</div>
	</div>
	
	<?php
	$html .= ob_get_contents();
	ob_get_clean();
	if (ob_get_contents()) ob_end_flush();
	echo $html;
	die();
}


?>