<?php

function register_prereg_acf_fields() {

if( function_exists('acf_add_local_field_group') ):

acf_add_local_field_group(array (
	'key' => 'group_56da79ad4b387',
	'title' => 'Joining Form MednCures',
	'fields' => array (
		array (
			'key' => 'field_56da79b98b29c',
			'label' => 'Personal Information',
			'name' => '',
			'type' => 'tab',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'placement' => 'left',
			'endpoint' => 0,
		),
		array (
			'key' => 'title_apr',
			'label' => 'Personal Information',
			'name' => 'tab_title_apr',
			'type' => 'message',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '100%',
				'class' => 'tab_title',
				'id' => '',
			),
			'message' => '',
			'new_lines' => 'wpautop',
			'esc_html' => 0,
		),
		array (
			'key' => 'field_56da79e68b29d',
			'label' => 'First Name',
			'name' => 'first_name',
			'type' => 'text',
			'instructions' => '',
			'required' => 1,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => 'left_half',
				'id' => '',
			),
			'default_value' => '',
			'placeholder' => '',
			'prepend' => '',
			'append' => '',
			'maxlength' => '',
			'readonly' => 0,
			'disabled' => 0,
		),
		array (
			'key' => 'field_56da7a9c8b29e',
			'label' => 'Last Name',
			'name' => 'last_name',
			'type' => 'text',
			'instructions' => '',
			'required' => 1,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => 'right_half',
				'id' => '',
			),
			'default_value' => '',
			'placeholder' => '',
			'prepend' => '',
			'append' => '',
			'maxlength' => '',
			'readonly' => 0,
			'disabled' => 0,
		),
		array (
			'key' => 'field_56da7ab78b29f',
			'label' => 'Best Email',
			'name' => 'best_email',
			'type' => 'email',
			'instructions' => '',
			'required' => 1,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => 'left_half acf_first_email',
				'id' => '',
			),
			'default_value' => '',
			'placeholder' => 'someone@example.com',
			'prepend' => '',
			'append' => '',
		),
		array (
			'key' => 'field_56da7ad68b2a0',
			'label' => 'Confirm Email',
			'name' => 'confirm_email',
			'type' => 'email',
			'instructions' => '',
			'required' => 1,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => 'right_half acf_confirm_email',
				'id' => '',
			),
			'default_value' => '',
			'placeholder' => 'someone@example.com',
			'prepend' => '',
			'append' => '',
		),
		array (
			'key' => 'field_56da7ae98b2a1',
			'label' => 'Phone',
			'name' => 'phone',
			'type' => 'text',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => 'left_half phone',
				'id' => '',
			),
			'default_value' => '',
			'placeholder' => '',
			'prepend' => '',
			'append' => '',
			'maxlength' => '',
			'readonly' => 0,
			'disabled' => 0,
		),
		array (
			'key' => 'field_56da7b078b2a2',
			'label' => 'Date of Birth',
			'name' => 'date_of_birth',
			'type' => 'text',
			'instructions' => '',
			'required' => 1,
			'conditional_logic' => 0,
			'wrapper' => array (
				'class' => 'left_half mdates',
				'id' => 'dob-intake',
			),
			'first_day' => 1,
			'placeholder' => 'mm/dd/yyyy',
		),
	array (
			'key' => 'field_56c2b5c140932',
			'label' => 'Address Line1:',
			'name' => 'street_address',
			'type' => 'text',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => 'f-col-2',
				'id' => '',
			),
			'default_value' => '',
			'placeholder' => '',
			'prepend' => '',
			'append' => '',
			'maxlength' => '',
			'readonly' => 0,
			'disabled' => 0,
		),
			array (
			'key' => 'field_56c2b5c140933',
			'label' => 'Address Line2:',
			'name' => 'address2',
			'type' => 'text',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => 'f-col-2',
				'id' => '',
			),
			'default_value' => '',
			'placeholder' => '',
			'prepend' => '',
			'append' => '',
			'maxlength' => '',
			'readonly' => 0,
			'disabled' => 0,
		),
		array (
			'key' => 'field_56c2b5c140d17',
			'label' => 'City',
			'name' => 'city',
			'type' => 'text',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => 'f-col-2',
				'id' => '',
			),
			'default_value' => '',
			'placeholder' => '',
			'prepend' => '',
			'append' => '',
			'maxlength' => '',
			'readonly' => 0,
			'disabled' => 0,
		),
		array (
			'key' => 'field_56c2b5c740d58',
			'label' => 'State / Province / Region',
			'name' => 'state',
			'type' => 'text',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => array (
				array (
					array (
						'field' => 'field_56c2b5c222236',
						'operator' => '!=',
						'value' => 'United States',
					),
				),
			),
			'wrapper' => array (
				'width' => '',
				'class' => 'f-col-2',
				'id' => '',
			),
			'default_value' => '',
			'placeholder' => '',
			'prepend' => '',
			'append' => '',
			'maxlength' => '',
			'readonly' => 0,
			'disabled' => 0,
		),
		array (
			'key' => 'field_56c2b5c141109',
			'label' => 'State / Province / Region',
			'name' => 'state',
			'type' => 'select',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => array (
				array (
					array (
						'field' => 'field_56c2b5c222236',
						'operator' => '==',
						'value' => 'United States',
					),
				),
			),
			'wrapper' => array (
				'width' => '',
				'class' => 'f-col-2',
				'id' => '',
			),
			'choices' => array (
				'Alabama' => 'Alabama',
				'Alaska' => 'Alaska',
				'Arizona' => 'Arizona',
				'Arkansas' => 'Arkansas',
				'California' => 'California',
				'Colorado' => 'Colorado',
				'Connecticut' => 'Connecticut',
				'Delaware' => 'Delaware',
				'Florida' => 'Florida',
				'Georgia' => 'Georgia',
				'Hawaii' => 'Hawaii',
				'Idaho' => 'Idaho',
				'Illinois' => 'Illinois',
				'Indiana' => 'Indiana',
				'Iowa' => 'Iowa',
				'Kansas' => 'Kansas',
				'Kentucky' => 'Kentucky',
				'Louisiana' => 'Louisiana',
				'Maine' => 'Maine',
				'Maryland' => 'Maryland',
				'Massachusetts' => 'Massachusetts',
				'Michigan' => 'Michigan',
				'Minnesota' => 'Minnesota',
				'Mississippi' => 'Mississippi',
				'Missouri' => 'Missouri',
				'Montana' => 'Montana',
				'Nebraska' => 'Nebraska',
				'Nevada' => 'Nevada',
				'New Hampshire' => 'New Hampshire',
				'New Jersey' => 'New Jersey',
				'New Mexico' => 'New Mexico',
				'New York' => 'New York',
				'North Carolina' => 'North Carolina',
				'North Dakota' => 'North Dakota',
				'Ohio' => 'Ohio',
				'Oklahoma' => 'Oklahoma',
				'Oregon' => 'Oregon',
				'Pennsylvania' => 'Pennsylvania',
				'Rhode Island' => 'Rhode Island',
				'South Carolina' => 'South Carolina',
				'South Dakota' => 'South Dakota',
				'Tennessee' => 'Tennessee',
				'Texas' => 'Texas',
				'Utah' => 'Utah',
				'Vermont' => 'Vermont',
				'Virginia' => 'Virginia',
				'Washington' => 'Washington',
				'West Virginia' => 'West Virginia',
				'Wisconsin' => 'Wisconsin',
				'Wyoming' => 'Wyoming',
			),
			'default_value' => array (
			),
			'allow_null' => 1,
			'multiple' => 0,
			'ui' => 0,
			'ajax' => 0,
			'placeholder' => '',
			'disabled' => 0,
			'readonly' => 0,
		),
		array (
			'key' => 'field_56c2b5c1414e8',
			'label' => 'ZIP / Postal Code',
			'name' => 'zip_code',
			'type' => 'text',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => 'f-col-2',
				'id' => '',
			),
			'default_value' => '',
			'placeholder' => '',
			'prepend' => '',
			'append' => '',
			'maxlength' => '',
			'readonly' => 0,
			'disabled' => 0,
		),
		array (
			'key' => 'field_56c2b5c222236',
			'label' => 'Country',
			'name' => 'country',
			'type' => 'select',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => 'f-col-2',
				'id' => '',
			),
			'choices' => array (
				'Abkhazia' => 'Abkhazia',
				'Afghanistan' => 'Afghanistan',
				'Akrotiri and Dhekelia' => 'Akrotiri and Dhekelia',
				'Aland' => 'Aland',
				'Albania' => 'Albania',
				'Algeria' => 'Algeria',
				'American Samoa' => 'American Samoa',
				'Andorra' => 'Andorra',
				'Angola' => 'Angola',
				'Anguilla' => 'Anguilla',
				'Antigua and Barbuda' => 'Antigua and Barbuda',
				'Argentina' => 'Argentina',
				'Armenia' => 'Armenia',
				'Aruba' => 'Aruba',
				'Ascension Island' => 'Ascension Island',
				'Australia' => 'Australia',
				'Austria' => 'Austria',
				'Azerbaijan' => 'Azerbaijan',
				'Bahamas, The' => 'Bahamas, The',
				'Bahrain' => 'Bahrain',
				'Bangladesh' => 'Bangladesh',
				'Barbados' => 'Barbados',
				'Belarus' => 'Belarus',
				'Belgium' => 'Belgium',
				'Belize' => 'Belize',
				'Benin' => 'Benin',
				'Bermuda' => 'Bermuda',
				'Bhutan' => 'Bhutan',
				'Bolivia' => 'Bolivia',
				'Bosnia and Herzegovina' => 'Bosnia and Herzegovina',
				'Botswana' => 'Botswana',
				'Brazil' => 'Brazil',
				'Brunei' => 'Brunei',
				'Bulgaria' => 'Bulgaria',
				'Burkina Faso' => 'Burkina Faso',
				'Burundi' => 'Burundi',
				'Cambodia' => 'Cambodia',
				'Cameroon' => 'Cameroon',
				'Canada' => 'Canada',
				'Cape Verde' => 'Cape Verde',
				'Cayman Islands' => 'Cayman Islands',
				'Central Africa Republic' => 'Central Africa Republic',
				'Chad' => 'Chad',
				'Chile' => 'Chile',
				'China' => 'China',
				'Christmas Island' => 'Christmas Island',
				'Cocos (Keeling) Islands' => 'Cocos (Keeling) Islands',
				'Colombia' => 'Colombia',
				'Comoros' => 'Comoros',
				'Congo' => 'Congo',
				'Cook Islands' => 'Cook Islands',
				'Costa Rica' => 'Costa Rica',
				'Cote d\'lvoire' => 'Cote d\'lvoire',
				'Croatia' => 'Croatia',
				'Cuba' => 'Cuba',
				'Cyprus' => 'Cyprus',
				'Czech Republic' => 'Czech Republic',
				'Denmark' => 'Denmark',
				'Djibouti' => 'Djibouti',
				'Dominica' => 'Dominica',
				'Dominican Republic' => 'Dominican Republic',
				'East Timor Ecuador' => 'East Timor Ecuador',
				'Egypt' => 'Egypt',
				'El Salvador' => 'El Salvador',
				'Equatorial Guinea' => 'Equatorial Guinea',
				'Eritrea' => 'Eritrea',
				'Estonia' => 'Estonia',
				'Ethiopia' => 'Ethiopia',
				'Falkland Islands' => 'Falkland Islands',
				'Faroe Islands' => 'Faroe Islands',
				'Fiji' => 'Fiji',
				'Finland' => 'Finland',
				'France' => 'France',
				'French Polynesia' => 'French Polynesia',
				'Gabon' => 'Gabon',
				'Cambia, The' => 'Cambia, The',
				'Georgia' => 'Georgia',
				'Germany' => 'Germany',
				'Ghana' => 'Ghana',
				'Gibraltar' => 'Gibraltar',
				'Greece' => 'Greece',
				'Greenland' => 'Greenland',
				'Grenada' => 'Grenada',
				'Guam' => 'Guam',
				'Guatemala' => 'Guatemala',
				'Guemsey' => 'Guemsey',
				'Guinea' => 'Guinea',
				'Guinea-Bissau' => 'Guinea-Bissau',
				'Guyana' => 'Guyana',
				'Haiti' => 'Haiti',
				'Honduras' => 'Honduras',
				'Hong Kong' => 'Hong Kong',
				'Hungary' => 'Hungary',
				'Iceland' => 'Iceland',
				'India' => 'India',
				'Indonesia' => 'Indonesia',
				'Iran' => 'Iran',
				'Iraq' => 'Iraq',
				'Ireland' => 'Ireland',
				'Isle of Man' => 'Isle of Man',
				'Israel' => 'Israel',
				'Italy' => 'Italy',
				'Jamaica' => 'Jamaica',
				'Japan' => 'Japan',
				'Jersey' => 'Jersey',
				'Jordan' => 'Jordan',
				'Kazakhstan' => 'Kazakhstan',
				'Kenya' => 'Kenya',
				'Kiribati' => 'Kiribati',
				'Korea, N' => 'Korea, N',
				'Korea, S' => 'Korea, S',
				'Kosovo' => 'Kosovo',
				'Kuwait' => 'Kuwait',
				'Kyrgyzstan' => 'Kyrgyzstan',
				'Laos' => 'Laos',
				'Latvia' => 'Latvia',
				'Lebanon' => 'Lebanon',
				'Lesotho' => 'Lesotho',
				'Liberia' => 'Liberia',
				'Libya' => 'Libya',
				'Liechtenstein' => 'Liechtenstein',
				'Lithuania' => 'Lithuania',
				'Luxembourg' => 'Luxembourg',
				'Macao' => 'Macao',
				'Macedonia' => 'Macedonia',
				'Madagascar' => 'Madagascar',
				'Malawi' => 'Malawi',
				'Malaysia' => 'Malaysia',
				'Maldives' => 'Maldives',
				'Mali' => 'Mali',
				'Malta' => 'Malta',
				'Marshall Islands' => 'Marshall Islands',
				'Mauritania' => 'Mauritania',
				'Mauritius' => 'Mauritius',
				'Mayotte' => 'Mayotte',
				'Mexico' => 'Mexico',
				'Micronesia' => 'Micronesia',
				'Moldova' => 'Moldova',
				'Monaco' => 'Monaco',
				'Mongolia' => 'Mongolia',
				'Montenegro' => 'Montenegro',
				'Montserrat' => 'Montserrat',
				'Morocco' => 'Morocco',
				'Mozambique' => 'Mozambique',
				'Myanmar' => 'Myanmar',
				'Nagorno-Karabakh' => 'Nagorno-Karabakh',
				'Namibia' => 'Namibia',
				'Nauru' => 'Nauru',
				'Nepal' => 'Nepal',
				'Netherlands' => 'Netherlands',
				'Netherlands Antilles' => 'Netherlands Antilles',
				'New Caledonia' => 'New Caledonia',
				'New Zealand' => 'New Zealand',
				'Nicaragua' => 'Nicaragua',
				'Niger' => 'Niger',
				'Nigeria' => 'Nigeria',
				'Niue' => 'Niue',
				'Norfolk Island' => 'Norfolk Island',
				'Northern Cyprus' => 'Northern Cyprus',
				'Northern Mariana Islands' => 'Northern Mariana Islands',
				'Norway' => 'Norway',
				'Oman' => 'Oman',
				'Pakistan' => 'Pakistan',
				'Palau' => 'Palau',
				'Palestine' => 'Palestine',
				'Panama' => 'Panama',
				'Papua New Guinea' => 'Papua New Guinea',
				'Paraguay' => 'Paraguay',
				'Peru' => 'Peru',
				'Philippines' => 'Philippines',
				'Pitcaim Islands' => 'Pitcaim Islands',
				'Poland' => 'Poland',
				'Portugal' => 'Portugal',
				'Puerto Rico' => 'Puerto Rico',
				'Qatar' => 'Qatar',
				'Romania' => 'Romania',
				'Russia' => 'Russia',
				'Rwanda' => 'Rwanda',
				'Sahrawi Arab Democratic Republic' => 'Sahrawi Arab Democratic Republic',
				'Saint-Barthelemy' => 'Saint-Barthelemy',
				'Saint Helena' => 'Saint Helena',
				'Saint Kitts and Nevis' => 'Saint Kitts and Nevis',
				'Saint Lucia' => 'Saint Lucia',
				'Saint Martin' => 'Saint Martin',
				'Saint Pierre and Miquelon' => 'Saint Pierre and Miquelon',
				'Saint Vincent and Grenadines' => 'Saint Vincent and Grenadines',
				'Samos' => 'Samos',
				'San Marino' => 'San Marino',
				'Sao Tome and Principe' => 'Sao Tome and Principe',
				'Saudi Arabia' => 'Saudi Arabia',
				'Senegal' => 'Senegal',
				'Serbia' => 'Serbia',
				'Seychelles' => 'Seychelles',
				'Sierra Leone' => 'Sierra Leone',
				'Singapore' => 'Singapore',
				'Slovakia' => 'Slovakia',
				'Slovenia' => 'Slovenia',
				'Solomon Islands' => 'Solomon Islands',
				'Somalia' => 'Somalia',
				'Somaliland' => 'Somaliland',
				'South Africa' => 'South Africa',
				'South Ossetia' => 'South Ossetia',
				'Spain' => 'Spain',
				'Sri Lanka' => 'Sri Lanka',
				'Sudan' => 'Sudan',
				'Suriname' => 'Suriname',
				'Svalbard' => 'Svalbard',
				'Swaziland' => 'Swaziland',
				'Sweden' => 'Sweden',
				'Switzerland' => 'Switzerland',
				'Syria' => 'Syria',
				'Tajikistan' => 'Tajikistan',
				'Tanzania' => 'Tanzania',
				'Thailand' => 'Thailand',
				'Togo' => 'Togo',
				'Tokelau' => 'Tokelau',
				'Tonga' => 'Tonga',
				'Transnistria' => 'Transnistria',
				'Trinidad and Tobago' => 'Trinidad and Tobago',
				'Tristan da Cunha' => 'Tristan da Cunha',
				'Tunisia' => 'Tunisia',
				'Turkey' => 'Turkey',
				'Turkmenistan' => 'Turkmenistan',
				'Turks and Caicos Islands' => 'Turks and Caicos Islands',
				'Tuvalu' => 'Tuvalu',
				'Uganda' => 'Uganda',
				'Ukraine' => 'Ukraine',
				'United Arab Emirates' => 'United Arab Emirates',
				'United Kingdom' => 'United Kingdom',
				'United States' => 'United States',
				'Uruguay' => 'Uruguay',
				'Uzbekistan' => 'Uzbekistan',
				'Vanuatu' => 'Vanuatu',
				'Vatican City' => 'Vatican City',
				'Venezuela' => 'Venezuela',
				'Vietnam' => 'Vietnam',
				'Virgin Islands, British' => 'Virgin Islands, British',
				'Virgin Islands, U.S.' => 'Virgin Islands, U.S.',
				'Wallis and Futuna' => 'Wallis and Futuna',
				'Yemen' => 'Yemen',
				'Zambia' => 'Zambia',
				'Zimbabwe' => 'Zimbabwe',
			),
			'default_value' => array (
				  0 => 'United States',
			),
			'allow_null' => 1,
			'multiple' => 0,
			'ui' => 0,
			'ajax' => 0,
			'placeholder' => '',
			'disabled' => 0,
			'readonly' => 0,
		),
		array (
			'key' => 'field_56da7b718b2a7',
			'label' => 'Gender',
			'name' => 'gender',
			'type' => 'radio',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => 'left_half',
				'id' => '',
			),
			'choices' => array (
				'Male' => 'Male',
				'Female' => 'Female',
			),
			'other_choice' => 0,
			'save_other_choice' => 0,
			'default_value' => '',
			'layout' => 'horizontal',
		),
		array (
			'key' => 'field_56da7b8d8b2a8',
			'label' => 'Interested In',
			'name' => 'interested_in',
			'type' => 'select',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => 'right_half interest',
				'id' => '',
			),
			'choices' => array (
				'Membership Options:' => 'Membership Options:',
				'Products & Consultation (California Only)' => 'Products & Consultation (California Only)',
				'Products (California Only)' => 'Products (California Only)',
				'Consultation (Available Worldwide)' => 'Consultation (Available Worldwide)',
			),
			'default_value' => array (
			),
			'allow_null' => 0,
			'multiple' => 0,
			'ui' => 0,
			'ajax' => 0,
			'placeholder' => '',
			'disabled' => 0,
			'readonly' => 0,
		),
		array (
			'key' => 'field_56da7c618b2a9',
			'label' => 'Products & Consultation Info',
			'name' => 'products_&_consultation_info',
			'type' => 'message',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => array (
				array (
					array (
						'field' => 'field_56da7b8d8b2a8',
						'operator' => '==',
						'value' => 'Products & Consultation (California Only)',
					),
				),
			),
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'message' => 'MednCures\'s products are for medical use & possession by authorized patients only. In compliance with California Health & Safety Code 11362.5 and 215.

Consultations are provided by MednCures. Click here to learn more.

<strong>On the next screen we will collect your California identification, physician information, and medical cannabis recommendation. Please have these documents ready.</strong>',
			'new_lines' => 'wpautop',
			'esc_html' => 0,
		),
		array (
			'key' => 'field_56da7c8f8b2aa',
			'label' => 'MednCures\'s Product Info',
			'name' => '_copy',
			'type' => 'message',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => array (
				array (
					array (
						'field' => 'field_56da7b8d8b2a8',
						'operator' => '==',
						'value' => 'Products (California Only)',
					),
				),
			),
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'message' => 'MednCures\'s products are for medical use & possession by authorized patients only. In compliance with California Health & Safety Code 11362.5 and 215.

<strong>On the next screen we will collect your California identification, physician information, and medical cannabis recommendation. Please have these documents ready.</strong>',
			'new_lines' => 'wpautop',
			'esc_html' => 0,
		),
		array (
			'key' => 'field_56da7cac8b2ab',
			'label' => 'Consultation Info',
			'name' => '',
			'type' => 'message',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => array (
				array (
					array (
						'field' => 'field_56da7b8d8b2a8',
						'operator' => '==',
						'value' => 'Consultation (Available Worldwide)',
					),
				),
			),
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'message' => 'Consultations are provided by Med & Cures. Click here to learn more.

<strong>On the next screen we will ask you some basic questions to create your personal health profile.</strong>',
			'new_lines' => 'wpautop',
			'esc_html' => 0,
		),
		array (
			'key' => 'field_56da7d078b2ac',
			'label' => 'Doctor\'s Information',
			'name' => '',
			'type' => 'tab',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => array (
				array (
					array (
						'field' => 'field_56da7b8d8b2a8',
						'operator' => '!=',
						'value' => 'Consultation (Available Worldwide)',
					),
				),
			),
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'placement' => 'left',
			'endpoint' => 0,
		),
		array (
			'key' => 'title_bpr',
			'label' => 'Doctor\'s Information',
			'name' => 'tab_title_bpr',
			'type' => 'message',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => array (
				array (
					array (
						'field' => 'field_56da7b8d8b2a8',
						'operator' => '!=',
						'value' => 'Consultation (Available Worldwide)',
					),
				),
			),
			'wrapper' => array (
				'width' => '100%',
				'class' => 'tab_title',
				'id' => '',
			),
			'message' => '',
			'new_lines' => 'wpautop',
			'esc_html' => 0,
		),
		array (
			'key' => 'field_56da7d3e8b2ad',
			'label' => 'Physician Name',
			'name' => 'physician_name',
			'type' => 'text',
			'instructions' => 'Name of physician who wrote recommendation:',
			'required' => 1,
			'conditional_logic' => array (
				array (
					array (
						'field' => 'field_56da7b8d8b2a8',
						'operator' => '!=',
						'value' => 'Consultation (Available Worldwide)',
					),
				),
			),
			'wrapper' => array (
				'width' => '',
				'class' => 'left_half pname',
				'id' => '',
			),
			'default_value' => '',
			'placeholder' => '',
			'prepend' => '',
			'append' => '',
			'maxlength' => '',
			'readonly' => 0,
			'disabled' => 0,
		),
		array (
			'key' => 'field_56da7d8d8b2ae',
			'label' => 'Physician Phone',
			'name' => 'physician_phone',
			'type' => 'text',
			'instructions' => 'Phone number of physician who wrote recommendation:',
			'required' => 0,
			'conditional_logic' => array (
				array (
					array (
						'field' => 'field_56da7b8d8b2a8',
						'operator' => '!=',
						'value' => 'Consultation (Available Worldwide)',
					),
				),
			),
			'wrapper' => array (
				'width' => '',
				'class' => 'right_half phone',
				'id' => '',
			),
			'default_value' => '',
			'placeholder' => '',
			'prepend' => '',
			'append' => '',
			'maxlength' => '',
			'readonly' => 0,
			'disabled' => 0,
		),
		array (
			'key' => 'field_56da7da68b2af',
			'label' => 'Medical Marijuana Recommendation Number',
			'name' => 'medical_marijuana_recommendation_number',
			'type' => 'text',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => array (
				array (
					array (
						'field' => 'field_56da7b8d8b2a8',
						'operator' => '!=',
						'value' => 'Consultation (Available Worldwide)',
					),
				),
			),
			'wrapper' => array (
				'width' => '',
				'class' => 'left_half',
				'id' => '',
			),
			'default_value' => '',
			'placeholder' => '',
			'prepend' => '',
			'append' => '',
			'maxlength' => '',
			'readonly' => 0,
			'disabled' => 0,
		),
		array (
			'key' => 'field_56da7db78b2b0',
			'label' => 'Date of Expiration',
			'name' => 'marijuana_date_of_expiration',
			'type' => 'text',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => array (
				array (
					array (
						'field' => 'field_56da7b8d8b2a8',
						'operator' => '!=',
						'value' => 'Consultation (Available Worldwide)',
					),
				),
			),
			'wrapper' => array (
				'class' => 'right_half mdates',
				'id' => '',
			),
			'first_day' => 1,
			'placeholder' => 'mm/dd/yyyy',
		),
		array (
			'key' => 'field_56da7dd88b2b1',
			'label' => 'Physician or Clinic Verification Website:',
			'name' => 'physician_or_clinic_verification_website',
			'type' => 'text',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => array (
				array (
					array (
						'field' => 'field_56da7b8d8b2a8',
						'operator' => '!=',
						'value' => 'Consultation (Available Worldwide)',
					),
				),
			),
			'wrapper' => array (
				'width' => '',
				'class' => 'left_half',
				'id' => '',
			),
			'default_value' => '',
			'placeholder' => '',
		),
		array (
			'key' => 'field_56da7df18b2b2',
			'label' => 'Scan/Photo of Physician Recommendation:',
			'name' => 'scanphoto_of_physician_recommendation',
			'type' => 'image',
			'instructions' => 'TIP: Use your mobile phone to take a picture of your drivers license.',
			'required' => 0,
			'conditional_logic' => array (
				array (
					array (
						'field' => 'field_56da7b8d8b2a8',
						'operator' => '!=',
						'value' => 'Consultation (Available Worldwide)',
					),
				),
			),
			'wrapper' => array (
				'width' => '',
				'class' => 'right_half',
				'id' => '',
			),
			'return_format' => 'url',
			'preview_size' => 'medium',
			'library' => 'uploadedTo',
			'min_width' => '',
			'min_height' => '',
			'min_size' => '',
			'max_width' => '',
			'max_height' => '',
			'max_size' => '',
			'mime_types' => 'jpg,jpeg,png,gif',
		),
		array (
			'key' => 'field_56da7e248b2b3',
			'label' => 'Driver\'s License or I.D. Number',
			'name' => 'drivers_license_or_id_number',
			'type' => 'text',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => array (
				array (
					array (
						'field' => 'field_56da7b8d8b2a8',
						'operator' => '!=',
						'value' => 'Consultation (Available Worldwide)',
					),
				),
			),
			'wrapper' => array (
				'width' => '',
				'class' => 'left_half',
				'id' => '',
			),
			'default_value' => '',
			'placeholder' => '',
			'prepend' => '',
			'append' => '',
			'maxlength' => '',
			'readonly' => 0,
			'disabled' => 0,
		),
		array (
			'key' => 'field_56da7e368b2b4',
			'label' => 'Scan/Photo of Drivers License or I.D',
			'name' => 'scanphoto_of_drivers_license_or_id',
			'type' => 'image',
			'instructions' => 'TIP: Use your mobile phone to take a picture of your drivers license.',
			'required' => 0,
			'conditional_logic' => array (
				array (
					array (
						'field' => 'field_56da7b8d8b2a8',
						'operator' => '!=',
						'value' => 'Consultation (Available Worldwide)',
					),
				),
			),
			'wrapper' => array (
				'width' => '',
				'class' => 'right_half',
				'id' => '',
			),
			'return_format' => 'url',
			'preview_size' => 'medium',
			'library' => 'uploadedTo',
			'min_width' => '',
			'min_height' => '',
			'min_size' => '',
			'max_width' => '',
			'max_height' => '',
			'max_size' => '',
			'mime_types' => 'jpg,jpeg,png,gif',
		),
		/*array (
			'key' => 'field_56da7e5a8b2b5',
			'label' => 'How did you hear about us?',
			'name' => 'how_did_you_hear_about_us',
			'type' => 'text',
			'instructions' => 'Friend, Internet, Event, etc.',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => 'right_half',
				'id' => '',
			),
			'default_value' => '',
			'placeholder' => '',
			'prepend' => '',
			'append' => '',
			'maxlength' => '',
			'readonly' => 0,
			'disabled' => 0,
		),
		array (
			'key' => 'field_56da7e6a8b2b6',
			'label' => 'When is the best time to contact you?',
			'name' => 'when_is_the_best_time_to_contact_you',
			'type' => 'text',
			'instructions' => 'Morning or Afternoon?',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => 'right_half',
				'id' => '',
			),
			'default_value' => '',
			'placeholder' => '',
			'prepend' => '',
			'append' => '',
			'maxlength' => '',
			'readonly' => 0,
			'disabled' => 0,
		),*/
		array (
			'key' => 'field_56da7e888b2b7',
			'label' => 'Medical Cannabis Questions',
			'name' => 'medical_cannabis_questions',
			'type' => 'tab',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'placement' => 'left',
			'endpoint' => 0,
		),
		array (
			'key' => 'title_cpr',
			'label' => 'Medical Cannabis Questions',
			'name' => 'tab_title_cpr',
			'type' => 'message',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '100%',
				'class' => 'tab_title',
				'id' => '',
			),
			'message' => '',
			'new_lines' => 'wpautop',
			'esc_html' => 0,
		),
		array (
			'key' => 'field_56da7ea98b2b8',
			'label' => 'What is your primary diagnosis?',
			'name' => 'principal_primary_diagnosis',
			'type' => 'text',
			'instructions' => '',
			'required' => 1,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => 'cannbi',
				'id' => '',
			),
			'default_value' => '',
			'placeholder' => '',
			'prepend' => '',
			'append' => '',
			'maxlength' => '',
			'readonly' => 0,
			'disabled' => 0,
		),
		array (
			'key' => 'field_56c2b5c1112ec',
			'label' => 'Stage of Cancer',
			'name' => 'stage_cancer',
			'type' => 'select',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => 'f-col-3 satges',
				'id' => '',
			),
			'choices' => array (
				'Stage 0' => 'Stage 0',
				'Stage 1' => 'Stage 1',
				'Stage 2' => 'Stage 2',
				'Stage 3' => 'Stage 3',
				'Stage 4' => 'Stage 4',
			),
			'default_value' => array (
			),
			'allow_null' => 1,
			'multiple' => 0,
			'ui' => 0,
			'ajax' => 0,
			'placeholder' => '',
			'disabled' => 0,
			'readonly' => 0,
		),
		array (
			'key' => 'field_56da81188b2b9',
			'label' => 'What is your previous experience with cannabis?',
			'name' => 'what_is_your_previous_experience_with_cannabis',
			'type' => 'select',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'choices' => array (
				'Never Tried It' => 'Never Tried It',
				'Tried It Years Ago' => 'Tried It Years Ago',
				'Use It Occasionally' => 'Use It Occasionally',
				'Fairly Frequent' => 'Fairly Frequent',
				'Use cannabis daily' => 'Use cannabis daily',
			),
			'default_value' => array (
			),
			'allow_null' => 0,
			'multiple' => 0,
			'ui' => 0,
			'ajax' => 0,
			'placeholder' => '',
			'disabled' => 0,
			'readonly' => 0,
		),
		array (
			'key' => 'field_56da814d8b2ba',
			'label' => 'Please list your current pharmaceuticals and supplements',
			'name' => 'please_list_your_current_pharmaceuticals_and_supplements',
			'type' => 'repeater',
			'instructions' => 'Type one per line, press (+) for additional entries.',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'collapsed' => '',
			'min' => '',
			'max' => '',
			'layout' => 'table',
			'button_label' => 'Add More',
			'sub_fields' => array (
				array (
					'key' => 'field_56da815e8b2bb',
					'label' => 'Medication Name',
					'name' => 'medication_name',
					'type' => 'text',
					'instructions' => '',
					'required' => 0,
					'conditional_logic' => 0,
					'wrapper' => array (
						'width' => '',
						'class' => 'medication',
						'id' => '',
					),
					'default_value' => '',
					'placeholder' => '',
					'prepend' => '',
					'append' => '',
					'maxlength' => '',
					'readonly' => 0,
					'disabled' => 0,
				),
				array (
					'key' => 'field_56da818e8b3bb',
					'label' => 'Dose',
					'name' => 'dose',
					'type' => 'text',
					'instructions' => '',
					'required' => 0,
					'conditional_logic' => 0,
					'wrapper' => array (
						'width' => '',
						'class' => '',
						'id' => '',
					),
					'default_value' => '',
					'placeholder' => '',
					'prepend' => '',
					'append' => '',
					'maxlength' => '',
					'readonly' => 0,
					'disabled' => 0,
				),
				array (
					'key' => 'field_56da81fe8b701',
					'label' => 'Frequency',
					'name' => 'frequency',
					'type' => 'text',
					'instructions' => '',
					'required' => 0,
					'conditional_logic' => 0,
					'wrapper' => array (
						'width' => '',
						'class' => '',
						'id' => '',
					),
					'default_value' => '',
					'placeholder' => '',
					'prepend' => '',
					'append' => '',
					'maxlength' => '',
					'readonly' => 0,
					'disabled' => 0,
				),
			),
		),
		array (
			'key' => 'field_56da817f8b2bc',
			'label' => 'What is your objective?',
			'name' => 'what_is_your_objective',
			'type' => 'checkbox',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'choices' => array (
				'Anti-cancer Therapy' => 'Anti-cancer Therapy',
				'Manage Side Effects of Chemo' => 'Manage Side Effects of Chemo',
				'Mental Health' => 'Mental Health',
				'Pain Relief' => 'Pain Relief',
				'Pharmaceutical Replacement' => 'Pharmaceutical Replacement',
				'Seizure Control' => 'Seizure Control',
				'Other' => 'Other',
			),
			'default_value' => array (
			),
			'layout' => 'horizontal',
			'toggle' => 0,
		),
		array (
			'key' => 'field_56c2efd4e636c112',
			'label' => 'Tell us anything else you would like us to know: (optional)',
			'name' => 'tell_us_anything_else_you_would_like_us_to_know',
			'type' => 'textarea',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'default_value' => '',
			'placeholder' => '',
			'prepend' => '',
			'append' => '',
			'maxlength' => '',
			'readonly' => 0,
			'disabled' => 0,
		),
		array (
			'key' => 'field_56da81ca8b2bd',
			'label' => 'Terms & Conditions',
			'name' => 'terms_&_conditions',
			'type' => 'checkbox',
			'instructions' => '<a href="#" class="mypopup" data-target="#privacyPolicypp" data-toggle="modal">Privacy Policy</a>, <a href="#" class="mypopup" data-target="#termsServicepp" data-toggle="modal">Terms of Service</a>, and <a href="#" class="mypopup" data-target="#hippaPolicypp" data-toggle="modal">Hippa Statement</a>',
			'required' => 1,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'choices' => array (
				'I agree to the Privacy Policy, Terms of Service, and HIPAA Statement' => 'I agree to the Privacy Policy, Terms of Service, and HIPAA Statement',
			),
			'default_value' => array (
			),
			'layout' => 'vertical',
			'toggle' => 0,
		),
	),
	'location' => array (
		array (
			array (
				'param' => 'post_type',
				'operator' => '==',
				'value' => 'pre-registrations',
			),
		),
	),
	'menu_order' => 0,
	'position' => 'normal',
	'style' => 'default',
	'label_placement' => 'top',
	'instruction_placement' => 'label',
	'hide_on_screen' => '',
	'active' => 1,
	'description' => '',
));


endif;

}
add_action( 'init', 'register_prereg_acf_fields' );

function loadmyjs(){

    if(is_page('pre-registration') ) { 
	?>
	<!--
<script type="text/javascript">
//Change submit button text on click
var ij = 0;
jQuery('.send').live('click', function(event) {
    ij++;
    alert(ij);
    if(ij == 2){ 
    	if(jQuery(".acf-button").val() == 'Submit and Save'){
    		jQuery(".acf-button").prop("type", "submit");
    	    jQuery('.acf-button').val('Submitting and Uploading your registration...');
    	}
    }
    if(ij == 1 && jQuery('#acf-field_56da7b8d8b2a8').val() == 'Consultation (Available Worldwide)'){
    	jQuery(".acf-button").prop("type", "submit");
    	jQuery('.acf-button').val('Submitting and Uploading your registration...');
    }
});

/*jQuery('.acf-button').live('click', function() {
    var index = jQuery("ul.acf-tab-group li.active").index();
    if(index == 0){

    }else if(index == 1){

    }else{
    	jQuery(".acf-button").prop("type", "submit");
    	jQuery('.acf-button').val('Submitting and Uploading your registration...');
    }
		//jQuery('.acf-button').val('Submitting and Uploading your registration...');
});*/
jQuery(document).ready(function($) {
	 jQuery(".acf-button").prop("type", "button");
    jQuery('.acf-button').val('Next');

	jQuery('.acf-form-submit input').click(function(){
		var fieldval = jQuery(".interest select").val();

		
			jQuery('.acf-hl.acf-tab-group li.active').nextAll('li:visible').eq(0).find('a').trigger('click');
		var index = jQuery("ul.acf-tab-group li.active").index();
			if(index == 1){
				ij = -1;
    		}
		
		

    /*if(index == 2 && ij ==1){
				ij = 0;
    }*/
	});


//Set text on pre registration data
jQuery("ul.acf-tab-group li").click(function(event) {
 		var fieldval = jQuery(".interest select").val();		
      var index = jQuery(this).index();
     //alert(index+" "+ij+" "+jQuery(".acf-button").val());
      if(index === 0 || index === 1){
      	ij = 0;
      		  jQuery(".acf-button").prop("type", "button");
      		  jQuery('.acf-button').removeClass('send');
              jQuery('.acf-button').val('Next');
              
      }else{

      		if(fieldval== 'Consultation (Available Worldwide)'){
		
		
		if(index == 2){
				ij = 0;
    			}
		}else{
			 ij++;
		}
	
      	jQuery(".acf-button").prop("type", "button");
      	jQuery('.acf-button').addClass('send');
        jQuery('.acf-button').val('Submit and Save');
     //if(ij == 0){
       
    //}
      }

      
    });
 });
</script>-->
<script type="text/javascript">
var active_tab_index;
var total_tabs;
var last_tab;
jQuery(document).ready(function(){
	init_tabs_dom();
	jQuery('.acf-button[type="button"]').click(function(){
		jQuery('.acf-hl.acf-tab-group li.active').nextAll('li:visible').eq(0).find('a').trigger('click');
		setTimeout(function(argument) {
			init_tabs_dom();
		}, 100);
	});
	jQuery('.acf-hl.acf-tab-group li a').click(function(){
		setTimeout(function(argument) {
			init_tabs_dom();
		}, 100);
	});
	jQuery( document ).ajaxComplete(function( event, xhr, settings ) {
	  var setting_data = settings.data;
	  if(setting_data.indexOf('action=acf/validate_save_post') > -1){
	  	init_tabs_dom();
	  }
	});
});
function init_tabs_dom(){
	active_tab_index = jQuery('.acf-hl.acf-tab-group li.active').index();
	total_tabs = jQuery('.acf-hl.acf-tab-group li').length;
	last_tab = parseInt(total_tabs) - 1;
	if(active_tab_index == last_tab){
		jQuery('.acf-button').val('Submit and Save');
		jQuery(".acf-button").prop("type", "submit");
	}else{
		jQuery('.acf-button').val('Next');
		jQuery(".acf-button").prop("type", "button");
	}
}
</script>
<?php
}}
add_action( 'wp_head', 'loadmyjs' );
?>
