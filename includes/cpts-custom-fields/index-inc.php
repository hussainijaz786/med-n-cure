<?php 
if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly 
// Include Custom Post Types Files
include('dispensary-registrations-custom-fields.php');
include('physician-registrations-custom-fields.php');
include('intake-custom-fields.php');
include('mmj-recommendation.php');
include('pre-registrations-custom-fields.php');
include('pre-register-patient-physician.php');
include('documents-custom-fields.php');
include('shoop-strains-fields.php');
include('cron_export_intake.php');
include('intake_create_csv_upload.php');
include('user-profile-custom-fields.php');
?>