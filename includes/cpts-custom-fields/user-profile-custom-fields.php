<?php

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly 


add_action( 'wp_ajax_fetch_patient_info_for_update_ajax', 'fetch_patient_info_for_update_ajax');
add_action( 'wp_ajax_nopriv_fetch_patient_info_for_update_ajax', 'fetch_patient_info_for_update_ajax');

function fetch_patient_info_for_update_ajax() {
	$patient_id = $_POST['patient_id'];
	$post_id = get_user_meta($patient_id, 'patient_intake', true);
	
	$date_of_birth = get_field('date_of_birth',$post_id);
	$address1 = get_field('street_address',$post_id);
	$address2 = get_field('address2',$post_id);
	$city = get_field('city',$post_id);
	$state = get_field('state',$post_id);
	$zip_code = get_field('zip_code',$post_id);
	$country = get_field('country',$post_id);
	echo json_encode(array('dob' => $date_of_birth, 'address1' => $address1, 'address2' => $address2, 'city' => $city, 'state' => $state, 'zip_code' => $zip_code, 'country' => $country));
	die();
}

add_action( 'wp_ajax_update_user_profile_request', 'update_user_profile_request');
add_action( 'wp_ajax_nopriv_update_user_profile_request', 'update_user_profile_request');

function update_user_profile_request() {
	
	$current_user = wp_get_current_user();
	$current_usid = get_current_user_id();

	if(isset($_POST['call_action'])){
		
		if($_POST['call_action'] == 'by_dashboard'){
			if(isset($_POST['remove'])){
				update_user_meta( $current_usid, 'user_avatar', '' );
			}
			else{
				$profile_img_change = $_POST['profile_img_change'];
				$imgCropped = $_POST['imgCropped'];
				
				if($profile_img_change == 'change'){
					update_user_meta( $current_usid, 'user_avatar', $imgCropped );
				}
			}
		}
		else if($_POST['call_action'] == 'by_profile'){
			$userrole = $_POST['userrole'];
			$profile_img_change = $_POST['profile_img_change'];
			$imgCropped = $_POST['imgCropped'];
			$first_name = ucwords($_POST['first_name']);
			$last_name = ucwords($_POST['last_name']);
			$upassword = $_POST['upassword'];
			
			if($profile_img_change == 'change'){
				update_user_meta( $current_usid, 'user_avatar', $imgCropped );
			}
			if(isset($_POST['remove'])){
				update_user_meta( $current_usid, 'user_avatar', '' );
			}
			
			
			if( $userrole == 'physician' || $userrole == 'administrator'){
				$organization_name = ucwords($_POST['organization_name']);
				$news_feed_link = $_POST['news_feed_link'];
				update_user_meta( $current_usid, 'news_feed_link', $news_feed_link );
				update_user_meta( $current_usid, 'organization_name', $organization_name );
				
				$appointment_system = $_POST['appointment_system'];
				$calendly_url = $_POST['calendly_url'];
				$calendly_api_key = $_POST['calendly_api_key'];
				
				update_user_meta( $current_usid, 'appointment_system', $appointment_system );
				if($appointment_system == 'Calendly Appointment System'){
					update_user_meta( $current_usid, 'calendly_url', $calendly_url );
					update_user_meta( $current_usid, 'calendly_api_key', $calendly_api_key );
				}
			}
			elseif( $userrole == 'dispensary' ){
				$organization_name = ucwords($_POST['organization_name']);
				$news_feed_link = $_POST['news_feed_link'];
				update_user_meta( $current_usid, 'news_feed_link', $news_feed_link );
				update_user_meta( $current_usid, 'organization_name', $organization_name );
				
				$pos_system = $_POST['pos_system'];
				$c_pos_system_other = $_POST['c_pos_system_other'];
				$inventory_system = $_POST['inventory_system'];
				$c_inventory_system_other = $_POST['c_inventory_system_other'];
				
				//Inventory and POS API Keys and Secrets
				$inv_api_token = $_POST['c_invnentory_api_token'];
				$inv_api_consumer = $_POST['c_invnentory_api_consumer'];
				$inv_api_secret = $_POST['c_invnentory_api_secret'];
				$pos_api_token = $_POST['c_pos_api_token'];
				$pos_api_consumer = $_POST['c_pos_api_consumer'];
				$pos_api_secret = $_POST['c_pos_api_secret'];
				
				if($pos_api_token == '' && $pos_api_consumer == '' && $pos_api_secret == ''){
					update_user_meta($current_usid, 'api_sync_now', '');
				}
				
				update_user_meta( $current_usid, 'pos_system', $pos_system );
				update_user_meta( $current_usid, 'c_pos_system_other', $c_pos_system_other );
				update_user_meta( $current_usid, 'inventory_system', $inventory_system );
				update_user_meta( $current_usid, 'c_inventory_system_other', $c_inventory_system_other );
				
				//Inventory and POS API Keys and Secrets to User Meta
				update_user_meta( $current_usid, 'inventory_api_token', $inv_api_token );
				update_user_meta( $current_usid, 'inventory_api_consumer', $inv_api_consumer );
				update_user_meta( $current_usid, 'inventory_api_secret', $inv_api_secret );
				update_user_meta( $current_usid, 'pos_api_token', $pos_api_token );
				update_user_meta( $current_usid, 'pos_api_consumer', $pos_api_consumer );
				update_user_meta( $current_usid, 'pos_api_secret', $pos_api_secret );
				
			}
			elseif( $userrole == 'patient' || $userrole == 'subscriber'){
				
				$medicine_notification = $_POST['medicine_notification'];
				
				// get all ids of patients from wp_options table
				// return false if option not exists
				$sms_notifications = get_option('sms_medicine_notification_for_patient');
				$email_notifications = get_option('email_medicine_notification_for_patient');
				
				
				if($medicine_notification == 'SMS'){
					//add option if option not exits in table
					if($email_notifications == false){
						add_option( 'sms_medicine_notification_for_patient', json_encode(array($current_usid)), '', 'no' );
					}
					else{
					//update option if option exits in table
						$patient_ids = json_decode($email_notifications, true);
						$patient_ids[] = $current_usid;
						update_option('sms_medicine_notification_for_patient', json_encode($patient_ids));
					}
				}
				else if($medicine_notification == 'Email'){
					//add option if option not exits in table
					if($email_notifications == false){
						add_option( 'email_medicine_notification_for_patient', json_encode(array($current_usid)), '', 'no' );
					}
					else{
					//update option if option exits in table
						$patient_ids = json_decode($email_notifications, true);
						$patient_ids[] = $current_usid;
						update_option('email_medicine_notification_for_patient', json_encode($patient_ids));
					}
				}
				else if($medicine_notification == 'Never'){
					//if user disable notifications
					if($sms_notifications != false){
						$patient_ids = json_decode($sms_notifications, true);
						//find current user id
						$match_sms_index = array_search($current_usid, $patient_ids);
						if($match_sms_index > -1){
							//remove current user id
							unset($patient_ids[$match_sms_index]);
							update_option('sms_medicine_notification_for_patient', json_encode($patient_ids));
						}
					}
					
					if($email_notifications != false){
						$patient_ids = json_decode($email_notifications, true);
						//find current user id
						$match_email_index = array_search($current_usid, $patient_ids);
						if($match_email_index > -1){
							//remove current user id
							unset($patient_ids[$match_email_index]);
							update_option('email_medicine_notification_for_patient', json_encode($patient_ids));
						}
					}
				}
				
				$health_report = $_POST['health_report'];
				$how_health_report = $_POST['how_health_report'];
				update_user_meta( $current_usid, 'medicine_notification', $medicine_notification );
				update_user_meta( $current_usid, 'user_rhp', $health_report );
				update_user_meta( $current_usid, 'user_lruh', $how_health_report );
			}
				
			update_user_meta( $current_usid, 'first_name', $first_name );
			update_user_meta( $current_usid, 'last_name', $last_name );
			if($upassword != ''){
				update_user_meta( $current_usid, 'who_updated_password', $current_usid );
				//wp_set_password( $upassword, $current_usid );
				wp_set_password( $upassword, $current_usid );
				wp_clear_auth_cookie();
				wp_set_current_user ( $current_usid );
				wp_set_auth_cookie  ( $current_usid );
			}
		}
		
		cdrmed_save_activity_log('Update Profile', '');
		
		echo 'true';
		die();
	}
}



// Add Shortcode
function edit_profiles_all() {
// Bail if not logged in or able to post
	if ( ! ( is_user_logged_in() ) ) {
		echo '<p>You Must Be Logged In To Update Your Profile</p>';
		return;
	}
	
	$current_user = wp_get_current_user();
	$userrole = get_user_role();
	$current_usid = get_current_user_id();
	$useremail = $current_user->user_email;
	
	if($current_usid) {
		
			$html = '<form method="post" id="user-custom-profile-form" class="user-custom-profile-form" >
				
				<table class="form-table" id="user-custom-profile" ng-app="app" ng-controller="Ctrl">
					<tr>
						<th>
							<label for="first_name">Account No</label></th>
						<td>
							<span>'.$current_usid.'</span>
						</td>
					</tr>
					<tr>
						<th>
							<label for="profile_img">Profile Image
						</label></th>
						<td>';
						
							$html .= '<div class="user-avatar-group">
								
								<label for="profile_img_file" id="profile_img_file_label" class="profile_img_file_label">
							
								<span id="change-profile-btn" class="change-profile-btn"><i class="fa fa-edit"></i></span>
								<span class="button button-primary profile_img_file_span" id="profile_img_file_span">Choose Image</span>
								<img alt="" src="'.$current_user->user_avatar.'" class="user-avatar" id="user-avatar" height="100" width="100"/>
								
								</label>

								<span id="remove-profile-btn" class="remove-profile-btn"><i class="fa fa-remove"></i></span>
								
							</div>
							
							<input type="file" name="profile_img_file" id="profile_img_file" class="profile_img_file" value="Choose Image" style="visibility: hidden; position: absolute;"/>
							<div class="profile-img-crop" id="profile-img-crop">
								<div class="cropArea" id="cropArea">
									<img-crop image="myImage" result-image="myCroppedImage" id="myImage" class="myImage"></img-crop>
								</div>
								<input class="acf-button button button-primary clear-profile-img-btn" id="clear-profile-img-btn" value="Clear" type="submit">
								<img ng-src="{{myCroppedImage}}" name="imgCropped" id="imgCropped" class="imgCropped" height="100" width="100"/>
								<input type="hidden" id="profile_img_change_flag" class="profile_img_change_flag">
							</div>
							
						</td>
					</tr>
					<tr>
						<th>
							<label for="first_name">First Name <span>*</span>
						</label></th>
						<td>
							<input type="text" name="first_name" id="first_name" value="'.$current_user->first_name.'" class="regular-text" />
						</td>
					</tr>
					<tr>
						<th>
							<label for="last_name">Last Name <span>*</span>
						</label></th>
						<td>
							<input type="text" name="last_name" id="last_name" value="'.$current_user->last_name.'" class="regular-text" />
						</td>
					</tr>';
					if( $userrole == 'physician' || $userrole == 'administrator' || $userrole == 'dispensary'){
						$html .= '<tr>
							<th>
								<label for="organization_name">Organization</label></th>
							<td>
								<input type="text" name="organization_name" id="organization_name" value="'.$current_user->organization_name.'" class="regular-text" />
							</td>
						</tr>';
					}
					$html .= '<tr>
						<th>
							<label for="password">Password
						</label></th>
						<td class="relative_position">
							<input type="password" name="upassword" data-pass="password-input" id="upassword" value="" class="regular-text upassword" />
							<input type="checkbox" name="show_password" id="show_password" class="show-password" title="Show Password">
							<div id="password-feedback"></div>
						</td>
					</tr>
					<tr>
						<th>
							<label for="ucpassword">Confirm Password
						</label></th>
						<td class="relative_position">
							<input type="password" name="ucpassword" data-pass="password-input" id="ucpassword" value="" class="regular-text" />
							<div id="cpassword-feedback"></div>
							<input type="hidden" id="upassword_health">
						</td>
					</tr>
					<input type="hidden" name="userrole" id="userrole" value="'.$userrole.'" />';
					
					if( $userrole == 'physician' || $userrole == 'administrator' || $userrole == 'dispensary'){
						$html .= '<tr>
							<th>
								<label for="ucpassword">News Feed Link
							</label></th>
							<td>
								<input type="text" name="news_feed_link" id="news_feed_link" value="'.$current_user->news_feed_link.'" class="regular-text" />
							</td>
						</tr>';
					}
					
					
					if( $userrole == 'physician' || $userrole == 'administrator'){
						
						$calendly_system_show = $calendly_url = $calendly_api_key = $calendly_appointment_selected = $cdrmed_appointment_selected = '';
						
						$appointment_system = $current_user->appointment_system;
						
						if($appointment_system == "Calendly Appointment System"){
							$calendly_appointment_selected =  'selected';
							$calendly_url = $current_user->calendly_url;
							$calendly_api_key = $current_user->calendly_api_key;
						}
						elseif($appointment_system == "MednCures Appointment System"){
							$cdrmed_appointment_selected = 'selected';
							$calendly_system_show = 'style="display:none;"';
						}
						
						$html .= '<tr>
							<th>
								<label for="appointment_system">Appointment System</label>
							</th>
							<td>
								<select name="appointment_system" id="appointment_system" class="expand-full">
									<option '.$calendly_appointment_selected.'>Calendly Appointment System</option>
									<option '.$cdrmed_appointment_selected.'>MednCures Appointment System</option>
								</select>';
								
								$html .= '<div class="calendly-appointment-group" '.$calendly_system_show.'>';
									$html .= '<input type="text" name="calendly_url" id="calendly_url" value="'.$calendly_url.'" class="regular-text new-inp calendly_url" placeholder="API URL: https://calendly.com/example"/>
									<input type="text" name="calendly_api_key" id="calendly_api_key" value="'.	$calendly_api_key.'" class="regular-text new-inp calendly_api_key" placeholder="API Key: GOGLPCJCNAJBK7FQNHLYL3AEZ45BHO8P"/>';
								$html .= '</div>';
								
							$html .= '</td>
						</tr>';
					}
					if( $userrole == 'dispensary'){
						
						$arr_system = array('--select--','Biotrack THC','Greenbits','NCR Silver','Proteus 420', 'QuickBooks', 'Xero', 'MMJ Menu', 'MJFreeway Menu', 'ZOHO');		
						$pos_system = $current_user->pos_system;
						$c_pos_system_other = $current_user->c_pos_system_other;
						$inventory_system = $current_user->inventory_system;
						$c_inventory_system_other = $current_user->c_inventory_system_other;
						$inv_api_token = $current_user->inventory_api_token;
						$inv_api_consumer = $current_user->inventory_api_consumer;
						$inv_api_secret = $current_user->inventory_api_secret;
						$pos_api_token = $current_user->pos_api_token;
						$pos_api_consumer = $current_user->pos_api_consumer;
						$pos_api_secret = $current_user->pos_api_secret;
						
						$html .= '<tr>
							<th>
								<label for="pos_system">POS System</label>
							</th>
							<td>
								<select name="pos_system" id="pos_system" class="expand-full">';
									
									$flag = 'style="display:none;"';
									if($c_pos_system_other != ''){
										$flag = 'style="display:block;"';
									}
									foreach($arr_system as $system){
										
										$selected = '';
										if($pos_system == $system){
											$selected = 'selected';
										}
										$html .= '<option '.$selected.'>'.$system.'</option>';
									}
									
									if($c_pos_system_other != ''){
										$html .= '<option selected>Other (type in field)</option>';
									}
									else{
										$html .= '<option>Other (type in field)</option>';
									}
									
								$html .= '</select>
								<input '.$flag.' type="text" name="c_pos_system_other" id="c_pos_system_other" class="regular-text" value="'.$c_pos_system_other.'">
							</td>
						</tr>
						<tr>
							<th>
								<label for="c_pos_api_key">API Keys
							</label></th>
							<td class="relative_position">';
								$token_display = '';
								if(empty($pos_api_token)){ $token_display = 'style="display: none;"'; }
								$html .= '<input type="text" name="c_pos_api_token" id="c_pos_api_token" value="'.$pos_api_token.'" class="regular-text new-inp c_pos_api_token" placeholder="API Token" '.$token_display.'/><br>';
								
								$consumer_display = '';
								if(empty($pos_api_consumer)){ $consumer_display = 'style="display: none;"'; }
								$html .= '<input type="text" name="c_pos_api_consumer" id="c_pos_api_consumer" value="'.$pos_api_consumer.'" class="regular-text new-inp c_pos_api_consumer" placeholder="API Consumer Key" '.$consumer_display.'/><br>';
								
								$secret_display = '';
								if(empty($pos_api_secret)){ $secret_display = 'style="display: none;"'; }
								$html .= '<input type="text" name="c_pos_api_secret" id="c_pos_api_secret" value="'.$pos_api_secret.'" class="regular-text new-inp c_pos_api_secret" placeholder="API Consumer Secret" '.$secret_display.'/>
								<span class="api-alert-message"></span>';
							$html .= '</td>
						</tr>';
						if($pos_system != '--select--') {
						$html .= '<tr>
								<th><label>API Connection Status</label></th>
								<td><strong id="current_api">Current System: '.$pos_system.'</strong><br><br><div id="current_api_status">'.apply_filters('which_inventory',$current_usid,'connect_approved_api').'</div><br><a href="#" onclick="window.location.reload(true);">Refresh Status</a></td></tr>';
						}
					}
					elseif( $userrole == 'subscriber' || $userrole == 'patient' ){
						
						$medicine_notification = $current_user->medicine_notification;
						$health_report = $current_user->user_rhp;
						$how_health_report = $current_user->user_lruh;
						
						$physician_arr = json_decode(get_user_meta($current_usid, 'patient_physician_id', true), true);
						$html .= '<tr>
							<th>
								<label for="dispensaries">Physicians</label>
							</th>
							<td class="dispensaries_td">';
							
								$html .= '
								
								<a id="patient_dispensary_model_show" data-call_action="patient_physician_model_show" class="load-cdrmed-modal-box patient_physician_model_show" modal-title="Add Physicians" modal-action="view-physician" modal-footer="true" data-call_type="physician" style="cursor: pointer;">ADD/REMOVE Physicians</a>';
								
								$html .= '<div class="patient-select-dispensary-list">';
								$getdispensaries = get_users( 'orderby=user_id&role=physician' );
								foreach ($getdispensaries as $key => $value) {
									$dispensary_id = $value->ID;
									$logo = wp_get_attachment_url(get_user_meta($dispensary_id, 'logo', true));
									if($logo == '')
										$logo = 'http://placehold.it/50x50';
									if((sizeof($physician_arr) > 0) && in_array($dispensary_id,$physician_arr )){
										$post_id = get_user_meta($dispensary_id, 'physician_post_id', true);
										$name = get_the_title($post_id);
										if(!$name){
											$name = get_user_meta($dispensary_id, 'first_name', true).' '.get_user_meta($dispensary_id, 'last_name', true);
										}
										$html .= '<span id="dispensar_id'.$dispensary_id.'"><img src="'. $logo .'" alt="dispensary logo"> '.$name.'</span>';
									}
								}
								
								$html .= '</div>';
								
							$html .= '</td>
						</tr>';
						
						$dispensary_arr = json_decode(get_user_meta($current_usid, 'patient_dispenary_products_id', true), true);
						$html .= '<tr>
							<th>
								<label for="dispensaries">Dispensaries</label>
							</th>
							<td class="dispensaries_td">';
							
								$html .= '
								<a id="patient_dispensary_model_show" data-call_action="patient_dispensary_model_show" class="load-cdrmed-modal-box patient_dispensary_model_show" modal-title="Add Dispensaries" modal-action="view-dispensary" modal-footer="true" data-call_type="dispensary" style="cursor: pointer;">ADD/REMOVE Dispensaries</a>';
								
								$html .= '<div class="patient-select-dispensary-list">';

								$getdispensaries = get_users( 'orderby=user_id&role=dispensary' );
								foreach ($getdispensaries as $key => $value) {
									$dispensary_id = $value->ID;
									$logo = wp_get_attachment_url(get_user_meta($dispensary_id, 'logo', true));
									if($logo == '')
										$logo = 'http://placehold.it/50x50';
									
									if((sizeof($dispensary_arr) > 0) && in_array($dispensary_id,$dispensary_arr )){
										$html .= '<span id="dispensar_id'.$dispensary_id.'"><img src="'. $logo .'" alt="dispensary logo"> '.get_user_meta($dispensary_id, 'first_name', true).' '.get_user_meta($dispensary_id, 'last_name', true).'</span>';
									}
								}
								
								$html .= '</div>';
							$html .= '</td>
						</tr>';
						
						$html .= '<tr>
							<th>
								<label for="medicine_notification">Medicine Notification</label>
							</th>
							<td class="health_report_td">
								<input type="radio" name="medicine_notification" id="medicine_notification" value="Email" ';
								if($medicine_notification == 'Email'){$html .='checked';}
								$html .= '> Email<br>
								<input type="radio" name="medicine_notification" id="medicine_notification" value="Never" ';
								if($medicine_notification == 'Never'){$html .='checked';}
								$html .= '> Never
							</td>
						</tr>
						<tr>
							<th>
								<label for="health_report">Report my health progress</label>
							</th>
							<td class="health_report_td">
								<input type="radio" name="health_report" id="health_report" value="Daily" ';
								if($health_report == 'Daily'){$html .='checked';}
								$html .= '> Daily<br>
								<input type="radio" name="health_report" id="health_report" value="Weekly" ';
								if($health_report == 'Weekly'){$html .='checked';}
								$html .= '> Weekly<br>
								<input type="radio" name="health_report" id="health_report" value="Monthly" ';
								if($health_report == 'Monthly'){$html .='checked';}
								$html .= '> Monthly<br>
								<input type="radio" name="health_report" id="health_report" value="Never" ';
								if($health_report == 'Never'){$html .='checked';}
								$html .= '> Never
							</td>
						</tr>
						<tr>
							<th>
								<label for="how_health_report">How would you like to report your health?</label>
							</th>
							<td class="health_report_td">
								<input type="radio" name="how_health_report" id="how_health_report" value="Email" ';
								if($how_health_report == 'Email'){$html .="checked ";}
								$html .= '> Email<br>
								<input type="radio" name="how_health_report" id="how_health_report" value="Never Report" ';
								if($how_health_report == 'Never Report'){$html .="checked ";}
								$html .= '> Never Report
							</td>
						</tr>';
					}
					$html .= '<tr><span id="user-custom-profile-feedback"></span></tr>
				</table>
				
				<div class="submit-form-btn">
					<input class="acf-button button button-primary button-large" id="user-profile-customs-btn" value="Update Profile" type="submit">
				</div>
				
			</form>';
	}
		return $html;
}
add_shortcode( 'editprofile', 'edit_profiles_all' );




?>