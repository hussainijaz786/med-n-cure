<?php

function register_documents_acf_fields() {

if( function_exists('acf_add_local_field_group') ):

acf_add_local_field_group(array (
	'key' => 'group_56ddf2042490e',
	'title' => 'Add New Document',
	'fields' => array (
		array (
			'key' => 'field_56ddf4b331ec4',
			'label' => 'Document Title',
			'name' => 'document_title',
			'type' => 'text',
			'instructions' => '',
			'required' => 1,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'default_value' => '',
			'placeholder' => '',
			'prepend' => '',
			'append' => '',
			'maxlength' => '',
			'readonly' => 0,
			'disabled' => 0,
		),
		array (
			'key' => 'field_56ddf35d31ec1',
			'label' => 'Document Type',
			'name' => 'document_type',
			'type' => 'select',
			'instructions' => '',
			'required' => 1,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'choices' => array (
				'Please Select Document Type:' => 'Please Select Document Type:',
				'Cancer Markers' => 'Cancer Markers',
				'Medical / Article Research' => 'Medical / Article Research',
				'Medical Reports' => 'Medical Reports',
				'Pathological Reports' => 'Pathological Reports',
				'Scans' => 'Scans',
				'X-Rays' => 'X-Rays',
				'Other' => 'Other',
			),
			'default_value' => array (
			),
			'allow_null' => 0,
			'multiple' => 0,
			'ui' => 0,
			'ajax' => 0,
			'placeholder' => '',
			'disabled' => 0,
			'readonly' => 0,
		),
		array (
			'key' => 'field_56ddf49b31ec3',
			'label' => 'Document Description',
			'name' => 'document_description',
			'type' => 'text',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'default_value' => '',
			'placeholder' => '',
			'prepend' => '',
			'append' => '',
			'maxlength' => '',
			'readonly' => 0,
			'disabled' => 0,
		),
		array (
			'key' => 'field_56ddf40131ec2',
			'label' => 'Attach Document',
			'name' => 'patient_document',
			'type' => 'file',
			'instructions' => '',
			'required' => 1,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'return_format' => 'url',
			'library' => 'uploadedTo',
			'min_size' => '',
			'max_size' => 125,
			'mime_types' => '',
		),
	),
	'location' => array (
		array (
			array (
				'param' => 'post_type',
				'operator' => '==',
				'value' => 'document',
			),
		),
	),
	'menu_order' => 0,
	'position' => 'normal',
	'style' => 'default',
	'label_placement' => 'left',
	'instruction_placement' => 'label',
	'hide_on_screen' => array (
		0 => 'excerpt',
		1 => 'discussion',
		2 => 'comments',
		3 => 'revisions',
		4 => 'slug',
		5 => 'format',
		6 => 'page_attributes',
		7 => 'featured_image',
		8 => 'categories',
		9 => 'tags',
		10 => 'send-trackbacks',
	),
	'active' => 1,
	'description' => '',
));

endif;

}
add_action( 'init', 'register_documents_acf_fields' );
?>