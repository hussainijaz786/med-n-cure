<?php

// Patient Code

function patients_file_system() {

	$labels = array(
		'name'                => _x( 'Documents', 'Post Type General Name', 'wordpress' ),
		'singular_name'       => _x( 'Document', 'Post Type Singular Name', 'wordpress' ),
		'menu_name'           => __( 'Documents', 'wordpress' ),
		'name_admin_bar'      => __( 'Documents', 'wordpress' ),
	);
	$args = array(
		'label'               => __( 'Document', 'wordpress' ),
		'description'         => __( 'Patients Documents Management', 'wordpress' ),
		'labels'              => $labels,
		'supports'            => array( 'title', 'editor', 'author', 'custom-fields', ),
		'hierarchical'        => false,
		'public'              => false,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'menu_position'       => 5,
		'menu_icon'           => 'dashicons-welcome-add-page',
		'show_in_admin_bar'   => true,
		'show_in_nav_menus'   => true,
		'show_in_rest'   => true,
	);
	register_post_type( 'document', $args );

}
add_action( 'init', 'patients_file_system', 0 );

?>