<?php 

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly 


// Our custom post type function
function strain_posttype() {
	
	register_post_type( 'strain_shoops',
		// CPT Options
		array(
			'labels' => array(
				'name' => __( 'Shoops' ),
				'singular_name' => __( 'Shoops' )
			),
			'show_ui' => true,
			'exclude_from_search' => true,
			'hierarchical' => true,
			'supports' => array( 'title', 'editor', 'thumbnail' ),
			'query_var' => true,
			'rewrite' => array('slug' => 'strain_shoops'),
		)
	);
}
// Hooking up our function to theme setup
add_action( 'init', 'strain_posttype' );


add_action('manage_strain_shoops_posts_columns','manage_strain_shoops_posts_columns');
 
function manage_strain_shoops_posts_columns($post_columns) {
    $post_columns = array(
        "cb"            => "<input type=\"checkbox\" />",
        'post_author' => 'Physician',
        'patient_id' => 'Patient',
        'primary_dosage' => 'Primary CBD',
        'daily_dosage' => 'Target Daily Dosage',
        'frequency' => 'Frequency',
		// 'secornday_cann' => 'Secondary CBD',
		// 'secornday_daily_dosage' => 'Target Daily Dosage',
		// 'secornday_frequency' => 'Frequency',
		// 'terpene' => 'Terpene',
        'ingestion_method' => 'Ingestion Method'
    );
    return $post_columns;
}

add_action('manage_strain_shoops_posts_custom_column', 'manage_strain_shoops_custom_column',10,2);

function manage_strain_shoops_custom_column( $column_name, $post_id ) {

    if ($column_name == 'post_author') {
		$author_id = get_post_field ('created_by', $post_id);
		echo  $author_name = get_the_author_meta('display_name',$author_id );
    }
	if ($column_name == 'patient_id') {
		$patient_id = get_post_meta( $post_id, 'patient_id', true );
		$user_info = get_userdata($patient_id);
		$first_name = $user_info->first_name;
		$last_name = $user_info->last_name;
		echo  $first_name."  ".$last_name;
    }
    if ($column_name == 'primary_dosage') {
		$primary_dosage = get_post_meta( $post_id, 'medicine1_cannabinoid_1', true );
		echo   $primary_dosage;
    }
    if ($column_name == 'daily_dosage') {
		$daily_dosage = get_post_meta( $post_id, 'medicine1_target_dosage_1', true );
		echo   $daily_dosage;
    }
    if ($column_name == 'frequency') {
		$frequency = get_post_meta( $post_id, 'medicine1_frequency_1', true );
		echo   $frequency;
    }
	
    if ($column_name == 'secornday_cann') {
		echo get_post_meta( $post_id, 'secornday_cann', true );
    }
    if ($column_name == 'secornday_daily_dosage') {
		echo get_post_meta( $post_id, 'secornday_daily_dosage', true );
    }
    if ($column_name == 'secornday_frequency') {
		echo get_post_meta( $post_id, 'secornday_frequency', true );
    }
    if ($column_name == 'terpene') {
		$arr = get_post_meta( $post_id, 'terpene', true );
		echo $arr;
		/*foreach ( $arr as  $value) {
			echo $value;
			echo "<br/>";
		}*/
    }
    if ($column_name == 'ingestion_method') {
		echo get_post_meta( $post_id, 'medicine1_ingestion_method', true );
    }
    /* if ($column_name == 'notes') {
		echo get_post_meta( $post_id, 'notes', true );
    }*/
}
?>