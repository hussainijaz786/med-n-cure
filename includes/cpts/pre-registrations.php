<?php 
if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly 
// Register Custom Post Type
function pre_reg_post_type() {

  /* $args = array();
  register_post_type( 'pre-registrations', $args ); */
  $labels = array(
    'name'                  => _x( 'Pre-Registrations', 'Post Type General Name', 'text_domain' ),
    'singular_name'         => _x( 'Pre-Registrations', 'Post Type Singular Name', 'text_domain' ),
    'menu_name'             => __( 'Pre-Registrations', 'text_domain' ),
    'name_admin_bar'        => __( 'Pre-Registrations', 'text_domain' ),
  );
  $args = array(
    'label'                 => __( 'Pre-Registrations', 'text_domain' ),
    'description'           => __( 'Pre-Registrations Description', 'text_domain' ),
	'supports'            => array( 'title', 'editor', 'author', 'custom-fields', ),
    'show_ui'               => true,
    'show_in_menu'          => true,
    'menu_position'         => 5,
    'show_in_admin_bar'     => true,
    'show_in_nav_menus'     => true,
	'show_in_rest'   => true,
  );
  register_post_type( 'pre-registrations', $args );

}
add_action( 'init', 'pre_reg_post_type', 0 );
?>