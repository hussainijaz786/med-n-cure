<?php

// Patient Code

function physicians_post_type() {

	$labels = array(
		'name'                => _x( 'Physicians', 'Post Type General Name', 'wordpress' ),
		'singular_name'       => _x( 'Physician', 'Post Type Singular Name', 'wordpress' ),
		'menu_name'           => __( 'Physicians', 'wordpress' ),
		'name_admin_bar'      => __( 'Physicians', 'wordpress' ),
	);
	$args = array(
		'label'               => __( 'Physician', 'wordpress' ),
		'description'         => __( '', 'wordpress' ),
		'labels'              => $labels,
		'supports'            => array( 'title', 'editor', 'author', 'custom-fields', ),
		'hierarchical'        => false,
		'public'              => false,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'menu_position'       => 5,
		'menu_icon'           => 'dashicons-welcome-add-page',
		'show_in_admin_bar'   => true,
		'show_in_nav_menus'   => true,
		'show_in_rest'   => true,
	);
	register_post_type( 'physicians', $args );

}
add_action( 'init', 'physicians_post_type', 0 );

add_action('manage_physicians_posts_columns','manage_physicians_posts_columns');
 
function manage_physicians_posts_columns($post_columns) {
    $post_columns = array(
        "cb"            => "<input type=\"checkbox\" />",
		'title' => 'Title',
		'phy_id' => 'Physician ID',
        'phy_name' => 'Physician Name',
        'phy_email' => 'Email',
        'phy_url' => 'URL',
		'created_by' => 'Created By'
    );
    return $post_columns;
}

add_action('manage_physicians_posts_custom_column', 'manage_physicians_custom_column',10,2);

function manage_physicians_custom_column( $column_name, $post_id ) {

    if ($column_name == 'phy_id') {
		echo $phy_id = get_post_meta( $post_id, 'physician_user_id', true );
    }
	if ($column_name == 'phy_name') {
		$phy_fname = get_post_meta( $post_id, 'first_name', true );
		$phy_lname = get_post_meta( $post_id, 'last_name', true );
		echo $phy_fname.' '.$phy_lname;
    }
    if ($column_name == 'phy_email') {
		echo $phy_email = get_post_meta( $post_id, 'email', true );
    }
	 if ($column_name == 'phy_url') {
		echo $phy_email = get_post_meta( $post_id, 'url', true );
    }
	 if ($column_name == 'created_by') {
		$author_id = get_post_field ('post_author', $post_id);
		echo  $author_name = get_the_author_meta('display_name',$author_id );
    }
    
}

?>