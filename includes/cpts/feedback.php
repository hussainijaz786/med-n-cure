<?php

// Patient Code
// Our custom post type function
function feedback_type() {


  register_post_type( 'patient_feedback',
  // CPT Options
    array(
      'labels' => array(
        'name' => __( 'Patient Feedback' ),
        'singular_name' => __( 'Patient Feedback' )
      ),
      'show_ui' => true,
      'exclude_from_search' => true,
      'hierarchical' => true,
      'supports' => array( 'title', 'editor', 'thumbnail' ),
      'query_var' => true,
      'rewrite' => array('slug' => 'patient_feedback'),
    )
  );
}
// Hooking up our function to theme setup
add_action( 'init', 'feedback_type' );

add_action('manage_patient_feedback_posts_columns','manage_patient_feedback_posts_columns');
 
function manage_patient_feedback_posts_columns($post_columns) {
    $post_columns = array(
     	"cb"            => "<input type=\"checkbox\" />",
      'feel' => 'Feeling',
        'comment' => 'Comments',
        'post_author' => 'Author',
        );
    return $post_columns;
}

add_action('manage_patient_feedback_posts_custom_column', 'manage_patient_feedback_custom_column',10,2);

function manage_patient_feedback_custom_column( $column_name, $post_id ) {

    
   
    if ($column_name == 'feel') {
      $primary_dosage = get_post_meta( $post_id, 'feel', true );
      echo   $primary_dosage;
    }
    
    if ($column_name == 'comment') {
     $comments = get_post_meta( $post_id, 'comments', true );
      echo   $comments;
    }

    if ($column_name == 'post_author') {
     $author_id = get_post_field ('post_author', $post_id);
      echo  $author_name = get_the_author_meta('display_name',$author_id );
    }
    
}
?>