<?php

// Patient Code
// Our custom post type function


function codex_custom_init() {
    $args = array(
      'public' => true,
      'label'  => 'Symptoms Tracker'
    );
    register_post_type( 'symptoms_tracker', $args );
}
add_action( 'init', 'codex_custom_init' );



add_action('manage_symptoms_tracker_posts_columns','manage_symptoms_tracker_posts_columns');
 
function manage_symptoms_tracker_posts_columns($post_columns) {
    $post_columns = array(
      	"cb" => "<input type=\"checkbox\" />",
        'cannabis' => 'Cannabis Today',
        'happy' => 'Happy',
        'focused' => 'Focused',
        'relaxed' => 'Relaxed',
        'anxious_mood' => 'Anxious Mood',
        'depressed_mood' => 'Depressed Mood',
        'energetic' => 'Energetic',
        'foggy_headed' => 'Foggy headed',
        'dizzy' => 'Dizzy',
        'paranoid' => 'Paranoid',
        'sleepy' => 'Sleepy',
        'post_author' => 'Author'
        );
    return $post_columns;
}

add_action('manage_symptoms_tracker_posts_custom_column', 'manage_symptoms_tracker_custom_column',10,2);

function manage_symptoms_tracker_custom_column( $column_name, $post_id ) {

    if ($column_name == 'post_author') {
     $author_id = get_post_field ('post_author', $post_id);
      echo  $author_name = get_the_author_meta('display_name',$author_id );
    }
   if ($column_name == 'cannabis') {
      $daily_dosage = get_post_meta( $post_id, 'cannabis', true );
      echo   $daily_dosage;
    }
    if ($column_name == 'depressed_mood') {
      echo get_post_meta( $post_id, 'depressed_mood', true );
    }
    if ($column_name == 'happy') {
      echo get_post_meta( $post_id, 'happy', true );
    }
    if ($column_name == 'relaxed') {
      echo get_post_meta( $post_id, 'relaxed', true );
    }
    if ($column_name == 'focused') {
     echo  get_post_meta( $post_id, 'focused', true );
     
    }
    if ($column_name == 'energetic') {
      echo get_post_meta( $post_id, 'energetic', true );
    }
    if ($column_name == 'foggy_headed') {
      echo get_post_meta( $post_id, 'foggy_headed', true );
    }
    if ($column_name == 'dizzy') {
       echo  get_post_meta( $post_id, 'dizzy', true );
     
    }
  if ($column_name == 'paranoid') {
     echo  get_post_meta( $post_id, 'paranoid', true );
   }
 if ($column_name == 'sleepy') {
     echo  get_post_meta( $post_id, 'sleepy', true );
   }
}
?>