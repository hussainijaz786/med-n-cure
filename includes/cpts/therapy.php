<?php

// Patient Code
// Our custom post type function


function cdrmed_register_therapy_cpt() {
    $args = array(
      'public' => true,
      'label'  => 'Patient Therapy',
	  'supports'            => array( 'title', 'editor', 'author', 'custom-fields', )
    );
    register_post_type( 'patient_therapy', $args );
}
add_action( 'init', 'cdrmed_register_therapy_cpt' );

?>