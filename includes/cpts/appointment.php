<?php

// Patient Code

function cdrmed_appointment_system() {

	$labels = array(
		'name'                => _x( 'Appointments', 'Post Type General Name', 'wordpress' ),
		'singular_name'       => _x( 'Appointment', 'Post Type Singular Name', 'wordpress' ),
		'menu_name'           => __( 'Appointments', 'wordpress' ),
		'name_admin_bar'      => __( 'Appointments', 'wordpress' ),
	);
	$args = array(
		'label'               => __( 'Appointment', 'wordpress' ),
		'description'         => __( 'MednCures Patients Appointment System', 'wordpress' ),
		'labels'              => $labels,
		'supports'            => array( 'title', 'editor', 'author', 'custom-fields', ),
		'hierarchical'        => false,
		'public'              => false,
		'show_ui'             => false,
		'show_in_menu'        => true,
		'menu_position'       => 5,
		'menu_icon'           => 'dashicons-welcome-add-page',
		'show_in_admin_bar'   => true,
		'show_in_nav_menus'   => true,
		'show_in_rest'   => true,
	);
	register_post_type( 'appointment', $args );

}
add_action( 'init', 'cdrmed_appointment_system', 0 );

?>