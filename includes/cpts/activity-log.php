<?php

// Patient Code

function cdrmed_user_activity_log_system() {

	$labels = array(
		'name'                => _x( 'Activity Log', 'Post Type General Name', 'wordpress' ),
		'singular_name'       => _x( 'Activity Log', 'Post Type Singular Name', 'wordpress' ),
		'menu_name'           => __( 'Activity Log', 'wordpress' ),
		'name_admin_bar'      => __( 'Activity Log', 'wordpress' ),
	);
	$args = array(
		'label'               => __( 'Activity Log', 'wordpress' ),
		'description'         => __( 'User Activity Log', 'wordpress' ),
		'labels'              => $labels,
		'supports'            => array( 'title', 'editor', 'author', 'custom-fields', ),
		'hierarchical'        => false,
		'public'              => false,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'menu_position'       => 5,
		'menu_icon'           => 'dashicons-list-view',
		'show_in_admin_bar'   => true,
		'show_in_nav_menus'   => true,
		'show_in_rest'   => true,
	);
	register_post_type( 'cdrmed_activity_log', $args );

}
add_action( 'init', 'cdrmed_user_activity_log_system', 0 );

?>