<?php 

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly 
// Include Custom Post Types Files
include('appointment.php');
include('dispensaries.php');
include('physicians.php');
include('documents.php');
include('pre-registrations.php');
include('intake.php');
include('feedback.php');
include('symptoms_tracker_feedback.php');
include('therapy.php');
include('shoop.php');
include('activity-log.php');
?>