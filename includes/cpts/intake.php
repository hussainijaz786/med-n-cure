<?php 

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly 
// Register Custom Post Type
function intake_post_type() {
   
   /* $args = array();
      register_post_type( 'Intake', $args ); */
  $labels = array(
    'name'                  => _x( 'Intake', 'Post Type General Name', 'text_domain' ),
    'singular_name'         => _x( 'Intake', 'Post Type Singular Name', 'text_domain' ),
    'menu_name'             => __( 'Intake', 'text_domain' ),
    'name_admin_bar'        => __( 'Intake', 'text_domain' ),
  );
  $args = array(
    'label'                 => __( 'Intake', 'text_domain' ),
    'description'           => __( 'Intake Description', 'text_domain' ),
	'supports'            => array( 'title', 'editor', 'author', 'custom-fields', ),
    'show_ui'               => true,
    'show_in_menu'          => true,
    'menu_position'         => 5,
    'show_in_admin_bar'     => true,
    'show_in_nav_menus'     => true,
	'show_in_rest'   => true,
  );
  register_post_type( 'intake', $args );
}
add_action( 'init', 'intake_post_type', 0 );
?>
