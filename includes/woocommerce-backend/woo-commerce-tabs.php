<?php
error_reporting(E_ALL);
//ini_set('display_errors', 0);
// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit;

// Check if WooCommerce is active and bail if it's not
if ( ! WooCommerceCustomProductTabslab::is_woocommerce_active() ) {
	return;
}

$GLOBALS['woocommerce_product_tabs_lab'] = new WooCommerceCustomProductTabslab();

class WooCommerceCustomProductTabslab {

	private $tab_data = false;
	
	public function __construct() {
		// Installation
		add_action( 'init',             array( $this, 'load_translation' ) );
		add_action( 'woocommerce_init', array( $this, 'init' ) );
	}
	
	/**
	 * Load translations
	 *
	 * @since 1.2.5
	 */
	public function load_translation() {
		// localization
		load_plugin_textdomain( 'woocommerce-custom-product-tabs-lab', false, dirname( plugin_basename( __FILE__ ) ) . '/i18n/languages' );
	}
	
	/**
	 * Init WooCommerce Product Tabs lab extension once we know WooCommerce is active
	 */
	public function init() {
		// backend stuff
		add_action( 'woocommerce_product_write_panel_tabs', array( $this, 'product_write_panel_tab' ) );
		add_action( 'woocommerce_product_write_panels',     array( $this, 'product_write_panel' ) );
		//add_action( 'woocommerce_process_product_meta',     array( $this, 'product_save_data' ), 10, 2 );

		// frontend stuff
		add_filter( 'woocommerce_product_tabs', array( $this, 'add_custom_product_tabs' ) );
		add_action( 'woocommerce_process_product_meta',array( $this,  'woo_add_custom_general_fields_save' ));
		// allow the use of shortcodes within the tab content
		add_filter( 'woocommerce_custom_product_tabs_lab_content', 'do_shortcode' );
		/* $current_url = $_SERVER["HTTP_HOST"] . $_SERVER["REQUEST_URI"];
		if ( $_GET['action'] == 'edit'  || strpos($current_url,'?post_type=product') !== false) {
        
    		wp_enqueue_script( 'jquery-bootstrap' );
   			wp_enqueue_script( 'woo-tabs-js' );
    		wp_enqueue_style( 'custom-tabs' );

    	} */

	}
	
	public function current_page_url() {
		$pageURL = 'http';
		if( isset($_SERVER["HTTPS"]) ) {
			if ($_SERVER["HTTPS"] == "on") {$pageURL .= "s";}
		}
		$pageURL .= "://";
		if ($_SERVER["SERVER_PORT"] != "80") {
			$pageURL .= $_SERVER["SERVER_NAME"].":".$_SERVER["SERVER_PORT"].$_SERVER["REQUEST_URI"];
		} else {
			$pageURL .= $_SERVER["SERVER_NAME"].$_SERVER["REQUEST_URI"];
		}
		return $pageURL;
	}

	/** Frontend methods ******************************************************/


	/**
	 * Add the custom product tab
	 *
	 * $tabs structure:
	 * Array(
	 *   id => Array(
	 *     'title'    => (string) Tab title,
	 *     'priority' => (string) Tab priority,
	 *     'callback' => (mixed) callback function,
	 *   )
	 * )
	 *
	 * @since 1.2.0
	 * @param array $tabs array representing the product tabs
	 * @return array representing the product tabs
	 */
	public function add_custom_product_tabs( $tabs ) {
		global $product;
		
		if ( $this->product_has_custom_tabs( $product ) ) {
			foreach ( $this->tab_data as $tab ) {
				$tab_title = __( $tab['title'], 'woocommerce-custom-product-tabs-lab' );
				$tabs[ $tab['id'] ] = array(
					'title'    => apply_filters( 'woocommerce_custom_product_tabs_lab_title', $tab_title, $product, $this ),
					'priority' => 25,
					'callback' => array( $this, 'custom_product_tabs_panel_content' ),
					'content'  => $tab['content'],  // custom field
				);
			}
		}
		
		return $tabs;
	}
	
	
	/**
	 * Render the custom product tab panel content for the given $tab
	 *
	 * $tab structure:
	 * Array(
	 *   'title'    => (string) Tab title,
	 *   'priority' => (string) Tab priority,
	 *   'callback' => (mixed) callback function,
	 *   'id'       => (int) tab post identifier,
	 *   'content'  => (sring) tab content,
	 * )
	 *
	 * @param string $key tab key
	 * @param array $tab tab data
	 *
	 * @param array $tab the tab
	 */
	public function custom_product_tabs_panel_content( $key, $tab ) {

		// allow shortcodes to function
		$content = apply_filters( 'the_content', $tab['content'] );
		$content = str_replace( ']]>', ']]&gt;', $content );

		echo apply_filters( 'woocommerce_custom_product_tabs_lab_heading', '<h2>' . $tab['title'] . '</h2>', $tab );
		echo apply_filters( 'woocommerce_custom_product_tabs_lab_content', $content, $tab );
	}


	/** Admin methods ******************************************************/


	/**
	 * Adds a new tab to the Product Data postbox in the admin product interface
	 */
	public function product_write_panel_tab() {
		echo "<li class=\"product_tabs_lab_tab\"><a href=\"#woocommerce_product_tabs_lab\">" . __( 'Lab Results', 'woocommerce-custom-product-tabs-lab' ) . "</a></li>";
	}


	/**
	 * Adds the panel to the Product Data postbox in the product interface
	 */
	public function product_write_panel() {
		global $post;
		// the product
		 $post_id = !isset($_GET['post'])?null:$_GET['post'];
		// pull the custom tab data out of the database
		$tab_data = maybe_unserialize( get_post_meta( $post->ID, 'frs_woo_product_tabs', true ) );

		if ( empty( $tab_data ) ) {
			$tab_data[] = array( 'title' => '', 'content' => '' );
		}
		
		foreach ( $tab_data as $tab ) {
			// display the custom tab panel
			echo '<div id="woocommerce_product_tabs_lab" class="panel wc-metaboxes-wrapper woocommerce_options_panel">';
				// Number Field
				echo "<h2>Report of Analysis</h2>";
				echo '<div id="tabs">
					<ul id="tabs" class="nav nav-pills nav-stacked col-md-12" data-tabs="tabs">
						<li><a href="#tabs-0" data-toggle="tab">Test Details</a></li>
						<li><a href="#tabs-1" data-toggle="tab">Potency Test Results</a></li>
						<li><a href="#tabs-2" data-toggle="tab">Terpene Test Results</a></li>
						<li><a href="#tabs-3" data-toggle="tab">Residual Solvent Results</a></li>
					</ul>
					<div id="my-tab-content" class="tab-content">
						<div id="tabs-0" class="tab-pane active">
							<h2><center>Potency Test Results</center></h2>';
							echo "<div class='bdr tbs2'>";
								woocommerce_wp_text_input( array( 
									'id' => '_azcl_customer', 
									'label'             => __( 'Customer', 'woocommerce' ), 
									'class'              => 'text', 
									'type'              => 'text'
								));
								woocommerce_wp_text_input( array( 
									'id' => '_azcl_report_no', 
									'label'             => __( 'Report Number:', 'woocommerce' ),
									'class'              => 'text',  
									'type'              => 'text'
								));
								woocommerce_wp_text_input( array( 
									'id' => '_azcl_analyzed_date', 
									'label'             => __( 'Analyzed Date:', 'woocommerce' ),
									'class'              => 'text',  
									'type'              => 'date'
								));
								woocommerce_wp_text_input( array( 
									'id' => '_azcl_report_date', 
									'label'             => __( 'Report Date:', 'woocommerce' ), 
									'class'              => 'text', 
									'type'              => 'date'
								));
								woocommerce_wp_text_input( array( 
									'id' => '_azcl_received_date', 
									'label'             => __( 'Received Date:', 'woocommerce' ), 
									'class'              => 'text', 
									'type'              => 'date'
								) );
							echo '</div>';
							echo "<div class='bdr2 tbs2'>";
								woocommerce_wp_text_input( array( 
									'id' => '_azcl_sample_type', 
									'label'             => __( 'Sample Type:', 'woocommerce' ),
									'class'              => 'text', 
									'type'              => 'text'
								));
								woocommerce_wp_text_input(  array( 
									'id' => '_azcl_sample_code', 
									'label'             => __( 'Sample Code:', 'woocommerce' ), 
									'class'              => 'text', 
									'type'              => 'text'
								));
								woocommerce_wp_text_input(  array( 
									'id' => '_azcl_sample_id', 
									'label'             => __( 'Sample I.D:', 'woocommerce' ), 
									'class'              => 'text', 
									'type'              => 'text'
								));
								woocommerce_wp_text_input(  array( 
									'id' => '_azcl_issue_date', 
									'label'             => __( 'Issue Date:', 'woocommerce' ), 
									'class'              => 'text', 
									'type'              => 'date'
								));
								woocommerce_wp_text_input( array( 
									'id' => '_azcl_lab_kit', 
									'label'             => __( 'Lab Kit ID:', 'woocommerce' ),
									'class'              => 'text',  
									'type'              => 'text'
								));
							echo  '</div>
						</div>
						<div id="tabs-1" class="tab-pane">
							<h2><center>Potency Test Results</center></h2>
							<table class="table">
								<tr>
									<th>Compounds <span class="tst">Tests</span></th><th>mg/gram</th><th>PPM</th>
								</tr>
								<tr>
								<td>';
									woocommerce_wp_text_input( array( 
										'id' => '_azcl_thc', 
										'label' => __( 'THC', 'woocommerce' ), 
										'class'  => 'short',
										'type' => 'text'
									));
								echo "</td>";
								echo "<td>";
									echo '<input type="text" class="tp "  name="_azcl_thc_tests" id="_azcl_thc_tests" value="'.get_post_meta( $post_id, '_azcl_thc_tests', true ).'" placeholder="">
								</td><td>
									<input type="text" class="tp ppm "  name="_azcl_thc_ppm" id="_azcl_thc_ppm" value="'.((get_post_meta( $post_id, '_azcl_thc_ppm', true ) =='')? '0.001' : get_post_meta( $post_id, '_azcl_thc_ppm', true )).'" placeholder="">';
								echo "</td>
								</tr>
								<tr>";
								echo "<td>";
									woocommerce_wp_text_input( array( 
										'id' => '_azcl_thca', 
										'label'             => __( 'THCA', 'woocommerce' ), 
										'class'  => 'short',
										'type'              => 'text'
									));
								echo "</td>";
								echo "<td>";
									echo '<input type="text" class="tp "  name="_azcl_thca_tests" id="_azcl_thca_tests" value="'.get_post_meta( $post_id, '_azcl_thca_tests', true ).'" placeholder="">
								</td>
								<td>
									<input type="text" class="tp ppm "  name="_azcl_thca_ppm" id="_azcl_thca_ppm" value="'.((get_post_meta( $post_id, '_azcl_thca_ppm', true ) =='')? '0.001' : get_post_meta( $post_id, '_azcl_thca_ppm', true )).'" placeholder="">';
								echo "</td>
								</tr>
								<tr>";
								echo "<td>";
									woocommerce_wp_text_input( array( 
										'id' => '_azcl_delta_thc', 
										'label'             => __( 'DELTA 9-(THC)', 'woocommerce' ), 
										'class'  => 'short', 
										'class'  => 'short',
										'type'              => 'text' 
									));
								echo "</td>";
								echo "<td>";
									echo '<input type="text" class="tp "  name="_azcl_delta_thc_tests" id="_azcl_delta_thc_tests" value="'.get_post_meta( $post_id, '_azcl_delta_thc_tests', true ).'" placeholder="">
								</td>
								<td>
									<input type="text" class="tp ppm "  name="_azcl_delta_thc_ppm" id="_azcl_delta_thc_ppm" value="'.((get_post_meta( $post_id, '_azcl_delta_thc_ppm', true ) =='')? '0.001' : get_post_meta( $post_id, '_azcl_delta_thc_ppm', true )).'" placeholder="">';
								echo "</td>
								</tr>
								<tr>";
								echo "<td>";
									woocommerce_wp_text_input( array( 
										'id'  => '_azcl_thcv', 
										'label'             => __( 'THCV', 'woocommerce' ), 
										'class'  => 'short',
										'type'              => 'text'
									));
								echo "</td>";
								echo "<td>";
									echo '<input type="text" class="tp"  name="_azcl_thcv_tests" id="_azcl_thcv_tests" value="'.get_post_meta( $post_id, '_azcl_thcv_tests', true ).'" placeholder="">
								</td>
								<td>
									<input type="text" class="tp ppm"  name="_azcl_thcv_ppm" id="_azcl_thcv_ppm" value="'.((get_post_meta( $post_id, '_azcl_thcv_ppm', true ) =='')? '0.001' : get_post_meta( $post_id, '_azcl_thcv_ppm', true )).'" placeholder="">';
								echo "</td>
								</tr>
								<tr>";
								echo "<td>";
									woocommerce_wp_text_input( array( 
										'id'                => '_azcl_cbd', 
										'label'             => __( 'CBD', 'woocommerce' ), 
										'class'  => 'short',
										'type'              => 'text'
									));
								echo "</td>";
								echo "<td>";
									echo '<input type="text" class="tp"  name="_azcl_cbd_tests" id="_azcl_cbd_tests" value="'.get_post_meta( $post_id, '_azcl_cbd_tests', true ).'" placeholder="">
								</td>
								<td>
									<input type="text" class="tp ppm"  name="_azcl_cbd_ppm" id="_azcl_cbd_ppm" value="'.((get_post_meta( $post_id, '_azcl_cbd_ppm', true ) =='')? '0.001' : get_post_meta( $post_id, '_azcl_cbd_ppm', true )).'" placeholder="">';
								echo "</td>
								</tr>
								<tr>";
								echo "<td>";
									woocommerce_wp_text_input( array( 
										'id'                => '_azcl_cbda', 
										'label'             => __( 'CBDA', 'woocommerce' ), 
										'class'  => 'short', 
										'type'              => 'text', 
									));
								echo "</td>";
								echo "<td>";
									echo '<input type="text" class="tp"  name="_azcl_cbda_tests" id="_azcl_cbda_tests" value="'.get_post_meta( $post_id, '_azcl_cbda_tests', true ).'" placeholder="" >
								</td>
								<td>
									<input type="text" class="tp ppm"  name="_azcl_cbda_ppm" id="_azcl_cbda_ppm" value="'.((get_post_meta( $post_id, '_azcl_cbda_ppm', true ) =='')? '0.001' : get_post_meta( $post_id, '_azcl_cbda_ppm', true )).'" placeholder="">';
								echo "</td>
								</tr>
								<tr>";
								echo "<td>";
									woocommerce_wp_text_input( array( 
										'id'                => '_azcl_cbg', 
										'label'             => __( 'CBG', 'woocommerce' ), 
										'class'  => 'short', 
										'type'              => 'text'
									));
								echo "</td>";
								echo "<td>";
									echo '<input type="text" class="tp"  name="_azcl_cbg_tests" id="_azcl_cbg_tests" value="'.get_post_meta( $post_id, '_azcl_cbg_tests', true ).'" placeholder="">
								</td>
								<td>
									<input type="text" class="tp ppm"  name="_azcl_cbg_ppm" id="_azcl_cbg_ppm" value="'.((get_post_meta( $post_id, '_azcl_cbg_ppm', true ) =='')? '0.001' : get_post_meta( $post_id, '_azcl_cbg_ppm', true )).'" placeholder="">';
								echo "</td>
								</tr>
								<tr>";
								echo "<td>";
									woocommerce_wp_text_input( array( 
										'id'                => '_azcl_cbn', 
										'label'             => __( 'CBN', 'woocommerce' ), 
										'class'  => 'short',  
										'type'              => 'text'
									));
								echo "</td>";
								echo "<td>";
									echo '<input type="text" class="tp"  name="_azcl_cbn_tests" id="_azcl_cbn_tests" value="'.get_post_meta( $post_id, '_azcl_cbn_tests', true ).'" placeholder="">
								</td>
								<td>
									<input type="text" class="tp ppm"  name="_azcl_cbn_ppm" id="_azcl_cbn_ppm" value="'.((get_post_meta( $post_id, '_azcl_cbn_ppm', true ) =='')? '0.001' : get_post_meta( $post_id, '_azcl_cbn_ppm', true )).'" placeholder="">';
								echo "</td>
								</tr>
								<tr>";
								echo "<td>"; 
									woocommerce_wp_text_input( array( 
										'id'                => '_azcl_cbc', 
										'label'             => __( 'CBC', 'woocommerce' ), 
										'class'  => 'short',  
										'type'              => 'text'
									));
								echo "</td>";
								echo "<td>";
									echo '<input type="text" class="tp"  name="_azcl_cbc_tests" id="_azcl_cbc_tests" value="'.get_post_meta( $post_id, '_azcl_cbc_tests', true ).'" placeholder="">
								</td>
								<td>
									<input type="text" class="tp ppm"  name="_azcl_cbc_ppm" id="_azcl_cbc_ppm" value="'.((get_post_meta( $post_id, '_azcl_cbc_ppm', true ) =='')? '0.001' : get_post_meta( $post_id, '_azcl_cbc_ppm', true )).'" placeholder="">';
								echo "</td>
								</tr>";
								$array =   explode(",",trim(trim(get_post_meta( $post_id, '_azcl_compunds', true ),'[]'),'&quot;'));
								$arr2 =   explode(",",trim(trim(get_post_meta( $post_id, '_azcl_potency', true ),'[]'),'&quot;'));
								$arr3 =   explode(",",trim(trim(get_post_meta( $post_id, '_azcl_potency_tests', true ),'[]'),'&quot;'));
								$arr4 =   explode(",",trim(trim(get_post_meta( $post_id, '_azcl_potency_ppm', true ),'[]'),'&quot;'));
								$i = 0;
								
								//$arrs = get_object_vars($arr);

								if($array['0'] != 'null'  && $array['0'] != ''){
									foreach ($array as $value) {
										$str = $value;

										$value = trim($str, '&quot;');
										?>
											<tr>
												<div class="form-field _azcl_cbc_field tp">
													<td>
														<input type="text" class="form-control frst" name="_azcl_compunds[]" value="<?php echo trim($value, '"'); ?>">
														<span class="scnd"><input type="text" class="form-control scnd2" name="_azcl_potency[]" value="<?php echo trim($arr2[$i], '&quot;'); ?>"></span>
													</td>
													<td>
														<input type="text" class="form-control tp2" name="_azcl_potency_tests[]" value="<?php echo trim($arr3[$i], '&quot;'); ?>">
													</td>
													<td>
														<input type="text" class="form-control tp2" name="_azcl_potency_ppm[]" value="<?php echo trim($arr4[$i], '&quot;'); ?>"><br><br>
													</td>
												</div>
											</tr>
										<?php
										$i++;
									}
								}
								
								echo '<tr  class="input_fields_wrap"><td colspan="4">
									<button id="add_field_button" class="add_field_button">Add More Fields</button>';
								// Second tab code   
								echo '</td>
								</tr>
							</table>
						</div>
						<div id="tabs-2" class="tab-pane"> 
							<h2><center>Terpene Test Result</center></h2>
							<table class="table">
								  <tr>
									 <th>Compounds <span class="tst">Tests</span></th><th>mg/gram</th><th>PPM</th>
								  </tr>
								  <tr> ';
								 echo "<td>";
								 woocommerce_wp_text_input( 
													array( 
														'id' => '_azcl_bisabolol', 
														'label'             => __( 'Bisabolol', 'woocommerce' ), 
														'class'  => 'short', 
														'type'              => 'text'
													)
												);
									 echo "</td>";
									echo "<td>";
									echo '<input type="text" class="tp"  name="_azcl_bisabolol_tests" id="_azcl_bisabolol_tests" value="'.get_post_meta( $post_id, '_azcl_bisabolol_tests', true ).'" placeholder="">
									</td><td> <input type="text" class="tp ppm"  name="_azcl_bisabolol_ppm" id="_azcl_bisabolol_ppm" value="'.((get_post_meta( $post_id, '_azcl_bisabolol_ppm', true ) =='')? '0.001' : get_post_meta( $post_id, '_azcl_bisabolol_ppm', true )).'" placeholder="">';
								 echo "</td></tr>
											<tr>";
								   echo "<td>"; 
								   woocommerce_wp_text_input( 
													array( 
														'id' => '_azcl_alpha_bisabolol', 
														'label'             => __( 'alpha-Bisabolol', 'woocommerce' ), 
														'class'  => 'short', 
														'type'              => 'text'
													)
												);
									 echo "</td>";
									echo "<td>";
									echo '<input type="text" class="tp"  name="_azcl_alpha_bisabolol_tests" id="_azcl_alpha_tests" value="'.get_post_meta( $post_id, '_azcl_alpha_bisabolol_tests', true ).'" placeholder="">
									</td><td> <input type="text" class="tp ppm"  name="_azcl_alpha_bisabolol_ppm" id="_azcl_alpha_ppm" value="'.((get_post_meta( $post_id, '_azcl_alpha_bisabolol_ppm', true ) =='')? '0.001' : get_post_meta( $post_id, '_azcl_alpha_ppm', true )).'" placeholder="">';
								 echo "</td></tr>
											<tr>";
								   echo "<td>"; 
								  woocommerce_wp_text_input( 
													array( 
														'id' => '_azcl_camphene', 
														'label'             => __( 'Camphene', 'woocommerce' ), 
														'class'  => 'short', 
														'type'              => 'text' 
													)
												);
									echo "</td>";
									echo "<td>";
								 echo '<input type="text" class="tp"  name="_azcl_camphene_tests" id="_azcl_camphene_tests" value="'.get_post_meta( $post_id, '_azcl_camphene_tests', true ).'" placeholder="">
								   </td><td>  <input type="text" class="tp ppm"  name="_azcl_camphene_ppm" id="_azcl_camphene_ppm" value="'.((get_post_meta( $post_id, '_azcl_camphene_ppm', true ) =='')? '0.001' : get_post_meta( $post_id, '_azcl_camphene_ppm', true )).'" placeholder="">';
								echo "</td></tr>
											<tr>";
								   echo "<td>"; 
								woocommerce_wp_text_input( 
													array( 
														'id'  => '_azcl_delta_3_carene', 
														'label'             => __( 'Delta-3-Carene', 'woocommerce' ), 
														'class'  => 'short', 
														'type'              => 'text'
													)
												);
								echo "</td>";
									echo "<td>";
								echo '<input type="text" class="tp"  name="_azcl_delta_3_carene_tests" id="_azcl_delta3_carene_tests" value="'.get_post_meta( $post_id, '_azcl_delta3_carene_tests', true ).'" placeholder="">
									</td><td> <input type="text" class="tp ppm"  name="_azcl_delta_3_carene_ppm" id="_azcl_delta3_carene_ppm" value="'.((get_post_meta( $post_id, '_azcl_delta3_carene_ppm', true ) =='')? '0.001' : get_post_meta( $post_id, '_azcl_delta3_carene_ppm', true )).'" placeholder="">';
								echo "</td></tr>
											<tr>";
								   echo "<td>"; 
								woocommerce_wp_text_input( 
													array( 
														'id'                => '_azcl_beta_caryophyllene', 
														'label'             => __( 'Beta-Caryophyllene', 'woocommerce' ), 
														'class'  => 'short',  
														'type'              => 'text'
													)
												);
								echo "</td>";
									echo "<td>";
								echo '<input type="text" class="tp"  name="_azcl_beta_caryophyllene_tests" id="_azcl_beta_caryophyllene_tests" value="'.get_post_meta( $post_id, '_azcl_beta_caryophyllene_tests', true ).'" placeholder="">
								   </td><td> <input type="text" class="tp ppm"  name="_azcl_beta_caryophyllene_ppm" id="_azcl_beta_caryophyllene_ppm" value="'.((get_post_meta( $post_id, '_azcl_beta_caryophyllene_ppm', true ) =='')? '0.001' : get_post_meta( $post_id, '_azcl_beta_caryophyllene_ppm', true )).'" placeholder="">';
								echo "</td></tr>
											<tr>";
								   echo "<td>"; 
								woocommerce_wp_text_input( 
													array( 
														'id'                => '_azcl_caryophyllene_oxide', 
														'label'             => __( 'Caryophyllene Oxide', 'woocommerce' ), 
														'class'  => 'short',  
														'type'              => 'text', 
														 
													)
												);
								echo "</td>";
									echo "<td>";
								echo '<input type="text" class="tp"  name="_azcl_caryophyllene_oxide_tests" id="_azcl_caryophyllene_oxide_tests" value="'.get_post_meta( $post_id, '_azcl_caryophyllene_oxide_tests', true ).'" placeholder="" >
								 </td><td> <input type="text" class="tp ppm"  name="_azcl_caryophyllene_oxide_ppm" id="_azcl_caryophyllene_oxide_ppm" value="'.((get_post_meta( $post_id, '_azcl_caryophyllene_oxide_ppm', true ) =='')? '0.001' : get_post_meta( $post_id, '_azcl_caryophyllene_oxide_ppm', true )).'" placeholder="">
								';
								echo "</td></tr>
											<tr>";
								   echo "<td>"; 
								woocommerce_wp_text_input( 
													array( 
														'id'                => '_azcl_p_cymene', 
														'label'             => __( 'P-Cymene', 'woocommerce' ), 
														'class'  => 'short',  
														'type'              => 'text'
													)
												);
								echo "</td>";
									echo "<td>";
								echo '<input type="text" class="tp"  name="_azcl_p_cymene_tests" id="_azcl_p_cymene_tests" value="'.get_post_meta( $post_id, '_azcl_p_cymene_tests', true ).'" placeholder="">
									 </td><td> <input type="text" class="tp ppm"  name="_azcl_p_cymene_ppm" id="_azcl_p_cymene_ppm" value="'.((get_post_meta( $post_id, '_azcl_p_cymene_ppm', true ) =='')? '0.001' : get_post_meta( $post_id, '_azcl_p_cymene_ppm', true )).'" placeholder="">';
								echo "</td></tr>
											<tr>";
								   echo "<td>"; 
								  woocommerce_wp_text_input( 
													array( 
														'id'                => '_azcl_geraniol', 
														'label'             => __( 'Geraniol', 'woocommerce' ), 
														'class'  => 'short',  
														'type'              => 'text'
													)
												);
								echo "</td>";
									echo "<td>";
								   echo '<input type="text" class="tp"  name="_azcl_geraniol_tests" id="_azcl_geraniol_tests" value="'.get_post_meta( $post_id, '_azcl_geraniol_tests', true ).'" placeholder="">
								 </td><td>   <input type="text" class="tp ppm"  name="_azcl_geraniol_ppm" id="_azcl_geraniol_ppm" value="'.((get_post_meta( $post_id, '_azcl_geraniol_ppm', true ) =='')? '0.001' : get_post_meta( $post_id, '_azcl_geraniol_ppm', true )).'" placeholder="">';
								echo "</td></tr>
											<tr>";
								   echo "<td>"; 
								 woocommerce_wp_text_input( 
													array( 
														'id'                => '_azcl_guaiol', 
														'label'             => __( 'Guaiol', 'woocommerce' ), 
														'class'  => 'short',  
														'type'              => 'text'
													)
												);
								echo "</td>";
									echo "<td>";
								   echo '<input type="text" class="tp"  name="_azcl_guaiol_tests" id="_azcl_guaiol_tests" value="'.get_post_meta( $post_id, '_azcl_guaiol_tests', true ).'" placeholder="">
								  </td><td>  <input type="text" class="tp ppm"  name="_azcl_guaiol_ppm" id="_azcl_guaiol_ppm" value="'.((get_post_meta( $post_id, '_azcl_guaiol_ppm', true ) =='')? '0.001' : get_post_meta( $post_id, '_azcl_guaiol_ppm', true )).'" placeholder="">';
								echo "</td></tr>
											<tr>";
								   echo "<td>"; 
									  woocommerce_wp_text_input( 
													array( 
														'id'                => '_azcl_alpha_humulene', 
														'label'             => __( 'Alpha-Humulene', 'woocommerce' ), 
														'class'  => 'short',  
														'type'              => 'text'
													)
												);
								echo "</td>";
									echo "<td>";
								   echo '<input type="text" class="tp"  name="_azcl_alpha_humulene_tests" id="_azcl_alpha_humulene_tests" value="'.get_post_meta( $post_id, '_azcl_alpha_humulene_tests', true ).'" placeholder="">
								  </td><td>  <input type="text" class="tp ppm"  name="_azcl_alpha_humulene_ppm" id="_azcl_alpha_humulene_ppm" value="'.((get_post_meta( $post_id, '_azcl_alpha_humulene_ppm', true ) =='')? '0.001' : get_post_meta( $post_id, '_azcl_alpha_humulene_ppm', true )).'" placeholder="">';
								echo "</td></tr>
											<tr>";
								   echo "<td>"; 
									woocommerce_wp_text_input( 
													array( 
														'id'                => '_azcl_isopulegol', 
														'label'             => __( 'Isopulegol', 'woocommerce' ), 
														'class'  => 'short',  
														'type'              => 'text'
													)
												);
								echo "</td>";
									echo "<td>";
								   echo '<input type="text" class="tp"  name="_azcl_isopulegol_tests" id="_azcl_isopulegol_tests" value="'.get_post_meta( $post_id, '_azcl_isopulegol_tests', true ).'" placeholder="">
								   </td><td> <input type="text" class="tp ppm"  name="_azcl_isopulegol_ppm" id="_azcl_isopulegol_ppm" value="'.((get_post_meta( $post_id, '_azcl_isopulegol_ppm', true ) =='')? '0.001' : get_post_meta( $post_id, '_azcl_isopulegol_ppm', true )).'" placeholder="">';
								echo "</td></tr>
											<tr>";
								   echo "<td>";
								 woocommerce_wp_text_input( 
													array( 
														'id'                => '_azcl_delta_limonene', 
														'label'             => __( 'Delta-Limonene', 'woocommerce' ), 
														'class'  => 'short',  
														'type'              => 'text'
													)
												);
								echo "</td>";
									echo "<td>";
								   echo '<input type="text" class="tp"  name="_azcl_delta_limonene_tests" id="_azcl_delta_limonene_tests" value="'.get_post_meta( $post_id, '_azcl_delta_limonene_tests', true ).'" placeholder="">
								 </td><td>   <input type="text" class="tp ppm"  name="_azcl_delta_limonene_ppm" id="_azcl_delta_limonene_ppm" value="'.((get_post_meta( $post_id, '_azcl_delta_limonene_ppm', true ) =='')? '0.001' : get_post_meta( $post_id, '_azcl_delta_limonene_ppm', true )).'" placeholder="">';
								echo "</td></tr>
											<tr>";
								   echo "<td>";
									woocommerce_wp_text_input( 
													array( 
														'id'                => '_azcl_limonene', 
														'label'             => __( 'Limonene', 'woocommerce' ), 
														'class'  => 'short',  
														'type'              => 'text'
													)
												);
								echo "</td>";
									echo "<td>";
								   echo '<input type="text" class="tp"  name="_azcl_limonene_tests" id="_azcl_linalool_tests" value="'.get_post_meta( $post_id, '_azcl_limonene_tests', true ).'" placeholder="">
								 </td><td>   <input type="text" class="tp ppm"  name="_azcl_limonene_ppm" id="_azcl_linalool_ppm" value="'.((get_post_meta( $post_id, '_azcl_linalool_ppm', true ) =='')? '0.001' : get_post_meta( $post_id, '_azcl_linalool_ppm', true )).'" placeholder="">';
								echo "</td></tr>
											<tr>";
								   echo "<td>";
								 woocommerce_wp_text_input( 
													array( 
														'id'                => '_azcl_linalool', 
														'label'             => __( 'Linalool', 'woocommerce' ), 
														'class'  => 'short',  
														'type'              => 'text'
													)
												);
								echo "</td>";
									echo "<td>";
								   echo '<input type="text" class="tp"  name="_azcl_linalool_tests" id="_azcl_linalool_tests" value="'.get_post_meta( $post_id, '_azcl_linalool_tests', true ).'" placeholder="">
								 </td><td>   <input type="text" class="tp ppm"  name="_azcl_linalool_ppm" id="_azcl_linalool_ppm" value="'.((get_post_meta( $post_id, '_azcl_linalool_ppm', true ) =='')? '0.001' : get_post_meta( $post_id, '_azcl_linalool_ppm', true )).'" placeholder="">';
								echo "</td></tr>
											<tr>";
								   echo "<td>";
								woocommerce_wp_text_input( 
													array( 
														'id'                => '_azcl_myrcene', 
														'label'             => __( 'Myrcene', 'woocommerce' ), 
														'class'  => 'short',  
														'type'              => 'text'
													)
												);
								echo "</td>";
									echo "<td>";
								   echo '<input type="text" class="tp"  name="_azcl_myrcene_tests" id="_azcl_myrcene_tests" value="'.get_post_meta( $post_id, '_azcl_myrcene_tests', true ).'" placeholder="">
								  </td><td>  <input type="text" class="tp ppm"  name="_azcl_myrcene_ppm" id="_azcl_myrcene_ppm" value="'.((get_post_meta( $post_id, '_azcl_myrcene_ppm', true ) =='')? '0.001' : get_post_meta( $post_id, '_azcl_myrcene_ppm', true )).'" placeholder="">';
								echo "</td></tr>
											<tr>";
								   echo "<td>";
								woocommerce_wp_text_input( 
													array( 
														'id'                => '_azcl_nerolidol_1', 
														'label'             => __( 'Nerolidol 1', 'woocommerce' ), 
														'class'  => 'short',  
														'type'              => 'text'
													)
												);
								echo "</td>";
									echo "<td>";
								   echo '<input type="text" class="tp"  name="_azcl_nerolidol_1_tests" id="_azcl_nerolidol_1_tests" value="'.get_post_meta( $post_id, '_azcl_nerolidol_1_tests', true ).'" placeholder="">
								  </td><td>  <input type="text" class="tp ppm"  name="_azcl_nerolidol_1_ppm" id="_azcl_nerolidol_1_ppm" value="'.((get_post_meta( $post_id, '_azcl_nerolidol_1_ppm', true ) =='')? '0.001' : get_post_meta( $post_id, '_azcl_nerolidol_1_ppm', true )).'" placeholder="">';
								echo "</td></tr>
											<tr>";
								   echo "<td>";
								woocommerce_wp_text_input( 
													array( 
														'id'                => '_azcl_nerolidol_2', 
														'label'             => __( 'Nerolidol 2', 'woocommerce' ), 
														'class'  => 'short',  
														'type'              => 'text'
													)
												);
								echo "</td>";
									echo "<td>";
								   echo '<input type="text" class="tp"  name="_azcl_nerolidol_2_tests" id="_azcl_nerolidol_2_tests" value="'.get_post_meta( $post_id, '_azcl_nerolidol_2_tests', true ).'" placeholder="">
								 </td><td>   <input type="text" class="tp ppm"  name="_azcl_nerolidol_2_ppm" id="_azcl_nerolidol_2_ppm" value="'.((get_post_meta( $post_id, '_azcl_nerolidol_2_ppm', true ) =='')? '0.001' : get_post_meta( $post_id, '_azcl_nerolidol_2_ppm', true )).'" placeholder="">';
								echo "</td></tr>
											<tr>";
								   echo "<td>";
								woocommerce_wp_text_input( 
													array( 
														'id'                => '_azcl_ocimene', 
														'label'             => __( 'Ocimene', 'woocommerce' ), 
														'class'  => 'short',  
														'type'              => 'text'
													)
												);
								echo "</td>";
									echo "<td>";
								   echo '<input type="text" class="tp"  name="_azcl_ocimene_tests" id="_azcl_ocimene_tests" value="'.get_post_meta( $post_id, '_azcl_ocimene_tests', true ).'" placeholder="">
								 </td><td>   <input type="text" class="tp ppm"  name="_azcl_ocimene_ppm" id="_azcl_ocimene_ppm" value="'.((get_post_meta( $post_id, '_azcl_ocimene_ppm', true ) =='')? '0.001' : get_post_meta( $post_id, '_azcl_ocimene_ppm', true )).'" placeholder="">';
								echo "</td></tr>
											<tr>";
								   echo "<td>";
								woocommerce_wp_text_input( 
													array( 
														'id'                => '_azcl_alpha_pinene', 
														'label'             => __( 'alpha-Pinene', 'woocommerce' ), 
														'class'  => 'short',  
														'type'              => 'text'
													)
												);
								echo "</td>";
									echo "<td>";
								   echo '<input type="text" class="tp"  name="_azcl_alpha_pinene_tests" id="_azcl_alpha_pinene_tests" value="'.get_post_meta( $post_id, '_azcl_alpha_pinene_tests', true ).'" placeholder="">
								  </td><td>  <input type="text" class="tp ppm"  name="_azcl_alpha_pinene_ppm" id="_azcl_alpha_pinene_ppm" value="'.((get_post_meta( $post_id, '_azcl_alpha_pinene_ppm', true ) =='')? '0.001' : get_post_meta( $post_id, '_azcl_alpha_pinene_ppm', true )).'" placeholder="">';

								echo "</td></tr>
											<tr>";
								   echo "<td>";
								woocommerce_wp_text_input( 
													array( 
														'id'                => '_azcl_beta_pinene', 
														'label'             => __( 'Beta-Pinene', 'woocommerce' ), 
														'class'  => 'short',  
														'type'              => 'text'
													)
												);
								echo "</td>";
									echo "<td>";
								   echo '<input type="text" class="tp"  name="_azcl_beta_pinene_tests" id="_azcl_beta_pinene_tests" value="'.get_post_meta( $post_id, '_azcl_beta_pinene_tests', true ).'" placeholder="">
								  </td><td>  <input type="text" class="tp ppm"  name="_azcl_beta_pinene_ppm" id="_azcl_beta_pinene_ppm" value="'.((get_post_meta( $post_id, '_azcl_beta_pinene_ppm', true ) =='')? '0.001' : get_post_meta( $post_id, '_azcl_beta_pinene_ppm', true )).'" placeholder="">';
								echo "</td></tr>
											<tr>";
								   echo "<td>";
								woocommerce_wp_text_input( 
													array( 
														'id'                => '_azcl_alpha_terpinene', 
														'label'             => __( 'alpha-Terpinene', 'woocommerce' ), 
														'class'  => 'short',  
														'type'              => 'text'
													)
												);
								echo "</td>";
									echo "<td>";
								   echo '<input type="text" class="tp"  name="_azcl_alpha_terpinene_tests" id="_azcl_alpha_terpinene_tests" value="'.get_post_meta( $post_id, '_azcl_alpha_terpinene_tests', true ).'" placeholder="">
								   </td><td>   <input type="text" class="tp ppm"  name="_azcl_alpha_terpinene_ppm" id="_azcl_alpha_terpinene_ppm" value="'.((get_post_meta( $post_id, '_azcl_alpha_terpinene_ppm', true ) =='')? '0.001' : get_post_meta( $post_id, '_azcl_alpha_terpinene_ppm', true )).'" placeholder="">';
								echo "</td></tr>
											<tr>";
								   echo "<td>";
								woocommerce_wp_text_input( 
													array( 
														'id'                => '_azcl_gamma_terpinene', 
														'label'             => __( 'Gamma-Terpinene', 'woocommerce' ), 
														'class'  => 'short',  
														'type'              => 'text'
													)
												);
								echo "</td>";
									echo "<td>";
								   echo '<input type="text" class="tp"  name="_azcl_gamma_terpinene_tests" id="_azcl_gamma_terpinene_terpinene_tests" value="'.get_post_meta( $post_id, '_azcl_gamma_terpinene_tests', true ).'" placeholder="">
								  </td><td>  <input type="text" class="tp ppm"  name="_azcl_gamma_terpinene_ppm" id="_azcl_gamma_terpinene_ppm" value="'.((get_post_meta( $post_id, '_azcl_gamma_terpinene_ppm', true ) =='')? '0.001' : get_post_meta( $post_id, '_azcl_gamma_terpinene_ppm', true )).'" placeholder="">';
								echo "</td></tr>
											<tr>";
								   echo "<td>";
								woocommerce_wp_text_input( 
													array( 
														'id'                => '_azcl_terpinolene', 
														'label'             => __( 'Terpinolene', 'woocommerce' ), 
														'class'  => 'short',  
														'type'              => 'text'
													)
												);
								echo "</td>";
									echo "<td>";
								   echo '<input type="text" class="tp"  name="_azcl_terpinolene_tests" id="_azcl_terpinolene_tests" value="'.get_post_meta( $post_id, '_azcl_terpinolene_tests', true ).'" placeholder="">
								   </td><td> <input type="text" class="tp ppm"  name="_azcl_terpinolene_ppm" id="_azcl_terpinolene_ppm" value="'.((get_post_meta( $post_id, '_azcl_terpinolene_ppm', true ) =='')? '0.001' : get_post_meta( $post_id, '_azcl_terpinolene_ppm', true )).'" placeholder="">';
								echo "</td></tr>";

								/*echo '<p class="form-field _azcl_terpinolene_field "><label for="_azcl_terpinolene">Total Terpenes</label><input type="text" class="short" style="" readonly name="_azcl_total_trepene" id="_azcl_total_trepene"> </p>';
								echo '<input type="text" class="tp"  name="_azcl_total_trepene_tests" id="_azcl_total_trepene_tests" readonly>';*/

								$array =   explode(",",trim(trim(get_post_meta( $post_id, '_azcl_compunds_terpene', true ),'[]'),'&quot;'));
								$arr2 =   explode(",",trim(trim(get_post_meta( $post_id, '_azcl_terpene', true ),'[]'),'&quot;'));
								$arr3 =   explode(",",trim(trim(get_post_meta( $post_id, '_azcl_terpene_tests', true ),'[]'),'&quot;'));
								$arr4 =   explode(",",trim(trim(get_post_meta( $post_id, '_azcl_terpene_ppm', true ),'[]'),'&quot;'));
								$i = 0;
								
								//$arrs = get_object_vars($arr);
								if($array['0'] != '' && $array['0'] != 'null'){
									foreach ($array as $value) {
										$str = $value;

										$value = trim($str, '&quot;');
										?><tr><div class="form-field _azcl_cbc_field tp">
												<td><input type="text" class="form-control frst" name="_azcl_compunds_terpene[]" value="<?php echo trim($value, '"'); ?>">
												<span class="scnd"><input type="text" class="form-control scnd2" name="_azcl_terpene[]" value="<?php echo trim($arr2[$i], '&quot;'); ?>"></span></td>
												<td><input type="text" class="form-control tp2" name="_azcl_terpene_tests[]" value="<?php echo trim($arr3[$i], '&quot;'); ?>"></td>
												<td><input type="text" class="form-control tp2" name="_azcl_terpene_ppm[]" value="<?php echo trim($arr4[$i], '&quot;'); ?>"><br><br></td>		
										</div></tr>
										<?php
										$i++;
									}
								}
								echo '<tr  class="input_fields_wrap2"><td colspan="4">
									<button class="add_field_button2">Add More Fields</button>';

								echo '</td></tr>';
									// third tab code start from here
							echo '</table>
						</div>
						<div id="tabs-3" class="tab-pane">
							<h2><center>Residual Solvent Test Results</center></h2>
							<table class="table">
									<tr>
										<th>Compounds <span class="tst">Tests</span></th><th>mg/gram</th><th>PPM</th>
									</tr>
									<tr> ';
									echo "<td>";
									   woocommerce_wp_text_input( 
														array( 
															'id' => '_azcl_methanol', 
															'label'             => __( 'Methanol', 'woocommerce' ), 
															'class'  => 'short',  
															'type'              => 'text'
														)
													);
									echo "</td>";
										echo "<td>";
										echo '<input type="text" class="tp"  name="_azcl_methanol_tests" id="_azcl_methanol_tests" >
										</td><td> <input type="text" class="tp ppm" value="'.((get_post_meta( $post_id, '_azcl_methanol_ppm', true ) =='')? '0.001' : get_post_meta( $post_id, '_azcl_methanol_ppm', true )).'"  name="_azcl_methanol_ppm" id="__azcl_methanol_ppm">';
									  echo "</td></tr>
												<tr>";
									   echo "<td>";
									  woocommerce_wp_text_input( 
														array( 
															'id' => '_azcl_pentane', 
															'label'             => __( 'Pentane', 'woocommerce' ), 
															'class'  => 'short', 
															'type'              => 'text' 
														)
													);
									  echo "</td>";
										echo "<td>";
									 echo '<input type="text" class="tp"  name="_azcl_pentane_tests" id="_azcl_pentane_tests" value="'.get_post_meta( $post_id, '_azcl_pentane_tests', true ).'" placeholder="">
										</td><td>  <input type="text" class="tp ppm"  name="_azcl_pentane_ppm" id="_azcl_pentane_ppm" value="'.((get_post_meta( $post_id, '_azcl_pentane_ppm', true ) =='')? '0.001' : get_post_meta( $post_id, '_azcl_pentane_ppm', true )).'" placeholder="">';
									 echo "</td></tr>
												<tr>";
									   echo "<td>";
									woocommerce_wp_text_input( 
														array( 
															'id'  => '_azcl_ethanol', 
															'label'             => __( 'Ethanol', 'woocommerce' ), 
															'class'  => 'short', 
															'type'              => 'text'
														)
													);
									   echo "</td>";
										echo "<td>";
									echo '<input type="text" class="tp"  name="_azcl_ethanol_tests" id="_azcl_ethanol_tests" value="'.get_post_meta( $post_id, '_azcl_ethanol_tests', true ).'" placeholder="">
									   </td><td>  <input type="text" class="tp ppm"  name="_azcl_ethanol_ppm" id="_azcl_ethanol_ppm" value="'.((get_post_meta( $post_id, '_azcl_ethanol_ppm', true ) =='')? '0.001' : get_post_meta( $post_id, '_azcl_ethanol_ppm', true )).'" placeholder="">';
									echo "</td></tr>
												<tr>";
									   echo "<td>";
									woocommerce_wp_text_input( 
														array( 
															'id'                => '_azcl_acetone', 
															'label'             => __( 'Acetone', 'woocommerce' ), 
															'class'  => 'short',  
															'type'              => 'text'
														)
													);
									echo "</td>";
									   echo "<td>";
									echo '<input type="text" class="tp"  name="_azcl_acetone_tests" id="_azcl_acetone_tests" value="'.get_post_meta( $post_id, '_azcl_acetone_tests', true ).'" placeholder="">
									   </td><td>  <input type="text" class="tp ppm"  name="_azcl_acetone_ppm" id="_azcl_acetone_ppm" value="'.((get_post_meta( $post_id, '_azcl_acetone_ppm', true ) =='')? '0.001' : get_post_meta( $post_id, '_azcl_acetone_ppm', true )).'" placeholder="">';
									echo "</td></tr>
												<tr>";
									   echo "<td>";
									 woocommerce_wp_text_input( 
														array( 
															'id'                => '_azcl_iospropyl', 
															'label'             => __( 'Iospropyl Alcohol', 'woocommerce' ), 
															'class'  => 'short',  
															'type'              => 'text', 
															 
														)
													);
									echo "</td>";
									   echo "<td>";
									echo '<input type="text" class="tp"  name="_azcl_iospropyl_tests" id="_azcl_iospropyl_tests" value="'.get_post_meta( $post_id, '_azcl_iospropyl_tests', true ).'" placeholder="">
									 </td><td> <input type="text" class="tp ppm"  name="_azcl_iospropyl_ppm" id="_azcl_iospropyl_ppm" value="'.((get_post_meta( $post_id, '_azcl_iospropyl_ppm', true ) =='')? '0.001' : get_post_meta( $post_id, '_azcl_iospropyl_ppm', true )).'" placeholder="">
									';
									echo "</td></tr>
												<tr>";
									   echo "<td>";
									woocommerce_wp_text_input( 
														array( 
															'id'                => '_azcl_hexane', 
															'label'             => __( 'Hexane', 'woocommerce' ), 
															'class'  => 'short',  
															'type'              => 'text'
														)
													);
									echo "</td>";
									   echo "<td>";
									echo '<input type="text" class="tp"  name="_azcl_hexane_tests" id="_azcl_hexane_tests" value="'.get_post_meta( $post_id, '_azcl_hexane_tests', true ).'" placeholder="">
									   </td><td> <input type="text" class="tp ppm"  name="_azcl_hexane_ppm" id="_azcl_hexane_ppm" value='.((get_post_meta( $post_id, '_azcl_hexane_ppm', true ) =='')? '0.001' : get_post_meta( $post_id, '_azcl_hexane_ppm', true )).' placeholder="">';
									echo "</td></tr>
												<tr>";
									   echo "<td>";
									woocommerce_wp_text_input( 
														array( 
															'id'                => '_azcl_benzene', 
															'label'             => __( 'Benzene', 'woocommerce' ), 
															'class'  => 'short',  
															'type'              => 'text'
														)
													);
									 echo "</td>";
									   echo "<td>";
									echo '<input type="text" class="tp"  name="_azcl_benzene_tests" id="_azcl_benzene_tests" value="'.get_post_meta( $post_id, '_azcl_benzene_tests', true ).'" placeholder="">
										</td><td> <input type="text" class="tp ppm"  name="_azcl_benzene_ppm" id="_azcl_benzene_ppm" value="'.((get_post_meta( $post_id, '_azcl_benzene_ppm', true ) =='')? '0.001' : get_post_meta( $post_id, '_azcl_benzene_ppm', true )).'" placeholder="">';
									echo "</td></tr>
												<tr>";
									   echo "<td>";
									   woocommerce_wp_text_input( 
														array( 
															'id'                => '_azcl_toluene', 
															'label'             => __( 'Toluene', 'woocommerce' ), 
															'class'  => 'short',  
															'type'              => 'text'
														)
													);
									 echo "</td>";
									   echo "<td>";
										echo '<input type="text" class="tp"  name="_azcl_toluene_tests" id="_azcl_toluene_tests" value="'.get_post_meta( $post_id, '_azcl_toluene_tests', true ).'" placeholder="">
									</td><td><input type="text" class="tp ppm"  name="_azcl_toluene_ppm" id="_azcl_toluene_ppm" value="'.((get_post_meta( $post_id, '_azcl_toluene_ppm', true ) =='')? '0.001' : get_post_meta( $post_id, '_azcl_toluene_ppm', true )).'" placeholder="">';
								echo "</td></tr>";
							
							
								$array =   explode(",",trim(trim(get_post_meta( $post_id, 'compunds_residual', true ),'[]'),'&quot;'));
								$arr2  =   explode(",",trim(trim(get_post_meta( $post_id, 'residual', true ),'[]'),'&quot;'));
								$arr3  =   explode(",",trim(trim(get_post_meta( $post_id, 'residual_tests', true ),'[]'),'&quot;'));
								$arr4  =   explode(",",trim(trim(get_post_meta( $post_id, 'residual_ppm', true ),'[]'),'&quot;'));
								$i = 0;


								//$arrs = get_object_vars($arr);
								if($array['0'] != ''){
									foreach ($array as $value) {
										$str = $value;

										$value = trim($str, '&quot;');
										?><tr><div class="form-field _azcl_cbc_field tp">
												<td><input type="text" class="form-control frst" name="compunds_residual[]" value="<?php echo trim($value, '"'); ?>">
												<span class="scnd"><input type="text" class="form-control scnd2" name="residual[]" value="<?php echo trim($arr2[$i], '&quot;'); ?>"></span></td>
												<td><input type="text" class="form-control tp2" name="residual_tests[]" value="<?php echo trim($arr3[$i], '&quot;'); ?>"></td>
												<td><input type="text" class="form-control tp2" name="residual_ppm[]" value="<?php echo trim($arr4[$i], '&quot;'); ?>"><br><br>	</td>	
										</div></tr>
										<?php
										$i++;
									}
								}
								echo '<tr class="input_fields_wrap3"><td colspan="4">
									<button  class="add_field_button3">Add More Fields</button>';
								echo '<td>
								<tr>';
							echo '</table>
						</div>';
						
						
						
						
						
					echo '</div>';
				echo '</div>';
			echo '</div>';
		}
	}


	/**
	 * Saves the data inputed into the product boxes, as post meta data
	 * identified by the name 'frs_woo_product_tabs'
	 *
	 * @param int $post_id the post (product) identifier
	 * @param stdClass $post the post (product)
	 */

	public function woo_add_custom_general_fields_save( $post_id, $post ){
		/**/
	   // first part customer

		//New rows data
		$i = 0;
		$compunds = json_encode($_POST['_azcl_compunds']);
		if( !empty( $compunds ) )
			update_post_meta( $post_id, '_azcl_compunds', esc_attr( $compunds ) );
		$potency = $_POST['_azcl_potency'];
		$tests = $_POST['_azcl_potency_tests'];
		$ppm = $_POST['_azcl_potency_ppm'];
		foreach ($_POST['_azcl_compunds'] as $value) {

			$value = strtolower($value);
			update_post_meta( $post_id, '_azcl_'.strtolower($value), esc_attr( $potency[$i] ) );
			update_post_meta( $post_id, '_azcl_'.strtolower($value).'_test', esc_attr( $tests[$i] ) );
			update_post_meta( $post_id, '_azcl_'.strtolower($value).'_ppm'.strtolower($value), esc_attr( $ppm[$i] ) );
			$i++;
		}

		$compunds = json_encode($_POST['_azcl_potency']);
		if( !empty( $compunds ) )
			update_post_meta( $post_id, '_azcl_potency', esc_attr( $compunds ) );
		$compunds = json_encode($_POST['_azcl_potency_tests']);
		if( !empty( $compunds ) )
			update_post_meta( $post_id, '_azcl_potency_tests', esc_attr( $compunds ) );
		$compunds = json_encode($_POST['_azcl_potency_ppm']);
		if( !empty( $compunds ) )
			update_post_meta( $post_id, '_azcl_potency_ppm', esc_attr( $compunds ) );



		$woocommerce_select = $_POST['_azcl_customer'];
		if( !empty( $woocommerce_select ) )
			update_post_meta( $post_id, '_azcl_customer', esc_attr( $woocommerce_select ) );

		$woocommerce_select = $_POST['_azcl_report_no'];
		if( !empty( $woocommerce_select ) )
			update_post_meta( $post_id, '_azcl_report_no', esc_attr( $woocommerce_select ) );

			$woocommerce_select = $_POST['_azcl_analyzed_date'];
		if( !empty( $woocommerce_select ) )
			update_post_meta( $post_id, '_azcl_analyzed_date', esc_attr( $woocommerce_select ) );

			$woocommerce_select = $_POST['_azcl_report_date'];
		if( !empty( $woocommerce_select ) )
			update_post_meta( $post_id, '_azcl_report_date', esc_attr( $woocommerce_select ) );

		$woocommerce_select = $_POST['_azcl_received_date'];
		if( !empty( $woocommerce_select ) )
			update_post_meta( $post_id, '_azcl_received_date', esc_attr( $woocommerce_select ) );

		// Second part Sample

		$potency = $_POST['_azcl_terpene'];
		$tests = $_POST['_azcl_terpene_tests'];
		$ppm = $_POST['_azcl_terpene_ppm'];
		foreach ($_POST['_azcl_compunds_terpene'] as $value) {

			$value = strtolower($value);
			update_post_meta( $post_id, '_azcl_'.strtolower($value), esc_attr( $potency[$i] ) );
			update_post_meta( $post_id, '_azcl_'.strtolower($value).'_test', esc_attr( $tests[$i] ) );
			update_post_meta( $post_id, '_azcl_'.strtolower($value).'_ppm'.strtolower($value), esc_attr( $ppm[$i] ) );
			$i++;
		}

		$potency = $_POST['residual'];
		$tests = $_POST['residual_tests'];
		$ppm = $_POST['residual_ppm'];
		foreach ($_POST['compunds_residual'] as $value) {

			$value = strtolower($value);
			update_post_meta( $post_id, '_azcl_'.strtolower($value), esc_attr( $potency[$i] ) );
			update_post_meta( $post_id, '_azcl_'.strtolower($value).'_test', esc_attr( $tests[$i] ) );
			update_post_meta( $post_id, '_azcl_'.strtolower($value).'_ppm'.strtolower($value), esc_attr( $ppm[$i] ) );
			$i++;
		}
		$compunds = json_encode($_POST['_azcl_compunds_terpene']);
		if( !empty( $compunds ) )
			update_post_meta( $post_id, '_azcl_compunds_terpene', esc_attr( $compunds ) );
		$compunds = json_encode($_POST['_azcl_terpene']);
		if( !empty( $compunds ) )
			update_post_meta( $post_id, '_azcl_terpene', esc_attr( $compunds ) );

		$compunds = json_encode($_POST['_azcl_terpene_tests']);
		if( !empty( $compunds ) )
			update_post_meta( $post_id, '_azcl_terpene_tests', esc_attr( $compunds ) );
		$compunds = json_encode($_POST['_azcl_terpene_ppm']);
		if( !empty( $compunds ) )
			update_post_meta( $post_id, '_azcl_terpene_ppm', esc_attr( $compunds ) );

		$woocommerce_select = $_POST['_azcl_sample_type'];
		if( !empty( $woocommerce_select ) )
			update_post_meta( $post_id, '_azcl_sample_type', esc_attr( $woocommerce_select ) );

		$woocommerce_select = $_POST['_azcl_sample_code'];
		if( !empty( $woocommerce_select ) )
			update_post_meta( $post_id, '_azcl_sample_code', esc_attr( $woocommerce_select ) );

			$woocommerce_select = $_POST['_azcl_sample_id'];
		if( !empty( $woocommerce_select ) )
			update_post_meta( $post_id, '_azcl_sample_id', esc_attr( $woocommerce_select ) );

			$woocommerce_select = $_POST['_azcl_issue_date'];
		if( !empty( $woocommerce_select ) )
			update_post_meta( $post_id, '_azcl_issue_date', esc_attr( $woocommerce_select ) );
		
		$woocommerce_select = $_POST['_azcl_lab_kit'];
		if( !empty( $woocommerce_select ) )
			update_post_meta( $post_id, '_azcl_lab_kit', esc_attr( $woocommerce_select ) );
		// First Tab Fields
		$woocommerce_select = $_POST['_azcl_thc'];
		if( !empty( $woocommerce_select ) )
			{
			update_post_meta( $post_id, '_azcl_thc', esc_attr( $woocommerce_select ) );
			update_post_meta( $post_id, '_azcl_thc_tests', esc_attr($_POST['_azcl_thc_tests']) );
			update_post_meta( $post_id, '_azcl_thc_ppm', esc_attr($_POST['_azcl_thc_ppm']) );
		}

		$woocommerce_select = $_POST['_azcl_thca'];
		if( !empty( $woocommerce_select ) )
			{
			update_post_meta( $post_id, '_azcl_thca', esc_attr( $woocommerce_select ) );
			update_post_meta( $post_id, '_azcl_thca_tests', esc_attr($_POST['_azcl_thca_tests']) );
			update_post_meta( $post_id, '_azcl_thca_ppm', esc_attr($_POST['_azcl_thca_ppm']) );
		}

		$woocommerce_select = $_POST['_azcl_delta_thc'];
		if( !empty( $woocommerce_select ) )
			{
			update_post_meta( $post_id, '_azcl_delta_thc', esc_attr( $woocommerce_select ) );
			update_post_meta( $post_id, '_azcl_delta_thc_tests', esc_attr( $_POST['_azcl_delta_thc_tests'] ) );
			update_post_meta( $post_id, '_azcl_delta_thc_ppm', esc_attr( $_POST['_azcl_delta_thc_ppm'] ) );
		}

		$woocommerce_select = $_POST['_azcl_thcv'];
		if( !empty( $woocommerce_select ) )
			{
			update_post_meta( $post_id, '_azcl_thcv', esc_attr( $woocommerce_select ) );
			update_post_meta( $post_id, '_azcl_thcv_tests', esc_attr( $_POST['_azcl_thcv_tests']) );
			update_post_meta( $post_id, '_azcl_thcv_ppm', esc_attr( $_POST['_azcl_thcv_ppm'] ) );
		}

		$woocommerce_select = $_POST['_azcl_cbd'];
		if( !empty( $woocommerce_select ) )
			{
			update_post_meta( $post_id, '_azcl_cbd', esc_attr( $woocommerce_select ) );
			update_post_meta( $post_id, '_azcl_cbd_tests', esc_attr( $_POST['_azcl_cbd_tests']) );
			update_post_meta( $post_id, '_azcl_cbd_ppm', esc_attr( $_POST['_azcl_cbd_ppm'] ) );
		}

			$woocommerce_select = $_POST['_azcl_cbda'];
		if( !empty( $woocommerce_select ) )
			{
			update_post_meta( $post_id, '_azcl_cbda', esc_attr( $woocommerce_select ) );
			update_post_meta( $post_id, '_azcl_cbda_tests', esc_attr( $_POST['_azcl_cbda_tests'] ) );
			update_post_meta( $post_id, '_azcl_cbda_ppm', esc_attr( $_POST['_azcl_cbda_ppm']) );
		}
		
		$woocommerce_select = $_POST['_azcl_bisabolol'];
		if( !empty( $woocommerce_select ) )
			{
			update_post_meta( $post_id, '_azcl_bisabolol', esc_attr( $woocommerce_select ) );
			update_post_meta( $post_id, '_azcl_bisabolol_tests', esc_attr( $_POST['_azcl_bisabolol_tests'] ) );
			update_post_meta( $post_id, '_azcl_bisabolol_ppm', esc_attr( $_POST['_azcl_bisabolol_ppm']) );
		}
		$woocommerce_select = $_POST['_azcl_alpha_bisabolol'];
		if( !empty( $woocommerce_select ) )
			{
			update_post_meta( $post_id, '_azcl_alpha_bisabolol', esc_attr( $woocommerce_select ) );
			update_post_meta( $post_id, '_azcl_alpha_bisabolol_tests', esc_attr( $_POST['_azcl_alpha_bisabolol_tests'] ) );
			update_post_meta( $post_id, '_azcl_alpha_bisabolol_ppm', esc_attr( $_POST['_azcl_alpha_bisabolol_ppm']) );
		}
		$woocommerce_select = $_POST['_azcl_cbg'];
		if( !empty( $woocommerce_select ) )
			{
			update_post_meta( $post_id, '_azcl_cbg', esc_attr( $woocommerce_select ) );
			update_post_meta( $post_id, '_azcl_cbg_tests', esc_attr( $_POST['_azcl_cbg_tests'] ) );
			update_post_meta( $post_id, '_azcl_cbg_ppm', esc_attr( $_POST['_azcl_cbg_ppm']) );
		}

		$woocommerce_select = $_POST['_azcl_cbn'];
		if( !empty( $woocommerce_select ) ){
			update_post_meta( $post_id, '_azcl_cbn', esc_attr( $woocommerce_select ) );
			update_post_meta( $post_id, '_azcl_cbn_tests', esc_attr( $_POST['_azcl_cbn_tests'] ) );
			update_post_meta( $post_id, '_azcl_cbn_ppm', esc_attr( $_POST['_azcl_cbn_ppm'] ) );
		}
			

		$woocommerce_select = $_POST['_azcl_cbc'];
		if( !empty( $woocommerce_select ) ){
			update_post_meta( $post_id, '_azcl_cbc', esc_attr( $woocommerce_select ) );
			update_post_meta( $post_id, '_azcl_cbc_tests', esc_attr( $_POST['_azcl_cbc_tests'] ) );
			update_post_meta( $post_id, '_azcl_cbc_ppm', esc_attr( $_POST['_azcl_cbc_ppm']) );
		}

		// Second Tab Fields
		$compunds = json_encode($_POST['compunds_residual']);
		if( !empty( $compunds ) )
			update_post_meta( $post_id, '_azcl_compunds_residual', esc_attr( $compunds ) );
		$compunds = json_encode($_POST['residual']);
		if( !empty( $compunds ) )
			update_post_meta( $post_id, 'residual', esc_attr( $compunds ) );

		$compunds = json_encode($_POST['_azcl_residual_tests']);
		if( !empty( $compunds ) )
			update_post_meta( $post_id, 'residual_tests', esc_attr( $compunds ) );
		$compunds = json_encode($_POST['_azcl_residual_ppm']);
		if( !empty( $compunds ) )
			update_post_meta( $post_id, 'residual_ppm', esc_attr( $compunds ) );

			$woocommerce_select = $_POST['_azcl_alpha'];
		if( !empty( $woocommerce_select ) )
			{
			update_post_meta( $post_id, '_azcl_alpha', esc_attr( $woocommerce_select ) );
			update_post_meta( $post_id, '_azcl_alpha_tests', esc_attr( $_POST['_azcl_alpha_tests'] ) );
			update_post_meta( $post_id, '_azcl_alpha_ppm', esc_attr( $_POST['_azcl_alpha_ppm'] ) );
		}

		$woocommerce_select = $_POST['_azcl_camphene'];
		if( !empty( $woocommerce_select ) )
			{
			update_post_meta( $post_id, '_azcl_camphene', esc_attr( $woocommerce_select ) );
			update_post_meta( $post_id, '_azcl_camphene_tests', esc_attr( $_POST['_azcl_camphene_tests'] ) );
			update_post_meta( $post_id, '_azcl_camphene_ppm', esc_attr( $_POST['_azcl_camphene_ppm'] ) );
		}

		$woocommerce_select = $_POST['_azcl_delta_3_carene'];
		if( !empty( $woocommerce_select ) )
			{
			update_post_meta( $post_id, '_azcl_delta_3_carene', esc_attr( $woocommerce_select ) );
			update_post_meta( $post_id, '_azcl_delta_3_carene_tests', esc_attr( $_POST['_azcl_delta_3_carene_tests'] ) );
			update_post_meta( $post_id, '_azcl_delta_3_carene_ppm', esc_attr( $_POST['_azcl_delta_3_carene_ppm'] ) );
		}
		$woocommerce_select = $_POST['_azcl_beta_caryophyllene'];
		if( !empty( $woocommerce_select ) )
			{
			update_post_meta( $post_id, '_azcl_beta_caryophyllene', esc_attr( $woocommerce_select ) );
			update_post_meta( $post_id, '_azcl_beta_caryophyllene_tests', esc_attr( $_POST['_azcl_beta_caryophyllene_tests'] ) );
			update_post_meta( $post_id, '_azcl_beta_caryophyllene_ppm', esc_attr( $_POST['_azcl_beta_caryophyllene_ppm'] ) );
		}

		$woocommerce_select = $_POST['_azcl_beta_cary'];
		if( !empty( $woocommerce_select ) )
			{
			update_post_meta( $post_id, '_azcl_beta_cary', esc_attr( $woocommerce_select ) );
			update_post_meta( $post_id, '_azcl_beta_cary_tests', esc_attr( $_POST['_azcl_beta_cary_tests'] ) );
			update_post_meta( $post_id, '_azcl_beta_cary_ppm', esc_attr( $_POST['_azcl_beta_cary_ppm'] ) );
		}
			$woocommerce_select = $_POST['_azcl_caryophyllene_oxide'];
		if( !empty( $woocommerce_select ) )
			{
			update_post_meta( $post_id, '_azcl_caryophyllene_oxide', esc_attr( $woocommerce_select ) );
			update_post_meta( $post_id, '_azcl_caryophyllene_oxide_tests', esc_attr( $_POST['_azcl_caryophyllene_oxide_tests'] ) );
			update_post_meta( $post_id, '_azcl_caryophyllene_oxide_ppm', esc_attr( $_POST['_azcl_caryophyllene_oxide_ppm'] ) );
		}

		$woocommerce_select = $_POST['_azcl_p_cymene'];
		if( !empty( $woocommerce_select ) )
			{
			update_post_meta( $post_id, '_azcl_p_cymene', esc_attr( $woocommerce_select ) );
			update_post_meta( $post_id, '_azcl_p_cymene_tests', esc_attr( $_POST['_azcl_p_cymene_tests'] ) );
			update_post_meta( $post_id, '_azcl_p_cymene_ppm', esc_attr( $_POST['_azcl_p_cymene_ppm'] ) );
		}

		$woocommerce_select = $_POST['_azcl_geraniol'];
		if( !empty( $woocommerce_select ) ){
			update_post_meta( $post_id, '_azcl_geraniol', esc_attr( $woocommerce_select ) );
			update_post_meta( $post_id, '_azcl_geraniol_tests', esc_attr( $_POST['_azcl_geraniol_tests'] ) );
			update_post_meta( $post_id, '_azcl_geraniol_ppm', esc_attr( $_POST['_azcl_geraniol_ppm'] ) );
		}
		

		$woocommerce_select = $_POST['_azcl_guaiol'];
		if( !empty( $woocommerce_select ) ){
			update_post_meta( $post_id, '_azcl_guaiol', esc_attr( $woocommerce_select ) );
			update_post_meta( $post_id, '_azcl_guaiol_tests', esc_attr( $_POST['_azcl_guaiol_tests'] ) );
			update_post_meta( $post_id, '_azcl_guaiol_ppm', esc_attr( $_POST['_azcl_guaiol_ppm'] ) );
		}

		$woocommerce_select = $_POST['_azcl_alpha_humulene'];
		if( !empty( $woocommerce_select ) ){
			update_post_meta( $post_id, '_azcl_alpha_humulene', esc_attr( $woocommerce_select ) );
			update_post_meta( $post_id, '_azcl_alpha_humulene_tests', esc_attr( $_POST['_azcl_alpha_humulene_tests'] ) );
			update_post_meta( $post_id, '_azcl_alpha_humulene_ppm', esc_attr( $_POST['_azcl_alpha_humulene_ppm'] ) );
		}

		$woocommerce_select = $_POST['_azcl_isopulegol'];
		if( !empty( $woocommerce_select ) ){
			update_post_meta( $post_id, '_azcl_isopulegol', esc_attr( $woocommerce_select ) );
			update_post_meta( $post_id, '_azcl_isopulegol_tests', esc_attr( $_POST['_azcl_isopulegol_tests'] ) );
			update_post_meta( $post_id, '_azcl_isopulegol_ppm', esc_attr( $_POST['_azcl_isopulegol_ppm'] ) );
		}

		$woocommerce_select = $_POST['_azcl_delta_limonene'];
		if( !empty( $woocommerce_select ) ){
			update_post_meta( $post_id, '_azcl_delta_limonene', esc_attr( $woocommerce_select ) );
			update_post_meta( $post_id, '_azcl_delta_limonene_tests', esc_attr( $_POST['_azcl_delta_limonene_tests'] ) );
			update_post_meta( $post_id, '_azcl_delta_limonene_ppm', esc_attr( $_POST['_azcl_delta_limonene_ppm'] ) );
		}
		$woocommerce_select = $_POST['_azcl_limonene'];
		if( !empty( $woocommerce_select ) ){
			update_post_meta( $post_id, '_azcl_limonene', esc_attr( $woocommerce_select ) );
			update_post_meta( $post_id, '_azcl_limonene_tests', esc_attr( $_POST['_azcl_limonene_tests'] ) );
			update_post_meta( $post_id, '_azcl_limonene_ppm', esc_attr( $_POST['_azcl_limonene_ppm'] ) );
		}
		$woocommerce_select = $_POST['_azcl_linalool'];
		if( !empty( $woocommerce_select ) ){
			update_post_meta( $post_id, '_azcl_linalool', esc_attr( $woocommerce_select ) );
			update_post_meta( $post_id, '_azcl_linalool_tests', esc_attr( $_POST['_azcl_linalool_tests'] ) );
			update_post_meta( $post_id, '_azcl_linalool_ppm', esc_attr( $_POST['_azcl_linalool_ppm'] ) );
		}
		$woocommerce_select = $_POST['_azcl_myrcene'];
		if( !empty( $woocommerce_select ) ){
			update_post_meta( $post_id, '_azcl_myrcene', esc_attr( $woocommerce_select ) );
			update_post_meta( $post_id, '_azcl_myrcene_tests', esc_attr( $_POST['_azcl_myrcene_tests'] ) );
			update_post_meta( $post_id, '_azcl_myrcene_ppm', esc_attr( $_POST['_azcl_myrcene_ppm']) );
		}
		$woocommerce_select = $_POST['_azcl_nerolidol_1'];
		if( !empty( $woocommerce_select ) ){
			update_post_meta( $post_id, '_azcl_nerolidol_1', esc_attr( $woocommerce_select ) );
			update_post_meta( $post_id, '_azcl_nerolidol_1_tests', esc_attr( $_POST['_azcl_nerolidol_1_tests']) );
			update_post_meta( $post_id, '_azcl_nerolidol_1_ppm', esc_attr( $_POST['_azcl_nerolidol_1_ppm'] ) );
		}

		$woocommerce_select = $_POST['_azcl_nerolidol_2'];
		if( !empty( $woocommerce_select ) ){
			update_post_meta( $post_id, '_azcl_nerolidol_2', esc_attr( $woocommerce_select ) );
			update_post_meta( $post_id, '_azcl_nerolidol_2_tests', esc_attr( $_POST['_azcl_nerolidol_2_tests'] ) );
			update_post_meta( $post_id, '_azcl_nerolidol_2_ppm', esc_attr( $_POST['_azcl_nerolidol_2_ppm'] ) );
		}
		$woocommerce_select = $_POST['_azcl_ocimene'];
		if( !empty( $woocommerce_select ) ){
			update_post_meta( $post_id, '_azcl_ocimene', esc_attr( $woocommerce_select ) );
			update_post_meta( $post_id, '_azcl_ocimene_tests', esc_attr( $_POST['_azcl_ocimene_tests'] ) );
			update_post_meta( $post_id, '_azcl_ocimene_ppm', esc_attr( $_POST['_azcl_ocimene_ppm'] ) );
		}

		$woocommerce_select = $_POST['_azcl_alpha_pinene'];
		if( !empty( $woocommerce_select ) ){
			update_post_meta( $post_id, '_azcl_alpha_pinene', esc_attr( $woocommerce_select ) );
			update_post_meta( $post_id, '_azcl_alpha_pinene_tests', esc_attr( $_POST['_azcl_alpha_pinene_tests'] ) );
			update_post_meta( $post_id, '_azcl_alpha_pinene_ppm', esc_attr( $_POST['_azcl_alpha_pinene_ppm'] ) );
		}
		$woocommerce_select = $_POST['_azcl_beta_pinene'];
		if( !empty( $woocommerce_select ) ){
			update_post_meta( $post_id, '_azcl_beta_pinene', esc_attr( $woocommerce_select ) );
			update_post_meta( $post_id, '_azcl_beta_pinene_tests', esc_attr( $_POST['_azcl_beta_pinene_tests'] ) );
			update_post_meta( $post_id, '_azcl_beta_pinene_ppm', esc_attr( $_POST['_azcl_beta_pinene_ppm'] ) );
		}

		$woocommerce_select = $_POST['_azcl_alpha_terpinene'];
		if( !empty( $woocommerce_select ) )
			{
			update_post_meta( $post_id, '_azcl_alpha_terpinene', esc_attr( $woocommerce_select ) );
			update_post_meta( $post_id, '_azcl_alpha_terpinene_tests', esc_attr( $_POST['_azcl_alpha_terpinene_tests'] ) );
			update_post_meta( $post_id, '_azcl_alpha_terpinene_ppm', esc_attr($_POST['_azcl_alpha_terpinene_ppm'] ) );
		}
		$woocommerce_select = $_POST['_azcl_gamma_terpinene'];
		if( !empty( $woocommerce_select ) ){
			update_post_meta( $post_id, '_azcl_gamma_terpinene', esc_attr( $woocommerce_select ) );
			update_post_meta( $post_id, '_azcl_gamma_terpinene_tests', esc_attr( $_POST['_azcl_gamma_terpinene_tests'] ) );
			update_post_meta( $post_id, '_azcl_gamma_terpinene_ppm', esc_attr( $_POST['_azcl_gamma_terpinene_ppm'] ) );
		}
		$woocommerce_select = $_POST['_azcl_terpinolene'];
		if( !empty( $woocommerce_select ) ){
			update_post_meta( $post_id, '_azcl_terpinolene', esc_attr( $woocommerce_select ) );
			update_post_meta( $post_id, '_azcl_terpinolene_tests', esc_attr( $_POST['_azcl_terpinolene_tests'] ) );
			update_post_meta( $post_id, '_azcl_terpinolene_ppm', esc_attr( $_POST['_azcl_terpinolene_ppm'] ) );
		}
		// third tab data

		$woocommerce_select = $_POST['_azcl_methanol'];
		if( !empty( $woocommerce_select ) ){
			update_post_meta( $post_id, '_azcl_methanol', esc_attr( $woocommerce_select ) );
			update_post_meta( $post_id, '_azcl_methanol_tests', esc_attr( $_POST['_azcl_methanol_tests'] ) );
			update_post_meta( $post_id, '_azcl_methanol_ppm', esc_attr( $_POST['_azcl_methanol_ppm'] ) );
		}

		$woocommerce_select = $_POST['_azcl_pentane'];
		if( !empty( $woocommerce_select ) ){
			update_post_meta( $post_id, '_azcl_pentane', esc_attr( $woocommerce_select ) );
			update_post_meta( $post_id, '_azcl_pentane_tests', esc_attr( $_POST['_azcl_pentane_tests'] ) );
			update_post_meta( $post_id, '_azcl_pentane_ppm', esc_attr( $_POST['_azcl_pentane_ppm'] ) );
		}

		$woocommerce_select = $_POST['_azcl_ethanol'];
		if( !empty( $woocommerce_select ) ){
			update_post_meta( $post_id, '_azcl_ethanol', esc_attr( $woocommerce_select ) );
			update_post_meta( $post_id, '_azcl_ethanol_tests', esc_attr( $_POST['_azcl_ethanol_tests'] ) );
			update_post_meta( $post_id, '_azcl_ethanol_ppm', esc_attr( $_POST['_azcl_ethanol_ppm'] ) );
		}

		$woocommerce_select = $_POST['_azcl_acetone'];
		if( !empty( $woocommerce_select ) ){
			update_post_meta( $post_id, '_azcl_acetone', esc_attr( $woocommerce_select ) );
			update_post_meta( $post_id, '_azcl_acetone_tests', esc_attr( $_POST['_azcl_acetone_tests'] ) );
			update_post_meta( $post_id, '_azcl_acetone_ppm', esc_attr( $_POST['_azcl_acetone_ppm'] ) );
		}
			$woocommerce_select = $_POST['_azcl_iospropyl'];
		if( !empty( $woocommerce_select ) ){
			update_post_meta( $post_id, '_azcl_iospropyl', esc_attr( $woocommerce_select ) );
			update_post_meta( $post_id, '_azcl_iospropyl_tests', esc_attr( $_POST['_azcl_iospropyl_tests'] ) );
			update_post_meta( $post_id, '_azcl_iospropyl_ppm', esc_attr( $_POST['_azcl_iospropyl_ppm'] ) );
		}

		$woocommerce_select = $_POST['_azcl_hexane'];
		if( !empty( $woocommerce_select ) ){
			update_post_meta( $post_id, '_azcl_hexane', esc_attr( $woocommerce_select ) );
			update_post_meta( $post_id, '_azcl_hexane_tests', esc_attr( $_POST['_azcl_hexane_tests'] ) );
			update_post_meta( $post_id, '__azcl_hexane_ppm', esc_attr( $_POST['__azcl_hexane_ppm'] ) );
		}

		$woocommerce_select = $_POST['_azcl_benzene'];
		if( !empty( $woocommerce_select ) ){
			update_post_meta( $post_id, '_azcl_benzene', esc_attr( $woocommerce_select ) );
			update_post_meta( $post_id, '_azcl_benzene_tests', esc_attr( $_POST['_azcl_benzene_tests'] ) );
			update_post_meta( $post_id, '_azcl_benzene_ppm', esc_attr( $_POST['_azcl_benzene_ppm'] ) );
		}
			

		$woocommerce_select = $_POST['_azcl_toluene'];
		if( !empty( $woocommerce_select ) ){
			update_post_meta( $post_id, '_azcl_toluene', esc_attr( $woocommerce_select ) );
			update_post_meta( $post_id, '_azcl_toluene_tests', esc_attr( $_POST['_azcl_toluene_tests'] ) );
			update_post_meta( $post_id, '_azcl_toluene_ppm', esc_attr( $_POST['_azcl_toluene_ppm'] ) );
		}
	
	}
	public function product_save_data( $post_id, $post ) {

		$tab_title = stripslashes( $_POST['_wc_custom_product_tabs_lab_tab_title'] );
		$tab_content = stripslashes( $_POST['_wc_custom_product_tabs_lab_tab_content'] );
		print_r($tab_content);exit();
		if ( empty( $tab_title ) && empty( $tab_content ) && get_post_meta( $post_id, 'frs_woo_product_tabs', true ) ) {
			// clean up if the custom tabs are removed
		} elseif ( ! empty( $tab_title ) || ! empty( $tab_content ) ) {

			delete_post_meta( $post_id, 'frs_woo_product_tabs' );			$tab_data = array();

			$tab_id = '';
			if ( $tab_title ) {
				if ( strlen( $tab_title ) != strlen( utf8_encode( $tab_title ) ) ) {
					// can't have titles with utf8 characters as it breaks the tab-switching javascript
					$tab_id = "tab-custom";
				} else {
					// convert the tab title into an id string
					$tab_id = strtolower( $tab_title );
					$tab_id = preg_replace( "/[^\w\s]/", '', $tab_id );
					// remove non-alphas, numbers, underscores or whitespace
					$tab_id = preg_replace( "/_+/", ' ', $tab_id );
					// replace all underscores with single spaces
					$tab_id = preg_replace( "/\s+/", '-', $tab_id );
					// replace all multiple spaces with single dashes
					$tab_id = 'tab-' . $tab_id;
					// prepend with 'tab-' string
				}
			}

			// save the data to the database
			$tab_data[] = array( 'title' => $tab_title, 'id' => $tab_id, 'content' => $tab_content );
			update_post_meta( $post_id, 'frs_woo_product_tabs', $tab_data );
		}
	}


	private function woocommerce_wp_textarea_input( $field ) {
		global $thepostid, $post;

		if ( ! $thepostid ) $thepostid = $post->ID;
		if ( ! isset( $field['placeholder'] ) ) $field['placeholder'] = '';
		if ( ! isset( $field['class'] ) ) $field['class'] = 'short';
		if ( ! isset( $field['value'] ) ) $field['value'] = get_post_meta( $thepostid, $field['id'], true );

		echo '<p class="form-field ' . $field['id'] . '_field"><label style="display:block;" for="' . $field['id'] . '">' . $field['label'] . '</label><textarea class="' . $field['class'] . '" name="' . $field['id'] . '" id="' . $field['id'] . '" placeholder="' . $field['placeholder'] . '" rows="2" cols="20"' . (isset( $field['style'] ) ? ' style="' . $field['style'] . '"' : '') . '>' . esc_textarea( $field['value'] ) . '</textarea> ';

		if ( isset( $field['description'] ) && $field['description'] ) {
			echo '<span class="description">' . $field['description'] . '</span>';
		}

		echo '</p>';
	}


	/** Helper methods ******************************************************/

	private function product_has_custom_tabs( $product ) {
		if ( false === $this->tab_data ) {
			$this->tab_data = maybe_unserialize( get_post_meta( $product->id, 'frs_woo_product_tabs', true ) );
		}
		// tab must at least have a title to exist
		return ! empty( $this->tab_data ) && ! empty( $this->tab_data[0] ) && ! empty( $this->tab_data[0]['title'] );
	}


	/**
	 * Checks if WooCommerce is active
	 *
	 * @since  1.0
	 * @return bool true if WooCommerce is active, false otherwise
	 */
	public static function is_woocommerce_active() {

		$active_plugins = (array) get_option( 'active_plugins', array() );

		if ( is_multisite() ) {
			$active_plugins = array_merge( $active_plugins, get_site_option( 'active_sitewide_plugins', array() ) );
		}

		return in_array( 'woocommerce/woocommerce.php', $active_plugins ) || array_key_exists( 'woocommerce/woocommerce.php', $active_plugins );
	}
	
	
	
	
}?>