<?php 

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly 

// We declared New Intervals In WP Cron Job System
function appointment_status_intervals($schedules) {
	$schedules['daily'] = array(
		'interval' => 86400,
		'display' => __('Daily')
	);
	
	return $schedules;
}
add_filter( 'cron_schedules', 'appointment_status_intervals');

	// Setting up Cron Job
	// Daily Appointment status Cron
	if( !wp_next_scheduled( 'appointment_status_cron' ) ) {
	   wp_schedule_event( time(), 'daily', 'appointment_status_cron' );
	}
	

// Function to Run on Daily Basis
function appointment_status_cron() {
	
	$args = array(
		'post_type' => 'appointment',
		'posts_per_page' => -1,
		'post_status' => 'publish',
		'meta_query' => array(
			array(
				'key' => 'appointment_status',
				'value' => array ( 'Pending', 'Confirmed', 'Canceled'),
				'compare' => 'IN'
			),
		),
	);
	$post = new WP_Query( $args );
	while ( $post->have_posts() ) : $post->the_post();
		wp_update_post(
			array(
				'ID'    =>  get_the_ID(),
				'post_status'   =>  'trash'
			)
		);
	endwhile;
	
}

