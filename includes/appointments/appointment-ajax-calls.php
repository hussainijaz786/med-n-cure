<?php

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly 


/*
*
*		Call For get All Physicians ate patient side for Appointment
*
*
*/
add_action( 'wp_ajax_patient_get_physician_ajax', 'patient_get_physician_ajax');
add_action( 'wp_ajax_nopriv_patient_get_physician_ajax', 'patient_get_physician_ajax');

function patient_get_physician_ajax() {
	
	$limit = (intval($_POST['limit']) != 0 ) ? $_POST['limit'] : 15;
	$offset = (intval($_POST['offset']) != 0 ) ? $_POST['offset'] : 0;
	$current_usid = get_current_user_id();
	
	$physicians_list = json_decode(get_user_meta( $current_usid, 'patient_physician_id', true ), true);
	//print_r($physicians_list);
	//'role' => array( 'physician', 'administrator' ),
	$args = array(
		'role' => 'physician',
		'number' => $limit,
		'offset' => $offset,
		'meta_query' => array(
			'key' => 'user_parent',
			'value' => 0,
			'compare' => '!='
		),
	);
	if(isset($_POST['search']) && $_POST['search'] != ''){
			
		$args['meta_query'] = array(
			'relation' => 'OR',
			array(
				'key' => 'first_name',
				'value' => $_POST['search'],
				'compare' => 'LIKE'
			),
			array(
				'key' => 'last_name',
				'value' => $_POST['search'],
				'compare' => 'LIKE'
			),
			array(
				'key' => 'user_parent',
				'value' => 0,
				'compare' => '!='
			),
		);
	}
	
	$users = new WP_User_Query($args);
	$getphysician = $users->get_results();
	
	$response = (sizeof($getphysician) > 0)? true : null;
	
	$html = '';
	foreach ($getphysician as $key => $value) {
		$physician_id = $value->ID;
		//echo '<br> '.$physician_id;
		if(in_array($physician_id, $physicians_list)){
			//$profile = get_avatar_url($physician_id);
			$profile = get_user_meta( $physician_id, 'user_avatar', true );
			if($profile == '')
				$profile = 'http://placehold.it/50x50';
			
			$url = '';
			$appointment_system = get_user_meta( $physician_id, 'appointment_system', true );
			
			if($appointment_system == 'Calendly Appointment System'){
				
				$calendly_url = get_user_meta( $physician_id, 'calendly_url', true );
				$calendly_api_key = get_user_meta( $physician_id, 'calendly_api_key', true );
				if($calendly_url == '' || $calendly_api_key == ''){
					$appointment_system = "MednCures Appointment System";
				}
				else{
					$url = "Calendly.showPopupWidget('".$calendly_url."');return false;";
				}
				
			}
			
			$name = get_user_meta($physician_id, 'first_name', true).' '.get_user_meta($physician_id, 'last_name', true);
			if(!empty($name) && $name != ' '){
				$html .= '<div class="single-physician single-physician-select" data-physician_id="'.$physician_id.'" data-name="'.$name.'" data-appointment_system="'.$appointment_system.'" data-url="'.$url.'">';
					$html .= '<img src="'. $profile .'" alt="Physician Profile Image">
					<span class="name">'.$name.'</span>';
				$html .= '</div>';
			}
		}
	}
	
	$output = array('action' => $response, 'output' => $html);
	echo json_encode($output);
	die();
}




/*
*
*		Call For Send Appointment to Physician
*		Custom Post: appointment
*
*/
add_action( 'wp_ajax_send_appointment_ajax', 'send_appointment_ajax');
add_action( 'wp_ajax_nopriv_send_appointment_ajax', 'send_appointment_ajax');

function send_appointment_ajax() {
	
	$current_user = wp_get_current_user();
	$current_usid = get_current_user_id();
	
	$physician_id = $_POST['physician_id'];
	$first_name = ucfirst($_POST['first_name']);
	$last_name = ucfirst($_POST['last_name']);
	$email = $_POST['email'];
	$phone = $_POST['phone'];
	$date = $_POST['date'];
	$time = $_POST['time'];
	$time_perority = $_POST['time_perority'];
	
	$html = '';
	$response = true;
	
	if($date == date('m/d/Y') || $date < date('m/d/Y')){
		$response = null;
		$html = "Enter valid Date";
	}
	
	if($response){
	
		$patient_appointments = json_decode(get_user_meta($current_usid, 'appointment_post_id', true));
		
		$post_id = post_exists( $date.' '.$time );
		if($post_id){
			
			$appointment_status = get_post_meta($post_id, 'appointment_status', true);
			$appointment_physician_id = get_post_meta($post_id, 'appointment_physician_id', true);
			// Check Post already exists ar not
			// if appointment status != passed
			// if appointment is not already sent at same physician
			if($appointment_status != 'cancel' && in_array($post_id, $patient_appointments) && $appointment_physician_id == $physician_id){
				$response = null;
				$html = "Physician is not available at this time";
			}
		}
		
		if($response){
		
			$post = array(
				'post_title'    => $date.' '.$time,
				'post_content'  => '',
				'post_status'   => 'publish',
				'post_author'   => $current_usid,
				'post_type'	  => 'appointment'
			);
			
			// Insert the post into the database
			$created_post_id = wp_insert_post( $post );
			if($created_post_id){
				
				update_post_meta($created_post_id, 'first_name', $first_name);
				update_post_meta($created_post_id, 'last_name', $last_name);
				update_post_meta($created_post_id, 'email', $email);
				update_post_meta($created_post_id, 'phone', $phone);
				update_post_meta($created_post_id, 'date', $date);
				update_post_meta($created_post_id, 'time', $time);
				update_post_meta($created_post_id, 'time_perority', $time_perority);
				update_post_meta($created_post_id, 'appointment_physician_id', $physician_id);
				update_post_meta($created_post_id, 'appointment_status', 'pending');
				
				$patient_appointments[] = $created_post_id;
				update_user_meta($current_usid, 'appointment_post_id', json_encode($patient_appointments));
				
				update_user_meta($physician_id, 'appointment_notification', 1);
				
				//Email send to Physician
				$user_info = get_userdata($physician_id);
				
				$args = array(
					'call_by' => 'appointment-request-physician',
					'receiver' => $user_info->user_email, 
					'subject_name' => 'New Appointment Request from Patient', 
					'body_name' => $first_name, 
					'body_part' => '', 
					'password_reset_url' => $date.', '.$time,
				);
				cdrmed_send_email($args);
				
				$html = "<span style='width: 100%; float: left; color: #39b56a;'>Appointment sent to Physician successfully! ".$html."</span>";
				
				cdrmed_save_activity_log('Appointment sent to Physician', $physician_id);
			}
		}
	}
	
	$output = array('action' => $response, 'output' => $html);
	echo json_encode($output);
	die();
	
}



/*
*
*		Call for appointment stataus change
*		
*
*/
add_action( 'wp_ajax_change_appointment_status_ajax', 'change_appointment_status_ajax');
add_action( 'wp_ajax_nopriv_change_appointment_status_ajax', 'change_appointment_status_ajax');

function change_appointment_status_ajax() {
	
	$call_type = $_POST['call_type'];
	$post_id = $_POST['id'];
	
	$current_user = wp_get_current_user();
	$current_usid = get_current_user_id();
	$current_usid = is_dispensary_physician_child($current_usid);
	if($call_type != 'remove'){
	
		update_post_meta($post_id, 'appointment_status', $call_type);
		
		$patient_first_name = get_post_meta($post_id, 'first_name', true);
		$patient_email = get_post_meta($post_id, 'email', true);
		$date = get_post_meta($post_id, 'date', true);
		$time = get_post_meta($post_id, 'time', true);
		$physician_id = get_post_meta($post_id, 'appointment_physician_id', true);
		
		if($call_type == 'cancel'){
			
			$userrole = get_user_role();
			
			$appointment_cancel_by = 'physician';//get_user_meta($physician_id, 'first_name', true).' '.get_user_meta($physician_id, 'last_name', true);
			
			if($userrole == 'patient'){
				$appointment_cancel_by = 'patient';//$patient_name
			}
			update_post_meta($post_id, 'appointment_cancel_by', $appointment_cancel_by);
		}
		
		if($call_type == 'confirm'){
			$args = array(
				'call_by' => 'appointment-confirmation-patient',
				'receiver' => $patient_email,
				'subject_name' => $patient_first_name.', Your Appointment Has Been Confirmed!', 
				'body_name' => get_user_meta($physician_id, 'first_name', true), 
				'body_part' => '', 
				'password_reset_url' => $date.', '.$time,
			);
			cdrmed_send_email($args);
		}
		
	}
	else{
		$physician_id = array(get_post_meta($post_id, 'appointment_physician_id', true));
		
		$patient_appointments = json_decode(get_user_meta($current_usid, 'appointment_post_id', true));
		$result = array_diff($patient_appointments, $physician_id);
		update_user_meta($post_id, 'appointment_post_id', json_encode($result));
		
		wp_delete_post($post_id);
	}
	
	echo 'Appointment '.$call_type.' successfully!';
	
	die();
	
}




/*
*
*		Get all Current Login Physician Appointments
*		
*
*/	
add_action( 'wp_ajax_read_appointments_ajax', 'read_appointments_ajax');
add_action( 'wp_ajax_nopriv_read_appointments_ajax', 'read_appointments_ajax');

function read_appointments_ajax() {
	
	
	$call_type = isset($_POST['by'])? $_POST['by'] : null;
	$current_user = wp_get_current_user();
	$current_usid = get_current_user_id();
	$current_usid = is_dispensary_physician_child($current_usid);
	$userrole = get_user_role();
	
	$call = isset($_POST['call'])? $_POST['call'] : '>=';
	
	if($call_type){
		$args = array(
			'post_type' => 'appointment',
			'posts_per_page' => -1,
			'order' => 'DESC',
			'post_status' => 'publish',
			'meta_query' => array(
				'relation' => 'AND',
				array(
					'key' => 'appointment_physician_id',
					'value' => $current_usid,
					'compare' => '='
				),
				array(
					'key' => 'appointment_status',
					'value' => array ( 'pending', 'confirm', 'cancel'),
					'compare' => 'IN'
				),
				array(
					'key' => 'date',
					'value' => date('m/d/Y'),
					'compare' => $call,
				),
			),
		);
	}
	else{
		$args = array(
			'post_type' => 'appointment',
			'posts_per_page' => -1,
			'order' => 'DESC',
			'post_status' => 'publish',
			'author' => $current_usid,
			'meta_query' => array(
				'relation' => 'AND',
				array(
					'key' => 'appointment_status',
					'value' => array ( 'pending', 'confirm', 'cancel'),
					'compare' => 'IN'
				),
				array(
					'key' => 'date',
					'value' => date('m/d/Y'),
					'compare' => $call,
				),
			),
		);
	}
	
	if(isset($_POST['by_search']) && $_POST['by_search'] == 'date'){
		
		$search = explode('-', str_replace(' ','', $_POST['search']));
		$date1 = $search[0];
		$date2 = $search[1];
		
		if($date1 == $date2){
			$args['meta_query'] = array(
				'relation' => 'AND',
				array(
					'key' => 'appointment_physician_id',
					'value' => $current_usid,
					'compare' => '='
				),
				array(
					'key' => 'appointment_status',
					'value' => array ( 'pending', 'confirm', 'cancel'),
					'compare' => 'IN'
				),
				array(
					'relation' => 'OR',
					array(
						'key' => 'date',
						'value' => date('m/d/Y'),
						'compare' => '='
					),
				),
			);
		}
		else{
			$args['meta_query'] = array(
				'relation' => 'AND',
				array(
					'key' => 'appointment_physician_id',
					'value' => $current_usid,
					'compare' => '='
				),
				array(
					'key' => 'appointment_status',
					'value' => array ( 'pending', 'confirm', 'cancel'),
					'compare' => 'IN'
				),
				array(
					'key' => 'date',
					'value' => $date1,
					'compare' => '>='
				),
				array(
					'key' => 'date',
					'value' => $date2,
					'compare' => '<='
				),
			);
		}
		
	}
	elseif(isset($_POST['search']) && $_POST['search'] != ''){
		
		$args['meta_query'] = array(
			'relation' => 'AND',
			array(
				'key' => 'appointment_physician_id',
				'value' => $current_usid,
				'compare' => '='
			),
			array(
				'key' => 'appointment_status',
				'value' => array ( 'pending', 'confirm', 'cancel'),
				'compare' => 'IN'
			),
			array(
				'relation' => 'OR',
				array(
					'key' => 'first_name',
					'value' => $_POST['search'],
					'compare' => 'LIKE'
				),
				array(
					'key' => 'last_name',
					'value' => $_POST['search'],
					'compare' => 'LIKE'
				),
				array(
					'key' => 'email',
					'value' => $_POST['search'],
					'compare' => 'LIKE'
				),
			),
			array(
				'relation' => 'AND',
				array(
					'key' => 'date',
					'value' => date('m/d/Y'),
					'compare' => $call,
				),
			),
		);
	}
	
	$post = new WP_Query( $args );
	
	$response = (intval($post->post_count) > 0)? true : null;
	/* print_r($args);
	die(); */
	$html = '';
	$counter = 1;
	
	while ( $post->have_posts() ) : $post->the_post();
		
		$appointment_day = '';
		$appointment_date = get_post_meta(get_the_ID(), 'date', true);
		$name = '';
		if($call_type){
			$name = get_post_meta(get_the_ID(), 'first_name', true).' '.get_post_meta(get_the_ID(), 'last_name', true);
		}
		else{
			$physician_id = get_post_meta(get_the_ID(), 'appointment_physician_id', true);
			$name = get_user_meta($physician_id, 'first_name', true).' '.get_user_meta($physician_id, 'last_name', true);
		}
		
		
		$appointment_date = DateTime::createFromFormat('m/d/Y', $appointment_date);
		if($appointment_date == date('m/d/Y')){
			$appointment_day = 'Today';
		}
		else{
			$appointment_day = $appointment_date->format('l');
		}
		
		$status = get_post_meta(get_the_ID(), 'appointment_status', true);
		/* $head_status_class = ($status == 'pending') ? 'pending' : 'confirm';
		$head_status_class = ($status != 'pending' && $status != 'confirm') ? 'cancel' : '';
		$body_status_class = ($status == 'confirm') ? 'confirm' : '';
		$body_status_class = ($status == 'cancel' && $status != 'confirm' && $status != 'pending') ? 'cancel' : ''; */
		
		$status_html =  get_post_meta(get_the_ID(), 'appointment_cancel_by', true) ;
		
		if($status == 'cancel'){
			if($userrole == 'patient'){
				$status_html = ($status_html == 'patient') ? 'Cancel By You' : 'Cancel by '.$name;
			}
			else{
				$status_html = ($status_html == 'physician') ? 'Cancel By You' : 'Cancel by '.$name;
			}
		}
		else{
			$status_html = 'Status '.ucfirst($status);
		}
		
		
		
		
		$html .= '<div class="physician-single-appointment app'.$counter.'">
			<div class="date">'.$appointment_day.', '.$appointment_date->format('F').' '.$appointment_date->format('d').', '.$appointment_date->format('Y').'</div>
			<div class="panel panel-default appointment-collapse appointment_id'.get_the_ID().'">
				<div class="panel-heading panel-collapsed panel-opened '.$status.'">
						<span class="pull-left time">'.get_post_meta(get_the_ID(), 'time', true).'</span>
						<span class="info">'.$name.'</span>
						<span class="pull-right detail">Details <i class="fa fa-chevron-down"></i></span>
						<span class="pull-right closed">Close <i class="fa fa-remove"></i></span>
				</div>
				<div class="panel-body" style="display: none;">
					<div class="col-md-6">
						<label for="email">Email</label><br>
						<span class="email">'.get_post_meta(get_the_ID(), 'email', true).'</span>
					</div>
					<div class="col-md-6">
						<label for="phone">Phone</label><br>
						<span class="phone">'.get_post_meta(get_the_ID(), 'phone', true).'</span>
					</div>
					<div class="col-md-6">
						<span class="created">created '.get_the_date().'</span>
					</div>
					<div class="col-md-6">
						<span class="created">'.$status_html.'</span>
					</div>
						
					
					<div class="action">';
						if($call_type && $status == 'pending'){
							$html .= '<a class="appointment-action-btn" data-action="confirm" data-id="'.get_the_ID().'"><span>Confirm</span><i class="fa fa-check"></i></a>';
						}
						if($status != 'cancel'){
							$html .= '<a class="appointment-action-btn" data-action="cancel" data-id="'.get_the_ID().'"><span>Cancel</span><i class="fa fa-remove"></i></a>';
						}
						if($call_type){
							$html .= '<a class="appointment-action-btn" data-action="remove" data-id="'.get_the_ID().'"><span>Remove</span><i class="fa fa-minus-square"></i></a>';
						}
					$html .= '</div>
					
				</div>
			</div>
		</div>';
		$counter++;
		
	endwhile;
		
	$output = array('action' => $response, 'output' => $html);
	echo json_encode($output);
	die();
	
	
}





	
	


?>