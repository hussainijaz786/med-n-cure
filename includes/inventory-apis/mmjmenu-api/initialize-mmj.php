<?php 

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly 
//Dont ever remove it. Else sky will fall.
//require 'Mmjmenu.php';

function mmjmenu_init_con($atts) {
	$html = '';
	$user_vals = define_user_vars_xero();
	$user_val_arr = json_decode($user_vals, true);
	
	extract( shortcode_atts( array(
        'call_by' => '',
		'token' => '',
        'comsumer_key' => '',
        'secret_key' => '',
    ), $atts ) );
	
	if($call_by == 'c-check'){
		$api_token = $token;
		$api_cons = $comsumer_key;
		$api_secr = $secret_key;
	}
	else{
		$api_token = $user_val_arr['pos-api-token'];
		$api_cons = $user_val_arr['pos-api-consumer'];
		$api_secr = $user_val_arr['pos-api-secret'];
	}
	
	$client = new Mmjmenu($api_token);
	$menuItems      = $client->menuItems();
	$menuItems      = json_decode($menuItems, true);
	//echo $response;
	
	/*if (!empty($menuItems)){
		return 'MMJ Menu Is Connected';
		die();
	}*/
	if (!empty($menuItems)){	
		$html .= '<div style="border: 2px solid green; text-align: center; padding: 8px; color: green; font-size: 14px;">
		MMJ Menu Is CONNECTED!<br>
		<div style="max-height:200px;overflow: scroll;background:#f1f1f1;color:black !important;">';
		$html .= '<br><strong>Sample Response from Connection</strong><hr>';	
		// Now Extract all internal Arrays, from Parent Element
		$inventory = $menuItems['menu_items'];

		// Iterate through total inventory and get product value
		foreach($inventory as $item=>$value) {
			// Item here represents the Index of Array and Value represents internal elements of array
			 //echo "<br>".$value['Code'];   
			 $product_name = $value['name'];
			 $product_description = $value['body_html'];
			 $product_category = $value['category'];
			 $product_price = intval($value['price']['gram']);
			 $product_sku = $value['Id'];
			 $product_quantity = $value['amount'];
			 $product_measurement = $value['measurement'];
			 $product_created = $value['created_at'];
			 $product_genetics = $value['genetics'];
			 $product_indica = $value['indica'];
			 $product_sativa = $value['sativa'];
			 $product_thc = $value['thc_percent'];
			 $product_cbn = $value['cbn'];
			 $product_cbd = $value['cbd'];
			 $product_picture = $value['picture']['original'];
			 
			 $html .= 'ID: '.$product_sku.' Name: '.$product_name.' Price: '.$product_price.' Active Qty: '.$product_quantity.'<hr>';
		}
		$html .= '</div></div><br><a class="dispensary-api-sync-btn">Sync Now</a>';
		$check = 'true@end@';
	} else {
		
		$html .= '<div style="border: 2px solid red; text-align: center; padding: 8px; color: red;"><b>Error in Connection!<b><br>Please Make sure you API Key is correct and you are authorised to use it.</div>';	
		$check = 'false@end@';
	}
	
	if($call_by == 'c-check'){
		$html = $check.$html;
	}
	
	return $html;
	
	
}

add_shortcode('mmjmenu-api-init','mmjmenu_init_con');
add_filter('mmjmenu-api-init','mmjmenu_init_con');
?>