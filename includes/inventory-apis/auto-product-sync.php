<?php 

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly 

// We declared New Intervals In WP Cron Job System
function api_sync_interval($schedules) 
{
	//86400
	$schedules['api_daily'] = array(
		'interval' => 86400,
		'display' => __('Daily')
	);

	return $schedules;
}
add_filter( 'cron_schedules', 'api_sync_interval');

if ( ! wp_next_scheduled( 'auto_product_sync' ) ) {
	wp_schedule_event( time(), 'api_daily', 'auto_product_sync' );
}

//add_action('auto_product_sync','auto_product_sync');

add_action( 'wp_ajax_auto_product_sync_fun', 'auto_product_sync_fun');
add_action( 'wp_ajax_nopriv_auto_product_sync_fun', 'auto_product_sync_fun');

function auto_product_sync_fun() {
	
	$args = array(
		'role' => "dispensary",
		'orderby' => 'ID',
		'meta_query' => array(
			'relation' => 'AND',
			array(
				'key' => 'api_sync_now',
				'value' => 'Yes',
				'compare' => '='
			),
		),
	);
	$users = new WP_User_Query($args);
	$dispensars = $users->get_results();
	foreach ( $dispensars as $user ) {
		$dispensar_id = $user->ID;
		//get user api meta
		$pos_system = get_user_meta($dispensar_id, 'pos_system', true);
		$pos_api_token = get_user_meta($dispensar_id, 'pos_api_token', true);
		$pos_api_consumer = get_user_meta($dispensar_id, 'pos_api_consumer', true);
		$pos_api_secret = get_user_meta($dispensar_id, 'pos_api_secret', true);
		//set filter arrguments
		$atts = array('call_by' => 'c-check', 'user_id' => $dispensar_id, 'token' => $pos_api_token, 'comsumer_key' => $pos_api_consumer, 'secret_key' => $pos_api_secret);
		//call for api
		if($pos_system == "QuickBooks") {
			$data = apply_filters('quickbook-get-set-products', $atts);
		} else if($pos_system == "Xero") {
			$data = apply_filters('xero-api-get-set-products', $atts);
		} else if($pos_system == "MMJ Menu") {
			$data = apply_filters('mmj-menu-get-set-products', $atts);
		} else if($pos_system == "MJFreeway Menu") {
			$data = apply_filters('mjfreeway-get-set-products', $atts);
		} else if($pos_system == "ZOHO") {
			$data = apply_filters('zoho-get-set-products', $atts);
		}	
		$user_old_data = array();
		$user_old_data = json_decode(get_user_meta($dispensar_id, 'products_api_status'));
		$user_old_data[] = $data;
		update_user_meta( $dispensar_id, 'products_api_status', $user_old_data);
	}
}
?>