<?php 

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

function check_product_sku( $sku ) {
 $psku = wc_get_product_id_by_sku( $sku );
	if($psku) {
		$id = $psku;
	} else {
		$id = null;
	}
	return $id;
}


function show_appropriate_api_to_dispenser($userid,$requesttype) {
	// Turn on some error reporting
//error_reporting(E_ALL);
////ini_set('display_errors', 0);
	$api = '';
	if($userid) {
			$site = get_site_url();
			$current_user = get_userdata($userid);	// This is user id which we will get from action pass
			//print_r($current_user);
			$useremail = $current_user->user_email;
			$pos_system = $current_user->pos_system;
			$c_pos_system_other = $current_user->c_pos_system_other;
			$inventory_system = $current_user->inventory_system;
			$c_inventory_system_other = $current_user->c_inventory_system_other;
			$inv_api_token = $current_user->inventory_api_token;
			$inv_api_consumer = $current_user->inventory_api_consumer;
			$inv_api_secret = $current_user->inventory_api_secret;
			$pos_api_token = $current_user->pos_api_token;
			$pos_api_consumer = $current_user->pos_api_consumer;
			$pos_api_secret = $current_user->pos_api_secret;
			
	if($pos_system == "QuickBooks") {
		//Show QuickBooks API Button
		$api .= do_shortcode('[qb-api-init]');
		
	} else if($pos_system == "Xero") {
		//Show XERO API Sync Button
		$api .= do_shortcode('[xero-api-init]');
		
	} else if($pos_system == "MMJ Menu") {
		//Show MJMenu API Settings
		$api .= do_shortcode('[mmjmenu-api-init]');
		
	} else if($pos_system == "MJFreeway Menu") {
		//Show MJFreeway API Settings
		
		$api .= do_shortcode('[mjfreeway-api-init]');
		
	} else if($pos_system == "ZOHO") {
		//Show MJFreeway API Settings
		
		$api .= do_shortcode('[zoho-api-init]');
		
	} else {
	// Show Nothing Over here for time being..
	$api .= 'Hmm... No API Is Connected to System';
		
	}
			
	// User Id Check Error
	} else {
		
	// Show Nothing Over here for time being..
	$api .= 'User Connection Error, No User ID Found in Filter';
		
	}

	return $api;

}

add_filter( 'which_inventory', 'show_appropriate_api_to_dispenser', 10, 2 );
?>