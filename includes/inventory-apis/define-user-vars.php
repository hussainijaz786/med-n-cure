<?php 

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly 

function define_user_vars_xero() {
	if(! ( is_user_logged_in() )  || is_admin()){
	return;
	} 
	// Turn on some error reporting
//error_reporting(E_ALL);
////ini_set('display_errors', 0);
			$site = get_site_url();
			//$current_user = get_userdata($userid);	// This is user id which we will get from action pass
			//$test = wp_get_current_user();
			//print_r($test);
			$current_usid = get_current_user_id();
			$current_user = get_userdata($current_usid);
			$useremail = $current_user->user_email;
			$pos_system = $current_user->pos_system;
			$c_pos_system_other = $current_user->c_pos_system_other;
			$inventory_system = $current_user->inventory_system;
			$c_inventory_system_other = $current_user->c_inventory_system_other;
			$inv_api_token = $current_user->inventory_api_token;
			$inv_api_consumer = $current_user->inventory_api_consumer;
			$inv_api_secret = $current_user->inventory_api_secret;
			$pos_api_token = $current_user->pos_api_token;
			$pos_api_consumer = $current_user->pos_api_consumer;
			$pos_api_secret = $current_user->pos_api_secret;
			
			
	$user_details_print = array( 
		'user-id' =>  $current_usid, 
		'user-email' =>  $useremail,
		'pos-system' =>	 $pos_system,
		'pos-system-other' =>	 $c_pos_system_other,
		'inventory-system' =>	 $inventory_system,
		'inventory-system-other' =>	 $c_inventory_system_other,
		'pos-api-token' =>	 $pos_api_token,
		'pos-api-consumer' =>	 $pos_api_consumer,
		'pos-api-secret' =>	 $pos_api_secret,
		'inv-api-token' => $inv_api_token,
		'inv-api-consumer' => $inv_api_consumer,
		'inv-api-secret' => $inv_api_secret,
		'site-url' => $site
		);
		
	$user_details = json_encode($user_details_print);
	
	return $user_details;
	
	//echo $user_details;
					
//Check for which purpose user is using QuickBooks			

}

?>