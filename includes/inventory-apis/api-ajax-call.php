<?php 

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

add_action( 'wp_ajax_update_dispensary_api_sync_ajax', 'update_dispensary_api_sync_ajax');
add_action( 'wp_ajax_nopriv_update_dispensary_api_sync_ajax', 'update_dispensary_api_sync_ajax');

function update_dispensary_api_sync_ajax() {
	
	$current_user = wp_get_current_user();
	$current_usid = get_current_user_id();
	
	update_user_meta($current_usid, 'api_sync_now', 'Yes');
	
	$pos_system = $current_user->pos_system;
	$pos_api_token = $current_user->pos_api_token;
	$pos_api_consumer = $current_user->pos_api_consumer;
	$pos_api_secret = $current_user->pos_api_secret;
	
	$atts = array('call_by' => 'c-check', 'user_id' => $current_usid, 'token' => $pos_api_token, 'comsumer_key' => $pos_api_consumer, 'secret_key' => $pos_api_secret);
	
	if($pos_system == "QuickBooks") {
		$data = apply_filters('quickbook-get-set-products', $atts);
		
	} else if($pos_system == "Xero") {
		
		$data = apply_filters('xero-api-get-set-products', $atts);
		
	} else if($pos_system == "MMJ Menu") {
		
		$data = apply_filters('mmj-menu-get-set-products', $atts);
		
	} else if($pos_system == "MJFreeway Menu") {
		$data = apply_filters('mjfreeway-get-set-products', $atts);
	} else if($pos_system == "ZOHO") {
		$data = apply_filters('zoho-get-set-products', $atts);
	}
	
	$user_old_data = array();
	$user_old_data = json_decode(get_user_meta($current_usid, 'products_api_status'), true);
	$user_old_data[] = $data;
	update_user_meta( $current_usid, 'products_api_status', $user_old_data);
	
	$products_counter = json_decode($data, true);
	$upload_products = $products_counter['upload_products'];
	
	echo 'Synchronizing is complete. '.$upload_products.' Product(s) are sync...';
	
	cdrmed_save_activity_log('API Products Sync...', '');
	
	die();
}


add_action( 'wp_ajax_api_keys_read_ajax', 'api_keys_read_ajax');
add_action( 'wp_ajax_nopriv_api_keys_read_ajax', 'api_keys_read_ajax');

function api_keys_read_ajax() {
	$current_user = wp_get_current_user();
	$current_usid = get_current_user_id();
	
	$pos_system = $current_user->pos_system;
	$pos_api_token = $current_user->pos_api_token;
	$pos_api_consumer = $current_user->pos_api_consumer;
	$pos_api_secret = $current_user->pos_api_secret;
	
	$arr = array('pos_system' => $pos_system, 'token' => $pos_api_token, 'consumer_key' => $pos_api_consumer, 'secret_key' => $pos_api_secret);
	echo json_encode($arr);
	die();	
}

add_action( 'wp_ajax_api_keys_save_ajax', 'api_keys_save_ajax');
add_action( 'wp_ajax_nopriv_api_keys_save_ajax', 'api_keys_save_ajax');

function api_keys_save_ajax() {
	
	$current_user = wp_get_current_user();
	$current_usid = get_current_user_id();
	//get all keys
	$api = $_POST['api_action'];
	$pos_system = $_POST['pos_system'];
	$token = $_POST['token'];
	$consumer = $_POST['consumer'];
	$secret = $_POST['secret'];
	$action = $message = '';
	
	$atts = array('call_by' => 'c-check', 'token' => $token, 'comsumer_key' => $consumer, 'secret_key' => $secret);
	
	if($pos_system == 'QuickBooks'){
		// Initiate QuickBooks API Here
		
		//pass parameters to api
		$flag = apply_filters('qb-api-init', $atts);
		$flag = explode('@end@', $flag);
		$action = $flag[0];
		$message = $flag[1];
	}
	elseif($pos_system == 'Xero'){
		// Initiate Xero API Here
		
		//pass parameters to api
		$flag = apply_filters('xero-api-init', $atts);
		
		$flag = explode('@end@', $flag);
		$action = $flag[0];
		$message = $flag[1];
	}
	elseif($pos_system == 'MMJ Menu'){
		$flag = apply_filters('mmjmenu-api-init', $atts);
		$flag = explode('@end@', $flag);
		$action = $flag[0];
		$message = $flag[1];
		/* $action = true;
		$message = $flag; */
	}
	elseif($pos_system == 'MJFreeway Menu'){
		// Initiate MJFreeway API Here
		
		//pass parameters to api
		$flag = apply_filters('mjfreeway-api-init', $atts);
		
		if($flag == 'success'){
			$action = 'true';
			$message = 'Connection Successfully!';
			$message = '<div style="border: 2px solid green; text-align: center; padding: 8px; color: green;  font-size: 14px;"> MJFreeway Is Connected Successfully</div><br><a class="dispensary-api-sync-btn">Sync Now</a>';
		}
		else{
			$action = 'false';
			$message = '<div style="border: 2px solid red; text-align: center; padding: 8px; color: red;">'.$flag.'</div>';
		}
	}
	elseif($pos_system == 'ZOHO'){
		// Initiate ZOHO API Here
		$flag = json_decode(apply_filters('zoho-api-init', $atts), true);
		if($flag['action']){
			$action = 'true';
			$message = '<div style="border: 2px solid green; text-align: center; padding: 8px; color: green;  font-size: 14px;"> ZOHO is Connected Successfully</div><br><a class="dispensary-api-sync-btn">Sync Now</a>';
		}
		else{
			$action = 'false';
			$message = '<div style="border: 2px solid red; text-align: center; padding: 8px; color: red;">'.$flag["output"].'</div>';
		}
	}
	
	update_user_meta( $current_usid, 'pos_system', $pos_system );
	update_user_meta( $current_usid, 'pos_api_token', $token );
	update_user_meta( $current_usid, 'pos_api_consumer', $consumer );
	update_user_meta( $current_usid, 'pos_api_secret', $secret );
	
	
	cdrmed_save_activity_log('Dispensary update API', '');
	
	$arr = array('action' => $action, 'message' => $message);
	echo json_encode($arr);
	die();
}


?>