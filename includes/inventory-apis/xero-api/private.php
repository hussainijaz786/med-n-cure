<?php
//error_reporting(E_ALL);
////ini_set('display_errors', 0);
require 'lib/XeroOAuth.php';
$user_vals = define_user_vars_xero();
$user_val_arr = json_decode($user_vals, true);
if($call_by == 'c-check'){
	$api_cons = $comsumer_key;
	$api_secr = $secret_key;
}
else{
	$api_cons = $user_val_arr['pos-api-consumer'];
	$api_secr = $user_val_arr['pos-api-secret'];
}
//echo "<pre>";print_r($user_val_arr);echo '</pre>';

define ( 'BASE_PATH', dirname(__FILE__) );
define ( "XRO_APP_TYPE", "Private" );
define ( "OAUTH_CALLBACK", "oob" );
$useragent = "XeroOAuth-PHP Private App Test";

//XERO Live AZCAN Account API
//OWJHL2HJU9VGPLLS4OJJBGYVW1IJUX
//3Z7340MWF8VHHYTHTARTLIOMT3FM8B

$signatures = array (
		//'consumer_key' => 'OWJHL2HJU9VGPLLS4OJJBGYVW1IJUX',
		//'shared_secret' => '3Z7340MWF8VHHYTHTARTLIOMT3FM8B',
		'consumer_key' => $api_cons,
		'shared_secret' => $api_secr,
		// API versions
		'core_version' => '2.0',
		'payroll_version' => '1.0',
		'file_version' => '1.0' 
);

if (XRO_APP_TYPE == "Private" || XRO_APP_TYPE == "Partner") {
	$signatures ['rsa_private_key'] = BASE_PATH . '/certs/privatekey.pem';
	$signatures ['rsa_public_key'] = BASE_PATH . '/certs/publickey.cer';
}

$XeroOAuth = new XeroOAuth ( array_merge ( array (
		'application_type' => XRO_APP_TYPE,
		'oauth_callback' => OAUTH_CALLBACK,
		'user_agent' => $useragent 
), $signatures ) );
include 'tests/testRunner.php';

$initialCheck = $XeroOAuth->diagnostics ();
$checkErrors = count ( $initialCheck );
if ($checkErrors > 0) {
	// you could handle any config errors here, or keep on truckin if you like to live dangerously
	foreach ( $initialCheck as $check ) {
		//echo 'Error: ' . $check . PHP_EOL;
	}
} else {
	$session = persistSession ( array (
			'oauth_token' => $XeroOAuth->config ['consumer_key'],
			'oauth_token_secret' => $XeroOAuth->config ['shared_secret'],
			'oauth_session_handle' => '' 
	) );
	$oauthSession = retrieveSession ();
	
	if (isset ( $oauthSession ['oauth_token'] )) {
		$XeroOAuth->config ['access_token'] = $oauthSession ['oauth_token'];
		$XeroOAuth->config ['access_token_secret'] = $oauthSession ['oauth_token_secret'];
		
		include 'tests/tests.php';
	}
	
	testLinks ();
}
