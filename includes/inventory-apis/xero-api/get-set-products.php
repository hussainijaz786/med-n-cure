<?php
if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly 
/* 
* XERO WooCommerce API Integration by Hassan
* Dated: 05-August-2016
* Description: API to Populate products from XERO into WooCommerce or Updating Products according to updates in inventory
*/
//add_action('wp_ajax_xero_api_product','xero_api_product');
//add_action('wp_ajax_nopriv_xero_api_product','xero_api_product');

set_time_limit(0);

add_filter('xero-api-get-set-products','xero_api_product');
add_shortcode('xero-api-get-set-products','xero_api_product');

function xero_api_product( $atts) {
	
	extract( shortcode_atts( array(
        'call_by' => '',
		'user_id' => '',
        'token' => '',
        'comsumer_key' => '',
        'secret_key' => '',
    ), $atts ) );
	
	$child_user = get_user_meta($user_id, 'user_parent', true);
	if($child_user != ''){
		$user_id = get_user_meta($user_id, 'user_parent_id', true);
	}
	
	// No possiblity of anything without having Private.php
	require 'private.php';
	
	$html = '';
	
	$upload_products = 0;
	$update_products = 0;
	$read_products = 0;
	$status = '';
	
	// Get All Items from Inventory in XERO
	$response = $XeroOAuth->request('GET', $XeroOAuth->url('Items', 'core'), array());
	
	// Check if Items are returned or not?
	if ($XeroOAuth->response['code'] == 200) {
		if($call_by == 'c-check'){
			
			// If Items are returned, then show the response in XML Format
			$items = $XeroOAuth->parseResponse($XeroOAuth->response['response'], $XeroOAuth->response['format']);
			
			// Convert XML to Array format for further Use
			$array = json_decode(json_encode((array)$items), TRUE);
			// Now Extract all internal Arrays, from Parent Element
			//echo "<pre>";print_r($array);echo "</pre>";
			$inventory = $array['Items']['Item'];
			// Iterate through total inventory and get product value
			foreach($inventory as $item=>$value) {
				
				$read_products++;
				$status = 'Success';
				
				// Item here represents the Index of Array and Value represents internal elements of array
				//echo "<br>".$value['Code'];   
				$product_name = $value['Name'];
				$product_description = $value['Description'];
				$product_category = '';
				$product_price = intval($value['SalesDetails']['UnitPrice']);
				$product_sku = $value['Code'];
				$product_quantity = $value['QuantityOnHand'];
				$product_already_exists_check = check_product_sku($product_sku);
				
				// Start Building or Updating Products as they come in Code Array
				if($product_already_exists_check){
					//echo "<br>Product Already Exists and Here is Result: ".$product_already_exists_check;
					$post_id = $product_already_exists_check;
					$update_products++;
					//update_post_meta($post_id, 'product_dispensary_id', $user_id);
				} else {
					$post = array(
						'post_author' => $user_id,
						'post_content' => $product_description,
						'post_status' => "publish",
						'post_title' => $product_name,
						'post_parent' => '',
						'post_type' => "product",
					);
					$post_id = wp_insert_post( $post, $wp_error );
					$upload_products++;
					update_post_meta($post_id, 'product_dispensary_id', $user_id);
					
				}
				if($post_id){
					$attach_id = get_post_meta($product->parent_id, "_thumbnail_id", true);
					add_post_meta($post_id, '_thumbnail_id', $attach_id);
				}
			
				//wp_set_object_terms( $post_id, 'Races', 'product_cat' );
				wp_set_object_terms($post_id, 'simple', 'product_type');
				
				update_post_meta( $post_id, '_visibility', 'visible' );
				update_post_meta( $post_id, '_stock_status', 'instock');
				update_post_meta( $post_id, '_virtual', 'no');
				update_post_meta( $post_id, '_regular_price', $product_price );
				update_post_meta( $post_id, '_purchase_note', "" );
				update_post_meta( $post_id, '_featured', "no" );
				update_post_meta( $post_id, '_weight', "" );
				update_post_meta( $post_id, '_length', "" );
				update_post_meta( $post_id, '_width', "" );
				update_post_meta( $post_id, '_height', "" );
				update_post_meta($post_id, '_sku', $product_sku);
				update_post_meta( $post_id, '_product_attributes', array());
				update_post_meta( $post_id, '_price', $product_price );
				update_post_meta( $post_id, '_manage_stock', "yes" );
				update_post_meta( $post_id, '_stock', $product_quantity );
				
				//echo "<br>New Product Type Posts Created: ".$post_id;
			 
			}
			// echo 'This is extracted Code: '.$items->Items->Item[0]->Code;
			//echo "There are " . count($items->Items[0]). " items in this Xero organisation, the first one is: </br>";
			// pr($items->Items[0]->Item);
		}

	} else {
		$html = '';//$XeroOAuth->response['response'];
		$status = $XeroOAuth->response['response'];
		
		//outputError($XeroOAuth);
	}
	
	$user_old_data = array( 'api' => 'Xero', 'date' => the_date('m-d-Y'), 'time' => the_time('h-i-s'), 'total_products' => $read_products, 'upload_products' => $upload_products, 'update_products' => $update_products, 'Status' => $status);
	
	return json_encode($user_old_data);
}