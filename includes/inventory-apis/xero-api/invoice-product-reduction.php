<?php
if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly 
/* 
* XERO WooCommerce API Integration by Hassan
* Dated: 05-August-2016
* Description: API to Populate products from XERO into WooCommerce or Updating Products according to updates in inventory
*/

add_action('woocommerce_thankyou', function( $order_id ) {
	require 'private.php';
    $order = new WC_Order($order_id);
     //echo "<pre>";print_r($order); echo "</pre>";
    
	$billing_firstname = get_post_meta( $order->id, '_billing_first_name', true );
	$billing_lastname = get_post_meta( $order->id, '_billing_last_name', true );
	$billing_email = get_post_meta( $order->id, '_billing_email', true );
	$shipping_firstname = get_post_meta( $order->id, '_shipping_first_name', true );
	$shipping_lastname = get_post_meta( $order->id, '_shipping_last_name', true );
	$billing_address1 = get_post_meta( $order->id, '_billing_address_1', true );
	$billing_address2 = get_post_meta( $order->id, '_billing_address_2', true );
	$billing_city = get_post_meta( $order->id, '_billing_city', true );
	$billing_state = get_post_meta( $order->id, '_billing_state', true );
	$billing_postcode = get_post_meta( $order->id, '_billing_postcode', true );
	$billing_phone = get_post_meta( $order->id, '_billing_phone', true );
	$billing_country = get_post_meta( $order->id, '_billing_country', true );
	$billing_company = get_post_meta( $order->id, '_billing_company', true );
	$shipping_address1 = get_post_meta( $order->id, '_shipping_address_1', true );
	$shipping_address2 = get_post_meta( $order->id, '_shipping_address_2', true );
	$shipping_city = get_post_meta( $order->id, '_shipping_city', true );
	$shipping_state = get_post_meta( $order->id, '_shipping_state', true );
	$shipping_postcode = get_post_meta( $order->id, '_shipping_postcode', true );
	$shipping_country = get_post_meta( $order->id, '_shipping_country', true );
	$shipping_company = get_post_meta( $order->id, '_shipping_company', true );
	$shipping_method = $order->get_shipping_method();
	$shipping_fee = get_post_meta( $order->id, '_order_shipping', true );
	$order_total = get_post_meta($order->id,'_order_total',true);
	$payment_method = get_post_meta( $order->id, '_payment_method_title', true );
	$ord_date = $order->post->post_date;
		$items = $order->get_items();
		
	$invoice_already_created = get_post_meta($order->id, 'successful_invoice_xero', TRUE);
	
	if(!$invoice_already_created) {
	$xml = "<Invoices>
                      <Invoice>
                        <Type>ACCREC</Type>
						<Status>AUTHORISED</Status>
                        <Contact>
                          <Name>{$billing_firstname} {$billing_lastname}</Name>
						  <FirstName>{$billing_firstname}</FirstName>
						  <LastName>{$billing_lastname}</LastName>
						  <EmailAddress>{$billing_email}</EmailAddress>
						  <Addresses>  
							<Address>   
								<AddressType>STREET</AddressType> 
								<AddressLine1>{$billing_address1}</AddressLine1> 
								<AddressLine2>{$billing_address2}</AddressLine2>  
								<City>{$billing_city}</City>  
								<PostalCode>{$billing_postcode}</PostalCode>
								<Country>{$billing_country}</Country>
							  </Address>
						  </Addresses>
						   <Phones>
							  <Phone>
								<PhoneType>DEFAULT</PhoneType>
								<PhoneNumber>{$billing_phone}</PhoneNumber>
							  </Phone>
							</Phones>
							<IsCustomer>true</IsCustomer>
                        </Contact>
						<Date>{$ord_date}</Date>
                        <DueDate>{$ord_date}</DueDate>
                        <LineAmountTypes>Exclusive</LineAmountTypes>
                        <LineItems>";
						foreach ($items as $item) {
							$product_name = $item['name'];
							$product_id = $item['product_id'];
							$product_quantity = $item['qty'];
							$product_sku = get_post_meta( $product_id, '_sku', true );
							$product_price = get_post_meta( $product_id, '_price', true );
							
				$xml .="
                          <LineItem>
                            <Description>{$product_name} - Order: {$order->id}</Description>
                            <Quantity>{$product_quantity}</Quantity>
                            <UnitAmount>{$product_price}</UnitAmount>
							<ItemCode>001</ItemCode>
                            <AccountCode>200</AccountCode>
                          </LineItem>";
							
						}
				$xml .="
                        </LineItems>
                      </Invoice>
                    </Invoices>";
					
					
			$response = $XeroOAuth->request('PUT', $XeroOAuth->url('Invoices', 'core'), array(), $xml);
            if ($XeroOAuth->response['code'] == 200) {
                $invoice = $XeroOAuth->parseResponse($XeroOAuth->response['response'], $XeroOAuth->response['format']);
				$array = json_decode(json_encode((array)$invoice), TRUE);
				$invoice_number = $array['Invoices']['Invoice']['InvoiceNumber'];
				//echo "This is XERO Invoice Number: ".$invoice_number;
				if($invoice_number){
					update_post_meta($order->id, 'successful_invoice_xero', $invoice_number);
				} else {
					update_post_meta($order->id, 'error_invoice_xero', $invoice_number);
				}
				
            } else {
                outputError($XeroOAuth);
            }
	} else { // If Invoice Doesn't Exists Already
			echo "<h4>Invoice Already Created</h4>";
	}
	
});


//add_action('wp_ajax_xero_api_invoice','xero_api_invoice');
//add_action('wp_ajax_nopriv_xero_api_invoice','xero_api_invoice');
function xero_api_invoice() {
require 'private.php';

		$xml = "<Invoices>
                      <Invoice>
                        <Type>ACCREC</Type>
						<Status>AUTHORISED</Status>
                        <Contact>
                          <Name>Genius Mughal</Name>
						  <FirstName>Hassan</FirstName>
						  <LastName>Ejaz</LastName>
						  <EmailAddress>genius.hassan@gmail.com</EmailAddress>
						  <Addresses>  
							<Address>   
								<AddressType>STREET</AddressType> 
								<AddressLine1>Genius Enterprises</AddressLine1> 
								<AddressLine2>Genius Enterprises</AddressLine2>  
								<City>Gujranwala</City>  
								<PostalCode>52250</PostalCode>
								<Country>Pakistan</Country>
							  </Address>
						  </Addresses>
						   <Phones>
							  <Phone>
								<PhoneType>DEFAULT</PhoneType>
								<PhoneNumber>92336360037</PhoneNumber>
								<PhoneAreaCode>04</PhoneAreaCode>
								<PhoneCountryCode>64</PhoneCountryCode>
							  </Phone>
							</Phones>
							<IsCustomer>true</IsCustomer>
                        </Contact>
                        <Date>2016-08-12T00:00:00</Date>
                        <DueDate>2016-08-12T00:00:00</DueDate>
                        <LineAmountTypes>Exclusive</LineAmountTypes>
                        <LineItems>
                          <LineItem>
                            <Description>Purchased ITEM 001 By Hassan</Description>
                            <Quantity>1</Quantity>
                            <UnitAmount>200</UnitAmount>
							<ItemCode>001</ItemCode>
                            <AccountCode>200</AccountCode>
                          </LineItem>
                        </LineItems>
                      </Invoice>
                    </Invoices>";
						
						
            $response = $XeroOAuth->request('PUT', $XeroOAuth->url('Invoices', 'core'), array(), $xml);
            if ($XeroOAuth->response['code'] == 200) {
                $invoice = $XeroOAuth->parseResponse($XeroOAuth->response['response'], $XeroOAuth->response['format']);
                echo "" . count($invoice->Invoices[0]). " invoice created in this Xero organisation.";
                if (count($invoice->Invoices[0])>0) {
                    echo "The first one is: </br>";
                    pr($invoice->Invoices[0]->Invoice);
                }
            } else {
                outputError($XeroOAuth);
            }
}