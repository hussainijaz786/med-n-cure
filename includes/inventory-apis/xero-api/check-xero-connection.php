<?php
if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly 

function check_xero_con($atts) {
	
	extract( shortcode_atts( array(
        'call_by' => '',
		'token' => '',
        'comsumer_key' => '',
        'secret_key' => '',
    ), $atts ) );
	
	require 'private.php';
	
	$html = '';
	$response = $XeroOAuth->request('GET', $XeroOAuth->url('Items', 'core'), array());
	// Check if Items are returned or not?
	if ($XeroOAuth->response['code'] == 200) {
		$html .= '<div style="border: 2px solid green; text-align: center; padding: 8px; color: green; font-size: 14px;">
				XERO Is Connected Successfully
		<div style="max-height:200px;overflow: scroll;background:#f1f1f1;color:black !important;">';
		$html .= '<br><strong>Sample Response from Connection</strong><hr>';
		$items = $XeroOAuth->parseResponse($XeroOAuth->response['response'], $XeroOAuth->response['format']);
			// Convert XML to Array format for further Use
			$array = json_decode(json_encode((array)$items), TRUE);
			// Now Extract all internal Arrays, from Parent Element
			$inventory = $array['Items']['Item'];
			// Iterate through total inventory and get product value
			foreach($inventory as $item=>$value) {
				// Item here represents the Index of Array and Value represents internal elements of array
				//echo "<br>".$value['Code'];   
				$product_name = $value['Name'];
				if(array_key_exists('Description',$value)) {
				$product_description = $value['Description'];
				}
				$product_category = '';
				if(sizeof($value['SalesDetails']) > 0){
				$product_price = intval($value['SalesDetails']['UnitPrice']);
				} else {
				$product_price = 0;	
				}
				$product_sku = $value['Code'];
				if(array_key_exists('QuantityOnHand',$value)) {
				$product_quantity = $value['QuantityOnHand'];
				}
				$product_already_exists_check = check_product_sku($product_sku);
			$html .= 'Product Name: '.$product_name.' Product Price: '.$product_price.' Product Quantity: '.$product_quantity.'<hr>';	
			}
				
		$html .= '</div></div><br><a class="dispensary-api-sync-btn">Sync Now</a>';
		$check = 'true@end@';
		
	} else {
		$html .= '<div style="border: 2px solid red; text-align: center; padding: 8px; color: red;">API Connection Failed!<br>
		'.$XeroOAuth->response['response'].'
		</div>';
		$check = 'false@end@';
	}
	if($call_by == 'c-check'){
		$html = $check.$html;
	}
	
	return $html;
	
	
}

add_shortcode('xero-api-init','check_xero_con');
add_filter('xero-api-init','check_xero_con');
?>