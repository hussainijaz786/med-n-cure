<?php 

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

function api_model() {
	$current_user = wp_get_current_user();
	$pos_system = $current_user->pos_system;
	?>
	
<!-- Modal -->
<div class="modal fade" id="api_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="js-title-step">Setting <?php echo $pos_system; ?></h4>
      </div>
      <div class="modal-body">
		<input type="hidden" id="step-now" value="1">
		<input type="hidden" id="last_api" value="<?php echo $pos_system; ?>">
		<span class="label label-success" id="title_span">Select One API</span>
        <div class="row" data-step="1" id="s1" data-title="API">
			<div class="well">
				<div class="api_icons">
					<label><input type="radio" name="api" class="api api1" value="api1" <?php if($pos_system == 'Biotrack THC'){ echo 'checked'; } ?> data-text="Biotrack THC"><img src="<?php echo plugins_url('med-cure/includes/assets/img/biotracjthc.jpeg'); ?>" alt="Biotrack THC" id="api1"></label>
					
					<label><input type="radio" name="api" class="api api2" value="api2" <?php if($pos_system == 'Greenbits'){ echo 'checked'; } ?> data-text="Greenbits"><img src="<?php echo plugins_url('med-cure/includes/assets/img/greenbits.png'); ?>" alt="Greenbits" id="api2"></label>
					
					<label><input type="radio" name="api" class="api api3" value="api3" <?php if($pos_system == 'NCR Silver'){ echo 'checked'; } ?> data-text="NCR Silver"><img src="<?php echo plugins_url('med-cure/includes/assets/img/ncrsilver.jpg'); ?>" alt="NCR Silver" id="api3"></label>
					
					<label><input type="radio" name="api" class="api api4" value="api4" <?php if($pos_system == 'Proteus 420'){ echo 'checked'; } ?> data-text="Proteus 420"><img src="<?php echo plugins_url('med-cure/includes/assets/img/proteus420.png'); ?>" alt="Proteus 420" id="api4"></label>

					<label><input type="radio" name="api" class="api api5" value="api5" <?php if($pos_system == 'QuickBooks'){ echo 'checked'; } ?> data-text="QuickBooks"><img src="<?php echo plugins_url('med-cure/includes/assets/img/quickbooks.png'); ?>" alt="QuickBooks" id="api5"></label>
					
					<label><input type="radio" name="api" class="api api6" value="api6" <?php if($pos_system == 'Xero'){ echo 'checked'; } ?> data-text="Xero"><img src="<?php echo plugins_url('med-cure/includes/assets/img/xero.jpg'); ?>" alt="Xero" id="api6"></label>
					
					<label><input type="radio" name="api" class="api api7" value="api7" <?php if($pos_system == 'MMJ Menu'){ echo 'checked'; } ?> data-text="MMJ Menu"><img src="<?php echo plugins_url('med-cure/includes/assets/img/mmjmenu.png'); ?>" alt="MMJ Menu" id="api7"></label>
					
					<label><input type="radio" name="api" class="api api8" value="api8" <?php if($pos_system == 'MJFreeway Menu'){ echo 'checked'; } ?> data-text="MJFreeway Menu"><img src="<?php echo plugins_url('med-cure/includes/assets/img/mjfreeway.png'); ?>" alt="MJFreeway Menu" id="api8"></label>
					
					<label><input type="radio" name="api" class="api api9" value="api9" <?php if($pos_system == 'ZOHO'){ echo 'checked'; } ?> data-text="ZOHO"><img src="<?php echo plugins_url('med-cure/includes/assets/img/zoho-api.jpg'); ?>" alt="ZOHO" id="api9"></label>
				</div>
			</div>
        </div>
        <div class="row hide" data-step="2" id="s2" data-title="Keys" id="step2">
          <div class="well">
			<div class="keys">
				<input type="text" name="c_pos_api_token" id="c_pos_api_token" value="" class="regular-text new-inp c_pos_api_token" placeholder="API Token" />
				<input type="text" name="c_pos_api_consumer" id="c_pos_api_consumer" value="" class="regular-text new-inp c_pos_api_consumer" placeholder="API Consumer Key"/>
				<input type="text" name="c_pos_api_secret" id="c_pos_api_secret" value="" class="regular-text new-inp c_pos_api_secret" placeholder="API Consumer Secret"/>
				<br><span class="api-alert-message"></span>
			</div>
		  </div>
        </div>
		
		
		 <div class="row hide" data-step="3" id="s3" data-title="Connection Status!" id="step3">
			<h2 id="status" style="text-align:center;"></h2>
		 </div>
		
		
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Cancel</button>
		<a href="#" class="pull-left" id="api-help">Do you Need Help?</a>
        <button type="button" class="btn btn-warning" id="previous_btn">Go Back</button>
		<button type="button" class="btn btn-success" id="next_btn">Next</button>
      </div>
    </div>
  </div>
</div>
	
	<?php
}

//add_action( 'wp_footer', 'api_model');

   ?>