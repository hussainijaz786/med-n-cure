<?php
if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly 

add_filter('zoho-get-set-products', 'zoho_api_get_set_product');
add_shortcode('zoho-get-set-products', 'zoho_api_get_set_product');

function zoho_api_get_set_product($atts) {
	$html = '';
	extract( shortcode_atts( array(
        'call_by' => '',
		'user_id' => '',
        'token' => '',
        'comsumer_key' => '',
        'secret_key' => '',
    ), $atts ) );
	
	$organization_id = get_user_meta( $user_id, 'zoho_organization_id', true );
	$child_user = get_user_meta($user_id, 'user_parent', true);
	if($child_user != ''){
		$user_id = get_user_meta($user_id, 'user_parent_id', true);
	}
	
	// No possiblity of anything without having Private.php
	require 'private.php';
	
	$curl = curl_init();
	curl_setopt_array($curl, array(
		CURLOPT_URL => $items_url.'&organization_id='.$organization_id,
		CURLOPT_RETURNTRANSFER => true,
		CURLOPT_SSL_VERIFYPEER => false,// Turn off the server and peer verification  
		CURLOPT_ENCODING => "",
		CURLOPT_MAXREDIRS => 10,
		CURLOPT_TIMEOUT => 30,
		CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		CURLOPT_CUSTOMREQUEST => "GET",
	));
	$response = curl_exec($curl);
	$err = curl_error($curl);
	curl_close($curl);
	
	$upload_products = 0;
	$update_products = 0;
	$read_products = 0;
	$status = '';
	
	if($call_by == 'c-check'){
		
		$result = json_decode($response, true);
		
		if ($err) {
			$status = "cURL Error #:" . $err;
		}
		else if($result['message'] == 'success'){
			
			$inventory = $result['items'];
			foreach($inventory as $item => $value) {
				$read_products++;
				$status = 'Success';
				$product_price = $product_category = $product_stock = $product_quantity = $product_thc = $product_thca = $product_cbn = $product_cbd = $product_cbda = $product_cbc = $product_picture = '';
				
				$record_id = $value['item_id'];
				$product_name = $value['name'];
				$product_description = $value['description'];
				if(array_key_exists('group_name', $value)){
					$product_category = $value['group_name'];
				}
				if(array_key_exists('rate', $value)){
					$product_price = $value['rate'];
				}
				$product_sku = $value['sku'];
				$product_upc = $value['upc'];
				$product_ean = $value['ean'];
				$product_isbn = $value['isbn'];
				
				if(array_key_exists('stock_on_hand', $value)){
					$product_stock = $value['stock_on_hand'];
				}
				if(array_key_exists('unit', $value)){
					$product_quantity = $value['unit'];
				}
				if(array_key_exists('attribute_option_name1', $value)){
					if($value['attribute_option_name1'] == 'THC'){
						$product_thc = $value['attribute_option_name1'];
					}
					elseif($value['attribute_option_name1'] == 'THCA'){
						$product_thca = $value['attribute_option_name1'];
					}
					elseif($value['attribute_option_name1'] == 'CBN'){
						$product_cbn = $value['attribute_option_name1'];
					}
					elseif($value['attribute_option_name1'] == 'CBD'){
						$product_cbd = $value['attribute_option_name1'];
					}
					elseif($value['attribute_option_name1'] == 'CBDA'){
						$product_cbda = $value['attribute_option_name1'];
					}
					elseif($value['attribute_option_name1'] == 'CBC'){
						$product_cbc = $value['attribute_option_name1'];
					}
				}
				if (array_key_exists('image_name', $value)){
					$product_picture = $value['image_name'];
				}
				$product_already_exists_check = check_product_sku($product_sku);
				if(!$product_already_exists_check){}
				
				if($product_already_exists_check){
					//echo "<br>Product Already Exists and Here is Result: ".$product_already_exists_check;
					$post_id = $product_already_exists_check;
					$update_products++;
					//update_post_meta($post_id, 'product_dispensary_id', $user_id);
				} else {
					$post = array(
						'post_author' => $user_id,
						'post_content' => $product_description,
						'post_status' => "publish",
						'post_title' => $product_name,
						'post_parent' => '',
						'post_type' => "product",
					);
					$post_id = wp_insert_post( $post, $wp_error );
					$upload_products++;
					
					update_post_meta($post_id, 'product_dispensary_id', $user_id);
				}
				
				if($product_picture != ''){
					$image_url  = 'https://inventory.zoho.com/api/v1/items/'.$record_id.'/'.$product_picture;
					$upload_dir = wp_upload_dir(); // Set upload folder
					$image_data = file_get_contents($image_url); // Get image data
					$filename   = basename($image_url); // Create image file name
					// Check folder permission and define file location
					if( wp_mkdir_p( $upload_dir['path'] ) ) {
						$file = $upload_dir['path'] . '/' . $filename;
					} else {
						$file = $upload_dir['basedir'] . '/' . $filename;
					}
					// Create the image  file on the server
					file_put_contents( $file, $image_data );
					// Check image file type
					$wp_filetype = wp_check_filetype( $filename, null );
					// Set attachment data
					$attachment = array(
						'post_mime_type' => $wp_filetype['type'],
						'post_title'     => sanitize_file_name( $filename ),
						'post_content'   => '',
						'post_status'    => 'inherit'
					);
					// Create the attachment
					$attach_id = wp_insert_attachment( $attachment, $file, $post_id );
					// Include image.php
					require_once(ABSPATH . 'wp-admin/includes/image.php');
					// Define attachment metadata
					$attach_data = wp_generate_attachment_metadata( $attach_id, $file );
					// Assign metadata to attachment
					wp_update_attachment_metadata( $attach_id, $attach_data );
					// And finally assign featured image to post
					set_post_thumbnail( $post_id, $attach_id );
				}
					
				update_post_meta( $post_id, '_record_Id', $record_id );
				update_post_meta( $post_id, '_visibility', 'visible' );
				if($product_stock){
					update_post_meta( $post_id, '_stock_status', 'instock');
					update_post_meta( $post_id, '_stock', $product_stock );
				}
				else{
					update_post_meta( $post_id, '_stock_status', '');
					update_post_meta( $post_id, '_stock', 0 );
				}
					
				update_post_meta( $post_id, '_virtual', 'no');
				update_post_meta( $post_id, '_regular_price', $product_price );
				update_post_meta( $post_id, '_purchase_note', "" );
				update_post_meta( $post_id, '_featured', "no" );
				update_post_meta( $post_id, '_weight', "" );
				update_post_meta( $post_id, '_length', "" );
				update_post_meta( $post_id, '_width', "" );
				update_post_meta( $post_id, '_height', "" );
				update_post_meta($post_id, '_sku', $product_sku);
				update_post_meta( $post_id, '_product_attributes', array());
				update_post_meta( $post_id, '_price', $product_price );
				update_post_meta( $post_id, '_manage_stock', "yes" );
				update_post_meta( $post_id, '_stock', '');
				
				update_post_meta( $post_id, '_azcl_thc', $product_thc );
				update_post_meta( $post_id, '_azcl_thca', $product_thca );
				update_post_meta( $post_id, '_azcl_cbn', $product_cbn );
				update_post_meta( $post_id, '_azcl_cbd', $product_cbd );
				update_post_meta( $post_id, '_azcl_cbda', $product_cbda );
				update_post_meta( $post_id, '_azcl_cbc', $product_cbc );
				
				wp_set_object_terms($post_id, $product_category, 'product_cat');
			}
		}
		else{
			$status = $result['message'];
		}
	}
	else{
		/*$html .= '<div style="border: 2px solid green; text-align: center; padding: 8px; color: green;">
	MJ Free Way CONNECTED!<br></div>';*/
		//$result['response_details'];
	}
	
	$user_old_data = array( 'api' => 'ZOHO', 'date' => the_date('m-d-Y'), 'time' => the_time('h-i-s'), 'total_products' => $read_products, 'upload_products' => $upload_products, 'update_products' => $update_products, 'Status' => $status);
	
	return json_encode($user_old_data);
}
