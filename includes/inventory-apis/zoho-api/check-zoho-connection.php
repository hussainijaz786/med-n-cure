<?php
if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly 

function zoho_api_con($atts) {
	$html = '';
	extract( shortcode_atts( array(
        'call_by' => '',
        'token' => '',
        'comsumer_key' => '',
        'secret_key' => '',
    ), $atts ) );
	
	$current_usid = get_current_user_id();
	
	// No possiblity of anything without having Private.php
	require 'private.php';
	//organization token = f18461bcba115e8a8f4ecf9033ec2b54
	//item token = cdb0b36e7d4d67af305552d700325228
	
	$organization_id = get_user_meta( $current_usid, 'zoho_organization_id', true );
	//If Organization Id is not fetch then
	if(!$organization_id || $organization_id != $api_id){
		$curl = curl_init();
		curl_setopt_array($curl, array(
			CURLOPT_URL => $organization_url,
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_SSL_VERIFYPEER => false,// Turn off the server and peer verification  
			CURLOPT_ENCODING => "",
			CURLOPT_MAXREDIRS => 10,
			CURLOPT_TIMEOUT => 30,
			CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			CURLOPT_CUSTOMREQUEST => "GET",
		));
		$response0 = curl_exec($curl);
		$err = curl_error($curl);
		curl_close($curl);
		
		if ($err) {
			return json_encode(array("action" => null, "output" => "cURL Error #:".$err));
		}
		else {
			//just get and update organization ID
			$result0 = json_decode($response0, true);
			if($result0['message'] == 'success'){
				$organization_id = $result0['organizations'][0]['organization_id'];
				update_user_meta( $current_usid, 'zoho_organization_id', $organization_id );
			}
			else{
				return json_encode(array("action" => null, "output" => $result0['message']));
			}
		}
	}
	
	//return $organization_id;
	
	if($organization_id){
		$curl = curl_init();
		curl_setopt_array($curl, array(
			CURLOPT_URL => $items_url.'&organization_id='.$organization_id,
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_SSL_VERIFYPEER => false,// Turn off the server and peer verification  
			CURLOPT_ENCODING => "",
			CURLOPT_MAXREDIRS => 10,
			CURLOPT_TIMEOUT => 30,
			CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			CURLOPT_CUSTOMREQUEST => "GET",
		));
		$response = curl_exec($curl);
		$err = curl_error($curl);
		curl_close($curl);
		if ($err) {
			return json_encode(array("action" => null, "output" => "cURL Error #:" . $err));
		}
		else{
			$result = json_decode($response, true);
			if($result['message'] == 'success'){
				if($call_by == 'c-check'){
					return json_encode(array("action" => true, "output" => ''));
				}
				else{
					return '<div style="border: 2px solid green; text-align: center; padding: 8px; color: green; font-size: 14px;">
				ZOHO CONNECTED!<br></div><br><a class="dispensary-api-sync-btn">Sync Now</a>';
				}
			}
			else{
				return json_encode(array("action" => null, "output" => $result['message']));
			}
		}
	}
	else{
		return json_encode(array("action" => null, "output" => 'Organization ID is not defined!'));
	}
}

add_filter('zoho-api-init', 'zoho_api_con');
add_shortcode('zoho-api-init', 'zoho_api_con');