<?php
if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly 


function mjfreewaymenu_api_con($atts) {
	$html = '';
	extract( shortcode_atts( array(
        'call_by' => '',
        'token' => '',
        'comsumer_key' => '',
        'secret_key' => '',
    ), $atts ) );
	
	// No possiblity of anything without having Private.php
	require 'private.php';
	
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, $api_menu);
	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
	curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
	curl_setopt($ch, CURLOPT_FOLLOWLOCATION, false);
	curl_setopt($ch, CURLOPT_MAXREDIRS, 1);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	//curl_setopt($ch, CURLOPT_HTTPHEADER, array( 'Content-Type: application/json', 'Accept: application/json' ));
	
	curl_setopt($ch, CURLOPT_POST, true);
	curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
	
	$values = array(
		'api_id' => $api_id,
		'api_key' => $api_key,
		'location_nid' => '45',
		'version' => '7',
		'format' => 'JSON'
	);
	$params = http_build_query($values);
	curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
	curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 10);
	curl_setopt($ch, CURLOPT_TIMEOUT, 30);
	
	$result = curl_exec($ch);
	curl_close($ch);
	
	$result = json_decode($result, true);
	$response = $result['response_header']['response_code'];
	
	if($call_by == 'c-check'){
		if ($response === 'ERROR') {
			$html .= $result['response_details']['error_details'];
		}
		elseif ($response === 'OK'){
			$html .= 'success';
		}
	}
	else{
		$html .= '<div style="border: 2px solid green; text-align: center; padding: 8px; color: green; font-size: 14px;">
	MJ Free Way CONNECTED!<br></div><br><a class="dispensary-api-sync-btn">Sync Now</a>';
		//$result['response_details'];
	}
	
	/* if($call_by == 'c-check'){
		if ($response === 'ERROR') {
			$html .= $result['response_details']['error_details'];
		}
		elseif ($response === 'OK'){
			$html .= 'success';
		}
	}
	else{
		$html .= '<div style="border: 2px solid green; text-align: center; padding: 8px; color: green;">
	MJ Free Way CONNECTED!<br></div>';
		//$result['response_details'];
	} */
	return $html;

}

add_filter('mjfreeway-api-init', 'mjfreewaymenu_api_con');
add_shortcode('mjfreeway-api-init', 'mjfreewaymenu_api_con');