<?php
if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly 

add_filter('mjfreeway-get-set-products', 'mjfreewaymenu_api_product');
add_shortcode('mjfreeway-get-set-products', 'mjfreewaymenu_api_product');

function mjfreewaymenu_api_product($atts) {
	$html = '';
	extract( shortcode_atts( array(
        'call_by' => '',
		'user_id' => '',
        'token' => '',
        'comsumer_key' => '',
        'secret_key' => '',
    ), $atts ) );
	
	$child_user = get_user_meta($user_id, 'user_parent', true);
	if($child_user != ''){
		$user_id = get_user_meta($user_id, 'user_parent_id', true);
	}
	
	// No possiblity of anything without having Private.php
	require 'private.php';
	
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, $api_menu);
	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
	curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
	curl_setopt($ch, CURLOPT_FOLLOWLOCATION, false);
	curl_setopt($ch, CURLOPT_MAXREDIRS, 1);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	//curl_setopt($ch, CURLOPT_HTTPHEADER, array( 'Content-Type: application/json', 'Accept: application/json' ));
	
	curl_setopt($ch, CURLOPT_POST, true);
	curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
	
	$values = array(
		'api_id' => $api_id,
		'api_key' => $api_key,
		'location_nid' => '45',
		'version' => '7',
		'format' => 'JSON'
	);
	$params = http_build_query($values);
	curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
	curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 10);
	curl_setopt($ch, CURLOPT_TIMEOUT, 30);
	
	$result = curl_exec($ch);
	curl_close($ch);
	
	$result = json_decode($result, true);
	$response = $result['response_header']['response_code'];
	
	$upload_products = 0;
	$update_products = 0;
	$read_products = 0;
	$status = '';
	
	if($call_by == 'c-check'){
		if ($response === 'ERROR') {
			//$html .= $result['response_details']['error_details'];
			$status = $result['response_details']['error_details'];
		}
		elseif ($response === 'OK'){
			
			$inventory = $result['response_details']['menu_items'];
			foreach($inventory as $item => $value) {
				
				$read_products++;
				$status = 'Success';
				
				$product_price = '';
				
				 $record_id = $value['record_id'];
				 $product_name = $value['title'];
				 $product_description = $value['description'];
				 $product_category = $value['category'];
				 
				 if (array_key_exists('price_options', $value)){
					 if (array_key_exists('simple_price', $value['price_options'])){
						 $product_price = intval($value['price_options']['simple_price']);
					 }
					 elseif (array_key_exists('price', $value['price_options'])){
						 $product_price = intval($value['price_options']['price']);
					 }
				 }
					
				 $product_sku = $value['sku'];
				 
				  //new
				 $product_stock = $value['in_stock'];
				 $product_quantity = '';
				 if (array_key_exists('level', $value['current_level'])){
					 $product_quantity = intval($value['current_level']['level']);
				 }
				 //$product_measurement = $value['measurement'];
				 
				 $product_created = $value['last_update'];
				if (array_key_exists('testing_results', $value)){
					 $product_thc = $value['testing_results']['thc_percent'];
					 $product_thca = $value['testing_results']['thca_percent'];
					 $product_cbn = $value['testing_results']['cbn_percent'];
					 $product_cbd = $value['testing_results']['cbd_percent'];
					 $product_cbda = $value['testing_results']['cbda_percent'];
				}
				$product_picture = '';
				if (array_key_exists('product_images', $value) && count($value['product_images']) > 0){
					$product_picture = $value['product_images'][0]['full'];
				}
				$product_already_exists_check = check_product_sku($product_sku);
				 
				if($product_already_exists_check){
					//echo "<br>Product Already Exists and Here is Result: ".$product_already_exists_check;
					$post_id = $product_already_exists_check;
					$update_products++;
					//update_post_meta($post_id, 'product_dispensary_id', $user_id);
				} else {
					$post = array(
						'post_author' => $user_id,
						'post_content' => $product_description,
						'post_status' => "publish",
						'post_title' => $product_name,
						'post_parent' => '',
						'post_type' => "product",
					);
					$post_id = wp_insert_post( $post, $wp_error );
					$upload_products++;
					
					update_post_meta($post_id, 'product_dispensary_id', $user_id);
				}
				
				if($product_picture != ''){
					$image_url  = $product_picture;
					$upload_dir = wp_upload_dir(); // Set upload folder
					$image_data = file_get_contents($image_url); // Get image data
					$filename   = basename($image_url); // Create image file name
					// Check folder permission and define file location
					if( wp_mkdir_p( $upload_dir['path'] ) ) {
						$file = $upload_dir['path'] . '/' . $filename;
					} else {
						$file = $upload_dir['basedir'] . '/' . $filename;
					}
					// Create the image  file on the server
					file_put_contents( $file, $image_data );
					// Check image file type
					$wp_filetype = wp_check_filetype( $filename, null );
					// Set attachment data
					$attachment = array(
						'post_mime_type' => $wp_filetype['type'],
						'post_title'     => sanitize_file_name( $filename ),
						'post_content'   => '',
						'post_status'    => 'inherit'
					);
					// Create the attachment
					$attach_id = wp_insert_attachment( $attachment, $file, $post_id );
					// Include image.php
					require_once(ABSPATH . 'wp-admin/includes/image.php');
					// Define attachment metadata
					$attach_data = wp_generate_attachment_metadata( $attach_id, $file );
					// Assign metadata to attachment
					wp_update_attachment_metadata( $attach_id, $attach_data );
					// And finally assign featured image to post
					set_post_thumbnail( $post_id, $attach_id );
				}
				
					
				update_post_meta( $post_id, '_record_Id', $record_id );
				update_post_meta( $post_id, '_visibility', 'visible' );
				if($product_stock){
					update_post_meta( $post_id, '_stock_status', 'instock');
					update_post_meta( $post_id, '_stock', $product_quantity );
				}
				else{
					update_post_meta( $post_id, '_stock_status', '');
					update_post_meta( $post_id, '_stock', '0' );
				}
					
				update_post_meta( $post_id, '_virtual', 'no');
				update_post_meta( $post_id, '_regular_price', $product_price );
				update_post_meta( $post_id, '_purchase_note', "" );
				update_post_meta( $post_id, '_featured', "no" );
				update_post_meta( $post_id, '_weight', "" );
				update_post_meta( $post_id, '_length', "" );
				update_post_meta( $post_id, '_width', "" );
				update_post_meta( $post_id, '_height', "" );
				update_post_meta($post_id, '_sku', $product_sku);
				update_post_meta( $post_id, '_product_attributes', array());
				update_post_meta( $post_id, '_price', $product_price );
				update_post_meta( $post_id, '_manage_stock', "yes" );
				update_post_meta( $post_id, '_stock', '');
				
				update_post_meta( $post_id, '_azcl_thc', $product_thc );
				update_post_meta( $post_id, '_azcl_thca', $product_thca );
				update_post_meta( $post_id, '_azcl_cbn', $product_cbn );
				update_post_meta( $post_id, '_azcl_cbd', $product_cbd );
				update_post_meta( $post_id, '_azcl_cbda', $product_cbda );
				
				wp_set_object_terms($post_id, $product_category, 'product_cat');
				
			}
			
			//$html .= 'success';
			/* return $result['response_details']['menu_items'];
			die(); */
		}
		
	}
	else{
		/*$html .= '<div style="border: 2px solid green; text-align: center; padding: 8px; color: green;">
	MJ Free Way CONNECTED!<br></div>';*/
		//$result['response_details'];
	}
	
	//$user_old_data = array();
	//$user_old_data[] = unserialize(get_user_meta($user_id, 'products_api_status'));
	
	$user_old_data = array( 'api' => 'MJFreeway Menu', 'date' => the_date('m-d-Y'), 'time' => the_time('h-i-s'), 'total_products' => $read_products, 'upload_products' => $upload_products, 'update_products' => $update_products, 'Status' => $status);
	
	return json_encode($user_old_data);

}
