<?php
if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly 
/* 
* QuickBooks WooCommerce API Integration by Hassan
* Dated: 05-August-2016
* Description: API to Populate products from QuickBooks into WooCommerce or Updating Products according to updates in inventory
*/
//add_action('wp_ajax_quickbooks_api_product','quickbooks_api_product');
//add_action('wp_ajax_nopriv_quickbooks_api_product','quickbooks_api_product');
function quickbooks_api_product() {
	
	
	$curl = curl_init();
	
	curl_setopt_array($curl, array(
	  CURLOPT_URL => "https://sandbox-quickbooks.api.intuit.com/v3/company/123145838372272/query?query=Select%20*%20from%20Item",
	  CURLOPT_RETURNTRANSFER => true,
	  CURLOPT_ENCODING => "",
	  CURLOPT_MAXREDIRS => 10,
	  CURLOPT_TIMEOUT => 30,
	  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
	  CURLOPT_CUSTOMREQUEST => "GET",
	  CURLOPT_HTTPHEADER => array(
		"authorization: OAuth oauth_consumer_key=\"qyprdaCKSoFmU5ymPCZlvUC7peTixR\",oauth_token=\"qyprd8q7YdHSz7s13IOZo7HDOTI8Kg4q7i1Nv99wfzWvWhDA\",oauth_signature_method=\"HMAC-SHA1\",oauth_timestamp=\"1470706545\",oauth_nonce=\"cp8PET\",oauth_version=\"1.0\",oauth_signature=\"%2FuRdTtpnFElc5XuuE5o1m7bIMUs%3D\"",
		"cache-control: no-cache",
		"postman-token: 8ba2ae7c-db5e-dea0-5a65-1238a12eee04"
	  ),
	));
	
	$response = curl_exec($curl);
	$err = curl_error($curl);
	
	curl_close($curl);
	
	if ($err) {
	  echo "cURL Error #:" . $err;
	} else {
	//echo $response;
	
	//Start Conversion XML Response into PHP Array
	$items = simplexml_load_string($response, "SimpleXMLElement", LIBXML_NOCDATA);
	
	// Convert XML to Array format for further Use
	$array = json_decode(json_encode((array)$items), TRUE);
			
	// Now Extract all internal Arrays, from Parent Element
	$inventory = $array['QueryResponse']['Item'];

	// Iterate through total inventory and get product value
		foreach($inventory as $item=>$value) {
		// Item here represents the Index of Array and Value represents internal elements of array
		 //echo "<br>".$value['Code'];   
		 $product_name = $value['Name'];
		 $product_description = $value['Description'];
		 $product_category = '';
		 $product_price = intval($value['UnitPrice']);
		 $product_sku = $value['Id'];
		 $product_quantity = $value['QtyOnHand'];
		 $product_already_exists_check = check_product_sku($product_sku);
		 
		$product_dispensary_id = get_current_user_id();
		$child_user = get_user_meta($product_dispensary_id, 'user_parent', true);
		if($child_user != ''){
			$product_dispensary_id = get_user_meta($product_dispensary_id, 'user_parent_id', true);
		}
		 // Start Building or Updating Products as they come in Code Array
		 if($product_already_exists_check){
			 echo "<br>Product Already Exists and Here is Result: ".$product_already_exists_check;
			 $post_id = $product_already_exists_check;
		 } else {
		 $post = array(
				'post_author' => $user_id,
				'post_content' => $product_description,
				'post_status' => "publish",
				'post_title' => $product_name,
				'post_parent' => '',
				'post_type' => "product",
			);
			$post_id = wp_insert_post( $post, $wp_error );
			
			update_post_meta($post_id, 'product_dispensary_id', $product_dispensary_id);
			
		 }
			if($post_id){
				$attach_id = get_post_meta($product->parent_id, "_thumbnail_id", true);
				add_post_meta($post_id, '_thumbnail_id', $attach_id);
			}
			
			//wp_set_object_terms( $post_id, 'Races', 'product_cat' );
			wp_set_object_terms($post_id, 'simple', 'product_type');
			
			update_post_meta( $post_id, '_visibility', 'visible' );
			update_post_meta( $post_id, '_stock_status', 'instock');
			update_post_meta( $post_id, '_virtual', 'no');
			update_post_meta( $post_id, '_regular_price', $product_price );
			update_post_meta( $post_id, '_purchase_note', "" );
			update_post_meta( $post_id, '_featured', "no" );
			update_post_meta( $post_id, '_weight', "" );
			update_post_meta( $post_id, '_length', "" );
			update_post_meta( $post_id, '_width', "" );
			update_post_meta( $post_id, '_height', "" );
			if($product_quantity) {
			update_post_meta($post_id, '_sku', $product_sku);
			}
			update_post_meta( $post_id, '_product_attributes', array());
			update_post_meta( $post_id, '_price', $product_price );
			update_post_meta( $post_id, '_manage_stock', "yes" );
			update_post_meta( $post_id, '_stock', $product_quantity );
			
			echo "<br>New Product Type Posts Created: ".$post_id;
		 
		}
			  // echo 'This is extracted Code: '.$items->Items->Item[0]->Code;
               //echo "There are " . count($items->Items[0]). " items in this Xero organisation, the first one is: </br>";
              // pr($items->Items[0]->Item);

           }
}