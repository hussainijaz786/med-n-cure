<?php

// Turn on some error reporting
//error_reporting(E_ALL);
////ini_set('display_errors', 0);
$get_site_url="http://".$_SERVER['HTTP_HOST'];

$user_vals = define_user_vars_xero();
$user_val_arr = json_decode($user_vals, true);
$current_usid = $user_val_arr['user-id'];
$useremail = $user_val_arr['user-email'];
if($call_by == 'c-check'){
	$token = $token;
	$oauth_consumer_key = $comsumer_key;
	$oauth_consumer_secret = $secret_key;
}
else{
	$token = $user_val_arr['pos-api-token'];
	$oauth_consumer_key = $user_val_arr['pos-api-consumer'];
	$oauth_consumer_secret = $user_val_arr['pos-api-secret'];
}
$site = $user_val_arr['site-url'];
// Require the library code
require_once 'QuickBooks.php';

// Your application token (Intuit will give you this when you register an Intuit Anywhere app)
//$token = '64ac4f1db3509b4b4eb9e22b3bc9bcbb1fe2';
//$oauth_consumer_key = 'qyprdaCKSoFmU5ymPCZlvUC7peTixR';
//$oauth_consumer_secret = 'ytjaz89IBzJM4BNzRb5WmGk7Wl0EhGbMgESoCNKu';

// If you're using DEVELOPMENT TOKENS, you MUST USE SANDBOX MODE!!!  If you're in PRODUCTION, then DO NOT use sandbox.
$sandbox = true;     // When you're using development tokens
//$sandbox = false;    // When you're using production tokens

// This is the URL of your OAuth auth handler page
$quickbooks_oauth_url = $site.'/dispensar-profile/';

// This is the URL to forward the user to after they have connected to IPP/IDS via OAuth
$quickbooks_success_url = $site.'/dispensar-profile/?page-transfer=success-api-connect';

// This is the Grant URL which we will be using to send request upon
$grant_url = $site.'/dispensar-profile/?requested-page=oauth';

// This is the menu URL script 
$quickbooks_menu_url = 'http://quickbooks.v3.com:8888/quickbooks-php/docs/partner_platform/example_app_ipp_v3/menu.php';

// Check if its on Staging site or if its on localhost
if($get_site_url == 'http://beta2.cdrmed.org' || $get_site_url == 'https://beta2.cdrmed.org' || $get_site_url == 'http://cdrmed.org') {
$dsn = 'mysqli://hassan:yaraheemu786@10.209.66.14/example_app_ipp_v3';	
} else {
$dsn = 'mysqli://hassan:yaraheemu786@localhost/example_app_ipp_v3';		
}
// You should set this to an encryption key specific to your app
$encryption_key = 'cdrmedencryption';

// Do not change this unless you really know what you're doing!!!  99% of apps will not require a change to this.
$the_username = $useremail;

// The tenant that user is accessing within your own app
$the_tenant = $current_usid;

// Initialize the database tables for storing OAuth information
if (!QuickBooks_Utilities::initialized($dsn))
{
	// Initialize creates the neccessary database schema for queueing up requests and logging
	QuickBooks_Utilities::initialize($dsn);
}

// Instantiate our Intuit Anywhere auth handler 
// 
// The parameters passed to the constructor are:
//	$dsn					
//	$oauth_consumer_key		Intuit will give this to you when you create a new Intuit Anywhere application at AppCenter.Intuit.com
//	$oauth_consumer_secret	Intuit will give this to you too
//	$this_url				This is the full URL (e.g. http://path/to/this/file.php) of THIS SCRIPT
//	$that_url				After the user authenticates, they will be forwarded to this URL
// 
$IntuitAnywhere = new QuickBooks_IPP_IntuitAnywhere($dsn, $encryption_key, $oauth_consumer_key, $oauth_consumer_secret, $quickbooks_oauth_url, $quickbooks_success_url);

// Are they connected to QuickBooks right now? 
if ($IntuitAnywhere->check($the_username, $the_tenant) and 
	$IntuitAnywhere->test($the_username, $the_tenant))
{
	// Yes, they are 
	$quickbooks_is_connected = true;

	// Set up the IPP instance
	$IPP = new QuickBooks_IPP($dsn);

	// Get our OAuth credentials from the database
	$creds = $IntuitAnywhere->load($the_username, $the_tenant);

	// Tell the framework to load some data from the OAuth store
	$IPP->authMode(
		QuickBooks_IPP::AUTHMODE_OAUTH, 
		$the_username, 
		$creds);

	if ($sandbox)
	{
		// Turn on sandbox mode/URLs 
		$IPP->sandbox(true);
	}

	// Print the credentials we're using
	//print_r($creds);

	// This is our current realm
	$realm = $creds['qb_realm'];

	// Load the OAuth information from the database
	$Context = $IPP->context();

	// Get some company info
	$CompanyInfoService = new QuickBooks_IPP_Service_CompanyInfo();
	$quickbooks_CompanyInfo = $CompanyInfoService->get($Context, $realm);
}
else
{
	// No, they are not
	$quickbooks_is_connected = false;
}