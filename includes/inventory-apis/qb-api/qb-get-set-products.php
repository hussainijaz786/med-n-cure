<?php 

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly 


add_shortcode('quickbook-get-set-products','quickbooks_api_product');
add_filter('quickbook-get-set-products','quickbooks_api_product');

function quickbooks_api_product($atts) {
	//error_reporting(E_ALL);
	////ini_set('display_errors', 0);
	
	extract( shortcode_atts( array(
        'call_by' => '',
        'user_id' => '',
		'token' => '',
        'comsumer_key' => '',
        'secret_key' => '',
    ), $atts ) );
	$child_user = get_user_meta($user_id, 'user_parent', true);
	if($child_user != ''){
		$user_id = get_user_meta($user_id, 'user_parent_id', true);
	}
	require 'private.php';
	
	/* if($call_by == 'c-check'){
		if($quickbooks_is_connected == true){
			return 'success'; die();
		}
		else{
			return 'fail'; die();
		}
	} */
	
	$upload_products = 0;
	$update_products = 0;
	$read_products = 0;
	$status = '';
	
	if($quickbooks_is_connected){

		$ItemService = new QuickBooks_IPP_Service_Term();
		$items = $ItemService->query($Context, $realm, "SELECT * FROM Item WHERE Metadata.LastUpdatedTime > '2013-01-01T14:50:22-08:00' ORDER BY Metadata.LastUpdatedTime ");
		
		foreach ($items as $Item){
			$read_products++;
				$status = 'Success';
				
			$product_sku = $Item->getId();
			$product_name = $Item->getName();
			$product_description = $Item->getDescription();
			$product_price = $Item->getUnitPrice();
			$product_type = $Item->getType();
			$product_qty_active = $Item->getTrackQuantityOnHand();
			$product_purchase_cost = $Item->getPurchaseCost();
			$product_quantity = $Item->getQtyOnHand();
			
			$product_already_exists_check = check_product_sku($product_sku);
			if($product_already_exists_check){
				//echo "<br>Product Already Exists and Here is Result: ".$product_already_exists_check;
				$post_id = $product_already_exists_check;
				$update_products++;
				//update_post_meta($post_id, 'product_dispensary_id', $user_id);
			} else {
				$post = array(
					'post_author' => $user_id,
					'post_content' => $product_description,
					'post_status' => "publish",
					'post_title' => $product_name,
					'post_parent' => '',
					'post_type' => "product",
				);
				$post_id = wp_insert_post( $post, $wp_error );
				$upload_products++;
				update_post_meta($post_id, 'product_dispensary_id', $user_id);
			}
			
			//wp_set_object_terms( $post_id, 'Races', 'product_cat' );
			wp_set_object_terms($post_id, 'simple', 'product_type');
			
			update_post_meta( $post_id, '_visibility', 'visible' );
			update_post_meta( $post_id, '_stock_status', 'instock');
			update_post_meta( $post_id, '_virtual', 'no');
			update_post_meta( $post_id, '_regular_price', $product_price );
			update_post_meta( $post_id, '_purchase_note', "" );
			update_post_meta( $post_id, '_featured', "no" );
			update_post_meta( $post_id, '_weight', "" );
			update_post_meta( $post_id, '_length', "" );
			update_post_meta( $post_id, '_width', "" );
			update_post_meta( $post_id, '_height', "" );
			if($product_quantity) {
				update_post_meta($post_id, '_sku', $product_sku);
			}
			update_post_meta( $post_id, '_product_attributes', array());
			update_post_meta( $post_id, '_price', $product_price );
			update_post_meta( $post_id, '_manage_stock', "yes" );
			update_post_meta( $post_id, '_stock', $product_quantity );
			
			/* if($post_id){
				$attach_id = get_post_meta($product->parent_id, "_thumbnail_id", true);
				add_post_meta($post_id, '_thumbnail_id', $attach_id);
			} */
			//echo 'ID: '.$id.' Name: '.$name.' Description: '.$description.' Price: '.$price.' Type: '.$type.' Active Qty: '.$qty_active.' Purchase Cost: '.$purchase_cost.' Quantity: '.$qty.'<br>';
		}
	}
	else{
		$status = 'API Not Connected';
	}
	
	$user_old_data = array( 'api' => 'QuickBooks', 'date' => the_date('m-d-Y'), 'time' => the_time('h-i-s'), 'total_products' => $read_products, 'upload_products' => $upload_products, 'update_products' => $update_products, 'Status' => $status);
	
	return json_encode($user_old_data);
	
}

?>