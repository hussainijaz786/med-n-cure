<?php 

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly 

function qb_header_grant() {
	
	$site = get_site_url();
	$grant_url = $site.'/dispensar-profile/?requested-page=oauth';
	$disconnect_url = $site.'/dispensar-profile/?page-disconnect=true';
	
	$ouput = '<script type="text/javascript" src="https://appcenter.intuit.com/Content/IA/intuit.ipp.anywhere.js"></script>
		<script type="text/javascript">
		intuit.ipp.anywhere.setup({
			grantUrl: "'.$grant_url.'",
		});
		</script>';
	echo $ouput;
}

add_action('wp_head','qb_header_grant');

function check_qb_con($atts) {
	
	extract( shortcode_atts( array(
        'call_by' => '',
		'token' => '',
        'comsumer_key' => '',
        'secret_key' => '',
    ), $atts ) );
	
	require 'private.php';
	
	$site = get_site_url();
	$redirect_url = $site.'/dispensar-profile/';
	$grant_url = $site.'/dispensar-profile/?requested-page=oauth';
	$disconnect_url = $site.'/dispensar-profile/?page-disconnect=true';
	
	$html = '';
	
	if(isset($_GET['page-transfer']) && $_GET['page-transfer'] == "success-api-connect" ) {
		$html .= '<div style="text-align: center; font-family: sans-serif; font-weight: bold;">
				You\'re connected! Please wait...
			</div>
	
			<script type="text/javascript">
				window.opener.location.reload(false);
				window.setTimeout("window.close()", 2000);
			</script>';
	} else if(isset($_GET['requested-page']) || isset($_GET['oauth_token'])) {
		if($_GET['requested-page'] && $_GET['requested-page'] == "oauth" || $_GET['oauth_token']){
			if ($IntuitAnywhere->handle($the_username, $the_tenant))
			{
				; // The user has been connected, and will be redirected to $that_url automatically. 
			}
			else
			{
				// If this happens, something went wrong with the OAuth handshake
				die('Oh no, something bad happened: ' . $IntuitAnywhere->errorNumber() . ': ' . $IntuitAnywhere->errorMessage());
			}
		}
	} else if(isset($_GET['page-disconnect']) && $_GET['page-disconnect'] == 'true') {
	if ($IntuitAnywhere->disconnect($the_username, $the_tenant)){
		// Nothing Needed Here, Page will be disconnected
		wp_redirect($redirect_url);exit;
	}	
	}	else {
		
		if($quickbooks_is_connected == true){
			
			$html .= '<div style="border: 2px solid green; text-align: center; padding: 8px; color: green; font-size: 14px;">
				CONNECTED!<br>
				<br>
				<i>
					Realm: '.$realm.'
					Company: '.$quickbooks_CompanyInfo->getCompanyName().'
					Email: '.$quickbooks_CompanyInfo->getEmail()->getAddress().'
					Country: '.$quickbooks_CompanyInfo->getCountry().'
				</i>
				<br>
				<div style="max-height:200px;overflow: scroll;background:#f1f1f1;color:black !important;">';
				$html .= '<br><strong>Sample Response from Connection</strong><hr>';
				// Sample Response Print
				$ItemService = new QuickBooks_IPP_Service_Term();
				$items = $ItemService->query($Context, $realm, "SELECT * FROM Item WHERE Metadata.LastUpdatedTime > '2013-01-01T14:50:22-08:00' ORDER BY Metadata.LastUpdatedTime ");
				foreach ($items as $Item){
						$id = $Item->getId();
						$name = $Item->getName();
						$description = $Item->getDescription();
						$price = $Item->getUnitPrice();
						$type = $Item->getType();
						$qty_active = $Item->getTrackQuantityOnHand();
						$purchase_cost = $Item->getPurchaseCost();
						$qty = $Item->getQtyOnHand();
						
						$html .= 'ID: '.$id.' Name: '.$name.' Description: '.$description.' Price: '.$price.' Type: '.$type.' Active Qty: '.$qty_active.' Purchase Cost: '.$purchase_cost.' Quantity: '.$qty.'<hr>';
					}
				$html .= '</div>
				<a href="'.$disconnect_url.'">Disconnect</a>
			</div><br> <a class="dispensary-api-sync-btn">Sync Now</a>';
			
			$check = 'true@end@';
			
		} else {
			//Connect Button for QuickBooks To Be Displayed
			$html .= '<div style="border: 2px solid red; text-align: center; padding: 8px; color: red;">
				<b>NOT</b> CONNECTED!<br>
				<br>
				<ipp:connectToIntuit></ipp:connectToIntuit>
				<br>
				<br>
				You must authenticate to QuickBooks <b>once</b> before you can exchange data with it. <br>
				<br>
				<strong>You only have to do this once!</strong> <br><br>
				
				After youve authenticated once, you never have to go 
				through this connection process again. <br>
				Click the button above to 
				authenticate and connect.

			</div>';
			
			$check = 'false@end@';
		}
	}
	if($call_by == 'c-check'){
		$html = $check.$html;
	}
	return $html;
}

add_shortcode('qb-api-init','check_qb_con');
add_filter('qb-api-init','check_qb_con');
?>