<?php 

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly 
// Include All Function Files from Different Inventories
include('api-popup.php');
include('mjfreeway-api/index-inc.php');
include('xero-api/index-inc.php');
include('qb-api/index-inc.php');
include('mmjmenu-api/index-inc.php');
include('zoho-api/index-inc.php');
include('api-filters.php');
include('define-user-vars.php');
include('api-ajax-call.php');
include('auto-product-sync.php');
?>