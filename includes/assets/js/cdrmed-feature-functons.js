var $ = jQuery;


/*
==================================================================================
		General
==================================================================================
*/


function monthSorter(a, b) {
    if (a.month < b.month) return -1;
    if (a.month > b.month) return 1;
    return 0;
}
function docSorter(a, b) {
    if (a.doc < b.doc) return -1;
    if (a.doc > b.doc) return 1;
    return 0;
}
function starsSorter(a, b) {
    if (a.id < b.id) return 1;
    if (a.id > b.id) return -1;
    return 0;
}

function reset_modal_feedback(feedback_class, page_reload){
	jQuery(".modal-box-feedback").addClass(feedback_class);
	setTimeout( function() {
		jQuery(".modal-box-feedback").html('');
		jQuery(".modal-box-feedback").removeClass(feedback_class);
		if(page_reload){
			window.location.reload();
		}
	}, 2000);
}

function reset_feedback(feedback_class, page_reload){
	jQuery(".default-feedback").addClass(feedback_class);
	setTimeout( function() {
		jQuery(".default-feedback").html('');
		jQuery(".default-feedback").removeClass(feedback_class);
		if(page_reload){
			window.location.reload();
		}
	}, 2000);
}

jQuery(document).ready(function(){
	
	setTimeout(function() {
		$('div#message').fadeOut('fast');
	}, 1500);
	setTimeout(function() {
		$('.alert-success').fadeOut('fast');
	}, 5000);
	setTimeout(function() {
		$('div.feedbackrecord').fadeOut('fast');
	}, 60000);
	
	var WinWidth=jQuery(this).width();
	///console.log('window width is :' + WinWidth);
	if(WinWidth > 767){
		///do some thing
	}
	
	if(cdrmedajax.character1){
		// Js for Registration Counts on Physcian Dashboard
		var hv = $('#regicounts').val();
		//var hv = 0;
		if( hv > 0 ) {
			jQuery('#pending-counts').show();
			jQuery('#pending-counts').html(hv);
		}
	}
	
	
	(function($) {
		var url = document.location.toString();
		if (url.match('#')) {
			var url_to_go = url.split('#')[1];
			$('.nav a[href="#'+url_to_go+'"]').tab('show') ;
			window.scrollTo(0, -200);
			
			if (url.match('page_type')){
				console.log(url.split('#')[1])
				if(url.split('#')[1] != 'tabs-1' || url.split('#')[1] != 'tabs-2' || url.split('#')[1] != 'tabs-3'){
					var without_page_type = url.split('?page_type=')[0];
					document.location.href = without_page_type+'#'+url.split('#')[1];
					return false;
				}
				else{
					window.location.reload();
				}
			}
			
		}
		
		
		
		// Change hash for page-reload
		$('.nav a').on('shown.bs.tab', function (e) {
			window.location.hash = e.target.hash;
		});
		var shiftWindow = function() {
			scrollBy(0, -200)
		};
		if (location.hash) shiftWindow();
		window.addEventListener("hashchange", shiftWindow);
	})( jQuery );
	
	
	jQuery('.close-palert-bar').click(function(){
		$(".password-reset-alert-bar").hide();
	});
	jQuery('.close-alert-bar').click(function(){
		$(".client-alert-content-bar").hide();
	});
	jQuery('#close-alert-bar2').click(function(){
		$(".red-alert-box").hide();
	});
	
	jQuery('.main-nav-tabs li a').click(function(){
		var find_var = "?page_type=settings";
		var url = window.location.href;
		
		if((url.indexOf(find_var)) > 0){
			var new_link = jQuery(this).attr('href');
			document.location.href = url.replace('?page_type=settings', '') + new_link;
		}
	});
	
	
	jQuery('.cdmed_site_selectors').each(function(){
		if(jQuery(this).is(":checked")) {
			jQuery(this).closest('label').addClass('active');
		}
	});
	
	
	jQuery('#update_my_health').click(function(event) {
		jQuery('a[href="#myhealth"]').click();
	});
	
	
	
	jQuery('#remind').click(function(){ 
		// This does the ajax request
		jQuery('.message').hide();
		
		var data = {
			action: 'set_my_cookie',
		};
		jQuery.post(wc_add_to_cart_params.ajax_url, data, function(response) {
		});
	});
	
	$('.selectpicker').selectpicker({
		showIcon: true,
		size: 4
	});
	
	jQuery('[data-toggle="tooltip"]').tooltip(); 
	
	
	jQuery('.right-dob').hover(function(){
	jQuery('span.dob-tip').fadeIn();
	},function(){
		jQuery('span.dob-tip').fadeOut();
	}
	);

	jQuery('.inducer').hover(function(){
	jQuery('span.induc').fadeIn();
	},function(){
		jQuery('span.induc').fadeOut();
	}
	);

	jQuery('.substrates').hover(function(){
	jQuery('span.subs').fadeIn();
	},function(){
		jQuery('span.subs').fadeOut();
	}
	);

	jQuery('.prodrug').hover(function(){
	jQuery('span.prodr').fadeIn();
	},function(){
		jQuery('span.prodr').fadeOut();
	}
	);
	
	
	//Patient Feedback Today Cannabis?
	
	$('input[name="cannabis"]').click(function(){
        if($(this).attr("value")=="yes"){
            $(".today_cannabis_rows").show();
        }else{
			$(".today_cannabis_rows").hide();
		}
    });
	
	
	var wrapper_today         = jQuery(".input_fields_wrap_today_ingested"); //Fields wrapper
    var add_button_today      = jQuery("#add_field_button_today_ingested"); //Add button ID
	jQuery(add_button_today).click(function(e){ //on add input button click
        e.preventDefault();
		jQuery(wrapper_today).before('<tr><td><input type="text" class="form-control frst" name="_today_cannabis_amount[]"/></td><td><span class="scnd"><select id="frequency_of_ingested" name="_today_cannabis_frequency[]" class="form-control"><option>1</option><option>2</option><option>3</option><option>4</option><option>5</option><option>6</option><option>7</option><option>8</option><option>9</option><option>10</option></select></span></td><td><select id="method_of_ingested" name="_today_cannabis_ingested[]" class="form-control"><option>Inhaled</option><option>Ingested</option><option>Sublingual</option><option>Suppository</option><option>Topically</option></select></td><td><a href="javascript:void(0);" class="remove_field_today_cannabis"><i class="fa fa-remove"></i></a></tr>'); //add input box
		return false;
    });
	jQuery(document).on("click",".remove_field_today_cannabis", function (e) {
        e.preventDefault(); 
         $(this).parent().parent().remove();
    });
	
	//var deathdate = $('input[name="deathdate"]').val();
	//var dtdate;  // Added for a quick fix.
	if(dtdate){
		var deathdate = dtdate;
		console.log(deathdate);
	}
	if(deathdate){
		if(!deathdate.match(/\S/)) {
			// Don't Show Field If Value is Empty
		} else {
			//alert('VALUE EXISTS');
			//Show Death Date Field If Value is Already In There
			$('input[name="deathdate"]').val(deathdate);
			$(".patient-death-date, #update-death").show();
		}
	}
	
	
	jQuery(document).on("click",".pre-regis-delete", function (e) {
		if (!confirm('Are you sure you want to delete this Pre-Registeration?')) e.preventDefault();
	});
	
	/* var elems = document.getElementsByClassName('pre-regis-delete');
	var confirmIt = function (e) {
		if (!confirm('Are you sure?')) e.preventDefault();
	};
	for (var i = 0, l = elems.length; i < l; i++) {
		elems[i].addEventListener('click', confirmIt, false);
	} */
	
});


/*
==================================================================================
		Dashboard
==================================================================================
*/

jQuery(document).ready(function(){
	
	/*
	*
	*	Patient Dashboard Face Trackers
	*
	*/
	jQuery('.radio-item').live('click', function(e) { 
		var nword = '';
		$word = jQuery(this).find('input[type=radio]').val();
		if($word == 'v-good'){
			nword = 'Very Well';
		}else if($word == 'good'){
			nword = 'Well';
		}else if($word == 'neutral'){
			nword = 'Neutral';
		}
		else if($word == 'bad'){ 
			nword = 'Unwell';
		}else if($word == 'v-bad'){
			nword = 'Very Unwell';
		}
		jQuery('#txt-change').html(nword);
		jQuery('#mcomments').show();
		jQuery('#cancel').show();
		jQuery("#comments").focus();
	});
	/*
	*
	*	Patient Dashboard Face Tracker Cancel
	*
	*/
	jQuery("#cancel").click(function(){
		jQuery("#comments").val('');
		jQuery('#mcomments').hide(); 
		$('.patient_feedback').prop('checked', false);
		jQuery('#cancel').hide();
	});
});


$(document).on("click", '.symptoms-all-trackers-view .panel-heading', function(event) {
	if ($(this).hasClass('panel-collapsed')) {
		// expand the panel
		$(this).parents('.panel').find('.panel-body').slideDown();
		$(this).removeClass('panel-collapsed');
		$(this).addClass('panel-closed');
		$(this).removeClass('panel-opened');
		//$(this).find('i').removeClass('fa-chevron-down').addClass('fa-remove');
		$('.symptoms-collapse').addClass('body-border');
	}
	else {
		// collapse the panel
		$(this).parents('.panel').find('.panel-body').slideUp();
		//$('.physician-single-appointment .panel-heading').removeClass('panel-collapsed');
		$(this).addClass('panel-collapsed');
		$(this).addClass('panel-opened');
		$(this).removeClass('panel-closed');
		//$(this).find('i').removeClass('fa-remove').addClass('fa-chevron-down');
		$('.symptoms-collapse').removeClass('body-border');
	}
});



/*
==================================================================================
		API
==================================================================================
*/



jQuery(document).ready(function(){
	
	
	
	
	
	
});





/*
==================================================================================
		Appoitments
==================================================================================
*/
// js for Appointment date search

$('body').on('focus',".physician-appointment-date-search", function(){
   
	$(this).daterangepicker({
		 "locale": {
			"format": "MM/DD/YYYY",
			"separator": " - ",
			"applyLabel": "Apply",
			"cancelLabel": "Cancel",
			"fromLabel": "From",
			"toLabel": "To",
			"customRangeLabel": "Custom",
			"daysOfWeek": [
				"Su",
				"Mo",
				"Tu",
				"We",
				"Th",
				"Fr",
				"Sa"
			],
			"monthNames": [
				"January",
				"February",
				"March",
				"April",
				"May",
				"June",
				"July",
				"August",
				"September",
				"October",
				"November",
				"December"
			],
			"firstDay": 1
		}
	});
});

$(document).ready(function() {
	
});



/*
=======================================================================
		User Profile
=======================================================================
*/


//Show Password in Modal Box
$(document).on("click", '.m-show-password', function(event) {
	if($(this).is(':checked')) {
		$('input[data-pass="m-password-input"]').addClass('form-control');
		$('input[data-pass="m-password-input"]').addClass('show-password-field');
		$('input[data-pass="m-password-input"]').attr('type','text');
	}
	else{
		$('input[data-pass="m-password-input"]').attr('type','password');
	}
});
// Show Password in Profile
$(document).on("click", '.show-password', function(event) {
	if($(this).is(':checked')) {
		$('input[data-pass="password-input"]').addClass('form-control');
		$('input[data-pass="password-input"]').addClass('show-password-field');
		$('input[data-pass="password-input"]').attr('type','text');
	}
	else{
		$('input[data-pass="password-input"]').attr('type','password');
	}
});






jQuery(document).ready(function(){
	
	// js for admin and physician
	
	//if(cdrmedajax.group0 || cdrmedajax.group1 ){
		
		//JS for Physician Side Appointment System Selection
		$('.cdrmed-appointment-group').hide();
		$('#appointment_system').change(function(e) {
			var appointment_system = $('#appointment_system option:selected').text();
			if(appointment_system == 'Calendly Appointment System'){
				$('.calendly-appointment-group').show();
				$('.cdrmed-appointment-group').hide();
			}
			else{
				$('.calendly-appointment-group').hide();
				$('.cdrmed-appointment-group').show();
			}
		});
	//}
	
	$("#featured_image").on('change',function(event){
		if (this.files && this.files[0]) {
            var reader = new FileReader();
            reader.onload = function (e) {
                $('#featured_img').attr('src', e.target.result);
                $('#featured_image_src').val(e.target.result);
                $('#featured_image_change').val('changed');
            }
            reader.readAsDataURL(this.files[0]);
        }
    });
	
	
	if(jQuery(".user-avatar").attr('src') != '' && isNaN(jQuery(".user-avatar").attr('src'))){
		$(".profile_img_file_span").hide();
		$(".profile_img_file_label").show();
	}
	else{
		jQuery(".user-avatar").hide();
		$(".profile-img-crop").show();
		$(".profile_img_file_span").show();
		$(".profile-img-crop").hide();
		jQuery('.change-profile-btn').addClass('change-profile-btn-display');
		jQuery('.remove-profile-btn').addClass('change-profile-btn-display');
	}
	
	jQuery('.change-profile-btn').click(function(){
		$(".profile_img_file_label").show();
		jQuery('.change-profile-btn').addClass('change-profile-btn-display');
	});
	
	jQuery('#upassword').keyup(function(){
		var upassword = jQuery('#upassword').val();
		var password_health = checkStrength('#upassword', '#password-feedback', upassword);
		jQuery('#upassword_health').val(password_health);
		jQuery('#password-feedback').html(password_health);
		
	});
	
	jQuery('#ucpassword').keyup(function(){
		recheckStrength('#upassword', '#ucpassword', '#cpassword-feedback');
	});
	
	// ADD New Patient Child user Password verification
	jQuery('#nu_password').keyup(function(){
		var upassword = jQuery('#nu_password').val();
		var password_health = checkStrength('#nu_password', '#nu_password_feedback', upassword);
		jQuery('#nu_password_health').val(password_health);
		jQuery('#nu_password_feedback').html(password_health);
	});
	
	jQuery('#nu_c_password').keyup(function(){
		recheckStrength('#nu_password', '#nu_c_password', '#nu_c_password_feedback');
	});
	
	
	
	
	// Stopped the Default Behvaior of Button from Submitting Form
	$('#user-profile-customs-btn').click(function (e) {
		// custom handling here
		e.preventDefault();
	});
	
	jQuery('#clear-profile-img-btn').click(function(){
		//jQuery('#imgCropped').attr('ng-src','');
		if(jQuery("#user-avatar").attr('src') != ''){
			jQuery('#change-profile-btn').removeClass('change-profile-btn-display');
		}
		if(jQuery("#user-avatar").attr('src') == ''){
			$("#profile-img-crop").hide();
		}
		$("#profile_img_file_label").show();
		jQuery('#profile_img_change_flag').val('');
		$("#profile_img_file").val('');
		jQuery("#cropArea").hide();
		var src = jQuery("#user-avatar").attr('src');
		if(src != ''){
			jQuery("#user-avatar").show();
			//jQuery('#imgCropped').attr('ng-src',src);
			//jQuery('#imgCropped').attr('src',src);
		}
		jQuery("#clear-profile-img-btn").hide();
		return false;
	});
	
	
	$('#inventory_system').change(
		function() {
			var inventory_system = $('#inventory_system option:selected').text();
			if(inventory_system == 'Other (type in field)'){
				$('#c_inventory_system_other').show();
			}
			else{
				$('#c_inventory_system_other').val('');
				$('#c_inventory_system_other').hide();
			}
		}
	);
	
	
	
});




// Show Password in Profile
$(document).on("keyup", '.cdrmed-modal-box-content #password', function(event) {
	var upassword = jQuery('.cdrmed-modal-box-content #password').val();
	var password_health = checkStrength('.cdrmed-modal-box-content #password', '.cdrmed-modal-box-content #m_password_feedback', upassword);
	jQuery('.cdrmed-modal-box-content #m_password_health').val(password_health);
	jQuery('.cdrmed-modal-box-content #m_password_feedback').html(password_health);
})
$(document).on("keyup", '.cdrmed-modal-box-content #c_password', function(event) {
	recheckStrength('.cdrmed-modal-box-content #password', '.cdrmed-modal-box-content #c_password', '.cdrmed-modal-box-content #m_c_password_feedback');
});


// check password health
function checkStrength(id, fid, password){
	if(password != ''){
		//initial strength
		var strength = 0
		
		//if the password length is less than 6, return message.
		if (password.length < 6) { 
			jQuery(id).removeClass('pweak pshort pgood pstrong')
			jQuery(fid).removeClass()
			jQuery(fid).addClass('short')
			jQuery(id).addClass('pshort')
			return 'Too short'; 
		}
		
		//length is ok, lets continue.
		
		//if length is 8 characters or more, increase strength value
		if (password.length > 7) strength += 1
		
		//if password contains both lower and uppercase characters, increase strength value
		if (password.match(/([a-z].*[A-Z])|([A-Z].*[a-z])/))  strength += 1
		
		//if it has numbers and characters, increase strength value
		if (password.match(/([a-zA-Z])/) && password.match(/([0-9])/))  strength += 1 
		
		//if it has one special character, increase strength value
		if (password.match(/([!,%,&,@,#,jQuery,^,*,?,_,~])/))  strength += 1
		
		//if it has two special characters, increase strength value
		if (password.match(/(.*[!,%,&,@,#,jQuery,^,*,?,_,~].*[!,%,&,@,#,jQuery,^,*,?,_,~])/)) strength += 1
		
		//now we have calculated strength value, we can return messages
		
		//if value is less than 2
		if (strength < 2 )
		{
			jQuery(id).removeClass('pweak pshort pgood pstrong')
			jQuery(fid).removeClass()
			jQuery(fid).addClass('weak')
			jQuery(id).addClass('pweak')
			return 'Weak'
		}
		else if (strength == 2 )
		{
			jQuery(id).removeClass('pweak pshort pgood pstrong')
			jQuery(fid).removeClass()
			jQuery(fid).addClass('good')
			jQuery(id).addClass('pgood')
			return 'Good'		
		}
		else
		{
			jQuery(id).removeClass('pweak pshort pgood pstrong')
			jQuery(fid).removeClass()
			jQuery(fid).addClass('strong')
			jQuery(id).addClass('pstrong')
			return 'Strong'
		}
	}
	else{
		return '';
	}
}

// check re-password
function recheckStrength(f_id, s_id, fid){
	if(jQuery(f_id).val() != jQuery(s_id).val()){
		jQuery(s_id).removeClass('pweak pshort pgood pstrong');
		jQuery(fid).addClass('short');
		jQuery(fid).html('Passwords do not match.');
		jQuery(s_id).addClass('pshort');
	}
	else if((jQuery(f_id).val() == jQuery(s_id).val()) || jQuery(f_id).val() == ''){
		jQuery(s_id).removeClass('pweak pshort pgood pstrong');
		jQuery(fid).removeClass();
		jQuery(fid).html('');
	}
	
	return '';
}



/*
==================================================================================
		Inventory/Products
==================================================================================
*/

jQuery(document).ready(function($) {
	
	$("#lab_result").on('change',function(e){
		
		if (this.files && this.files[0]) {
			
			var filename = $('#lab_result').val().split('\\').pop();
			//console.log(filename,$('#lab_result_select'));
			var lastIndex = filename.lastIndexOf("\\");   
			$('#lab_result_select').html(filename);
			
			//alert(this.files[0])
            var reader = new FileReader();
            reader.onload = function (e) {
                $('#lab_result_src').val(e.target.result);
				//$('#lab_result_select').html('Lab Result Selected');
            }
            reader.readAsDataURL(this.files[0]);
        }
    });
	
	
	$("#product_cvs_import").on('change',function(e){
		/*
		*
		*	Code for import product from CVS File
		*	above lab results array realted to this code
		*/
		//get uploaded file extension
		var ext = $("input#product_cvs_import").val().split(".").pop().toLowerCase();
		//if file is not csv
		if($.inArray(ext, ["csv"]) == -1) {
			$('.product_select_import').html('Invalid Format');
			setTimeout( function() {
				$('.product_select_import').html('Import product');
			}, 2000);
		}
		else if (e.target.files != undefined) {
			
			$('.product_select_import').html('Products importing...');
			
			var reader = new FileReader();
			reader.onload = function(e) {
				//fetch rows in array from csv file
				var products = e.target.result.split("\n");
				//index of lab results array
				var product_counter = 0;
				//foreach loop for row by row
				
				var products_arr = {};
				
				var product_head = products[0].split(",");
				
				$.each(products, function (index, value) {
					
					if(value && product_counter > 0){
						
						var products_arr_data = {};
						var product_data = value.split(",");
						for(var i=0; i < product_head.length; i++){
							if(product_head[i] != ''){
								var label = product_head[i].toLowerCase();
								label = label.replace(" ", "_");
								label = label.replace("-", "_");
								label = label.replace("-", "_");
								products_arr_data[label] = product_data[i];
							}
						}
						
						products_arr[product_counter] = products_arr_data;
					}
					
					product_counter++;
					
				});
				
				var data = {
					action: 'import_product_from_cvs_ajax',
					'products': products_arr
				};
				jQuery.post(cdrmedajax.ajax_url, data, function(response) {
					//$('.product_select_import').html('Uploaded Successfully');
					$('.product_select_import').html(response);
					
					setTimeout( function() {
						$('.product_select_import').html('Import product');
						window.location.reload();
					}, 3000);
					
				});
				
			};
			reader.readAsText(e.target.files.item(0));
		}
		return false;
	});
	
	
	
	$("#lab_result_cvs_import").on('change',function(e){
		/*
		*
		*	Code for Lab results from import CVS File
		*	above lab results array realted to this code
		*/
		//get uploaded file extension
		var ext = $("input#lab_result_cvs_import").val().split(".").pop().toLowerCase();
		//if file is not csv
		if($.inArray(ext, ["csv"]) == -1) {
			$('.lab_result_select_import').html('Invalid Format');
		}
		else if (e.target.files != undefined) {
			var reader = new FileReader();
			reader.onload = function(e) {
				//fetch rows in array from csv file
				var csvval = e.target.result.split("\n");
				
				//index of lab results array
				//var lab_counter = 1;
				//foreach loop for row by row
				//$.each(csvval, function (index, value) {
					
					
					
					//split each row by comma as a value
					var lab_result_head = csvval[0].split(",");
					var lab_result_data = csvval[1].split(",");
					//loop for convert each split value into lab result value
					for(var i=0; i < lab_result_head.length; i++){
						//fetch values for lab results
						
						var label = lab_result_head[i].toLowerCase();
						label = label.replace(" ", "_");
						label = label.replace("-", "_");
						label = label.replace("-", "_");
						var data = lab_result_data[i];
						//push lab results
						$('.inventory-form-custom #'+cdrmedajax.lab_result_pre_fix1+label).val(data);
						$('.inventory-form-custom #'+cdrmedajax.lab_result_pre_fix1+label+cdrmedajax.lab_result_pre_fix2).val(data * 100);
						
						//$('.inventory-form-custom .lb_'+i).val(csvvalue[i]);
						//$('.inventory-form-custom .lb_m_'+i).val(csvvalue[i]*100);
					}
					//lab_counter++;
				//});
				$('.lab_result_select_import').html('Uploaded Successfully');
			};
			reader.readAsText(e.target.files.item(0));
		}
		
		setTimeout( function() {
			$('.lab_result_select_import').html('Upload Lab Results');
		}, 3000);

		return false;
	});
	
	
	$('#new_category_group').hide();
	jQuery('#product_cat_toogle').click(function(e){
		e.preventDefault();
		
		if($("#new_category_group").is(':hidden')){
			$('#new_category_group').show();
		}
		else{
			$('#new_category_group').hide();
		}
	});
	
	
	
	
	
	
	var max_fields      = 60; //maximum input boxes allowed
    var wrapper         = jQuery(".input_fields_wrap"); //Fields wrapper
    var add_button      = jQuery(".add_field_button"); //Add button ID

    var wrapper2         = jQuery(".input_fields_wrap2"); //Fields wrapper
    var add_button2      = jQuery(".add_field_button2"); //Add button ID

    var wrapper3         = jQuery(".input_fields_wrap3"); //Fields wrapper
    var add_button3      = jQuery(".add_field_button3"); //Add button ID
    
    var x = 1; //initlal text box count
    jQuery(add_button).click(function(e){ //on add input button click
        e.preventDefault();
		
        if(x < max_fields){ //max input box allowed
            x++; //text box increment

            jQuery(wrapper).before('<tr><td><input type="text" class="form-control frst" name="_azcl_compunds[]"/></td><td><span class="scnd"><input type="text" class="form-control scnd2" name="_azcl_potency[]"/></span></td><td><input type="text" class="form-control tp2" name="_azcl_potency_tests[]"/></td><td><input type="text" class="form-control tp2" value="0.001" name="_azcl_potency_ppm[]"/><a href="javascript:void(0);" class="remove_field"><i class="fa fa-remove"></i></a></td></tr>'); //add input box
        }
		return false;
    });

      jQuery(add_button2).click(function(e){ //on add input button click
        e.preventDefault();
        if(x < max_fields){ //max input box allowed
            x++; //text box increment
            jQuery(wrapper2).before('<tr><td><input type="text" class="form-control frst" name="_azcl_compunds_terpene[]"/></td><td><span class="scnd"><input type="text" class="form-control scnd2" name="_azcl_terpene[]"/></span></td><td><input type="text" class="form-control tp2" name="_azcl_terpene_tests[]"/></td><td><input type="text" class="form-control tp2" value="0.001" name="_azcl_terpene_ppm[]"/><a href="javascript:void(0);" class="remove_field2"><i class="fa fa-remove"></i></a></td></tr>'); //add input box
        }
		return false;
    });

   jQuery(add_button3).click(function(e){ //on add input button click
        e.preventDefault();
        if(x < max_fields){ //max input box allowed
            x++; //text box increment
            jQuery(wrapper3).before('<tr><td><input type="text" class="form-control frst" name="compunds_residual[]"/></td><td><span class="scnd"><input type="text" class="form-control scnd2" name="residual[]"/></span></td><td><input type="text" class="form-control tp2" name="residual_tests[]"/></td><td><input type="text" class="form-control tp2" value="0.001" name="residual_ppm[]"/><a href="javascript:void(0);" class="remove_field3"><i class="fa fa-remove"></i></a></td></tr>'); //add input box
        }
		return false;
    });

	jQuery(document).on("click",".remove_field", function (e) {
   // jQuery(".remove_field").click(function(e){ //user click on remove text
        e.preventDefault(); 
        var row = jQuery(this).closest('tr');
        // var prev = row.prev();
        //prev.remove();
        row.remove();
        //jQuery(this).closest("tr").remove();
        //jQuery(this).parent('tr').remove();
        x--;
    });
    jQuery(document).on("click",".remove_field2", function (e) { //user click on remove text
        e.preventDefault();
        var row = jQuery(this).closest('tr');
        //var prev = row.prev();
        row.remove();
        //jQuery(this).closest("tr").remove();
        //jQuery(this).parent('tr').remove();
        x--;
    });

    jQuery(document).on("click",".remove_field3", function (e) { //user click on remove text
        e.preventDefault(); 
        var row = jQuery(this).closest('tr');
        //var prev = row.prev();
        row.remove();
        //jQuery(this).closest("tr").remove();
        //jQuery(this).parent('tr').remove();
        x--;
    })
	
	
	
});




/*
==================================================================================
		Commnets & Notes
==================================================================================
*/

// Submit Functionality Function of Edit Note
jQuery('.sub2').live('click', function (event) {
	var id = event.target.id;
	jQuery('#'+id).hide();
	var old_comment_id = id.replace('update', "");
	jQuery('#old_comment_id').val(old_comment_id);
	id =  id.replace('update', "#commentsnote");
	jQuery('#notescomments').val(jQuery(id).html());
	jQuery('#notes-form').submit();
	jQuery('#submit-note').trigger('click');
});

// Live Edit Functionality for Notes
jQuery('.edit').live('click', function (event) {
	var id = event.target.id;
	id =  id.replace('edit', "");
	jQuery('#submit-note').attr('data-type','edit_note');
	jQuery('#old_comments').val(jQuery('#commentsnote'+id).text());

	var attr = jQuery('.commentsnote').attr('contenteditable');
	var value = jQuery('#commentsnote'+id).attr('contenteditable');

	if (value == 'false' || typeof value === "undefined") {
		 jQuery('#commentsnote'+id).css("background-color", "#FFFFCC");
		jQuery('#commentsnote'+id).attr('contenteditable','true');
		jQuery("#previousnotes"+id).css({ 'margin-bottom': '65px' });
		jQuery('#update'+id).show();
	}
	else {
		jQuery('#commentsnote'+id).css("background-color", "#ffffff");
		jQuery('#commentsnote'+id).attr('contenteditable','false');
		 jQuery("#previousnotes"+id).css({ 'margin-bottom': '15px' });
		 jQuery('#submit-note').attr('data-type','new_note');
		jQuery('#update'+id).hide();		
	}       
});

// Delete Call Functionality of Note
jQuery('.delete-patient-note').live('click', function (event) {
	var answer = confirm ("Are you sure you want to delete the note?");
	if (answer) {
	jQuery('#submit-note').attr('data-type','delete_note');
	var id = event.target.id;
	jQuery('#old_comment_id').val(id);
	jQuery('#notes-form').submit();
	jQuery('#submit-note').trigger('click');
	}
});

jQuery(document).ready(function($) {
	// Model Box for Notes History
	jQuery(".mytooltop").live("click",function(){
		jQuery('.cdrmed-modal-box-content .view_user_notes').html('');
		jQuery('.cdrmed-modal-box-content .view_user_notes').html(jQuery(".notesalla"+this.id).html());
	});
});

jQuery('#notesalla').on('hidden.bs.modal', function () {
	//jQuery(".notesalla").hide();
});

// Function for Printing Any HTML Area Enclosed in a Div.
jQuery(document).on('click','#printDiv',function() {
	var getPrintEl = jQuery(this).data('print');
	var printContents = document.getElementById(getPrintEl).innerHTML;
	 var originalContents = document.body.innerHTML;
	 document.body.innerHTML = printContents;
	 window.print();
	 document.body.innerHTML = originalContents;
});

jQuery(document).ready(function(e) {
	
	// code for frequency dropdrown
	// jQuery('.frequency1').unbind('change');
	jQuery("body").tooltip({ selector: '[data-toggle=tooltip]' });
	
});



/*
==================================================================================
		Advance search
==================================================================================
*/







jQuery(document).ready(function($) {
	
	
	$('.advance_search_filter').each(function() {
		var filter_id = $(this).data('filter_id');
		var selected = $(this).data('selected');
		if(selected == ''){
			$("#" + filter_id).hide();
			$(this).data('selected','');
			$(this).removeClass('active');
		}
	});

	$(".advance_search_filter").click(function(){
		var filter_id = $(this).data('filter_id');
		var selected = $(this).data('selected');
		if(selected != ''){
			$("#" + filter_id).hide();
			$(this).data('selected','');
			$(this).removeClass('active');
		}
		else{
			$("#" + filter_id).show();
			$(this).data('selected','selected');
			$(this).addClass('active');
		}
	});
	
	
	
	$('.search-all-patients').click(function () {
		var allFilled = true;
		window.patinets_search_arr = [];
		$('#search-condition-rows :input:not(:button), #search-condition-rows select').each(function(index, element) {
			if (element.value == '' || element.value == '0') {
				allFilled = false;
			}
		});
		if(allFilled){
			jQuery('#search-condition-rows .search-condition-row').each(function(){
				var search_filed = jQuery(this).find('.row-condition-field').val();
				var search_operator = jQuery(this).find('.row-condition-operator').val();
				var search_value = jQuery(this).find('.row-condition-value').val();
			var search_value_two = jQuery(this).find('.row-condition-value-two').val();
				var search_row_relation = jQuery(this).find('.row-condition-relation').val();
				patinets_search_arr.push({
					row_field: search_filed,
					row_operator: search_operator,
					row_value: search_value,
			  search_value_two:search_value_two,
					row_relation: search_row_relation,
				});
			});

			jQuery('#advanced-search-loading').show();

			jQuery.post(wc_add_to_cart_params.ajax_url, {action: 'cdrmed_advanced_patients_search', data:patinets_search_arr}, function(data, textStatus, xhr) {
				jQuery('.advanced-search-results-table tbody').html(data);
				jQuery('#advanced-search-loading').hide();
				jQuery('.advanced-search-results-table').show();
			});

		}else{
			alert('Looks like some fields are empty! Please fill all fields');
		}
	});
	
	
	jQuery('body').on('click', '.add-new-condition-row', function(event) {
		event.preventDefault();
		var rows_count = jQuery('#search-condition-rows .search-condition-row').length;
		var second_last_column = $( "#search-condition-rows div:nth-last-child(2)" ).find('.search-last-column');
		if ( second_last_column.children('.row-condition-relation').length < 1 ) {
			var relation_dropdown = '<select class="row-condition-relation" style="width: auto; margin-right: 8px;"><option value="and">And</option><option value="or">Or</option></select>';
			second_last_column.find('label').text("Relation");
			second_last_column.find('br').after(relation_dropdown);
		}
		jQuery('#search-condition-rows .search-condition-row:first-child .remove-condition-row').show();
		var condition_row = $(this).closest('#search-condition-rows').find('.search-condition-row').first().clone();
		condition_row.find(".row-condition-field option[value=0]").attr("selected", "selected");
		condition_row.find(".row-condition-operator option[value=0]").attr("selected", "selected");
		condition_row.find('.row-condition-value').val('');
		condition_row.find('.search-last-column label').html('&nbsp;');
		condition_row.find('.search-last-column .row-condition-relation').remove();
		condition_row.find('.row-condition-operator .conditional-option-age').remove();
		condition_row.find('.row-condition-value-col .conditional-option-age, .row-condition-value-col span').remove();
		condition_row.find('.row-condition-value-col input').attr({
			type: 'text',
			style: '',
			placeholder: 'Value'
		});
		condition_row.find('.row-condition-field').closest('.col').removeClass('col-md-2').addClass('col-md-4');
		condition_row.find('.row-condition-value-col').removeClass('col-md-4').addClass('col-md-2');
		$('#search-condition-rows div:last').before( condition_row );
	});

	jQuery('body').on('click', '.remove-condition-row', function(event) {
		var delete_row = confirm('Are you sure you want to delete this row?');
		if(delete_row)
			jQuery(this).closest('.search-condition-row').remove();
		var rows_count = jQuery('#search-condition-rows .search-condition-row').length;
		if (rows_count > 1) {
			jQuery('#search-condition-rows .search-condition-row:first-child .remove-condition-row').show();
		} //else {
			jQuery('#search-condition-rows .search-condition-row:first-child .remove-condition-row').hide();
			var second_last_column = $( "#search-condition-rows div:nth-last-child(2)" ).find('.search-last-column');
			second_last_column.find('label').text("");
			second_last_column.find('.row-condition-relation').remove();
		//}
	});
	
	
	
	
	jQuery('body').on('change', '.row-condition-field', function(event) {
		var row = jQuery(this).closest('.search-condition-row');
		var selected_value = jQuery(this).val();
		if(selected_value == "Age"){
			row.find('.row-condition-operator').append('<option class="conditional-option-age" value="between">Between</option>');
		}else if(selected_value == "principal_primary_diagnosis"){
			row.find('.row-condition-value-col input').autocomplete({
			source: locations_arrayd
		});
			row.find('.row-condition-operator .conditional-option-age').remove();
		}else if(selected_value == "medication_name"){
			row.find('.row-condition-value-col input').autocomplete({
				source: locations_array
			});
			row.find('.row-condition-operator .conditional-option-age').remove();
		}else{
			row.find('.row-condition-operator .conditional-option-age').remove();
			row.find('.row-condition-value-col input').autocomplete("destroy");
			//row.find('.row-condition-operator').change();
		}
	});
	
	jQuery('body').on('change', '.row-condition-operator', function(event) {
		var row = jQuery(this).closest('.search-condition-row');
		var its_value = row.find('.row-condition-operator').val();
		if(its_value == 'between'){
			row.find('.row-condition-value-col').append('<span style="float: left; margin: 7px 6px;">to</span> <input type="number" class="row-condition-value-two conditional-option-age"  />');
			row.find('.row-condition-field').closest('.col').removeClass('col-md-4').addClass('col-md-2');
			row.find('.row-condition-value-col').removeClass('col-md-2').addClass('col-md-4');
			row.find('.row-condition-value-col input').attr({
				type: 'number',
				style: 'width:45%; float:left;',
				placeholder: ''
			});
		}else{
			row.find('.row-condition-value-col .conditional-option-age, .row-condition-value-col 	span').remove();
			row.find('.row-condition-value-col input').attr({
				type: 'text',
				style: '',
				placeholder: 'Value'
			});
			row.find('.row-condition-field').closest('.col').removeClass('col-md-2').addClass('col-md-4');
			row.find('.row-condition-value-col').removeClass('col-md-4').addClass('col-md-2');
		}
	});
	
	
	
});






/*
==================================================================================
		Shoop System & Medicine
==================================================================================
*/





function submitDetailsForm() {
	var dt1 = jQuery(".selected-terpens11 span").html();
	var dt2 = jQuery(".selected-terpens12 span").html();
	var dt3 = jQuery(".selected-terpens13 span").html();
	var dt4 = jQuery(".selected-terpens14 span").html();

	jQuery('.myterpene1').val(dt1);
	jQuery('.myterpene').val(dt1);
	jQuery('.myterpene2').val(dt2);
	jQuery('.myterpene3').val(dt3);
	jQuery('.myterpene4').val(dt4);
	return true;
}
function showpurple(){
	jQuery('.reded').show();
	jQuery('.purple').removeClass("last");
	jQuery('.reded').addClass("last");
}


//2nd medicine add
function purple_add(){
	jQuery('.reded').show();
	jQuery('.purple').removeClass("last");
	jQuery('.reded').addClass("last");
}
//2nd medicine remove
function reded_remove(){
	jQuery('.reded').hide();
	jQuery('.purple').addClass("last");
	jQuery('.reded').removeClass("last");
	
	// reset Cannabinoid if medicine remove
	$('.reded .primary_dosage button').attr('title','Select Target Cannabinoid...');
	$('.reded .primary_dosage button span.filter-option').html('Select Target Cannabinoid...');
	$('.reded .primary_dosage ul li').removeClass('selected');
	$('.reded .primary_dosage ul li:first').addClass('selected');
	$('.reded .primary_dosage select').removeAttr('selected');
	$('.reded .primary_dosage select option:first').attr('selected','selected');
}
//3nd medicine add
function reded_add(){
	jQuery('.lightblue').show();
	jQuery('.reded').removeClass("last");
	jQuery('.lightblue').addClass("last");
}
//3nd medicine remove
function lightblue_remove(){
	jQuery('.lightblue').hide();
	jQuery('.reded').addClass("last");
	jQuery('.lightblue').removeClass("last");
	
	// reset Cannabinoid if medicine remove
	$('.lightblue .primary_dosage button').attr('title','Select Target Cannabinoid...');
	$('.lightblue .primary_dosage button span.filter-option').html('Select Target Cannabinoid...');
	$('.lightblue .primary_dosage ul li').removeClass('selected');
	$('.lightblue .primary_dosage ul li:first').addClass('selected');
	$('.lightblue .primary_dosage select').removeAttr('selected');
	$('.lightblue .primary_dosage select option:first').attr('selected','selected');
}
//4nd medicine add
function lightblue_add(){
	jQuery('.lightgreen ').show();
	jQuery('.lightblue').removeClass("last");
	jQuery('.lightgreen').addClass("last");
}
//4nd medicine remove
function lightgreen_remove(){
	jQuery('.lightgreen ').hide();
	jQuery('.lightgreen').removeClass("last");
	jQuery('.lightblue').addClass("last");
	
	// reset Cannabinoid if medicine remove
	$('.lightgreen .primary_dosage button').attr('title','Select Target Cannabinoid...');
	$('.lightgreen .primary_dosage button span.filter-option').html('Select Target Cannabinoid...');
	$('.lightgreen .primary_dosage ul li').removeClass('selected');
	$('.lightgreen .primary_dosage ul li:first').addClass('selected');
	$('.lightgreen .primary_dosage select').removeAttr('selected');
	$('.lightgreen .primary_dosage select option:first').attr('selected','selected');
}



function red_remove(){
	jQuery('.reded').hide();
	jQuery('.purple').addClass("last");
	jQuery('.reded').removeClass("last");
}
function red_add(){
	jQuery('.lightblue').show();
	jQuery('.reded').removeClass("last");
	jQuery('.lightblue').addClass("last");
}





/**
* Medicine recommend functions - popup steps
*/
window.cdrmed_steps = {};
cdrmed_steps['current_step'] = 0;
cdrmed_steps['selected_prod'] = [];

jQuery(document).ready(function(e) {
	
	/* Go Product Selection for Patient's Shoop */
	
	//jQuery('#ProductsModal').on('shown.bs.modal', function() {
	jQuery('.show-product-modal').on('click', function() {
		
		// emplty all steps when modal is open
		jQuery('#calucations_items_thumbs').html('');
		jQuery('#calucations_items').html('');
		jQuery('#selected_products_markup_thumb').html('');
		jQuery('#selected_products_markup').html('');
		jQuery('#ProductsModal .modal-contendddt.cdrmed-step').hide();
		// get toal midicine in shoop
		var shoop_medicine_count = parseInt($('.shoop_medicine_count').val());
		for(i = 0; i < shoop_medicine_count; i++){
			jQuery('#ProductsModal .step_'+i+' #heading-text_'+i+' h4').attr('data-cannabinoid', '');
			jQuery('#ProductsModal .step_'+i+' #heading-text_'+i+' h4').html('');
			jQuery('#ProductsModal .step_'+i+' .cdrmed_modal_slider').html('');
		}
		
		jQuery('#ProductsModal .shoop-waiting').html('<img style="display: inherit; margin: 5px auto;" src="'+cdrmedajax.img_url+'/loading-secure-animation.gif" alt="" />');
		 $('.shoop_current_step').val('0');
		// get aptient value from modal box
		var patient_id = $('.shoop_patient_id').val();
		//show loading animation and step one (number 0)
		jQuery('#ProductsModal .shoop-waiting').show();
		jQuery('#ProductsModal .step_0').show();
		
		var data = {
			action: 'shoop_product_fetch_ajax',
			'patient_id': patient_id,
			'shoop_medicine': shoop_medicine_count,
			'step': 1,
		};
		//ajax call for fetch product for single medicine
		jQuery.post(cdrmedajax.ajax_url, data, function(response) {
			var response = jQuery.parseJSON(response);
			jQuery('#ProductsModal .shoop-waiting').hide();
			if(response.action){
				//heading data attribute
				jQuery('#ProductsModal .step_0 #heading-text_0 h4').attr('data-cannabinoid',response.cannabinoid);
				// step heading
				jQuery('#ProductsModal .step_0 #heading-text_0 h4').html(response.heading);
				//step products
				jQuery('#ProductsModal .step_0 .cdrmed_modal_slider').html(response.products);
				//if products from other dispensary
				jQuery('#ProductsModal .step_0 .cdrmed_modal_slider').append(response.other);
			}
			else{
				//if no product fount for step
				jQuery('#ProductsModal .step_0').html(response.products);
			}
			
		});
	});
	
	
	jQuery('#ProductsModal .fa-close').live('click', function(){
		//reset array when modal close
		cdrmed_steps['current_step'] = 0;
		cdrmed_steps['selected_prod'] = [];
	});
	
	jQuery('.cancel-therapy').live('click', function(){
		//modal cancel button
		window.location.reload();
	});
	
	jQuery('.select_cdrm_item.btn').live('click', function(){
		
		var product_id = $(this).attr('data-id');
		
		//current selected prodcut id push in selected array
		var existing_selected_products = cdrmed_steps['selected_prod'];
		var current_step = cdrmed_steps['current_step'];
		var selected_product = product_id;
		//push slected product id in array
		existing_selected_products.push(product_id);
		
		var patient_id = $('.shoop_patient_id').val();
		//current step
		var shoop_current_step = parseInt($('.shoop_current_step').val());
		//total medicine in shoop
		var shoop_medicine_count = parseInt($('.shoop_medicine_count').val());
		//count next step
		var next_step = shoop_current_step + 1;
		
		if(!jQuery('.cdrmed-step.step_'+next_step).hasClass('step-show-selected-products')){
			
			//hide old step
			jQuery('.cdrmed-step.step_'+shoop_current_step).hide();
			jQuery('#ProductsModal .shoop-waiting').show();
			
			var data = {
				action: 'shoop_product_fetch_ajax',
				'patient_id': patient_id,
				'existing_selected_products': existing_selected_products,
				'shoop_medicine': shoop_medicine_count,
				'step': next_step + 1,
			};
			//ajax call for fetch product for single medicine
			jQuery.post(cdrmedajax.ajax_url, data, function(response) {
				var response = jQuery.parseJSON(response);
				jQuery('#ProductsModal .shoop-waiting').hide();
				if(response.action){
					//set heading data cannabinoid
					jQuery('#ProductsModal .step_'+next_step+' #heading-text_'+next_step+' h4').attr('data-cannabinoid',response.cannabinoid);
					//set step heading
					jQuery('#ProductsModal .step_'+next_step+' #heading-text_'+next_step+' h4').html(response.heading);
					//set step products
					jQuery('#ProductsModal .step_'+next_step+' .cdrmed_modal_slider').html(response.products);
					//if products from other dispensary
					jQuery('#ProductsModal .step_'+next_step+' .cdrmed_modal_slider').append(response.other);
				}
				else{
					//if no product fount for step
					jQuery('#ProductsModal .step_'+next_step).html(response.products);
				}
				//show new step
				jQuery('.cdrmed-step.step_'+next_step).show();
				//update step value
				$('.shoop_current_step').val(next_step);
				
			});
		}
		
		jQuery(this).closest('.cdrmed-step.step_'+current_step).fadeOut('fast', function() {
			/*
			* display selected products when calculate dosage
			*/
			
			
			
			cdrmed_steps['current_step'] = next_step;
			jQuery('.cdrmed-step.step_'+next_step).show();
			if(jQuery('.cdrmed-step.step_'+next_step).hasClass('step-show-selected-products')){
				jQuery.each(existing_selected_products, function(index, val) {
					//var product_item = jQuery('.cdrmed_slide_item.pro'+val);
					var product_title = jQuery('.step_'+index+' .cdrmed_slide_item.pro'+val+' .cdrm_title').html();
					var product_image = jQuery('.step_'+index+' .cdrmed_slide_item.pro'+val+' img').attr('src');
					var product_link = jQuery('.step_'+index+' .cdrmed_slide_item.pro'+val+' a').attr('href');
					var product_markup = '<div class="cdrm_item">';
					product_markup += '<img alt="image" src="'+product_image+'">';
					product_markup += '<h3 class="cdrm_title">'+product_title+'</h3>';
					
					product_markup += '<a href="'+product_link+'">More info</a></div>';

					var cannabinoid_heading = jQuery('.step_'+index+' #heading-text_'+index).text();

					cannabinoid_name = cannabinoid_heading.slice(cannabinoid_heading.indexOf('(') +1,cannabinoid_heading.indexOf(')'));
					var product_thumb_markup = '<div class="selected_cdrm_items" style="background: url('+product_image+');">'
					product_thumb_markup += '<span>'+cannabinoid_name+'</span></div>';
					jQuery('#selected_products_markup_thumb').append(product_thumb_markup);
					jQuery('#selected_products_markup').append(product_markup);
				});
			} 
		});
		
	});
	
	
	
	//calculate daily dosage print
	jQuery('#calculate-daily-dosage').click(function(){
		
		jQuery('#ProductsModal .shoop-waiting').show();
		jQuery('.step-show-selected-products').fadeOut('fast', function() {
			
			
			if(cdrmed_steps['selected_prod'].length == 2){
				
				
				jQuery('#calucations_items').html('<img style="display: inherit; margin: 5px auto;" src="'+cdrmedajax.img_url+'/loading-secure-animation.gif" alt="" />');
				
				var patient_id = jQuery('#calculate-daily-dosage').attr('data-id');
				// ajax call from patient-function/calculate-two-medicine-dose-ajax-call.php
				var data = {
					action: 'calculate_two_medicine_dose_ajax',
					'patient_id': patient_id,
					'pro_ids': cdrmed_steps['selected_prod'],
				};
				jQuery.post(cdrmedajax.ajax_url, data, function(response) {
					var response = jQuery.parseJSON(response);
					//console.log(response);
					// if calculation is greate than 0
					if(response.dose_calculation_check){
						jQuery('#calucations_items').html('');
						var step = 0;
						for (i = 1; i < 3; i++) {
							
							var cannabinoid_heading = jQuery('#heading-text_'+step+' h4').attr('data-cannabinoid');
							
							var val = response['p'+i];
							var product_name = jQuery('.step_'+step+' .cdrmed_slide_item[data-product-id="'+val+' .cdrm_title').html();
							var product_image = jQuery('.step_'+step+' .cdrmed_slide_item[data-product-id="'+val+' img').attr('src');
							//jQuery("img").attr('src');
							
							/*
							*	get values from hidden inputs
							*/
							var $dosage_markup = '<div class="theropy_item">';
								$dosage_markup += '<h4>To achieve of your target dose of <br /> '+cannabinoid_heading+'</h4>';
								$dosage_markup += '<div class="droper_icon"><span class="fa fa-eyedropper"></span></div>';
								
								//append currenr product info in list
								$dosage_markup += '<ul class="the_medicine'+i+'"><li>'+ $('.step_'+step+'  .cdrmed_slide_item.pro'+val+' .p_product_title').val() +' <br /> '+ $('.step_'+step+' .cdrmed_slide_item.pro'+val+' .p_cannabinoid_percent_value').val() +' % '+ $('.step_'+step+' .cdrmed_slide_item.pro'+val+' .p_cannabinoid').val() +' | '+ $('.step_'+step+' .cdrmed_slide_item.pro'+val+' .s_cannabinoid_percent_value').val() +' % '+ $('.step_'+step+' .cdrmed_slide_item.pro'+val+' .s_cannabinoid').val() +'</li>';
								
								// append frequency per day 
								$dosage_markup += '<li> To Be Taken '+ jQuery('.step_'+step+' .cdrmed_slide_item.pro'+val+' .frequency_per_day').val() +'</li>';
								
								//update two medicine calculation values
								$('.step_'+step+' .cdrmed_slide_item.pro'+val+' .this_much_value').val(response['med'+i+'_per_day1']);
								//console.log(response['med'+i+'_per_day']+" => "+$('.cdrmed_slide_item.pro'+val+' .this_much_value').val())
								
								//update two medicine calculation values
								$('.step_'+step+' .cdrmed_slide_item.pro'+val+' .dose_per_day').val(response['med'+i+'_per_dose']);
								//console.log(response['med'+i+'_per_dose']+ " => " +$('.cdrmed_slide_item.pro'+val+' .dose_per_day').val())
								
								// append per day medicine
								$dosage_markup += '<li> This Much '+ jQuery('.step_'+step+' .cdrmed_slide_item.pro'+val+' .p_cannabinoid').val() +' Per Day: '+ response['med'+i+'_per_day'] +' (g)</li>';
								
								// append per dose medicine
								$dosage_markup += '<li> Dose of '+ jQuery('.step_'+step+' .cdrmed_slide_item.pro'+val+' .p_cannabinoid').val() +' Each Time: '+ response['med'+i+'_per_dose'] +' (g)</li>';
								
								/*
								*	medicine ammount formula
								*/
								
								$dosage_markup += '<li> Total Amount of '+ jQuery('.step_'+step+' .cdrmed_slide_item.pro'+val+' .p_cannabinoid').val() +': '+response['med'+i+'_amount1']+' (mg)</li>';
								
								$dosage_markup += '<li> Total Amount of '+ jQuery('.step_'+step+' .cdrmed_slide_item.pro'+val+' .s_cannabinoid').val() +': '+response['med'+i+'_amount2']+' (mg)</li></ul>';
								
								/* $dosage_markup += '<li> Amount of '+ jQuery('.cdrmed_slide_item.pro'+val+' .p_cannabinoid').val() +' Per Day: '+jQuery('.cdrmed_slide_item.pro'+val+' .p_cannabinoid_percent_value').val()+' (mg)</li>';
								
								$dosage_markup += '<li> Amount of '+ jQuery('.cdrmed_slide_item.pro'+val+' .s_cannabinoid').val() +' Per Day: '+jQuery('.cdrmed_slide_item.pro'+val+' .s_cannabinoid_percent_value').val()+' (mg)</li></ul>'; */
								
							$dosage_markup += '</div>';

							cannabinoid_name = cannabinoid_heading.slice(cannabinoid_heading.indexOf('(') +1,cannabinoid_heading.indexOf(')'));
							var product_thumb_markup = '<div class="selected_cdrm_items" style="background: url('+product_image+');">'
								product_thumb_markup += '<span>'+cannabinoid_name+'</span></div>';

							jQuery('#calucations_items_thumbs').append(product_thumb_markup);
							jQuery('#calucations_items').append($dosage_markup);
							
							step++;
						}
					}
					else{
						jQuery('#calucations_items').html(response.output);
						jQuery('.step-view-therapy .cdrm-header').hide();
					}
					jQuery('#ProductsModal .shoop-waiting').hide();
					jQuery('.step-view-therapy').show();
				});
			}
			else{
				
				var existing_selected_products = cdrmed_steps['selected_prod'];
				
				var counter_med = 1;
				var step = 0;
				jQuery.each(existing_selected_products, function(index, val) {
					
					var cannabinoid_heading = jQuery('#heading-text_'+index+' h4').attr('data-cannabinoid');
					var product_name = jQuery('.step_'+step+' .cdrmed_slide_item.pro'+val).find(".cdrm_title").text();
					var product_image = jQuery('.step_'+step+' .cdrmed_slide_item.pro'+val).find("img").attr('src');
					
					/*
					*	get values from hidden inputs
					*/
					var $dosage_markup = '<div class="theropy_item">';
						$dosage_markup += '<h4>To achieve of your target dose of <br /> '+cannabinoid_heading+'</h4>';
						$dosage_markup += '<div class="droper_icon"><span class="fa fa-eyedropper"></span></div>';
						//append currenr product info in list
						$dosage_markup += '<ul class="the_medicine'+counter_med+'"><li>'+ $('.step_'+step+' .cdrmed_slide_item.pro'+val+' .p_product_title').val() +' <br /> '+ $('.step_'+step+' .cdrmed_slide_item.pro'+val+' .p_cannabinoid_percent_value').val() +' % '+ $('.step_'+step+' .cdrmed_slide_item.pro'+val+' .p_cannabinoid').val() +' | '+ $('.step_'+step+' .cdrmed_slide_item.pro'+val+' .s_cannabinoid_percent_value').val() +' % '+ $('.step_'+step+' .cdrmed_slide_item.pro'+val+' .s_cannabinoid').val() +'</li>';
						// append frequency per day 
						$dosage_markup += '<li> To Be Taken '+ jQuery('.step_'+step+' .cdrmed_slide_item.pro'+val+' .frequency_per_day').val() +'</li>';
						// append per day medicine
						$dosage_markup += '<li> This Much '+ jQuery('.step_'+step+' .cdrmed_slide_item.pro'+val+' .p_cannabinoid').val() +' Per Day: '+ jQuery('.step_'+step+' .cdrmed_slide_item.pro'+val+' .this_much_value').val() +' (g)</li>';
						// append per dose medicine
						$dosage_markup += '<li> Dose of '+ jQuery('.step_'+step+' .cdrmed_slide_item.pro'+val+' .p_cannabinoid').val() +' Each Time: '+ jQuery('.step_'+step+' .cdrmed_slide_item.pro'+val+' .dose_per_day').val() +' (g)</li>';
						// append medicine ammount
						$dosage_markup += '<li> Total Amount of '+ jQuery('.step_'+step+' .cdrmed_slide_item.pro'+val+' .s_cannabinoid').val() +': '+jQuery('.step_'+step+' .cdrmed_slide_item.pro'+val+' .secondary_calculations').val()+' (mg)</li></ul>';
						
					$dosage_markup += '</div>';

					cannabinoid_name = cannabinoid_heading.slice(cannabinoid_heading.indexOf('(') +1,cannabinoid_heading.indexOf(')'));
					var product_thumb_markup = '<div class="selected_cdrm_items" style="background: url('+product_image+');">'
						product_thumb_markup += '<span>'+cannabinoid_name+'</span></div>';

					jQuery('#calucations_items_thumbs').append(product_thumb_markup);
					jQuery('#calucations_items').append($dosage_markup);
					counter_med++;
					step++;
				});
				jQuery('#ProductsModal .shoop-waiting').hide();
				jQuery('.step-view-therapy').show();
			}
		});
		
	});
	
	
	
	
	
	
	
	
	
	
	
	//var form_holder=jQuery('.testlist');
	//form_holder.find('input[type="checkbox"]').click(function(e) {
	$('.testlist input[type="checkbox"]').click(function(e) {
		var selected_data=new Array();
		jQuery('.selected-terpens11').show();
		$('.testlist input[type="checkbox"]:checked').each(function(){
			selected_data.push(jQuery(this).val());
		});
		var target_p= jQuery(this).parents('.testlist').next('.selected-terpens').children('p');
		var target_p2= jQuery('.selected-terpens11').children('span');
		console.log('Selected Terpenes: ' + selected_data.length);
		if(selected_data.length !== 0){
		//	jQuery('.myterpene').val(selected_data.toString());
			target_p.html(selected_data.toString());
			target_p2.html(selected_data.toString());
		}
		else{
			target_p2.html('');
			jQuery('.selected-terpens11').hide();
			target_p.html('Nothing !');
		}
	});
	
	
	//var form_holder=jQuery('.testlist2');	
	$('.testlist2 input[type="checkbox"]').click(function(e) {
		var selected_data=new Array();
		jQuery('.selected-terpens12').show();
		$('.testlist2 input[type="checkbox"]:checked').each(function(){		
			selected_data.push(jQuery(this).val());
		});
		var target_p= jQuery(this).parents('.testlist2').next('#red').children('p');
		var target_p2= jQuery('.selected-terpens12').children('span');
		
		if(selected_data.length!==0){
			//jQuery('.myterpene2').val(selected_data.toString());
			target_p.html(selected_data.toString());
			target_p2.html(selected_data.toString());
		}else{
			target_p2.html('');
			jQuery('.selected-terpens12').hide();
			target_p.html('Nothing !');
		}
	});
	
	// Code for Lightblue terpene
	//var form_holder=jQuery('.testlist3');	
	$('.testlist3 input[type="checkbox"]').click(function(e) {
		var selected_data=new Array();
		jQuery('.selected-terpens13').show();
		$('.testlist3 input[type="checkbox"]:checked').each(function(){		
			selected_data.push(jQuery(this).val());
		});
		var target_p= jQuery(this).parents('.testlist3').next('#lightblue').children('p');
		var target_p3= jQuery('.selected-terpens13').children('span');
		
		if(selected_data.length!==0){
			//jQuery('.myterpene3').val(selected_data.toString());
			target_p.html(selected_data.toString());
			target_p3.html(selected_data.toString());
		}else{
			target_p3.html('');
			jQuery('.selected-terpens13').hide();
			target_p.html('Nothing !');
		}
	});
	
	// Code for Lightgreen terpene
	//var form_holder=jQuery('.testlist4');	
	$('.testlist4 input[type="checkbox"]').click(function(e) {
		var selected_data=new Array();
		jQuery('.selected-terpens14').show();
		$('.testlist4 input[type="checkbox"]:checked').each(function(){		
			selected_data.push(jQuery(this).val());
		});
		var target_p= jQuery(this).parents('.testlist4').next('#lightgreen').children('p');
		var target_p4= jQuery('.selected-terpens14').children('span');
		
		if(selected_data.length!==0){
			//jQuery('.myterpene4').val(selected_data.toString());
			target_p.html(selected_data.toString());
			target_p4.html(selected_data.toString());
		}else{
			target_p4.html('');
			jQuery('.selected-terpens14').hide();
			target_p.html('Nothing !');
		}
	});
	
	
	
	
	
	// ok button on terpene model boxes
	jQuery("#mode1").live("click",function(){
		   jQuery('#myModal1').modal('hide');
	}); 
	jQuery("#mode2").live("click",function(){
		   jQuery('#myModal2').modal('hide');
	});  
	jQuery("#mode3").live("click",function(){
		   jQuery('#myModal3').modal('hide');
	}); 
	jQuery("#mode4").live("click",function(){
		   jQuery('#myModal4').modal('hide');
	}); 
	
	
	
	// First Medicine
	jQuery('#frequency1').on('change', function() {
		jQuery(".new1").html('');
		var numb = jQuery('#frequency1').val(); // or $(this).val()
		var page_type = $('#medicine_page_type').val();
		if(jQuery('.frequency1 option:selected').val() == 'daily'){
			jQuery(".new1").html('');
			if(page_type == 'edit'){
				frequency_dose_ajax(1, numb);
			}
			else{
				for(var i = 1; i <= numb; i++) {
					frequency_dose_append(1, i, '', '', '', '', '', '', '');
				}
			}
		}
	});
	jQuery('.frequency1 .frequency1').change(function () {
		jQuery(".new1").html('');
		if(jQuery('.frequency1 option:selected').val() != 'daily'){ 
			//jQuery(".news1").remove();
		}
		else if ( jQuery('.frequency1 option:selected').val() == 'daily'){
			jQuery(".new1").html('');
			var numb = jQuery('#frequency1').val(); // or $(this).val()
			var page_type = $('#medicine_page_type').val();
			if(page_type == 'edit'){
				frequency_dose_ajax(1, numb);
			}
			else{
				for(var i = 1; i <= numb; i++) {
					frequency_dose_append(1, i, '', '', '', '', '', '', '');
				}
			}
		}
	});
	
	
	
	// Second Medicine
	jQuery('#frequency2').on('change', function() {
		jQuery(".new2").html('');
		var numb = jQuery('#frequency2').val(); // or $(this).val()
		var page_type = $('#medicine_page_type').val();
		if(jQuery('.frequency2 option:selected').val() == 'daily'){
			jQuery(".new2").html('');
			if(page_type == 'edit'){
				frequency_dose_ajax(2, numb);
			}
			else{
				for(var i = 1; i <= numb; i++) {
					frequency_dose_append(2, i, '', '', '', '', '', '', '');
				}
			}
		}
	});
	jQuery('.frequency2 .frequency2').change(function () {
		jQuery(".new2").html('');
		if(jQuery('.frequency2 option:selected').val() != 'daily'){ 
			//jQuery(".news2").remove();
		}
		if (jQuery('.frequency2 option:selected').val() == 'daily'){
			jQuery(".new2").html('');
			var numb = jQuery('#frequency2').val(); // or $(this).val()
			var page_type = $('#medicine_page_type').val();
				if(page_type == 'edit'){
					frequency_dose_ajax(2, numb);
				}
				else{
					for(var i = 1; i <= numb; i++) {
						frequency_dose_append(2, i, '', '', '', '', '', '', '');
					}
				}
			
		}
	}); 
	
	
	// Third Medicine
	jQuery('#frequency3').on('change', function() {
		jQuery(".new3").html('');
		var numb = jQuery('#frequency3').val(); // or $(this).val()
		var page_type = $('#medicine_page_type').val();
		if(jQuery('.frequency3 option:selected').val() == 'daily'){
			jQuery(".new3").html('');
			if(page_type == 'edit'){
				frequency_dose_ajax(3, numb);
			}
			else{
				for(var i = 1; i <= numb; i++) {
					frequency_dose_append(3, i, '', '', '', '', '', '', '');
				}
			}
		}
	});
	jQuery('.frequency3 .frequency3').change(function () {
		jQuery(".new3").html('');
		if(jQuery('.frequency3 option:selected').val() != 'daily'){ 
			//jQuery(".news3").remove();
		}
		if (jQuery('.frequency3 option:selected').val() == 'daily'){
			jQuery(".new3").html('');
			var numb = jQuery('#frequency3').val(); // or $(this).val()
			var page_type = $('#medicine_page_type').val();
				if(page_type == 'edit'){
					frequency_dose_ajax(3, numb);
				}
				else{
					for(var i = 1; i <= numb; i++) {
						frequency_dose_append(3, i, '', '', '', '', '', '', '');
					}
				}
			
		}
	});
	
	// Fourth Medicine
	jQuery('#frequency4').on('change', function() {
		jQuery(".new4").html('');
		var numb = jQuery('#frequency4').val(); // or $(this).val()
		var page_type = $('#medicine_page_type').val();
		if(jQuery('.frequency4 option:selected').val() == 'daily'){
			jQuery(".new4").html('');
			if(page_type == 'edit'){
				
				frequency_dose_ajax(4, numb);
			}
			else{
				for(var i = 1; i <= numb; i++) {
					frequency_dose_append(4, i, '', '', '', '', '', '', '');
				}
			}
		}
		
	});
	jQuery('.frequency4 .frequency4').change(function () {
		jQuery(".new4").html('');
		if(jQuery('.frequency4 option:selected').val() != 'daily'){ 
			//jQuery(".news4").remove();
		}
		if (jQuery('.frequency4 option:selected').val() == 'daily'){
			
			var numb = jQuery('#frequency4').val(); // or $(this).val()
			var page_type = $('#medicine_page_type').val();
			jQuery(".new4").html('');
				if(page_type == 'edit'){
					
					frequency_dose_ajax(4, numb);
				}
				else{
					for(var i = 1; i <= numb; i++) {
						frequency_dose_append(4, i, '', '', '', '');
					}
				}
			
		}
	});
	
	
	
	
	function frequency_dose_ajax(frequency_no, numb){
		jQuery(".new"+frequency_no).html('');
		var post_id = $('#medicine_post_id').val();
		
		var data = {
			'action': 'read_medicine_dosage_frequency_ajax',
			'post_id': post_id,
			'medicine_no': frequency_no,
		};
		
		$.post(cdrmedajax.ajax_url, data, function(response) {
			var response_data = jQuery.parseJSON(response);
			var medicine_dosag_counter = 0;
			for(var i = 1; i <= numb; i++) {
				var dose = '';
				var weight = '';
				var time = ''; 
				var afternoon = '';
				var evening = '';
				var morning = '';
				var mg = '';
				var gl = '';
				var ml = '';
				//depend on already stored no of dose
				if(response_data.medicine1_dosage_frequency.length > medicine_dosag_counter){
					dose = response_data.medicine1_dosage_frequency[medicine_dosag_counter];
					weight = response_data.medicine1_dosage_mg[medicine_dosag_counter];
					time = response_data.medicine1_dosage_timing[medicine_dosag_counter];
					
					if(time == 'afternoon'){
						afternoon = 'selected'
					}
					if(time == 'evening'){
						evening = 'selected';
					}
					if(time == 'morning'){
						morning = 'selected';
					}
					if(weight == 'mg'){
						mg = 'selected'
					}
					if(weight == 'gl'){
						gl = 'selected';
					}
					if(weight == 'ml'){
						ml = 'selected';
					}
					medicine_dosag_counter++;	
				}
				
				frequency_dose_append(frequency_no, i, dose, morning, afternoon, evening, mg, gl, ml);
			}
			
		});
	}
		
	function frequency_dose_append(frequency_no, i, dose, morning, afternoon, evening, mg, gl, ml){
		
		jQuery(".new"+frequency_no).append('<div class="col-sm-6 news'+i+'"><div class="form-group"><label>Daily Dose '+i+'</label><span class="fa fa-question round-info" data-toggle="tooltip" title="" data-original-title="Target Daily Dose"></span></div><div class="form-group"><input name="daily_dosage_frequncy'+frequency_no+'[]" id="daily_dosage_frequncy" type="text" value="'+dose+'" class="two-third appended-width form-control" tabindex="2"> <select class="selectpicker new-appended one-third" name="primary_dosage_timing'+frequency_no+'[]"><option value="morning" '+morning+'>morning</option><option value="afternoon" '+afternoon+'>afternoon</option><option value="evening" '+evening+'>evening</option></select><select class="selectpicker new-appended one-third" name="frequency_daily_dosage'+frequency_no+'[]" tabindex="-98"><option value="mg" '+mg+'>mg</option><option value="g" '+gl+'>g</option></select></div></div>');
		$('.selectpicker.new-appended').selectpicker();
	}
	
	
	
	
	jQuery('.shoop_edit_update_form').on('submit', function() {
		/* var error = null;
		
		if($('button[data-id="primary_dosage"]').attr('title') == 'Select Target Cannabinoid...'){
			$('button[data-id="primary_dosage"]').addClass('shoop_form_feedback_error');
			$('button[data-id="primary_dosage"]').addClass('shoop_form_feedback_border');
			error = true;
		}
		if($('.daily_dosage').val('') == ''){
			$('.daily_dosage').addClass('shoop_form_feedback_error');
			$('.daily_dosage').addClass('shoop_form_feedback_border');
			error = true;
		}
		if($('button[data-id="frequency1"]').attr('title') == 'Select Frequency...'){
			$('button[data-id="frequency1').addClass('shoop_form_feedback_error');
			$('button[data-id="frequency1').addClass('shoop_form_feedback_border');
			error = true;
		}
		if($('button[data-id="frequency2"]').attr('title') == 'Select Frequency...'){
			$('button[data-id="frequency2').addClass('shoop_form_feedback_error');
			$('button[data-id="frequency2').addClass('shoop_form_feedback_border');
			error = true;
		}
		if($('button[data-id="frequency3"]').attr('title') == 'Select Frequency...'){
			$('button[data-id="frequency3').addClass('shoop_form_feedback_error');
			$('button[data-id="frequency3').addClass('shoop_form_feedback_border');
			error = true;
		}
		if($('button[data-id="frequency4"]').attr('title') == 'Select Frequency...'){
			$('button[data-id="frequency4"]').addClass('shoop_form_feedback_error');
			$('button[data-id="frequency4"]').addClass('shoop_form_feedback_border');
			error = true;
		}
		if($('button[data-id="ingestion_method"]').attr('title') == 'Select Frequency...'){
			$('button[data-id="ingestion_method"]').addClass('shoop_form_feedback_error');
			$('button[data-id="ingestion_method"]').addClass('shoop_form_feedback_border');
			error = true;
		}
		
		if(error){
			setTimeout( function() {
				$('.shoop_form_feedback_error').removeClass('shoop_form_feedback_border');
			}, 4000);
			return false;
		} */

	});
	
	
	
	
});


/*
==================================================================================
		Patient Document
==================================================================================
*/




jQuery(document).ready(function(e) {
	
	var now     = new Date(); 
    var year    = now.getFullYear();
    var month   = now.getMonth()+1; 
    var day     = now.getDate();
    var hour    = now.getHours();
    var minute  = now.getMinutes();
    var second  = now.getSeconds(); 
    if(month.toString().length == 1) {
        var month = "0"+month;
    }
    if(day.toString().length == 1) {
        var day = "0"+day;
    }   
    if(hour.toString().length == 1) {
        var hour = "0"+hour;
    }
    if(minute.toString().length == 1) {
        var minute = "0"+minute;
    }
    if(second.toString().length == 1) {
        var second = "0"+second;
    }   
    var ampm = hour >= 12 ? 'PM' : 'AM';
    var dateTime = year+'/'+month+'/'+day+' '+hour+':'+minute+':'+second;   
	jQuery("#updated_date").val(dateTime);
	
	
	// Create two variable with the names of the months and days in an array
	var monthNames = [ "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December" ]; 
	var dayNames= ["Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday"]

	// Create a newDate() object
	var newDate = new Date();
	// Extract the current date from Date object
	newDate.setDate(newDate.getDate());
	// Output the day, date, month and year    
	jQuery('#Date').html(dayNames[newDate.getDay()] + " " + newDate.getDate() + ' ' + monthNames[newDate.getMonth()] + ' ' + newDate.getFullYear());

	setInterval( function() {
		// Create a newDate() object and extract the seconds of the current time on the visitor's
		var seconds = new Date().getSeconds();
		// Add a leading zero to seconds value
		jQuery("#sec").html(( seconds < 10 ? "0" : "" ) + seconds);
	},1000);
  
	setInterval( function() {
		// Create a newDate() object and extract the minutes of the current time on the visitor's
		var minutes = new Date().getMinutes();
		// Add a leading zero to the minutes value
		jQuery("#min").html(( minutes < 10 ? "0" : "" ) + minutes);
    },1000);
  
	setInterval( function() {
		// Create a newDate() object and extract the hours of the current time on the visitor's
		var hours = new Date().getHours();
		var ampm = hours >= 12 ? 'PM' : 'AM';
		jQuery("#am").html(ampm);
		// Add a leading zero to the hours value
		jQuery("#hours").html(( hours < 10 ? "0" : "" ) + hours);
    }, 1000);
	
	
	
	jQuery( ".modal" ).on('shown', function(){
		jQuery("#loading").hide();
	});

	jQuery('[data-toggle="modal"]').live("click",function(){
		var id = this.id;
		if(id > 0){
			jQuery.ajax({
				url: rt_theme_params.ajax_url,  
				cache: false,  
				data: {
					'action':'documents_ajax_request',
					'keyword' : id
				},
				success:function(data) {
					if(!jQuery('#docEditMbox'+id).hasClass('in')){
						jQuery('#docEditMbox'+id).hide();
						jQuery(data).modal();
					}
				},
				beforeSend: function() {  jQuery("#loading").show(); },
				complete: function() { jQuery("#loading").hide(); }   
			});  
		}
	});
	
	jQuery(function() {
		jQuery("#successMessage").delay(5000).fadeOut('slow');
	});
	
	jQuery("#docedit").live("click",function(){
		jQuery(".docedits").modal('hide');
		jQuery( '.modal' ).remove();
		jQuery( '.modal-backdrop' ).remove();
	});
	
	jQuery("#add_new").click(function(){ 
		jQuery('.new').show();
	});
});


/*
* View patient Document
*/
/* jQuery(document).ready(function($) {
	$(".patient-show-ducument").live("click", function(){ });
}); */


function patient_show_document(current_class){
	var path = jQuery(current_class).attr('data-path');
	var newdesc = jQuery('.doc-desci').val();
	var doctype = jQuery('.doc-type').val();
	
	jQuery('.cdrmed-modal-box-content .docu-type').html(doctype);
	jQuery('.cdrmed-modal-box-content .mbox-doc-desc').html(newdesc);
	jQuery('.cdrmed-modal-box-content .fdlinks').attr("href", path);
	var ext = path.split('.').pop().toLowerCase();
	if(ext == 'pdf') {
		$('.cdrmed-modal-box-content .cdrmed-modal-box .modal-body .body-contents .view-document').html("<iframe class='doc-frame' src='http://docs.google.com/gview?url=" + path + "&embedded=true' style='width:600px; height:500px;' frameborder='0'></iframe>");
		//jQuery('#img').html("<iframe class='doc-frame' src='http://docs.google.com/gview?url=" + $path + "&embedded=true' style='width:600px; height:500px;' frameborder='0'></iframe>");
	}else{
		if($.inArray(ext, ['gif','png','jpg','jpeg']) == -1) {
			$('.cdrmed-modal-box-content .cdrmed-modal-box .modal-body .body-contents .view-document').html('<iframe class="doc-frame" src="https://view.officeapps.live.com/op/embed.aspx?src='+path+'&embedded=true"></iframe>');
			//jQuery('#img').html('<iframe class="doc-frame" src="https://view.officeapps.live.com/op/embed.aspx?src='+$path+'&embedded=true"></iframe>');
		}else{
			$('.cdrmed-modal-box-content .cdrmed-modal-box .modal-body .body-contents .view-document').html('<img src="'+path+'" />');
			//jQuery('#img').html('<img src="'+$path+'" />');
		}
	}
}




/*
==================================================================================
		ACF
==================================================================================
*/


jQuery(document).ready(function(e) {
	
	
	$("select.intake-code").change(function(){
		$(this).find("option:selected").each(function(){
			if($(this).attr("value")=="patient-info"){
				$(".box").not("#patient-info").hide();
				$("#patient-info").show();
			}
			else if($(this).attr("value")=="contacts"){
				$(".box").not("#contacts").hide();
				$("#contacts").show();
			}
			else if($(this).attr("value")=="diagnosis"){
				$(".box").not("#diagnosis").hide();
				$("#diagnosis").show();
			}
			else if($(this).attr("value")=="contacts"){
				$(".box").not("#contacts").hide();
				$("#contacts").show();
			}
			else if($(this).attr("value")=="treatment"){
				$(".box").not("#treatment").hide();
				$("#treatment").show();
			}
			else if($(this).attr("value")=="medications"){
				$(".box").not("#medications").hide();
				$("#medications").show();
			}
			else if($(this).attr("value")=="family"){
				$(".box").not("#family").hide();
				$("#family").show();
			}
			else if($(this).attr("value")=="cannabis"){
				$(".box").not("#cannabis").hide();
				$("#cannabis").show();
			}
			else if($(this).attr("value")=="nutrition"){
				$(".box").not("#nutrition").hide();
				$("#nutrition").show();
			}
			else if($(this).attr("value")=="military"){
				$(".box").not("#military").hide();
				$("#military").show();
			}
			else if($(this).attr("value")=="comments"){
				$(".box").not("#comments").hide();
				$("#comments").show();
			}
			else{
				$(".box").show();
			}
		});
	}).change();
	
	
	
	jQuery(".acf-form").on('submit', function(e){
		var femail = jQuery('.acf_first_email input[type=email]').val();
		var cemail = jQuery('.acf_confirm_email input[type=email]').val();
		if(femail != cemail){
			e.preventDefault();
			if ($('.acf-form .acf-error-message').length){}
			else{
				$(this).prepend('<div class="acf-error-message"><p>Validation failed.</p><a href="#" class="acf-icon -cancel small"></a></div>');
			}
			return false;
		}
	});
	
	
	jQuery('.cannbi .acf-label label').append('<span class="fa fa-question round-info" data-toggle="tooltip" title="What is your primary diagnosis?"></span>');
	jQuery('.icd .acf-label label').append('<span class="fa fa-question round-info" data-toggle="tooltip" title=" This is the statistical classification for your condition used by medical professionals. You are not required to answer this question."></span>');
	jQuery('.dte .acf-label label').append('<span class="fa fa-question round-info" data-toggle="tooltip" title="When were you first diagnosed with your disease or condition?"></span>');
	jQuery('.duration .acf-label label').append('<span class="fa fa-question round-info" data-toggle="tooltip" title=" How long has it been since you were diagnosed with your primary disease or condition?"></span>');
	
});

//Disable second tab for pre registration form 
jQuery('.interest select').live('change', function() {
	if( this.value == 'Consultation (Available Worldwide)'){
		jQuery('ul.acf-tab-group li:nth-child(2)').hide();
		jQuery('.pname .acf-input input').val('N/A');
	}else{
		jQuery('.pname .acf-input input').val('');
		jQuery('ul.acf-tab-group li:nth-child(2').show();
	}
});


// code for masks like phone number and dates
jQuery(function(jQuery){
	jQuery('.phone .acf-input input').inputmask("(999) 999-9999");
	jQuery('.mdates .acf-input input').inputmask("mm/dd/yyyy");

	// Remove empty tds and trs from site
	jQuery("td:empty").remove();
	jQuery("tr:empty").remove();
});

// Date Input Mask Validation
jQuery(document).on("keyup", '.mdates .acf-input input', function(e) {
	if(e.keyCode !== 37){
		jQuery('.mdates .acf-input input').inputmask("mm/dd/yyyy");
	}
});

jQuery(document).on("keyup", '.cdrmed_shoop_date', function(e) {
	jQuery('.cdrmed_shoop_date').inputmask("mm/dd/yyyy");
});