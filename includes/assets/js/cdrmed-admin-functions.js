
var $ = jQuery;


/*
==================================================================================
		Medications & Diagnosis
==================================================================================
*/

$(document).ready(function(){
	$('#medication_tr1').hide();
	$('#medication_tr2').hide();
	$('#diagnosis_tr1').hide();
	$('#diagnosis_tr2').hide();
	
	$(".all_medication_select").click(function () {
		$(".mediaction_check").prop('checked', $(this).prop("checked"));
	});
	
	$(".all_diagnosis_select").click(function () {
		$(".diagnosis_check").prop('checked', $(this).prop("checked"));
	});
	
	
});

$('.all_medication_select').live('click', function() {
});
$('.medication_select').live('change', function() {
	var id = $(this).data('id');
	if( $('.medication_select option:selected').text() == '--New Category--'){
		$('#medication_tr'+id).show();
	}
	else{
		$('#medication_tr'+id).hide();
	}
});

$('.diagnosis_select').live('change', function() {
	var id = $(this).data('id');
	if( $('.diagnosis_select option:selected').text() == '--New Category--'){
		$('#diagnosis_tr'+id).show();
	}
	else{
		$('#diagnosis_tr'+id).hide();
	}
});

$('.p450alert-checkbox').live('click', function() {
 this.checked?$('#p450-alert-category').show(1000):$('#p450-alert-category').hide(1000);

	/* if($(this).checked){
		$("#p450-alert-category").show();
	}
	else{
		$("#p450-alert-category").hide();
	} */
});




/*
==================================================================================
		Activity Log
==================================================================================
*/

$(document).ready(function(){
	
	// check all activity log
	$(".all_activity_log_select").click(function () {
		$(".activity_log_check").prop('checked', $(this).prop("checked"));
	});
	
	// search by email activity log
	jQuery('.activity-log-email-search').keyup(function(){
		var email = jQuery(this).val();
		$(".cdrmed-activity-log table tbody tr").show();
		if(email){
			$(".cdrmed-activity-log table tbody tr .email-search").not("[data-email*="+ email +"],[data-email2*="+ email +"]").parent().parent().parent().hide();
		}
	});
	
	jQuery('.activity-log-email-search2').keyup(function(){
		var email = jQuery(this).val();
		$(".single-user-activity-log .single-activity").show();
		if(email){
			$(".single-user-activity-log .single-activity").not("[data-email*="+ email +"]").hide();
		}
	});
});


/* date picker for search by date */
$('body').on('focus',".activity-log-date-search", function(){
   
	$(this).daterangepicker({
		 "locale": {
			"format": "MM/DD/YYYY",
			"separator": " - ",
			"applyLabel": "Apply",
			"cancelLabel": "Cancel",
			"fromLabel": "From",
			"toLabel": "To",
			"customRangeLabel": "Custom",
			"daysOfWeek": [
				"Su",
				"Mo",
				"Tu",
				"We",
				"Th",
				"Fr",
				"Sa"
			],
			"monthNames": [
				"January",
				"February",
				"March",
				"April",
				"May",
				"June",
				"July",
				"August",
				"September",
				"October",
				"November",
				"December"
			],
			"firstDay": 1
		}
	});
});

// search activity log by date
$(document).on("click", '.search-by-date-search-btn', function(event) {
	var search = $('.activity-log-date-search').val();
	var date1 = $.trim(search.split('-')[0]);
	var date2 = $.trim(search.split('-')[1]);
	
	$(".single-user-activity-log .single-activity").show();
	if(date1 && date2){
		$( ".single-user-activity-log .single-activity" ).each( function( index, element ){
			var activity_date = $(this).attr('data-date');
			// if activity date is less or greater from search date than hide this block
			if(activity_date < date1 || activity_date > date2){
				$(this).hide();
			}
		});
		
		//$(".single-user-activity-log .single-activity").not("[data-date*="+ date1 +"],[data-email2*="+ email +"]").parent().parent().hide();
	}
	
});

