
var $ = jQuery;

jQuery(function() {
    jQuery( "#tabs" ).tabs();
  });

jQuery(document).ready(function() {
    var max_fields      = 60; //maximum input boxes allowed
    var wrapper         = jQuery(".input_fields_wrap"); //Fields wrapper
    var add_button      = jQuery(".add_field_button"); //Add button ID

    var wrapper2         = jQuery(".input_fields_wrap2"); //Fields wrapper
    var add_button2      = jQuery(".add_field_button2"); //Add button ID

    var wrapper3         = jQuery(".input_fields_wrap3"); //Fields wrapper
    var add_button3      = jQuery(".add_field_button3"); //Add button ID
    
    var x = 1; //initlal text box count
    jQuery(add_button).click(function(e){ //on add input button click
        e.preventDefault();
        if(x < max_fields){ //max input box allowed
            x++; //text box increment

            jQuery(wrapper).after('<tr><td><input type="text" class="form-control frst" name="_azcl_compunds[]"/><span class="scnd"><input type="text" class="form-control scnd2" name="_azcl_potency[]"/></span></td><td><input type="text" class="form-control tp2" name="_azcl_potency_tests[]"/></td><td><input type="text" class="form-control tp2" value="0.001" name="_azcl_potency_ppm[]"/></td></tr><tr><td colspan="4"><a href="javascript:void(0);" class="remove_field">Remove</a></tr>'); //add input box
        }
		return false;
    });

      jQuery(add_button2).click(function(e){ //on add input button click
        e.preventDefault();
        if(x < max_fields){ //max input box allowed
            x++; //text box increment
            jQuery(wrapper2).after('<tr><td><input type="text" class="form-control frst" name="_azcl_compunds_terpene[]"/><span class="scnd"><input type="text" class="form-control scnd2" name="_azcl_terpene[]"/></span></td><td><input type="text" class="form-control tp2" name="_azcl_terpene_tests[]"/></td><td><input type="text" class="form-control tp2" value="0.001" name="_azcl_terpene_ppm[]"/></td></tr><tr><td colspan="4"><a href="javascript:void(0);" class="remove_field2">Remove</a></td></div></tr>'); //add input box
        }
    });

   jQuery(add_button3).click(function(e){ //on add input button click
        e.preventDefault();
        if(x < max_fields){ //max input box allowed
            x++; //text box increment
            jQuery(wrapper3).after('<tr><td><input type="text" class="form-control frst" name="compunds_residual[]"/><span class="scnd"><input type="text" class="form-control scnd2" name="residual[]"/></span></td><td><input type="text" class="form-control tp2" name="residual_tests[]"/></td><td><input type="text" class="form-control tp2" value="0.001" name="residual_ppm[]"/></td></tr><tr><td colspan="4"><a href="javascript:void(0);" class="remove_field3">Remove</a></td></div></tr>'); //add input box
        }
    });

jQuery(document).on("click",".remove_field", function (e) {
   // jQuery(".remove_field").click(function(e){ //user click on remove text
        e.preventDefault(); 
        var row = jQuery(this).closest('tr');
        var prev = row.prev();
        prev.remove();
        jQuery(this).closest("tr").remove();
         jQuery(this).parent('tr').remove();

          x--;
    });
    jQuery(document).on("click",".remove_field2", function (e) { //user click on remove text
        e.preventDefault();
         var row = jQuery(this).closest('tr');
        var prev = row.prev();
        prev.remove();
        jQuery(this).closest("tr").remove();
         jQuery(this).parent('tr').remove();
          x--;
    });

    jQuery(document).on("click",".remove_field3", function (e) { //user click on remove text
        e.preventDefault(); 
       var row = jQuery(this).closest('tr');
        var prev = row.prev();
        prev.remove();
        jQuery(this).closest("tr").remove();
         jQuery(this).parent('tr').remove();

         x--;
    })
});
