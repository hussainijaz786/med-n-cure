
var $ = jQuery;


/*
*
*	Add New Diagnosis
*
*/
$(document).on("click", '.add_new_diagnosis_btn', function(event) {
	$('.add_new_diagnosis_btn1').removeClass('add_new_diagnosis_btn');
	jQuery("#loading").show();
	var data = {
		'action':'add_diagnosis_ajax',
		'keyword' : jQuery('.cannbi input').val()
	};
	jQuery.post(cdrmedajax.ajax_url, data, function(response) {
		jQuery("#loading").hide();
		// This outputs the result of the ajax request
		jQuery('.cannbi').before("<div class='success autocomplete__message' id='successMessage'>"+response+"</div>");
		jQuery("#result").remove();
		setTimeout(function() {
			$('.add_new_diagnosis_btn').removeAttr('disabled');
			jQuery(".autocomplete__message").hide();
		}, 3000);
	});
});


jQuery(document).ready(function() {
	
	$('.satges').addClass('hidden-by-tab');
	
	function monkeyPatchAutocomplete() {
		jQuery("#result").remove();
		// Don't really need to save the old fn, 
		// but I could chain if I wanted to
		var oldFn = jQuery.ui.autocomplete.prototype._renderItem;

		jQuery.ui.autocomplete.prototype._renderItem = function( ul, item) {
			var re = new RegExp( "\\b" + this.term, "i") ;
			var t = item.label.replace(re,"<span class='highlighted'>" + this.term + "</span>");
			return jQuery( "<li></li>" )
				.data( "item.autocomplete", item )
				.append( "<a>" + t + "</a>" )
				.appendTo( ul );
		};
	}
	
	function sendto(){ 
		// This does the ajax request
		jQuery("#loading").show();
		jQuery.ajax({
			url: ajaxurl,
			data: {
				'action':'diagnosis_ajax_request',
				'keyword' : jQuery('.cannbi input').val()
			},
			success:function(data) {
				jQuery("#loading").hide();
				// This outputs the result of the ajax request
				jQuery('.cannbi').before("<div class='success' id='successMessage'>"+data+"</div>");
				jQuery("#result").remove();
				setTimeout(function() {
					jQuery("#successMessage").hide('blind', {}, 500)
				}, 5000);
			},
			error: function(errorThrown){
				//jQuery("#loading").hide();
				//console.log(errorThrown);
			}
		});   
	}
	
	var data = {
		action: 'diagnosis_auto_complete_get_locations_ajax_call'
	};
	
	var icd;
	var locationsd = '';
	var locations_arrayd = '';
	jQuery.post(cdrmedajax.ajax_url, data, function(response) {
		
		response = JSON.parse(response)
		icd = response.icd;
		locationsd = response.json;
		locations_arrayd = JSON.parse(locationsd);
		locations_arrayd.sort();
		
	});
	
	
	jQuery('.auto_shoop_diagnosis .acf-input-wrap input').live('keyup', function (event) {
		if(this.value.length < 2){
			jQuery("#result").remove();
		}
		
		monkeyPatchAutocomplete();
		jQuery( ".auto_shoop_diagnosis .acf-input-wrap input" ).autocomplete({
			matchContains: true,
			minLength: 2,
			messages: {
				noResults: function(count) {
				},
			},
			source: function(req, responseFn) {
				//addMessage("search on: '" + req.term + "'<br/>");
				var re = jQuery.ui.autocomplete.escapeRegex(req.term);
				var matcher = new RegExp(  "\\b" + re, "i" );
				var a = jQuery.grep( locations_arrayd, function(item,index){
				  //addMessage("&nbsp;&nbsp;sniffing: '" + item + "'<br/>");
				  return matcher.test(item);
				});
				if(!a.length) {

					jQuery("#result").remove();
					jQuery(".auto_shoop_diagnosis .acf-input-wrap input").after(' <div id="result"></div>');
					//jQuery("#result").show();
					jQuery("#result").html('');
			  
				} else {
					jQuery("#result").remove();
				}

				// response(results);
				  responseFn( a );
			},
			focus: function (event, ui) {
				//jQuery(event.target).val(ui.item.label);
				jQuery("#result").remove();
				return false;
			},
		});
		
		
	});
	
	jQuery('.cannbi .acf-input-wrap input, input.cannbi').live('keyup', function (event) {
		
		if(this.value.length < 2){
			jQuery("#result").remove();
		}
		
		var str_data = $(this).val();
		if (str_data.toLowerCase().indexOf("cancer") < 0){
			//jQuery( ".satges" ).attr("style", "display: none");
			$('.satges').addClass('hidden-by-tab');
		}
	
		monkeyPatchAutocomplete();
		jQuery( ".cannbi .acf-input-wrap input, input.cannbi" ).autocomplete({
			matchContains: true,
			minLength: 2,
			messages: {
				noResults: function(count) {
				},
			},
			source: function(req, responseFn) {
				//addMessage("search on: '" + req.term + "'<br/>");
				var re = jQuery.ui.autocomplete.escapeRegex(req.term);
				var matcher = new RegExp(  "\\b" + re, "i" );
				var a = jQuery.grep( locations_arrayd, function(item,index){
				  //addMessage("&nbsp;&nbsp;sniffing: '" + item + "'<br/>");
				  return matcher.test(item);
				});
				if(!a.length) {

					jQuery("#result").remove();
					jQuery(".cannbi .acf-input-wrap input, input.cannbi").after(' <div id="result"></div>');
					//jQuery("#result").show();
					jQuery("#result").html('<p class="no-result">Diagnosis could not be found in our database   <a class="add_new_diagnosis_btn1 add_new_diagnosis_btn" style="cursor: pointer;"> Add Diagnosis</a><p>');
				} else {
					jQuery("#result").remove();
				}

				// response(results);
				  responseFn( a );
			},
			focus: function (event, ui) {
				//jQuery(event.target).val(ui.item.label);
				jQuery("#result").remove();
				return false;
			},
		});
	
	
		jQuery( ".cannbi .acf-input-wrap input, input.cannbi" ).on( "autocompleteselect", function( event, ui ) {
			//jQuery('.medication .acf-input-wrap input').after('<input type="hidden" class="medi" id="medi" name="medication[]" value="'+ui.item.value+'" />');
			var codes = icd;
			var my_cidcodes = JSON.parse(codes);
			var str = ui.item.value;
			if (str.toLowerCase().indexOf("cancer") >= 0){
				//jQuery( ".satges" ).show();
				//jQuery( ".satges" ).attr("style", "display: inline;z-index:999");
				$('.satges').removeClass('hidden-by-tab');
			}else{
				//jQuery( ".satges" ).attr("style", "display: none");
				$('.satges').addClass('hidden-by-tab');
			}
			if(my_cidcodes[ui.item.value] != ''){
				jQuery('#acf-field_56c2ebaac5f9a').val(my_cidcodes[ui.item.value]);
			}else{
				jQuery('#acf-field_56c2ebaac5f9a').val(my_cidcodes[ui.item.value]);
				jQuery( "#acf-field_56c2ebaac5f9a" ).attr("placeholder", "Enter if known");
			}
			console.log(my_cidcodes[ui.item.value]);
		});
		
		
			
			
		
	});
});
