
var $ = jQuery;



/*
*
*	Add New Medication
*
*/
$(document).on("click", '.add_new_medication_btn', function(event) {
	$('.add_new_medication_btn1').removeClass('add_new_medication_btn');
	jQuery("#loading").show();
	var data = {
		'action':'add_medication_ajax',
		'keyword' : jQuery('.medication input').val()
	};
	jQuery.post(cdrmedajax.ajax_url, data, function(response) {
		jQuery("#loading").hide();
		jQuery('#result').hide();
		// This outputs the result of the ajax request
		jQuery('.acf-table').before("<div class='success autocomplete__message' id='successMessage'>"+response+"</div>");
		setTimeout(function() {
			//jQuery("#successMessage").hide('blind', {}, 500)
			jQuery(".autocomplete__message").hide();
		}, 3000);
	});
});



jQuery(document).ready(function() {
	
	jQuery('.medication .acf-input-wrap input, input.medication').append('<input type="hidden" class="medi" name="medication[]" value="" />');
	function monkeyPatchAutocomplete2() {
		jQuery("#result").remove();
		// Don't really need to save the old fn, 
		// but I could chain if I wanted to
		var oldFn = jQuery.ui.autocomplete.prototype._renderItem;

		jQuery.ui.autocomplete.prototype._renderItem = function( ul, item) {
			var re = new RegExp( "\\b" + this.term, "i") ;
			var t = item.label.replace(re,"<span class='highlighted'>" + this.term + "</span>");
			return jQuery( "<li></li>" )
				.data( "item.autocomplete", item )
				.append( "<a>" + t + "</a>" )
				.appendTo( ul );
		};
	}
	
	function medication_sendto(){
		// This does the ajax request
		jQuery("#loading").show();
		jQuery.ajax({
			url: ajaxurl,
			data: {
				'action':'medication_ajax_request',
				'keyword' : jQuery('.medication input').val()
			},
			success:function(data) {
				jQuery("#loading").hide();
				jQuery('#result').hide();
				// This outputs the result of the ajax request
				jQuery('.acf-table').before("<div class='success' id='successMessage'>"+data+"</div>");
				setTimeout(function() {
					jQuery("#successMessage").hide('blind', {}, 500)
				}, 5000);
			},
			error: function(errorThrown){
			}
		});   
	}
	
	var data = {
		action: 'medication_auto_complete_get_locations_ajax_call'
	};
	var locations = '';
	var locations_array = '';
	jQuery.post(cdrmedajax.ajax_url, data, function(response) {
		locations = response;
		locations_array = JSON.parse(locations);
		locations_array.sort();
	});
	
	jQuery('.medication .acf-input-wrap input, input.medication').live('keyup', function (event) {
		
		if(this.value.length < 2){
			jQuery("#result").remove();
		}

		monkeyPatchAutocomplete2();
		//jQuery("#result").remove();
		jQuery( ".medication .acf-input-wrap input, input.medication" ).autocomplete({
			matchContains: true,
			multiselect: true,
			minLength: 2,
			source: function(req, responseFn) {
				//addMessage("search on: '" + req.term + "'<br/>");
				var re = jQuery.ui.autocomplete.escapeRegex(req.term);
				var matcher = new RegExp(  "\\b" + re, "i" );
				var a = jQuery.grep( locations_array, function(item,index){
					//addMessage("&nbsp;&nbsp;sniffing: '" + item + "'<br/>");
					return matcher.test(item);
				}); 
				if(!a.length) {
					var input = this.element;
					jQuery("#result").remove();
					jQuery("#result").hide();
					jQuery(input).after('<div id="result"><p class="no-result">Medication could not be found in our database  <a class="add_new_medication_btn1 add_new_medication_btn" style="cursor: pointer;"> Add Medication</a><p></div>');
					//jQuery("#result").show();
					// jQuery("#result").html("<p class='no-result'>No Diagnosis Matched the keywords   <a href='<?php echo $current_uri;?>?keyword="+jQuery('.cannbi input').val()+"'> Add This Keyword</a><p>");
				} else {
					jQuery("#result").remove();
				}

				// response(results);
				responseFn( a );
			},
			focus: function (event, ui) {
				jQuery("#result").remove();
				return false;
			},
		});
		jQuery( ".remove .-plus" ).on( "click", function() {
			jQuery("#result").remove();
		});
		jQuery( ".medication .acf-input-wrap input, input.medication" ).on( "autocompleteselect", function( event, ui ) {
			//jQuery('.medication .acf-input-wrap input, input.medication').after('<input type="hidden" class="medi" id="medi" name="medication[]" value="'+ui.item.value+'" />');
			//console.log(ui.item.value);
		});
			
			
			
		
	});
});