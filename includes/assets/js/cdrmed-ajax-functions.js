
var $ = jQuery;



/*
==================================================================================
		General
==================================================================================
*/

//  ajax calls for cron job testing

/* $(document).on("click", '.send-medicine-notification', function(event) {
	var data = {
		action: 'send_medicine_notification'
	};
	
	jQuery.post(cdrmedajax.ajax_url, data, function(response) {
		console.log(response)
	});
});

$(document).on("click", '.send-health-journey', function(event) {
	var data = {
		action: 'send_email_daily'
	};
	
	jQuery.post(cdrmedajax.ajax_url, data, function(response) {
		console.log(response)
	});
}); */

var check_calls = null;


$(document).on("click", '.load-cdrmed-modal-box', function(event) {
	
	//$(this).attr('disabled','disabled');
	//$(this).addClass('already-clicked');
	
	//var original_html = $(this).html();
	//$(this).html('waiting...');
	var current_class = $(this);
	
	$('body').addClass('rt-loading');
	$('body').append('<div id="loader-wrapper"><div id="loader"></div></div>');
	
	//action for find shortcode
	var modal_action = $(this).attr('modal-action');
	//modal box title
	var modal_title = $(this).attr('modal-title');
	//true/false
	var modal_footer = $(this).attr('modal-footer');
	var call_action = $(this).attr('data-call_action');
	
	//var last_click = $(this);
	
	var data = {
		action: 'load_cdrmed_modal_ajax',
		'modal_action': modal_action,
		'modal_footer': modal_footer,
	};
	
	jQuery.post(cdrmedajax.ajax_url, data, function(response) {
			//load modal box body
			$('<div class="cdrmed-modal-box-content">'+response+'</div>').modal();
			//set modal title
			$(".cdrmed-modal-box-content .cdrmed-modal-box .modal-title").html(modal_title);
			//show modal box
			$(".cdrmed-modal-box-content .cdrmed-modal-box").show();
			
			$('body').removeClass('rt-loading');
			$('body #loader-wrapper').remove();
			
			if(call_action == 'edit_child_profile_modal'){
				edit_child_profile_modal(current_class);
			}
			else if(call_action == 'show_mmj_recommendation'){
				show_mmj_recommendation(current_class);
			}
			else if(call_action == 'appointment_view_modal'){
				appointment_view_modal(current_class);
			}
			else if(call_action == 'api_view_modal'){
				api_view_modal(current_class);
			}
			else if(call_action == 'patient_dispensary_model_show'){
				patient_dispensary_model_show(current_class);
			}
			else if(call_action == 'patient_physician_model_show'){
				patient_physician_model_show(current_class);
			}
			else if(call_action == 'patient_edit_document'){
				patient_edit_document(current_class);
			}
			else if(call_action == 'send_email_to_patient_modal'){
				send_email_to_patient_modal(current_class);
			}
			else if(call_action == 'patient_show_document'){
				patient_show_document(current_class);
			}
			else if(call_action == 'physician_appointment_view_modal'){
				physician_appointment_view_modal(current_class);
			}
			
			//eval(call_action+"_func("+current_click_class+")");
			//$(current_click_class).html(original_html);
			//$(current_click_class).attr('data-load','true');
		
	});
	
	
	
});


/*
*
*	Close Modal Box
*
*/
$(document).on("click", '.cdrmed-modal-box-content .cdrmed-modal-box .close', function(event) {
	jQuery(".cdrmed-modal-box-content").remove();
	jQuery(".cdrmed-modal-box-content .cdrmed-modal-box").hide();
});


jQuery(document).ready(function(){
	
	/*
	*
	*	Load user Notifications
	*
	*/
	var data = {
		action: 'load_cdrmed_notifications_ajax'
	};
	jQuery.post(cdrmedajax.ajax_url, data, function(response) {
		var response = jQuery.parseJSON(response);
		if(response.action){
			$('body').append(response.output);
		}
		
	});
	
	
	/**
	* Code for cloning user into child site
	*/
	jQuery('#patientCloneBtn').click(function(){
		var patient_id = jQuery(this).attr('data-pid');
		var security_key = jQuery(this).attr('data-security');
		
		var data = {
			'action': 'cdrmed_clone_user_ajax',
			'patient_id': patient_id,
			'security': security_key,
		};
		$.post(wc_add_to_cart_params.ajax_url, data, function(returned_data, textStatus, xhr) {
			//update post ids of newly added posts and user
			send_second_request(patient_id, security_key, returned_data);			
		});
		
	});
	
	jQuery('#new_category_btn').click(function(e){
		e.preventDefault();
		var new_cat = $("#new_category").val();
		if(new_cat == ''){
			return false;
		}
		var cat_parent = $("#all_category_select option:selected").val();
		var data = {
			action: 'product_new_category_request',
			'new_cat': new_cat,
			'cat_parent': cat_parent
		};
		jQuery.post(cdrmedajax.ajax_url, data, function(response) {
			$(response).insertAfter('.parent'+cat_parent);
			$("#new_category").val('');
		});
		
	});
	
	
	
	// MednCures Ajax Call for Updating Patient's Deceased or Alive Status
	$("#patientlifestatus").bind("change",function(){
		$("#patient-message").show();
		if($(this).is(":checked")) {
			// set value for ajax
			$(".patient-death-date, #update-death").fadeIn();
		}
		else{
			$(".patient-death-date, #update-death").fadeOut();
			var patientid = $(this).attr("data-patient");
			var data = {
				'action': 'mark_patient_dead',
				'patientsid': patientid,
				'deathdate': '',
				'patientlife': 'alive'
			};
			jQuery.post(wc_add_to_cart_params.ajax_url, data, function(response) {
				//alert('Got this from the server: ' + response);
				//$("#patient-message").append('<div id="feedback">'+response+'</div>');
				$("#patient-message").html('<div id="feedback">'+response+'</div>');
			});
		}
	
		// Get Date from the Input Field on Click of Update Patient Button
		$('#update-death').click(function(e) {
			var deathdate = $('input[name="deathdate"]').val();
			if(!deathdate.match(/\S/)) {
				alert ('Please Enter Date of Death');
				return false;
			} else {
				var patientid = $(this).attr("data-patient");
				var data = {
					'action': 'mark_patient_dead',
					'patientsid': patientid,
					'deathdate': deathdate,
					'patientlife': 'dead'
				};
				jQuery.post(wc_add_to_cart_params.ajax_url, data, function(response) {
					//alert('Got this from the server: ' + response);
					$("#patient-message").append('<div id="feedback">'+response+'</div>');
				});
			}
		});
	});
});

function send_second_request(patient_id, security_key, returned_data){
	var data = {
		'action': 'cdrmed_after_clone_user_ajax',
		'patient_id': patient_id,
		'security': security_key,
		'data': returned_data,
	};
	jQuery.post(wc_add_to_cart_params.ajax_url, data, function(data, textStatus, xhr) {
	});
}

(function($) {
    $('#form-import').live('click', function (event) { 
        $import_true = confirm('are you sure to import Pages content ? it will overwrite the existing data');
        if($import_true == false)
			return;
        $('.redux-section-desc').html(' Data is being imported please be patient, while the awesomeness is being created :)  ');
		var data = {
			'action': 'my_action'       
		};
		// since 2.8 ajaxurl is always defined in the admin header and points to admin-ajax.php
		$.post(ajaxurl, data, function(response) {
			if(response == 0){
				$('.redux-section-desc').html('<div class="import_message_success" style="    height: 20px; padding: 10px;color: #4F8A10;background-color: #DFF2BF;">Data Imported Successfully..</div>');
			}else{
				$('.redux-section-desc').html('<div class="import_message_success">'+ response +'</div>');
			}
			setTimeout(function() {
				$('.import_message_success').fadeOut('fast');
			}, 5000);
		});
	});
})(jQuery);


/*
*
*	Action for user Notifications
*
*/
$(document).on("click", '.notification-panel-area .notification-btn', function(event){
	if($('.notification-panel-area .notification-btn').hasClass('no-radius')){
		$('.notification-panel-area .notification-btn').removeClass('no-radius');
		$('.notification-panel-area .single-notification').hide();
	}
	else{
		$('.notification-panel-area .notification-btn').addClass('no-radius');
		$('.notification-panel-area .single-notification').show();
	}
});

$(document).on("click", '.notification-panel-area .notification-click', function(event){	
	$('.notification-panel-area .notification-btn').removeClass('no-radius');
	$('.notification-panel-area .single-notification').hide();
	var notification = $(this).attr('data-notification');
	var url = $(this).attr('url');
	$(this).remove();
	var data = {
		action: 'remove_cdrmed_notifications_ajax',
		'notification': notification
	};
	jQuery.post(cdrmedajax.ajax_url, data, function(response) {
		if (url.match('#')){
			window.location.hash = url.split('#')[1];
			window.location.reload();
		}
		else{
			window.location.href = url;
		}
	});
});



/*
*
*	Import patient
*
*/
//show group
$(document).on("click", '.import_patient_show_group', function(event) {
	$('.import-patient-id').val('');
	$('.import-patient-group').css('display','inline');
});
//hide group
$(document).on("click", '.import_patient_cancel', function(event) {
	$('.import-patient-group').css('display','none');
});
//send requiest to patient
$(document).on("click", '.import_patient_send', function(event) {
	var patient_id = $('.import-patient-id').val();
	if(patient_id == ''){
		return false;
	}
	$('.import_patient_show_group').html('waiting...');
	$('.import_patient_show_group0').removeClass('import_patient_show_group');
	$('.import-patient-group').css('display','none');
	
	//call from patient-function/import-patient-and-intake-active-ajax-call.php
	var data = {
		action: 'import_patient_ajax',
		'patient_id': patient_id,
	};
	jQuery.post(cdrmedajax.ajax_url, data, function(response) {
		var response = jQuery.parseJSON(response);
		$('.import_patient_show_group0').html(response.output);
		if(response.action){
			$('.import-patient-id').val('');
			$('.import-patient-group').css('display','none');
		}
		else{
			$('.import-patient-group').css('display','inline');
		}
		setTimeout( function() {
			$('.import_patient_show_group0').addClass('import_patient_show_group');
			$('.import_patient_show_group0').html('Import Patient');
		}, 2000);
	});
});

$(document).on("click", '.import_request_action', function(event) {
	var user_id = $(this).data('user_id');
	var user_action = $(this).data('action');
	var data = {
		action: 'approve_reject_import_request_ajax',
		'user_id': user_id,
		'user_action': user_action,
	};
	jQuery.post(cdrmedajax.ajax_url, data, function(response) {
		$('.import-request-feedback').html(response);
		$('.import-request-feedback').show();
		setTimeout( function() {
			window.location.reload();
		}, 2000);
	});
	
});


//Call for active Patient
$(document).on("click", '.active_patient_account', function(event) {
	var post_id = $(this).data('post_id');
	var email_id = $(this).data('email_id');
	//call from patient-function/import-patient-and-intake-active-ajax-call.php
	var data = {
		action: 'intake_active_ajax',
		'post_id': post_id,
		'email_id': email_id,
	};
	jQuery.post(cdrmedajax.ajax_url, data, function(response) {
		var response = jQuery.parseJSON(response);
		$('.pre_reg_feedback').addClass('success-message');
		$('.pre_reg_feedback').html(response.output);
		if(response.action){
			setTimeout( function() {
				window.location.hash = 'patients';
				window.location.reload();
			}, 2000);
		}
		else{
			
		}
		
	});
});







/*
==================================================================================
		API
==================================================================================
*/

// Unset Everything when Modal load
//$(document).on("click", '.api_view_modal', function(event) {
function api_view_modal(current_class){
	$('.cdrmed-modal-box-content .modal-footer').html('<a href="#" class="pull-left" id="api-help">Do you Need Help?</a><button type="button" class="btn btn-warning api_previous_btn">Go Back</button><button type="button" class="btn btn-success api_next_btn">Next</button>');
	
	$.each( $( "#api_modal .row" ), function() {
		$(this).addClass('hide');
	});
	$('#api_modal #s1').removeClass('hide');
	$('#api_modal #step-now').val('1');
	$('#api_modal #previous_btn').attr('disabled','disabled');
	$('#api_modal #title_span').html('Select One API');
	
	$('#api_modal #c_pos_api_token').removeClass('hide');
	$('#api_modal #c_pos_api_consumer').removeClass('hide');
	$('#api_modal #c_pos_api_secret').removeClass('hide');
	$('.cdrmed-modal-box-content .api_next_btn').removeAttr('data-dismiss');
	$('.cdrmed-modal-box-content .api_next_btn').addClass('hide');
	$('.cdrmed-modal-box-content .api_next_btn').html('Next');
}

$(document).on("click", '#api_modal .api', function(event) {
	next_process();
	$('.cdrmed-modal-box-content .api_next_btn').removeClass('hide');
});


function next_process(){
	var api = $('#api_modal .api:checked').val();
	var pos_system = $('#api_modal .api:checked').data('text');
	var last_api = $('#api_modal #last_api').val();
	
	api_keys_show_hide(api);
	var step = find_step('inc');
	$('#api_modal  #previous_btn').removeAttr('disabled');
	
	if(step == 2 && pos_system != last_api){
		$('#api_modal #c_pos_api_token').val('');
		$('#api_modal #c_pos_api_consumer').val('');
		$('#api_modal #c_pos_api_secret').val('');
	}
	else{
		
		$('.cdrmed-modal-box-content .api_status').html('<img style="display: inherit; margin: 5px auto;" src="'+cdrmedajax.img_url+'/loading-secure-animation.gif" alt="" />');
		
		var data = {
			action: 'api_keys_read_ajax'
		};
		jQuery.post(cdrmedajax.ajax_url, data, function(response) {
			var response = $.parseJSON(response);
			$('#api_modal #c_pos_api_token').val(response.token);
			$('#api_modal #c_pos_api_consumer').val(response.consumer_key);
			$('#api_modal #c_pos_api_secret').val(response.secret_key);
		});
	}
	
	if(step == 3){
		
		//take all keys from user
		//if user submit empty key then he will not able to process next step
		$('.cdrmed-modal-box-content .api_status').html('');
		var token = $('#api_modal #c_pos_api_token').val();
		if(!$("#api_modal #c_pos_api_token").hasClass("hide") && token == ''){
			find_step('dec');
			return false;
		}
		var consumer = $('#api_modal #c_pos_api_consumer').val();
		if(!$("#api_modal #c_pos_api_consumer").hasClass("hide") && consumer == ''){
			find_step('dec');
			return false;
		}
		var secret = $('#api_modal #c_pos_api_secret').val();
		if(!$("#api_modal #c_pos_api_secret").hasClass("hide") && secret == ''){
			find_step('dec');
			return false;
		}
		
		$('.cdrmed-modal-box-content .api_status').html('<img style="display: inherit; margin: 5px auto;" src="'+cdrmedajax.img_url+'/loading-secure-animation.gif" alt="" />');
		var data = {
			action: 'api_keys_save_ajax',
			'pos_system': pos_system,
			'token': token,
			'consumer': consumer,
			'secret': secret
		};
		
		jQuery.post(cdrmedajax.ajax_url, data, function(response) {
			var response = $.parseJSON(response);
			$('.cdrmed-modal-box-content .api_status').html(response.message);
			//$('.cdrmed-modal-box-content .api_status').removeClass();
			if(response.action == 'true'){
				$('.cdrmed-modal-box-content .api_status').addClass('success');
				$('.cdrmed-modal-box-content .api_next_btn').html('Finish');
				$('.cdrmed-modal-box-content .api_next_btn').attr('data-dismiss','modal');
			}
			else{
				$('.cdrmed-modal-box-content .api_status').addClass('fail');
			}
		});
	}
	
	if(step == 4){
		jQuery(".cdrmed-modal-box-content").remove();
		jQuery(".cdrmed-modal-box-content .cdrmed-modal-box").hide();
	}
}


// When user reach a step one everything set default
$(document).on("click", '.cdrmed-modal-box-content  .api_previous_btn', function(event) {
	var step = find_step('dec');
	$('#api_modal #status').html('');
	if(step == 1){
		$('#api_modal #previous_btn').attr('disabled','disabled');
		$('#api_modal #c_pos_api_token').removeClass('hide');
		$('#api_modal #c_pos_api_consumer').removeClass('hide');
		$('#api_modal #c_pos_api_secret').removeClass('hide');
		$('#api_modal #c_pos_api_token').val('');
		$('#api_modal #c_pos_api_consumer').val('');
		$('#api_modal #c_pos_api_secret').val('');
		$('.cdrmed-modal-box-content .api_next_btn').addClass('hide');
		//$('.api').attr('checked', false);
	}
	var api = $('#api_modal .api:checked').val();
	$('.cdrmed-modal-box-content .api_next_btn').removeAttr('data-dismiss');
	$('.cdrmed-modal-box-content .api_next_btn').html('Next');
});

$(document).on("click", '.cdrmed-modal-box-content .api_next_btn', function(event) {
	next_process();
});



// check for Next or Previous Button click
function find_step(symbol){
	var step = parseInt($('#api_modal #step-now').val());
	$('#api_modal #s'+step).addClass('hide');
	if(symbol == 'dec'){
		step = --step;
	}
	else if(symbol == 'inc'){
		step = ++step;
	}
	$('#api_modal #s'+step).removeClass('hide');
	$('#api_modal #step-now').val(step);
	if(step > 1){
		var api = $('#api_modal .api:checked').val();
		var title = $('#api_modal .api:checked').data('text');
		$('#api_modal #title_span').html(title + ' ' + $('#api_modal #s'+step).data('title'));
	}
	else{
		$('#api_modal #title_span').html('Select One API');
	}
	return step;
}


//show-hide keys base on api requirement
function api_keys_show_hide(api){
	if(api == 'api6'){
		$('#api_modal #c_pos_api_token').addClass('hide');
		$('#api_modal #c_pos_api_token').val('');
	}
	if(api == 'api7'){
		$('#api_modal #c_pos_api_consumer').addClass('hide');
		$('#api_modal #c_pos_api_secret').addClass('hide');
		$('#api_modal #c_pos_api_consumer').val('');
		$('#api_modal #c_pos_api_secret').val('');
	}
	if(api == 'api8'){
		$('#api_modal #c_pos_api_token').attr('placeholder','API ID');
		$('#api_modal #c_pos_api_consumer').attr('placeholder','API key');
		$('#api_modal #c_pos_api_secret').addClass('hide');
		$('#api_modal #c_pos_api_secret').val('');
	}
	if(api == 'api9'){
		$('#api_modal #c_pos_api_consumer').addClass('hide');
		$('#api_modal #c_pos_api_secret').addClass('hide');
		$('#api_modal #c_pos_api_consumer').val('');
		$('#api_modal #c_pos_api_secret').val('');
		$('#api_modal .api-alert-message').html('To get your Authentication Token, please Login to your ZOHO Account and <a href="https://accounts.zoho.com/apiauthtoken/create?SCOPE=ZohoInventory/inventoryapi" target="_blank">Click Here</a>');
	}
	else{
		$('#api_modal .api-alert-message').html('');
	}
	
	return;
}








// If dispensary Click this button then Api auto integrate data into system
$(document).on("click", '.dispensary-api-sync-btn', function(event) {
	if($('.dispensary-api-sync-btn').attr("disabled")!="disabled"){
		$('.dispensary-api-sync-btn').html('Synchronizing...');
		$('.dispensary-api-sync-btn').attr('disabled','disabled');
		
		var data = {
			action: 'update_dispensary_api_sync_ajax',
		};
		jQuery.post(cdrmedajax.ajax_url, data, function(response) {
			$('.dispensary-api-sync-btn').removeAttr('disabled');
			//var response = jQuery.parseJSON(response);
			$('.dispensary-api-sync-btn').html(response);
			
			//$('.dispensary-api-sync-btn').html('Sync Now');
			
			//jQuery("#user-custom-profile-feedback").html(response.message);
			//jQuery('#user-custom-profile-feedback').addClass('success');
			setTimeout( function() {
				
				//$(this).html('Sync Now');
				//jQuery("#user-custom-profile-feedback").html('');
				//jQuery('#user-custom-profile-feedback').removeClass('success');
				window.location.href = cdrmedajax.inventory_url;
			}, 5000);
			
		});
	}
});







/*
*
*		Patietn Side Dispensary List
*
*
*/


//Add New dispensaries in Patient
$(document).on("click", '.add_patient_to_dispensary_list', function(event) {
	var patient = $(this).data('patient');
	var data = {
		action: 'add_patient_to_dispensary_request',
		'patient_id': patient
	};
	jQuery.post(cdrmedajax.ajax_url, data, function(response) {
		$('.add_patient_to_dispensary_td'+patient).html('Patient in Dispensary');
	});	
});

var dispensary_limit = 4;
var dispensary_offset = 0;


// Physician server side Pagination with scroll
jQuery(function($){
    $('#patient_dispensaries_modal_body').bind('scroll', function(){
		if($(this).scrollTop() + $(this).innerHeight()>=$(this)[0].scrollHeight){
            dispensary_offset = dispensary_limit + dispensary_offset;
			var search = $('.patient-dispensary-search').val();
			var call_type = $('.patient_dispensary_model .call_type').val();
			var data = {
				action: 'patient_get_dispensary_request',
				'call_type': call_type,
				'search': search,
				'limit': dispensary_limit,
				'offset': dispensary_offset
			};
			jQuery.post(cdrmedajax.ajax_url, data, function(response) {
				var response = jQuery.parseJSON(response);
				$('#patient_dispensaries_modal_body').append(response.output);
			});
			
		}
	});
});


// Search Dispensary by First OR Last Name
$(document).on("keyup", '.patient-dispensary-search', function(event) {
	
	
	$('.patient_dispensaries_modal_body').html('<img style="display:inherit;max-width:100px;width: 100px;margin: 5px auto;" src="'+cdrmedajax.img_url+'/loading-secure-animation.gif" alt="Loading" />');
	
	var search = $(this).val();
	var call_type = $('.patient_dispensary_model .call_type').val();
	if(search == ''){
		dispensary_offset = 0;
	}
	
	var data = {
		action: 'patient_get_dispensary_request',
		'call_type': call_type,
		'search': search,
	};
	jQuery.post(cdrmedajax.ajax_url, data, function(response) {
		var response = jQuery.parseJSON(response);
		if(response.action){
			$('#patient_dispensaries_modal_body').html(response.output);
		}
		else{
			$('#patient_dispensaries_modal_body').html('<span class="feedback empty-message">No Dispensary Found!</span>');
		}
	});
	jQuery("#patient_dispensary_feedback").hide();
});

//load dispensaries when Popup show
//$(document).on("click", '.patient_dispensary_model_show', function(event) {
function patient_dispensary_model_show(current_class){
	$('.cdrmed-modal-box-content .modal-footer').html('<button type="button" class="btn btn-primary patient-add-physician-dispensary-btn" id="patient-dispensary-modal-btn">Update Dispensaries</button>');
	
	$('.cdrmed-modal-box-content .patient_dispensaries_modal_body').html('<img style="display: inherit; max-width:100px;width:100px; margin:5px auto;" src="'+cdrmedajax.img_url+'/loading-secure-animation.gif" alt="" />');
	
	var call_type = $(current_class).data('call_type');
	$('.cdrmed-modal-box-content .call_type').val(call_type);
	
	var data = {
		action: 'patient_get_physician_dispensary_ajax',
		'call_type': call_type,
	};
	jQuery.post(cdrmedajax.ajax_url, data, function(response) {
		var response = jQuery.parseJSON(response);
		if(response.action){
			$('.cdrmed-modal-box-content .patient_dispensaries_modal_body').html(response.output);
		}
		else{
			$('.cdrmed-modal-box-content .patient_dispensaries_modal_body').html('<span class="feedback empty-message">No Found!</span>');
		}
	});
}

//load dispensaries when Popup show
//$(document).on("click", '.patient_physician_model_show', function(event) {
function patient_physician_model_show(current_class){	
	$('.cdrmed-modal-box-content .modal-footer').html('<button type="button" class="btn btn-primary patient-add-physician-dispensary-btn" id="patient-dispensary-modal-btn">Update Physicians</button>');
	
	$('.cdrmed-modal-box-content .patient_dispensaries_modal_body').html('<img style="display: inherit; max-width:100px;width:100px; margin:5px auto;" src="'+cdrmedajax.img_url+'/loading-secure-animation.gif" alt="" />');
	
	var call_type = $(current_class).data('call_type');
	$('.cdrmed-modal-box-content .call_type').val(call_type);
	
	var data = {
		action: 'patient_get_physician_dispensary_ajax',
		'call_type': call_type,
	};
	jQuery.post(cdrmedajax.ajax_url, data, function(response) {
		var response = jQuery.parseJSON(response);
		if(response.action){
			$('.cdrmed-modal-box-content .patient_dispensaries_modal_body').html(response.output);
		}
		else{
			$('.cdrmed-modal-box-content .patient_dispensaries_modal_body').html('<span class="feedback empty-message">No Found!</span>');
		}
	});
}



//add dispensary in aptient dispensary list
$(document).on("click", '.patient-add-physician-dispensary-btn', function(event) {
	var physician_dispensaries_id_arr = new Array();
	$(".cdrmed-modal-box-content .dispensaries_id").each(function() {
		$('.cdrmed-modal-box-content #dispensar_id'+$(this).val()).removeClass('checked');
	});
	$(".cdrmed-modal-box-content .dispensaries_id:checked").each(function() {
		$('#dispensar_id'+$(this).val()).addClass('checked');
		physician_dispensaries_id_arr.push($(this).val());
	});
	
	var call_type = $('.cdrmed-modal-box-content .call_type').val();
	
	var data = {
		action: 'patient_add_physician_dispensary_ajax',
		'call_type': call_type,
		'physician_dispensaries_id_arr': physician_dispensaries_id_arr,
	};
	
	jQuery.post(cdrmedajax.ajax_url, data, function(response) {
		$('.modal-box-feedback').html(response);
		reset_modal_feedback('success-message', true);
	});
});


// remove user
$(document).on("click", '.user-remove-custom-btn', function(event) {
	
	if (confirm('Do you want to Remove?')){
		
		$('body').addClass('rt-loading');
		$('body').append('<div id="loader-wrapper"><div id="loader"></div></div>');
		
		var user_id = jQuery(this).data('user_id');
		var data = {
			action: 'remove_child_physician_dispensary_ajax',
			'user_id': user_id
		};
		jQuery.post(cdrmedajax.ajax_url, data, function(response) {
			
			setTimeout( function() { window.location.reload(); }, 2000);
		});
		
		$(this).parent().parent().remove();
		$('body').removeClass('rt-loading');
		$('body #loader-wrapper').remove();
	}
});





/*
==================================================================================
		Dashboard
==================================================================================
*/

function analogClock(){
}
analogClock.prototype.run = function() {
    var date = new Date();
	var weekdays = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];
	var weekday = weekdays[date.getDay()];
	var day = date.getDate();
	var months = [ "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December" ];
	var month = months[date.getMonth()];
	var year = date.getFullYear();
	var today = weekday + ", " + day + " " + month + " " + year;
	$("#current_date").html(today);
};





jQuery(document).ready(function(){
	
	
	if(cdrmedajax.dashboard_widgets_run){
		if(!cdrmedajax.character3){
			//setInterval('get_appointments()', 200000);
			//get_weather();
			var analogclock = new analogClock();
			analogclock.run();
			window.setInterval(function(){
				if(cdrmedajax.character1){
					get_appointments();
				}
				else{
					get_orders();
				}
				analogclock.run(); 
			}, 200000);
		}
		dashboard_appointment_widget_load_func();
	}
	
	/*
	* Dashboard Profile Button ajax
	*/
	jQuery('.remove-profile-btn').click(function(){
		var data = {
			action: 'update_user_profile_request',
			'call_action': 'by_dashboard',
			'remove': 'remove',
		};
		jQuery.post(cdrmedajax.ajax_url, data, function(response) {
			window.location.reload();
		});
	});
	
	jQuery('#profile-change-btn').click(function(){
		var profile_img_change = jQuery('#profile_img_change_flag1').val();
		var imgCropped = jQuery('#imgCropped1').attr('ng-src');
		
		var data = {
			action: 'update_user_profile_request',
			'call_action': 'by_dashboard',
			'profile_img_change': profile_img_change,
			'imgCropped': imgCropped,
		};
		
		jQuery.post(cdrmedajax.ajax_url, data, function(response) {
			if(profile_img_change == 'change'){
				jQuery("#user-avatar1").show();
				jQuery("#user-avatar").show();
				jQuery("#user-avatar").attr('src',imgCropped);
				jQuery("#user-avatar1").attr('src',imgCropped);
				jQuery('#profile_img_change_flag').val('');
				jQuery('#profile_img_change_flag1').val('');
				$("#profile_img_file").val('');
				$("#profile_img_file1").val('');
				jQuery("#cropArea").hide();
				jQuery("#cropArea1").hide();
				jQuery("#clear-profile-img-btn1").hide();
				$("#profile_img_file_span").hide();
				$("#profile_img_file_span1").hide();
				$("#profile-img-crop").show();
				$("#profile-img-crop1").show();
				$("#profile-change-btn").hide();
				jQuery('#change-profile-btn').removeClass('change-profile-btn-display');
				jQuery('#remove-profile-btn').removeClass('change-profile-btn-display');
				jQuery('#remove-profile-btn1').removeClass('change-profile-btn-display');
				jQuery('#change-profile-btn1').removeClass('change-profile-btn-display');
			}
			$("#profile_img_file_label1").show();
		});
	});
});


/*
*
*	JS Functions for Load Dashboard Appointment Widget with AJAX CALL
*
*/
function dashboard_appointment_widget_load_func(){
	
	if(!cdrmedajax.character3){
		
		jQuery('.second-widget .other-data').addClass('white-background');
		$('.second-widget .other-data').html('<img style="display: inherit; margin: 5px auto;" src="'+cdrmedajax.img_url+'/loading-secure-animation.gif" alt="" />');
		
		jQuery('.third-widget .other-data').addClass('white-background');
		$('.third-widget .other-data').html('<img style="display: inherit; margin: 5px auto;" src="'+cdrmedajax.img_url+'/loading-secure-animation.gif" alt="" />');
		
		jQuery('.fourth-widget .other-data').addClass('white-background');
		$('.fourth-widget .other-data').html('<img style="display: inherit; margin: 5px auto;" src="'+cdrmedajax.img_url+'/loading-secure-animation.gif" alt="" />');
		
		var data = {
			action: 'get_dashboard_patients_ajax'
		};
		jQuery.post(wc_add_to_cart_params.ajax_url, data, function(response) {
			// call weather js library
			var response = jQuery.parseJSON(response);
			jQuery('.second-widget .active-box').html(response.active);
			jQuery('.second-widget .other-data').html(response.other);
		});
		
		jQuery('.second-widget .other-data').removeClass('white-background');
		
		if(cdrmedajax.user_role == 'dispensary'){
			
			var data = {
				action: 'get_dashboard_products_ajax'
			};
			jQuery.post(wc_add_to_cart_params.ajax_url, data, function(response) {
				var response = jQuery.parseJSON(response);
				jQuery('.third-widget .title').html('Products');
				jQuery('.third-widget .active-box').html(response.active);
				jQuery('.third-widget .other-data').html(response.other);
			});
		}
		else{
			var data = {
				action: 'get_dashboard_therapy_ajax'
			};
			jQuery.post(wc_add_to_cart_params.ajax_url, data, function(response) {
				var response = jQuery.parseJSON(response);
				jQuery('.third-widget .title').html('Therapy');
				jQuery('.third-widget .active-box').html(response.active);
				jQuery('.third-widget .other-data').html(response.other);
			});
		}
		
		jQuery('.third-widget .other-data').removeClass('white-background');
		if(cdrmedajax.character1){
			get_appointments();
		}
		else{
			get_orders();
		}
		
		jQuery('.fourth-widget .other-data').removeClass('white-background');
		
	}
	else{
		
		$('.patinet-side-appointments .appointments').html('<img style="display: inherit; margin: 5px auto;" src="'+cdrmedajax.img_url+'/loading-secure-animation.gif" alt="" />');
		
		var data = {
			action: 'read_appointments_ajax',
		};
		jQuery.post(cdrmedajax.ajax_url, data, function(response) {
			var response = jQuery.parseJSON(response);
			if(response.action){
				$('.patinet-side-appointments .appointments').html(response.output);
			}
			else{
				$('.patinet-side-appointments .appointments').html('<span class="feedback empty-message">You have no Appointment!</span>');
			}
		});
	}
}

// Get Appointments for Physician/Dispensary
function get_appointments(){
	var data = {
		action: 'get_dashboard_appointment_ajax'
	};
	jQuery.post(cdrmedajax.ajax_url, data, function(response) {
		var response = jQuery.parseJSON(response);
		jQuery('.fourth-widget .title').html('Schedule');
		jQuery('.fourth-widget .active-box').html(response.active);
		jQuery('.fourth-widget .other-data').html(response.other);
	});
}

function get_orders(){
	var data = {
		action: 'get_dashboard_orders_ajax'
	};
	jQuery.post(cdrmedajax.ajax_url, data, function(response) {
		var response = jQuery.parseJSON(response);
		jQuery('.fourth-widget .title').html('Orders');
		jQuery('.fourth-widget .active-box').html(response.active);
		jQuery('.fourth-widget .other-data').html(response.other);
	});
}



/*
*
*	Educatonal Topics
*
*/

$(document).on("click", '.educational_modal_box_show', function(event) {
	
	$('<div class="educational-modal-box"><div id="educational_modal_box" class="modal fade in"><div class="modal-dialog"><div class="modal-content"><div class="modal-header"><button type="button" class="close fa fa-close" data-dismiss="modal"></button></div><div class="modal-body educational-learnings"></div></div></div></div>').modal();
	jQuery(".educational-modal-box #educational_modal_box").show();
	jQuery(".educational-modal-box #educational_modal_box .modal-content").addClass('white-background');
	
	$('.educational-modal-box #educational_modal_box .modal-body').html('<img style="display: inherit; margin: 5px auto;" src="'+cdrmedajax.img_url+'/loading-secure-animation.gif" alt="" />');
	
	var data = {
		action: 'educational_topics_load_ajax'
	};
	jQuery.post(cdrmedajax.ajax_url, data, function(response) {
		$('.educational-modal-box #educational_modal_box .modal-body').html(response);
		jQuery(".educational-modal-box #educational_modal_box .modal-content").removeClass('white-background');
	});
	
});

$(document).on("click", '.educational-modal-box .close', function(event) {
	jQuery(".educational-modal-box").remove();
	jQuery(".educational-modal-box #educational_modal_box").hide();
});


/*
==================================================================================
		patient Document
==================================================================================
*/

//$(document).on("click", '.patient-edit-document', function(event) {
function patient_edit_document(current_class){
	$('.cdrmed-modal-box-content .cdrmed-modal-box .modal-body .body-contents .docedits').html('<img style="display: inherit; margin: 5px auto;" src="'+cdrmedajax.img_url+'/loading-secure-animation.gif" alt="" />');
	
	var post_id = $(current_class).attr('data-post-id');
	console.log(post_id)
	var data = {
		action: 'edit_documents_ajax',
		'post_id': post_id,
	};
	jQuery.post(wc_add_to_cart_params.ajax_url, data, function(response) {
		$('.cdrmed-modal-box-content .cdrmed-modal-box .modal-body .body-contents .docedits').html(response);
	});
}


/*
==================================================================================
		On Patients
==================================================================================
*/

// Send Email to patient
//$(document).on("click", '.send_email_to_patient_modal', function(event) {
function send_email_to_patient_modal(current_class){
	$('.cdrmed-modal-box-content .modal-footer').html('<button type="button" class="btn btn-primary send_email_to_patient">Send Email</button>');
	
	var recipient = jQuery(current_class).attr('data-email');
	jQuery(".cdrmed-modal-box-content #patient_email").val(recipient);
	
}

$(document).on("click", '.send_email_to_patient', function(event) {
	event.preventDefault();
	recipient = jQuery('.cdrmed-modal-box-content #patient_email').val();
	subject = jQuery('.cdrmed-modal-box-content #subject').val();
	message = jQuery('.cdrmed-modal-box-content #message').val();
	if(message == ''){
		jQuery(".cdrmed-modal-box-content #message").addClass('empty');
		jQuery(".modal-box-feedback").html('Enter Message');
		reset_modal_feedback('error-message', false);
		return false;
	}
	
	$(this).html('waiting...');
	$(this).attr('disabled','disabled');
	var data = {
		action: 'send_email_to_patient_ajax',
		'to': recipient,
		'subject': subject,
		'message': message,
	};
	
	jQuery.post(cdrmedajax.ajax_url, data, function(response) {
		var response = jQuery.parseJSON(response);
		jQuery(".modal-box-feedback").html(response.output);
		if(response.action){
			reset_modal_feedback('success-message', true);
		}
		else{
			reset_modal_feedback('error-message', false);
		}
		
		$('.send_email_to_patient').removeAttr('disabled');
	});
	
});


// Custom Popup For Edit profile
//$(document).on("click", '.edit-child-profile-modal', function(event) {
function edit_child_profile_modal(current_class){
	
	$('.cdrmed-modal-box-content .modal-footer').html('<button type="button" class="btn btn-primary edit-child-profile">Update Profile</button>');
	
	var patient_id = jQuery(current_class).data('user_id');
	var call_for = jQuery(current_class).attr('data-call_for');
	
	if(call_for == 'patient'){
		
		jQuery('.cdrmed-modal-box-content .form-group.user-profile-modal-group.half_width').addClass('half');
		jQuery('.cdrmed-modal-box-content .form-group.user-profile-modal-group.three_width').addClass('three');
		
		var data = {
			action: 'fetch_patient_info_for_update_ajax',
			'patient_id': patient_id,
		};
		
		jQuery.post(cdrmedajax.ajax_url, data, function(response) {
			var response = jQuery.parseJSON(response);
			jQuery('.cdrmed-modal-box-content #date_of_birth').val(response.dob);
			jQuery('.cdrmed-modal-box-content #address1').val(response.address1);
			jQuery('.cdrmed-modal-box-content #address2').val(response.address2);
			jQuery('.cdrmed-modal-box-content #city').val(response.city);
			jQuery('.cdrmed-modal-box-content #state').val(response.state);
			jQuery('.cdrmed-modal-box-content #zip_code').val(response.zip_code);
			//jQuery('.cdrmed-modal-box-content #country option[value='+response.country+']').attr('selected');
			$(".cdrmed-modal-box-content #country option[value='"+response.country+"']").prop('selected', true);
			//jQuery('.cdrmed-modal-box-content #country').val(response.country);
			jQuery('.cdrmed-modal-box-content #call_for').val('patient');
			
			
		});
	}
	else{
		jQuery('.cdrmed-modal-box-content .edit-child-profile-modal .other-fields').remove();
	}
	
	var first_name = jQuery(current_class).data('first_name');
	var last_name = jQuery(current_class).data('last_name');
	var email = jQuery(current_class).data('email');
	jQuery('.cdrmed-modal-box-content #patient_id').val(patient_id);
	jQuery('.cdrmed-modal-box-content #first_name').val(first_name);
	jQuery('.cdrmed-modal-box-content #last_name').val(last_name);
	jQuery('.cdrmed-modal-box-content #email').val(email);
	jQuery('.cdrmed-modal-box-content #email').val(email);
	jQuery('.cdrmed-modal-box-content #email').val(email);
		
	$('.cdrmed-modal-box-content input[data-pass="m-password-input"]').addClass('form-control');
	$('.cdrmed-modal-box-content input[data-pass="m-password-input"]').addClass('show-password-field');
}


jQuery(document).on("keydown", '.cdrmed-modal-box-content .mdates', function(event) {
	jQuery('.cdrmed-modal-box-content .mdates').inputmask("mm/dd/yyyy");
});

$(document).on("click", '.edit-child-profile', function(event) {
	event.preventDefault();
	
	var address1, address2, city, state, zip_code, country, date_of_birth = '';
	
	var user_id = jQuery('.cdrmed-modal-box-content #patient_id').val();
	var first_name = jQuery('.cdrmed-modal-box-content #first_name').val();
	var last_name = jQuery('.cdrmed-modal-box-content #last_name').val();
	var email = jQuery('.cdrmed-modal-box-content #email').val();
	var password = jQuery('.cdrmed-modal-box-content #password').val();
	var cpassword = jQuery('.cdrmed-modal-box-content #c_password').val();
	var call_for = jQuery('.cdrmed-modal-box-content #call_for').val();
	
	
	if(email == ''){
		jQuery(".modal-box-feedback").html('Enter Email');
		jQuery(".cdrmed-modal-box-content #email").addClass('empty');
		reset_modal_feedback('error-message', false);
		return false;
	}
	if(password != ''){
		if(jQuery('#m_password_health').val() != 'Good' && jQuery('#m_password_health').val() != 'Strong'){
			jQuery('#m_password_feedback').html('Do not Use Weak Passowrd');
			return false;
		}
		else if(password != cpassword){
			jQuery('#m_c_password_feedback').html('Passwords do not match.');
			return false;
		}
	}
	if(call_for){
		date_of_birth = jQuery('.cdrmed-modal-box-content #date_of_birth').val();
		address1 = jQuery('.cdrmed-modal-box-content #address1').val();
		address2 = jQuery('.cdrmed-modal-box-content #address2').val();
		city = jQuery('.cdrmed-modal-box-content #city').val();
		state = jQuery('.cdrmed-modal-box-content #state').val();
		zip_code = jQuery('.cdrmed-modal-box-content #zip_code').val();
		country = jQuery('.cdrmed-modal-box-content #country option:selected').val();
		if(date_of_birth == ''){
			jQuery(".modal-box-feedback").html('Enter Date of Birth');
			jQuery(".cdrmed-modal-box-content #date_of_birth").addClass('empty');
			reset_modal_feedback('error-message', false);
			return false;
		}
	}
	
	$(this).html('waiting...');
	$(this).attr('disabled','disabled');
	
	var data = {
		action: 'edit_child_profile_ajax',
		'user_id': user_id,
		'first_name': first_name,
		'last_name': last_name,
		'email': email,
		'password': password,
		'date_of_birth': date_of_birth,
		'address1': address1,
		'address2': address2,
		'city': city,
		'state': state,
		'zip_code': zip_code,
		'country': country
	};
	
	jQuery.post(cdrmedajax.ajax_url, data, function(response) {
		var response = jQuery.parseJSON(response);
		jQuery(".modal-box-feedback").html(response.output);
		if(response.action){
			reset_modal_feedback('success-message', true);
		}
		else{
			reset_modal_feedback('error-message', false);
		}
		$('.edit-child-profile').removeAttr('disabled');
	});
	
});







/*
==================================================================================
		Profile
==================================================================================
*/

angular.module('app', ['ngImgCrop'])
  .controller('Ctrl', function($scope) {
    $scope.myImage='';
    $scope.myCroppedImage='';
	

	
	var handleFileSelect1=function(evt) {
		$("#profile_img_file_label1").hide();
		jQuery("#user-avatar1").hide();
		jQuery("#cropArea1").show();
		jQuery("#clear-profile-img-btn1").show();
		$("#profile-img-crop1").show();
		$("#profile-change-btn").show();
		jQuery('#profile_img_change_flag1').val('change');
      var file=evt.currentTarget.files[0];
      var reader = new FileReader();
      reader.onload = function (evt) {
        $scope.$apply(function($scope){
          $scope.myImage=evt.target.result;
        });
      };
      reader.readAsDataURL(file);
    };
	
	var handleFileSelect=function(evt) {
		$("#profile_img_file_label").hide();
		jQuery("#user-avatar").hide();
		jQuery("#cropArea").show();
		jQuery("#clear-profile-img-btn").show();
		$("#profile-img-crop").show();		
		jQuery('#profile_img_change_flag').val('change');
      var file=evt.currentTarget.files[0];
      var reader = new FileReader();
      reader.onload = function (evt) {
        $scope.$apply(function($scope){
          $scope.myImage=evt.target.result;
        });
      };
      reader.readAsDataURL(file);
    };
	
    angular.element(document.querySelector('#profile_img_file')).on('change',handleFileSelect);
	angular.element(document.querySelector('#profile_img_file1')).on('change',handleFileSelect1);
});



jQuery(document).ready(function(){
	
	
	jQuery('#user-profile-customs-btn').click(function(){
		
		$('#current_api_status').html('<img src="'+cdrmedajax.img_url+'/loading-secure-animation.gif" alt="Loading" />');
		
		jQuery('#user-profile-customs-btn').val('Processing...');
		
		jQuery('#password-feedback').html('');
		jQuery('#cpassword-feedback').html('');
		
		var userrole = jQuery('#userrole').val();
		
		var profile_img_change = jQuery('#profile_img_change_flag').val();
		var imgCropped = jQuery('#imgCropped').attr('ng-src');
		var first_name = jQuery('#first_name').val();
		var last_name = jQuery('#last_name').val();
		var upassword = '';
		if(first_name == ''){
			jQuery('#user-profile-customs-btn').val('Update Profile');
			jQuery('#user-custom-profile-feedback').html('Enter First Name');
			jQuery("#first_name").addClass('empty');
			jQuery("#user-custom-profile-feedback").addClass('fail');
			setTimeout( function() {
				jQuery("#user-custom-profile-feedback").html('');jQuery("#first_name").removeClass('empty');jQuery("#user-custom-profile-feedback").removeClass('empty');
			}, 2000);
			return false;
		}
		else if(last_name == ''){
			jQuery('#user-profile-customs-btn').val('Update Profile');
			jQuery('#user-custom-profile-feedback').html('Enter Last Name');
			jQuery("#last_name").addClass('empty');
			jQuery("#user-custom-profile-feedback").addClass('fail');
			setTimeout( function() {
				jQuery("#user-custom-profile-feedback").html('');jQuery("#last_name").removeClass('empty');jQuery("#user-custom-profile-feedback").removeClass('empty');
			}, 2000);
			return false;
		}
		else if(jQuery('#upassword').val() != ''){
			if(jQuery('#upassword_health').val() != 'Good' && jQuery('#upassword_health').val() != 'Strong'){
				jQuery('#password-feedback').html('Do not Use Weak Passowrd');
				jQuery('#user-profile-customs-btn').val('Update Profile');
				return false;
			}
			else if(jQuery('#upassword').val() != jQuery('#ucpassword').val()){
				jQuery('#cpassword-feedback').html('Passwords do not match.');
				jQuery('#user-profile-customs-btn').val('Update Profile');
				return false;
			}
			upassword = jQuery('#upassword').val();
		}
		
		var organization_name,pos_system, c_pos_system_other, inventory_system, c_inventory_system_other, inv_api_token, inv_api_consumer, inv_api_secret, pos_api_token, pos_api_consumer, pos_api_secret, health_report, how_health_report, appointment_system, calendly_url, news_feed_link, medicine_notification = '';
		
		var calendly_api_key = '';
		if( userrole == 'physician' || userrole == 'administrator'){
			appointment_system = $('#appointment_system option:selected').text();
			calendly_url = $('#calendly_url').val();
			calendly_api_key = $('#calendly_api_key').val();
			news_feed_link = $('#news_feed_link').val();
			organization_name = $('#organization_name').val();
		}
		else if(userrole == 'dispensary'){
			pos_system = $('#pos_system option:selected').text();
			if(pos_system == 'Other (type in field)'){
				c_pos_system_other = $('#c_pos_system_other').val();
			}
			inventory_system = $('#inventory_system option:selected').text();
			if(inventory_system == 'Other (type in field)'){
				c_inventory_system_other = $('#c_inventory_system_other').val();
			}
			
			inv_api_token = $('#c_invnentory_api_token').val();
			inv_api_consumer = $('#c_invnentory_api_consumer').val();
			inv_api_secret = $('#c_invnentory_api_secret').val();
			pos_api_token = $('#c_pos_api_token').val();
			pos_api_consumer = $('#c_pos_api_consumer').val();
			pos_api_secret = $('#c_pos_api_secret').val();
			news_feed_link = $('#news_feed_link').val();
			organization_name = $('#organization_name').val();
			
		}
		else if(userrole == 'patient' || userrole == 'subscriber'){
			medicine_notification = $("#medicine_notification:checked").val();
			health_report = $("#health_report:checked").val();
			how_health_report = $("#how_health_report:checked").val();
		}
		
		var data = {
			action: 'update_user_profile_request',
			'call_action': 'by_profile',
			'userrole': userrole,
			'profile_img_change': profile_img_change,
			'imgCropped': imgCropped,
			'first_name': first_name,
			'last_name': last_name,
			'organization_name': organization_name,
			'upassword': upassword,
			'news_feed_link': news_feed_link,
			'appointment_system': appointment_system,
			'calendly_url': calendly_url,
			'calendly_api_key': calendly_api_key,
			'pos_system': pos_system,
			'c_pos_system_other': c_pos_system_other,
			'inventory_system': inventory_system,
			'c_inventory_system_other': c_inventory_system_other,
			'medicine_notification': medicine_notification,
			'health_report': health_report,
			'how_health_report': how_health_report,
			'c_invnentory_api_token': inv_api_token,
			'c_invnentory_api_consumer': inv_api_consumer,
			'c_invnentory_api_secret': inv_api_secret,
			'c_pos_api_token': pos_api_token,
			'c_pos_api_consumer': pos_api_consumer,
			'c_pos_api_secret': pos_api_secret
		};
		
		jQuery.post(cdrmedajax.ajax_url, data, function(response) {
			
			if(profile_img_change == 'change'){
				jQuery("#user-avatar").show();
				jQuery("#user-avatar1").show();
				jQuery("#user-avatar").attr('src',imgCropped);
				jQuery("#user-avatar1").attr('src',imgCropped);
				jQuery('#profile_img_change_flag').val('');
				jQuery('#profile_img_change_flag1').val('');
				$("#profile_img_file").val('');
				$("#profile_img_file1").val('');
				jQuery("#cropArea").hide();
				jQuery("#cropArea1").hide();
				jQuery("#clear-profile-img-btn").hide();
				$("#profile_img_file_span").hide();
				$("#profile_img_file_span1").hide();
				$("#profile-img-crop").show();
				$("#profile-img-crop1").show();
				jQuery('#change-profile-btn').removeClass('change-profile-btn-display');
				jQuery('#remove-profile-btn').removeClass('change-profile-btn-display');
				jQuery('#change-profile-btn1').removeClass('change-profile-btn-display');
				jQuery('#remove-profile-btn1').removeClass('change-profile-btn-display');
				
			}
			if(upassword != ''){
				jQuery('#upassword').val('');
				jQuery('#ucpassword').val('');
				jQuery('#upassword_health').val('');
				jQuery('#cpassword-feedback').html('');
				$(".password-reset-alert-bar").hide();
			}
			
			$("#profile_img_file_label").show();
			
			jQuery('#user-profile-customs-btn').val('Update Profile');
			jQuery('#user-custom-profile-feedback').removeClass('');
			jQuery('#user-custom-profile-feedback').addClass('success');
			jQuery('#user-custom-profile-feedback').html('Profile Update Successfully!');
			
			if(userrole == 'dispensary'){
				$('#current_api').html('Current System: '+ pos_system);
				var data = {
					action: 'api_keys_save_ajax',
					'pos_system': pos_system,
					'token': pos_api_token,
					'consumer': pos_api_consumer,
					'secret': pos_api_secret
				};
				
				jQuery.post(cdrmedajax.ajax_url, data, function(response) {
					response = $.parseJSON(response);
					$('#current_api_status').html(response.message);
				});
			}
			
			setTimeout( function() {
				jQuery("#user-custom-profile-feedback").html('');
				jQuery('#user-custom-profile-feedback').removeClass('success');
			}, 2000);
			
		});
	});
	
	
	$('#pos_system').change(
		function() {
			var pos_system = $('#pos_system option:selected').text();
			if(pos_system == 'Other (type in field)'){
				$('#c_pos_system_other').show();
			}
			else{
				$('#c_pos_system_other').val('');
				$('#c_pos_system_other').hide();
			}
			
			default_keys(pos_system);
			
		}
	);
	
	function default_keys(system){
		
		$('#c_pos_api_token').show();
		$('#c_pos_api_consumer').show();
		$('#c_pos_api_secret').show();
		
		if(system == 'Xero'){
			$('#c_pos_api_token').hide();
		}
		else if(system == 'MMJ Menu'){
			$('#c_pos_api_consumer').hide();
			$('#c_pos_api_secret').hide();
		}
		else if(system == 'MJFreeway Menu'){
			$('#c_pos_api_token').attr('placeholder','API ID');
			$('#c_pos_api_consumer').attr('placeholder','API key');
			$('#c_pos_api_secret').hide();
		}
		else if(system == 'ZOHO'){
			$('#c_pos_api_consumer').hide();
			$('#c_pos_api_secret').hide();
			$('.api-alert-message').html('To get your Authentication Token, please Login to your ZOHO Account and <a href="https://accounts.zoho.com/apiauthtoken/create?SCOPE=ZohoInventory/inventoryapi" target="_blank">Click Here</a>');
		}
		else{
			$('.api-alert-message').html('');
		}
		
		var data = {
			action: 'api_keys_read_ajax'
		};
		jQuery.post(cdrmedajax.ajax_url, data, function(response) {
			var response = $.parseJSON(response);
			if(system == response.pos_system){
				$('.c_pos_api_token').val(response.token);
				$('.c_pos_api_consumer').val(response.consumer_key);
				$('.c_pos_api_secret').val(response.secret_key);
			}
			else{
				$('.c_pos_api_token').val('');
				$('.c_pos_api_consumer').val('');
				$('.c_pos_api_secret').val('');
			}
		});
	}	
	
	
	// ADD New Physician-Dispensary Child user
	jQuery("#nu-btn").click(function(e){
		e.preventDefault();
		
		var email = jQuery('#nu_email').val();
		var first_name = jQuery('#nu_firstname').val();
		var last_name = jQuery('#nu_lastname').val();
		var password = jQuery('#nu_password').val();
		var cpassword = jQuery('#nu_c_password').val();
		var error_found = null;
		
		if(email == ''){
			error_found = true;
			jQuery(".default-feedback").html('Enter Email');
			jQuery("#nu_email").addClass('empty');
		}
		else if(first_name == ''){
			error_found = true;
			jQuery(".default-feedback").html('Enter First Name');
			jQuery("#nu_firstname").addClass('empty');
		}
		else if(last_name == ''){
			error_found = true;
			jQuery(".default-feedback").html('Enter Last Name');
			jQuery("#nu_lastname").addClass('empty');
		}
		else if(password == ''){
			error_found = true;
			jQuery(".default-feedback").html('Enter Password');
			jQuery("#nu_password").addClass('empty');
		}
		if(error_found){
			reset_feedback('error-message', false);
			return false;
		}
		
		if(jQuery('#nu_password_health').val() != 'Good' && jQuery('#nu_password_health').val() != 'Strong'){
			jQuery('#nu_password_feedback').html('Do not Use Weak Passowrd');
			return false;
		}
		else if(password != cpassword){
			jQuery('#nu_c_password_feedback').html('Passwords do not match.');
			return false;
		}
		
		var data = {
			action: 'add_child_physician_dispensary_ajax',
			'email': email,
			'first_name': first_name,
			'last_name': last_name,
			'password': password
		};
		
		jQuery.post(cdrmedajax.ajax_url, data, function(response) {
			var response = jQuery.parseJSON(response);
			jQuery(".default-feedback").html(response.feedback);
			if(response.action){
				reset_feedback('success-message', true);
			}
			else{
				reset_feedback('error-message', false);
			}
		});
	});
	
});







/*
==================================================================================
		Shoop / Therapy
==================================================================================
*/

jQuery(document).ready(function(){
	
	// Save therapy
	jQuery('#save-therapy-btn').click(function(){
		//jQuery('#save-therapy-loader').show();
		//var product_id = jQuery('#save-therapy-btn').data('product_id');
		var data_to_send_ajax = [];
		var therapy_details = jQuery(this).attr('data-details');
		therapy_details = therapy_details.split(',');
		//console.log(therapy_details);
		var products_selected = cdrmed_steps['selected_prod'];
		var finilized_products = {};
		var other_data = {
			shoop_id : therapy_details[0],
			patient_id : therapy_details[1],
		};
		jQuery.each(products_selected, function(index, val) {
			//var product_item = jQuery('.cdrmed_slide_item.pro'+val);
			//var product_json = product_item.attr('data-product-json');
			
			//{"p_product_title":"Mangia Ganja Spaghetti Sauce","p_cannabinoid":"THC","p_cannabinoid_value":"180","p_cannabinoid_percent_value":"70","s_cannabinoid":"CBD","s_cannabinoid_value":"200","s_cannabinoid_percent_value":"3","this_much_value":0.29,"dose_per_day":0.15,"secondary_calculations":8.7,"frequency_per_day":"2 X daily","full_profile_link":"<a href=http:\/\/genius3.com\/product\/mangia-ganja-spaghetti-sauce\></div>See Full Profile<\/a>","calculations":514.28571428571}
			
			/*
			*	get values from hidden inputs
			*/
			var myObj = {};
			
			myObj["p_product_title"] = $('.step_'+index+' .cdrmed_slide_item.pro'+val+' .p_product_title').val();
			myObj["p_cannabinoid"] = $('.step_'+index+' .cdrmed_slide_item.pro'+val+' .p_cannabinoid').val();
			myObj["p_cannabinoid_value"] = $('.step_'+index+' .cdrmed_slide_item.pro'+val+' .p_cannabinoid_value').val();
			myObj["p_cannabinoid_percent_value"] = $('.step_'+index+' .cdrmed_slide_item.pro'+val+' .p_cannabinoid_percent_value').val();
			
			myObj["s_cannabinoid"] = $('.step_'+index+' .cdrmed_slide_item.pro'+val+' .s_cannabinoid').val();
			myObj["s_cannabinoid_value"] = $('.step_'+index+' .cdrmed_slide_item.pro'+val+' .s_cannabinoid_value').val();
			myObj["s_cannabinoid_percent_value"] = $('.step_'+index+' .cdrmed_slide_item.pro'+val+' .s_cannabinoid_percent_value').val();
			
			myObj["this_much_value"] = $('.step_'+index+' .cdrmed_slide_item.pro'+val+' .this_much_value').val();
			myObj["dose_per_day"] = $('.step_'+index+' .cdrmed_slide_item.pro'+val+' .dose_per_day').val();
			
			myObj["secondary_calculations"] = $('.step_'+index+' .cdrmed_slide_item.pro'+val+' .secondary_calculations').val();
			myObj["frequency_per_day"] = $('.step_'+index+' .cdrmed_slide_item.pro'+val+' .frequency_per_day').val();
			myObj["full_profile_link"] = $('.step_'+index+' .cdrmed_slide_item.pro'+val+' .full_profile_link').val();
			myObj["calculations"] = $('.step_'+index+' .cdrmed_slide_item.pro'+val+' .calculations').val();
			
			var product_json = JSON.stringify(myObj);
			finilized_products[val] = product_json;
		});
		var data_to_send = [other_data, finilized_products];
		//console.log(data_to_send);
		jQuery.post(wc_add_to_cart_params.ajax_url, {action:'save_patient_therapy_ajax', data:data_to_send}, function(data, textStatus, xhr) {
		//	jQuery('#save-therapy-loader').hide();
			jQuery('#save-therapy-btn').closest('.buttons').append(data);
			if(cdrmedajax.character2){
				//Reload when Dispensary save therapy
				//window.location.href = cdrmedajax.save_therapy;
			}
			else if(cdrmedajax.character3){
				//Reload when patient save therapy
				//window.location.hash = 'shoop';
				//window.location.reload();
			}
			//cdrmedajax.save_therapy;//jQuery('#logo > a').attr('href') + '/patient-dashboard/';
			
			var data = {
				'action': 'send_enquiry_ajax_call',
				'by_therapy': 'by_therapy',
				'patient_id': therapy_details[1],
				'product_ids': finilized_products
			};
			jQuery.post(cdrmedajax.ajax_url, data, function(response) {
				if(cdrmedajax.character2){
					//Reload when Dispensary save therapy
					window.location.href = cdrmedajax.save_therapy;
				}
				else if(cdrmedajax.character3){
					//Reload when patient save therapy
					window.location.hash = 'shoop';
					window.location.reload();
				}
			});
			
		});
		
		
		
		
	});
	
	// Save therapy at dispnser side
	jQuery('#save-therapy-btn2').click(function(){
		jQuery('#save-therapy-loader').show();
		var data_to_send_ajax = [];
		var therapy_details = jQuery(this).attr('data-details');
		therapy_details = therapy_details.split(',');
		console.log(therapy_details);
		var products_selected = cdrmed_steps['selected_prod'];
		var finilized_products = {};
		var other_data = {
			shoop_id : therapy_details[0],
			patient_id : therapy_details[1],
		};
		jQuery.each(products_selected, function(index, val) {
			var product_item = jQuery('.cdrmed_slide_item[data-product-id="'+val+'"]');
			var product_json = product_item.attr('data-product-json');
			finilized_products[val] = product_json;
		});
		var data_to_send = [other_data, finilized_products];
		//console.log(data_to_send);
		jQuery.post(wc_add_to_cart_params.ajax_url, {action:'save_patient_therapy_ajax', data:data_to_send}, function(data, textStatus, xhr) {
			jQuery('#save-therapy-loader').hide();
			jQuery('#save-therapy-btn').closest('.buttons').append(data);
			window.location.reload();
		});
	});
	
	
	
	
	
	jQuery('.cdmed_site_selectors').change(function() {
		var _this = jQuery(this);
		var patient_id = _this.attr('data-pid');
		var security_key = _this.attr('data-security');
		var site_url = _this.attr('data-url'); 
		var site_id = _this.attr('data-sid');
		if($(this).is(":checked")) {
			var returnVal = confirm("Are you sure?");
			$(this).attr("checked", returnVal);
			if(returnVal == false){ 
				setTimeout(function(){
					//alert('dd');
					_this.closest('label').removeClass('active');
				}, 20);
			}else if (returnVal) {
				_this.closest('.bizcontent').find('span').attr('class', 'fa fa-spinner fa-spin');
				jQuery.post(wc_add_to_cart_params.ajax_url, { action: 'cdrmed_clone_user_ajax', patient_id : patient_id, security : security_key, child_site_url : site_url  }, function(returned_data, textStatus, xhr) {
					jQuery.post(wc_add_to_cart_params.ajax_url, { action: 'cdrmed_after_clone_user_ajax', patient_id : patient_id, security : security_key, data: returned_data, site_id: site_id }, function(data, textStatus, xhr) {  });
					_this.closest('.bizcontent').find('span').attr('class', 'glyphicon glyphicon-ok glyphicon-lg');			
				});
			}
		}else{
			var returnVal = confirm("Your All Records associated with this Dispensary/Medical Clinic will be removed from there and you won't be able to access that dispensary. Are you sure you want to do it?");
			if(returnVal == false){
				setTimeout(function(){
					//alert(returnVal);
					$(this).attr("checked", true);
					_this.closest('label').addClass('active');
				}, 20);
			}else{
				jQuery.post(wc_add_to_cart_params.ajax_url, { action: 'cdrmed_remove_user_from_site', patient_id : patient_id, security : security_key, site_id: site_id, child_site_url : site_url }, function(data, textStatus, xhr) { 

				});
			}
		}
	});
	
	
	
	
	
});


var import_shoop_limit = 2;
var import_shoop_offset = 0;


//Show modal to import shoop
$(document).on("click", '.import_shoop_modal_btn', function(event) {
	
	jQuery(".import_shoop_modal .modal-body").html('<img style="display:inherit;max-width:100px;width: 100px;margin: 5px auto;" src="'+cdrmedajax.img_url+'/loading-secure-animation.gif" alt="Loading" />');
	
	import_shoop_offset = 0;
	
	var patient_id = $(this).data('patient_id');
	jQuery(".import_shoop_modal #import_shoop_patient_id").val(patient_id);
	//call from shoop-system/import-shoop.php
	var data = {
		action: 'import_shoop_ajax',
		'patient_id': patient_id,
	};
	jQuery.post(cdrmedajax.ajax_url, data, function(response) {
		var response = jQuery.parseJSON(response);
		jQuery(".import_shoop_modal .modal-body").html(response.output);
	});
	
	jQuery(".import_shoop_modal").modal('show');
});


// scroll pagination on import shoop modal
jQuery(function($){
    $('.import_shoop_modal .modal-body').bind('scroll', function(){
		if($(this).scrollTop() + $(this).innerHeight()>=$(this)[0].scrollHeight){
			jQuery(".import_shoop_modal .feedback").html('Loading More Shoops');
            import_shoop_offset = import_shoop_limit + import_shoop_offset;
			var patient_id = jQuery(".import_shoop_modal #import_shoop_patient_id").val();
			var data = {
				action: 'import_shoop_ajax',
				'limit': import_shoop_limit,
				'offset': import_shoop_offset,
				'patient_id': patient_id,
			};
			jQuery.post(cdrmedajax.ajax_url, data, function(response) {
				jQuery(".import_shoop_modal .feedback").html('');
				var response = jQuery.parseJSON(response);
				jQuery(".import_shoop_modal .modal-body").append(response.output);
			});
			
		}
	});
});

// import shoop button
$(document).on("click", '.import_shoop_send_to_patient', function(event) {
	
	$(this).html('Sending. Please wait...');
	$(this).attr('disabled','disabled');
	
	var shoop_id = $(this).data('shoop_id');
	var patient_id = $(this).data('patient_id');
	var send_shoop = $(this).data('click_by');
	var call_by = $(this).data('call_by');
	//call from shoop-system/import-shoop.php
	var data = {
		action: 'send_shoop_to_patient_ajax',
		'call_by': call_by,
		'shoop_id': shoop_id,
		'patient_id': patient_id,
	};
	jQuery.post(cdrmedajax.ajax_url, data, function(response) {
		
		$('.'+call_by+' .feedback').show();
		$('.'+call_by+' .feedback').addClass('success-message');
		$('.'+call_by+' .feedback').html(response);
		jQuery(".import_shoop_modal .feedback").html(response);
		jQuery(".import_shoop_modal .feedback").addClass('success-message');
		setTimeout( function() {
			window.location.href = cdrmedajax.shoop_for_patient+'?pid='+patient_id;
		}, 2000);
	});
});


/*
*
*	Remove Shoop
*
*/
$(document).on("click", '.delete-patient-shoop', function(event) {
	if (confirm('Do you want to Remove this Shoop?')){
	}
	else{
		return false;
	}
	$(this).html('Please wait...');
	$(this).attr('disabled','disabled');
	$('.auto-shoop-message').show();
	$('.auto-shoop-message').html('<img src="'+cdrmedajax.img_url+'/loading-secure-animation.gif" alt="Loading" />');
	var shoop_id = $(this).data('shoop_id');
	var patient_id = $(this).data('patient_id');
	var post_id = $(this).attr('data-id');
	var data = {
		action: 'remove_patient_shoop_ajax',
		'shoop_id': shoop_id,
		'patient_id': patient_id,
	};
	jQuery.post(cdrmedajax.ajax_url, data, function(response) {
		$('.auto-shoop-message').html(response);
		$('.delete-patient-shoop').removeAttr('disabled');
		reset_feedback('success-message', true);
	});
});


//ajax call for auto shoop
$(document).on("click", '.generate-auto-shoop', function(event) {
	$('.auto-shoop-message').show();
	$('.auto-shoop-message').html('<img src="'+cdrmedajax.img_url+'/loading-secure-animation.gif" alt="Loading" />');
	
	var patient_id = $(this).data('patient_id');
	//call in auto-shoop folder
	var data = {
		action: 'generate_auto_shoop',
		'patient_id': patient_id,
	};
	jQuery.post(cdrmedajax.ajax_url, data, function(response) {
		var response = jQuery.parseJSON(response);
		$('.auto-shoop-message').html(response.output);
		if(response.action){
			$('.auto-shoop-message').addClass('success-message');
			setTimeout( function() {
				window.location.reload();
			}, 2000);
		}
		else{
			$('.auto-shoop-message').addClass('error-message');
			setTimeout( function() {
				$('.auto-shoop-message').removeClass('error-message');
				$('.auto-shoop-message').hide();
			}, 2000);
		}
	});
	
});


/*
==================================================================================
		Product
==================================================================================
*/


jQuery(document).ready(function(){
	
	$(document).on('submit', '#product-form', function(e){
		e.preventDefault();
		var formData = JSON.stringify($("#product-form").serializeArray());
		var data = {
			'action': 'product_form_ajax_request',
			'data': formData
		};
		jQuery.post(wc_add_to_cart_params.ajax_url, data, function(response) {
			alert('Got this from the server: ' + response);
			//$("#patient-message").append('<div id="feedback">'+response+'</div>');
		});
	});
	
});

/*
*
*	Remove Inventory Product
*
*/
$(document).on("click", '.inventory-product-delete', function(event) {
	if (confirm('Do you want to Remove this Inventory Product?')){
	}
	else{
		return false;
	}
	var product_id = $(this).attr('data-id');
	var data = {
		action: 'remove_inventory_product_ajax',
		'product_id': product_id,
	};
	jQuery.post(cdrmedajax.ajax_url, data, function(response) {
		$('.inventory-product-feedback').html(response);
		reset_feedback('success-message', true);
	});
});


/*
==================================================================================
		ACF
==================================================================================
*/


jQuery(document).ready(function(){
	
	// find user age by date of birth
	// this function is use in intake form
	find_user_age();
	$('#acf-field_56c2b5c142848').on('keyup',function(e){
		var unicode=e.keyCode? e.keyCode : e.charCode
		if(unicode >= 48 && unicode <= 57){
			find_user_age();
		}
	});
	function find_user_age(){
		// ajax call from general-functions/user-functions.php
		var dob = $('#acf-field_56c2b5c142848').val();
		if(dob){
			var tmp = dob.split('/');
			var month = tmp[0];
			var year = tmp[2];
			if(month.length == 2 && month > 0 && year.length == 4 && year > 1000 ){
				var data = {
					action: 'count_user_age_ajax',
					'dob': dob
				};
				jQuery.post(cdrmedajax.ajax_url, data, function(response) {
					if(response > 15){
						$("#field_57bd243249bd0").hide(); //Lansky Score
						$("#field_57bd246249bd3").show(); //Karnofsky Score show when age > 15
					}
					else if(response < 16){
						$("#field_57bd246249bd3").hide(); //Karnofsky Score
						$("#field_57bd243249bd0").show(); //Lansky Score show when age 0-15
					}
				});
			}
			else{
				$("#field_57bd246249bd3").hide(); //Karnofsky Score
				$("#field_57bd243249bd0").show(); //Lansky Score
			}
		}
		else{
			$("#field_57bd246249bd3").hide(); //Karnofsky Score
			$("#field_57bd243249bd0").show(); //Lansky Score
		}
	}

});


/*
==================================================================================
		Appointments
==================================================================================
*/


/* Patient Side Appointments */

jQuery(document).ready(function(){
});

var physician_limit = 9;
var physician_offset = 0;

//When Show Modal at Patient Side for Appointment
//$(document).on("click", '.physician_appointment_view_modal', function(event) {
function physician_appointment_view_modal(current_class){
	$('.step-now').val('2');
	appointment_step('dec');
	physician_offset = 0;
	fetch_all_physician();
}

//Previous Step
$(document).on("click", '.appointment-prev', function(event) {
	physician_offset = 0;
	appointment_step('dec');
	fetch_all_physician();
});


// find Steps for Appointment Step Modal
function appointment_step(symbol){
	var step = parseInt($('.step-now').val());
	$('.appointment-step-row-'+step).addClass('hide');
	if(symbol == 'dec'){
		step = --step;
		$('.appointment-prev').addClass('hide');
		$('.appointment-send').addClass('hide');		
		$('.title_span').html('Select One Physician');
	}
	else if(symbol == 'inc'){
		step = ++step;
		$('.appointment-prev').removeClass('hide');
	}
	$('.appointment-step-row-'+step).removeClass('hide');
	$('.step-now').val(step);
}


// js function for get all physician for appointment
function fetch_all_physician(){
		
	$('.cdrmed-modal-box #patient-physician-list').html('<img style="display: inherit; margin: 5px auto;" src="'+cdrmedajax.img_url+'/loading-secure-animation.gif" alt="" />');
	//call from appointments/appointment-ajax-call.php
	var data = {
		action: 'patient_get_physician_ajax',
	};
	
	jQuery.post(wc_add_to_cart_params.ajax_url, data, function(response) {
		var response = jQuery.parseJSON(response);
		if(response.action){
			$('#patient-physician-list').html(response.output);
			$('.cdrmed-modal-box-content .modal-footer').html('<div class="modal-body"><button type="button" class="btn btn-warning appointment-prev hide" id="previous_btn">Go Back</button><button type="button" class="btn btn-warning appointment-send hide" id="appointment-send">Fix Appointment</button></div>');
		}
		else{
			$('#patient-physician-list').html('<span class="feedback empty-message">No Physician Found!</span>');
		}
	});
}

// Physician server side Pagination with scroll
jQuery(function($){
    $('#patient-physician-list').bind('scroll', function(){
		if($(this).scrollTop() + $(this).innerHeight()>=$(this)[0].scrollHeight){
			jQuery(".cdrmed-modal-box-content .feedback").html('Loading More Physicians');
            physician_offset = physician_limit + physician_offset;
			var search = $('.patient-physician-search').val();
			var data = {
				action: 'patient_get_physician_ajax',
				'search': search,
				'limit': physician_limit,
				'offset': physician_offset
			};
			jQuery.post(cdrmedajax.ajax_url, data, function(response) {
				jQuery(".cdrmed-modal-box-content .feedback").html('');
				var response = jQuery.parseJSON(response);
				$('#patient-physician-list').html(response.output);
			});
		}
	});
});



// Search Physician by First OR Last Name
$(document).on("keyup", '.patient-physician-search', function(event) {
	$('.cdrmed-modal-box #patient-physician-list').html('<img style="display: inherit; margin: 5px auto;" src="'+cdrmedajax.img_url+'/loading-secure-animation.gif" alt="" />');
	var search = $(this).val();
	if(search == ''){
		physician_offset = 0;
	}
	var data = {
		action: 'patient_get_physician_ajax',
		'search': search,
		'limit': physician_limit,
		'offset': physician_offset
	};
	jQuery.post(cdrmedajax.ajax_url, data, function(response) {
		var response = jQuery.parseJSON(response);
		if(response.action){
			$('#patient-physician-list').html(response.output);
		}
		else{
			$('#patient-physician-list').html('<span class="feedback empty-message">No Physician Found!</span>');
		}
	});
});



// When click on a Physician
$(document).on("click", '.single-physician-select', function(event) {
	
	$('#patient-physician-list').html('');
	appointment_step('inc');
	
	var physician_id = $(this).data('physician_id');
	
	var name = $(this).data('name');
	var appointment_system = $(this).data('appointment_system');
	var url = $(this).data('url');
	
	if(appointment_system == 'Calendly Appointment System'){
		$('.appointment-selection').show();
		$('.title_span').html(name+' Calendary');
		$('.cdrmed-appointment-selection').addClass('hide');
		$('.appointment-selection').html('<a class="btn btn-default" href="#" onclick="'+url+'">Click Here for Appointment</a>');
	}
	else{
		
		$('.appointment-selection').hide();
		$('.appointment-selection').html('');
		jQuery('#patient_side_appointment_modal .phone').inputmask("(999) 999-9999");
		
		$('.appointment-send').removeClass('hide');
		$('.title_span').html(name+' MednCures Appointment');
		
		$('.cdrmed-appointment-selection').removeClass('hide');
		$('#physician_id').val(physician_id);
	}
});

jQuery(document).on("keydown", '#patient_side_appointment_modal .mdates', function(event) {
	jQuery('#patient_side_appointment_modal .mdates').inputmask("mm/dd/yyyy");
});



// Send Call when Physician have MednCures Appointment System
$(document).on("click", '.appointment-send', function(event) {
	event.preventDefault();
	var physician_id = $('#physician_id').val();
	var first_name = $('#app_first_name').val();
	var last_name = $('#app_last_name').val();
	var email = $('#app_email').val();
	var phone = $('#app_phone').val();
	var date = $('#app_date').val();
	var time = $('#app_time option:selected').text();
	var time_perority = $('#app_time').val();
	
	var error_flag = false;
	
	var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/igm;
	var date_regex = /^(0[1-9]|1[0-2])\/(0[1-9]|1\d|2\d|3[01])\/(19|20)\d{2}$/;
	
	if(first_name == ''){
		$('.modal-box-feedback').html('Enter First Name');
		error_flag = true;
	}
	else if(last_name == ''){
		$('.modal-box-feedback').html('Enter Last Name');
		error_flag = true;
	}
	else if(email == ''){
		$('.modal-box-feedback').html('Enter Email');
		error_flag = true;
	}
	else if (!re.test(email)) {
		$('.modal-box-feedback').html('Enter Valid Email');
		error_flag = true;
	}
	else if(phone == ''){
		$('.modal-box-feedback').html('Enter Phone');
		error_flag = true;
	}
	else if(date == ''){
		$('.modal-box-feedback').html('Enter Date');
		error_flag = true;
	}
	else if (!(date_regex.test(date))) {
		$('.modal-box-feedback').html('Enter Valid Date');
		error_flag = true;
	}

	if(error_flag){
		//reset_modal_feedback(time, feedback-class, page-reload)
		reset_modal_feedback('error-message', false);
		return false;
	}
	
	var data = {
		action: 'send_appointment_ajax',
		'physician_id': physician_id,
		'first_name': first_name,
		'last_name': last_name,
		'email': email,
		'phone': phone,
		'date': date,
		'time': time,
		'time_perority': time_perority,
	};
	jQuery.post(cdrmedajax.ajax_url, data, function(response) {
		var response = jQuery.parseJSON(response);
		$('.modal-box-feedback').html(response.output);
		if(response.action){
			reset_modal_feedback('success-message', true);
		}
		else{
			reset_modal_feedback('error-message', false);
		}
	});
	
});




/* Physician Side */

//When Show Appointments Modal at physician Side
//$(document).on("click", '.appointment_view_modal', function(event) {
function appointment_view_modal(current_class){	
	$('.cdrmed-modal-box-content .patient-appointment-detail').html('<img style="display: inherit; margin: 5px auto;" src="'+cdrmedajax.img_url+'/loading-secure-animation.gif" alt="" />');
	var call = $(current_class).attr('data-call');
	//call from appointments/appointment-ajax-call.php
	var data = {
		action: 'read_appointments_ajax',
		'by': 'physician',
		'call': call,
	};
	jQuery.post(cdrmedajax.ajax_url, data, function(response) {
		var response = jQuery.parseJSON(response);
		if(response.action){
			$('.cdrmed-modal-box-content .patient-appointment-detail').html(response.output);
		}
		else{
			$('.cdrmed-modal-box-content .patient-appointment-detail').html('<span class="feedback empty-message">No Appointment Found!</span>');
		}
	});
	
}

$(document).on("click", '.all-appointments .panel-heading', function(event) {
	if ($(this).hasClass('panel-collapsed')) {
		// expand the panel
		$(this).parents('.panel').find('.panel-body').slideDown();
		$(this).removeClass('panel-collapsed');
		$(this).addClass('panel-closed');
		$(this).removeClass('panel-opened');
		//$(this).find('i').removeClass('fa-chevron-down').addClass('fa-remove');
		$('.appointment-collapse').addClass('body-border');
	}
	else {
		// collapse the panel
		$(this).parents('.panel').find('.panel-body').slideUp();
		//$('.physician-single-appointment .panel-heading').removeClass('panel-collapsed');
		$(this).addClass('panel-collapsed');
		$(this).addClass('panel-opened');
		$(this).removeClass('panel-closed');
		//$(this).find('i').removeClass('fa-remove').addClass('fa-chevron-down');
		$('.appointment-collapse').removeClass('body-border');
	}
});


// search appointments when click on datpicker apply button
$(document).on("click", '.search-by-date-search-btn', function(event) {
	
	$('.cdrmed-modal-box-content .patient-appointment-detail').html('<img style="display: inherit; margin: 5px auto;" src="'+cdrmedajax.img_url+'/loading-secure-animation.gif" alt="Loading" />');
	var search = $('.physician-appointment-date-search').val();
	//console.log(search)
	var data = {
		action: 'read_appointments_ajax',
		'by': 'physician',
		'by_search': 'date',
		'search': search,
	};
	jQuery.post(cdrmedajax.ajax_url, data, function(response) {
		var response = jQuery.parseJSON(response);
		if(response.action){
			$('.cdrmed-modal-box-content .patient-appointment-detail').html(response.output);
		}
		else{
			$('.cdrmed-modal-box-content .patient-appointment-detail').html('<span class="feedback empty-message">No Appointment Found!</span>');
		}
	});
});

// Search Physician by First OR Last Name
$(document).on("keyup", '.physician-appointment-search', function(event) {
	
	$('.cdrmed-modal-box-content .patient-appointment-detail').html('<img style="display: inherit; margin: 5px auto;" src="'+cdrmedajax.img_url+'/loading-secure-animation.gif" alt="Loading" />');
	
	var search = $(this).val();
	var data = {
		action: 'read_appointments_ajax',
		'by': 'physician',
		'search': search,
	};
	jQuery.post(cdrmedajax.ajax_url, data, function(response) {
		var response = jQuery.parseJSON(response);
		if(response.action){
			$('.cdrmed-modal-box-content .patient-appointment-detail').html(response.output);
		}
		else{
			$('.cdrmed-modal-box-content .patient-appointment-detail').html('<span class="feedback empty-message">No Appointment Found!</span>');
		}
	});
});


// Appointment Confirm/ Cancel Action
$(document).on("click", '.all-appointments .appointment-action-btn', function(event) {
	$('.modal-box-feedback').html('<img style="display: inherit; margin: 5px auto;" src="'+cdrmedajax.img_url+'/loading-secure-animation.gif" alt="Loading" />');
	var call_type = $(this).data('action');
	if(call_type == 'remove'){
		if (confirm('Do you want to Remove this Appointment?')){
		}
		else{
			return false;
		}
	}
	var id = $(this).data('id');
	var data = {
		action: 'change_appointment_status_ajax',
		'call_type': call_type,
		'id': id,
	};
	jQuery.post(cdrmedajax.ajax_url, data, function(response) {
		$('.modal-box-feedback').html(response);
		reset_modal_feedback('success-message', true);
	});
});








/*
==================================================================================
		Commnets & Notes
==================================================================================
*/

// MednCures Ajax Call for Patient Notes Crud Functions
jQuery('.submit-note').live('click', function (e) {
	e.preventDefault();
	jQuery('#feedback').remove();
	jQuery('#patient-message1').show();
	//Get necessary details for posting Notes Data Properly
			var note_data = jQuery('#notescomments').val();
			var old_note = jQuery('#old_comments').val();
			var old_note_id = jQuery('#old_comment_id').val();
			var created_by = jQuery('#created_by').val();
			var creater_first_name = jQuery('#creater_first_name').val();
			var creater_last_name = jQuery('#creater_last_name').val();
			var note_type = jQuery('#note_type').val();
			var site_url = jQuery('#site_url').val();
			var patientid = jQuery('#submit-note').attr('data-patient');
			var data_type = jQuery('#submit-note').attr('data-type');
			var data = {
				'action': 'notes_crud_ajax_call',
				'patientid': patientid,
				'createdby': created_by,
				'cfirstname': creater_first_name,
				'clastname': creater_last_name,
				'notetype': note_type,
				'siteurl': site_url,
				'datatype': data_type,
				'notedata': note_data,
				'oldnotedata':  old_note,
				'oldnotedataid':  old_note_id
			};
			jQuery.post(wc_add_to_cart_params.ajax_url, data, function(response) {
				$('#list-patient-notes').html('<img style="display:inherit;max-width:100px;width: 100px;margin: 5px auto;" src="'+cdrmedajax.img_url+'/loading-secure-animation.gif" alt="Loading" />');
				jQuery('#submit-note').attr('data-type','new_note');
				jQuery('#notescomments').attr("value", "");
				jQuery("#patient-message1").html('<div id="feedback" class="feedback success-message">'+response+'</div>');
				jQuery(".previous-listed-notes").fadeOut();
				setTimeout( function() {
					jQuery("#patient-message1").hide();
				}, 2000);
				// Second Ajax Call for Refreshing Notes Data Once Again
				var data = {
					'action': 'print_notes_on_page',
					'patientid': patientid,
					'note_type': note_type
				};
				
				jQuery.post(wc_add_to_cart_params.ajax_url, data, function(response) {
				jQuery("#list-patient-notes").html('<div class="previous-listed-notes">'+response+'</div>');
				});
				
			});
		
});



/*
==================================================================================
		MMJ Recommendation
==================================================================================
*/


/* jQuery(document).ready(function($) {
	$(".show-mmj-recommendation").live("click", function(){ });
}); */

function show_mmj_recommendation(current_class){
	$('.cdrmed-modal-box-content .modal-dialog').addClass('width700');
		
	$('.cdrmed-modal-box-content .cdrmed-modal-box .modal-body .body-contents .mmj-recommendation-form-acf').html('<img style="display: inherit; margin: 5px auto;" src="'+cdrmedajax.img_url+'/loading-secure-animation.gif" alt="" />');

	var patient_id = $(current_class).attr('data-patient-id');
	
	var data = {
		action: 'edit_patient_mmj_recommendation_ajax',
		'patient_id': patient_id,
	};
	jQuery.post(cdrmedajax.ajax_url, data, function(response) {
		$('.cdrmed-modal-box-content .cdrmed-modal-box .modal-body .body-contents .mmj-recommendation-form-acf').html(response);
		
	});
}



/*
==================================================================================
		Inventory Orders
==================================================================================
*/


jQuery(document).ready(function(){
	
	$(document).on('click', '.send_enquiry_now_btn', function(e){
		
		$('.send_enquiry_now_btn').html('waiting...');
		$('.send_enquiry_now_btn').attr('disabled','disabled');
		e.preventDefault();
		var data = {
			'action': 'send_enquiry_ajax_call'
		};
		jQuery.post(cdrmedajax.ajax_url, data, function(response) {
			$('.send_enquiry_now_btn').html(response);
			setTimeout( function() {
				window.location.href = cdrmedajax.site_url;
			}, 2000);
		});
	});
	
});


$(document).on("click", '.inventory-orders .panel-heading', function(event) {
	
	if ($(this).hasClass('panel-collapsed')) {
		
		var order_id = $(this).attr('order-id');
		$('.inventory-orders .single-order .order_id'+order_id+' .panel-body').html('<img style="display: inherit; margin: 5px auto;" src="'+cdrmedajax.img_url+'/loading-secure-animation.gif" alt="" />');
		
		// expand the panel
		$(this).parents('.panel').find('.panel-body').slideDown();
		$(this).removeClass('panel-collapsed');
		$(this).addClass('panel-closed');
		$(this).removeClass('panel-opened');
		$('.inventory-order-collapse').addClass('body-border');
		
		var data = {
			action: 'show_order_items_ajax',
			'order_id': order_id,
		};
		jQuery.post(wc_add_to_cart_params.ajax_url, data, function(response) {
			$('.inventory-orders .single-order .order_id'+order_id+' .panel-body').html(response);
		});
		
	}
	else {
		// collapse the panel
		$(this).parents('.panel').find('.panel-body').slideUp();
		$(this).addClass('panel-collapsed');
		$(this).addClass('panel-opened');
		$(this).removeClass('panel-closed');
		$('.inventory-order-collapse').removeClass('body-border');
	}
});

$(document).on("keyup", '.inventory-orders .order-search', function(event) {
	
	var order_id = $(this).val();
	$('.inventory-orders .single-order').hide();
	$('.inventory-orders .no-order').removeClass('feedback');
	$('.inventory-orders .no-order').removeClass('error-message');
	$('.inventory-orders .no-order').html('');	
    if(order_id) {
        
        $('.inventory-orders .single-order').hide();
        $(".inventory-orders .single-order[data-search*='"+ order_id +"']").show();
        if(!$('.inventory-orders .single-order:visible').get(0)){
            $('.inventory-orders .no-order').addClass('feedback');
            $('.inventory-orders .no-order').addClass('error-message');
            $('.inventory-orders .no-order').html('Type correct Order #');
        }
    }else{
		$('.inventory-orders .single-order').show();	
    }
});

$(document).on("click", '.inventory-orders .order-action-btn', function(event) {
	
	var call_type = $(this).attr('data-action');
	var data_class = $(this).attr('data-class');
	var data_status = $(this).attr('data-status');
	
	if(call_type == 'remove'){
		if (confirm('Do you want to Remove this Order?')){
		}
		else{
			return false;
		}
	}
	$('.inventory-orders .feedback').html('<img style="display: inherit; margin: 5px auto;" src="'+cdrmedajax.img_url+'/loading-secure-animation.gif" alt="" />');
	
	var order_id = $(this).attr('data-id');
	var data = {
		action: 'change_order_status_ajax',
		'call_type': call_type,
		'order_id': order_id,
	};
	jQuery.post(cdrmedajax.ajax_url, data, function(response) {
		
		if(call_type == 'remove'){
			$('.inventory-orders .single-order .order_id'+order_id+'').remove();
		}
		else{
			$('.inventory-orders .single-order .order_id'+order_id+' .panel-body .order-info .status').html(data_status);
		}
		
		var old_status = $('.inventory-orders .single-order .order_id'+order_id).attr('data-status');
		
		var old_count = $('.inventory-orders .order-counter .'+old_status+' span').html();
		old_count--;
		$('.inventory-orders .order-counter .'+old_status+' span').html(old_count);
		
		var new_count = $('.inventory-orders .order-counter .'+data_class+' span').html();
		new_count++;
		$('.inventory-orders .order-counter .'+data_class+' span').html(new_count);
		
		$('.inventory-orders .single-order .order_id'+order_id+' .panel-heading').removeClass('on-hold processing completed cancelled');
		$('.inventory-orders .single-order .order_id'+order_id+' .panel-heading').addClass(data_class);
		
		$('.inventory-orders .single-order .order_id'+order_id).attr('data-status',data_class);
		
		
		$('.inventory-orders .feedback').html(response);
		$('.inventory-orders .feedback').addClass('success-message');
		setTimeout( function() {
				$('.inventory-orders .feedback').html('');
				$('.inventory-orders .feedback').removeClass('success-message');
		}, 2000);
		
		//window.location.reload();
		//$('.modal-box-feedback').html(response);
		//reset_modal_feedback('success-message', true);
	});
});
