<?php

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly 

function create_disease_table() {
	
	
	$path = ABSPATH . 'wp-config.php';
  	require_once ($path);
	global $wpdb;
	$charset_collate = $wpdb->get_charset_collate();
	$table_name = $wpdb->prefix . 'cdrmed_disease';
	
	$sql = "CREATE TABLE IF NOT EXISTS $table_name (
  `id` int(7) NOT NULL AUTO_INCREMENT,
  `category` varchar(255) NOT NULL,
  `diagnosis` varchar(255) NOT NULL,
  `icd_code` varchar(255) NOT NULL,
  `additional_description` text NOT NULL,
  `status` smallint(5) NOT NULL,
  `medication` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB";
	
	require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
	dbDelta( $sql );
}

?>