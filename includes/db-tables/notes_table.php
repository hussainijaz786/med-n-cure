<?php

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly 

function create_notes_table() {
	
	
	$path = ABSPATH . 'wp-config.php';
  	require_once ($path);
	global $wpdb;
	$charset_collate = $wpdb->get_charset_collate();
	$table_name = $wpdb->prefix . 'cdrmed_notes';
	
	$sql = "CREATE TABLE IF NOT EXISTS $table_name ( `id` INT(99) NOT NULL AUTO_INCREMENT , `patient_id` INT(99) NOT NULL , `user_name` VARCHAR(250) NOT NULL , `user_id` INT(99) NOT NULL , `value` TEXT NOT NULL , `note_type` TEXT NOT NULL , `date_created` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP , PRIMARY KEY (`id`)) ENGINE = InnoDB";
	
	require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
	dbDelta( $sql );
}

?>