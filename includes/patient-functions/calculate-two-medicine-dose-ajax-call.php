<?php
if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly



add_action('wp_ajax_calculate_two_medicine_dose_ajax','calculate_two_medicine_dose_ajax');
add_action('wp_ajax_npriv_calculate_two_medicine_dose_ajax','calculate_two_medicine_dose_ajax');

function calculate_two_medicine_dose_ajax(){
	
	
	/*
	$p1_strain1 = 70;
	$p1_strain2 = 3;
	$p2_strain1 = 3;
	$p2_strain2 = 60;
	$target_dose1 = 200;
	$target_dose2 = 400;
	$dose_frequency1 = 1;
	$dose_frequency2 = 1; */
	
	$p1 = $_POST['pro_ids'][0];//15522;
	$p2 = $_POST['pro_ids'][1];//15544;
	
	$p1_strain1 = get_post_meta($p1, '_azcl_thc', true);
	$p1_strain2 = get_post_meta($p1, '_azcl_cbd', true);
	$p2_strain1 = get_post_meta($p2, '_azcl_thc', true);
	$p2_strain2 = get_post_meta($p2, '_azcl_cbd', true);
	
	$patient_id = isset($_POST['patient_id']) ? $_POST['patient_id'] : get_current_user_id();
	
	$latest_shoop_id = get_user_meta($patient_id, 'latest_patient', true);
	
	// get last selected shoop data
	$target_dosage1 = explode('X',get_post_meta($latest_shoop_id, 'medicine1_target_dosage_1', true));
	$target_dose1 = $target_dosage1[0];
	$target_dosage2 = explode('X',get_post_meta($latest_shoop_id, 'medicine2_target_dosage_1', true));
	$target_dose2 = $target_dosage2[0];
	$dose_frequency1 = get_post_meta($latest_shoop_id, 'medicine1_frequency_1', true);
	$dose_frequency2 = get_post_meta($latest_shoop_id, 'medicine2_frequency_1', true);
	//save values in array and that array return to js
	$therapy_array = array();
	$therapy_array['p1'] = $p1;
	$therapy_array['p2'] = $p2;
	$therapy_array['p1_strain1'] = $p1_strain1;
	$therapy_array['p1_strain2'] = $p1_strain2;
	$therapy_array['p2_strain1'] = $p2_strain1;
	$therapy_array['p2_strain2'] = $p2_strain2;
	$therapy_array['target_dose1'] = $target_dose1;
	$therapy_array['target_dose2'] = $target_dose2;
	$therapy_array['dose_frequency1'] = $dose_frequency1;
	$therapy_array['dose_frequency2'] = $dose_frequency2;
	
	$dose_calculation_check = true;
	
	// pass parameta for calculate medicicne 1 per day
	$med1_per_day =  med1_strain_per_day($p1_strain1, $p1_strain2, $p2_strain1, $p2_strain2, $target_dose1, $target_dose2);
	// pass parameta for calculate medicicne 2 per day
	$med2_per_day =  med2_strain_per_day($p1_strain1, $p1_strain2, $p2_strain1, $p2_strain2, $target_dose1, $target_dose2);
	
	//push dosage ammount values
	$therapy_array['med1_amount1'] = ROUND($p1_strain1 * 10 * $med1_per_day, 2);
	$therapy_array['med1_amount2'] = ROUND($p1_strain2 * 10 * $med1_per_day, 2);
	
	//value for display
	//medicine per day
	$therapy_array['med1_per_day'] = ROUND($med1_per_day, 2);
	//value for save in meta key
	$therapy_array['med1_per_day1'] = $med1_per_day;
	//value for display
	//medicine per day
	$therapy_array['med2_per_day'] = ROUND($med2_per_day, 2);
	//value for save in meta key
	$therapy_array['med2_per_day1'] = $med2_per_day;
	
	// pass parameta for calculate medicicne 1 per dose
	$med1_per_dose =  med1_strain_per_dose($dose_frequency1, $med1_per_day);
	// pass parameta for calculate medicicne 2 per dose
	$med2_per_dose =  med2_strain_per_dose($dose_frequency2, $med2_per_day);
	
	//push dosage ammount values
	$therapy_array['med2_amount2'] = ROUND($p2_strain1 * 10 * $med2_per_day, 2);
	$therapy_array['med2_amount1'] = ROUND($p2_strain2 * 10 * $med2_per_day, 2);
	
	// medicine per dose
	$therapy_array['med1_per_dose'] = ROUND($med1_per_dose, 2);
	$therapy_array['med2_per_dose'] = ROUND($med2_per_dose, 2);
	
	$output = '';
	//check if calculation is less than 0 then show message
	if($med1_per_day < 0 || $med2_per_day < 0 || $med1_per_dose < 0 || $med2_per_dose < 0){
		$dose_calculation_check = null;
		$output = '<h3 class="no-therapy">Dosage information cannot be calculated. Required Cannabinoid Values are low / negative in Product. Please contact your Physician/Dispensary.</h3>';
	}
	
	//return true or null
	$therapy_array['dose_calculation_check'] = $dose_calculation_check;
	$therapy_array['output'] = $output;
	echo json_encode($therapy_array);
	die();
}
//medicinme 1 per day calculation
function med1_strain_per_day($d6, $d7, $d8, $d9, $d10, $d11){
	return ((($d9/100)*($d10/1000)) - (($d8/100)*($d11/1000))) / ( (($d9/100)*($d6/100)) - (($d8/100) * ($d7/100)));
}
//medicinme 2 per day calculation
function med2_strain_per_day($d6, $d7, $d8, $d9, $d10, $d11){
	return ((($d7/100)*($d10/1000)) - (($d6/100)*($d11/1000))) / ( (($d7/100)*($d8/100)) - (($d6/100) * ($d9/100)));
}
//medicinme 1 per dose calculation
function med1_strain_per_dose($d12, $d14){
	return $d14/$d12;
}
//medicinme 2 per dose calculation
function med2_strain_per_dose($d12, $d15){
	return $d15/$d12;
}
