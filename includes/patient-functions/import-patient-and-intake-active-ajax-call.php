<?php

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly 

/*
*
*	AJAX Call for Approve/Reject Physician/Dispensary Request
*
*/
add_action( 'wp_ajax_approve_reject_import_request_ajax', 'approve_reject_import_request_ajax');
add_action( 'wp_ajax_nopriv_approve_reject_import_request_ajax', 'approve_reject_import_request_ajax');

function approve_reject_import_request_ajax() {
	global $wpdb;
	$user_id = $_POST['user_id'];
	// user action is approve/reject
	$user_action = $_POST['user_action'];
	$current_usid = get_current_user_id();
	if($user_action == 'approve'){
		//get user role of request id
		$user_role = cdrmed_get_user_role($user_id);
		$meta_key = 'patient_physician_id';
		if(	$user_role == 'dispensary'){
			$meta_key = 'patient_dispensary_id';
		}
		
		// get intake id of current loged in user
		$patient_intake = get_user_meta($current_usid, 'patient_intake', true);
		$patient_parent_user_id = $wpdb->get_var( $wpdb->prepare( "select meta_value from $wpdb->postmeta where meta_key = 'patient_parent_user_id' and post_id = '%s' LIMIT 1", $patient_intake ) );
		if($patient_parent_user_id == 'orphan'){
			//if user have no parent already
			update_post_meta($patient_intake, 'patient_parent_user_id', $user_id);
		}
		
		$parent_ids = json_decode(get_user_meta($current_usid, $meta_key, true), true);
		$parent_ids[] = $user_id;
		update_user_meta( $current_usid, $meta_key, '' );
		update_user_meta($current_usid, $meta_key, json_encode($parent_ids));
		
		//If request for dispensary and action is approve then id add in patient-dispensary-product-list also
		if(	cdrmed_get_user_role($user_id) == 'dispensary'){
			$parent_product_ids = json_decode(get_user_meta($current_usid, 'patient_dispenary_products_id', true), true);
			$parent_product_ids[] = $user_id;
			update_user_meta($current_usid, 'patient_dispenary_products_id', json_encode($parent_product_ids));
		}
	}
	//get all request ids
	$request_ids = json_decode(get_user_meta($current_usid, 'import_request_ids', true), true);
	$new_ids = array();
	foreach($request_ids as $val){
		if($val != $user_id){
			$new_ids[] = $val;
		}
	}
	update_user_meta($current_usid, 'import_request_ids', '');
	if(sizeof($new_ids) > 0){
		update_user_meta($current_usid, 'import_request_ids', json_encode($new_ids));
	}
	$output = ($user_action == 'approve') ? 'Request approved successfully!' : 'Request rejected successfully!';
	echo $output;
	die();
}

/*
*	AJAX Call for Import patient 
*/
add_action( 'wp_ajax_import_patient_ajax', 'import_patient_ajax');
add_action( 'wp_ajax_nopriv_import_patient_ajax', 'import_patient_ajax');

function import_patient_ajax() {
	global $wpdb;
	
	$patient_id = $_POST['patient_id'];
	$user_info = get_userdata($patient_id);
	$patient_email = $user_info->user_email;
	$patient_role = $user_info->roles;
	//if user role is not patient
	if($patient_role[0] != 'patient'){
		echo json_encode(array('action' => null, 'output' => 'Enter Correct User ID'));
		die();
	}
	
	$current_usid = get_current_user_id();
	$current_usid = is_dispensary_physician_child($current_usid);
	$userrole = get_user_role();
	
	$meta_key = 'patient_physician_id';
	if(	$userrole == 'dispensary'){
		$meta_key = 'patient_dispensary_id';
	}
	
	$meta_value = $current_usid;
	$child_user = get_user_meta($meta_value, 'user_parent', true);
	if($child_user != ''){
		$meta_value = get_user_meta($meta_value, 'user_parent_id', true);
	}
	$query = "SELECT DISTINCT user_id FROM {$wpdb->prefix}usermeta
				WHERE `meta_key` = '$meta_key' AND `meta_value` like '%$meta_value%'
				ORDER BY user_id DESC";				
	$getpatients = $wpdb->get_results($query);
	//if user is already in patient list
	if(check_child_patient_id($getpatients, 'user_id', $patient_id)){
		echo json_encode(array('action' => null, 'output' => 'Patient already in your list'));
		die();
	}
	$flag = null;
	$request_ids = json_decode(get_user_meta($patient_id, 'import_request_ids', true), true);
	if($request_ids != '' && sizeof($request_ids) > 0){
		//if already send import request
		if(in_array($current_usid, $request_ids)){
			$flag = true;
		}
		else{
			$request_ids[] = $current_usid;
			update_user_meta($patient_id, 'import_request_ids', json_encode($request_ids));
		}
	}
	else if($request_ids == ''){
		update_user_meta($patient_id, 'import_request_ids', json_encode(array($current_usid)));
	}
	
	if($flag){
		echo json_encode(array('action' => null, 'output' => 'Your request already sent!'));
		die();
	}
	else{
		$name = get_user_meta($current_usid, 'first_name', true);
		$body_name = '';
		if($userrole == 'dispensary'){
			$body_name = ucwords($name).' wants to add you in Dispensary';
		}
		else{
			$body_name = ucwords($name).' wants to add you in Clinic';
		}
		
		update_user_meta($patient_id, 'import_patient_notification', 1);
		
		$args = array(
			'call_by' => 'import-patient',
			'receiver' => $patient_email,
			'subject_name' => $name.', is waiting for your approval', 
			'body_name' => $body_name.'\nPlease visit your Dashboard and goto Settings to approve or decline the request.', 
			'body_part' => '', 
			'password_reset_url' => '',
		);
		cdrmed_send_email($args);
		echo json_encode(array('action' => true, 'output' => 'Request sent to Patient successfully!'));
		
		cdrmed_save_activity_log('Import Request sent to Patient!', $patient_id);
		
	}
	die();
}

/*
* function for find already child patients
*/
function check_child_patient_id($array, $key, $val) {
    foreach ($array as $item)
        if($item->$key == $val)
            return true;
    return null;
}


/*
*	AJAX Call for Active Pre-Registeration
*/
add_action( 'wp_ajax_intake_active_ajax', 'intake_active_ajax');
add_action( 'wp_ajax_nopriv_intake_active_ajax', 'intake_active_ajax');

function intake_active_ajax() {
	$current_usid = get_current_user_id();
	$pre_reg_id = $_POST['post_id'];
	$user_name = $_POST['email_id'];
	$action = patient_intake_active($pre_reg_id, $user_name, true);
	$output = ($action) ? 'Patient Activated Successfully.' : 'User already exists';
	echo json_encode(array('action' => $action, 'output' => $output));
	die();
}
//patientadm1@gmail.com
/*
*	Function Take Three parameters
*	1- Pre-Registeration ID
*	2- Pre-Registeration Email
*	3- Check for Call
*/

function patient_intake_active($pre_reg_id, $user_name, $call_by = null) {
	
	$user_id = username_exists( $user_name );
	$intake_id_exists = get_user_meta($user_id, 'patient_intake', true);
	$user_email = $user_name;
	//check for user email already exists or not
	if ( !$user_id and email_exists($user_email) == false ) {
		
		$first_name = get_field( 'first_name', $pre_reg_id );
		$last_name = get_field( 'last_name', $pre_reg_id );
		$random_password = wp_generate_password( $length=12, $include_standard_special_chars=false );
		$user_id = wp_create_user( $user_name, $random_password, $user_email );
		$user_id_role = new WP_User($user_id);
		$user_id_role->set_role('patient');
		$now = date('Y-m-d H:i:s');
		$setuserid = wp_update_user( array( 'ID' => $user_id, 'first_name' => $first_name, 'last_name' => $last_name, 'display_name' => $first_name, 'nickname' => $first_name ) );
		
		// Check if Intake Already EXISTS for that User
		if($intake_id_exists) {
			$post_id = $intake_id_exists;
		} else {
			$my_post = array(
				'post_title'    => $user_email,
				'post_content'  => 'Intake Form details',
				'post_type'		=> 'intake',
				'post_status'		=> 'publish',
				'post_author'   => $user_id,
			);
			$post_id = wp_insert_post( $my_post );
		}
		/*
		*	Get Data from Pre-registeration
		*/
		$fname  = get_field( "first_name", $pre_reg_id );
		$lname 	= get_field( "last_name", $pre_reg_id );
		$email  = get_field( "best_email", $pre_reg_id );
		$phone  = get_field( "phone", $pre_reg_id );
		$dob 	= get_field( "date_of_birth", $pre_reg_id );
		$street = get_field( "street_address", $pre_reg_id );
		$street2 = get_field( "address2", $pre_reg_id );
		$city 	= get_field( "city", $pre_reg_id );
		$state  = get_field( "state", $pre_reg_id );
		$zip  	= get_field( "zip", $pre_reg_id );
		$gender = get_field( "gender", $pre_reg_id );
		$medications = get_field('please_list_your_current_pharmaceuticals_and_supplements',$pre_reg_id);
		$diagnosis  = get_field( "principal_primary_diagnosis", $pre_reg_id );
		$stages 	= get_field( "stage_cancer", $pre_reg_id );
		$cannibus  = get_field( "what_is_your_previous_experience_with_cannabis", $pre_reg_id );
		$interested_in  = get_field( "interested_in", $pre_reg_id );
		/*
		*	save data in intake
		*/
		update_field('field_56c2b5c140152',$fname , $post_id);
		update_field('field_56c2b5c140566', $lname, $post_id);
		update_field('field_56c47071c3827',$email, $post_id);
		update_field('field_56c2b5c1422dc',$phone , $post_id);
		update_field('field_56c2b5c142848', $dob, $post_id);
		update_field('field_56c2b5c140932',$street, $post_id);
		update_field('field_56c2b5c140933',$street2, $post_id);
		update_field('field_56c2b5c140d17',$city , $post_id);
		update_field('field_56c2b5c141109', $state, $post_id);
		update_field('field_56c2b5c1414e8',$zip, $post_id);
		update_field('field_56c2b5c1418e7', $gender, $post_id);
		update_field('field_56c2eb9ec5f99',$diagnosis , $post_id);
		update_field('field_56c2b5c1112ec', $stages, $post_id);
		update_field('field_56c47071c3827',$cannibus, $post_id);
		
		if($medications){
			foreach($medications as $medicine){ 
				$row = array(
					'field_56c2ed2038706' => $medicine['medication_name'],
					'field_56c2ed4138707'   => $medicine['dose'],
					'field_56c2ed5038708'  => $medicine['frequency']
				);
				$i = add_row('field_56c2ecf138705', $row, $post_id);
			}
		}
		
		$db = date("M-d-Y", strtotime($dob));
		$age = date_diff(date_create($db), date_create('now'))->y;
		update_post_meta( $post_id, 'patient_calculated_age', $age);
		//Save User ID in Pre-Registeration Post
		update_post_meta( $pre_reg_id, 'user-id-prereg', $user_id );
		$my_post = array(
			'ID'           => $pre_reg_id,
			'post_title'   => $user_email,
			'post_author' => $user_id,
		);
		/*
		*	save data in User Meta
		*/
		update_user_meta( $user_id, 'user_full_name', $fname.' '.$lname );
		update_user_meta( $user_id, 'pre-registration-id', $pre_reg_id );
		update_user_meta($user_id, 'patient_intake', $post_id);
		update_user_meta($user_id, 'interested_in', $interested_in);
		
		$patient_id = $user_id;
		$parent_id = get_post_meta($pre_reg_id, 'patient_parent_user_id', true);
		update_post_meta($post_id, 'patient_parent_user_id', $parent_id);
		if($parent_id == 'orphan' ){
			update_user_meta($patient_id, 'patient_physician_id', $parent_id);
		}
		else{
			$patient_parents = array($parent_id);
			update_user_meta($patient_id, 'patient_physician_id', json_encode($patient_parents));
		}
		
		$password_reset_url = password_reset_link($user_id);
		$args = array(
			'call_by' => 'post-registeration-patient',
			'receiver' => $user_email, 
			'subject_name' => $fname.', Your MednCures Membership Has Been Approved!', 
			'body_name' => '', 
			'body_part' => '', 
			'password_reset_url' => $password_reset_url,
		);	
		cdrmed_send_email($args);
		
		
		cdrmed_save_activity_log('Active Pre-Registeration', $patient_id);
		
		cdrmed_save_activity_log('Physician activate Patient', $patient_id);
		
		return true;
	}
	else{
		return null;
	}
}