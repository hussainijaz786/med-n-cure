<?php

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly 
function details_results_terpene() {
      $pid = $_GET['prodcut'];
   
          $meta = get_post_meta($pid);
         
          

            $html .= '<div class="table-responsive">
                            <table class="table table-bordered table-hover">
                                    <thead>
                                      <tr>
                                        <th>Compounds</th>
                                        <th>Percentage </th>
										<th>Test</th>
                                        <th>PPM</th>
                                      </tr>
                                    </thead>
                                    <tbody>';

            foreach ($meta as $key => $value) {
             
       
              if(strpos($key, "_azcl") !== false && $value[0] > 0){
                
                $name = ucwords(trim(str_replace("_azcl", "", $key),"_"));
                if(strpos($name, "_test") === false && strpos($name, "_ppm") === false){
                if(get_post_meta($pid, $key.'_tests',true)){
                  $tests = get_post_meta($pid, $key.'_tests',true);
                }elseif (get_post_meta($pid, $key.'_test',true)) {
                  $tests = get_post_meta($pid, $key.'_test',true);
                }else{
                  $tests = 0;
                }
         
         if (get_post_meta($pid, $key.'_ppm',true)) {
                  $ppm = get_post_meta($pid, $key.'_ppm',true);
                }else{
                  $ppm = 0.001;
                }
                  $html .= '<tr>';
                  $html .= '<td><b>'.$name.': </b></td>';
				  $html .='<td>'.$value[0].'%</td>';
                   $html .= '<td>'.$tests.'</td>';
                   $html .= '<td>'.$ppm.'</td>';
                    $html .= '<tr>';
                }
                
              }
              
            }
                               
                                 
                                     
            $html .= '</tbody></table></div>';
          
	
	

  return $html;
}
add_shortcode( 'details_results', 'details_results_terpene' );
?>
