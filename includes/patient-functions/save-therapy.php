<?php
if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly


add_action( 'wp_ajax_save_patient_therapy_ajax', 'save_patient_therapy_ajax' );
add_action('wp_ajax_npriv_save_patient_therapy_ajax','save_patient_therapy_ajax');
function save_patient_therapy_ajax(){
	
	$current_usid = get_current_user_id();
	$current_usid = is_dispensary_physician_child($current_usid);
	$data = $_POST['data'];
	$shoop_id = $data[0]['shoop_id'];
	$patient_id = $data[0]['patient_id'];
	//$product_id = $data[0]['product_id'];
	$userrole = get_user_role();
	$user_obj = get_userdata( $patient_id );
	$products_data = $data[1];
	// Create post object
	$therapy_post = array(
		'post_title'    => $user_obj->data->user_email,
		'post_content'  => '',
		'post_status'   => 'publish',
		'post_author'   => $patient_id,
		'post_type'	  => 'patient_therapy'
	);
	 
	// Insert the post into the database
	$created_post_id = wp_insert_post( $therapy_post );
	if($created_post_id){
		
		$medicine_counter = 0;
		for($i=1; $i < 5; $i++) {
			if(get_post_meta( $shoop_id, 'medicine'.($i).'_cannabinoid_1', true )){
				$medicine_counter++;
			}
		}
		// Therapy created by(current loged in User id)
		update_post_meta($created_post_id, 'therapy_by', $current_usid);
		//total medicine in shoop
		update_post_meta($created_post_id, '_total_medicine', $medicine_counter);
		update_post_meta($created_post_id, '_cdrmed_therapy_data', $products_data);
		//update_post_meta($created_post_id, '_cdrmed_therapy_data', '');
		update_post_meta($created_post_id, '_cdrmed_therapy_shoop_id', $shoop_id);
		update_user_meta($patient_id, '_cdrmed_therapy_id', $created_post_id);
		
		echo '<br><span class="feedback success-message">Therapy Saved Successfully!<br>Please wait while we notify your dispensary about your selected products.</span>';
	}
	if($userrole == 'dispensary'){
		cdrmed_save_activity_log('Patient Therapy Saved!', $patient_id);
	}
	else{
		cdrmed_save_activity_log('Patient Therapy Saved!', '');
	}
	
	die();
}


add_action( 'wp_ajax_mark_patient_dead', 'mark_patient_dead_callback' );

function mark_patient_dead_callback(){
	$user_id = intval( $_POST['patientsid'] );
	$status = $_POST['patientlife'];
	$ddate = $_POST['deathdate'];
	if($user_id) {
	update_user_meta( $user_id, 'patient_life_status', $status );
	update_user_meta( $user_id, 'patient_death_date', $ddate );
	echo "<span style='width: 100%; float: left; color: #39b56a;'>Patient Updated Successfully.</span>";
	}
	die();
	
}

add_action( 'wp_ajax_get_pat_life', 'get_pat_life_callback' );

function get_pat_life_callback(){
	$user_id = intval( $_POST['patientsid'] );
	if($user_id) {
	$lifestats = get_user_meta( $user_id, 'patient_life_status', true );
	$deathdate = get_user_meta( $user_id, 'patient_death_date', true );
	echo "<span style='width: 100%; float: left; color: #39b56a;'>Patient Updated Successfully.</span>";
	}
	die();
	
}