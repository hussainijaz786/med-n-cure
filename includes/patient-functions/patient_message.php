<?php

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly 
function set_my_cookie() {
       $cookie_name = "user";
      $cookie_value = get_current_user_id();
    // The $_REQUEST contains all the data sent via ajax
    if(!isset($_COOKIE[$cookie_name])) {
    setcookie($cookie_name, $cookie_value, time() + (3600*24*2), "/");
} 
     
    // Always die in functions echoing ajax content
   die();
}
 
add_action( 'wp_ajax_set_my_cookie', 'set_my_cookie' );
function patient_message() {
	$single = true;
  $postidpatient = get_user_meta( get_current_user_id(), 'pre-registration-id' ,$single);
  $current_user =  get_post_meta( $postid, 'user-id-prereg', get_current_user_id() );
  $expirydate  =  get_field( "marijuana_date_of_expiration", $postid );
 $date1 = new DateTime($expirydate);
$date2 = new DateTime(date("Y-m-d"));
$interval = $date1->diff($date2);
 $cookie_name = "user";
 $cookie_value = get_current_user_id();
    // The $_REQUEST contains all the data sent via ajax
 
if($interval->m > 0){
  //More then month
}else {
  //Less then month
  $current_user = wp_get_current_user();
  $all_meta_for_user = get_user_meta( get_current_user_id() );
if($postidpatient) {
$user_data = array(
   "first_name"=> get_field( "first_name", $postidpatient ),
   "last_name"=> get_field( "last_name", $postidpatient ),
   "email"=> get_field( "best_email", $postidpatient ),
   "phone"=> get_field( "phone", $postidpatient ),
   "address_1"=> get_field( "street", $postidpatient )." ".get_field( "city", $postidpatient ),
   "zipcode"=> get_field( "zip", $postidpatient ),
   "gender"=> get_field( "gender", $postidpatient )
);	
} else {
 $user_data = array( "first_name"=>$all_meta_for_user['first_name'][0],
      "last_name"=> $all_meta_for_user['last_name'][0],
      "birthdate"=>"",
      "phone"=>$all_meta_for_user['phone'][0],
      "email"=>$current_user->user_email, 
     "address_1"=>$all_meta_for_user['addr1'][0],
     "address_2"=>$all_meta_for_user['addr2'][0],
     "zipcode"=>$all_meta_for_user['zip'][0],
     "city"=>$all_meta_for_user['city'][0],
     "state"=>$all_meta_for_user['thestate'][0]);
}
$user_data_json = json_encode($user_data);
$preset = base64_encode($user_data_json);
$user_name = get_user_meta( get_current_user_id(), 'first_name' ,$single);
// link http://medncures.hellomd.com/signup?preset='.$preset.'
$link = 'https://medncures.hellomd.com/signup?preset='.$preset;
   $html = '<div class="row message">
  <div class="col-md-12 main">
  <div class="col-md-7">
  <div class="text">
<p class="main-text">
<b>'.$user_name.'</b>, Your California medical marijuana recomendation  <span class="red">will expire soon.</span><br>
You can <a class="blue-text" href="'.$link.'">renew now online</a> with a physician using HelloMD.<br>
<span class="gray">* HelloMD is a trusted partner of MednCures’s - All recomendations are 100% safe & secure</span>
<a href="javascript:void(0)" id="remind"  title="REMIND ME LATER"><img src="'.CDRMED.'/includes/assets/img/circle-cross.png" class="icon"></a></p>
  </div>
  </div>
  <div class="col-md-2">
<img src="'.CDRMED.'/includes/assets/img/hello.png" class="img-responsive logo">
  </div>
  <div class="col-md-3 green-button">
  <h3 class="renew"><a href="'.$link.'">RENEW MY CARD</a></h3>
  </div>
  </div>
</div>';
	
	}

  return $html;
}
add_shortcode( 'patient-message', 'patient_message' );
?>
