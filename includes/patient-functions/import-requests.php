<?php

/* 
File conatins the [edit_child_profile] shortcode for add new Child users
A popup window for Child User Update profile
*/

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly 


// Add Shortcode
function import_requests() {
	// Bail if not logged in or able to post
	if ( ! ( is_user_logged_in() ) ) {
		echo '<p>You Must Be Logged In To Add User</p>';
		return;
	}
	$html = '';
	$current_user = wp_get_current_user();
	$current_usid = get_current_user_id();
	
	$request_ids = json_decode(get_user_meta($current_usid, 'import_request_ids', true), true);
	
	$html .= '<div class="journey"><h3>Import Requests</h3></div>';
	$html .= '<div class="import-request-feedback feedback success-message" style="display:none; "></div>';
	$html .= '<div class="display-child-users">
		<table>
			<thead>
				<tr><th>Name</th><th>Action</th></tr>
			</thead>
			<tbody>';
			if(sizeof($request_ids) > 0){
				foreach ($request_ids as $value) {
					$user_info = get_userdata($value);
					$parent_role = $user_info->roles;
					$meta_key = 'physician_post_id';
					if(	$parent_role[0] == 'dispensary'){
						$meta_key = 'dispensary_post_id';
					}
					$post_id = get_user_meta($value, $meta_key, true);
					$name = get_the_title($post_id);
					$html .= '<tr>
					<td>'.$name.'</td>
					<td><a class="import_request_action" data-user_id="'.$value.'" data-action="approve">Approve</a>&nbsp;&nbsp;&nbsp;<a class="import_request_action" data-user_id="'.$value.'" data-action="reject">Reject</a></td>
					<td></td>
					</tr>';
				}
			}
			if(sizeof($request_ids) < 1){
				$html .= '<tr><td>No User Found!</td></tr>';
			}
			$html .= '</tbody>
		</table>
		</div>';
	
	return $html;
	
}

add_shortcode( 'import_requests', 'import_requests');