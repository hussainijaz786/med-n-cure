<?php
if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly 
function cdrmed_patient_dashboard_home(){
	  $url = site_url( '/patient-dashboard/', 'https' );
  ob_start();
  ?>
  <link rel="stylesheet" type="text/css" href="<?php echo plugins_url(); ?>/js_composer/assets/css/js_composer.css">
  <div class="content_row row vc_row wpb_row vc_row-fluid default-style default">
    <div class="content_row_wrapper">
      <div class="vc_col-sm-8 wpb_column vc_column_container">
        <div class="wpb_wrapper">
          <div class="wpb_text_column wpb_content_element ">
          <div class="wpb_wrapper">
            <?php echo do_shortcode('[feedback_form]'); ?>
          </div>
      </div>
      <div style="float: left;" class="rt_divider style-1 "></div>
      <div class="wpb_text_column wpb_content_element ">
        <div class="wpb_wrapper">
          <?php echo patient_health_feedback_list(); ?>
          <br><br>
          <center><a href="patient-health-journey"><button id="update_my_health1" name="submit" class="button">VIEW FULL HEALTH JOURNEY</button></a></center>
        </div>
      </div>
      </div>
    </div>
    <div class="vc_col-sm-4 wpb_column vc_column_container">
      <div class="wpb_wrapper">
        <div class="wpb_text_column wpb_content_element ">
          <div class="wpb_wrapper" style="text-align: center;">
          <?php 
          $intake_filled_tabs = $GLOBALS['intake_complete'];
          //echo count($intake_filled_tabs);
          if(count($intake_filled_tabs) == 1){ ?>
            <h3 style="text-align: center;">Health Intake Completed!</h3>
            <p style="text-align: center;"><img alt="intake-not-complete" src="<?php echo CDRMED; ?>/includes/assets/img/health-completed.png" class=""></p>
            <button id="update_my_health" name="submit" class="button">UPDATE MY HEALTH</button>
          <?php }else{ 
          ?>
            <h3 style="text-align: center;">Health Intake Not Complete…</h3>
            <p style="text-align: center;"><img alt="intake-not-complete" src="<?php echo CDRMED; ?>/includes/assets/img/health-not-complete.png" class=""></p>
            <button href="<?php echo $url; ?>#myhealth" id="update_my_health" name="submit" class="button">UPDATE MY HEALTH</button>
          <?php } ?>
          </div>
        </div>
      </div>
    </div>
  </div>
  </div>

<?php /*
<div class="content_row row vc_row wpb_row vc_row-fluid default-style default">
  <div class="content_row_wrapper  ">
  <div class="vc_col-sm-12 wpb_column vc_column_container">
    <div class="wpb_wrapper">
      <div class="vc_separator wpb_content_element vc_separator_align_center vc_sep_width_100 vc_sep_pos_align_center vc_sep_color_grey"><span class="vc_sep_holder vc_sep_holder_l"><span class="vc_sep_line"></span></span>   <h4>Browse All of MednCures's Products</h4><span class="vc_sep_holder vc_sep_holder_r"><span class="vc_sep_line"></span></span>
  </div>
    </div>
  </div>
</div>
</div>

<div class="content_row row vc_row wpb_row vc_row-fluid default-style default">
  <div class="content_row_wrapper  ">
  <div class="vc_col-sm-12 wpb_column vc_column_container">
    <div class="wpb_wrapper">
      <div class="woocommerce columns-4"><div class="product_holder clearfix border_grid fixed_heights woocommerce"> 
 <div class="row">
  <?php 
  $taxonomy = 'product_cat';
  $categories = array();
  $categories[] = get_term_by( 'slug', 'cannabis-extract', $taxonomy );
  $categories[] = get_term_by( 'slug', 'cannabis-infused-coconut-topical', $taxonomy );
  $categories[] = get_term_by( 'slug', 'cannabis-infused-mct-oil', $taxonomy );
  $categories[] = get_term_by( 'slug', 'cannabis-infused-olive-oil', $taxonomy );
  foreach ($categories as $term) {
    $thumbnail_id = get_woocommerce_term_meta( $term->term_id, 'thumbnail_id', true );
    $image = wp_get_attachment_url( $thumbnail_id );
    $term_link = get_term_link( $term );
  ?>
  <div class="col col-sm-3 product-category cdrmed product first" style="min-height: 335px;">
      <a href="<?php echo $term_link; ?>">
        <img width="300" height="300" alt="<?php echo $term->name; ?> " src="<?php echo $image; ?>">
      </a>
      
        <a class="button" href="<?php echo $term_link; ?>">
        <?php echo $term->name; ?> 
        </a>
      
  </div>
  <?php } ?>
</div></div></div>
    </div>
  </div>
</div>
</div>
*/ ?>
  <?php
  $markup_output = ob_get_contents();
  ob_end_clean();
  return $markup_output;
}
add_shortcode( 'cdrmed-patient-dashboard', 'cdrmed_patient_dashboard_home' );