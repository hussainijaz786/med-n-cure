<?php 

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly 

// Add Shortcode
function mmj_detail_function() {

	// Code
	$user_ID = get_current_user_id();
	$post_id = get_the_author_meta ( 'pre-registration-id', $user_ID );
	
	$physician_name = get_field('physician_name',$post_id);
	$physician_phone = get_field('physician_phone',$post_id);
	$reco_number = get_field('medical_marijuana_recommendation_number',$post_id);
	$mdoe = get_field('marijuana_date_of_expiration',$post_id);
	$physician_website = get_field('physician_or_clinic_verification_website',$post_id);
	$physician_recomendation = get_field('scanphoto_of_physician_recommendation',$post_id);
	$driving_license_number = get_field('drivers_license_or_id_number',$post_id);
	$scanned_license = get_field('scanphoto_of_drivers_license_or_id',$post_id);

	
	$info = '';
	
	if($physician_name || $reco_number || $mdoe || $physician_recomendation || $driving_license_number ) {
	
	$info .= '<div class="journey"><h3>My Medical Marijuana Recommendation:</h3><br><br></div>';
	$info .= '<div class="mmjrecomendation">';
	
	}
	
	if($physician_name){
	$info .='<div class="mmj-detail-row">';
    $info .='<span class="mmj-detail-label">Physician Name</span>';
    $info .='<div class="mmj-detail-value">'. $physician_name . '</div>';
    $info .='</div>';
	}
	
	if($physician_phone){
	$info .='<div class="mmj-detail-row">';
    $info .='<span class="mmj-detail-label">Physician Phone</span>';
    $info .='<div class="mmj-detail-value">'. $physician_phone . '</div>';
    $info .='</div>';
	}
	
	if($reco_number){
	$info .='<div class="mmj-detail-row">';
    $info .='<span class="mmj-detail-label">Medical Marijuana Recommendation Number</span>';
    $info .='<div class="mmj-detail-value">'. $reco_number . '</div>';
    $info .='</div>';
	}
	
	if($mdoe){
	$info .='<div class="mmj-detail-row">';
    $info .='<span class="mmj-detail-label">Recommendation Date of Expiration</span>';
    $info .='<div class="mmj-detail-value">'. $mdoe . '</div>';
    $info .='</div>';
	}
	
	if($physician_website){
	$info .='<div class="mmj-detail-row">';
    $info .='<span class="mmj-detail-label">Physician or Clinic Verification Website</span>';
    $info .='<div class="mmj-detail-value">'. $physician_website . '</div>';
    $info .='</div>';
	}
	
	if($physician_recomendation){
	$info .='<div class="mmj-detail-row">';
    $info .='<span class="mmj-detail-label">Scan/Photo of Physician Recommendation</span>';
    $info .='<div class="mmj-detail-value"><img src="'. $physician_recomendation . '"></div>';
    $info .='</div>';
	}
	
	if($driving_license_number){
	$info .='<div class="mmj-detail-row">';
    $info .='<span class="mmj-detail-label">Driver License or I.D. Number</span>';
    $info .='<div class="mmj-detail-value">'. $driving_license_number . '</div>';
    $info .='</div>';
	}
	
	if($scanned_license){
	$info .='<div class="mmj-detail-row">';
    $info .='<span class="mmj-detail-label">Scan/Photo of Drivers License or I.D</span>';
    $info .='<div class="mmj-detail-value"><img src="'. $scanned_license  . '"></div>';
    $info .='</div>';
	}
	
	$info .= '</div>';
	
	
	return $info;











}  // Function Ends Here...


add_shortcode( 'mmjdetail', 'mmj_detail_function' );
?>