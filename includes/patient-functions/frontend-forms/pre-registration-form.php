<?php
if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly


add_shortcode( 'pre-registration-form', 'pre_registration_shortcode' );

function pre_registration_shortcode() {
ob_start();

	// echo $sendtourl;
	acf_form(array(
					'post_id'		=> 'new_post',
					'new_post'		=> array(
						'post_type'		=> 'pre-registrations',
						'post_status'		=> 'publish',
						'post_title'	=> "Pre-Registration",
					),
					'label_placement' => 'top',
					'updated_message' => __("Your Registration Details are Submitted Successfully.", 'acf'),
					'submit_value'		=> 'Submit Pre-Registration'
				));
				
				
	$html = ob_get_contents();
		ob_get_clean();
		ob_end_flush();
		return $html;
				
}


/*
*
*	Pre-save Pre-Registeration Post Check email is already used or not
*	Validation for Future Date of Birth
*
*/
add_filter('acf/validate_value/name=best_email', 'pre_regist_pre_save_acf', 10, 4);
add_filter('acf/validate_value/name=date_of_birth', 'pre_regist_pre_save_acf', 10, 4);
//add_filter('acf/validate_value', 'pre_regist_pre_save_acf', 10, 4);

function pre_regist_pre_save_acf( $valid, $value, $field, $input ){
	
	// bail early if value is already invalid
	if( !$valid ) {
		return $valid;
	}
	
	// Validation Filter for Pre-Registeration Email
	if($field['name'] == 'best_email'){
		// args to query for your key
		$args = array(
			'post_type' => 'pre-registrations',
			'meta_query' => array(
				array(
				   'key' => 'best_email',
				   'value' => $value
				)
			)
		);
		// perform the query
		$query = new WP_Query( $args );
		$duplicates = $query->posts;

		// do something if the key-value-pair exists in another post
		if ( ! empty( $duplicates ) ) {
			$valid = 'This Email Already Exists in the System and Cannot be used again for Registration';
		}
	}
	//validation for date of birth
	elseif($field['name'] == 'date_of_birth'){
		//date of birth value in date
		$dob = new DateTime($value);
		$current_date = new DateTime();
		if ($dob > $current_date){
			$valid = 'Date of Birth is Invalid!';
		}
	}
	
	return $valid;
}


add_action('acf/save_post', 'send_email_preregi');
function send_email_preregi( $post_id ) {
	// bail early if not a contact_form post
	if( get_post_type($post_id) !== 'pre-registrations' ) {		
		return;	
	}
	// bail early if editing in admin
	if( is_admin() ) {
		return;	
	}
	// vars
	$post = get_post( $post_id );
	
	//$current_usid = get_current_user_id();
	
	global $wpdb;
	$url = site_url();
	$physician_id = $wpdb->get_var( $wpdb->prepare( "select user_id from $wpdb->usermeta where meta_key = 'url' and meta_value = '%s' LIMIT 1", $url ) );
	$first_name = get_field('first_name', $post_id);
	$user_email = get_field('best_email', $post_id);
	if($physician_id){
		$parent_user_role = cdrmed_get_user_role($physician_id);
		if($parent_user_role != 'physician'){
			$physician_id = 5343;
		}
	}
	else{
		$physician_id = 5343;
	}
	
	if($physician_id == ''){
		
	}
	if($physician_id == ''){
		update_post_meta($post_id, 'patient_parent_user_id', 'orphan');
		patient_intake_active($post_id, $user_email, '');
	}
	else{
		$args = array(
			'call_by' => 'pre-registeration-patient',
			'receiver' => $user_email,
			'subject_name' => $first_name.', Thank You For Pre-Registring at MednCures', 
			'body_name' => '', 
			'body_part' => '',
			'password_reset_url' => '',
		);
		cdrmed_send_email($args);
		
		update_user_meta($physician_id, 'pre_registeration_notification', 1);
		
		update_post_meta($post_id, 'patient_parent_user_id', $physician_id);
		$physician_name = get_user_meta($physician_id, 'first_name', true);
		$physician_email = get_user_meta($physician_id, 'email', true);
		$siteurl = site_url( '/pre-registration-data/', 'http' );
		$password_reset_url = '<a href="'.$siteurl.'?poid='.$post_id.'">Click Here to view Patient Detail</a>';
		$args = array(
			'call_by' => 'pre-registeration-patient-physician',
			'receiver' => $physician_email,
			'subject_name' => 'Dr.'.$physician_name.', New Patient is Waiting in Pre-Registration', 
			'body_name' => $first_name, 
			'body_part' => '', 
			'password_reset_url' => $password_reset_url,
		);
		cdrmed_send_email($args);
	}
	
	
	if( get_post_type($post_id) == 'pre-registrations' && is_page('pre-registration') ) {
		wp_redirect( home_url('membership-confirmation') ); exit;
	}
}

?>