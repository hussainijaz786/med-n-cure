<?php

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly 


// Add Shortcode
function view_symptoms_data() {
	
    global $wpdb;
    $pid = !isset($_GET['pid'])?null:$_GET['pid'];
    if(isset($pid)){
        $userid = $pid;
    }else{
        $userid = get_current_user_id();
    }
	
	
	$msg = "<p class='text-center' style='margin-bottom:50px'><a href='".site_url()."/patient-dashboard/' class='btn bordered'>Go Back To Dashboard</a></p>";
	
	if(@$_GET['delete']){
		wp_delete_post($_GET['delete']);
		echo "<h2>Status deleted Successfully...</h2>";
	}
	
	$url = site_url()."/view-symptoms-updates/";
    add_filter( 'posts_where', 'filter_where' );
	
	$args = array(
		'posts_per_page' => -1,
		'orderby'          => 'date',
		'order'            => 'DESC',
		'post_type'        => 'symptoms_tracker',
		'author'           => $userid,
		'post_status'      => 'private'
	);
	$query = new WP_Query( $args );
	$posts = $query->posts;

	$html = '<section class="journey">
		<div class="container-fluid">
			<div class="row">
				<h1>Check Your Symptoms Health Journey!</h1>
				'.$msg.'
				<p class="title">Showing '.count($posts).' Health Check-Up Entries Between <span>'.date('F Y', strtotime('-2 month')).' - '.date('F Y').'</span></p><br/>
					
				<div class="symptoms-all-trackers-view">';
				
					$counter = 1;
					foreach ($posts as  $row) {
		
						$postid 		=     $row->ID;
						
						$cannabis       =     get_post_meta($postid,'cannabis',true);
						
						//$status 		=     get_post_meta($postid, 'feel',true);
						$anxious_mood   =     get_post_meta($postid, 'anxious_mood',true);
						$depressed_mood =     get_post_meta($postid,'depressed_mood',true);
						$happy          =     get_post_meta($postid, 'happy',true);
						$relaxed        =     get_post_meta($postid,'relaxed',true);
						$focused        =     get_post_meta($postid,'focused',true);
						$energetic      =     get_post_meta($postid,'energetic',true);
						$foggy_headed   =     get_post_meta($postid,'foggy_headed',true);
						$dizzy          =     get_post_meta($postid,'dizzy',true);
						$paranoid       =     get_post_meta($postid,'paranoid',true);
						$sleepy         =     get_post_meta($postid,'sleepy',true);
		
						if(empty($cannabis))
							$cannabis = 'No';
						
						if(empty($anxious_mood))
							$anxious_mood = 'None';
						if(empty($depressed_mood))
							$depressed_mood = 'None';
						if(empty($happy))
							$happy = 'None';
						if(empty($relaxed))
							$relaxed = 'None';
						if(empty($focused))
							$focused = 'None';
						if(empty($energetic))
							$energetic = 'None';
						if(empty($foggy_headed))
							$foggy_headed = 'None';
						if(empty($dizzy))
							$dizzy = 'None';
						if(empty($paranoid))
							$paranoid = 'None';
						if(empty($sleepy))
							$sleepy = 'None';
						$date = $row->post_date;
		
						$html .= '<div class="patient-symptoms-tracker app'.$counter.'">
							<div class="panel panel-default symptoms-collapse">
								<div class="panel-heading panel-collapsed panel-opened">
										<span class="pull-left time">'.$date.'</span>
										<span class="info">Cannabis Today: '.ucwords($cannabis).'</span>
										<span class="pull-right detail">Details <i class="fa fa-chevron-down"></i></span>
										<span class="pull-right closed">Close <i class="fa fa-remove"></i></span>
								</div>
								<div class="panel-body" style="display: none;">
									<table>
										<tbody>
											<tr><th>Anxious Mood</th><td>'.ucwords($anxious_mood).'</td></tr>
											<tr><th>Depressed Mood</th><td>'.ucwords($depressed_mood).'</td></tr>
											<tr><th>Happy</th><td>'.ucwords($happy).'</td></tr>
											<tr><th>Relaxed</th><td>'.ucwords($relaxed).'</td></tr>
											<tr><th>Focused</th><td>'.ucwords($focused).'</td></tr>
											<tr><th>Energetic</th><td>'.ucwords($energetic).'</td></tr>
											<tr><th>Foggy headed</th><td>'.ucwords($foggy_headed).'</td></tr>
											<tr><th>Dizzy</th><td>'.ucwords($dizzy).'</td></tr>
											<tr><th>Paranoid</th><td>'.ucwords($paranoid).'</td></tr>
											<tr><th>Sleepy</th><td>'.ucwords($sleepy) .'</td></tr>
										</tbody>
									</table>
								</div>
							</div>
						</div>';
						$counter++;			
					}  
				$html .= '</div>
			</div><!--End row-->
        </div>
    </section>';
		
	remove_filter( 'posts_where', 'filter_where' );
	return $html;
	
}
add_shortcode( 'view-symptoms', 'view_symptoms_data' );


?>