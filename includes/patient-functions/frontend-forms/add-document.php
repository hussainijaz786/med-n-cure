<?php
if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

add_shortcode( 'add-document', 'add_document_shortcode' );

function add_document_shortcode() {
	ob_start();	
	$current_user = wp_get_current_user();
	$useremail = $current_user->user_email;
	$user_ID = get_current_user_id();
	$getpatientid = isset($_GET['pid'])? $_GET['pid'] : '';
	
	if($getpatientid) {
		$userid = $getpatientid;
	} else {
		$userid = $user_ID;
	}
	$act = isset($_GET['action'])? $_GET['action'] : '';
	$updates = isset($_GET['updated'])? $_GET['updated'] : '';
	$postid = isset($_GET['post'])? $_GET['post']: '';
	if($act == "edit-user-document" && $act != '' && $updates != "true") {
		acf_form(array(
			'post_id' => $postid, // Get the post ID
			'field_groups' => array('group_56ddf2042490e'),
			'label_placement' => 'left',
			'updated_message' => __("Your document has been updated successfully.", 'acf'),
			'submit_value'		=> 'Update Document'
		));	
	
	} else {
		acf_form(array(
			'post_id'		=> 'new_post',
			'new_post'		=> array(
				'post_type'		=> 'document',
				'post_status'		=> 'publish',
				'post_title'	=> $useremail,
				'post_author'	=> 	$userid
			),
			'label_placement' => 'left',
			'updated_message' => __("Your document has been updated successfully.", 'acf'),
			'submit_value'		=> 'Add Document'
		));
				
	}
	$html = ob_get_contents();
	ob_get_clean();
	if (ob_get_contents()) ob_end_flush();
	return $html;
}
?>