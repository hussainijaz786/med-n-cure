<?php
if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly


add_action( 'wp_ajax_edit_patient_mmj_recommendation_ajax', 'edit_patient_mmj_recommendation_ajax');
add_action( 'wp_ajax_nopriv_edit_patient_mmj_recommendation_ajax', 'edit_patient_mmj_recommendation_ajax');

function edit_patient_mmj_recommendation_ajax() {
	$patient_id = $_POST['patient_id'];
	
	ob_start();
	// Bail if not logged in or able to post
	if ( ! ( is_user_logged_in() ) ) {
		echo '<p>You Must Be Logged In To Fill Your Intake Form</p>';
		return;
	}
	
	$user_mmj = get_user_meta($patient_id, 'pre-registration-id', true ); 
	acf_form(array(
		'post_id' => $user_mmj, // Get the post ID
		'field_groups' => array('group_570aa89043733'),
		'updated_message' => __("Recommendation has been updated successfully.", 'acf'),
		'label_placement' => 'left',
		'submit_value'		=> 'Update MMJ Recommendation'
	));
	?>
    <script>
        (function($) {
            jQuery(document).trigger('acf/setup_fields', jQuery(".cdrmed-modal-box") );
            acf.do_action('ready', $('body'));
            acf.do_action('load', $('body'));
            acf.fields.wysiwyg.initialize();
        })(jQuery);
    </script>
	<?php
	$mmjreco = ob_get_contents();
	ob_get_clean();
	if (ob_get_contents()) ob_end_flush();
	echo $mmjreco;
	die();
}


add_shortcode( 'mmj-recommendation-form', 'mmj_recommendation_form_shortcode' );

function mmj_recommendation_form_shortcode() {
	ob_start();
	// Bail if not logged in or able to post
	if ( ! ( is_user_logged_in() ) ) {
		echo '<p>You Must Be Logged In To Fill Your Intake Form</p>';
		return;
	}
	
	$patient_id = isset($_GET['pid']) ? $_GET['pid'] : '';
	if($patient_id) {
		$current_user_id = $patient_id;	
	} else {
		$current_user = wp_get_current_user();
		$current_user_id = get_current_user_id();
		$useremail = $current_user->user_email;
	}
	
	
	if($patient_id) {
		$user_mmj = get_user_meta($patient_id, 'pre-registration-id', true ); 
	} else {
		$user_mmj = get_user_meta(get_current_user_id(), 'pre-registration-id', true ); 
	}
	if($user_mmj) {
		acf_form(array(
			'post_id' => $user_mmj, // Get the post ID
			'field_groups' => array('group_570aa89043733'),
			'updated_message' => __("Recommendation has been updated successfully.", 'acf'),
			'label_placement' => 'left',
			'submit_value'		=> 'Update MMJ Recommendation'
		));
		
	} else {
		acf_form(array(
			'post_id'		=> 'new_post',
			'field_groups' => array('group_570aa89043733'),
			'new_post'		=> array(
				'post_type'		=> 'pre-registrations',
				'post_status'		=> 'publish',
				'post_title'	=> 'MMJ Recommendation',
				'post_author'	=> 	$current_user_id
			),
			'label_placement' => 'left',
			'submit_value'		=> 'Update MMJ Recommendation'
		));
				
	}
				
	$mmjreco = ob_get_contents();
	ob_get_clean();
	if (ob_get_contents()) ob_end_flush();
	return $mmjreco;
	
}

add_action('acf/save_post', 'my_save_mmj');

function my_save_mmj( $post_id ) {
	$patient_id = isset($_GET['pid']) ? $_GET['pid'] : '';
	$log_user = get_user_role();
	// bail early if not a contact_form post
	if( get_post_type($post_id) !== 'pre-registrations' ) {
		return;
	}
	// bail early if editing in admin
	if( is_admin() ) {
		return;
	}
	// vars
	$post = get_post( $post_id );
	if(is_page(array( 'view-complete-intake', 'patient-dashboard','mmj-recommendation'))){
		if($patient_id) {
			update_user_meta( $patient_id, 'pre-registration-id', $post_id );
			update_post_meta( $post_id, 'is-mmj-reco', 'yes' );
			cdrmed_save_activity_log('Patient MMJ Recommendation updated!', $patient_id);
		} else {
			update_user_meta(get_current_user_id(), 'pre-registration-id', $post_id);
			update_post_meta( $post_id, 'is-mmj-reco', 'yes' );
			cdrmed_save_activity_log('Patient MMJ Recommendation updated!', '');
		}
	}
	
	
	
	if($log_user == 'dispensary' && is_page(array( 'view-complete-intake', 'patient-dashboard','mmj-recommendation'))  || $log_user == 'physician' && is_page(array( 'view-complete-intake', 'patient-dashboard','mmj-recommendation'))  ) {
	wp_redirect(home_url('/view-complete-intake/?&pid='.$patient_id));
	exit;
	}
}
?>