<?php
if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

add_action( 'get_header', 'intake_do_acf_form_head', 1 );
function intake_do_acf_form_head() {
	acf_form_head();
}


add_shortcode( 'intake_form', 'intake_form_open' );

function intake_form_open() {
	ob_start();
	// Bail if not logged in or able to post
	if ( ! ( is_user_logged_in() ) ) {
		echo '<p>You Must Be Logged In To Fill Your Intake Form</p>';
		return;
	}
	$patient_id = isset($_GET['pid']) ? $_GET['pid'] : get_current_user_id();
	
	$current_user_id = $useremail = '';
	
	if($patient_id) {
		$current_user_id = $patient_id;
		$user_info = get_userdata($current_user_id);
		$useremail = $user_info->user_email;
	} else {
		$current_user = wp_get_current_user();
		$current_user_id = get_current_user_id();
		$useremail = $current_user->user_email;
	}
	
	$user_last = get_user_meta($patient_id, 'patient_intake', true ); 
	
	if($user_last) {
	acf_form(array(
					'post_id' => $user_last, // Get the post ID
					'field_groups' => array('group_56caa23f9c920'),
					'updated_message' => __("Your Intake has been updated successfully.", 'acf'),
					'submit_value'		=> 'Update My Health'
				));	
		
	} else {
	acf_form(array(
					'post_id'		=> 'new_post',
					'new_post'		=> array(
						'post_type'		=> 'intake',
						'post_status'		=> 'publish',
						'post_title'	=> $useremail,
						'post_author'	=> 	$current_user_id
					),
					'submit_value'		=> 'Update My Health'
				));
				
			}
			
		$html = ob_get_contents();
		ob_get_clean();
		if (ob_get_contents()) ob_end_flush();
		return $html;
				
}

add_action('acf/save_post', 'my_save_post');

function my_save_post( $post_id ) {
	
	// bail early if not a contact_form post
	if( get_post_type($post_id) !== 'intake' ) {
		return;
	}
	// bail early if editing in admin
	if( is_admin() ) {
		return;
	}
	// vars
	$post = get_post( $post_id );
	$userrole = get_user_role();
	$patient_id = isset($_GET['pid']) ? $_GET['pid'] : get_current_user_id();
	update_user_meta($patient_id, 'patient_intake', $post_id);
	update_post_meta($post_id, 'intake_last_update', date("Y-m-d h:i:s"));
	
	$dob = get_field( "date_of_birth", $post_id );
	$db = date("M-d-Y", strtotime($dob));
	$age = date_diff(date_create($db), date_create('now'))->y;
	update_post_meta($post_id, 'patient_calculated_age', $age);
	
	cdrmed_save_activity_log('Update Patient Intake', isset($_GET['pid']) ? $_GET['pid'] : '');
}
?>