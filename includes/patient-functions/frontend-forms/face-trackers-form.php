<?php
if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly


function feedback_data() {
	
	error_reporting(E_ALL);
	//ini_set('display_errors', 0);
	
	$post_id = !isset($_GET['post_id']) ? null : $_GET['post_id'];
     
	$feel = $comments = '';
	$view_comments = 'style="display:none;"';
	if($post_id){
		$feel      =   get_post_meta($post_id, 'feel',true);
		$comments  =   get_post_meta($post_id,'comments',true);
		$view_comments = '';
    }
	
	$msg = '';
    $html  =  '<section class="feedback-from ">
    <!--<div class="container-fluid">
        <div class="row">
            <div class="col-sm-12"><br/>-->'.$msg.'
                <form class="patient-feedback" action="'.get_admin_url().'admin-ajax.php?action=submit_face_symptoms_tracker_ajax&patient_face_tracker_submit=patient_face_tracker_submit&post_id='.$post_id.'" method="post">
                    <h1>Tell us how you\'re feeling today!</h1>
                    <h3>Select the face that corresponds to how you feel.</h3>
                    <input type="hidden" name="updated_date" id="updated_date" value="" />
                    <div class="radio_images">
                        <div class="radio-item">
                            <input type="radio" '.((@$feel=='v-well')? ' checked="checked"' : '').' class="patient_feedback" id="vgood" name="feel" value="v-well"/>
                            <label for="vgood"><img src="'.CDRMED.'/includes/assets/img/vgood.png"/><img src="'.CDRMED.'/includes/assets/img/vgood2.png" class="selected_smile"/>Very Well</label>
                        </div>
                        <div class="radio-item">
                            <input type="radio" id="good" '.((@$feel=='well')? ' checked="checked"' : '').' class="patient_feedback" name="feel" value="well"/>
                            <label for="good"><img src="'.CDRMED.'/includes/assets/img/good.png"/><img src="'.CDRMED.'/includes/assets/img/good2.png" class="selected_smile"/>Well</label>
                            
                        </div>
                        <div class="radio-item">
                            <input type="radio" id="neutral" '.((@$feel=='neutral')? ' checked="checked"' : '').' class="patient_feedback" name="feel" value="neutral"/>
                            <label for="neutral"><img src="'.CDRMED.'/includes/assets/img/neutral.png"/><img src="'.CDRMED.'/includes/assets/img/neutral2.png" class="selected_smile"/>Neutral</label>
                            
                        </div>
                        <div class="radio-item">
                            <input type="radio" id="bad" '.((@$feel=='unwell')? ' checked="checked"' : '').' class="patient_feedback" name="feel" value="unwell"/>
                            <label for="bad"><img src="'.CDRMED.'/includes/assets/img/bad.png"/><img src="'.CDRMED.'/includes/assets/img/bad2.png" class="selected_smile"/>Unwell</label>
                            
                        </div>
                        <div class="radio-item">
                            <input type="radio" id="vbad" '.((@$feel=='v-unwell')? ' checked="checked"' : '').' class="patient_feedback" name="feel" value="v-unwell"/>
                            <label for="vbad"><img src="'.CDRMED.'/includes/assets/img/vbad.png"/><img src="'.CDRMED.'/includes/assets/img/vbad2.png" class="selected_smile"/>Very Unwell</label>
                            
                        </div>
                    </div>
                    <div class="simple_radio_holder" id="mcomments" '.$view_comments.'>
						<div class="col-md-12">
							<label for="comments" id="forcmt">Why do you feel <b id="txt-change"> </b>? <span style="font-weight:normal;">(Optional)</span> <br/>
							<textarea rows="4" cols="50" id="comments" name="comments">'.$comments.'</textarea></label>
					
							<div class="clock">
								<p>Update your health status for </p>
								<div id="Date"></div>
								<span id="hours"> </span>
								<span id="point">:</span>
								<span id="min"> </span>
								<span id="point">:</span>
								<span id="sec"> </span>
								<span id="am"> </span>
							   </div>
							</div>
							
							<div class="buttons">
								<button type="submit" id="submit_feed" name="patient_feedback_submit">update</button>
								<button type="button" id="cancel">Cancel</button>
							</div>

					</div>
					<div class="buttons">
						<a href ="?view=2" class="">take Full survey</a>
					</div>
                    
                </form>
                   <!--</div> </div> </div>--></section>';
    return $html;

    
}
add_shortcode( 'feedback_form', 'feedback_data' );

?>