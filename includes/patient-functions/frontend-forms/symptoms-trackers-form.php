<?php

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly 


function  symptoms_tracker(){
	global $wpdb, $wp;
	$eid = !isset($_GET['edit'])?null:$_GET['edit'];
	$post_id = !isset($_GET['post_id'])?null:$_GET['post_id'];
	//$dates =  '<script type="text/javascript">document.write( moment().format("dddd, MMMM Do YYYY, h:mm:ss A"))</script>';
	
	$html = $nstatus = '';
	
	$feel = isset($_GET['feel']) ? $_GET['feel'] : '';
	
	switch ($feel) {
        case 'well':
            $nstatus = '<img src="'.CDRMED.'/includes/assets/img/good.png" width="48" height="48" /> Well';
            break;
        case 'v-well':
            $nstatus = '<img src="'.CDRMED.'/includes/assets/img/vgood.png" width="48" height="48" /> Very Well';
            break;
		case 'neutral':
            $nstatus = '<img src="'.CDRMED.'/includes/assets/img/neutral.png" width="48" height="48" /> Neutral';
            break;
		case 'unwell':
            $nstatus = '<img src="'.CDRMED.'/includes/assets/img/bad.png" width="48" height="48" /> Unwell';
            break;
		case 'v-unwell':
            $nstatus = '<img src="'.CDRMED.'/includes/assets/img/vbad.png" width="48" height="48" /> Very Unwell';
            break;
    }
	
	if(isset($_GET['view'])){
		$msg = "<div class='feed_save'><img src='".CDRMED."/includes/assets/img/good-light.png' /> You Successfully recorded a score of ".$nstatus." on ".get_the_date( 'l, F d Y h:i:s', $post_id )."</div><p class='text-center' style='margin-bottom:50px'><a href='".site_url()."/patient-dashboard/' class='btn bordered'>Go Back To Dashboard</a><!--<a href='".site_url()."/view-patient-history/' class='btn bordered'>View My Health Status Updates</a>  <a href='".site_url()."/view-symptoms-updates/' class='btn bordered'>View My Symptoms Update</a>--></p>";
	}
	
    $html  .=  '<section class="feedback-from ">
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12"><br/>'.$msg.'
                <form class="patient-feedback" action="'.get_admin_url().'admin-ajax.php?action=submit_face_symptoms_tracker_ajax&symptoms_tracker_submit=symptoms_tracker_submit" method="post"> <h1>Please elaborate on your feelings.</h1>
                    <h3>Please provide more details about your symptoms from Cannabis use today.</h3>
                    
                    <div class="simple_radio_holder ">
                        <div class="radio-label">Did you take cannabis today?</div>
                        <div class="s_radio">
                            <input type="radio" '.((@$cannabis=='yes')? ' checked="checked"' : '').' name="cannabis" value="yes" id="s_yes"/>
                            <label for="s_yes">Yes</label>
                        </div>
                        <div class="s_radio">
                            <input type="radio" '.((@$cannabis=='no')? ' checked="checked"' : '').' name="cannabis" value="no" id="s_no"/>
                            <label for="s_no">No</label>
                        </div>
						
                    </div><!--End imple_radio_holder div-->
					
					
					<div class="today_cannabis_rows" id="today_cannabis_rows" '.((@$cannabis=='yes')? ' ' : ' style="display:none;"').'>
							<table>
								<thead>
									<tr><th>Amount (mg)</th><th>Frequency</th><th>Method of Ingestion</th></tr>
								</thead>
								<tbody>
									<tr><div class="form-field _today_cannabis_field tp">
									<td><input type="text" class="form-control frst" name="_today_cannabis_amount[]" value=""></td>
									<td><span class="scnd">
										<select id="frequency_of_ingested" name="_today_cannabis_frequency[]" class="form-control">
											<option>1</option>
											<option>2</option>
											<option>3</option>
											<option>4</option>
											<option>5</option>
											<option>6</option>
											<option>7</option>
											<option>8</option>
											<option>9</option>
											<option>10</option>
										</select>
									</td>
									<td>
										<select id="method_of_ingested" name="_today_cannabis_ingested[]" class="form-control">
										<option>Inhaled</option>
										<option>Ingested</option>
										<option>Sublingual</option>
										<option>Suppository</option>
										<option>Topically</option>
										</select>
									</td>
									</div></tr>';
									
									$can_arr = json_decode(get_post_meta( $post_id, '_today_used_cannabis', true  ));
									
									$html_part = '';
									
									if($can_arr['0'] != 'null'  && $can_arr['0'] != ''){
										foreach ($can_arr as $cannab) {
											$html_part .= '<tr><div class="form-field _azcl_cbc_field tp">';
											foreach ($cannab as $cann) {
												$html_part .= '<td><input type="text" class="form-control frst" name="_today_cannabis_amount[]" value="'.$cann[0].'"></td>';
												$html_part .= '<td><span class="scnd"><input type="text" class="form-control scnd2" name="_today_cannabis_frequency[]" value="'.$cann[1].'"><span></td>';
												$html_part .= '<td>
												<select id="method_of_ingested" name="_today_cannabis_ingested[]" class="form-control">';
												
												$selected = ($cann[2] == 'Inhaled')? 'selected': '';
												$html_part .= '<option '.$selected .'>Inhaled</option>';
												
												$selected = ($cann[2] == 'Ingested')? 'selected': '';
												$html_part .= '<option '.$selected .'>Ingested</option>';
												
												$selected = ($cann[2] == 'Sublingual')? 'selected': '';
												$html_part .= '<option '.$selected .'>Sublingual</option>';
												
												$selected = ($cann[2] == 'Suppository')? 'selected': '';
												$html_part .= '<option '.$selected .'>Suppository</option>';
												
												$selected = ($cann[2] == 'Topically')? 'selected': '';
												$html_part .= '<option '.$selected .'>Topically</option>';
												
												$html_part .= '</select>
												</td>';
											}
											$html_part .= '</div></tr>';
										}
									}
									
									$html .= $html_part;
									
									$html  .=  '<tr class="input_fields_wrap_today_ingested"><td colspan="4"><button id="add_field_button_today_ingested" class="add_field_button">Add Row</button></td></tr>
								</tbody>
							</table>
						</div>
                    
                    <div class="simple_radio_holder ">
                        <div class="radio-label">Symptoms</div>
                        <p>Rate the severity of your symptoms to share your status with your healthcare professional and to gain a indication of trends over time </p>
                        <div class="row titles">
                            <div class="col-sm-3"></div>
                            <div class="col-sm-9">
                                <div class="t_item">None</div>
                                <div class="t_item">Mild</div>
                                <div class="t_item">Moderate</div>
                                <div class="t_item">Severe</div>
                            </div>
                        </div>
                        
                        <div class="row simptoms_data">
                            <div class="col-sm-3">
                                <span>Happy</span>
                            </div>
                            <div class="col-sm-9">
                                <div class="d_item">
                                    <div class="s_radio">
                                        
                                        <input type="radio" '.((@$happy=='none')? ' checked="checked"' : '').' name="happy" value="none" id="happy"/>
                                        <label for="happy">None</label>
                                    </div>
                                </div>
                                <div class="d_item">
                                    <div class="s_radio">
                                        
                                        <input type="radio" '.((@$happy=='mild')? ' checked="checked"' : '').' name="happy" value="mild" id="happy2"/>
                                        <label for="happy2">Mild</label>
                                    </div>
                                </div>
                                <div class="d_item">
                                    <div class="s_radio">
                                        
                                        <input type="radio" name="happy" '.((@$happy=='moderate')? ' checked="checked"' : '').' value="moderate" id="happy3"/>
                                        <label for="happy3">Moderate</label>
                                    </div>
                                </div>
                                <div class="d_item">
                                    <div class="s_radio">
                                        
                                        <input type="radio" name="happy" '.((@$happy=='moderate')? ' checked="checked"' : '').' value="severe" id="happy4"/>
                                        <label for="happy4">Severe</label>
                                    </div>
                                </div>
                            </div>
                        </div><!--End row.simptoms-data div-->
                        
                        <div class="row simptoms_data">
                            <div class="col-sm-3">
                                <span>Relaxed</span>
                            </div>
                            <div class="col-sm-9">
                                <div class="d_item">
                                    <div class="s_radio">
                                        
                                        <input type="radio" name="relaxed" '.((@$relaxed=='none')? ' checked="checked"' : '').' value="none" id="relaxed"/>
                                        <label for="relaxed">None</label>
                                    </div>
                                </div>
                                <div class="d_item">
                                    <div class="s_radio">
                                        
                                        <input type="radio" name="relaxed" '.((@$relaxed=='mild')? ' checked="checked"' : '').' value="mild" id="relaxed2"/>
                                        <label for="relaxed2">Mild</label>
                                    </div>
                                </div>
                                <div class="d_item">
                                    <div class="s_radio">
                                        
                                        <input type="radio" name="relaxed" '.((@$relaxed=='moderate')? ' checked="checked"' : '').' value="moderate" id="relaxed3"/>
                                        <label for="relaxed3">Moderate</label>
                                    </div>
                                </div>
                                <div class="d_item">
                                    <div class="s_radio">
                                        
                                        <input type="radio" name="relaxed" '.((@$relaxed=='severe')? ' checked="checked"' : '').' value="severe" id="relaxed4"/>
                                        <label for="relaxed4">Severe</label>
                                    </div>
                                </div>
                            </div>
                        </div><!--End row.simptoms-data div-->
                        
                        <div class="row simptoms_data">
                            <div class="col-sm-3">
                                <span>Focused</span>
                            </div>
                            <div class="col-sm-9">
                                <div class="d_item">
                                    <div class="s_radio">
                                        
                                        <input type="radio" name="focused" '.((@$focused=='none')? ' checked="checked"' : '').' value="none" id="focused"/>
                                        <label for="focused">None</label>
                                    </div>
                                </div>
                                <div class="d_item">
                                    <div class="s_radio">
                                        
                                        <input type="radio" name="focused" '.((@$focused=='mild')? ' checked="checked"' : '').' value="mild" id="focused2"/>
                                        <label for="focused2">Mild</label>
                                    </div>
                                </div>
                                <div class="d_item">
                                    <div class="s_radio">
                                        
                                        <input type="radio" name="focused" '.((@$focused=='moderate')? ' checked="checked"' : '').' value="moderate" id="focused3"/>
                                        <label for="focused3">Moderate</label>
                                    </div>
                                </div>
                                <div class="d_item">
                                    <div class="s_radio">
                                        
                                        <input type="radio" name="focused" '.((@$focused=='severe')? ' checked="checked"' : '').' value="severe" id="focused4"/>
                                        <label for="focused4">Severe</label>
                                    </div>
                                </div>
                            </div>
                        </div><!--End row.simptoms-data div-->

                         <div class="row simptoms_data">
                            <div class="col-sm-3">
                                <span>Energetic</span>
                            </div>
                            <div class="col-sm-9">
                                <div class="d_item">
                                    <div class="s_radio">
                                        
                                        <input type="radio" name="energetic" '.((@$energetic=='none')? ' checked="checked"' : '').' value="none" id="energetic"/>
                                        <label for="energetic">None</label>
                                    </div>
                                </div>
                                <div class="d_item">
                                    <div class="s_radio">
                                        
                                        <input type="radio" name="energetic" '.((@$energetic=='mild')? ' checked="checked"' : '').' value="mild" id="energetic2"/>
                                        <label for="energetic2">Mild</label>
                                    </div>
                                </div>
                                <div class="d_item">
                                    <div class="s_radio">
                                        
                                        <input type="radio" name="energetic" '.((@$energetic=='moderate')? ' checked="checked"' : '').' value="moderate" id="energetic3"/>
                                        <label for="energetic3">Moderate</label>
                                    </div>
                                </div>
                                <div class="d_item">
                                    <div class="s_radio">
                                        
                                        <input type="radio" name="energetic" '.((@$energetic=='severe')? ' checked="checked"' : '').' value="severe" id="energetic4"/>
                                        <label for="energetic4">Severe</label>
                                    </div>
                                </div>
                            </div>
                        </div><!--End row.simptoms-data div-->

                          <div class="row simptoms_data">
                            <div class="col-sm-3">
                                <span>Foggy headed</span>
                            </div>
                            <div class="col-sm-9">
                                <div class="d_item">
                                    <div class="s_radio">
                                        
                                        <input type="radio" name="foggy_headed" '.((@$foggy_headed=='none')? ' checked="checked"' : '').' value="none" id="foggy_headed"/>
                                        <label for="foggy_headed">None</label>
                                    </div>
                                </div>
                                <div class="d_item">
                                    <div class="s_radio">
                                        
                                        <input type="radio" name="foggy_headed" '.((@$foggy_headed=='mild')? ' checked="checked"' : '').' value="mild" id="foggy_headed2"/>
                                        <label for="foggy_headed2">Mild</label>
                                    </div>
                                </div>
                                <div class="d_item">
                                    <div class="s_radio">
                                        
                                        <input type="radio" name="foggy_headed" '.((@$foggy_headed=='moderate')? ' checked="checked"' : '').' value="moderate" id="foggy_headed3"/>
                                        <label for="foggy_headed3">Moderate</label>
                                    </div>
                                </div>
                                <div class="d_item">
                                    <div class="s_radio">
                                        
                                        <input type="radio" name="foggy_headed" '.((@$foggy_headed=='severe')? ' checked="checked"' : '').' value="severe" id="foggy_headed4"/>
                                        <label for="foggy_headed4">Severe</label>
                                    </div>
                                </div>
                            </div>
                        </div><!--End row.simptoms-data div-->

             <div class="row simptoms_data">
                            <div class="col-sm-3">
                                <span>Dizzy</span>
                            </div>
                            <div class="col-sm-9">
                                <div class="d_item">
                                    <div class="s_radio">
                                        
                                        <input type="radio" name="dizzy" '.((@$dizzy=='none')? ' checked="checked"' : '').' value="none" id="dizzy"/>
                                        <label for="dizzy">None</label>
                                    </div>
                                </div>
                                <div class="d_item">
                                    <div class="s_radio">
                                        
                                        <input type="radio" name="dizzy" '.((@$dizzy=='mild')? ' checked="checked"' : '').' value="mild" id="dizzy2"/>
                                        <label for="dizzy2">Mild</label>
                                    </div>
                                </div>
                                <div class="d_item">
                                    <div class="s_radio">
                                        
                                        <input type="radio" name="dizzy" '.((@$dizzy=='moderate')? ' checked="checked"' : '').' value="moderate" id="dizzy3"/>
                                        <label for="dizzy3">Moderate</label>
                                    </div>
                                </div>
                                <div class="d_item">
                                    <div class="s_radio">
                                        
                                        <input type="radio" name="dizzy" '.((@$dizzy=='severe')? ' checked="checked"' : '').' value="severe" id="dizzy4"/>
                                        <label for="dizzy4">Severe</label>
                                    </div>
                                </div>
                            </div>
                        </div><!--End row.simptoms-data div-->
<div class="row simptoms_data">
                            <div class="col-sm-3">
                                <span>Paranoid</span>
                            </div>
                            <div class="col-sm-9">
                                <div class="d_item">
                                    <div class="s_radio">
                                        
                                        <input type="radio" name="paranoid" '.((@$paranoid=='none')? ' checked="checked"' : '').' value="none" id="paranoid"/>
                                        <label for="paranoid">None</label>
                                    </div>
                                </div>
                                <div class="d_item">
                                    <div class="s_radio">
                                        
                                        <input type="radio" name="paranoid" '.((@$paranoid=='mild')? ' checked="checked"' : '').' value="mild" id="paranoid2"/>
                                        <label for="paranoid2">Mild</label>
                                    </div>
                                </div>
                                <div class="d_item">
                                    <div class="s_radio">
                                        
                                        <input type="radio" name="paranoid" '.((@$paranoid=='moderate')? ' checked="checked"' : '').' value="moderate" id="paranoid3"/>
                                        <label for="paranoid3">Moderate</label>
                                    </div>
                                </div>
                                <div class="d_item">
                                    <div class="s_radio">
                                        
                                        <input type="radio" name="paranoid" '.((@$paranoid=='severe')? ' checked="checked"' : '').' value="severe" id="paranoid4"/>
                                        <label for="paranoid4">Severe</label>
                                    </div>
                                </div>
                            </div>
                        </div><!--End row.simptoms-data div-->
                        <div class="row simptoms_data">
                            <div class="col-sm-3">
                                <span>Anxious</span>
                            </div>
                            <div class="col-sm-9">
                                <div class="d_item">
                                    <div class="s_radio">
                                        
                                        <input type="radio" name="anxious_mood" '.((@$anxious_mood=='none')? ' checked="checked"' : '').' value="none" id="anxious-mood"/>
                                        <label for="anxious-mood">None</label>
                                    </div>
                                </div>
                                <div class="d_item">
                                    <div class="s_radio">
                                        
                                        <input type="radio" name="anxious_mood" '.((@$anxious_mood=='mild')? ' checked="checked"' : '').' value="mild" id="anxious-mood2"/>
                                        <label for="anxious-mood2">Mild</label>
                                    </div>
                                </div>
                                <div class="d_item">
                                    <div class="s_radio">
                                        
                                        <input type="radio" name="anxious_mood" '.((@$anxious_mood=='moderate')? ' checked="checked"' : '').' value="moderate" id="anxious-mood3"/>
                                        <label for="anxious-mood3">Moderate</label>
                                    </div>
                                </div>
                                <div class="d_item">
                                    <div class="s_radio">
                                        
                                        <input type="radio" name="anxious_mood" '.((@$anxious_mood=='severe')? ' checked="checked"' : '').' value="severe" id="anxious-mood4"/>
                                        <label for="anxious-mood4">Severe</label>
                                    </div>
                                </div>
                            </div>
                        </div><!--End row.simptoms-data div-->
                        
                        <div class="row simptoms_data">
                            <div class="col-sm-3">
                                <span>Depressed</span>
                            </div>
                            <div class="col-sm-9">
                                <div class="d_item">
                                    <div class="s_radio">
                                        
                                        <input type="radio" name="depressed_mood" '.((@$depressed_mood=='none')? ' checked="checked"' : '').' value="none" id="depressed_mood"/>
                                        <label for="depressed_mood">None</label>
                                    </div>
                                </div>
                                <div class="d_item">
                                    <div class="s_radio">
                                        
                                        <input type="radio" name="depressed_mood" '.((@$depressed_mood=='mild')? ' checked="checked"' : '').' value="mild" id="depressed_mood2"/>
                                        <label for="depressed_mood2">Mild</label>
                                    </div>
                                </div>
                                <div class="d_item">
                                    <div class="s_radio">
                                        
                                        <input type="radio" name="depressed_mood" '.((@$depressed_mood=='moderate')? ' checked="checked"' : '').' value="moderate" id="depressed_mood3"/>
                                        <label for="depressed_mood3">Moderate</label>
                                    </div>
                                </div>
                                <div class="d_item">
                                    <div class="s_radio">
                                        
                                        <input type="radio" name="depressed_mood" '.((@$depressed_mood=='severe')? ' checked="checked"' : '').' value="severe" id="depressed_mood4"/>
                                        <label for="depressed_mood4">Severe</label>
                                    </div>
                                </div>
                            </div>
                        </div><!--End row.simptoms-data div-->
                        <div class="row simptoms_data last_simpltom">
                            <div class="col-sm-3">
                                <span>Sleepy</span>
                            </div>
                            <div class="col-sm-9">
                                <div class="d_item">
                                    <div class="s_radio">
                                        
                                        <input type="radio" name="sleepy" '.((@$sleepy=='none')? ' checked="checked"' : '').' value="none" id="sleepy"/>
                                        <label for="sleepy">None</label>
                                    </div>
                                </div>
                                <div class="d_item">
                                    <div class="s_radio">
                                        
                                        <input type="radio" name="sleepy" '.((@$sleepy=='mild')? ' checked="checked"' : '').' value="mild" id="sleepy2"/>
                                        <label for="sleepy2">Mild</label>
                                    </div>
                                </div>
                                <div class="d_item">
                                    <div class="s_radio">
                                        
                                        <input type="radio" name="sleepy" '.((@$sleepy=='moderate')? ' checked="checked"' : '').' value="moderate" id="sleepy3"/>
                                        <label for="sleepy3">Moderate</label>
                                    </div>
                                </div>
                                <div class="d_item">
                                    <div class="s_radio">
                                        
                                        <input type="radio" name="sleepy" '.((@$sleepy=='severe')? ' checked="checked"' : '').' value="severe" id="sleepy4"/>
                                        <label for="sleepy4">Severe</label>
                                    </div>
                                </div>
                            </div>
                        </div><!--End row.simptoms-data div-->
                        
                    </div><!--End simple_radio_holder div-->
                    
                    <div class="buttons">
						<button type="submit" id="submit_feed" name="symptoms_tracker_submit">Submit</button>
                        <button type="reset" id="reset_feed">Reset</button>
                    </div>
                    
                </form>
            </div>
        </div>
    </div>
</section>';
    return $html;

    
}
add_shortcode( 'symptoms_trackers', 'symptoms_tracker' );

?>