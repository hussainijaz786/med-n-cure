<?php
if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly 
add_action( 'wp_head', 'intake_progressbar' );
function intake_progressbar() {
	global $wpdb;
	$html = '';
	$info  = array();
	$contact = array();
	$cannabis  = array();
	$diagnosis = array();
	$treatment  = array();
	$patient = array();
	$functioning = array();
	$family  = array();
	$nutrition = array();
	$military  = array();
	$comments = array();
	$medications  = array();
	$i = 0;
	// Bail if not logged in or able to post
	if ( ! ( is_user_logged_in() || current_user_can('publish_posts') ) ) {
		//echo '<p>You must be a registered author to post.</p>';
		return;
	}
	
    $posts = get_posts(array(
		'numberposts' => 1,
		'post_type' => 'intake',
		'author'  => get_current_user_id()
	));
	
	$fields = '';
	$modify = '';
	$date = '';
	
	if(sizeof($posts) > 0){
		$fields = get_field_objects($posts[0]->ID);
		$modify = new DateTime($posts[0]->post_modified);
		$date = $modify->format('l, F d y h:i:s A ');
	}
	
	
	//print_r($fields);
	if( $fields != ''){

		foreach( $fields as $field_name => $field )
		{
			//echo count($field),">>>>>",count(array_filter($field));
			

			if(in_array($field['key'], array('field_56c2b5c140152','field_56c2b5c140112','field_56c2b5c140566','411865',
				'5field_56c2b5c140932','field_56c2b5c140d17','field_56c2b5c141109','field_56c2b5c1414e8',
				'field_56c2b5c1418e7','field_56c2b5c1422dc','field_56c2b5c1425dd','field_56c2b5c1425dd',
				'field_56c2b5c142848','field_56c2b878afeac','field_56c2b5c14300c','field_56c2b5c143408','field_56c2b5c1437ec'))){
				  if(is_array($field['value'])){
					foreach ($field['value'] as $value) {

						$info []= $value;
					}
				   
				   }else{
					$info []= $field['value'];

				}
				
			}else if(in_array($field['key'], array('field_56e291dce025f','field_56e29207e0260','field_56c2f06be6372','field_56c2f078e6373',
				'field_56c2f08be6374','field_56c2f0a4e6377','field_56c2f0b3e6378','field_56c2f0d6e6379',
				'field_56c2f0e7e637a'))){
					
				if(is_array($field['value'])){
				   
					if($field['type'] == 'repeater'){
						
					   foreach ($field['value'] as $key => $value) {
												  
					$contact[] = $value;
					
					   }
					}else{
					foreach ($field['value'] as $value) {

					   $contact[] = $value;
					}
				   
				}
					}else{
				  $contact[] = $field['value'];

				}
				
			}else if(in_array($field['key'], array('field_56c2eb9ec5f99','field_56c2ebaac5f9a','field_56c2ebafc5f9b',
				'field_56c2ebe4c5f9c'))){
				
				   if(is_array($field['value'])){

					foreach ($field['value'] as $value) {
						$diagnosis[] .= $value;
					}
					
					}else{
				   $diagnosis[] .= $field['value'];

				}
			}else if(in_array($field['key'], array('field_56c2d72d0d663'))){
				  $str = '';
				 if(is_array($field['value'])){
					if($field['type'] == 'repeater'){
					   
					   foreach ($field['value'] as $key => $value) {
						if($value['surgerytreatment'] != ' ')						  
					$treatment[] =  $value['surgerytreatment'] ." | ".$value['month']. " | ".$value['year'];
					
					   }

					}else{
					foreach ($field['value'] as $value) {

						$treatment[] =$value;
					}
					
				  }
				   }else{
					$treatment[] = $field['value'];

				}
				
			}else if(in_array($field['key'], array('field_56c2ecf138705','field_56c2ed7938709'))){
					
					if(is_array($field['value'])){
							 if($field['type'] == 'repeater'){
					   
					   foreach ($field['value'] as $key => $value) {
					if($value['medication_name'] != ' ')							  
					$medications[] = $value['medication_name'] ." | ".$value['dose']. " | ".$value['frequency'];
					
					   }

					}else{
					foreach ($field['value'] as $value) {

						 $medications[] = $value;
					}
				   
				 }
				   }else{
					  $medications[] = $field['value'];

				}

			}else if(in_array($field['key'], array('field_56e29c9ea1560','field_56e2a121795f3','field_56e29c9ea193d',
				'field_56e2a14f795f4','field_56e29c9ea1d36','field_56e2a161795f5','field_56e29c9ea2113','field_56e2a172795f6',
				'field_56e29c9ea24f0','field_56e2a184795f7','field_56e29c9ea28e9','field_56e2a193795f8','field_56e29c9ea2ccb','field_56e2a1a8795f9',
				'field_56e29c9ea30b1','field_56e2a1b7795fa','field_56e29c9ea34b9','field_56e2a1c3795fb','field_56e29c9ea3893',
				'field_56e2a1d2795fc','field_56e29c9ea3c83','field_56e2a1e5795fd','field_56e29c9ea4066','field_56e2a1fc795fe','field_56e29c9ea444c',
				'field_56e2a20c795ff','field_56e29c9ea483f','field_56e2a21a79600','field_56e29c9ea4c21','field_56e2a22c79601',
				'field_56e29c9ea5006','field_56e2a23c79602','field_56e29c9ea5408','field_56e2a24979603','field_56e29c9ea57e8','field_56e2a25879604',
				'field_56e29c9ea5bdf','field_56e2a26579605','field_56e29c9ea5fd4','field_56e2a27979606','field_56e29c9ea63cb','field_56e2a28a79607',
				'field_56e29c9ea67c1','field_56e2a29d79608','field_56e29c9ea6ba6','field_56e2a2b879609','field_56e29c9ea6f9b',
				'field_56e2a2c57960a','field_56e29c9ea737d','field_56e2a2d67960b','field_56e29c9ea776f','field_56e2a2e47960c','field_56e29c9ea7b4e',
				'field_56e2a2f37960d','field_56e29c9ea7f36','field_56e2a3087960e','field_56e29c9ea8327','field_56e2a3167960f','field_56e29c9ea8723',
				'field_56e2a32479610','field_56e29c9ea8b17'
				))){
			   
					if(is_array($field['value'])){
					foreach ($field['value'] as $value) {

						$family[] = $value;
					}
					
				
				   }else{
					$family[] = $field['value'];

				}
				
			}else if(in_array($field['key'], array('field_56e2a33579611','field_56c2cab914b32','field_56e2949adbf44','field_56e294e3dbf45',
				'field_56e29500dbf46','field_56e29512dbf47','field_56e29525dbf48','field_56e2953bdbf4a','field_56e29548dbf4b','field_56e29554dbf4c',
				'field_56e29560dbf4d','field_56e29571dbf4e','field_56e2957adbf4f','field_56e29585dbf50','field_56e29592dbf51','field_56e295e2dbf52',
				'field_56e295eddbf53','field_56e295fedbf54','field_56e29608dbf55','field_56e29612dbf56','field_56e2961fdbf57','field_56e29632dbf58',
				'field_56e2963cdbf59','field_56e29661dbf5a','field_56e2967ddbf5b','field_56e2968edbf5c','field_56e2969edbf5d',
				'field_56e296aadbf5e','field_56e296b4dbf5f','field_56e296c1dbf60','field_56e296dfdbf61','field_56e29700dbf62','field_56e2970adbf63','field_56e29723dbf64',
				'field_56e29730dbf65','field_56e2973ddbf66','field_56e29748dbf67','field_56e29754dbf68','field_56e29760dbf69','field_56e2976bdbf6a',
				'field_56e29775dbf6b','field_56e29781dbf6c'))){
				  if(is_array($field['value'])){
					foreach ($field['value'] as $value) {

						$patient[] = $value;
					}
				  
				
				   }else{
				   $patient[] = $field['value'];

				}

			}else if(in_array($field['key'], array('field_57bd243249bd0','field_57bd246249bd3','field_57bd24a949bd6'))){
				 if(is_array($field['value'])){
					if($field['type'] == 'repeater'){
					   
					   foreach ($field['value'] as $key => $value) {
												  
					$functioning[] =  $value['score'];
					
					   }

					}else{
					foreach ($field['value'] as $value) {

						$functioning[] =$value;
					}
					
				  }
				   }else{
					$functioning[] = $field['value'];

				}
			}else if(in_array($field['key'], array('field_56c47071c3827','field_56c4709ec3828','field_56c470d6c382a',
				'field_56c470c1c3829','field_56c470fac382b','field_56c47106c382c','field_56c47119c382d','field_56c4713bc382e',
				'field_56c4716dc3830','field_56c4717ac3831','field_56c4719ac3832'))){
				if(is_array($field['value'])){
					foreach ($field['value'] as $value) {

					   $cannabis[] = $value;
					}
					
				
				   }else{
					$cannabis[] =  $field['value'];

				}

			}else if(in_array($field['key'], array('field_56c2f202d0377','field_56c2f231d0378',
				'field_56c2f2acd0379','field_56c2f2bfd037a','field_56c2f2edd037b','field_56c2f2fdd037c','field_56cab3695c721',
				'field_56c2f314d037d','field_56c2f343d037e','field_56c2f34fd037f','field_56c2f36ed0381','field_56c2f386d0382',
				'field_56c2f6c80c16c','field_56c2f7280c170','field_56c2f7470c171','field_56c2f7550c172','field_56c2f7740c173',
				'field_56c2f7a30c174','field_56c2f7a30c175','field_56c2f80e0c177','field_56c2f81c0c178','field_56c2f82b0c179','field_56c2f7a30c179',
				'field_56c2f8370c17a','field_56c2f8410c17b','field_56c2f8510c17c','field_56c2f8600c17d','field_56c2f86f0c17e','field_56c2f87c0c17f',
				'field_56c2f8840c180','field_56c2f8ac0c181','field_56c2f8bc0c182','field_56c2f9040c183','field_56c2f9860c184','field_56c2f9a50c185',
				'field_56c2f9ba0c186','field_56c2f9ce0c187','field_56c2f9e00c188','field_56c2fa110c189','field_56c2fa480c18a','field_56c2fa560c18b',
				'field_56c2fa690c18c','field_56c2fa8c0c18d','field_56c2fb020c18f','field_56c2fb130c190'))){
				if(is_array($field['value'])){
					if($field['type'] == 'repeater'){
					   
					   foreach ($field['value'] as $key => $value) {
												  
					$nutrition[] = $value['breakfast']." ". $value['lunch'] ."  ".$value['dinner'];
					
					   }

					}else{
					foreach ($field['value'] as $value) {

						$nutrition[]= $value;
					}
					 }
				   }else{
					$nutrition[] = $field['value'];

				}

			}else if(in_array($field['key'], array('field_56c2eec85e582','field_56c2eeeb5e583','field_56c2ef085e584',
				'field_56c2ef145e585','field_56c2ef325e586','field_56c2ef445e587'))){
				if(is_array($field['value'])){
					foreach ($field['value'] as $value) {

						 $military[] = $value;
					}
				   
				
				   }else{
				   $military[] = $field['value'];

				}

			}else if(in_array($field['key'], array('field_56c2efd4e636c'))){ 
				 
				$comments[] = $field['value'];
				//echo "<pre/>";
			   
			}
			$i++;
		  
		}
	}      
	
    $info_class = ''; 
    $contact_class = '';
    $diagnosis_class = ''; 
    $treatment_class = '';  
    $medications_class = ''; 
    $family_class = '';
    $patient_class = ''; 
    $cannabis_class = '';
    $nutrition_class = ''; 
    $military_class = ''; 
    $comments_class = '';   
	$complete[] = array();
	// echo count(array_filter($info)),">>>>",count($info),"<br/>";
    if(count(array_filter($info)) != 0){
		if(floatval(count($info)/count(array_filter($info))) <= 2){
			$info_class = ' active2';
			?><script>jQuery(document).ready(function() {
			jQuery('.acf-tab-wrap ul li').eq(0).addClass('active2');});</script><?php 
		}
	}else{
		$complete[] = 1;
	}
	if(count(array_filter($contact)) != 0){
		if(floatval(count($contact)/count(array_filter($contact))) <= 2){
			?><script>jQuery(document).ready(function() {
			jQuery('.acf-tab-wrap ul li').eq(1).addClass('active2');});</script><?php
		}
	}
	else{
		$complete[] = 2;
	}  
	if(count(array_filter($diagnosis)) != 0){
		if(floatval(count($diagnosis)/count(array_filter($diagnosis))) <= 2){
			?><script>jQuery(document).ready(function() {
			jQuery('.acf-tab-wrap ul li').eq(2).addClass('active2');});</script><?php
	}
	}else{
		$complete[] = 3;
	} 
	if(count(array_filter($treatment)) != 0){
		if(floatval(count($treatment)/count(array_filter($treatment))) <= 2){
			?><script>jQuery(document).ready(function() {
			jQuery('.acf-tab-wrap ul li').eq(3).addClass('active2');});</script><?php
		}
	}else{
		$complete[] = 4;
	} 
	if(count(array_filter($medications)) != 0){
		if(floatval(count($medications)/count(array_filter($medications))) <= 2){
			?><script>jQuery(document).ready(function() {
			jQuery('.acf-tab-wrap ul li').eq(4).addClass('active2');});</script><?php
		}
	}else{
		$complete[] = 5;
	} 
	if(count(array_filter($family)) != 0){
		if(floatval(count($family)/count(array_filter($family))) <= 2){
			?><script>jQuery(document).ready(function() {
			jQuery('.acf-tab-wrap ul li').eq(5).addClass('active2');});</script><?php
		}
	}else{
		$complete[] = 6;
	} 
	if(count(array_filter($patient)) != 0){
		if(floatval(count($patient)/count(array_filter($patient))) <= 2){
			?><script>jQuery(document).ready(function() {
			jQuery('.acf-tab-wrap ul li').eq(6).addClass('active2');});</script><?php
		} 
	}else{
		$complete[] = 7;
	} 
	if(count(array_filter($functioning)) != 0){
		if(floatval(count($functioning)/count(array_filter($functioning))) <= 2){
			?><script>jQuery(document).ready(function() {
			jQuery('.acf-tab-wrap ul li').eq(7).addClass('active2');});</script><?php
		} 
	}else{
		$complete[] = 8;
	} 
	if(count(array_filter($cannabis)) != 0){
		if(floatval(count($cannabis)/count(array_filter($cannabis))) <= 2){
			?><script>jQuery(document).ready(function() {
			jQuery('.acf-tab-wrap ul li').eq(8).addClass('active2');});</script><?php
		}  
	}else{
		$complete[] = 9;
	}  
	if(count(array_filter($nutrition)) != 0){
		if(floatval(count($nutrition)/count(array_filter($nutrition))) <= 2){
			?><script>jQuery(document).ready(function() { 
			jQuery('.acf-tab-wrap ul li').eq(9).addClass('active2');});</script><?php
		} 
	}else{
		$complete[] = 10;
	}  
	if(count(array_filter($military)) != 0){
		if(floatval(count($military)/count(array_filter($military))) <= 2){
			?><script>jQuery(document).ready(function() {
			jQuery('.acf-tab-wrap ul li').eq(10).addClass('active2');});</script><?php
		} 
	}else{
		$complete[] = 11;
	}

	//echo count($comments),"????",count(array_filter($comments));
	if(count(array_filter($comments)) != 0){ 
		if(floatval(count($comments)/count(array_filter($comments))) <= 2){
			//echo "<pre>This should mark comment true</pre>";
			?><script>jQuery(document).ready(function() { 
			jQuery('.acf-tab-wrap ul li').eq(11).addClass('active2');});</script><?php
		} 
	}else{
		$complete[] = 12;
	} 

	if(count($complete) == 1 && count($complete[0]) == 0){  ?>
		<script>
		jQuery(document).ready(function() {
			// jQuery('.health').remove();
			jQuery('.intake-patient-form').before('<h4 class="health">Your health information was last updated <?php echo $date;?> Update your health information anytime.</h4>');
		});
		</script>
		<?php
	}else{ 
		?>
		<script> jQuery(document).ready(function() { jQuery('.intake-patient-form').before('<h4 class="health">Complete each section of the health form below.<br>You will see a checkmark <img  width="20" height="20" src="<?php echo CDRMED;?>/includes/assets/img/good-without-bg-icon.png" /> when a section is completed.</h4>');
		});</script><?php 
	}
	$GLOBALS['intake_complete'] = $complete;
	?>
	<script type="text/javascript">
		jQuery('.acf-tab-button').live("click",function(){
			if(jQuery(this).attr('data-key') == 'field_56c2e7e6c5f98'){
				var str = jQuery('#acf-field_56c2eb9ec5f99').val();
				if (str.toLowerCase().indexOf("cancer") >= 0){ 
					//jQuery( ".satges" ).attr("style", "display: block !important");
					$('.satges').removeClass('hidden-by-tab');
				}
			}
		});
	</script>
	<?php
}

?>
