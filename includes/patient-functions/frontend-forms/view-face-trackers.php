<?php

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly 

function filter_where( $where = '' ) {
	$where .= " AND post_date > '" . date( 'Y-m-d', strtotime( '-90 days' ) ) . "'";
	return $where;
}


/*
*
*	Patient dashboard Face Trackers List
*
*/
function patient_health_feedback_list() {
	
    global $wpdb;
	$user_id = isset($_GET['pid']) ? $_GET['pid'] : get_current_user_id();
	
    $userrole = get_user_role();
	$feedback_message = '';
	
	if(@$_GET['delete']){
		wp_delete_post($_GET['delete']);
		$feedback_message = '<div style="margin:10px auto; padding: 10px;" class="feedback alert-success">Status deleted Successfully...</div>';
	}
	
	$url = site_url()."/view-patient-history/";
    add_filter( 'posts_where', 'filter_where' );
	$args = array(
		'posts_per_page' => 5,
		'orderby'          => 'date',
		'order'            => 'DESC',
		'post_type'        => 'patient_feedback',
		'author'           => $user_id,
		'post_status'      => 'private'
	);
	
	$query = new WP_Query( $args );
	//echo "Last SQL-Query: {$query->request}";
	$posts = $query->posts;

	//if(is_page('patient-health-journey'))
	$html = '';
	$physician_class = ' patient_side_health_chart';
	
	/* if($userrole == 'physician' || $userrole == 'caregiver' || $userrole == 'administrator' || $userrole == 'dispensary') {
		$html .= do_shortcode('[physicians-tab-nav]');
		$physician_class = ' physican_side_health_chart';
		$html .= '<div class="patient-details-outerwrap">';
		$html .= do_shortcode('[user_profile_dispaly]');
		$html .= '</div>';
	}
	else{
		$html .= do_shortcode('[patients-tab-nav]');
	} */
	$html .= '<section class="journey journey-container">';
		$html .= '<div class="container">
			<div class="row">
				<p class="title">Showing '.count($posts).' Health Check-Up Entries Between <span>'.date('F Y', strtotime('-2 month')).' - '.date('F Y').'</span></p>
				<div class="checkup_comments_table '.$physician_class.'">';
					$html .= $feedback_message;
				
					
					if(count($posts) > 0){
						$html .= '<header class="health_hd">';
							if($userrole == 'patient'){
								$html .= '<div class="hd_item"><strong>&nbsp;</strong></div>';
							}
							$html .= '<div class="hd_item"><strong>Date | Time</strong></div>
							<div class="hd_item"><strong>Status</strong></div>
							<div class="hd_item"><strong>Comments</strong></div>
						</header>';	
					}
					$html .= '<section class="table_data feedback-table">';
						foreach ($posts as  $row) {
							$postid 		= $row->ID;
							$status 		= get_post_meta($postid, 'feel',true);
							$comments 		= get_post_meta($postid, 'comments',true);
							$date 			= new DateTime($row->post_date);
							switch ($status) {
								case 'well':
									$nstatus = '<div class="bg_good">Well</div>';
									break;
								case 'v-well':
									$nstatus = '<div class="bg_vgood">Very Well</div>';
									break;
								case 'neutral':
									$nstatus = '<div class="bg_neutral">Neutral</div>';
									break;
								case 'unwell':
									$nstatus = '<div class="bg_bad">Unwell</div>';
									break;
								case 'v-unwell':
									$nstatus = '<div class="bg_vbad">Very Unwell</div>';
									break;	
							}
							$html .= '<div class="data_row">';
								if($userrole == 'patient'){
									$html .= '<div class="data_item">
										<!--Title for mobile devices in b tag-->
										<b>Action: </b>
										<a href="'.site_url().'/patient-dashboard/?post_id='.$postid.'" class="btn btn-sm btn-flat">Edit</a>
										<a href="'.$url.'?delete='.$postid.'" class="btn btn-sm btn-flat">Delete</a>
									</div><!--end data_item-->';
								}
								
								$html .= '<div class="data_item">
									<!--Title for mobile devices in b tag-->
									<b>Date, Time: </b><span>'.$date->format('D, F d y h:i:s A ').'</span>
								</div><!--end data_item-->
								<div class="data_item">
									<b>Health Status: </b>
									'.$nstatus.'
								</div><!--end data_item-->
								<div class="data_item">
									<!--Title for mobile devices in b tag-->
									<b>Comment: </b><span>'.$comments.'</span>
								</div><!--end data_item-->
								</div>';
						}  
					$html .= '</section>
				</div><!--End checkup comment table-->
			</div><!--End row-->
		</div>
	</section>';
	remove_filter( 'posts_where', 'filter_where' );
	return $html;
	
}
add_shortcode( 'patient_health_feedback_list', 'patient_health_feedback_list' );


?>