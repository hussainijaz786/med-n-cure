<?php

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly 


/* if(isset($_POST['patient_feedback_submit']) || isset($_POST['symptoms_tracker_submit'])){
	add_action('wp_head', 'submit_feedback_symptoms_tracker');
} */

add_action('wp_ajax_submit_face_symptoms_tracker_ajax','submit_face_symptoms_tracker_ajax');
add_action('wp_ajax_npriv_submit_face_symptoms_tracker_ajax','submit_face_symptoms_tracker_ajax');

function submit_face_symptoms_tracker_ajax(){
	
	/*
	*		Submit Patient Face Tracker
	*/
	
	$current_usid = get_current_user_id();
	$siteurl = get_site_url();
	$name  = get_user_meta( $current_usid, 'first_name', true )." ".get_user_meta( $current_usid, 'last_name', true );
	
	if(isset($_GET['patient_face_tracker_submit'])){
		
		$postid = isset($_GET['post_id']) ? $_GET['post_id'] : null;
		if(!$postid){
			$date = date("Y-m-d h:i:s",strtotime($_POST['updated_date']));
			$my_post = array(
				'post_type'  => 'patient_feedback',
				'post_title' => $name ,
				'post_content' => 'Patient Feedback mode details',
				'post_status' => 'private',
				'post_date' => $date,   // if you prefer
				'ping_status' => 'closed',
				'post_author'  => $current_usid
			);
			
			// Insert the post into the database
			$postid = wp_insert_post( $my_post );
		}
		
		update_post_meta($postid, 'feel', $_POST['feel']);
		update_post_meta($postid,'comments', $_POST['comments']);
		
		cdrmed_save_activity_log('Patient save Daily Feelings', '');
		
		wp_redirect($siteurl."/patient-dashboard/?view=2&feel=".$_POST['feel']."&post_id=".$postid);
		die();
	}
	
	
	/*
	*		Submit Patient Symptoms Tracker
	*/
	if(isset($_GET['symptoms_tracker_submit'])){
		
		$my_post = array(
			'post_type'  => 'symptoms_tracker',
			'post_title' => $name ,
			'post_content' => 'Patient Feedback mode details',
			'post_status' => 'private',
			'comment_status' => 'closed',   // if you prefer
			'ping_status' => 'closed',
			'post_author'  => $current_usid
		);

		// Insert the post into the database
		$postid = wp_insert_post( $my_post );
		
		update_post_meta($postid, 'anxious_mood',$_POST['anxious_mood']);
		update_post_meta($postid,'depressed_mood',$_POST['depressed_mood']);
		update_post_meta($postid, 'happy',$_POST['happy']);        
		update_post_meta($postid,'cannabis',$_POST['cannabis']);
		update_post_meta($postid,'relaxed',$_POST['relaxed']);
		update_post_meta($postid,'focused',$_POST['focused']);
		update_post_meta($postid,'energetic',$_POST['energetic']);
		update_post_meta($postid,'foggy_headed',$_POST['foggy_headed']);
		update_post_meta($postid,'dizzy',$_POST['dizzy']);
		update_post_meta($postid,'paranoid',$_POST['paranoid']);
		update_post_meta($postid,'sleepy',$_POST['sleepy']);
		
		/*
		*	Repeater Values
		*/
		if($_POST['cannabis'] == 'yes'){
			$today_cannabis_counter = 0;
			$cannabis_arr = array();
			foreach($_POST['_today_cannabis_amount'] as $today_cannabis){
				$cannabis_arr[] = array( 'id' => $today_cannabis_counter, 'amount' => $today_cannabis, 'frequency' => $_POST['_today_cannabis_frequency'][$today_cannabis_counter], 'method_of_ingested' => $_POST['_today_cannabis_ingested'][$today_cannabis_counter]);
				$today_cannabis_counter++;
			}
			if($today_cannabis_counter > 0)
			update_post_meta( $postid, '_today_used_cannabis', json_encode($cannabis_arr ) );
		}
		
		cdrmed_save_activity_log('Patient save Symptoms', '');
		
		wp_redirect($siteurl."/patient-dashboard/?view=1");
		die();
	}
}

?>