<?php 

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly 

//Includes for Frontend forms on Patient Side

include('intake-form.php');
include('mmj-recommendation-form.php');
include('pre-registration-form.php');
include('add-document.php');
include('active_tabs.php');
include('face-trackers-form.php');
include('symptoms-trackers-form.php');
include('view-symptoms-trackers.php');
include('view-face-trackers.php');
include('submit-face-symptoms-trackers.php');

?>
