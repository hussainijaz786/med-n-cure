<?php

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly 


add_action( 'wp_ajax_patient_get_physician_dispensary_ajax', 'patient_get_physician_dispensary_ajax');
add_action( 'wp_ajax_nopriv_patient_get_physician_dispensary_ajax', 'patient_get_physician_dispensary_ajax');


// Get all Dispensary Role Users at patient side for Products view

function patient_get_physician_dispensary_ajax() {
	
	$current_usid = get_current_user_id();
	
	$call_type = isset($_POST['call_type']) ? $_POST['call_type'] : '';
	//$call_type = ($_POST['call_type'] == 'phy') ? 'physician' : 'dispensary';
	
	$limit = (isset($_POST['limit']) && intval($_POST['limit']) != 0 ) ? $_POST['limit'] : 4;
	$offset = (isset($_POST['offset']) && intval($_POST['offset']) != 0 ) ? $_POST['offset'] : 0;
	
	//'role' => array( 'physician', 'administrator' ),
	$args = array(
		'role' => "$call_type",
		'orderby' => 'display_name',
		'number' => $limit,
		'offset' => $offset,
		'meta_query' => array(
			array(
				'key' => 'url',
				'value' => '',
				'compare' => '!='
			),
		),
	);
	if(isset($_POST['search']) && $_POST['search'] != ''){
			
		$args['meta_query'] = array(
			'relation' => 'OR',
			array(
				'key' => 'first_name',
				'value' => $_POST['search'],
				'compare' => 'LIKE'
			),
			array(
				'key' => 'last_name',
				'value' => $_POST['search'],
				'compare' => 'LIKE'
			),
			array(
				'relation' => 'AND',
				array(
					'key' => 'url',
					'value' => '',
					'compare' => '!='
				),
			),
		);	
	}
	
	if($call_type == 'physician'){
		$old_dispensary_arr = json_decode(get_user_meta($current_usid, 'patient_physician_id', true), true);
	}
	else{
		$old_dispensary_arr = json_decode(get_user_meta($current_usid, 'patient_dispenary_products_id', true), true);
	}
	
	//$getdispensaries = get_users( 'orderby=user_id&role=Dispensary' );
	
	$users = new WP_User_Query($args);
	$getdispensaries = $users->get_results();
	
	$url = site_url();
	
	$response = (sizeof($getdispensaries) > 0)? true : null;
	
	$html = '';
	foreach ($getdispensaries as $key => $value) {
		$dispensary_id = $value->ID;
		
		
		$logo = wp_get_attachment_url(get_user_meta($dispensary_id, 'logo', true));
		if($logo == '')
			$logo = 'http://placehold.it/50x50';
		
		$post_id = get_user_meta($dispensary_id, $call_type.'_post_id', true);
		
		$name = get_the_title($post_id);
		if(!$name){
			$name = get_user_meta($dispensary_id, 'first_name', true).' '.get_user_meta($dispensary_id, 'last_name', true);
		}
		if(!empty($name) && $name != ' '){
			$html .= '<div class="single-dispensary">';
				$html .= '<input type="checkbox" class="dispensary dispensaries_id" name="dispensaries_id" id="dispensaries_id'.$dispensary_id.'" value="'.$dispensary_id.'" ';
				if(sizeof($old_dispensary_arr) > 0 && in_array($dispensary_id,$old_dispensary_arr )){
					$html .='checked ';
				}
				
				
				$html .= '> <label for="dispensaries_id'.$dispensary_id.'">
					<img src="'. $logo .'" alt="dispensary logo"> 
					<span class="name">'.$name.'</span>';
				$html .= '</label>';
				$html .= '<din class="info">';
				
				$address = '';
				
				$address1 = get_post_meta($post_id, 'address_line1', true);
				
				if($address1 != ''){
				
					$address = '<span><span class="title_span"><i class="fa fa-map-marker"></i></span><span class="data_span">';
										
					//if adddress exists
					$address .= ($address1 != '') ? $address1 : '';
					
					$city = get_post_meta($post_id, 'city', true);
					//if city is exists
					$address .= ($city != '') ? ', '.$city : '';
					
					$state = get_post_meta($post_id, 'state__province__region', true);
					//if state exists
					$address .= ($state != '') ? ', '.$state : '';
					
					$country = get_post_meta($post_id, 'country', true);
					//if country exists
					$address .= ($country != '') ? ', '.$country : '';
					
					$address .= '</span></span>';
					$html .= $address;
				}
				
				$dispensary_url = get_post_meta($post_id, 'url', true);
				$phone = get_post_meta($post_id, 'phone', true);
				if($phone){
					$html .= '<span><span class="title_span"><i class="fa fa-phone"></i></span><span class="data_span">'.$phone.'</span></span>';
				}
				
				$html .= '<span><span class="title_span"><i class="fa fa-external-link"></i></span><span class="data_span">'.$dispensary_url.'</span></span>';
				
				if($call_type == 'dispensary'){
					$html .= '<a href="'.$url.'/inventory-list?dis='.$dispensary_id.'" target="_blank">View Inventory</a>';
				}
				
				$html .= '</div>';
			$html .= '</div>';
		}
	}
	
	$output = array('action' => $response, 'output' => $html);
	echo json_encode($output);
	die();
}

add_action( 'wp_ajax_patient_add_physician_dispensary_ajax', 'patient_add_physician_dispensary_ajax');
add_action( 'wp_ajax_nopriv_patient_add_physician_dispensary_ajax', 'patient_add_physician_dispensary_ajax');

function patient_add_physician_dispensary_ajax() {
	
	global $wpdb;
	$current_usid = get_current_user_id();
	$call_type = isset($_POST['call_type']) ? $_POST['call_type'] : '';
	
	$physician_dispensaries_id_arr = $_POST['physician_dispensaries_id_arr'];
	
	$patient_intake = get_user_meta($current_usid, 'patient_intake', true);
	//get user parent id
	$patient_parent_user_id = $wpdb->get_var( $wpdb->prepare( "select meta_value from $wpdb->postmeta where meta_key = 'patient_parent_user_id' and post_id = '%s' LIMIT 1", $patient_intake ) );
	
	$parent_id = null;
	if($patient_parent_user_id == 'orphan' && sizeof($physician_dispensaries_id_arr) > 0){
		$parent_id = $physician_dispensaries_id_arr[0];
		$child_user = get_user_meta($parent_id, 'user_parent', true);
		if($child_user != ''){
			$parent_id = get_user_meta($parent_id, 'user_parent_id', true);
		}
		update_post_meta($patient_intake, 'patient_parent_user_id', $parent_id);
		update_user_meta( $current_usid, 'patient_physician_id', '' );
	}
	
	if(!in_array($patient_parent_user_id, $physician_dispensaries_id_arr) || $parent_id){
		$user_role = cdrmed_get_user_role($patient_parent_user_id);
		if($call_type == $user_role){
			$physician_dispensaries_id_arr[] = $patient_parent_user_id;
		}
	}
	
	if($call_type == 'physician'){
		update_user_meta( $current_usid, 'patient_physician_id', json_encode($physician_dispensaries_id_arr) );
		cdrmed_save_activity_log('Patient add Physician', '');
	}
	else if($call_type == 'dispensary'){
		update_user_meta( $current_usid, 'patient_dispensary_id', json_encode($physician_dispensaries_id_arr) );
		update_user_meta( $current_usid, 'patient_dispenary_products_id', json_encode($physician_dispensaries_id_arr) );
		cdrmed_save_activity_log('Patient add Dispensaries', '');
	}
	
	echo 'List updated successfully!';
	die();
}