<?php

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly 

function ghistory_notes() {

	global $wpdb, $wp, $add_my_script;
	$add_my_script= true;
	$current_url = home_url( add_query_arg( NULL, NULL ) );
	$pid = !isset($_GET['pid'])?null:$_GET['pid'];
		if($pid) {
			$user = $pid;
		} else {
			$pid = get_current_user_id();
		}
	
	$url = site_url();
	$user_ID = get_current_user_id();
	$user_info = get_userdata($pid);
	$cid = $_POST['cid'];
	$first_name = $user_info->first_name; 
	$last_name = $user_info->last_name; 
 // Intake Notes
	$pnotes = "SELECT * FROM {$wpdb->prefix}comments_history
				where patient_id = $pid ORDER BY id DESC";
				
	$getnotes = $wpdb->get_results($pnotes);
	$html1 .= ' <div class="col-sm-12"><h3 class="gnotes">History Notes</h3>';
	if($getnotes){
  $html1 .= '<div class="notesalla">';
  foreach($getnotes as $key => $value) {
		$id = $value->id;
		$datecreated = $value->date;
		$comments = $value->comments;
		$newDate = date("m-d-Y", strtotime($datecreated));
	
	$html1 .= '<div class="col-sm-6"><div class="previousnotes"><div class="createdby">Created By '.$first_name.'<span class="createddate">'.$datecreated.'</span></div>';	
	$html1 .= '<div class="commentsnote" id="commentsnote">'.$comments.'</div></div></div></div>';	
	}
		$html1 .= '</div></div>';
	} else {
	$html1 .= '<p>There are no History notes available at the moment</p></div>';	
	}
	return $html1;

	
}
add_shortcode( 'ghistorynotes', 'ghistory_notes' );

?>