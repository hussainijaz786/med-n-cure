<?php


add_action( 'wp_ajax_add_medication_ajax', 'add_medication_ajax' );
add_action( 'wp_ajax_nopriv_add_medication_ajax', 'add_medication_ajax');

function add_medication_ajax() {
  global $wpdb;
    // The $_REQUEST contains all the data sent via ajax
    if ( isset($_REQUEST) ) {
        $keyword = $_REQUEST['keyword'];
		if($keyword != ''){
			$smth = preg_replace("/%u([0-9a-f]{3,4})/i","&#x\\1;",urldecode($keyword)); 
			$smth = ucfirst(html_entity_decode($smth,null,'UTF-8'));
			$wpdb->query( "insert into wp_medication set supplement='".$smth."',status='1'");
			echo "Medication Added Successfully.";
			
			$url = site_url();
			
			$password_reset_url = "<a href='".$url.'?page=medication_update&id='.$wpdb->insert_id.'&med_diag_offline_request=offline'."'>Click here to Approve it</a>";
			$args = array(
				'call_by' => 'new-medication-admin',
				'receiver' => get_bloginfo('admin_email'),
				'subject_name' => '', 
				'body_name' => $smth, 
				'body_part' => '', 
				'password_reset_url' => $password_reset_url,
			);	
			cdrmed_send_email($args);
		}
    }
    // Always die in functions echoing ajax content
   die();
}
 

add_filter( 'gform_add_field_buttons', 'add_map_field' );


add_action( 'wp_ajax_medication_auto_complete_get_locations_ajax_call', 'medication_auto_complete_get_locations_ajax_call');
add_action( 'wp_ajax_nopriv_medication_auto_complete_get_locations_ajax_call', 'medication_auto_complete_get_locations_ajax_call');

function medication_auto_complete_get_locations_ajax_call(){
	global $wpdb;
    $row = array();
    //$wpdb->query('DELETE FROM '.$wpdb->posts.' WHERE post_type = "pre-registrations" AND post_status= "publish"');
    $url   = $_SERVER['PHP_SELF'];
    $url   = strtok($_SERVER["HTTP_REFERER"],'?');
    if(isset($_GET['keyword']) && $_GET['keyword'] != ''){
		$smth = preg_replace("/%u([0-9a-f]{3,4})/i","&#x\\1;",urldecode($_GET['keyword'])); 
		$smth = html_entity_decode($smth,null,'UTF-8');
		$wpdb->query( "insert into wp_medication set supplement='".$smth."',status='1'");
		header('Location:'.$url);
		exit;
    }
    
    $query = $wpdb->get_results( "SELECT * FROM wp_medication where status='0'");

    if ( $query ){
        foreach ( $query as  $valuek) {
            if(!in_array($valuek->supplement, $row))    
            $row[] = str_replace("'", "", $valuek->supplement);
        }
    }
    
	echo json_encode($row);
	die();
	
}


