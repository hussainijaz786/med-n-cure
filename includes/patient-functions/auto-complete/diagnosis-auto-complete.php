<?php

add_action( 'wp_ajax_add_diagnosis_ajax', 'add_diagnosis_ajax' );
add_action( 'wp_ajax_nopriv_add_diagnosis_ajax', 'add_diagnosis_ajax');

function add_diagnosis_ajax() {
  global $wpdb;
    // The $_REQUEST contains all the data sent via ajax
    if ( isset($_REQUEST) ) {
        $keyword = $_REQUEST['keyword'];
		if($keyword != ''){
			$smth = preg_replace("/%u([0-9a-f]{3,4})/i","&#x\\1;",urldecode($keyword)); 
			$smth = ucfirst(html_entity_decode($smth,null,'UTF-8'));
			$wpdb->query( "insert into wp_disease set diagnosis='".$smth."',status='1'");
			echo "Diagnosis Added Successfully.";
			
			$url = site_url();
			
			$password_reset_url = "<a href='".$url.'?page=diagnosis_update&id='.$wpdb->insert_id.'&med_diag_offline_request=offline'."'>Click here to Approve it</a>";
			$args = array(
				'call_by' => 'new-diagnosis-admin',
				'receiver' => get_bloginfo('admin_email'),
				'subject_name' => '', 
				'body_name' => $smth, 
				'body_part' => '', 
				'password_reset_url' => $password_reset_url,
			);	
			cdrmed_send_email($args);
		}
    }
    // Always die in functions echoing ajax content
   die();
}
 

add_filter( 'gform_add_field_buttons', 'add_map_field' );
global $json;
function add_map_field( $field_groups ) {


    foreach ( $field_groups as &$group ) {
        if ( $group['name'] == 'advanced_fields' ) {
            $group['fields'][] = array(
                'class'     => 'button',
                'id'     => 'search-disease',
                'data-type' => 'text',
                'value'     => __( 'Disease Type:', 'gravityforms' ),
                'onclick'   => "StartAddField('text');"
            );
            break;
        }
    }

    return $field_groups;
}

add_action( 'wp_ajax_diagnosis_auto_complete_get_locations_ajax_call', 'diagnosis_auto_complete_get_locations_ajax_call');
add_action( 'wp_ajax_nopriv_diagnosis_auto_complete_get_locations_ajax_call', 'diagnosis_auto_complete_get_locations_ajax_call');

function diagnosis_auto_complete_get_locations_ajax_call(){
	global $wpdb;
    $url = $_SERVER['PHP_SELF'];
    $url = strtok($_SERVER["HTTP_REFERER"],'?');
   

    $query = $wpdb->get_results( "SELECT * FROM wp_disease where status='0'");

    $row = array();

    if ( $query )
    {
        foreach ( $query as  $valuek) {

            if(!empty($valuek->diagnosis)){    
            $row[] = str_replace("'", "",$valuek->diagnosis);
            $icd[str_replace("'", "",$valuek->diagnosis)] = str_replace("'", "",$valuek->icd_code);
            }

        }
    }
    
	$json = json_encode($row);
	$icd_code = json_encode($icd);
	$return_arr = array('json' => $json, 'icd' => $icd_code);
	echo json_encode($return_arr);
	die();
}

