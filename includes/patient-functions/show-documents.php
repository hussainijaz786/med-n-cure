<?php
if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

// Add Shortcode
function patient_documents_search() {
	global $wpdb;
	$url = site_url();
	$current_usid = get_current_user_id();
	$userrole = get_user_role();
	$patient_id = isset($_GET['pid'])? $_GET['pid'] : null;
	// WP_Query arguments
	
	if(isset($_GET['action']) && $_GET['action'] == "delete-user-document") {
		$html = '<div class="alert alert-success">Document Deleted Successfully.</div>';
	}
	if(!$patient_id) {
		$patient_id = $current_usid;
	}
	
	$args = array (
		'post_type'              => array( 'document' ),
		'author'                 => $patient_id,
		'posts_per_page'		=> -1
	);


	$query = new WP_Query( $args );

			$html = '<div id="loading" style="display:none"><img src="'.CDRMED.'/includes/assets/img/loading-secure-animation.gif" alt=""/></div>';
			
			$html = '<table data-toggle="table" data-show-toggle="false" data-classes="table table-hover stripped" data-striped="true" data-show-columns="true" data-id-field="id" data-pagination="true" data-search="false" data-page-size="10" data-show-export="false" data-page-list="[25, 50, 100, 250, 500, 1000, 5000]" data-smart-display="true" data-toolbar="#filter-bar" data-show-filter="true" data-mobile-responsive="true">';
			
			$html .= '<thead><tr>
					  <th data-field="id" data-sortable="true" data-sorter="starsSorter" data-sort-name="_id_data">Document Name</th>
					  <th>Type</th>
					  <th data-field="date" data-sortable="true">Date</th>
					  <th>Action</th>
					  </tr></thead><tbody>';

	// The Loop
	if ( $query->have_posts() ) {
		while ( $query->have_posts() ) {
			
			$query->the_post();
			// do something
			$postid = get_the_ID();
			$doc_title = get_post_meta($postid, 'document_title', true);//get_field('document_title',$postid);
			$doc_desc = get_post_meta($postid, 'document_description', true);//get_field('document_description',$postid);
			$doc_type = get_post_meta($postid, 'document_type', true);//get_field('document_type',$postid);
			$doc_link1 = get_post_meta($postid, 'patient_document', true);//get_field('patient_document',$postid);
			$doc_link = wp_get_attachment_url( $doc_link1 ); 
			$date = get_the_date( 'm-d-Y', $postid );
		
			$html .= '<tr>';
			
				$html .= '<td data-id="'.strtolower($doc_title).'">
				<div><input type="hidden" value=""  class="doc-file" />
					
					<a class="load-cdrmed-modal-box patient-show-ducument editdoc" data-call_action="patient_show_document" modal-title="View '.$doc_title.'" modal-action="view-document" modal-footer="false" data-path="'.$doc_link.'">'.$doc_title.'</a>
					
					<input type="hidden" value="'.$doc_type.'"  class="doc-type" />
					<input type="hidden" value="'.$doc_desc.'"  class="doc-desci" />
				</div></td>';
				 
				$html .= '<td>'.$doc_type.'</td>';
				$html .= '<td>'.$date.'</td>';
			
				if($userrole != 'patient'){
					$html .= '<td><a class="load-cdrmed-modal-box patient-edit-document editdoc"  data-call_action="patient_edit_document" modal-title="Update Document" modal-action="edit-document" modal-footer="false" data-post-id ="'.$postid.'">Edit</a> / <a href="'.$url.'/patient-documents/?pid='.$patient_id.'&action=delete-user-document&post='.$postid.'">Delete</a></td>';
				}
				else{
					$html .= '<td><a class="load-cdrmed-modal-box patient-edit-document editdoc"  data-call_action="patient_edit_document" modal-title="Update Document" modal-action="edit-document" modal-footer="false" data-post-id ="'.$postid.'">Edit</a> / <a href="'.$url.'/patient-dashboard/?action=delete-user-document&post='.$postid.'#documents">Delete</a></td>';
				}
			 /* else {
				$edit_url = $url.'/patient-dashboard/?action=edit-user-document&post='.$postid.'#documents';
				$html .= '<td><!--<a  href="'.$edit_url.'">Edit</a>--> 
				<a data-target="#docEditMbox'.$postid.'" id="'.$postid.'" class="editdoc" data-toggle="modal" href="#">Edit</a>
				/ <a href="'.$url.'/patient-dashboard/?action=delete-user-document&post='.$postid.'#documents">Delete</a></td>';
			} */
			$html .= '</tr>';
			
			//$row_counter++;
		
		}
	} else {
		$html = '<div class="journey"><h3>No Documents Added Yet!</h3></div>';
	}

	// Restore original Post Data
	wp_reset_postdata();
	$html .= '</tbody></table>';
	return $html;
}
add_shortcode( 'patient-documents', 'patient_documents_search' );



add_action ('init','delete_patient_document');
function delete_patient_document() {
	global $wpdb;
	$act = isset($_GET['action']) ? $_GET['action'] : '';
	$postid = isset($_GET['post']) ? $_GET['post'] : '';
if($act == "delete-user-document" && $act != '') {
		wp_delete_post( $postid );
	}
			
}


?>