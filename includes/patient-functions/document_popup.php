<?php 

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly 

// Add Shortcode
//add_action( 'wp_head', 'model_boxes2');
function model_boxes2() {
 //if(is_page(array( 'patient-dashboard','patient-documents' ))){   
?>  
    <div class="modal fade" id="myModalmg" role="dialog">
            <div class="modal-dialog multi-select-modal navyblue">
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close fa fa-close" data-dismiss="modal"></button>
                        <h4 class="modal-title" id="title">Document Image</h4>
                        <div class="download-button-pp" id="down-doc-bt"><a href="#" id="fdlinks" download><img src="<?php echo CDRMED; ?>/includes/assets/img/download-file-btn.png" class="dfb" width="90"/></a></div>
                        <p class="docutment-types">Document Type: <span id="docu-type">Document Type Here</span></p>
                        <p id="mbox-doc-desc">Description of Document</p>
                    </div>
                    <div class="modal-body" id="img">
                    
                        <div class="text-center">
                         <button type="button" id="mode1" name="ok" class="btn preview">OK</button>
                       </div>
                    </div>
                    
                </div>
            
            </div>
        </div>
 <?php }  // function ends here
   


      //}  
 
//add_action( 'wp_ajax_documents_ajax_request', 'documents_ajax_request' );
function documents_ajax_request() {
  global $wpdb;
    // The $_REQUEST contains all the data sent via ajax
    if ( isset($_REQUEST) ) {
     
        $keyword = $_REQUEST['keyword'];

     
 ?>
 <div class="modal domodel docedits fade" id="docEditMbox<?php echo $keyword;?>" role="dialog">
            <div class="modal-dialog multi-select-modal navyblue">
                <!-- Modal content-->
                <div class="modal-content">
                <br>
                    <h4 class="modal-title"><center>Update Document</center></h4>
                    <?php echo  acf_form(array(
                    'post_id' => $keyword, // Get the post ID
                    'field_groups' => array('group_56ddf2042490e'),
                    'label_placement' => 'left',
                    'updated_message' => __("Your document has been updated successfully.", 'acf'),
                    'submit_value'      => 'Update Document'
                ));
       /* wp_redirect( $url, 301);
        exit;  */ 

                ?>
                    <div class="modal-body" id="img">
                    
                        <div class="text-center">
                         <button type="button" id="docedit" name="ok" class="btn preview docedit">Close</button>
                       </div>
                    </div>
                </div>
            
            </div>
        </div>
        <?php
    }
    // Always die in functions echoing ajax content
   die();
}


add_action('acf/save_post', 'my_acf_save_post', 20);

function my_acf_save_post( $post_id ){
	if( get_post_type($post_id) !== 'document' ) {		
		return;	
	}
	// bail early if editing in admin
	if( is_admin() ) {
		return;	
	}
	
	$current_usid = get_current_user_id();
	$userrole = get_user_role();
	if($userrole == 'patient'){
		cdrmed_save_activity_log('Patient Document updated!', '');
	}
	else{
		$auth = get_post($post_id);
		$patient_id = $auth->post_author;
		cdrmed_save_activity_log('Patient Document updated!', $patient_id);
	}
	
	
     $url= 'http://'.$_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
    wp_redirect( add_query_arg( 'updated', 'true', $url));exit(); 
}


// add_shortcode( 'strain_popup_dispaly', 'model_boxes' );
// Add Shortcode
//add_action( 'wp_head', 'model_notes');
function model_notes() {
 //if(is_page(array( 'patient-dashboard','patient-documents' ))){   
?>  
    <div class="modal fade" id="notesalla" role="dialog">
            <div class="modal-dialog multi-select-modal navyblue">
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close fa fa-close" data-dismiss="modal"></button>
                        <h4 class="modal-title" id="title">Notes History</h4>
                        
                    </div>
                    <div class="modal-body" id="user_notes">
                    
                        
                    </div>
                    
                </div>
            
            </div>
        </div>
 <?php }  // function ends here
   ?>