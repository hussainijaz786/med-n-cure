<?php

/* 
File conatins the [edit_child_profile] shortcode for add new Child users
A popup window for Child User Update profile
*/

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly 

/*
*
*
*	Patient dispensary Modal
*
*
*/
//add_action( 'wp_footer', 'patient_dispensary_model_modal');
function patient_dispensary_model_modal() {
	// popup show only in user setting page
	$userrole = get_user_role();
	if( $userrole == 'subscriber' || $userrole == 'patient' ){
	?>
	<div class="patient_dispensary_model">
		<div id="patient_dispensary_model" class="modal fade">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<input type="hidden" id="call_type" class="call_type">
						<button type="button" class="close fa fa-close" data-dismiss="modal"></button>
						<h4 class="modal-title" id="custom-user-modal-heading"></h4>
					</div>
					<div class="modal-body">
						<div class="patient-dispensary-filter">
							<input type="text" class="form-control patient-dispensary-search" placeholder="Search: Type Name"><br>
						</div>
						<div class="patient_dispensaries_modal_body" id="patient_dispensaries_modal_body">
						</div>
						<span id="patient_dispensary_feedback" class="feedback"></span>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-primary" id="patient-dispensary-modal-btn"></button>
					</div>
				</div>
			</div>
		</div>
	</div>

	<?php
	}
}