<?php
if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly 

add_action( 'wp_ajax_edit_documents_ajax', 'edit_documents_ajax');
add_action( 'wp_ajax_nopriv_edit_documents_ajax', 'edit_documents_ajax');

function edit_documents_ajax() {
	ob_start();
	$post_id = $_POST['post_id'];
	
	acf_form(array(
		'post_id' => $post_id,
		'field_groups' => array('group_56ddf2042490e'),
		'label_placement' => 'left',
		'updated_message' => __("Your document has been updated successfully.", 'acf'),
		'submit_value'      => 'Update Document'
	));
	?>
    <script>
        (function($) {
            jQuery(document).trigger('acf/setup_fields', jQuery(".cdrmed-modal-box") );
            acf.do_action('ready', $('body'));
            acf.do_action('load', $('body'));
            acf.fields.wysiwyg.initialize();
        })(jQuery);
    </script>
	<?php
	$html = ob_get_contents();
	ob_get_clean();
	if (ob_get_contents()) ob_end_flush();
	echo $html;
	die();
}