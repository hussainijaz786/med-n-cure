<?php
if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

// Add Shortcode
function patient_therapy_latest_display() {
	$userrole = get_user_role();
	$html = '';
	
	$patient_id = isset($_GET['pid'])? $_GET['pid'] : get_current_user_id();
	
	$url = site_url();
	$therapy_id = get_the_author_meta('_cdrmed_therapy_id', $patient_id);
	
	//$therapy_shoop_id = get_post_meta( $therapy_id, '_cdrmed_therapy_shoop_id', true );
	$user_meta_therapy_id = get_user_meta($patient_id, '_cdrmed_therapy_id',true);
	
	//if patient's shoop change then therapy hide for patient
	if($therapy_id == $user_meta_therapy_id){
		//pass therapy id to function and fetch therapy detail
		$html .= get_print_therapy($therapy_id, 'print-therapy');
		
	}else {
		$html .= '<div class="journey"><br><br><h3>No Therapy Defined Yet</h3></div><br><br>';
		if($userrole == 'patient' ){
			$html .= '<br><center><a class="btn bordered" href="'.$url.'/patient-dashboard?page_type=dosing_history">Dosing History</a></center>';
		}
	}
	return $html;
}
add_shortcode( 'patient-therapy-display', 'patient_therapy_latest_display' );






function get_print_therapy($therapy_id, $call_by = ''){
	
	$userrole = get_user_role();
	$url = site_url();
	$order_id = get_post_meta( $therapy_id, 'order_id', true );
	$therapy_total_medicine = get_post_meta( $therapy_id, '_total_medicine', true );
	$therapy_shoop_id = get_post_meta( $therapy_id, '_cdrmed_therapy_shoop_id', true );
	//get therapy data in form of array
	$therapy_data = get_post_meta( $therapy_id, '_cdrmed_therapy_data', false );
	
	$html = '';
	
	$ex_data_arr = $therapy_data['0'];
	if($ex_data_arr) {
		$html .= '<div class="journey"><br><br><h3>Current Patient Therapy:</h3></div>';
		if($call_by == 'print-therapy'){
			if($userrole == 'patient' ){				
				$html .= '<center><a href="#shoop" class="btn bordered" id="printDiv" data-print="shoopdosage">Print Dosage Information</a>&nbsp;&nbsp;<a class="btn bordered" href="'.$url.'/patient-dashboard?page_type=dosing_history">Dosing History</a></center>';
			}
			else{
				$html .= '<center><a href="#shoop" class="btn bordered" id="printDiv" data-print="shoopdosage">Print Dosage Information</a></center><br>';
			}
		}
		
		$html .= '<div id="shoopdosage">
			<span class="therapy-shoop-id">Therapy ID: '.$therapy_id.'</span>';
			if($order_id){
				$html .= '<span class="therapy-shoop-id">Status: '.ucfirst(str_replace('wc-','',get_post_status( $order_id ))).'</span>';
			}
			$i = 0;
			foreach($ex_data_arr as $key => $val){
				
				$ex_data = $val;
				$therapy = json_decode($ex_data);
				
				$product_name = $therapy->p_product_title;
				$pcan = $therapy->p_cannabinoid;
				$pcan_value = $therapy->p_cannabinoid_value;
				$pcan_percent_value = $therapy->p_cannabinoid_percent_value;
				$scan = $therapy->s_cannabinoid;
				$scan_value = $therapy->s_cannabinoid_value;
				$scan_percent_value = $therapy->s_cannabinoid_percent_value;
				$total_this_value = $therapy->this_much_value;
				$dpd = $therapy->dose_per_day;
				$sec_cal = $therapy->secondary_calculations;
				$freq = $therapy->frequency_per_day;
				$full_profile = $therapy->full_profile_link;
				$calculations = $therapy->calculations;

				if($userrole != 'patient') {
					$html .= ' <div class="col-sm-12">';
				} else {
					$html .= '<div class="col-sm-6">';
				}
				$html .= '<div class="shoop_review-content'.($i+1).' last dosageinfocal" style="margin-top:20px;">
					<div id="shoop_review-content'.($i+1).'">
					<section class="shoop-hd-crcl'.($i+1).' shoop-title-b"><b class="">'.$product_name.'</b></section>
					<ul class="shoop_list'.($i+1).'">
					 <!--<li>'.$product_name.'</b></li>-->
					 <li>'.$pcan_percent_value.' % <b>'.$pcan.'</b> | '.$scan_percent_value.' % <b>'.$scan.'</b></li>
					 <li><b>To Be Taken</b> '.$freq.'</li>
					 <li><b>'.$pcan.' Per Day:</b> '.ROUND($total_this_value, 2).' (g)</li>
					 <li><b>Dose of</b> '.$pcan.' each time: '.$dpd.' (g)</li>';
					 if(intval($therapy_total_medicine) == 2){
						 //$html .= '<li><b>Amount of</b> '.$scan.' Per Day: '.$sec_cal.' (mg)</li>';
						 $html .= '<li><b>Total Amount of</b> '.$pcan.': '.ROUND($pcan_percent_value*10*$total_this_value, 2) .' (mg)</li>';
						 $html .= '<li><b>Total Amount of</b> '.$scan.': '.ROUND($scan_percent_value*10*$total_this_value, 2) .' (mg)</li>';
					 }
					 else{
						 $html .= '<li><b>Total Amount of</b> '.$scan.': '.$sec_cal.' (mg)</li>';
					 }
					if($call_by == 'print-therapy'){
						$html .= '<li><center>'.$full_profile.'</center></li>';
					}
					$html .= '</ul>
				</div></div></div>';
				$i++;
			}
		$html .= '</div>';
	} elseif($call_by == 'print-therapy'){
		$html .= '<div class="journey"><br><br><h3>No Therapy Defined Yet</h3></div><br><br>';
		if($userrole == 'patient'){
			$html .= '<br><center><a class="btn bordered" href="'.$url.'/patient-dashboard?page_type=dosing_history">Dosing History</a></center>';
		}
	}
	return $html;
}
