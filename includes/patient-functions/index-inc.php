<?php 

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly 

//Includes for Patient Side Dashboard Functions
include('dispensary-list-ajax-calls.php');
include('dispensary-list-modal.php');
include('show-documents.php');
include('patient_message.php');
include('document_popup.php');
include('details_result.php');
include('ghistory_notes.php');
include('save-therapy.php');
include('calculate-two-medicine-dose-ajax-call.php');
include('patient-dashboard-home.php');
include('patient-therapy-display.php');
include('import-patient-and-intake-active-ajax-call.php');
include('import-requests.php');
include('documents-ajax-call.php');
//Frontend Forms on Patient Side Such as Documents, Intake and Pre-Registrations Include
include('frontend-forms/index-inc.php');
//Auto Complete Field for Medicine and Primary Diagnosis On Patient Side
include('auto-complete/index-inc.php');
?>
