<?php
if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly 

// Function For Creating SHOOP Data, We will Use Same Form for Editing of SHOOP As Well. We don't need to create multiple forms. It's just gonna make it more complex and full of error. Currently, I have no clear plan if we will be passing reference to function or we will use same good old ID Parameter in URL. So its just fine however it may work best.
function cdrmed_create_new_shoop($shoopid){
	
	// Make the Form Ready to Accept and Populate Values for Editing of SHOOP This is just getting of Values. Still actual form needs to be able to get values for editing etc.
	// Get $POST ID and assume that form is going to be in edit mode, as long as Page Type Edit Parameter is also in URL
	$pid = isset($_GET['pid']) ? $_GET['pid'] : '';
	$post_id = isset($_GET['post_id']) ? $_GET['post_id'] : null;
	$page_type = isset($_GET['page_type']) ? $_GET['page_type'] : 'create';
	
	
	
	//error_reporting(E_ALL);
	////ini_set('display_errors', 0);
	
	$primary_dosage = $ingestion_method = '';
	
	$shoop_size = 5;
	
	if(!empty($post_id) && $page_type == 'edit'){
		$shoop_size = 1;
		//$patient_id = get_post_meta( $post_id, 'patient_id', true );
		for( $j=1; $j<5; $j++ ) {
			
			${'primary_dosage'.$j} = get_post_meta( $post_id, 'medicine'.($j).'_cannabinoid_1', true );
			${'daily_dosage'.$j} = explode("X",get_post_meta( $post_id, 'medicine'.($j).'_target_dosage_1', true ));
			${'frequency'.$j} =  explode("X",get_post_meta( $post_id, 'medicine'.($j).'_frequency_1', true ));
			${'notes'.$j} = get_post_meta( $post_id, 'medicine_notes'.($j), true );   
			${'terpene'.$j} = get_post_meta( $post_id, 'medicine'.($j).'_terpene', true );
			${'medicine1_dosage_frequency'.$j} = explode(",",get_post_meta( $post_id, 'medicine'.($j).'_dosage_frequency'.($j), true ));
			${'medicine1_dosage_mg'.$j} = explode(",",get_post_meta( $post_id, 'medicine'.($j).'_dosage_mg'.($j), true ));
			${'medicine1_dosage_timing'.$j} = explode(",",get_post_meta( $post_id, 'medicine'.($j).'_dosage_timing'.($j), true ));
			${'ingestion_method'.$j} = get_post_meta( $post_id, 'medicine'.($j).'_ingestion_method', true );
			if(${'primary_dosage'.$j} != ''){
				$shoop_size++;
			}
		}
	}
	
	// We will replace Primary Dosage and Ingestion Method Code below while we are going to set our form in Edit Mode. It is necessary. Right now we are not setting up form for Edit Mode, so just empty variables to remove the warnings.
	
	$lst = '';
    $arr = array('Inhaled','Ingested (edible)','Sublingual','Suppository','Topical');
	
	//remove becasue edit and create form is same
	
	$html = '<section class="strains_section_holder">
        <form method="post" id="formId" class="shoop_edit_update_form" action="'.get_admin_url().'admin-ajax.php?action=cdrmed_save_update_shoop&pid='.$pid.'&page_type='.$page_type.'&post_id='.$post_id.'">
			<input type="hidden" id="shoop_patient" name="patient_id" value="'.$pid.'" />
			<input type="hidden" id="medicine_page_type" name="page_type" value="'.$page_type.'" />
			<input type="hidden" id="medicine_post_id" name="post_id" value="'.$post_id.'" />
            <div class="container-fluid">
                <div class="row">
                    <div class="col-sm-12">
                        <section class="main-heading">
                            <h2>Please Complete the following Information and Preview the Patients SHOOP before continuing.<br>Ingredients are color coded throughout the system from Input to Output of the SHOOP.</h2>
                            <div class="col-sm-12">
								<input type="text" id="cdrmed_shoop_date" class="cdrmed_shoop_date" name="cdrmed_shoop_date" value="" placeholder="mm/dd/yyyy"/>
							</div>
                        </section>';
	
	$array_index = 0;
	
	//for( $i=1; $i < $shoop_size; $i++ ) {
	for( $i=1; $i < 5; $i++ ) {
		
		//If call for new shoop then init empty variables
		if($page_type == 'create'){
			${'primary_dosage'.$i} = '';
			${'daily_dosage'.$i} = '';
			${'frequency'.$i} =  '';
			${'notes'.$i} = '';
			${'terpene'.$i} = '';
			${'medicine1_dosage_frequency'.$i} = '';
			${'medicine1_dosage_mg'.$i} = '';
			${'medicine1_dosage_timing'.$i} = '';
			${'ingestion_method'.$i} = '';
		}
		
			switch ($i) {
				  case '1':
					 $medicine_color = 'purple';
					 $exclass = 'last';
					 $style = '';
					  break;
				  case '2':
					  $medicine_color = 'reded';
					  $style = 'style= "display : none;"';
					  break;
				  case '3':
					   $medicine_color = 'lightblue';
					   $style = 'style= "display : none;"';
					  break;
				  case '4':
					   $medicine_color = 'lightgreen';
					   $style = 'style= "display : none;"';
					  break; 
				  default:
				  	$medicine_color = 'purple';
					 $exclass = 'last';
					 $style = '';
			  }
			  
			$primary_dosage = ${"primary_dosage".$i};
			if($primary_dosage != ''){
				 $style = '';
			}
			$daily_dosage = ${'daily_dosage'.$i};
			$frequency = $frequency_day = '';
			/*  && array_key_exists($i,(${'frequency'.$i}))
			* Added by Hassan 06:19PM
			* 20/10/2016
			*/
			if(is_array(${'frequency'.$i}) && array_key_exists(1, (${'frequency'.$i}))){
				$frequency = ${'frequency'.$i}[0];
				$frequency_day = str_replace(' ','',${'frequency'.$i}[1]);
			}
			/* if(!empty($post_id) && $page_type == 'edit'){
				$frequency = ${'frequency'.$i}[0];
				$frequency_day = str_replace(' ','',${'frequency'.$i}[1]);
			} */
			
			$ingestion_method = ${'ingestion_method'.$i};
			$notes = ${'notes'.$i};
			$terpene = ${'terpene'.$i};
			
			//$medicine1_dosage_timing = ${'medicine1_dosage_timing'.$i};

			$medicine1_dosage_mg = '';
			if(is_array(${'medicine1_dosage_mg'.$i}) && array_key_exists($array_index, (${'medicine1_dosage_mg'.$i}))){
				$medicine1_dosage_mg = str_replace(' ','',${'medicine1_dosage_mg'.$i}[$array_index]);
			}
			 
			$medicine1_dosage_html = '';
			
			$medicine1_dosage_counter = 1;
			if(!empty($post_id) && $page_type == 'edit'){
				
				$medicine1_dosage_frequency = ${'medicine1_dosage_frequency'.$i};
				
				if($medicine1_dosage_frequency[0] != '' && $frequency_day == 'daily'){
					$dose_counter = 0;
					foreach ($medicine1_dosage_frequency as  $value) {
						
						$medicine1_dosage_html .= '<div class="col-sm-6 news'.$medicine1_dosage_counter.'">
							<div class="form-group">
								<label>Daily Dose '.$medicine1_dosage_counter.'</label><span class="fa fa-question round-info" data-toggle="tooltip" title="" data-original-title="Target Daily Dose"></span>
							</div>
							<div class="form-group">
								<input name="daily_dosage_frequncy'.$i.'[]" id="daily_dosage_frequncy" type="text" value="'.$value.'" class="appended-width two-third form-control" tabindex="2">
								<select class="selectpicker new-appended one-third" name="primary_dosage_timing'.($i).'[]">';
									$selected = ((${'medicine1_dosage_timing'.$i}[$dose_counter]=='morning')? ' selected="selected"' : '');
									$medicine1_dosage_html .= '<option value="morning" '.$selected.'>morning</option>';
									$selected = ((${'medicine1_dosage_timing'.$i}[$dose_counter]=='afternoon')? ' selected="selected"' : '');
									$medicine1_dosage_html .= '<option value="afternoon" '.$selected.'>afternoon</option>';
									$selected = ((${'medicine1_dosage_timing'.$i}[$dose_counter]=='evening')? ' selected="selected"' : '');
									$medicine1_dosage_html .= '<option value="evening" '.$selected.'>evening</option>';
									$selected = '';
								 $medicine1_dosage_html .= '</select>
								<select class="selectpicker new-appended one-third" name="frequency_daily_dosage'.($j+1).'[]" tabindex="-98">';
									
									$selected = (($medicine1_dosage_mg=='mg')? ' selected="selected"' : '');
									$medicine1_dosage_html .= '<option value="mg" '.$selected.'>mg</option>';
									$selected = (($medicine1_dosage_mg=='g')? ' selected="selected"' : '');
									$medicine1_dosage_html .= '<option value="g" '.$selected.'>g</option>';
									$selected = (($medicine1_dosage_mg=='ml')? ' selected="selected"' : '');
									$medicine1_dosage_html .= '<option value="ml" '.$selected.'>ml</option>';
									$selected = '';
								 $medicine1_dosage_html .= '</select>
							</div>
						</div>';
						$medicine1_dosage_counter++;
						$dose_counter++;
					}
				}
			}
			  
			  
                   $html .= '<section class="strain '.$medicine_color.' '.$exclass.'" '.$style.'>
							<input type="hidden" class="myterpene'.$i.'" name="terpene[]" value="" />
                            <div class="col-sm-12"><b>Medicine '.$i.'</b></div>
                            <div class="col-sm-12"><b>Target Cannabinoid</b><span class="fa fa-question round-info" data-toggle="tooltip" title="Choose the cannabinoid to include in this medicine"></span></div>
                            <div class="col-sm-6">
                                <div class="form-group">
									<div clsss="form-control">
										<select name="primary_dosage[]" id="primary_dosage" class="selectpicker full-width primary_dosage">
											<option value="">Select Target Cannabinoid...</option>
											<option '.(($primary_dosage=='Tetrahydrocannabinol (THC)')? ' selected="selected"' : '').' value="Tetrahydrocannabinol (THC)">Tetrahydrocannabinol (THC)</option>
											<option '.(($primary_dosage=='Cannabidiol (CBD)')? ' selected="selected"' : '').' value="Cannabidiol (CBD)">Cannabidiol (CBD)</option>
											<option '.(($primary_dosage=='Cannabichromene (CBC)')? ' selected="selected"' : '').' value="Cannabichromene (CBC)">Cannabichromene (CBC)</option>
											<option '.(($primary_dosage=='Cannabichromenic acid (CBCA)')? ' selected="selected"' : '').' value="Cannabichromenic acid (CBCA)">Cannabichromenic acid (CBCA)</option>
											<option '.(($primary_dosage=='Cannabichromivarin (CBCV)')? ' selected="selected"' : '').' value="Cannabichromivarin (CBCV)">Cannabichromivarin (CBCV)</option>
											<option '.(($primary_dosage=='Cannabichromivarin acid (CBCA)')? ' selected="selected"' : '').' value="Cannabichromivarin acid (CBCA)">Cannabichromivarin acid (CBCA)</option>
											<option '.(($primary_dosage=='Cannabidiolic acid (CBDA)')? ' selected="selected"' : '').' value="Cannabidiolic acid (CBDA)">Cannabidiolic acid (CBDA)</option>
											<option '.(($primary_dosage=='Cannabidivarin (CBDV)')? ' selected="selected"' : '').' value="Cannabidivarin (CBDV)">Cannabidivarin (CBDV)</option>
											<option '.(($primary_dosage=='Cannabidivarin acid (CBDVA)')? ' selected="selected"' : '').' value="Cannabidivarin acid (CBDVA)">Cannabidivarin acid (CBDVA)</option>
											<option '.(($primary_dosage=='Cannabigerol (CBG)')? ' selected="selected"' : '').' value="Cannabigerol (CBG)">Cannabigerol (CBG)</option>
											<option '.(($primary_dosage=='Cannabigerolic acid (CBGA)')? ' selected="selected"' : '').' value="Cannabigerolic acid (CBGA)">Cannabigerolic acid (CBGA)</option>
											<option '.(($primary_dosage=='Cannabigevarin (CBGV)')? ' selected="selected"' : '').' value="Cannabigevarin (CBGV)">Cannabigevarin (CBGV)</option>
											<option '.(($primary_dosage=='Cannabigevarin acid (CBGVA)')? ' selected="selected"' : '').' value="Cannabigevarin acid (CBGVA)">Cannabigevarin acid (CBGVA)</option>
											<option '.(($primary_dosage=='Cannabinol (CBN)')? ' selected="selected"' : '').' value="Cannabinol (CBN)">Cannabinol (CBN)</option>
											<option '.(($primary_dosage=='Tetrahydrocannabinolic acid (THCA)')? ' selected="selected"' : '').' value="Tetrahydrocannabinolic acid (THCA)">Tetrahydrocannabinolic acid (THCA)</option>
											<option '.(($primary_dosage=='Tetrahydrocannabivarin (THCV)')? ' selected="selected"' : '').' value="Tetrahydrocannabivarin (THCV)">Tetrahydrocannabivarin (THCV)</option>
											<option '.(($primary_dosage=='Tetrahydrocannabivarin acid (THCVA)')? ' selected="selected"' : '').' value="Tetrahydrocannabivarin acid (THCVA)">Tetrahydrocannabivarin acid (THCVA)</option>
										</select>
									</div>
								</div>
							</div>
                            <div class="col-sm-6">
                                <a href="#" class="btn bordered" data-toggle="modal" data-target="#myModal'.$i.'">Select Terpenes</a>
                                <span class="fa fa-question round-info" data-toggle="tooltip" title="Choose the Terpenes you want to include in this medicine"></span>
								<div class="selected-terpens1'.$i.'" style="display:none;"><b>Selected Terpenes:</b><span>'.$terpene.'</span></div>
                            </div>
							<div class="clearfix"></div>
							
							<div class="col-sm-6">
								<div class="form-group">
									<label>Target Daily Dose</label>
									<span class="fa fa-question round-info" data-toggle="tooltip" title="Target Daily Dose"></span>
								</div>
                                <div class="form-group">
									<input name="daily_dosage[]" id="daily_dosage" type="text" value="'.trim(@$daily_dosage[0]).'" class="two-third form-control daily_dosage" tabindex="2">
									<select class="selectpicker one-third" name="daily_dosage_mg[]">
										<option value="mg">mg</option>
                                        <option value="g">g</option>
                                    </select>
                                </div>
                            </div>
							
							<div class="col-sm-6">
								<div class="form-group">
									<label>Frequency</label>
									<span class="fa fa-question round-info" data-toggle="tooltip" title="Enter the frequency for the medicine, i.e., two times daily"></span>
								</div>
								<div class="form-group">
                                    <select name="frequency[]" id="frequency'.$i.'" class="selectpicker two-third">
										<option value="">Select Frequency...</option>';
										for($a=1; $a < 15; $a++){
											$selected = '';
											if($frequency == $a){
												$selected = 'selected';
											}
											$html .= '<option '.$selected.' value="'.$a.'">'.$a.'</option>';
										}
									$html .= '</select>
                                    <span class="in-to">x</span>
                                    <select class="selectpicker one-third frequency'.$i.'" name="primary_daily_frequency[]">';
										$selected = (($frequency_day=='')? ' selected' : '');
                                        $html .= '<option value="" '.$selected.'>-Select-</option>';
										$selected = (($frequency_day=='daily')? ' selected' : '');
										$html .= '<option value="daily" '.$selected.'>daily</option>';
										$selected = (($frequency_day=='hourly')? ' selected' : '');
                                        $html .= '<option value="hourly"'.$selected.'>hourly</option>';
										$selected = '';
                                    $html .= '</select>
                                </div>
                            </div>
                            <div class="new'.$i.'">
							'.$medicine1_dosage_html.'
							</div>
                            <div class="clearfix"></div>
                            
							<div class="col-sm-6">
								<div class="form-group">
                                    <label>Ingestion Method</label>
                                    <span class="fa fa-question round-info" data-toggle="tooltip" title="Select the ingestion method you want the patient to use"></span>
								</div>
                                <div class="form-group">
									<select name="ingestion_method[]" class="selectpicker full-width" id="ingestion_method">
										<option value="Select Ingestion Method...">Select Ingestion Method...</option>';
						                foreach ($arr as $value) {
											$selected = '';
											if($ingestion_method == $value){
												$selected = 'selected';
											}
											$html .="<option ".$selected." value='".$value."'>$value</option>";
										}
						            $html .= '</select>
                                </div>
                            </div>
							<div class="col-sm-6">
                                <div class="form-group">
									<label>Notes</label>
									<span class="fa fa-question round-info" data-toggle="tooltip" title="Notes"></span>
								</div>
								<div class="form-group">
									<textarea name="notes[]" id="notes" class="full-width notes form-control">'.(!empty($notes) ? $notes : '').'</textarea>
								</div>
                            </div> 
                            <div class="clearfix"></div>
							<div class="col-md-6 col-sm-12 ">';
							$html .= '<div class="form-buttons">
									<a href="javascript:void(0)" class="btn bordered with-icon" id="'.$medicine_color.'_add" onClick="'.$medicine_color.'_add();"><span class="fa fa-plus-circle"></span>Add Medicine</a>
									<span class="fa fa-question round-info" data-toggle="tooltip" title="Add medicine to SHOOP Panel"></span>';
							// We need to Add Remove Button Only on Medicines No. 2,3,4. Medicine 1 will Always prevail.
							if($i > 1) {
							$html .= '<a href="javascript:void(0)" class="btn bordered with-icon" id="'.$medicine_color.'_remove" onClick="'.$medicine_color.'_remove();"><span class="fa fa-minus-circle"></span>Remove Medicine</a>
                                    <span class="fa fa-question round-info" data-toggle="tooltip" title="Remove medicine from SHOOP Panel"></span>';
							}
							$html .='
								</div>
							</div>
						</section>';
		$array_index++;
	// For Loop Ends Here
	}
                    $html .= '</div>
                    <div class="clearfix"></div>
                    <div class="col-sm-12">
						<div class="text-right form-footer-btns">
							<button type="submit" onClick="return submitDetailsForm();" name="submit" class="btn preview">Save</button>
							<button type="button" class="btn reset">Reset</button>    
                        </div>
					</div>
                </div>
            </div>
        </form>
    </section>';     
	return $html;
}

add_shortcode( 'patient_strain', 'cdrmed_create_new_shoop' );

?>