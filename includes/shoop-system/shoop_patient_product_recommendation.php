<?php 
if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly 
global $post_id;

/*
Product Recommendation Follows categories below

Inventory Category Types:

    Topical 
    Tinctures
    Edible 
    Extract 
    Flower 
    Cartridges
    Prerolls
    Terpenes
    Capsules
    Other

Cannabis Usage Methods:

    Inhaled
    Ingested
    Sublingual
    Suppository 
    Topically


For Every SHOOP usage method these inventory categories should fall under { 

    Inhaled { Extract, Flower, Cartridges,Prerolls }
    Ingested  { Edible, Extract, Capsules }
    Sublingual  { Extract, Tincture, Terpenes }
    Suppository  { Extract, Capsule }
    Topically  { Topical }

}

*/




function cdrmed_shoop_patient_products() {
	ob_start();
	$userrole = get_user_role();
	$patient_id = isset($_GET['pid']) ? $_GET['pid'] : get_current_user_id();
	
	$patient_physicians = get_user_meta($patient_id, 'patient_physician_id', true);
	$latest_shoop_id = get_user_meta($patient_id, 'latest_patient', true);
	$auto_shoop_post_id = '';
	//patient have no parent physician then
	if($patient_physicians == 'orphan' || $patient_physicians == ''){
		//If patient have no Shoop or 
		if(!$latest_shoop_id){
			$auto_shoop_post_id = do_shortcode('[auto-shoop call_by="call-by-product-recommended-shortcode" pid="'.$patient_id.'"]');
			//if auto shoop shortcode return shoop id then
			if($auto_shoop_post_id){
				update_user_meta($patient_id, 'latest_patient', $auto_shoop_post_id);
				//meta key for show/hide shoop at patient side
				update_post_meta($auto_shoop_post_id, 'auto_shoop_type', 'hidden');
				$latest_shoop_id = $auto_shoop_post_id;
			}
		}
	}			
	//$patient_dispensary_id_arr = json_decode(get_user_meta($patient_id, 'patient_dispensary_id', true),true);
	
	// Get Patient Dispensary ID for Products
	$patient_dispensary_id_arr = json_decode(get_user_meta($patient_id, 'patient_dispenary_products_id', true),true);

	//Check if User has selected any dispensary yet or not? We will use it to show different states of Product Selection Button
	if($patient_dispensary_id_arr) {
		$id_check_arr = array();
		foreach ($patient_dispensary_id_arr as $ids) {
		$id_check_arr[] = cdrmed_get_user_role($ids);
		}
	}
		//echo "<pre>";print_r($id_check_arr);echo "</pre>";

	// get patient's latest shoop post id
	
	
	/* Below line commented by Hassan, due to SHOOP button not appearing and admin thinking that it is broken. We have conditions defined further to deal with this issue.
	Dated: 26-10-2016
	*/
	//if(!$latest_shoop_id || FALSE === get_post_status( $latest_shoop_id ) ) return; // there is no shoop for patient, get the hell out of here

	$shoop_data = get_orgnized_shoop_data($latest_shoop_id);
	
	$product_check = null;
	
	$other_dispensary_arr = array();
	
	if(sizeof($patient_dispensary_id_arr) > 0){
		
		foreach ($patient_dispensary_id_arr as $dispensary_id) {
			if(!$product_check){
				$args = array(
					'post_type' => 'product',
					'posts_per_page' => -1,
					'order' => 'DESC',
					'post_status' => 'publish',
					'author' => $dispensary_id,
				);
				$post = new WP_Query( $args );
				$product_check = (intval($post->post_count) > 0)? true : null;
			}
		}
		
		//echo "<pre>";echo $product_check;echo "</pre>";
		
		if(!$product_check){
			$args = array(
				'role' => 'dispensary',
				'orderby' => 'ID',
				'posts_per_page' => -1,
			);
			$users = new WP_User_Query($args);
			$getdispensaries = $users->get_results();
			foreach ($getdispensaries as $key => $value) {
				$other_dispensary_arr[] = $value->ID;
			}
		}	
	}
	else if($userrole == 'dispensary'){
		$current_usid = get_current_user_id();
		$current_usid = is_dispensary_physician_child($current_usid);
		$args = array(
			'post_type' => 'product',
			'posts_per_page' => -1,
			'order' => 'DESC',
			'post_status' => 'publish',
			'author' => $current_usid,
		);
		$post = new WP_Query( $args );
		$product_check = (intval($post->post_count) > 0)? true : null;
	}
	
	$terpenes_raw = array();
	$medicine_number = 0;
	$medicine_number_args = array();	
	?>
	<!--<div class="col-md-3 shoop-img">
		<h3 class="p-shoops">Select from the following MednCures's Products to Match your SHOOP. </h3>-->
		<?php 
	if($product_check){
		
		foreach ($shoop_data as $medicine_number => $medicine) {
			//echo "<pre>";print_r($medicine);echo "</pre>";
			$medicine_number++;
			$products_args_arr = array(
				'post_type' => 'product',
				'posts_per_page' => 20,
			);
			
			$product_tax = 'product_cat';
			
			// All Ingestion Methods to fetch products
			
			if( 'Inhaled' == $medicine['ingestion_method'] ){
				$products_args_arr['tax_query'][] = array( 
					'taxonomy' => $product_tax,
					'field'    => 'slug',
					'terms'     => array('extract', 'flower', 'cartridges', 'prerolls'),
					'operator' => 'IN'
				);
			}
			if( 'Ingested (edible)' == $medicine['ingestion_method'] ){
				
				$products_args_arr['tax_query'][] = array( 
					'taxonomy' => $product_tax,
					'field'    => 'slug',
					'terms'     => array('edible', 'extract', 'capsules'),
					'operator' => 'IN'
				);
			}
			if( 'Sublingual' == $medicine['ingestion_method'] ){
				
				$products_args_arr['tax_query'][] = array( 
					'taxonomy' => $product_tax,
					'field'    => 'slug',
					'terms'     => array('extract', 'tinctures', 'terpenes'),
					'operator' => 'IN'
				);
			}
			if ( 'Suppository' == $medicine['ingestion_method'] ) {
				$products_args_arr['tax_query'][] = array( 
					'taxonomy' => $product_tax,
					'field'    => 'slug',
					'terms'     => array('extract', 'capsules'),
					'operator' => 'IN'
				);
			}
			if ( 'Topically' == $medicine['ingestion_method'] ) {
				$products_args_arr['tax_query'][] = array( 
					'taxonomy' => $product_tax,
					'field'    => 'slug',
					'terms'     => array('topical'),
					'operator' => 'IN'
				);
			}
			
			$cannabinoid = $medicine['cannabinoid'];
			preg_match('#\((.*?)\)#', $cannabinoid, $test_result_mgs);
			$test_result_mg = strtolower($test_result_mgs[1]);
			
			$target_dosage = explode("X", $medicine['target_dosage']);
			$dose = intval($target_dosage[0]);
			//echo $dose;
			if($product_check){
				$products_args_arr['meta_query'][] = array(
					array(
						'key' => 'product_dispensary_id',
						'value' => $patient_dispensary_id_arr,
						'compare' => 'IN',
					),
					array(
						'key' => '_stock',
						'value' => 0,
						'compare' => '>',
					),
					array(
						'key' => '_azcl_'.$test_result_mg.'_tests',
						'value' => array($dose-50, $dose+50),
						'type' => 'numeric',
						'compare' => 'BETWEEN',
					),
				);
			}
			else{
				$products_args_arr['meta_query'][] = array(
					array(
						'key' => 'product_dispensary_id',
						'value' => $other_dispensary_arr,
						'compare' => 'IN',
					),
					array(
						'key' => '_stock',
						'value' => 0,
						'compare' => '>',
					),
					array(
						'key' => '_azcl_'.$test_result_mg.'_tests',
						'value' => array($dose-50, $dose+50),
						'type' => 'numeric',
						'compare' => 'BETWEEN',
					),
				);
				$products_args_arr['posts_per_page'] = 10;
			}
			
			//echo "<pre>";print_r($products_args_arr);echo "</pre>";
			$terpenes_raw[] = $medicine['terpene'];

			$medicine_number_args[$medicine_number] = $products_args_arr;

		} // End foreeach
		
		
		?>
		<!--</div>-->
	  
		<style type="text/css">
		.modal-backdrop.fade.in{ display: none; }
		.modal.fade.in { background: rgba(0, 0, 0, 0.61) none repeat scroll 0 0; }
		</style>
		
		<?php
		
		
		// If Shoop contains Terpenes
		$terpene_arr = array();
		if(!array_empty($terpenes_raw) && is_array($terpenes_raw)){
			foreach($terpenes_raw[0] as $terpenes){
				//Terpenes convert into Meta Keys
				$ter = str_replace('-', '_', str_replace(' ', '_', strtolower($terpenes)));
				if(!in_array($ter, $terpene_arr)){
					$terpene_arr[] = $ter;
				}
				
			}
			sort($terpene_arr);
		}
	}
	
	/* echo '<pre>';
	print_r($medicine_number_args);
	echo '</pre>';
	die(); */
	
	if(!$latest_shoop_id){
		echo '<a href="#" class="btn bordered disabled">NO SHOOP for Products</a>';
	} else {
		if( $userrole == 'dispensary'){
			echo '<a href="#" class="btn bordered" data-target="#ProductsModal" data-toggle="modal">Select Products</a>';
		} else {
			if($patient_dispensary_id_arr && in_array("dispensary", $id_check_arr)) {
				// Placed another check over here to only show select product buttons if dispensary is already selected
				echo '<center>Please choose your products by clicking the button below. You will only see products from your preferred dispensaries.<br><a href="#" class="btn bordered" data-target="#ProductsModal" data-toggle="modal">Select Products</a></center>';
			} else {
				echo '<center>Before you may select products, please choose atleast one dispensary from your profile.<br><a href="#profile" class="btn bordered" data-toggle="tab" data-toggle="modal">Set Dispensary</a></center>';
			}
		}
	}// Top Most Latest SHOOP ID Else
	
	
	?>
	
	<div class="modal fade" id="ProductsModal" role="dialog" >
		<div class="modal-dialog multi-select-modal navypurple modal-lg">
			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close fa fa-close" data-dismiss="modal"></button>
				</div>

				<?php
				
				if(!$product_check){
					?> <h3 style="padding: 20px 0; text-align: center;">No Matching Product Found!</h3> <?php
				}
				else if($medicine_number_args){
					
					$step_number = 0;
					$product_reads_check = true;
					foreach ($medicine_number_args as $medicine_numer => $medicine_args) {
						
						$number_eng = array('first', 'second', 'third', 'fourth');
                        $retVal = ($step_number > 0) ? 'Now' : 'First' ;
						
						$products = get_posts($medicine_args);
						
							
							?>
							<div <?php if($step_number > 0){ ?> style="display: none;" <?php } ?> data-step="<?php echo $step_number; ?>" class="modal-contendddt cdrmed-step step_<?php echo $step_number; ?> medicine-steps">
								<?php if($products){ ?>
								<div id="heading-text_<?php echo $step_number; ?>" class="cdrm-header">
									<h4 data-cannabinoid="<?php echo $shoop_data[$medicine_numer-1]['cannabinoid']; ?>">
									<?php echo $retVal; ?>, select a product to match your <?php echo $number_eng[$step_number] ?> target cannabinoid of <?php echo $shoop_data[$medicine_numer-1]['cannabinoid']; ?></h4>
								</div>
								<?php } 
								
								if($products){
									?>
								<div class="cdrmed_modal_slider">
								  <?php
										
										$product_reads_check = null;
										
										$next_secondry_medicine = '';
										//Array for all find products id
										$product_id_arr = array();
										// Fetched Products
										foreach ($products as $product) {
											$product_id = $product->ID;
											
											//if Shoop contains Terpenes
											if(!array_empty($terpenes_raw) && is_array($terpenes_raw)){
												//fetch meta data of current product
												$product_meta_keys = get_post_meta($product_id);
												
												foreach($terpene_arr as $terpene){
													//terpenes convert into array intex (like meta key)
													$current_terpene = "_azcl_".$terpene."_tests";
													
													if(array_key_exists($current_terpene,$product_meta_keys)){
														if($product_meta_keys[$current_terpene][0] != ''){
															if(!in_array($product_id, $product_id_arr)){
																$product_id_arr[] = $product_id;
															}
														}
													}
												}
											}
											else{
												$ingestion_method = $shoop_data[$medicine_numer-1]['ingestion_method'];
												$args = array(
													'orderby' => 'name'
												);
												$terms = wp_get_object_terms( $product_id, 'product_cat', $args);
												if(in_array($ingestion_method, $terms)){
													if(!in_array($product_id, $product_id_arr)){
														$product_id_arr[] = $product_id;
													}
												}
											}
										}
										
										if(sizeof($product_id_arr) < 1){
											foreach ($products as $product) {
												$product_id_arr[] = $product->ID;
											}
										}
										
										foreach ($product_id_arr as $product) {
										
											//$product_id = $product->ID;
											$product_id = $product;
											
											$shoop_id = $latest_shoop_id;
											$prod_name = get_the_title( $product_id );

											$product_url = get_permalink($product_id);
											$see_full_profile_link = '<a href='.$product_url.'>See Full Profile</a>';
											//print_r($shoop_data[$medicine_numer-1]);
											// get medicine target dosage
											$target_dosage_arr = explode('X', $shoop_data[$medicine_numer-1]['target_dosage']);
											$target_dosage = $target_dosage_arr[0];
											
											// Number of Doses per day.
											$number_of_doses_arr = explode('X', $shoop_data[$medicine_numer-1]['frequency']);
											$number_of_doses = $number_of_doses_arr[0];

											preg_match_all("/\(([^)]+)\)/", $shoop_data[$medicine_numer-1]['cannabinoid'], $matches);
											$primary_cannabinoid = strtolower($matches[1][0]);
											$primary_cannabinoid_percent_value = get_post_meta( $product_id, '_azcl_'.$primary_cannabinoid, true );
											$primary_cannabinoid_value = get_post_meta( $product_id, '_azcl_'.$primary_cannabinoid.'_tests', true );
											$secondary_cannabinoid = '';

											if($primary_cannabinoid == 'thc'){ 
											  $secondary_cannabinoid = 'cbd';
											}else{
											  $secondary_cannabinoid = 'thc';
											}

											/**
											 * IF THERE IS MORE THAN ONE MEDICINE BEING RECOMMENDED, THE PRIMARY CANNABINOID IN BOTH MEDICINES NEEDS TO BE THE SECONDARY CANNABINOID IN THE OTHER.
											 * For example, recommended primary cannabinoid1 is THC, and primary cannabinoid2 is THCA, then BOTH medicines would need to account for both values from lab reports.
											 */
											// if(!empty($next_secondry_medicine)){
											//   $secondary_cannabinoid = $next_secondry_medicine;
											// }

											// secondary cannabinoid value prom product
											$secondary_cannabinoid_percent_value = get_post_meta( $product_id, '_azcl_'.$secondary_cannabinoid, true );
											$secondary_cannabinoid_value = get_post_meta( $product_id, '_azcl_'.$secondary_cannabinoid.'_tests', true );
											
											$frequency_always = $shoop_data[0]['frequency'];	// Number of Doses Each with Daily or Hourly Etc
											$extracted_frequency = preg_replace("/[^0-9]/","",$frequency_always);  // This is number of doses per day from SHOOP extracted number only
											
											//echo 'This is SECOND Cannabinoid '.$secondary_cannabinoid;
											//echo '<br>This is PRIMARY Cannabinoid % value '.$primary_cannabinoid_percent_value;
											//echo '<br>This is SECOND Cannabinoid % value '.$secondary_cannabinoid_percent_value;
											//echo '<br>This is SECOND Cannabinoid value '.$secondary_cannabinoid_value;

											// v1 = Take Target Dosage Value and Divide it by 1000. GOT: 0.1
											$v1 =  $target_dosage / 1000;

											// v2 = Take Secondary Cannabinoid Percentage Value and Divide it by 100. got: 0.126
											$v2 = $primary_cannabinoid_percent_value / 100;
											
											// Now divide v1 by v2 and you will have This Much Extract Daily Value.
											$v3 = $v1 / $v2;
											
											//echo '<br>'.$v1.'+++<br>'.$v2.'=>'.$v3.'---------';
											
											$tmed = round($v3,2);
											//echo 'USE THIS MUCH EXTRACT DAILY '.round($v3,2);
											// v3(this much extract daily) should be divided by Number of Doses per day.
											$epd = $v3 / $number_of_doses;
											
											// This Much Per Dose
											$tmdpd = $tmed/$extracted_frequency;
											$dose_per_day_round = round($tmdpd,2);
											
											//var_dump($secondary_cannabinoid_value);
											/**
											 * To calculate Amount Primary Cannabinoid(mg) per day:
											 * h1 = Primary Cannabinoid % Value from Product divided by 100.
											 */
											$h1 = $primary_cannabinoid_value / 100;

											// h2 = h1 multiplied by v3(this much extract daily).
											$h2 = $h1 * $v3;

											// h3 = h2 multiplies by 1000
											$h3 = $h2 * 1000;
										
										
											/**
											 * To calculate Amount Secondary Cannabinoid(mg) per day:
											 * h1 = Primary Cannabinoid % Value from Product divided by 100.
											 */
											$hg1 = $secondary_cannabinoid_percent_value / 100;

											// h2 = h1 multiplied by v3(this much extract daily).
											$hg2 = $hg1 * $tmed;

											// h3 = h2 multiplies by 1000
											$hg3 = $hg2 * 1000;
											
											$next_secondry_medicine = $primary_cannabinoid;

											$product_json = array(
												  'p_product_title' => $prod_name,
												  'p_cannabinoid' => strtoupper($primary_cannabinoid), 
												  'p_cannabinoid_value' => $primary_cannabinoid_value,
												  'p_cannabinoid_percent_value' => $primary_cannabinoid_percent_value,
												  's_cannabinoid' => strtoupper($secondary_cannabinoid), 
												  's_cannabinoid_value' => $secondary_cannabinoid_value,
												  's_cannabinoid_percent_value' => $secondary_cannabinoid_percent_value,
												  'this_much_value' => $tmed,
												  'dose_per_day' => $dose_per_day_round,
												  'secondary_calculations' => $hg3,
												  'frequency_per_day' => $frequency_always,
												  'full_profile_link' => $see_full_profile_link,
												  'calculations' => $h3
											);
											
											$product_json = json_encode($product_json);
											?>
											<div>
												<div data-product-json='<?php echo $product_json; ?>' data-product-id="<?php echo $product_id; ?>" class="cdrmed_slide_item">
												<?php 
												  $thumb_id = get_post_thumbnail_id($product_id);
												  $thumb_url_array = wp_get_attachment_image_src($thumb_id, 'medium', true);
												  $thumb_url = $thumb_url_array[0];
												?>
													<div class="cdrm_item">
														<img src="<?php echo $thumb_url; ?>" alt="image">
														<button type="button" class="select_cdrm_item btn">Select</button>
														<h3 class="cdrm_title"><?php echo $prod_name; ?></h3>
														<a href="<?php echo get_permalink($product_id); ?>">More info</a>
													</div><!-- end .cdrm_item -->
												</div><!-- end .cdrmed_slide_item -->
											</div><!-- end div holding .cdrmed_slide_item -->
											<!--<a href="<?php //echo get_permalink($product->ID); ?>"><?php //echo $h3.' :- '.$product->post_title.' -- '.$medicine_number; ?></a>
											<hr />-->
										<?php
											
										}
										
									
									
								  ?>
								</div><!-- end .cdrmed_modal_slider -->
								
								<?php
								}
								else{
									$med_no = $step_number + 1;
									?>
									<div class="no-matching-product">
										<h3 style="padding: 20px 0; text-align: center;">No Matching Product Found for Medicine # <?php echo $med_no ?></h3>
										<h6>Your Therapy is not 100% correct so you may </h6>
										<div class="buttons">
											<button class="cancel-therapy" type="button">Cancel</button>
											<button type="button" class="select_cdrm_item btn" style="width: 200px;">Proceed to Next Step</button>
										</div>
									</div>
									<?php
								}
								$step_number++;
								?>
								
								<div class="cdrmed_slid_footer ccdrmf_slider">
									<?php 
									if($products){
										foreach ($products as $product) {
											?>
											<div>
												<div class="selected_cdrm_items">
													<?php 
													$thumb_id = get_post_thumbnail_id($product_id);
													$thumb_url_array = wp_get_attachment_image_src($thumb_id, 'large', true);
													$thumb_url = $thumb_url_array[0];
													//$thumb_url = str_replace("http://localhost/cdrmed","http://beta2.cdrmed.org/", $thumb_url);
													?>
													<img style="width: 60px;" src="<?php echo $thumb_url; ?>" alt="image">
												</div>  
											</div>
										<?php 
										}
									} ?>
								</div><!-- end .cdrmed_slid_footer -->
							</div><!-- end .modal-contendddt -->
						  <?php 
						
						
					}
					if($product_reads_check){
						?> <h3 style="padding: 20px 0; text-align: center;">No Matching Product Found!</h3> <?php
					}
					?>
						  
					<div style="display: none;" class="modal-contendddt cdrmed-step step_<?php echo $step_number++; ?> step-show-selected-products">
						<div class="cdrm-header">
							<h4>Here are your selected products. <br />Click the button below to calculate your daily dosing guide.</h4>
						</div>

						<div id="selected_products_markup" class="step_2_items"></div>
						<div class="buttons">
							<button id="calculate-daily-dosage" type="button" style="min-width: 300px;" data-id="<?php echo $patient_id; ?>">Calculate my daily dose</button>
						</div>

						<div id="selected_products_markup_thumb" class="cdrmed_slid_footer"></div>
					</div><!-- end .modal-content -->

					<div style="display: none;" class="modal-contendddt cdrmed-step step_<?php echo $step_number; ?> step-view-therapy">
						<div class="cdrm-header">
							<h4>Here's your daily dose. Click the button below to save this therapy.</h4>
							<div class="buttons">
								
								<button class="cancel-therapy" type="button">Cancel</button>
								<button data-details="<?php echo $latest_shoop_id.','.$patient_id; ?>" id="save-therapy-btn" data-product_id="" type="button">Save Therapy</button>
								<img id="save-therapy-loader" src="<?php echo CDRMED ?>/includes/assets/img/ajax-loader.png" style="width: 20px; display: none;">
							</div>
						</div>

						<div id="calucations_items" class="step_3_slider"></div><!-- end step_3slider -->

						<div id="calucations_items_thumbs" class="cdrmed_slid_footer"></div>
					</div>
					<?php 
				} ?>
				  
			</div>
		</div>
	</div>
	
	<?php
	$data_output = ob_get_contents();
	ob_end_clean();
	return $data_output;
}

add_shortcode( 'shoop-products', 'cdrmed_shoop_patient_products' );




/*
*
*	Some other Function
*
*
*/



function contains($haystack, $needle, $caseSensitive = false) {
    return $caseSensitive ? 
		(strpos($haystack, $needle) === FALSE ? FALSE : TRUE):
		(stripos($haystack, $needle) === FALSE ? FALSE : TRUE);
}

/* 
 *  checking if array empty or not
 *  return false when array is not empty
 */
  function array_empty($arr){
    if(is_array($arr)){     
        foreach($arr as $key => $value){
            if(!empty($value) || $value != NULL || $value != ""){
                return false;
                break; //stop the process we have seen that at least 1 of the array has value so its not empty
            }
        }
        return true;
    }
  }

function get_orgnized_shoop_data($shoop_id){
	$total_madicine_count = 5;
	$shoop_arr = array();
	$meta_data_shoop = get_post_meta($shoop_id);
	
	//Start Magic trick
	for($i=0; $i < $total_madicine_count; $i++) {
		if(isset($meta_data_shoop["medicine{$i}_cannabinoid_1"])){
			$cannabinoid      = $meta_data_shoop["medicine{$i}_cannabinoid_1"];
			$target_dosage    = $meta_data_shoop["medicine{$i}_target_dosage_1"];
			$frequency        = $meta_data_shoop["medicine{$i}_frequency_1"];
			
			if(array_key_exists("medicine{$i}_dosage_frequency{$i}",$meta_data_shoop)){			
				$frequency_dosage = $meta_data_shoop["medicine{$i}_dosage_frequency{$i}"];
				$dosage_term      = $meta_data_shoop["medicine{$i}_dosage_mg{$i}"];
				$dosage_timing    = $meta_data_shoop["medicine{$i}_dosage_timing{$i}"];
				$terpene          = $meta_data_shoop["medicine{$i}_terpene"];
				$ingestion_method = $meta_data_shoop["medicine{$i}_ingestion_method"];
				$notes            = $meta_data_shoop["medicine_notes{$i}"];
			}
			else{
				$frequency_dosage = array('');
				$dosage_term = array('');
				$dosage_timing = array('');
				$terpene = array('');
				$ingestion_method = array('');
				$notes = array('');
			}
			
			if(!$cannabinoid) continue;
			
			$dosage = array();
			$terpene_arr = explode(',', $terpene[0]);
			if (strpos($frequency[0], 'daily') !== FALSE){
				$dosage_count_arr = explode('X', $frequency[0]);
				$dosage_count = $dosage_count_arr[0];
				for($j=0; $j < $dosage_count; $j++) {
				  $frequency_dosage_arr = explode(',', $frequency_dosage[0]);
				  $dosage_term_arr = explode(',', $dosage_term[0]);
				  $dosage_timing_arr = explode(',', $dosage_timing[0]);
				  
				  if(array_key_exists($j,$frequency_dosage_arr)){
					$frequency_d = $frequency_dosage_arr[$j];
				  }
				  else{
					  $frequency_d = '';
				  }
				  if(array_key_exists($j,$dosage_term_arr)){
					$dosage_te = $dosage_term_arr[$j];
				  }
				  else{
					  $dosage_te = '';
				  }
				  if(array_key_exists($j,$dosage_timing_arr)){
					$dosage_tim = $dosage_timing_arr[$j];
				  }
				  else{
					  $dosage_tim = '';
				  }
				  
				  $dosage[] = array(
					'frequency'       => $frequency_d,
					'frequency_term'  => $dosage_te,
					'timing'          => $dosage_tim,
					);
				}
			}

			$shoop_arr[] = array(
				'cannabinoid'       => $cannabinoid[0],
				'target_dosage'     => $target_dosage[0],
				'frequency'         => $frequency[0],
				'dosage_frequency'  => $dosage,
				'terpene'           => $terpene_arr,
				'ingestion_method'  => $ingestion_method[0],
				'notes'             => $notes[0]
			);
		}
	}
	//Magic trick ends, now return data
	return $shoop_arr;
}