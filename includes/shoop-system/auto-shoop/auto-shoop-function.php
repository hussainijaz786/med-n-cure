<?php

//Generate AutoShoop Function

add_action( 'wp_ajax_generate_auto_shoop', 'generate_auto_shoop');
add_action( 'wp_ajax_nopriv_generate_auto_shoop', 'generate_auto_shoop');

add_shortcode('auto-shoop','generate_auto_shoop');
function generate_auto_shoop($atts) {
	
	$atts = shortcode_atts(
		array(
			'call_by' => '',
			'pid' => '',
		),
		$atts
	);
	//set when call by shortcode
	$call_by = isset($atts['call_by']) ? $atts['call_by'] : null;
	//get patient id by shortcode
	
	$pid = '';
	if($call_by){
		$pid = isset($atts['pid']) ? $atts['pid'] : null;
	}
	else{
		$pid = isset($_POST['patient_id']) ? $_POST['patient_id'] : null;
	}
	
	$intakeid = '';
	if($pid){
		$intakeid = get_user_meta($pid, 'patient_intake', true ); 	
	}
	
	$db1 = get_field( "date_of_birth", $intakeid );
	$db = date("M-d-Y", strtotime($db1));
	$age = date_diff(date_create($db), date_create('now'))->y;
	$diagnosis = get_field( "principal_primary_diagnosis", $intakeid );
	
	// Check if diagnosis and age is available before calling function to return matching rules
	if($diagnosis != '' && $age != '') {
		// Call function and get term id from the matched diagnosis
		$required_cannabinoids_arr = get_required_cannabinoids_values($diagnosis, $age);
	} else {
		if($call_by != 'call-by-product-recommended-shortcode'){
			echo json_encode(array('action' => null, 'output' => 'SHOOP Cannot be Generated Automatically, Patient Age not found'));
			die();
		}
		else{
			return null;
		}
	}
	
	$action = null;
	$output = '';

	if($required_cannabinoids_arr != '' || $required_cannabinoids_arr == true){
		
		$val = $required_cannabinoids_arr;
		$thc_ratio = $val['thc_ratio'];
		$cbd_ratio = $val['cbd_ratio'];
		$terpenes = $val['terpenes'];
		$ingestion = $val['ingestion'];
		$tddm = $val['tddm'];
		$tdda = $val['tdda'];
		$tdde = $val['tdde'];
		$cddm = $val['cddm'];
		$cdda = $val['cdda'];
		$cdde = $val['cdde'];
		//$is_muiltiple = $val['multiple_medicines'];
		$notes = $val['notes'];
		$total_cannabinoids = $val['total_cannabinoids'];
		
		// Assigning THC Ratio Value
		$r1 = (empty($thc_ratio) ? 0 : $thc_ratio);
		// Assigning CBD Ratio Value
		$r2 = (empty($cbd_ratio) ? 0 : $cbd_ratio);
		
		$dosing_rat = ($total_cannabinoids / ($r1 + $r2));
		
		$target_dosing_thc = ($thc_ratio * $dosing_rat);
		$target_dosing_cbd = ($cbd_ratio * $dosing_rat);
		
		// Check which function to call and how many times each dose is available
		if($tddm > 0 || $tdda > 0 || $tdde > 0) {
		$frequency_med1 = calculate_frequency_medicine($target_dosing_thc,$tddm,$tdda,$tdde,'Tetrahydrocannabinol (THC)',$terpenes,$ingestion,$notes);
		}
		if($cddm > 0 || $cdda > 0 || $cdde > 0) {
		$frequency_med2 = calculate_frequency_medicine($target_dosing_cbd,$cddm,$cdda,$cdde,'Cannabidiol (CBD)',$terpenes,$ingestion,$notes);
		}
		
		// Check If we have multiple or single arrays with us
		
		if(is_array($frequency_med1) && is_array($frequency_med2)){
			$sorted_arr = array_merge($frequency_med1, $frequency_med2);
		} else if(is_array($frequency_med1)) {
			$sorted_arr = 	$frequency_med1;
		} else if(is_array($frequency_med2)) {
			$sorted_arr = 	$frequency_med2;
		} else {
			echo 'Something Seems to be Wrong Here';	
		}
		
		
		/**** Sample Response
		 [0] => Array
        (
            [primary_cannabinoid] => thc
            [target_dosage] => 367
            [frequency] => 2
            [frequency_time] => daily
            [morning] => 183
            [afternoon] => 0
            [evening] => 183
            [terpenes] => Alpha-pinene
            [ingestion] => Ingested (edible)
            [notes] => Its just a test medicine
        )
		******/
		
		//echo "<pre>";print_r($sorted_arr);echo "</pre>";
		
		//return shoop id when call by shortcode
		if($call_by == 'call-by-product-recommended-shortcode'){
			
			if($pid == get_current_user_id()){
				cdrmed_save_activity_log('Patient Auto Shoop created!', '');
			}
			else{
				cdrmed_save_activity_log('Patient Auto Shoop created!', $pid);
			}
			
			return create_patient_auto_shoop($sorted_arr, $pid);
		}
		// Call Create SHOOP Function Here and Assign to the Patient via URL
		create_patient_auto_shoop($sorted_arr, $pid);
		
		$output = 'Auto Shoop created successfully!';
		$action = true;
		if($pid == get_current_user_id()){
			cdrmed_save_activity_log('Patient Auto Shoop created!', '');
		}
		else{
			cdrmed_save_activity_log('Patient Auto Shoop created!', $pid);
		}
		
		
	} else {
		if($call_by == 'call-by-product-recommended-shortcode'){
			return null;
		}
		$action = null;
		//echo "No Rules Defined for Current Diagnosis";
		$output = 'No Rules Defined for Current Diagnosis';
	}
	
	echo json_encode(array('action' => $action, 'output' => $output));
	die();

// Main Shortcode or Ajax function ends here
}


function create_patient_auto_shoop($shoop_data, $pid){
	$postid = '';
	$i = 0;
	foreach ($shoop_data as $value){
		//echo"<pre>";print_r($value);echo"</pre>";
		
		$primary_cannabinoid = $value['primary_cannabinoid'];
		$target_dose = $value['target_dosage'];
		$frequency = $value['frequency'];
		$frequency_time = $value['frequency_time'];
		$morning_dose = $value['morning'];
		$afternoon_dose = $value['afternoon'];
		$evening_dose = $value['evening'];
		$dosage_unit = $value['dosage_unit'];
		$terpenes = $value['terpenes'];
		$ingestion = $value['ingestion'];
		$notes = $value['notes'];
		
		$dailydose = (($morning_dose > 0) ? "$morning_dose," : "");
		$dailydose .=(($afternoon_dose > 0) ? "$afternoon_dose," : "");
		$dailydose .=(($evening_dose > 0) ? "$evening_dose" : "");
		
		$dunit = (($morning_dose > 0) ? "$dosage_unit," : "");
		$dunit .=(($afternoon_dose > 0) ? "$dosage_unit," : "");
		$dunit .=(($evening_dose > 0) ? "$dosage_unit" : "");
		
		$dtime = (($morning_dose > 0) ? "morning," : "");
		$dtime .=(($afternoon_dose > 0) ? "afternoon," : "");
		$dtime .=(($evening_dose > 0) ? "evening" : "");
		
		if($i == 0){
			//$user_id = 4895;
			$user_id = $pid;
			$my_post = array(
				'post_type'  => 'strain_shoops',
				'post_title' => 'Auto Shoop For Patient',
				'post_content' => $primary_cannabinoid,
				'post_status' => 'private',
				'comment_status' => 'closed',   // if you prefer
				'ping_status' => 'closed',
				'post_author'  => $user_id
			);

			// Insert the post into the database
			$postid = wp_insert_post( $my_post );
			//echo "Inserted Post ID= ".$postid;
		}
		if ($postid) {  						
			// This field sets the Cannabinoid Value ( Tertrahydrocannabinol (THC) )
			update_post_meta($postid, 'medicine'.($i+1).'_cannabinoid_1',$primary_cannabinoid);						
			// This field sets total target dosage (100 X MG)
			update_post_meta($postid,'medicine'.($i+1).'_target_dosage_1',$target_dose." X ".$dosage_unit);						
			// This field Sets the Frequency in which it is required to be taken (2 x Daily)
			update_post_meta($postid,'medicine'.($i+1).'_frequency_1' ,$frequency." X ".$frequency_time);
			// Dosage Frequency Combined (50,50) If set as 2x daily so 2 values will be in there
			update_post_meta($postid,'medicine'.($i+1).'_dosage_frequency'.($i+1),rtrim($dailydose));					
			// Dosage Combined Measring Unit (mg,mg) If set as 2x daily so 2 values will be in there
			update_post_meta($postid,'medicine'.($i+1).'_dosage_mg'.($i+1) ,rtrim($dunit));						
			// Dosage Timing in which it should be taken (morning,evening) If set as 2x daily so 2 values will be in there
			update_post_meta($postid,'medicine'.($i+1).'_dosage_timing'.($i+1) ,rtrim($dtime));						
			// Multiple Terpenes if Selected
			update_post_meta($postid, 'medicine'.($i+1).'_terpene', $terpenes);					
			// Ingestion Method (Inhaled)
			update_post_meta($postid, 'medicine'.($i+1).'_ingestion_method', $ingestion);						
			// Simple notes field
			update_post_meta($postid, 'medicine_notes'.($i+1),$notes);
			// Patient ID for which it is created
			update_post_meta($postid, 'patient_id', $user_id);						
			// Physician ID who created the SHOOP
			update_post_meta($postid, 'physician_id', get_current_user_id());
			// Patient with Shoop post ID
			update_user_meta($pid, 'latest_patient',$postid);
			// Check for Auto Shoop
			update_post_meta($postid, 'auto_shoop', 1);

		} //if post inserted
		$i++;
	}
	return $postid;
}


function calculate_frequency_medicine($ratio,$md,$ad,$ed,$type,$terpenes,$ingestion,$notes) {
	// $md, $ad, $ed represent morning dose, afternoon dose and evening dose respectively
	
	// Calculating medicine frequency by Provided Formula and Evaluating Dosing
	$fm = $md + $ad + $ed;
	$morning_dose = (($ratio/$fm)*$md);
	$afternoon_dose = (($ratio/$fm)*$ad);
	$evening_dose = (($ratio/$fm)*$ed);
	
	
	// Calculate Number of Doses by evaluating Frequency This frequency is required to add shoop in the system such as 2 x daily etc
	$frequency = ($md > 0 ? $md = 1 : $md = 0) + ($ad > 0 ? $ad = 1 : $ad = 0) + ($ed > 0 ? $ed = 1 : $ed = 0);
	
	$medicine_arr[0] = array(
		'primary_cannabinoid' => $type,
		'target_dosage' => round($ratio),
		'frequency' => $frequency,
		'frequency_time' => 'daily',
		'morning' => round($morning_dose),
		'afternoon' => round($afternoon_dose),
		'evening' => round($evening_dose),
		'morning,afternoon,evening' => round($morning_dose).','.round($afternoon_dose).','.round($evening_dose),
		'dosage_unit' => 'mg',
		'terpenes' => $terpenes,
		'ingestion' => $ingestion,
		'notes' => $notes
	);
	
	return $medicine_arr;
	
}


function get_required_cannabinoids_values($diagnosis, $patient_age) {
	
	$args = array(
		'post_type'  => 'autoshoop',
		'numberposts' => 1
	);
	$post = get_posts( $args );

	//This is the POST ID OF AUTOSHOOP Rules
	$shoop_rules = $post[0]->ID;
		
		// check if the repeater field has rows of data
	if( have_rows('dosing_ratio_settings',$shoop_rules) ):
		$row_id = 0;
		// loop through the rows of data
		while ( have_rows('dosing_ratio_settings',$shoop_rules) ) : the_row();
			// display a sub field value
			$diagnosis_value = get_sub_field('diagnosis');
			$category_value = get_sub_field('category');
			
			if($diagnosis_value == $diagnosis) {
			//We are matching diagnosis here and then we set the category value to be the term id
			$term_id = $category_value;
			break;
			} else {
			$term_id = false;	
			}
			$row_id++;
		endwhile;
	else :
	 // no rows found
	endif;
	//Check if term id is found or not
	if ($term_id == true) {
		// $term_id should must be available in order to get the processing below
		if( have_rows("age_groups", "dosing_category_{$term_id}") ): $i = 0;

			// loop through the rows of data
			while ( have_rows("age_groups", "dosing_category_{$term_id}") ) : the_row();  $i++;
				
				$min_age = get_sub_field('min_age');
				$max_age = get_sub_field('max_age');
				$total_cannab = get_sub_field('total_cannabinoidsmg');
				
				if($patient_age >= $min_age && $patient_age <= $max_age) {
					$total_cannabinoids = $total_cannab;
					break;
				}
		   endwhile;
		else :
		 // no rows found
		endif;

		$selected_row = get_field('dosing_ratio_settings',$shoop_rules);
		$get_selected_row = $selected_row[$row_id];
		$total_cannabinoids_arr = array('total_cannabinoids' => $total_cannabinoids);
		$all_values_combined = array_merge( $get_selected_row, $total_cannabinoids_arr );

		/**** This is the sample response after all processing

		Array
		(
			[diagnosis] => Hepatitis C
			[category] => 23
			[thc_ratio] => 2
			[cbd_ratio] => 1
			[terpenes] => Terpene 1
			[ingestion] => Ingested (edible)
			[tddm] => 50
			[tdda] => 0
			[tdde] => 50
			[cddm] => 50
			[cdda] => 50
			[cdde] => 0
			[multiple_medicines] => 
			[notes] => Its just a test medicine
			[total_cannabinoids] => 550
		)

		***************/
		 return $all_values_combined;
	} else {
		return false;
	}
	
}

?>