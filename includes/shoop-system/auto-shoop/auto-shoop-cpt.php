<?php

// Patient Code

function auto_shoop_system() {

	$labels = array(
		'name'                => _x( 'Auto Shoops', 'Post Type General Name', 'wordpress' ),
		'singular_name'       => _x( 'Auto Shoop', 'Post Type Singular Name', 'wordpress' ),
		'menu_name'           => __( 'Auto Shoop', 'wordpress' ),
		'name_admin_bar'      => __( 'Auto Shoop', 'wordpress' ),
	);
	$args = array(
		'label'               => __( 'Auto Shoop', 'wordpress' ),
		'description'         => __( 'Auto SHOOP Rules Management', 'wordpress' ),
		'labels'              => $labels,
		'supports'            => array( 'title', 'author', 'custom-fields', ),
		'hierarchical'        => false,
		'public'              => false,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'menu_position'       => 5,
		'menu_icon'           => 'dashicons-list-view',
		'show_in_admin_bar'   => true,
		'show_in_nav_menus'   => true,
		'show_in_rest'   => true,
	);
	register_post_type( 'autoshoop', $args );

}
add_action( 'init', 'auto_shoop_system', 0 );

// Register Custom Taxonomy
function dosing_category_custom_tax() {

	$labels = array(
		'name'                       => _x( 'Dosing Categories', 'Taxonomy General Name', 'wordpress' ),
		'singular_name'              => _x( 'Dosing Category', 'Taxonomy Singular Name', 'wordpress' ),
		'menu_name'                  => __( 'Dosing Category', 'wordpress' ),
		'all_items'                  => __( 'All Items', 'wordpress' ),
		'parent_item'                => __( 'Parent Item', 'wordpress' ),
		'parent_item_colon'          => __( 'Parent Item:', 'wordpress' ),
		'new_item_name'              => __( 'New Item Name', 'wordpress' ),
		'add_new_item'               => __( 'Add New Item', 'wordpress' ),
		'edit_item'                  => __( 'Edit Item', 'wordpress' ),
		'update_item'                => __( 'Update Item', 'wordpress' ),
		'view_item'                  => __( 'View Item', 'wordpress' ),
		'separate_items_with_commas' => __( 'Separate items with commas', 'wordpress' ),
		'add_or_remove_items'        => __( 'Add or remove items', 'wordpress' ),
		'choose_from_most_used'      => __( 'Choose from the most used', 'wordpress' ),
		'popular_items'              => __( 'Popular Items', 'wordpress' ),
		'search_items'               => __( 'Search Items', 'wordpress' ),
		'not_found'                  => __( 'Not Found', 'wordpress' ),
		'no_terms'                   => __( 'No items', 'wordpress' ),
		'items_list'                 => __( 'Items list', 'wordpress' ),
		'items_list_navigation'      => __( 'Items list navigation', 'wordpress' ),
	);
	$args = array(
		'labels'                     => $labels,
		'hierarchical'               => false,
		'public'                     => true,
		'show_ui'                    => true,
		'show_admin_column'          => true,
		'show_in_nav_menus'          => true,
		'show_tagcloud'              => false,
		'rewrite'                    => false,
	);
	register_taxonomy( 'dosing_category', array( 'autoshoop' ), $args );

}
add_action( 'init', 'dosing_category_custom_tax', 0 );

?>