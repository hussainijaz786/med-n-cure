<?php 

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly 
// Include Functions for Physician Dashboard
include('create_shoop_model_boxes.php');
include('get_print_shoops.php');
include('save_update_delete_shoop.php');
include('create_new_shoop.php');
include('shoop_side_navigation.php');
//include('shoop_patient_product_recommendation.php');
include('shoop_patient_product_recommendation_modal.php');
include('shoop_patient_product_recommendation_ajax.php');
include('import-shoop.php');
include('shoop-init.php');
include('dosing-history.php');
//Auto Shoop
include('auto-shoop/index-inc.php');
?>