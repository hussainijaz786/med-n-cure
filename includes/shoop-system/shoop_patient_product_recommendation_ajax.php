<?php 
if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly 

/*
Product Recommendation Follows categories below

Inventory Category Types:

    Topical 
    Tinctures
    Edible 
    Extract 
    Flower 
    Cartridges
    Prerolls
    Terpenes
    Capsules
    Other

Cannabis Usage Methods:

    Inhaled
    Ingested
    Sublingual
    Suppository 
    Topically


For Every SHOOP usage method these inventory categories should fall under { 

    Inhaled { Extract, Flower, Cartridges,Prerolls }
    Ingested  { Edible, Extract, Capsules }
    Sublingual  { Extract, Tincture, Terpenes }
    Suppository  { Extract, Capsule }
    Topically  { Topical }

}

*/

add_action( 'wp_ajax_shoop_medicines_ajax', 'shoop_medicines_ajax');
add_action( 'wp_ajax_nopriv_shoop_medicines_ajax', 'shoop_medicines_ajax');

function shoop_medicines_ajax() {
}

add_action( 'wp_ajax_shoop_product_fetch_ajax', 'shoop_product_fetch_ajax');
add_action( 'wp_ajax_nopriv_shoop_product_fetch_ajax', 'shoop_product_fetch_ajax');

function shoop_product_fetch_ajax() {
	
	$userrole = get_user_role();
	$current_usid = get_current_user_id();
	$patient_id = isset($_POST['patient_id']) ? $_POST['patient_id'] : get_current_user_id();
	$step = $_POST['step'];
	$existing_selected_products = isset($_POST['existing_selected_products']) ? $_POST['existing_selected_products'] : null;
	$shoop_medicine = $_POST['shoop_medicine'];
	
	//get patient lastest shoop id
	$latest_shoop_id = get_user_meta($patient_id, 'latest_patient', true);
	
	//if therapy id not found due to any reason then move in else
	if($latest_shoop_id){
		
		//get shoop data by shoop id
		$shoop_data = get_orgnized_single_shoop_data($latest_shoop_id, $step);
		
		/*
		
		Array
        (
            [cannabinoid] => Tetrahydrocannabinol (THC)
            [target_dosage] => 200 X mg
            [frequency] => 2 X daily
            [dosage_frequency] => Array
                (
                    [0] => Array
                        (
                            [frequency] => 200
                            [frequency_term] => 
                            [timing] => morning
                        )

                    [1] => Array
                        (
                            [frequency] => 200
                            [frequency_term] => 
                            [timing] => morning
                        )

                )

            [terpene] => Array
                (
                    [0] => Alpha-humulene
                    [1] => Alpha-pinene
                    [2] => Beta-caryophyllene
                    [3] => Beta-pinene
                    [4] => Bisabolol
                    [5] => Caryophyllene Oxide
                )

            [ingestion_method] => Sublingual
            [notes] => medicine 1
        )
		
		*/
		
		//get products
		/*
		*	Parameters for fetch_shoop_products_args()
		*	patient ID
		*	Step No
		*	Shoop Data of current Step ( current seelcted product/medicine)
		*	Dispensary IDs for Product
		*	Minimum Matching Dose
		*	Maximum Matching Dose
		*/
		
		$dispensary_id = get_user_meta($patient_id, 'patient_dispenary_products_id', true);
		
		$product_check = null;
		
		// Get Patient Dispensary ID for Products
		if($dispensary_id){
			$dispensary_id_arr = json_decode($dispensary_id, true);
			$dose_min = 50;
			$dose_max = 50;
			$products_data = json_decode(fetch_shoop_products_args( $patient_id, $step, $shoop_data, $dispensary_id_arr, $dose_min, $dose_max, $existing_selected_products), true);
			// if product not found then increase dosing value
			if(intval($products_data['total']) == 0){
				$dose_max = 100;
				$products_data = json_decode(fetch_shoop_products_args( $patient_id, $step, $shoop_data, $dispensary_id_arr, $dose_min, $dose_max, $existing_selected_products), true);
			}
			if(intval($products_data['total']) > 0){
				$product_check = true;
				$products = $products_data['data'];
			}
		}
		
		$other_dispensary_products = '';
		
		if(!$product_check){
			//get all dispensary ids
			$dispensary_id_arr = array();
			$args = array(
				'role' => 'dispensary',
				'orderby' => 'ID',
				'posts_per_page' => -1,
			);
			$users = new WP_User_Query($args);
			$getdispensaries = $users->get_results();
			foreach ($getdispensaries as $key => $value) {
				$dispensary_id_arr[] = $value->ID;
			}
			
			$dose_min = 50;
			$dose_max = 50;
			$products_data = json_decode(fetch_shoop_products_args( $patient_id, $step, $shoop_data, $dispensary_id_arr, $dose_min, $dose_max, $existing_selected_products), true);
			// if product not found then increase dosing value
			if(intval($products_data['total']) == 0){
				
				$dose_max = 100;
				$products_data = json_decode(fetch_shoop_products_args( $patient_id, $step, $shoop_data, $dispensary_id_arr, $dose_min, $dose_max, $existing_selected_products), true);
			}
			if(intval($products_data['total']) > 0){
				$product_check = true;
				$products = $products_data['data'];
				//show cancel button if products from other dispensary
				$other_dispensary_products = '<div class="buttons" style="margin-top: 30px;"><p>There were no matching products in selected Dispensary according to your Therapy.<br>Above listed products are being shown from other Dispensaries in CDRMED. </p><br><button class="cancel-therapy" type="button">Cancel Therapy</button></div>';
			}
		}
		
		//if product found
		if($product_check){
			$products = $products_data['data'];
		}
		else if($shoop_medicine == 1){
			$products = '<h3 style="padding: 20px 0; text-align: center;">No Matching Product Found for your Therapy. Please contact to your Physician/Dispensary</h3>';
		}
		else{
			$products = '<div class="no-matching-product">
				<h3 style="padding: 20px 0; text-align: center;">No Matching Product Found for Medicine # '.$step.'</h3>
				<h6>Your Therapy might not be correct so you may </h6>
				<div class="buttons">
					<button class="cancel-therapy" type="button">Cancel</button>
					<button type="button" class="select_cdrm_item btn" style="width: 200px;">Proceed to Next Step</button>
				</div>
			</div>';
		}
		
		$retVal = (intval($step)-1 > 0) ? 'Now' : 'First' ;
		$number_eng = array('first', 'second', 'third', 'fourth');
		$heading = $retVal.' select a product to match your '.$number_eng[intval($step)-1].' target cannabinoid of '.$shoop_data[0]['cannabinoid'];
		$return_arr = array('action' => $product_check, 'cannabinoid' => $shoop_data[0]['cannabinoid'],'heading' => $heading, 'products' => $products, 'other' => $other_dispensary_products);
	}
	else{
		$return_arr = array('action' => $product_check, 'cannabinoid' => '','heading' => '', 'products' => '<h3 class="no-therapy">No Valid Therapy Found. Please contact to your Physician/Dispensary</h3>', 'other' => '');
	}
	echo json_encode($return_arr);
	die();
}

function fetch_shoop_products_args($patient_id, $step, $shoop_data, $dispensary_id_arr, $dose_min, $dose_max, $existing_selected_products){
	
	$products = array();
	$read_count = 0;
	//if user have already select minimum one product
	if($existing_selected_products){
		$alread_selected_products = $existing_selected_products;
	}
	else{
		//if step one
		$alread_selected_products = array();
	}
	
	if(sizeof($dispensary_id_arr) > 0){
		
		$terpenes_raw = array();
		
		// loop for check patient selected dispensaries have products or not
		foreach ($dispensary_id_arr as $dispensary_id) {
			
			$args = array(
				'post_type' => 'product',
				'posts_per_page' => 12,
				'order' => 'DESC',
				'post_status' => 'publish',
				'author' => $dispensary_id,
				'post__not_in' => $alread_selected_products,
			);
			
			$product_tax = 'product_cat';
			//run shoop data for product meta query
			//add product terms againt shoop medicine
			foreach ($shoop_data as $medicine){
				
				if( 'Inhaled' == $medicine['ingestion_method'] ){
					$args['tax_query'][] = array( 
						'taxonomy' => $product_tax,
						'field'    => 'slug',
						'terms'     => array('extract', 'flower', 'cartridges', 'prerolls'),
						'operator' => 'IN'
					);
				}
				if( 'Ingested (edible)' == $medicine['ingestion_method'] ){
					$args['tax_query'][] = array(
						'taxonomy' => $product_tax,
						'field'    => 'slug',
						'terms'     => array('edible', 'extract', 'capsules'),
						'operator' => 'IN'
					);
				}
				if( 'Sublingual' == $medicine['ingestion_method'] ){
					$args['tax_query'][] = array( 
						'taxonomy' => $product_tax,
						'field'    => 'slug',
						'terms'     => array('extract', 'tinctures', 'terpenes'),
						'operator' => 'IN'
					);
				}
				if ( 'Suppository' == $medicine['ingestion_method'] ) {
					$args['tax_query'][] = array( 
						'taxonomy' => $product_tax,
						'field'    => 'slug',
						'terms'     => array('extract', 'capsules'),
						'operator' => 'IN'
					);
				}
				if ( 'Topically' == $medicine['ingestion_method'] ) {
					$args['tax_query'][] = array( 
						'taxonomy' => $product_tax,
						'field'    => 'slug',
						'terms'     => array('topical'),
						'operator' => 'IN'
					);
				}
				
				$cannabinoid = $medicine['cannabinoid'];
				preg_match('#\((.*?)\)#', $cannabinoid, $test_result_mgs);
				$test_result_mg = strtolower($test_result_mgs[1]);
				
				$target_dosage = explode("X", $medicine['target_dosage']);
				$dose = intval($target_dosage[0]);
				//meta keys for stock and lab results
				$args['meta_query'][] = array(
					array(
						'key' => '_stock',
						'value' => 0,
						'compare' => '>',
					),
					array(
						'key' => '_azcl_'.$test_result_mg.'_tests',
						'value' => array($dose - $dose_min, $dose + $dose_max),
						'type' => 'numeric',
						'compare' => 'BETWEEN',
					),
				);
				
				$terpenes_raw[] = $medicine['terpene'];
				
			}
			
			$post = new WP_Query( $args );
			
			if ($post->have_posts()) :
				while ( $post->have_posts() ) : $post->the_post();
					$read_count++;
					$product_id = get_the_ID();
					
					$prod_name = get_the_title( $product_id );

					$product_url = get_permalink($product_id);
					$see_full_profile_link = '<a href='.$product_url.'>See Full Profile</a>';
					
					// get medicine target dosage
					$target_dosage_arr = explode('X', $shoop_data[0]['target_dosage']);
					$target_dosage = $target_dosage_arr[0];
					
					// Number of Doses per day.
					$number_of_doses_arr = explode('X', $shoop_data[0]['frequency']);
					$number_of_doses = $number_of_doses_arr[0];
					
					preg_match_all("/\(([^)]+)\)/", $shoop_data[0]['cannabinoid'], $matches);
					$primary_cannabinoid = strtolower($matches[1][0]);
					//echo '<br>proo => '.$primary_cannabinoid;
					$primary_cannabinoid_percent_value = get_post_meta( $product_id, '_azcl_'.$primary_cannabinoid, true );
					$primary_cannabinoid_value = get_post_meta( $product_id, '_azcl_'.$primary_cannabinoid.'_tests', true );
					$secondary_cannabinoid = '';

					if($primary_cannabinoid == 'thc'){ 
						$secondary_cannabinoid = 'cbd';
					}else{
						$secondary_cannabinoid = 'thc';
					}

					/**
					 * IF THERE IS MORE THAN ONE MEDICINE BEING RECOMMENDED, THE PRIMARY CANNABINOID IN BOTH MEDICINES NEEDS TO BE THE SECONDARY CANNABINOID IN THE OTHER.
					 * For example, recommended primary cannabinoid1 is THC, and primary cannabinoid2 is THCA, then BOTH medicines would need to account for both values from lab reports.
					 */
					// if(!empty($next_secondry_medicine)){
					//   $secondary_cannabinoid = $next_secondry_medicine;
					// }

					// secondary cannabinoid value prom product
					$secondary_cannabinoid_percent_value = get_post_meta( $product_id, '_azcl_'.$secondary_cannabinoid, true );
					$secondary_cannabinoid_value = get_post_meta( $product_id, '_azcl_'.$secondary_cannabinoid.'_tests', true );
					
					$frequency_always = $shoop_data[0]['frequency'];	// Number of Doses Each with Daily or Hourly Etc
					$extracted_frequency = preg_replace("/[^0-9]/","",$frequency_always);  // This is number of doses per day from SHOOP extracted number only
					
					//echo 'This is SECOND Cannabinoid '.$secondary_cannabinoid;
					//echo '<br>This is PRIMARY Cannabinoid % value '.$primary_cannabinoid_percent_value;
					//echo '<br>This is SECOND Cannabinoid % value '.$secondary_cannabinoid_percent_value;
					//echo '<br>This is SECOND Cannabinoid value '.$secondary_cannabinoid_value;

					// v1 = Take Target Dosage Value and Divide it by 1000. GOT: 0.1
					$v1 =  $target_dosage / 1000;

					// v2 = Take Secondary Cannabinoid Percentage Value and Divide it by 100. got: 0.126
					$v2 = $primary_cannabinoid_percent_value / 100;
					
					// Now divide v1 by v2 and you will have This Much Extract Daily Value.
					$v3 = $v1 / $v2;
					
					//echo '<br>'.$v1.'+++<br>'.$v2.'=>'.$v3.'---------';
					
					$tmed = round($v3,2);
					//echo 'USE THIS MUCH EXTRACT DAILY '.round($v3,2);
					// v3(this much extract daily) should be divided by Number of Doses per day.
					$epd = $v3 / $number_of_doses;
					
					// This Much Per Dose
					$tmdpd = $tmed/$extracted_frequency;
					$dose_per_day_round = round($tmdpd,2);
					
					//var_dump($secondary_cannabinoid_value);
					/**
					 * To calculate Amount Primary Cannabinoid(mg) per day:
					 * h1 = Primary Cannabinoid % Value from Product divided by 100.
					 */
					$h1 = $primary_cannabinoid_value / 100;

					// h2 = h1 multiplied by v3(this much extract daily).
					$h2 = $h1 * $v3;

					// h3 = h2 multiplies by 1000
					$h3 = $h2 * 1000;
				
				
					/**
					 * To calculate Amount Secondary Cannabinoid(mg) per day:
					 * h1 = Primary Cannabinoid % Value from Product divided by 100.
					 */
					$hg1 = $secondary_cannabinoid_percent_value / 100;

					// h2 = h1 multiplied by v3(this much extract daily).
					$hg2 = $hg1 * $tmed;

					// h3 = h2 multiplies by 1000
					$hg3 = $hg2 * 1000;
					
					//$next_secondry_medicine = $primary_cannabinoid;

					/* $product_json = array(
						  'p_product_title' => $prod_name,
						  'p_cannabinoid' => strtoupper($primary_cannabinoid), 
						  'p_cannabinoid_value' => $primary_cannabinoid_value,
						  'p_cannabinoid_percent_value' => $primary_cannabinoid_percent_value,
						  's_cannabinoid' => strtoupper($secondary_cannabinoid), 
						  's_cannabinoid_value' => $secondary_cannabinoid_value,
						  's_cannabinoid_percent_value' => $secondary_cannabinoid_percent_value,
						  'this_much_value' => $tmed,
						  'dose_per_day' => $dose_per_day_round,
						  'secondary_calculations' => $hg3,
						  'frequency_per_day' => $frequency_always,
						  'full_profile_link' => $see_full_profile_link,
						  'calculations' => $h3
					);
					 
					$product_json = json_encode($product_json); */
					
					$thumb_id = get_post_thumbnail_id($product_id);
					$thumb_url_array = wp_get_attachment_image_src($thumb_id, 'medium', true);
					$thumb_url = $thumb_url_array[0];
					
					
					$products[] = '<div data-product-json="" class="cdrmed_slide_item pro'.$product_id.'" data-product-id="'.$product_id.'">
							<div class="cdrm_item">
								<img src="'.$thumb_url.'" alt="image">
								<button type="button" class="select_cdrm_item btn" data-id="'.$product_id.'">Select</button>
								<h3 class="cdrm_title">'.$prod_name.'</h3>
								<a href="'.get_permalink($product_id).'">More info</a>
							</div>
							
							<input type="hidden" class="p_product_title" value="'.$prod_name.'">
							<input type="hidden" class="p_cannabinoid" value="'.strtoupper($primary_cannabinoid).'">
							<input type="hidden" class="p_cannabinoid_value" value="'.$primary_cannabinoid_value.'">
							<input type="hidden" class="p_cannabinoid_percent_value" value="'.$primary_cannabinoid_percent_value.'">
							<input type="hidden" class="s_cannabinoid" value="'.strtoupper($secondary_cannabinoid).'">
							<input type="hidden" class="s_cannabinoid_value" value="'.$secondary_cannabinoid_value.'">
							<input type="hidden" class="s_cannabinoid_percent_value" value="'.$secondary_cannabinoid_percent_value.'">
							<input type="hidden" class="this_much_value" value="'.round($tmed,2).'">
							<input type="hidden" class="dose_per_day" value="'.round($dose_per_day_round, 2).'">
							<input type="hidden" class="secondary_calculations" value="'.$hg3.'">
							<input type="hidden" class="frequency_per_day" value="'.$frequency_always.'">
							<input type="hidden" class="full_profile_link" value="'.$see_full_profile_link.'">
							<input type="hidden" class="calculations" value="'.$h3.'">
							
						
						
					</div>';
					 
				endwhile;
			endif;
			
		}
	}
	
	return json_encode(array('total' => $read_count, 'data' => $products));
}




function get_orgnized_single_shoop_data($shoop_id, $step){
	
	$shoop_arr = array();
	$cannabinoid = get_post_meta($shoop_id, "medicine{$step}_cannabinoid_1", true);
	//Start Magic trick
		if($cannabinoid){
			
			$target_dosage    = get_post_meta($shoop_id, "medicine{$step}_target_dosage_1", true);
			$frequency        = get_post_meta($shoop_id, "medicine{$step}_frequency_1", true);
			
			$frequency_dosage = '';
			$dosage_term = '';
			$dosage_timing = '';
			$terpene = '';
			$ingestion_method = '';
			$notes = '';
			
			if($frequency){
				$frequency_dosage = get_post_meta($shoop_id, "medicine{$step}_dosage_frequency{$step}", true);
				$dosage_term      = get_post_meta($shoop_id, "medicine{$step}_dosage_mg{$step}", true);
				$dosage_timing    = get_post_meta($shoop_id, "medicine{$step}_dosage_timing{$step}", true);
				$terpene          = get_post_meta($shoop_id, "medicine{$step}_terpene", true);
				$ingestion_method = get_post_meta($shoop_id, "medicine{$step}_ingestion_method", true);
				$notes            = get_post_meta($shoop_id, "medicine_notes{$step}", true);
			}
			
			$dosage = array();
			$terpene_arr = explode(',', $terpene);
			if (strpos($frequency, 'daily') !== FALSE){
				$dosage_count_arr = explode('X', $frequency);
				$dosage_count = $dosage_count_arr[0];
				for($j=0; $j < $dosage_count; $j++) {
				  $frequency_dosage_arr = explode(',', $frequency_dosage);
				  $dosage_term_arr = explode(',', $dosage_term);
				  $dosage_timing_arr = explode(',', $dosage_timing);
				  
				  if(array_key_exists($j,$frequency_dosage_arr)){
					$frequency_d = $frequency_dosage_arr[$j];
				  }
				  else{
					  $frequency_d = '';
				  }
				  if(array_key_exists($j,$dosage_term_arr)){
					$dosage_te = $dosage_term_arr[$j];
				  }
				  else{
					  $dosage_te = '';
				  }
				  if(array_key_exists($j,$dosage_timing_arr)){
					$dosage_tim = $dosage_timing_arr[$j];
				  }
				  else{
					  $dosage_tim = '';
				  }
				  
				  $dosage[] = array(
					'frequency'       => $frequency_d,
					'frequency_term'  => $dosage_te,
					'timing'          => $dosage_tim,
					);
				}
			}

			$shoop_arr[] = array(
				'cannabinoid'       => $cannabinoid,
				'target_dosage'     => $target_dosage,
				'frequency'         => $frequency,
				'dosage_frequency'  => $dosage,
				'terpene'           => $terpene_arr,
				'ingestion_method'  => $ingestion_method,
				'notes'             => $notes
			);
		}
	
	//Magic trick ends, now return data
	return $shoop_arr;
}