<?php
if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly 

/**
* This function gets raw data of a post (strain_shoop) and return well orgnized array.
* @param  (int) $shoop_id post id
* @return (array) orgnized data
* @author Maavuz Saif <maavuz.geniusent@gmail.com>
*/

add_action( 'wp_ajax_send_shoop_to_patient_ajax', 'send_shoop_to_patient_ajax');
add_action( 'wp_ajax_nopriv_send_shoop_to_patient_ajax', 'send_shoop_to_patient_ajax');

function send_shoop_to_patient_ajax() {
	
	global $wpdb;
	
	$call_by = ($_POST['call_by'] != '') ? $_POST['call_by'] : null;
	$post_id = $_POST['shoop_id'];
	$patient_id = $_POST['patient_id'];
	
	$post = get_post( $post_id );
	$new_post_author = $patient_id;
	
	$old_shoop = get_user_meta($patient_id, 'latest_patient', true);
	
	if (isset( $post ) && $post != null && $call_by == 'import-shoop' && $old_shoop != $post_id){
		$args = array(
			'comment_status' => $post->comment_status,
			'ping_status'    => $post->ping_status,
			'post_author'    => $new_post_author,
			'post_content'   => $post->post_content,
			'post_excerpt'   => $post->post_excerpt,
			'post_name'      => $post->post_name,
			'post_parent'    => $post->post_parent,
			'post_password'  => $post->post_password,
			'post_status'    => 'private',
			'post_title'     => $post->post_title,
			'post_type'      => $post->post_type,
			'to_ping'        => $post->to_ping,
			'menu_order'     => $post->menu_order
		);
		
		$new_post_id = wp_insert_post( $args );
		
		$post_meta_infos = $wpdb->get_results("SELECT meta_key, meta_value FROM $wpdb->postmeta WHERE post_id=$post_id");
		if (count($post_meta_infos)!=0) {
			$sql_query = "INSERT INTO $wpdb->postmeta (post_id, meta_key, meta_value) ";
			foreach ($post_meta_infos as $meta_info) {
				$meta_key = $meta_info->meta_key;
				$meta_value = addslashes($meta_info->meta_value);
				$sql_query_sel[]= "SELECT $new_post_id, '$meta_key', '$meta_value'";
			}
			$sql_query.= implode(" UNION ALL ", $sql_query_sel);
			$wpdb->query($sql_query);
		}
		
		$post_id = $new_post_id;
	}
	
	update_user_meta($patient_id, 'latest_patient', $post_id);
	update_user_meta($patient_id, 'shoop_notification', 1);
	
	$patient_info = get_userdata( $patient_id );
		
	$args = array(
		'call_by' => 'shoop-alert-patient',
		'receiver' => $patient_info->user_email, 
		'subject_name' => 'New Therapy has been created for you...', 
		'body_name' => '', 
		'body_part' => '', 
		'password_reset_url' => ''
	);		
	cdrmed_send_email($args);
	
	cdrmed_save_activity_log('Shoop Sent to Patient', $patient_id);
	
	echo 'Therapy Created Successfully and Sent to Patient.';
	die();
}


add_action( 'wp_ajax_import_shoop_ajax', 'import_shoop_ajax');
add_action( 'wp_ajax_nopriv_import_shoop_ajax', 'import_shoop_ajax');

function import_shoop_ajax() {
	
	$userrole = get_user_role();
	
	$limit = isset($_POST['limit']) ? $_POST['limit'] : 2;
	$offset = isset($_POST['offset']) ? $_POST['offset'] : 0;
	
	$patient_id = $_POST['patient_id'];
	$intakeid = get_user_meta($patient_id, 'patient_intake', true ); 
	$db1 = get_field( "date_of_birth", $intakeid );
	
	$db = date("M-d-Y", strtotime($db1));
	$age = date_diff(date_create($db), date_create('now'))->y;
	$diagnosis = get_field( "principal_primary_diagnosis", $intakeid );
	
	$args1 = array(
		'post_type' => 'intake',
		'meta_query' => array(
			'relation' => 'AND',
			array(
				'key' => 'patient_calculated_age',
				'value' => array($age-5, $age+5),
				'compare' => 'BETWEEN'
			),
			array(
				'key' => 'principal_primary_diagnosis',
				'value' => $diagnosis,
				'compare' => '='
			),
		),
	);
	
	$html = '';
	
	$loop_repeater_flag = null;
	
	$intake_posts_array = get_posts( $args1 );
	foreach ( $intake_posts_array as $post1 ) : setup_postdata( $post1 );
	
		
		
		/*----------------------------------------------------------------*/
		
		$intake_post_id = $post1->ID;
		
		$args = array(
			'posts_per_page'   => $limit,
			'offset'           => $offset,
			'category'         => '',
			'category_name'    => '',
			'orderby'          => 'ID',
			'order'            => 'DESC',
			'include'          => '',
			'exclude'          => '',
			'meta_key'         => '',
			'meta_value'       => '',
			'post_type'        => 'strain_shoops',
			'post_mime_type'   => '',
			'post_parent'      => '',
			'author'           => $post1->post_author,
			'post_status'      => 'private',
			'suppress_filters' => true 
		);
		
		$posts_array = get_posts( $args );
		
		if(count($posts_array) > 0){
			$loop_repeater_flag = true;
		}
		
		foreach ( $posts_array as $post ) : setup_postdata( $post );
		
			$jk = 0;
			$data = '';
			$post_id = $post->ID;
			$pid = get_post_meta( $post_id, 'patient_id', true );
			
			if($patient_id != $pid){
				$data = cdrmed_get_print_shoop($post_id,$patient_id,$userrole,'import-shoop');       
			}
			
			if($jk > 0){
				$deprecated = '';
				if($userrole != 'dispensary'){
					$data .=' <section>
						<div class="text-center form-footer-btns">
							<button type="button" class="btn preview import_shoop_send_to_patient"  data-patient_id="'.$patient_id.'" data-shoop_id="'.$post_id.'" data-click_by="import_shoop" data-reload="true">SEND TO PATIENT</button>
						</div>
					</section>';
				}

			}
			
			$html .= $data;
		
		endforeach;
		
		
		
		/*----------------------------------------------------------------*/
		
	endforeach;
	
	if(!$loop_repeater_flag && $limit > $offset){
		$html = '<div class="feedback error-message" style="display: inherit;">No Shoop Found!</div>';
	}
	else{
		cdrmed_save_activity_log('Patient Shoop Imported!', $patient_id);
	}
	
	//$html = ($response) ? $html : 'No Shoop Found!';
	$output = array('action' => $loop_repeater_flag, 'output' => $html);
	echo json_encode($output);
	die();
}

