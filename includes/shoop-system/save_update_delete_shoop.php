<?php
if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly 


add_action( 'wp_ajax_remove_patient_shoop_ajax', 'remove_patient_shoop_ajax');
add_action( 'wp_ajax_nopriv_remove_patient_shoop_ajax', 'remove_patient_shoop_ajax');

function remove_patient_shoop_ajax() {
	$shoop_id = $_POST['shoop_id'];
	$patient_id = $_POST['patient_id'];
	wp_delete_post($shoop_id, true);
	
	update_user_meta($patient_id, 'latest_patient', '');
	
	update_user_meta($patient_id, 'shoop_notification', 1);
	
	$therapy_id = get_user_meta($patient_id, '_cdrmed_therapy_id', true);
	if($therapy_id){
		wp_delete_post($therapy_id, true);
		update_user_meta($patient_id, '_cdrmed_therapy_id', '');
		
		/* $args = array(
			'post_type' => 'shop_order',
			'posts_per_page' => -1,
			'order' => 'DESC',
			'paged' => $paged,
			'post_status' => array('wc-processing','wc-completed', 'wc-on-hold', 'wc-cancelled'),
			'meta_query' => array(
				'relation' => 'AND',
				array(
					'key' => '_cdrmed_therapy_id',
					'value' => $therapy_id,
					'compare' => '='
				),
			),
		);
		$post = new WP_Query( $args );
		if($post){
			while ( $post->have_posts() ) : $post->the_post();
				$order_id = get_the_ID();
				wp_delete_post($product_id, true);
				$dispensary_order_id = get_post_meta($order_id, 'dispensary_order_id', true);
				update_user_meta($dispensary_order_id, 'order_notification', 1);
				$patient_id = get_post_meta($order_id, '_customer_user', true);
				update_user_meta($patient_id, 'order_notification', 1);
			endwhile;
		} */
	}
	
	echo 'Shoop removed successfully!';
	die();
}


add_action('wp_ajax_cdrmed_save_update_shoop','cdrmed_save_update_shoop');
add_action('wp_ajax_npriv_cdrmed_save_update_shoop','cdrmed_save_update_shoop');

// Function to be Used for Posting and Saving of Patient's SHOOP Data and Also for Editing for SHOOP Data
function cdrmed_save_update_shoop(){
	
	$pid = isset($_GET['pid']) ? $_GET['pid'] : null;
	$postid = isset($_GET['post_id']) ? $_GET['post_id'] : null;
	$page_type = isset($_GET['page_type']) ? $_GET['page_type'] : null;
	
	$user_id = '';
	if(isset($_GET['pid'])){
		$user_id = $_GET['pid'];
		cdrmed_save_activity_log('Patient Shoop Updated!', $user_id);
	}else{
		$user_id = get_current_user_id();
		cdrmed_save_activity_log('Patient Shoop Updated!', '');
	}
	
		$i = 0;
		foreach ($_POST['primary_dosage'] as  $row) {
			
			if($row  != 'Select Target Cannabinoid...'  && $_POST['daily_dosage'][$i] != ''){
				
				if($i == 0){
					if(empty($postid) || $page_type == 'create'){
						
						$my_post = array(
							'post_type'  => 'strain_shoops',
							'post_title' => 'Patient SHOOP Dosage and Values',
							'post_content' => $row,
							'post_status' => 'private',
							'comment_status' => 'closed',
							'ping_status' => 'closed',
							'post_author'  => $user_id
						);
						if(isset($_POST['cdrmed_shoop_date']) && !empty($_POST['cdrmed_shoop_date'])){
							$date_d = strtotime($_POST['cdrmed_shoop_date']);
							$post_date = date('Y-m-d H:i:s', $date_d);
							$my_post['post_date'] = $post_date;
						}

						// Insert the post into the database
						$postid = wp_insert_post( $my_post );
					}
				}
				if ($postid) {
					
					// This field sets the Cannabinoid Value ( Tertrahydrocannabinol (THC) )
					update_post_meta($postid, 'medicine'.($i+1).'_cannabinoid_1',$row);
					// This field sets total target dosage (100 X MG)
					update_post_meta($postid,'medicine'.($i+1).'_target_dosage_1',$_POST['daily_dosage'][$i]." X ".$_POST['daily_dosage_mg'][$i]);
					// This field Sets the Frequency in which it is required to be taken (2 x Daily)
					update_post_meta($postid,'medicine'.($i+1).'_frequency_1' ,$_POST['frequency'][$i]." X ".$_POST['primary_daily_frequency'][$i]);
					
					if($_POST['primary_daily_frequency'][$i] == 'daily'){
						// Dosage Frequency Combined (50,50) If set as 2x daily so 2 values will be in there
						update_post_meta($postid,'medicine'.($i+1).'_dosage_frequency'.($i+1)  ,@implode(",", $_POST['daily_dosage_frequncy'.($i+1)]));
						
						// Dosage Combined Measring Unit (mg,mg) If set as 2x daily so 2 values will be in there
						update_post_meta($postid,'medicine'.($i+1).'_dosage_mg'.($i+1) ,@implode(",",$_POST['frequency_daily_dosage'.($i+1)]));
						
						// Dosage Timing in which it should be taken (morning,evening) If set as 2x daily so 2 values will be in there
						update_post_meta($postid,'medicine'.($i+1).'_dosage_timing'.($i+1) ,@implode(",",$_POST['primary_dosage_timing'.($i+1)]));
					}
					
					// Multiple Terpenes if Selected
					update_post_meta($postid, 'medicine'.($i+1).'_terpene',  $_POST['terpene'][$i]);
					// Ingestion Method (Inhaled)
					update_post_meta($postid, 'medicine'.($i+1).'_ingestion_method', $_POST['ingestion_method'][$i]);
					// Simple notes field
					update_post_meta($postid, 'medicine_notes'.($i+1), $_POST['notes'][$i]);
					// Patient ID for which it is created
					update_post_meta($postid, 'patient_id', $_POST['patient_id']);
					// Physician ID who created the SHOOP
					//update_user_meta($user_id, 'latest_patient', $postid);
					//with old created by
					//update_post_meta($postid, 'physician_id', get_current_user_id());
					//with new created by
					update_post_meta($postid, 'created_by', get_current_user_id());
				
				} //if post inserted
			} // else condition ends here
			$i++;
		}
		
		update_user_meta($user_id, '_cdrmed_therapy_id', '');
		
		
		// This will be the URL Where we will redirect and Display the SHOOP Preview to the Dispensary or Physician
		$url = site_url();
		wp_redirect($url.'/preview-strain/?post_id='.$postid.'&pid='.$user_id);
		
		exit;
}
?>