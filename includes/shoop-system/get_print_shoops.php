<?php
if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly 


add_action('wp_ajax_read_medicine_dosage_frequency_ajax','read_medicine_dosage_frequency_ajax');
add_action('wp_ajax_npriv_read_medicine_dosage_frequency_ajax','read_medicine_dosage_frequency_ajax');

// Function to be Used for Posting and Saving of Patient's SHOOP Data and Also for Editing for SHOOP Data
function read_medicine_dosage_frequency_ajax(){
	
	$post_id = $_POST['post_id'];
	$medicine_no = ($_POST['medicine_no'] == '') ? 1 : $_POST['medicine_no'];
	

	$medicine1_dosage_frequency = explode(",",get_post_meta( $post_id, 'medicine'.($medicine_no).'_dosage_frequency'.($medicine_no), true ));
	$medicine1_dosage_mg = explode(",",get_post_meta( $post_id, 'medicine'.($medicine_no).'_dosage_mg'.($medicine_no), true ));
	$medicine1_dosage_timing = explode(",",get_post_meta( $post_id, 'medicine'.($medicine_no).'_dosage_timing'.($medicine_no), true ));
	
	$arr = array('medicine1_dosage_frequency' => $medicine1_dosage_frequency, 'medicine1_dosage_mg' => $medicine1_dosage_mg, 'medicine1_dosage_timing' => $medicine1_dosage_timing );
	
	echo json_encode($arr);
	die();
}

function cdrmed_get_patient_shoop_data($val) {
	
	global $wpdb;
	
	$current_usid = get_current_user_id();
	$userrole = get_user_role();
	
	// Lets Check if Physician is on Preview Page
	$get_preview_post_id = isset($_GET['post_id']) ? $_GET['post_id'] : null;
	$pid = isset($_GET['pid']) ? $_GET['pid'] : $current_usid;
	
	// Preview Page Date to be Displayed
	if($get_preview_post_id && $pid){
		
		$post_id = $get_preview_post_id;
		$html = '';
		if(is_page(array('preview-strain'))){
			
			$html .= '<div class="col-md-4">
				<section class="main-heading" style="padding-top: 20px;">
					<h2>This is only preview of Therapy which you just created. It won\'t be displayed to patient unless you click Send To Patient Button.</h2>
				</section>
			</div>';
		}
		
		$html .= '<div class="col-md-8">
			<div class="shoop-message"><div class="feedback auto-shoop-message send_shoop" style="display: none;"></div></div>
			'.cdrmed_get_print_shoop($post_id,$pid,$userrole,'shoop-message').'
		</div>';
		
		$html = '<div class="patient-details-outerwrap">'.$html.'</div>';
		
		return $html;
	}
	else if($userrole == 'physician' || $userrole == 'dispensary' || $userrole == 'patient'){
		$shoop_id =  get_user_meta($pid, 'latest_patient',true);
		$auto_shoop_type = get_post_meta($shoop_id, 'auto_shoop_type', true);
		if($userrole == 'patient'){
			/* if($auto_shoop_type == 'hidden'){
				return '<center><strong>Select Products for Your Therapy</strong></center>';
			}
			else */
			if($shoop_id){
				return cdrmed_get_print_shoop($shoop_id, $pid, $userrole,'');
			}
			else{
				return '<center><strong>Your SHOOP is pending for finalization. If you have any questions, please contact your Physician!</strong></center>';
			}
		}
		else{
			if($auto_shoop_type != 'hidden' || $userrole != 'dispensary'){
				ob_start();
				
				$args = array(
					'posts_per_page'   =>  5,
					'orderby'          => 'date',
					'order'            => 'DESC',
					'post_type'        => 'strain_shoops',
					'author'           => $pid,
					'post_status'      => 'private',
				);
				
				$posts_array = get_posts( $args );
				if($posts_array) {
					foreach ($posts_array as $post) {
						$current_read_shoop_id = $post->ID;
						$selected_shoop = '';
						if($shoop_id == $current_read_shoop_id)
								$selected_shoop = '<div class="latest-patient-shoop-select"><i class="fa fa-star"></i></div>';
						echo $selected_shoop.cdrmed_get_print_shoop($current_read_shoop_id, $pid, $userrole, 'shoop-message');
					}
				}
				
				//if($shoop_id)
					//$selected_shoop = '<div class="latest-patient-shoop-select"><i class="fa fa-star"></i></div>';
				//echo $selected_shoop.cdrmed_get_print_shoop($shoop_id, $pid, $userrole, 'shoop-message');
				$html .= '<div class="shoop-message"><div class="feedback auto-shoop-message send_shoop" style="display: none;"></div></div>'.ob_get_contents();
				ob_get_clean();
				if (ob_get_contents()) ob_end_flush();
				return $html;
			}
		}
	}else {
		return '<br><br><center><strong>Something Went Wrong, Please report this to administrator so technical team may check it up!</strong></center><br><br>';	
	}
	
}
add_shortcode('get-patient-shoop-data','cdrmed_get_patient_shoop_data');
add_shortcode( 'strain_preview_form', 'cdrmed_get_patient_shoop_data' );





// Function to Be Called for Getting All or Single Shoop of Patient
function cdrmed_get_print_shoop($shoopid,$pid,$userrole, $call_from = ''){
		
		$post_id = null;
		if($shoopid) {
			$post_id = $shoopid;
		}
		$data = '';
		if ( $post_id ){
			$i = 0;
			
			$auto_shoop_check = (get_post_meta($post_id, 'auto_shoop', true) != '') ? '<i class="fa fa-star"></i>' : '';
			
			$data .= '<section class="shoop_review col-md-12">
				<div class="container-fluid">
					<div class="row">';
						if($userrole == 'patient'){
							$data .='<div class="col-sm-12">
								<section class="main-heading">
									<h2>Welcome to your SHOOP.<br>This can be used as a baseline to help guide you to your cannabis treatment</h2>
								</section>
							</div>';
						}
						for($i=0; $i < 4; $i++) {
							$medicine_cannabinoid_1 = get_post_meta( $post_id, 'medicine'.($i+1).'_cannabinoid_1', true );
							$medicine_target_dosage_1 = get_post_meta( $post_id, 'medicine'.($i+1).'_target_dosage_1', true );
							$medicine_frequency_1 = get_post_meta( $post_id, 'medicine'.($i+1).'_frequency_1', true );
							$medicine_cannabinoid_2 = get_post_meta( $post_id, 'medicine'.($i+1).'_cannabinoid_2', true );   
							$medicine_dosage_frequency = explode(",",get_post_meta( $post_id, 'medicine'.($i+1).'_dosage_frequency'.($i+1), true ));
							$medicine_dosage_mg = explode(",",get_post_meta( $post_id, 'medicine'.($i+1).'_dosage_mg'.($i+1), true ));
							$medicine_dosage_timing = explode(",",get_post_meta( $post_id, 'medicine'.($i+1).'_dosage_timing'.($i+1), true ));
							$medicine_terpene = get_post_meta( $post_id, 'medicine'.($i+1).'_terpene', true );
							$medicine_ingestion_method = get_post_meta( $post_id, 'medicine'.($i+1).'_ingestion_method', true );
							$notes = get_post_meta( $post_id, 'medicine_notes'.($i+1), true ); 
							
							// Medicine Counter
								$ki = 0;
								$tbl2 ='';
							// Check if Medicine Frquency is Daily and Its not Hourly
							if($medicine_dosage_frequency[0] != '' && strpos($medicine_frequency_1, "hourly") === false){
								
								// Extract Each Medicine and Check with Counter
								foreach ($medicine_dosage_frequency as  $value) {
									
									if($value){
										$med_dose = '';
										$med_time = '';
										if(is_array($medicine_dosage_mg) && array_key_exists($ki,$medicine_dosage_mg)){
											$med_dose = $medicine_dosage_mg[$ki];
										}
										if(is_array($medicine_dosage_timing) && array_key_exists($ki,$medicine_dosage_timing)){
											$med_time = $medicine_dosage_timing[$ki];
										}
										$tbl2 .= '<li><b>Dosage '.($ki+1).': </b> '.$value.' '.$med_dose.' '.ucwords($med_time).'</li>';
										$ki++;
									}
								}
								//End for Each for Medicine Dosage Extraction
							}									
							
							// Check if there is no further cannabinoid in the system so that we may add last class border to end of Medicine
							$last_cannabinoid_system_check = get_post_meta( $post_id, 'medicine'.($i+2).'_cannabinoid_1', true );
							if($last_cannabinoid_system_check == ''){
								$last = 'last';
							} else if($i == 3){
								$last = 'last';
							} else {
								$last = '';
							}
							
							// Start Printing of Medicines by checking if Target Cannabinoid Exists						
								if($medicine_cannabinoid_1 != '' && $medicine_ingestion_method !=''){
									$data .= '<div class="col-sm-12">
										<div class="shoop_review-content'.($i+1).' '.$last.'">
											<div id="shoop_review-content'.($i+1).'">
												<section class="shoop-hd-crcl'.($i+1).'">
													'.$auto_shoop_check .' <b>Medicine : '.($i+1).'  </b><b class="txt-right"> '.$post_id.' Date Created : '.get_the_date( "Y-m-d", $post_id ).' </b>
												</section>
												<ul class="shoop_list'.($i+1).'">
													<li><b>Target Cannabinoid: </b> '.$medicine_cannabinoid_1.'</li>';
													if($medicine_terpene !=''){
														$data .= ' <li><b>Terpenes: </b> <ul>'. $medicine_terpene.' </ul></li>';
													}
													$data .= ' <li><b>Target Dose: </b> '.str_replace("X","",$medicine_target_dosage_1).'</li>
													<li><b>Frequency: </b> '.str_replace("X","x",$medicine_frequency_1).'</li>';
													$data .= $tbl2.'<li class="with-method">
														<p><b>Ingestion Method: </b> '.$medicine_ingestion_method.'</p>
														<div class="method-text">
															<span>'.$medicine_ingestion_method.'</span>';
																switch ($medicine_ingestion_method) {
																	case 'Inhaled':
																		$data .=' <img src="'.CDRMED.'/includes/assets/img/'.$i.'-1.png" alt=""/>';
																		break;
																	case 'Ingested (edible)':
																		$data .=' <img src="'.CDRMED.'/includes/assets/img/'.$i.'-2.png" alt=""/>';
																		break;
																	case 'Sublingual':
																		$data .=' <img src="'.CDRMED.'/includes/assets/img/'.$i.'-3.png" alt=""/>';
																		break;
																	case 'Suppository':
																		$data .=' <img src="'.CDRMED.'/includes/assets/img/'.$i.'-4.png" alt=""/>';
																		break; 
																	case 'Topical':
																		$data .=' <img src="'.CDRMED.'/includes/assets/img/'.$i.'-5.png" alt=""/>';
																		break;     
																}	
														$data .=' </div>
													</li>';
													if($notes != '') { 
														$data .=' <li><b>Dosage Notes:  </b> '.$notes.'</li>';
													}                                 
												$data .='</ul>
											</div>
										</div>
									</div>';
							} // if condition ends here
						}
					$data .='</div>';
				$data .='</div>';
				if($userrole != 'patient' && $post_id){
					// Buttons Under SHOOP For Deletion and Editing Etc
					$data .= '<section>
						<div class="text-center form-footer-btns">';
						if($call_from != 'import-shoop' && $userrole != 'dispensary'){
							$data .= '<a href="'.get_home_url().'/shoop-for-patient/create-new-shoop/?post_id='.$post_id.'&pid='.$pid.'&page_type=edit">
							<button type="button" class="btn reset">EDIT</button></a>';
						}
						if($userrole != 'dispensary'){
							$data .= '<button type="button" class="btn preview import_shoop_send_to_patient"  data-patient_id="'.$pid.'" data-shoop_id="'.$post_id.'" data-call_by="'.$call_from.'" data-reload="false">SEND TO PATIENT</button>';
						}
						if($call_from != 'import-shoop' && $userrole != 'dispensary'){
							$data .= '<button type="button" class="btn preview delete-patient-shoop" data-patient_id="'.$pid.'" data-shoop_id="'.$post_id.'">DELETE</button>';
						}
						$data .= '</div>
					</section>';
				}
			$data .= '</section>';  // SHOOP Review Section Ends Here

		} else {
			$data .='<center><strong>Your SHOOP is pending for finalization. If you have any questions, please contact your Physician!</strong></center>';
		}
	

	return $data;

}

//add_shortcode( 'cdrmed-get-print-shoop', 'cdrmed_get_print_shoop' );
//add_shortcode( 'strain_preview_form', 'cdrmed_get_print_shoop' );


// Function to Send SHOOP To Patient
function send_shoop_to_patient() {
	
	$send_email = isset($_GET['send_email']) ? $_GET['send_email'] : null;
	$feedback_message = '';
	if(!empty($send_email)){
		
		update_post_meta($send_email, 'sent_patient','1');
		update_user_meta($pid, 'latest_patient',$send_email);
		update_post_meta($send_email, 'send_patient_date',date("Y-m-d h:i:s"));
		
		$patient_info = get_userdata( $pid );
		
		$args = array(
			'call_by' => 'shoop-alert-patient',
			'receiver' => $patient_info->user_email, 
			'subject_name' => '', 
			'body_name' => '', 
			'body_part' => '', 
			'password_reset_url' => ''
		);
		cdrmed_send_email($args);
		$feedback_message = "<b id='successMessage'><center>SHOOP sent Successfully.</center></b>";
	}
}

?>