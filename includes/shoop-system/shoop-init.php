<?php
if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly 

function shoop_for_patient() {
	
	/*
	* Shortcode Changes [strain_shoop_physician] => [cdrmed-get-print-shoop]
	*
	*
	*/
	$page_type = isset($_GET['page_type'])? $_GET['page_type'] : '';
	$html = '';
	//this shortcode is base on user role(Physician/Dispensary)
	//$html .=  do_shortcode('[physicians-tab-nav]');
	$html .= '<div class="patient-details-outerwrap">
		<section class="shoop_review">
			<div class="container">
				<div class="row">';
					if($page_type != 'dosing_history'){
						$html .= '<div class="col-sm-12">
							<section class="main-heading">
								<h2> Please review the SHOOP information is correct before sending on to the patient..</h2>
							</section>
						</div>
						<div class="clearfix"></div>';
						$html .= do_shortcode('[straintabs]').'<div class="col-md-8">'.do_shortcode('[get-patient-shoop-data]').'</div>';
					}
					else{
						$html .= do_shortcode('[dosing_history]');
					}
					
				$html .= '</div>
			</div>
		</section>';
	$html .= '</div>';
	return $html;

}
add_shortcode( 'shoop-for-patient', 'shoop_for_patient' );
?>