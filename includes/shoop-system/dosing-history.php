<?php
if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly 


function dosing_history() {
	
	global $wpdb;
	
	$current_usid = get_current_user_id();
	
	$getpatientid = isset($_GET['pid'])? $_GET['pid'] : '';
	if($getpatientid) {
		$pid = $getpatientid;
	} else {
		$pid = get_current_user_id();
	}
	$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
	$args = array(
		'post_type'        => 'patient_therapy',
		'posts_per_page'   => 5,
		'orderby'          => 'date',
		'order'            => 'DESC',
		'author'   => $pid,
		'paged' => $paged,
	);
	
	$posts_array = new WP_Query( $args );
	$html = '<div class="journey topspace"><h3>Dosing History</h3><br></div>
	<div class="dosing-history" >';
	if($posts_array) {
		//foreach ($posts_array as $post) {
		while ( $posts_array->have_posts() ) : $posts_array->the_post();
				
			$html_part = '<div class="single-dose">';
			$data = '';
			$therapy_id = get_the_ID();
			
			$therapy_data = get_post_meta( $therapy_id,'_cdrmed_therapy_data',false );
			$shoop_id = get_post_meta( $therapy_id, '_cdrmed_therapy_shoop_id', true );
			
			$ex_data_arr = $therapy_data['0'];
			if($ex_data_arr) {
				$therapy_counter = 0;
				$html_part .= '<div class="col-md-4">';
					$html_part .= '<div id="shoopdosage">
						<span class="therapy-shoop-id">Therapy ID: '.$shoop_id.'</span>';
						foreach($ex_data_arr as $key => $val){
							$ex_data = $val;
							$therapy = json_decode($ex_data);
							$product_name = $therapy->p_product_title;
							$pcan = $therapy->p_cannabinoid;
							$pcan_value = $therapy->p_cannabinoid_value;
							$pcan_percent_value = $therapy->p_cannabinoid_percent_value;
							$scan = $therapy->s_cannabinoid;
							$scan_value = $therapy->s_cannabinoid_value;
							$scan_percent_value = $therapy->s_cannabinoid_percent_value;
							$total_this_value = $therapy->this_much_value;
							$dpd = $therapy->dose_per_day;
							$sec_cal = $therapy->secondary_calculations;
							$freq = $therapy->frequency_per_day;
							$full_profile = $therapy->full_profile_link;
							$calculations = $therapy->calculations;

							$html_part .= ' <div class="col-sm-12">
								<div class="shoop_review-content'.($therapy_counter+1).' last dosageinfocal" style="margin-top:20px;">
									<div id="shoop_review-content'.($therapy_counter+1).'">
										<section class="shoop-hd-crcl'.($therapy_counter+1).' shoop-title-b"><b class="">'.$product_name.'</b></section>
										<ul class="shoop_list'.($therapy_counter+1).'">
											 <li>'.$pcan_percent_value.' % <b>'.$pcan.'</b> | '.$scan_percent_value.' % <b>'.$scan.'</b></li>
											 <li><b>To Be Taken</b> '.$freq.'</li>
											 <li><b>'.$pcan.' Per Day:</b> '.$total_this_value.' (g)</li>
											 <li><b>Dose of</b> '.$pcan.' each time: '.$dpd.' (g)</li>
											 <li><b>Amount of</b> '.$scan.' Per Day: '.$sec_cal.' (mg)</li>
											 <li><center>'.$full_profile.'</center></li>
										 </ul>';
									$html_part .= '</div>
								</div>
							</div>';
							$therapy_counter++;
						}
					$html_part .= '</div>
				</div>';
				
				
				
				$data .= '<div class="col-md-8">';
					
					$auto_shoop_check = (get_post_meta($shoop_id, 'auto_shoop', true) != '') ? '<i class="fa fa-star"></i>' : '';
					for($i=0; $i < $therapy_counter; $i++) {
							$medicine_cannabinoid_1 = get_post_meta( $shoop_id, 'medicine'.($i+1).'_cannabinoid_1', true );
							$medicine_target_dosage_1 = get_post_meta( $shoop_id, 'medicine'.($i+1).'_target_dosage_1', true );
							$medicine_frequency_1 = get_post_meta( $shoop_id, 'medicine'.($i+1).'_frequency_1', true );
							$medicine_cannabinoid_2 = get_post_meta( $shoop_id, 'medicine'.($i+1).'_cannabinoid_2', true );   
							$medicine_dosage_frequency = explode(",",get_post_meta( $shoop_id, 'medicine'.($i+1).'_dosage_frequency'.($i+1), true ));
							$medicine_dosage_mg = explode(",",get_post_meta( $shoop_id, 'medicine'.($i+1).'_dosage_mg'.($i+1), true ));
							$medicine_dosage_timing = explode(",",get_post_meta( $shoop_id, 'medicine'.($i+1).'_dosage_timing'.($i+1), true ));
							$medicine_terpene = get_post_meta( $shoop_id, 'medicine'.($i+1).'_terpene', true );
							$medicine_ingestion_method = get_post_meta( $shoop_id, 'medicine'.($i+1).'_ingestion_method', true );
							$notes = get_post_meta( $shoop_id, 'medicine_notes'.($i+1), true ); 
							
							// Medicine Counter
								$ki = 0;
								$tbl2 ='';
							// Check if Medicine Frquency is Daily and Its not Hourly
							if($medicine_dosage_frequency[0] != '' && strpos($medicine_frequency_1, "hourly") === false){
								// Extract Each Medicine and Check with Counter
								foreach ($medicine_dosage_frequency as  $value) {
									$med_dose = '';
									$med_time = '';
									if(is_array($medicine_dosage_mg) && array_key_exists($ki,$medicine_dosage_mg)){
										$med_dose = $medicine_dosage_mg[$ki];
									}
									if(is_array($medicine_dosage_timing) && array_key_exists($ki,$medicine_dosage_timing)){
										$med_time = $medicine_dosage_timing[$ki];
									}
			
									
										$tbl2 .= '<li><b>Dosage '.($ki+1).': </b> '.$value.' '.$med_dose.' '.ucwords($med_time).'</li>';
										$ki++;
								}
								//End for Each for Medicine Dosage Extraction
							}									
							
							// Check if there is no further cannabinoid in the system so that we may add last class border to end of Medicine
							$last_cannabinoid_system_check = get_post_meta( $shoop_id, 'medicine'.($i+2).'_cannabinoid_1', true );
							if($last_cannabinoid_system_check == ''){
								$last = 'last';
							} else if($i == 3){
								$last = 'last';
							} else {
								$last = '';
							}
							
							// Start Printing of Medicines by checking if Target Cannabinoid Exists						
								if($medicine_cannabinoid_1 != '' && $medicine_ingestion_method !=''){
									$data .= '<div class="col-sm-12">
										<div class="shoop_review-content'.($i+1).' '.$last.'">
											<div id="shoop_review-content'.($i+1).'">
												<section class="shoop-hd-crcl'.($i+1).'">
													'.$auto_shoop_check .' <b>Medicine : '.($i+1).'  </b><b class="txt-right"> '.$shoop_id.' Date Created : '.get_the_date( "Y-m-d", $shoop_id ).' </b>
												</section>
												<ul class="shoop_list'.($i+1).'">
													<li><b>Target Cannabinoid: </b> '.$medicine_cannabinoid_1.'</li>';
													if($medicine_terpene !=''){
														$data .= ' <li><b>Terpenes: </b> <ul>'. $medicine_terpene.' </ul></li>';
													}
													$data .= ' <li><b>Target Dose: </b> '.str_replace("X","",$medicine_target_dosage_1).'</li>
													<li><b>Frequency: </b> '.str_replace("X","x",$medicine_frequency_1).'</li>';
													$data .= $tbl2.'<li class="with-method">
														<p><b>Ingestion Method: </b> '.$medicine_ingestion_method.'</p>
														<div class="method-text">
															<span>'.$medicine_ingestion_method.'</span>';
																switch ($medicine_ingestion_method) {
																	case 'Inhaled':
																		$data .=' <img src="'.CDRMED.'/includes/assets/img/'.$i.'-1.png" alt=""/>';
																		break;
																	case 'Ingested (edible)':
																		$data .=' <img src="'.CDRMED.'/includes/assets/img/'.$i.'-2.png" alt=""/>';
																		break;
																	case 'Sublingual':
																		$data .=' <img src="'.CDRMED.'/includes/assets/img/'.$i.'-3.png" alt=""/>';
																		break;
																	case 'Suppository':
																		$data .=' <img src="'.CDRMED.'/includes/assets/img/'.$i.'-4.png" alt=""/>';
																		break; 
																	case 'Topical':
																		$data .=' <img src="'.CDRMED.'/includes/assets/img/'.$i.'-5.png" alt=""/>';
																		break;     
																}	
														$data .=' </div>
													</li>';
													if($notes != '') { 
														$data .=' <li><b>Dosage Notes:  </b> '.$notes.'</li>';
													}                                 
												$data .='</ul>
											</div>
										</div>
									</div>';
							} // if condition ends here
					}
				$data .= '</div>';
				
				$html_part .= $data;
				
			}
			$html_part .= '</div>';
			
			$html .= $html_part;
		endwhile;
		
		$total_pages = $posts_array->max_num_pages;
		$current_page = max(1, get_query_var('paged'));
		$html .= '<div class="dosing-history-pag">'.paginate_links(array(
			'base' => get_pagenum_link(1) . '%_%',
			'format' => '&paged=%#%',
			'current' => $current_page,
			'total' => $total_pages,
			'type'         => 'list',
			'prev_text'    => 'Previous',
			'next_text'    => 'Next',
			'end_size'     => 3,
			'mid_size'     => 3
		)).'</div>';
		
	}
	$html .= '</div>';
	
	return $html;
}
add_shortcode( 'dosing_history', 'dosing_history' );

?>