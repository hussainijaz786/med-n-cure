<?php 

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly 
//Tabs new 

function strain_tabs(){
	
    $pid = !isset($_GET['pid'])?null:$_GET['pid'];
	$userrole = get_user_role();
	
    $tabs = ' <div class="col-sm-4">
		<div class="col-md-12 shoop_review_links">';
			if($userrole == 'physician' || $userrole == 'caregiver' || $userrole == 'administrator'){
				$tabs .= '<a href="'.get_home_url().'/shoop-for-patient/create-new-shoop/?pid='.$pid.'">Create New SHOOP</a>
				<a class="import_shoop_modal_btn" data-patient_id="'.$pid.'" style="cursor: pointer;">Import SHOOP</a>
				<a id="generate-auto-shoop" class="generate-auto-shoop" data-patient_id="'.$pid.'" style="cursor: pointer;">Auto SHOOP</a>
				<a href="'.get_home_url().'/shoop-for-patient/?pid='.$pid.'&page_type=dosing_history">Dosing History</a>';
			}
		$tabs .= '</div>';
		//shortcode from patient-functions/patient-therapy-dashboard-display.php
		$tabs .='<div class="col-md-12  phys-patient-therapy-class">';
			$tabs .= do_shortcode('[patient-therapy-display]');
		$tabs .= '</div>
	</div>';
	
    return $tabs;
}

add_shortcode( 'straintabs', 'strain_tabs' );

 ?>
