<?php 
if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly 

function cdrmed_shoop_patient_products() {
	ob_start();
	
	$userrole = get_user_role();
	$patient_id = isset($_GET['pid']) ? $_GET['pid'] : get_current_user_id();
	
	//get patient lastest shoop id
	$latest_shoop_id = get_user_meta($patient_id, 'latest_patient', true);
	
	//get patient physicians
	$patient_physicians = get_user_meta($patient_id, 'patient_physician_id', true);
	
	$auto_shoop_post_id = '';
	//patient have no parent physician then
	if($patient_physicians == 'orphan' || $patient_physicians == ''){
		//If patient have no Shoop or 
		if(!$latest_shoop_id){
			$auto_shoop_post_id = do_shortcode('[auto-shoop call_by="call-by-product-recommended-shortcode" pid="'.$patient_id.'"]');
			//if auto shoop shortcode return shoop id then
			if($auto_shoop_post_id){
				update_user_meta($patient_id, 'latest_patient', $auto_shoop_post_id);
				//meta key for show/hide shoop at patient side
				update_post_meta($auto_shoop_post_id, 'auto_shoop_type', 'hidden');
				$latest_shoop_id = $auto_shoop_post_id;
			}
		}
	}
	
	// Get Patient Dispensary ID for Products
	$dispensary_id_arr = json_decode(get_user_meta($patient_id, 'patient_dispenary_products_id', true),true);
	//count total medicines
	$medicine_counter = 0;
	for($i=1; $i < 5; $i++) {
		if(get_post_meta( $latest_shoop_id, 'medicine'.($i).'_cannabinoid_1', true )){
			$medicine_counter++;
		}
	}
	
	//if patient have no shoop
	if(!$latest_shoop_id){
		echo '<a href="#" class="btn bordered disabled">NO SHOOP for Products</a>';
	} else {
		if( $userrole == 'dispensary'){
			echo '<a href="#" class="btn bordered show-product-modal" data-target="#ProductsModal" data-toggle="modal">Select Products</a>';
			
			//if current aptient have any order then dispensary view patient's order
			$args = array(
				'post_type' => 'shop_order',
				'posts_per_page' => -1,
				'order' => 'DESC',
				'post_status' => array('wc-processing','wc-completed', 'wc-on-hold', 'wc-cancelled'),
				'meta_query' => array(
					'relation' => 'AND',
					array(
						'key' => '_customer_user',
						'value' => $patient_id,
						'compare' => '='
					),
				),
			);
			$post = new WP_Query( $args );
			if((intval($post->post_count) > 0)){
				echo '<a href="'.site_url().'/dispensary-dashboard?page_type=orders&pid='.$patient_id.'" class="btn bordered">Patient Orders</a>';
			}
		} else {
			//if patient have slscted dispensary for products
			if($dispensary_id_arr) {
				// Placed another check over here to only show select product buttons if dispensary is already selected
				echo '<center>Please choose your products by clicking the button below. You will only see products from your preferred dispensaries.<br><a href="#" class="btn bordered show-product-modal" data-target="#ProductsModal" data-toggle="modal">Select Products</a></center>';
			} else {
				//Select dispensary for products
				echo '<center>Before you may select products, please choose atleast one dispensary from your profile.<br><a href="#profile" class="btn bordered show-product-modal" data-toggle="tab" data-toggle="modal">Set Dispensary</a></center>';
			}
		}
	}
	/*
	*
	*	Start Modal Box
	*
	*/
	?>
	<!-- Css Style for Modal fade in -->
	<style type="text/css">
		.modal-backdrop.fade.in{ display: none; }
		.modal.fade.in { background: rgba(0, 0, 0, 0.61) none repeat scroll 0 0; }
	</style>
	<div class="modal fade" id="ProductsModal" role="dialog" >
		<div class="modal-dialog multi-select-modal navypurple modal-lg">
			<!-- Modal content-->
			<div class="modal-content">
				
				<div class="modal-header">
					<button type="button" class="close fa fa-close" data-dismiss="modal"></button>
				</div>
				<!-- Store patient value for get in ajax call for fetch products -->
				<input type="hidden" class="shoop_patient_id" value="<?php echo $patient_id; ?>" >
				<input type="hidden" class="shoop_current_step" value="0" >
				<input type="hidden" class="shoop_medicine_count" value="<?php echo $medicine_counter ?>" >
				
				<!-- Modal Body -->
					<div style="display: none; padding-top: 20px;" class="shoop-waiting"></div>
				
					<?php for($i=0; $i < $medicine_counter; $i++) { ?>
						<?php $selected = ''; if($i > 0){ $selected = 'style="display: none;"'; } ?>
						<div data-step="<?php echo $i; ?>" class="modal-contendddt cdrmed-step step_<?php echo $i; ?> medicine-steps" <?php echo $selected; ?>>
							<div id="heading-text_<?php echo $i; ?>" class="cdrm-header">
								<h4 data-cannabinoid=""></h4>
								<div class="cdrmed_modal_slider">
								</div>
							</div>
						</div>
					<?php } ?>
					
					<div style="display: none;" class="modal-contendddt cdrmed-step step_<?php echo $medicine_counter++; ?> step-show-selected-products">
						<div class="cdrm-header">
							<h4>Here are your selected products. <br />Click the button below to calculate your daily dosing guide.</h4>
						</div>

						<div id="selected_products_markup" class="step_2_items"></div>
						<div class="buttons">
							<button id="calculate-daily-dosage" type="button" style="min-width: 300px;" data-id="<?php echo $patient_id; ?>">Calculate my daily dose</button>
						</div>

						<div id="selected_products_markup_thumb" class="cdrmed_slid_footer"></div>
					</div><!-- end .modal-content -->

					<div style="display: none;" class="modal-contendddt cdrmed-step step_<?php echo $medicine_counter++; ?> step-view-therapy">
						<div class="cdrm-header">
							<h4>Here's your daily dose. Click the button below to save this therapy.</h4>
							<div class="buttons">
								
								<button class="cancel-therapy" type="button">Cancel</button>
								<button data-details="<?php echo $latest_shoop_id.','.$patient_id; ?>" id="save-therapy-btn" data-product_id="" type="button">Save Therapy</button>
								<img id="save-therapy-loader" src="<?php echo CDRMED ?>/includes/assets/img/ajax-loader.png" style="width: 20px; display: none;">
							</div>
						</div>

						<div id="calucations_items" class="step_3_slider"></div><!-- end step_3slider -->

						<div id="calucations_items_thumbs" class="cdrmed_slid_footer"></div>
					</div>
					
			</div>
		</div>
	</div>
	
	<?php
	$data_output = ob_get_contents();
	ob_end_clean();
	return $data_output;
}

add_shortcode( 'shoop-products', 'cdrmed_shoop_patient_products' );

