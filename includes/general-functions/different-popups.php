<?php 

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly 

// Add Shortcode

add_action( 'wp_footer', 'different_popups_functions',10);

function different_popups_functions() {
  
?>

<div class="modal fade" id="privacyPolicypp" role="dialog">
	<div class="modal-dialog multi-select-modal navyblue modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close fa fa-close" data-dismiss="modal"></button>
				<div class="journey">
				<h2>Privacy Policy</h2>
				</div>
			</div>
			<div class="popmake-content">
			<p>MednCures. (collectively, the “Companies,” “we” or “us”) respect and are committed to protecting your privacy. Generally, you can browse through our Website without giving us any information about yourself. When we do need to collect your PII (defined below) to provide you with the Service, or when you choose to provide us with your PII, this Privacy Policy describes how we collect, use and disclose your PII, except for personal health information submitted by you in the course of using our Service, which is covered by our MednCures. HIPAA Policy (the “<a href="http://beta.medncures.com/hipaa-statement/">HIPAA Policy</a>“). Any conflict between this Privacy Policy and the HIPAA Policy with respect to such submitted personal health information shall be resolved in favor of the HIPAA Policy. Also, please note that, unless we define a term in this Privacy Policy, all capitalized terms used in this Privacy Policy have the same meanings as in our Terms of Service. Therefore, please make sure that you have read and understand our Terms of Service.</p>
			
			<br>
			<h2>Changes to this Privacy Policy</h2>
			<p>Any information that is collected via our Website, App or the Service is covered by the Privacy Policy or the HIPAA Policy in effect at the time such information is collected. We may amend this Privacy Policy from time to time. If we make any substantial changes in the way we use your PII, we will notify you of those changes by posting on the Service, App and Website or by sending you an email or other notification, and we will update the “Updated Date” above to indicate when those changes will become effective.</p>
			
			<br>
			<h2>Information Collection</h2>
			<p>Personally identifiable Information (“PII”) means any information that may be used to identify an individual, including, but not limited to, a first and last name, email address, a home, postal or other physical address, and phone number.</p>
			<p>Account Information. We collect your PII when you register with the Companies for a MednCures account, when you submit an entry for a sweepstakes or other promotion, or when you submit your PII to us through the Service for any other reason. If you create an account through the Website or the App, we may also collect your title, birth date, gender, occupation, industry, personal interests, and other information that is not considered PII because it cannot be used by itself to identify you.</p>
			<p>Cookies and Tracking Technology. A “cookie” is a small data file that certain websites write to your hard drive when you visit them. A cookie file can contain information such as a user ID that the website uses to track the pages you’ve visited, but the only PII a cookie can contain is information you supply yourself. A cookie can’t read data off your hard disk or read cookie files created by other websites. Some parts of the Service use cookies to understand user traffic patterns and to tell us how and when you interact with our Website, App and Service. We do this in order to determine the usefulness of our Website, App and Service information to our users, to see how effective our navigational structure is in helping users reach that information and to customize and improve our Website, App and Service. Unlike persistent cookies, session cookies are deleted when you log off from the Website, App and Service and close your browser.</p>
			<p>If you prefer not to receive cookies while browsing our Website, App and Service, you can set your browser to warn you before accepting cookies and refuse the cookie when your browser alerts you to its presence. You can also refuse all cookies by turning them off in your browser. You do not need to have cookies turned on to use/navigate through many parts of our Website, App and Service, although if you do so, you may not be able to access all portions or features of the Website, App and Service. Some third-party service providers that we engage (including third-party advertisers) may also place their own cookies on your hard drive. Note that this Privacy Policy covers only our use of cookies and does not include use of cookies by such third parties.</p>
			<p>“Web Beacons” (also known as Web bugs, pixel tags or clear GIFs) are tiny graphics with a unique identifier that may be included on our Website, App and Service for several purposes, including to deliver or communicate with cookies, to track and measure the performance of our Website, App and Service, to monitor how many visitors view our Website, App and Service, and to monitor the effectiveness of our advertising. Unlike cookies, which are stored on the user’s hard drive, Web Beacons are typically embedded invisibly on web pages (or in an e-mail).</p>
			<p>Information Related to Your Use of the Service. If you have not created an account, when you use our Service, you use our Service, App and Website anonymously. We do automatically log your IP address (the Internet address of your computer) and other information such as your browser type, operating system, the web page that you were visiting before accessing our Website, App and Service, the pages or features of our Website, App and Service to which you browsed to give us an idea of which part of our Website, App and Service you visit and how long you spend there (we refer to this information as “Log Data”). We do not link your IP address to any PII unless you have logged in via your account. We use Log Data to administer the Website, App and Service and we analyze (and may engage third parties to analyze) Log Data to improve, customize and enhance the Website, App and Service by expanding their features and functionality and tailoring them to our users’ needs and preferences.</p>
			<p>Information Sent by Your Mobile Device. We collect certain information that your mobile device sends when you use our Website, App and Service, like a device identifier, user settings and the operating system of your device, as well as information about your use of our Website, App and Service. You understand that, by logging into the App on your mobile device, some information pertaining to your medical treatment may be stored to your mobile device. We are not responsible for any unauthorized access by any third party to such information on your mobile device.</p>
			<p>Location Information. When you use our App, we may collect and store information about your location by converting your IP address into a rough geo-location or by accessing your mobile device’s GPS coordinates or course location if you enable location services on your device. We may use location information to improve and personalize our App for you. If you do not want us to collect location information, you may disable that feature on your mobile device.</p>
			
			<br>
			<h2>Your Choices</h2>
			<p>Opt-Out. We may periodically send you free newsletters and e-mails that directly promote our Service. When you receive such promotional communications from us, you will have the opportunity to “opt-out” (either through your account or by following the unsubscribe instructions provided in the e-mail you receive). We do need to send you certain communications regarding the Service and you will not be able to opt-out of those communications – e.g., communications regarding updates to our Terms of Service or this Privacy Policy or information about billing.</p>
			
			<h2>Information Sharing and Disclosure</h2>
			<p>Your PII is not shared outside of the Companies without your permission, except as described below. Information Shared with Our Service providers. We may engage third-party service providers to work with us to administer and provide the Service. These third-party service providers have access to your PII only for the purpose of performing services on our behalf and are expressly obligated not to disclose or use your PII for any other purpose.</p>
			<p>Information Shared with Third Parties. We may share your aggregated information and non-identifying information with third parties to conduct ongoing quality improvement activities, or for industry research and analysis, demographic profiling and other similar purposes.</p>
			<p>Information Disclosed in Connection with Business Transactions. Information that we collect from our users, including PII, is considered to be a business asset. Thus, if we are acquired by a third party as a result of a transaction such as a merger, acquisition or asset sale or if our assets are acquired by a third party in the event we go out of business or enter bankruptcy, some or all of our assets, including your PII, may be disclosed or transferred to a third-party acquirer in connection with the transaction.</p>
			<p>Information Disclosed for Our Protection and the Protection of Others. We cooperate with government and law enforcement officials or private parties to enforce and comply with the law. We may disclose any information about you to government or law enforcement officials or private parties as we, in our sole discretion, believe necessary or appropriate: (i) to enforce our Terms of Service, (ii) to respond to claims, legal process (including subpoenas); (iii) to protect our property, rights and safety and the property, rights and safety of a third-party, our users, or the public in general; (iv) to stop any activity that we consider illegal, unethical or legally actionable activity; and (v) as required in accordance with HIPAA or related applicable local, state or federal laws (please refer to the HIPAA Policy).
			</p>
			<p>Your Ability to Review Your Account and Information</p>
			<p>If you are a registered user with an account, you can review your PII by going to www.medncures.com and signing in using the login page.</p>
			
			<br>
			<h2>Data Security</h2>
			<p>Your account information is password-protected for your privacy and security. The Companies safeguard the security of the information you provide to us with physical, electronic, and managerial procedures. In certain areas of our Website and App, we use industry-standard SSL-encryption to enhance the security of data transmissions. While we strive to protect your PII, we cannot ensure the security of the information you transmit to us, and so we urge you to take every precaution to protect your PII when you are on the Internet. Change your passwords often, use a combination of letters and numbers, and make sure you use a secure browser.</p>
			
			<br>
			<h2>Responding to Do Not Track Signals</h2>
			<p>Our Website does not have the capability to respond to “Do Not Track” signals received from various web browsers.</p>
			<p>Children and Privacy</p>
			<p>Our Website, App and Service do not target and are not intended to attract children under the age of 13. We do not knowingly solicit PII from children under the age of 13 or send them requests for PII. If we learn that we have collected PII of a child under 13 directly from that child we will take steps to delete such information from our files as soon as possible. Notwithstanding the foregoing, we may collect PII about children under 13 that parents or guardians provide to us when establishing an account for their children’s records.</p>
			
			
			<br>
			<h2>Third-Party Sites</h2>
			<p>The Website, App and Service contain links to other sites that are owned or operated by third parties. We are not responsible for the content, privacy or security practices of any third parties. To protect your information, we encourage you to learn about the privacy policies of those third parties.</p>
			
			<h2>International Transfer</h2>
			<p>Your information is stored by the Companies on controlled servers with limited access and may be stored and processed in the United States or any other country where the Companies or agents are located. If you are located outside the United States and choose to provide your PII to us, we may transfer your PII to the United States and process it there. Those who choose to access and use the Service from outside the U.S. do so on their own initiative, at their own risk, and are responsible for compliance with applicable laws.</p>
			
			<br>
			<h2>Notice for California Users and Residents</h2>
			<p>Under California Civil Code Section 1789.3, California users are entitled to the following specific consumer rights notice: If you have a question or complaint regarding the Website, please send an email to admin@medncures.org. You may also contact us by writing to MednCures., LLC, PO Box 38, Bodega Bay, CA 94923. California residents may contact the Complaint Assistance Unit of the Division of Consumer Services of the California Department of Consumer Affairs in writing at 400 R Street, Suite 1080, Sacramento, California 95814, or by telephone at (916) 445-1254 or (800) 952-5210.</p>
			<p>Under California Civil Code Sections 1798.83-1798.84, California residents are entitled to ask for a notice identifying the categories of PII which we share with certain third parties for direct marketing purposes under certain circumstances and providing contact information for such third parties. If you are a California resident and would like a copy of this notice, please submit a written request to:</p>
			<p>MednCures., LLC</p>
			<p>PO Box 38, Bodega Bay, CA 94923</p>
			
			<br>
			<h2>Questions or Suggestions</h2>
			<p>If you have questions or concerns about our collection, use, or disclosure of your PII, please email us at admin@medncures.com.</p>
			
			
			
			</div>
		</div>
	</div>
</div>


<div class="modal fade" id="hippaPolicypp" role="dialog">
	<div class="modal-dialog multi-select-modal navyblue modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close fa fa-close" data-dismiss="modal"></button>
				<div class="journey">
					<h2>HIPPA Statement</h2>
				</div>
			</div>
			<div class="popmake-content"><h2>Notice of MednCures. Privacy Practices</h2>
		
			<p>Effective Date: January 27, 2015</p>
			<p>THIS NOTICE DESCRIBES HOW MEDICAL INFORMATION ABOUT YOU MAY BE USED AND DISCLOSED, AND HOW YOU CAN OBTAIN ACCESS TO THIS INFORMATION. PLEASE REVIEW IT CAREFULLY.</p>
			<p>The Health Insurance Portability and Accountability Act of 1996 (HIPAA) requires us to ask each of our patients to acknowledge receipt of our Notice of Privacy Practices. The Notice is published on this page. You acknowledge receipt of this notice by accepting terms and conditions for joining MednCures.</p>
			<p>MednCures. affiliates, sites, locations and care providers will follow the terms of this joint notice. In addition, the entities, sites, locations and care providers may share medical information with each other for treatment, payment, or health care operations related to the ACE. This designation may be amended from time to time to add new covered entities that are under common control with MednCures.</p>
			
			
			<br>
			<h2>MednCures Responsibilities</h2>
			<p>Under the Health Insurance Portability and Accountability Act of 1996 (HIPAA), MednCures must take steps to protect the privacy of your “Protected Health Information” (PHI). PHI includes information that we have created or received regarding your health or payment for your health care. It includes both your medical records and personal information such as your name, social security number, address, and phone number.</p>
			<p>Under federal law, we are required to:</p>
			<ul>
				<li>Protect the privacy of your PHI. All of our employees and physicians are required to maintain the confidentiality of PHI and receive appropriate privacy training</li>
				<li>Provide you with this Notice of Privacy Practices explaining our duties and practices regarding your PHI</li>
				<li>Follow the practices and procedures set forth in the Notice</li>
				<li>Uses and Disclosures of Your Protected Health Information That Do Not Require Your Authorization</li>
			</ul>
			
			<p>MednCures uses and discloses PHI in a number of ways connected to your treatment, payment for your care, and our health care operations. Some examples of how we may use or disclose your PHI without your authorization are listed below.</p>
			
			
			<br>
			<h2>TREATMENT</h2>
			<ul>
				<li>To our physicians, nurses, and others involved in your health care or preventive health care.</li>
				<li>To our different departments to coordinate such activities as prescriptions, lab work, and X-rays.</li>
				<li>To other health care providers treating you who are not on our staff such as dentists, emergency room staff, and specialists. For example, if you are being treated for an injured knee we may share your PHI among your primary physician, the knee specialist, and your physical therapist so they can provide proper care.</li>
			</ul>
			
			<br>
			<h2>PAYMENT</h2>
			<ul>
				<li>To administer your health benefits policy or contract.</li>
				<li>To bill you for health care we provide.</li>
				<li>To pay others who provided care to you.</li>
				<li>To other organizations and providers for payment activities unless disclosure is prohibited by law.</li>
			</ul>
			
			<br>
			<h2>HEALTH CARE OPERATIONS</h2>
			<ul>
				<li>To administer and support our business activities or those of other health care organizations (as allowed by law) including providers and plans. For example, we may use your PHI to review and improve the care you receive and to provide training.</li>
				<li>To other individuals (such as consultants and attorneys) and organizations that help us with our business activities.</li>
			</ul>
			<p>(Note: If we share your PHI with other organizations for this purpose, they must agree to protect your privacy.)</p>
			
			
			<br>
			<h2>OTHER</h2>
			<p>We may use or disclose your Protected Health Information without your authorization for legal and/or governmental purposes in the following circumstances:</p>
			<ul>
				<li>Required by law – When we are required to do so by state and federal law, including workers’ compensation laws.</li>
				<li>Public health and safety – To an authorized public health authority or individual to:</li>
				<li>Protect public health and safety.</li>
				<li>Prevent or control disease, injury, or disability.</li>
				<li>Report vital statistics such as births or deaths.</li>
				<li>Investigate or track problems with prescription drugs and medical devices. (Food and Drug Administration.)</li>
				<li>Abuse or neglect – To government entities authorized to receive reports regarding abuse, neglect, or domestic violence.</li>
				<li>Oversight agencies – To health oversight agencies for certain activities such as audits, examinations, investigations, inspections, and licensures.</li>
				<li>Legal proceedings – In the course of any legal proceeding in response to an order of a court or administrative agency and, in certain cases, in response to a subpoena, discovery request, or other lawful process.</li>
				<li>Law enforcement – To law enforcement officials in limited circumstances for law enforcement purposes. For example disclosures may be made to identify or locate a suspect, witness, or missing person; to report a crime; or to provide information concerning victims of crimes.</li>
				<li>Military activity and national security – To the military and to authorized federal officials for national security and intelligence purposes or in connection with providing protective services to the President of the United States.</li>
				<li>We may also use or disclose your Protected Health Information without your authorization in the following miscellaneous circumstances:</li>
				<li>Family and friends—To a member of your family, a relative, a close friend—or any other person you identify who is directly involved in your health care—when you are either not present or unable to make a health care decision for yourself and we determine that disclosure is in your best interest. For example, we may disclose PHI to a friend who brings you into an emergency room.</li>
				<li>All of this information except religious affiliation will be disclosed to people who ask for you by name. Members of the clergy will be told your religious affiliation if they ask. This is to help your family, friends, and clergy visit you in the facility and generally know how you are doing.</li>
				<li>Treatment alternatives and plan description—To communicate with you about treatment services, options, or alternatives, as well as health-related benefits or services that may be of interest to you, or to describe our health plan and providers to you.</li>
				<li>De-identify information—If information is removed from your PHI so that you can’t be identified, as authorized by law.</li>
				<li>Coroners, funeral directors, and organ donation—To coroners, funeral directors, and organ donation organizations as authorized by law.</li>
				<li>Disaster relief—To an authorized public or private entity for disaster relief purposes. For example, we might disclose your PHI to help notify family members of your location or general condition.</li>
				<li>Threat to health or safety—To avoid a serious threat to the health or safety of yourself and others.</li>
				<li>Correctional facilities—If you are an inmate in a correctional facility we may disclose your PHI to the correctional facility for certain purposes, such as providing health care to you or protecting your health and safety or that of others.</li>
			</ul>
			<p>Uses and Disclosures of Your Protected Health Information That Require Us to Obtain Your Authorization Except in the situations listed in the sections above, we will use and disclose your PHI only with your written authorization. This means we will not use your Protected Health Information in the following cases, unless you give us written permission:</p>
			<ul>
				<li>Marketing Purposes</li>
				<li>Sale of your information</li>
				<li>Most sharing of psychotherapy notes</li>
			</ul>
			<p>In some situations, federal and state laws provide special protections for specific kinds of PHI and require authorization from you before we can disclose that specially protected PHI. In these situations, we will contact you for the necessary authorization. In some situations, you may revoke your authorization; instructions regarding how to do so are contained in the form authorization you obtain from us. If you have questions about these laws, please contact the Privacy Officer at 510-604-9550.</p>
			
			
			<br>
			<h2>Your Rights Regarding Your Protected Health Information</h2>
			<p>You have the right to:</p>
			<ul>
				<li>Request restrictions by asking that we limit the way we use or disclose your PHI for treatment, payment, or health care operations. You may also ask that we limit the information we give to someone who is involved in your care, such as a family or friend. Please note that we are not required to agree to your request except when a restriction has been requested regarding a disclosure to a health plan in situations where the patient has paid for services in full and where the purpose of the disclosure is for payment or health care operations. If we do agree, we will honor your limits unless it is an emergency situation.</li>
				<li>Ask that we communicate with you by another means. For example, if you want us to communicate with you at a different address we can usually accommodate that request. We may ask that you make your request to us in writing. We will agree to reasonable requests.</li>
				<li>Request an electronic or paper copy of your PHI. We may ask you to make this request in writing and we may charge a reasonable fee for the cost of producing and mailing the copies, which you will receive usually within 30 days. In certain situations we may deny your request and will tell you why we are denying it. In some cases you may have the right to ask for a review of our denial.</li>
				<li>Ask usually to amend PHI about you that we use to make decisions about you. Your request for an amendment must be in writing and provide the reason for your request. In certain cases we may deny your request, in writing. You may respond by filing a written statement of disagreement with us and ask that the statement be included with your PHI.</li>
				<li>Seek an accounting of certain disclosures by asking us for a list of the times we have disclosed your PHI. Your request must be in writing and give us the specific information we need in order to respond to your request. You may request disclosures made up to six years before your request. You may receive one list per year at no charge. If you request another list during the same year, we may charge you a reasonable fee. These lists will not include disclosures to other organizations that might pay for your care provided by MednCures.</li>
				<li>Request a paper copy of this Notice.</li>
				<li>Receive written notification of any breach of your unsecured PHI.</li>
				<li>File a complaint if you believe your privacy rights have been violated. You can file a written complaint with us at the address below, or with the U.S. Department of Health and Human Services Office for Civil Rights by sending a letter to 200 Independence Avenue, S.W., Washington, D.C. 20201, calling 1-877-696-6775, or visiting <a href="http://www.hhs.gov/ocr/privacy/hipaa/complaints">www.hhs.gov/ocr/privacy/hipaa/complaints.</a></li>
			</ul>
			
			
			<br>
			<h2>Email</h2>
			<p>By utilizing our services or replying to our emails, you acknowledge that you are aware that email is not a secure method of communication, and that you agree to the risks. If you would prefer not to exchange personal health information via email, please notify us at admin@medncures.com.</p>
			
			
			<br>
			<h2>Changes to Privacy Practices</h2>
			<p>MednCures may change the terms of this Notice at any time. The revised Notice would apply to all PHI that we maintain. We will make any such changes to our website.</p>
			
			<br>
			<h2>Questions and Complaints</h2>
			<p>If you have any questions about this Notice or would like an additional copy, please contact the Privacy Officer at 510-604-9550 or at admin@medncures.com.</p>
			<p>If you think that we may have violated your privacy rights or you disagree with a decision we made about access to your PHI, you may send a written complaint to the Privacy Officer at PO Box 38, Bodega Bay, CA 94923.</p>

		
		
		

			</div>
		</div>
	</div>
</div>




<div class="modal fade" id="termsServicepp" role="dialog">
	<div class="modal-dialog multi-select-modal navyblue modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close fa fa-close" data-dismiss="modal"></button>
				<div class="journey">
					<h2>Terms of Service</h2>
				</div>
			</div>
			<div class="popmake-content">
			
			<p>Welcome to MednCures. MednCures, and its affiliated entities (collectively, the “Companies,” “we” or “us“) offer a service that allows Users (defined below) to manage their health using online-enabled technology, enhanced digital health tools and other services (the “Service“) through its online platform located at www.medncures.com (the “Website“) and mobile device application (“App“). Please read these Terms of Service (these “Terms“), the MednCures. Privacy Policy (the “Privacy Policy“), the MednCures. HIPAA Policy (the “HIPAA Policy“), the Website Terms of Use and any notices on the Website carefully because they govern your use of the Service. For purposes of these Terms, the term “User” includes paid subscribers and non-subscribing users of the Service.</p>
			<p>By using the Service, you agree to be bound by these Terms. If you object to anything in these Terms, do not use the Service.</p>
			
			
			<br>
			<h2>Acceptance of Terms of Service</h2>
			<p>a. Electronic Agreement. These Terms are an electronic contract that set out the legally binding terms of your use of and subscription to the Service. By using the Service (including by clicking the “I accept” button associated with these Terms), you accept these Terms and agree to the terms, conditions, and notices contained or referenced herein. They may be modified by the Companies from time to time, at their sole discretion, with such modifications to be effective upon posting by the Companies on the Website or via the Service or providing you with notice of the modification. It is important that you review such modifications because if you continue to use the Service after we have posted the modifications, you indicate that you agree to be bound by the modified Terms.</p>
			<p>b. Consent to Electronic Disclosures. You agree that the Companies may send the following to you by email or posting them on the Website: these Terms, including legal disclosures; future changes to these Terms; MednCures’s Privacy Policy; MednCures’s HIPAA Policy; and other notices, legal communications or disclosures and information related to the Service (the “Communications”). You consent to receive these Communications electronically. You agree to notify us promptly if your email address changes. Your consent to conduct actions electronically and our agreement to do so covers all actions you conduct through the Service.</p>
			<p>c. Withdrawing Your Consent. If you later decide that you do not want to receive future Communications electronically, please send an email to admin@medncures.com or a letter to MednCures, PO Box 38, Bodega Bay, CA 94923. Your withdrawal of consent shall be effective within a reasonable time after we receive your withdrawal notice described above. Your withdrawal of consent will not affect the legal validity or enforceability of the Terms provided to, and electronically signed by, you prior to the effective date of your withdrawal. However, if you withdraw your consent to receive Communications electronically, we may terminate your use of the Service.</p>
			
			
			
			<br>
			<h2>Eligibility</h2>
			<p>You represent and warrant that you are at least 18 years of age and have the right, authority and capacity to enter into these Terms and to abide by all of the terms and conditions of these Terms.</p>
			
			
			
			<br>
			<h2>Account Registration and Security</h2>
			<p>If you want to use certain features of the Service, you will have to create a User account via the Website or the App. It is important that you provide us with accurate, complete and up-to-date information for your account and you agree to update such information, as needed, to keep it accurate, complete and up-to-date. You are also responsible for maintaining the confidentiality of the username and password that you designate during the registration process for your account, and you are fully responsible for all activities that occur under your account. You agree to (a) immediately notify the Companies of any unauthorized use of your account or any other breach of security, and (b) ensure that you exit from your account at the end of each session. If you do not, we might have to suspend or terminate your account. The Companies will not be liable for any loss or damage arising from your failure to comply with this provision. You should use particular caution when accessing your account from a public or shared computer so that others are not able to view or record your password or other personal information.</p>
			
			
			
			<br>
			<h2>Acknowledgement of Annual Membership Fee</h2>
			<p>The Annual Membership Fee is charged by MednCures, a healthcare technology and management services company affiliated with MednCures, which develops enhanced digital health tools and other services. The Annual Membership Fee covers costs associated with access to the MednCures proprietary technology platform, and other higher touch and value-added non-medical services including lifestyle and wellness offerings, mobile data and technology services, and value-added personal services.</p>
			<p>Payment of the Annual Membership Fee is not a requirement to receiving medical care through MednCures clinics and there are options for accessing medical services with MednCures without payment of this fee. To learn more about the Annual Membership Fee and these options, contact us at admin@medncures.com.</p>
			<p>The Annual Membership Fee is not a covered benefit under most health care benefit plans. As a result, you acknowledge that you will not be able to submit the Annual Membership Fee for coverage under your benefit plan, and as such, you will be responsible for the cost of such Annual Membership Fee.</p>
			<p>You acknowledge and agree that we may contact you via email, phone, or mailing regarding your membership with us or your annual membership fee.</p>
			
			
			
			<br>
			<h2>Term and Termination</h2>
			<p>These Terms will remain in full force and effect while you are a User. You may terminate your use of the Service at any time by contacting us at Attn: Customer Care, MednCures, PO Box 38, Bodega Bay, CA 94923 or via email at admin@medncures.com. If you terminate your paid subscription, your subscription will remain active until the end of your then-current subscription period (that is, the subscription period through which you had signed up prior to your termination). The Companies in their sole discretion may terminate your use of the Service at any time, including, but not limited to, if you breach these Terms, by sending notice to you at the email address you provide via the Service. If the Companies terminate your use of the Service because you have breached these Terms, you will not be entitled to any refund of paid but unused subscription fees. All decisions regarding the termination of accounts shall be made in the sole discretion of the Companies. The Companies are not required to provide you with notice prior to terminating your use of the Service and are not required, and may be prohibited, from disclosing a reason for the termination of your account. Even after termination, these Terms will remain in effect such that all terms that by their nature may survive termination shall be deemed to survive such termination, including, without limitation, ownership provisions, warranty disclaimers, limitations of liability, and dispute resolution provisions. In addition, you understand and acknowledge that following termination of your account, the Companies may be required to, and will, maintain electronic copies of your medical records in accordance with applicable law.</p>
			
			
			
			
			<br>
			<h2>Non-commercial User of the Service</h2>
			<p>The Service is for the personal use of individual Users only and may not be used in connection with any commercial endeavors. Organizations, companies, and/or businesses are not permitted to, and should not use the Service. Illegal and/or unauthorized uses of the Service, including collecting usernames and/or email addresses of Users by electronic or other means for the purpose of sending unsolicited email and unauthorized framing of or linking to the Website, App or Service, may be investigated, and appropriate legal action will be taken, including without limitation, civil, criminal, and injunctive redress. Use of the Service is with the permission of the Companies, which may be revoked at any time, for any reason, in the Companies’ sole discretion.</p>
			
			
			<br>
			<h2>Feedback</h2>
			<p>We welcome feedback, comments and suggestions for improvements to the Service (“Feedback”). You can submit Feedback by emailing us at feedback@medncures.com. You grant to us a non-exclusive, worldwide, perpetual, irrevocable, fully-paid, royalty-free, sub licensable and transferable license under any and all intellectual property rights that you own or control to use, copy, modify, create derivative works based upon and otherwise exploit the Feedback for any purpose.</p>
			
			
			
			<br>
			<h2>Content and Software on the Service</h2>
			<p>a. Proprietary Rights; License. MednCures or its licensors exclusively own and retain all proprietary rights, title and interest in the Website, the App and the Service, including the Software (defined below) and all associated intellectual property rights contained therein. The Website, the App and the Service contain the copyrighted material, trademarks, and other proprietary information of MednCures, and its licensors. Subject to the terms and conditions of these Terms, MednCures grants to you a personal, limited, non-exclusive, nontransferable, non-sub licensable license to (i) download and install a copy of the software and technology that enable the App, together with any updates and bug fixes (the “Software”) on a mobile device and (ii) access and use the graphics, audio, messages, photos or profiles and works of authorship of any kind that MednCures provides or makes available via the Service (the “Content”) during the term of these Terms solely in connection with your permitted use of the Website, the App and the Service and solely for your personal and non-commercial purposes. MednCures and its licensors retain all rights not expressly granted to you in these Terms. Content includes without limitation User Content (defined below).</p>
			<p>b. Restrictions. You shall not, and you shall not enable any third party, to (i) access or attempt to access any other systems, programs or data of the Companies that are not available for public use; (ii) copy, reproduce, republish, upload, post, transmit or distribute any Content that is not User Content; (iii) work around any technical limitations in the Software, or decompile, disassemble or otherwise reverse engineer the Software except as otherwise permitted by applicable law; (iv) perform or attempt to perform any actions that could interfere with the proper operation of the Software or Service, prevent access to or use of the Software or Service by the Companies’ other licensees or clients, or impose an unreasonable or disproportionately large load on the infrastructure; (v) remove, alter or obscure any copyright, trademark, service mark or other proprietary rights notices incorporated in or accompanying the Service (including the Software); or (vi) otherwise use the Software except as expressly allowed under this Section.</p>
			<p>c. Reliance on Content, Advice, Etc. Opinions, advice, statements, offers, or other information or Content made available through the Service, are those of their respective authors, and should not necessarily be relied upon. Such authors are solely responsible for such Content. The Companies do not: (i) guarantee the accuracy, completeness, or usefulness of any third-party Content on the Service, or (ii) adopt, endorse or accept responsibility for the accuracy or reliability of any opinion, advice, or statement made by any party that appears on the Service. Under no circumstances will the Companies or their affiliates be responsible for any loss or damage resulting from your reliance on third-party information or other Content posted on the Website or the App or transmitted to or by any Users via the Service.</p>
			
			
			
			<br>
			<h2>Content Posted by You to the Service</h2>
			<p>We may, in our sole discretion, permit Users to publicly post, upload, publish or transmit Content through the Service (the “User Content”). We will have no obligations with respect to User Content. By making any User Content available through the Service, you hereby grant us a non-exclusive, transferable, sub licensable, worldwide, royalty-free license to use, copy, disclose, distribute, incorporate, modify, publicly display, publicly perform and create derivative works based upon your User Content and all data, images, sounds, text, and other things embodied therein only in connection with operating and providing the Service to you. You are solely responsible for all your User Content. You represent and warrant that you own all your User Content or you have all rights that are necessary to grant us the license rights in your User Content under these Terms. You also represent and warrant that neither your User Content, nor your use and provision of your User Content to be made available through the Service, nor any use of your User Content by us on or through the Service will infringe, misappropriate or violate a third party’s intellectual property rights, or rights of publicity or privacy, or result in the violation of any applicable law or regulation. The Companies do not claim any ownership rights in any User Content and nothing in these Terms will be deemed to restrict any rights that you may have to use and exploit your User Content. You will not provide inaccurate, misleading or false User Content to the Companies via the Service. If the User Content you provided to the Companies subsequently becomes inaccurate, misleading or false, you will promptly notify the Companies of such change.</p>
			<p>a. You understand and agree that the Companies reserve the right to review and delete any User Content, in each case in whole or in part, for any reason including if, in the sole judgment of the Companies, the User Content (i) violates these Terms or might be fraudulent, false, misleading, deceptive, defamatory, discriminatory, offensive, illegal; or (ii) might violate, or encourage any conduct that would violate, any applicable law, regulation or the rights of others, or cause harm or threaten the safety of Users or others. The Companies have the right to investigate violations of these Terms or conduct that affects the Service. We may also consult and cooperate with law enforcement authorities to prosecute Users who violate the law.</p>
			<p>b. You can remove your User Content by specifically deleting it. However, in certain instances, some of your User Consent (such as posts or comments you make) may not be completely removed and copies of your User Content may continue to exist in the Service. We are not responsible or liable for the removal or deletion or for the failure to remove or delete any of your User Content.</p>
			<p>c. Your use of the Service must be in accordance with any and all applicable laws and regulations.</p>
			<p>d. You may not engage in advertising to, or solicitation of, other Users. Although the Companies cannot monitor the conduct of its Users, it is also a violation of these Terms to use any information obtained from the Service in order to harass, abuse, or harm another person, or in order to contact, advertise to, solicit, or sell to any User without the User’s prior explicit consent.</p>
			
			
			<br>
			<h2>Prohibited Activities</h2>
			<p>The following is a partial list of the type of actions that you may not engage in with respect to the Service:</p>
			<p>You will not post, distribute, or reproduce in any way any User Content that infringes third party intellectual property rights or violates third party rights of privacy or rights of publicity;</p>
			<p>You will not express or imply that any statements you make are endorsed by the Companies;</p>
			<p>You will not use any robot, spider, site search/retrieval application, or other manual or automatic device or process to download, access, retrieve, index, “data mine”, or in any way reproduce or circumvent, avoid, bypass, remove, or deactivate the navigational structure or technical measures or presentation of the Service or its contents;</p>
			<p>You will not interfere, access, tamper with or disrupt the Service, Website or App or the servers or networks connected to the Service, Website, or App;</p>
			<p>You will not attempt to probe, scan or test the vulnerability of any MednCures or MednCures system or network or breach any security or authentication measures;</p>
			<p>You will not send any unsolicited or unauthorized advertising, promotional materials, email, junk mail, spam, chain letters or other form of solicitation;</p>
			<p>You will not use any meta tags or other hidden text or metadata utilizing the Companies’ trademarks, logos, URLs or product names without the Companies’ express written consent;</p>
			<p>You will not use the Service, Website, App or Content, or any portion thereof, for any commercial purpose or for the benefit of any third party or in any manner not permitted by these Terms;</p>
			<p>You will not post, email or otherwise transmit any material that contains software viruses or any other computer code, files or programs designed to interrupt, destroy or limit the functionality of any computer software or hardware or telecommunications equipment;</p>
			<p>You will not forge headers or otherwise manipulate identifiers in order to disguise the origin of any information transmitted through the Service;</p>
			<p>You will not use, display, “frame” or “mirror” any part of the Service, the Companies’ names, any Companies’ trademarks, logos or other proprietary information, or the layout and design of any page or form contained on a page, without prior written authorization from the Companies. You also shall not use meta tags or code or other devices containing any reference to the Companies or the Service in order to direct any person to any other web site for any purpose; and</p>
			<p>You will not collect or store any personally identifiable information from Users without their express permission.</p>
			
			
			
			<br>
			<h2>Subscriptions; Charges on Your Billing Account</h2>
			<p>a. General; Annual Membership Fee. For our paid subscribers, payments of the Annual Membership Fee will be charged to your online account that you establish for use of the Service (your “Billing Account”). You agree to pay the Companies all charges at the prices then in effect for any use of the Service by you or other persons (including your agents) using your Billing Account, and you authorize the Companies to charge your chosen payment provider (your “Payment Method”) for the Service. You agree to make payment using that selected Payment Method. The Companies reserve the right to correct any errors or mistakes that they make even if they have already requested or received payment.</p>
			<p>b. Recurring Billing. Most subscription plans to the Service consist of an initial period, for which there is a one-time charge, followed by recurring period charges as agreed to by you. By entering into these Terms, you acknowledge that your subscription has an initial and recurring payment feature and you accept responsibility for all recurring charges prior to cancellation or termination. THE COMPANIES MAY MAKE PERIODIC CHARGES (E.G., ANNUALLY) WITHOUT FURTHER AUTHORIZATION FROM YOU, UNTIL YOU PROVIDE PRIOR NOTICE (CONFIRMED IN WRITING UPON REQUEST BY THE COMPANIES) THAT YOU HAVE TERMINATED THIS AUTHORIZATION OR WISH TO CHANGE YOUR PAYMENT METHOD. SUCH NOTICE WILL NOT AFFECT CHARGES SUBMITTED TO YOU FOR MEDICAL SERVICES PERFORMED BEFORE THE COMPANIES REASONABLY COULD ACT AFTER RECEIPT OF SUCH NOTICE. TO TERMINATE YOUR AUTHORIZATION OR CHANGE YOUR PAYMENT METHOD, PLEASE CONTACT THE COMPANIES AT ADMIN@medncures.com.</p>
			<p>c. Current Information Required. YOU MUST PROVIDE CURRENT, COMPLETE AND ACCURATE INFORMATION FOR YOUR BILLING ACCOUNT. YOU MUST PROMPTLY UPDATE ALL INFORMATION TO KEEP YOUR BILLING ACCOUNT CURRENT, COMPLETE AND ACCURATE (SUCH AS A CHANGE IN BILLING ADDRESS, CREDIT CARD NUMBER, OR CREDIT CARD EXPIRATION DATE), AND YOU MUST PROMPTLY NOTIFY THE COMPANIES IF YOUR PAYMENT METHOD IS CANCELED (E.G., FOR LOSS OR THEFT) OR IF YOU BECOME AWARE OF A POTENTIAL BREACH OF SECURITY, SUCH AS THE UNAUTHORIZED DISCLOSURE OR USE OF YOUR USER NAME OR PASSWORD. CHANGES TO SUCH INFORMATION CAN BE MADE THROUGH YOUR ACCOUNT SETTINGS. IF YOU FAIL TO PROVIDE THE COMPANIES ANY OF THE FOREGOING INFORMATION, YOU AGREE THAT THE COMPANIES MAY CONTINUE CHARGING YOU FOR ANY USE OF THE SERVICE UNDER YOUR BILLING ACCOUNT UNLESS YOU HAVE TERMINATED YOUR SUBSCRIPTION FOR THE SERVICE (CONFIRMED BY YOU IN WRITING UPON REQUEST BY THE COMPANIES).</p>
			<p>d. Payment Method. If the Companies do not receive payment from the third-party service provider who manages your Payment Method (“Payment Method Provider”), you agree to pay all amounts due on your Billing Account upon demand. You agree that your Payment Method Provider may impose terms and conditions on you, which are independent of these Terms and you agree to comply with all of those terms.</p>
			<p>e. Change in Amount Authorized. If the amount to be charged to your Billing Account varies from the amount you preauthorized (other than due to the imposition or change in the amount of state sales taxes), you have the right to receive, and we shall provide, notice of the amount to be charged and the date of the charge at least 10 days before the scheduled date of the transaction. If you do not agree with the new amount in the notice, you will have an opportunity to cancel the transaction by contacting the Companies at admin@medncures.com. As noted above, any agreement you have with your Payment Method Provider will govern your use of your Payment Method. You agree that the Companies may accumulate charges incurred and submit them as one or more aggregate charges during or at the end of each billing cycle.</p>
			<p>f. Auto-renewal. Your paid subscription will be automatically extended for successive renewal periods of the same duration as the subscription term originally selected, at the then-current non-promotional subscription rate. If you cancel your subscription, you may use your subscription until the end of your then-current subscription term and your subscription will not be renewed after your then-current term expires. However, you won’t be eligible for a prorated refund of any portion of the subscription fee paid for the then-current subscription period.</p>
			<p>g. Reaffirmation of Authorization. Your non-termination or continued use of the paid subscription features of the Service reaffirms that the Companies are authorized to charge your Payment Method. The Companies may submit those charges for payment and you will be responsible for such charges. This does not waive the Companies’ right to seek payment directly from you. Your charges may be payable in advance, in arrears, per usage, or as otherwise described when you initially subscribed to the Service.</p>
			<p>h. Free Trials and Other Promotions. Any free trial or other promotion that provides paid subscriber-level access to the Service must be used within the specified time of the trial. If you do not wish to continue your membership, you must cancel your trial subscription before the end of the trial period in order to avoid being charged a subscription fee. If you cancel prior to the end of the trial period and are inadvertently charged for a subscription, please contact Customer Care at admin@medncures.com.</p>
			
			
			<br>
			<h2>Appointments: Missed/Late Cancellation</h2>
			<p>You understand and agree that if you do not show for your appointment or you cancel your appointment with less than 24 hours’ notice, the Companies may charge you a fee for a missed/late cancelled appointment.</p>
			<p>Modifications to Service</p>
			<p>The Companies reserve the right at any time to modify or discontinue, temporarily or permanently, the Service (or any part thereof) with or without notice. You agree that the Companies shall not be liable to you or to any third party for any modification, suspension or discontinuance of the Service.</p>
			<p>Blocking of IP Addresses</p>
			<p>In order to protect the integrity of the Service, the Companies reserve the right at any time in their sole discretion to block Users from certain IP addresses from accessing the Service.</p>
			<p>Copyright Policy</p>
			<p>The Companies respect copyright law and expect their Users to do the same. It is the Companies’ policy to terminate in appropriate circumstances Users who repeatedly infringe or are believed to be repeatedly infringing the rights of copyright holders. Please see the Companies’ Copyright Policy in our Website Terms of Use.</p>
			<p>Privacy</p>
			<p>For information on how we collect, use and disclosure information from our Users, please refer to our Privacy Policy and our HIPAA Policy, which are incorporated by reference as though fully set forth herein.</p>
			<p>Disclaimers</p>
			<p>The Companies are not responsible for any incorrect or inaccurate Content posted on the Service, including User Content. The Companies are not responsible for the conduct, whether online or offline, of any User. The Companies assume no responsibility for any error, omission, interruption, deletion, defect, delay in operation or transmission, communications line failure, theft or destruction or unauthorized access to, or alteration of any Communication. The Companies are not responsible for any problems or technical malfunction of any telephone network or lines, computer online systems, servers or providers, computer equipment, software, failure of email or players on account of technical problems or traffic congestion on the Internet or at any website or combination thereof, including injury or damage to Users’ or to any other person’s computer related to or resulting from participating or downloading materials in connection with the Service. Under no circumstances will the Companies or any of their affiliates, advertisers, promoters or distribution partners be responsible for any loss or damage, including personal injury or death, resulting from anyone’s use of the Service, any Content posted on the Service or transmitted to Users, or any interactions between Users of the Service, whether online or offline. THE SERVICE IS PROVIDED “AS-IS” AND THE COMPANIES EXPRESSLY DISCLAIM ANY WARRANTY OF FITNESS FOR A PARTICULAR PURPOSE OR NON-INFRINGEMENT. The Companies cannot guarantee and do not promise any specific results from use of the Service.</p>
			<p>In addition to the preceding paragraph and other provisions of these Terms, any general advice that may be posted on the Website, the App or the Service is for informational purposes only and is not intended to replace or substitute for any medical or other advice. The Companies make no representations or warranties and expressly disclaim any and all liability concerning any treatment, action by, or effect on any person following the general information offered or provided within or through the Website, the App or the Service. If you have specific concerns or a situation arises in which you require medical advice, you should consult with an appropriately trained and qualified medical services provider.</p>
			<p>The Companies make no representation that the Service is appropriate, or is available for use outside the U.S. Access to and use of the Service where the access or use of the Service is illegal is prohibited. Those who choose to access and use the Service from outside the U.S. do so on their own initiative, at their own risk, and are responsible for compliance with applicable laws.</p>
			
			
			
			<br>
			<h2>Permission to Treat/Telehealth Medical Treatment</h2>
			<p>By clicking “Accept” during the account registration process, you give permission to the health care providers of MednCures to medically care for you in person, and, if applicable, by Telehealth (defined below), as applicable and appropriate. You may withdraw this consent at any time by both terminating your account and no longer seeking care from MednCures.</p>
			<p>You understand that part of the subscription Service requires release of your personal health information, including test results, to an online personal health record (in accordance with our HIPAA Policy) and communication with the MednCures healthcare team electronically via the Service or the App. You consent to receive emails or other communications from us pertaining to your care and your health (such as automated health reminders).</p>
			<p>You understand that virtual encounters with MednCures or our Telehealth services via phone, email, the App or otherwise, could involve, and you hereby consent to the use of, automated tools for diagnosis, care, treatment or communication pertaining to healthcare matters. You also acknowledge that such virtual encounters with MednCures may be provided by a variety of healthcare providers, including physicians, Registered Nurses, Nurse Practitioners, phlebotomists, and other support or medical personnel.</p>
			<p>This Website, the App and services are NOT appropriate for emergency or urgent medical matters. You agree that for all urgent or emergency matters that you believe may immediately affect your health, you will immediately contact your medical services provider, call 911 or go to an emergency room at a local hospital facility.</p>
			
			
			<br>
			<h2>Complementary Services Disclaimer</h2>
			<p>You understand that methods of treatment for MednCures’s Complementary and Alternative Medicine (“CAM”) services may include, but are not limited to: Cannabis therapy, Chinese herbal medicine, and nutritional counseling,</p>
			<p>Nutrition consultation services are not licensed by most states in the U.S. The methods of evaluation employed, which may include diet, supplementation, and assessment analysis, are not intended to diagnose disease.</p>
			<p>Herbs and nutritional supplements recommended are traditionally considered safe in the practice of Chinese Medicine, although some may be toxic in large doses. You acknowledge that you understand that some herbs may be inappropriate during pregnancy. Some possible side effects of taking herbs include, but are not limited to, nausea, gas, diarrhea, and hives. You acknowledge that you understand that the herbs should be consumed according to the instructions provided verbally and in writing. The herbs may have an unpleasant taste or smell.</p>
			<p>Cannabis is generally considered a safe herb with no risk of overdose. You acknowledge the existence of potential side effects, including but not limited to psychoactivity, anxiety, nausea, paranoia, dysphoria, or dizziness.</p>
			<p>You acknowledge that you understand that while this section describes the major risks of treatment, other side effects and risks may occur.</p>
			<p>You will immediately notify the provider who is caring for you if you are or become pregnant, as this may affect your treatment protocol. You will consult with a medical physician to be screened for any predispositions to injuries and risks in regard to any of the CAM treatments.</p>
			<p>You will consult with a medical physician to address serious health concerns and to avoid any contraindications.</p>
			<p>You understand that the Complementary and Alternative Medicine modalities are not a substitute for allopathic medical care and the CAM providers cannot anticipate all possible risks and complications of treatment. You will immediately notify your provider of any unanticipated or unpleasant effects associated with any of your CAM treatments.</p>
			<p>You hereby give consent to any of the CAM providers at MednCures to assess and care for your present condition and any other future conditions for which you seek attention via the Service.</p>
			
			
			
			<br>
			<h2>Links and Third-Party Applications</h2>
			<p>The Service may provide, or third parties may provide, links to other sites or other online resources. In addition, our Website or App may also integrate third-party applications, products or services (the “Third-Party Applications”), which may have access to your data and protected health information (“PHI”). These third parties are not affiliated with us and may have different policies with respect to handling your data and PHI, and use of such Third-Party Applications is subject to their applicable terms and conditions and privacy policies. We are not responsible for and do not make any representations or warranties with respect to any third-party technology or Third-Party Applications, or the actions or inactions of such third parties, accessible via the App, Service or the Website. Any purchase by you of any Third-Party Applications or any exchange of data or other interaction between you and a provider of a Third Party Application is solely between you and the applicable third-party service provider.</p>
			<p>Because the Companies have no control over such Third-Party Applications and third-party sites and resources, you acknowledge and agree that the Companies are not responsible for the availability, quality, accuracy, integrity, safety, reliability or legality of such external sites or resources, and do not endorse and are not responsible or liable for any content, advertising, products or other materials on or available from such Third-Party Applications, sites or resources. You further acknowledge and agree that the Companies shall not be responsible or liable, directly or indirectly, for any damage, harm, injury or loss of any kind caused or alleged to be caused by or in connection with the purchase, use of, download, installation or reliance upon, any such content, goods or services available on or through any such site,</p>
			
			
			
			<br>
			<h2>Third-Party Application or resource.</h2>
			<p>We do not endorse and are not responsible for the information and security practices or privacy policies of Third-Party Applications or web sites operated by others that may be linked to or from the Service, Website or App. No web sites that link to the Service may frame or otherwise incorporate the Content on the Service.</p>
			<p>Limitation on Liability</p>
			<p>TO THE EXTENT PERMITTED BY APPLICABLE LAW, IN NO EVENT WILL THE COMPANIES (OR THEIR EMPLOYEES, OFFICERS, DIRECTORS OR AGENTS) BE LIABLE TO YOU FOR ANY CONSEQUENTIAL, EXEMPLARY, INCIDENTAL, SPECIAL OR PUNITIVE DAMAGES, INCLUDING, WITHOUT LIMITATION, THOSE RELATING TO LOST PROFITS, LOSS OF DATA OR LOSS OF GOODWILL, SERVICE INTERRUPTION, COMPUTER DAMAGE OR SYSTEM FAILURE, THE COST OF SUBSTITUTE PRODUCTS OR SERVICES, BODILY INJURY OR DEATH, ARISING OUT OF OR IN CONNECTION WITH THESE TERMS OR FROM THE USE OF OR INABILITY TO USE THE WEBSITE, APP, SERVICE OR SOFTWARE, WHETHER BASED ON CONTRACT, WARRANTY, PRODUCT LIABILITY, TORT OR OTHER LEGAL THEORY AND EVEN IF THE COMPANIES HAVE BEEN INFORMED OF THE POSSIBILITY OF SUCH DAMAGES. TO THE EXTENT PERMITTED BY APPLICABLE LAW, NOTWITHSTANDING ANYTHING TO THE CONTRARY CONTAINED HEREIN, THE COMPANIES’ LIABILITY TO YOU ARISING FROM THESE TERMS, OR THE USE OF OR INABILITY TO USE THE WEBSITE, APP, SERVICE OR SOFTWARE, WILL AT ALL TIMES BE LIMITED TO THE GREATER OF USD $100.00 OR THE AMOUNTS PAID BY YOU TO THE COMPANIES FOR ACCESS TO AND USE OF THE SERVICE. SOME JURISDICTIONS DO NOT ALLOW THE EXCLUSION OR LIMITATION OF LIABILITY FOR CONSEQUENTIAL OR INCIDENTAL DAMAGES, SO THE ABOVE EXCLUSION MAY NOT APPLY TO YOU.</p>
			
			
			<br>
			<h2>U.S. Export Controls</h2>
			<p>You agree to comply fully with all U.S. and foreign export laws and regulations to ensure that neither the Software nor any technical data related thereto nor any direct product thereof is exported or re-exported directly or indirectly in violation of, or used for any purposes prohibited by, such laws and regulations. By using the Service, you represent and warrant that: (i) you are not located in a country that is subject to a U.S. Government embargo, or that has been designated by the U.S. Government as a “terrorist supporting” country; and (ii) you are not listed on any U.S. Government list of prohibited or restricted parties.</p>
			
			
			
			<br>
			<h2>Dispute Resolution</h2>
			<p>Governing Law</p>
			<p>These Terms and any action related thereto will be governed by the laws of the State of California without regard to its conflict of laws provisions.</p>
			<p>Agreement to Arbitrate</p>
			<p>You and the Companies agree that any dispute, claim or controversy arising out of or relating to these Terms or the breach, termination, enforcement, interpretation or validity thereof or the use of the Service (collectively, “Disputes”) will be settled by binding arbitration, except that each party retains the right: (i) to bring an individual action in small claims court, and (ii) to seek injunctive or other equitable relief in a court of competent jurisdiction to prevent the actual or threatened infringement, misappropriation or violation of a party’s copyrights, trademarks, trade secrets, patents or other intellectual property rights (the action described in the foregoing clause (ii), an (“IP Protection Action”). Without limiting the preceding sentence, you will also have the right to litigate any other Dispute if you provide the Companies with written notice of your desire to do so by email or regular mail at admin@medncures.com or MednCures, PO Box 38, Bodega Bay, CA 94923 within thirty (30) days following the date you first accept these Terms (such notice, an “Arbitration Opt-out Notice”). If you don’t provide the Companies with an Arbitration Opt-out Notice within the thirty (30) day period, you will be deemed to have knowingly and intentionally waived your right to litigate any Dispute except as expressly set forth in clauses (i) and (ii) above. The exclusive jurisdiction and venue of any IP Protection Action or, if you timely provide the Companies with an Arbitration Opt-out Notice, will be the state and federal courts located in the Northern District of California and each of the parties hereto waives any objection to jurisdiction and venue in such courts. Unless you timely provide the Companies with an Arbitration Opt-out Notice, you acknowledge and agree that you and the Companies are each waiving the right to a trial by jury or to participate as a plaintiff or class member in any purported class action or representative proceeding. Further, unless both you and the Companies otherwise agree in writing, the arbitrator may not consolidate more than one person’s claims, and may not otherwise preside over any form of any class or representative proceeding. If this specific paragraph is held unenforceable, then the entirety of this “Dispute Resolution” section will be deemed void. Except as provided in the preceding sentence, this “Dispute Resolution” section will survive any termination of these Terms.</p>
			<p>Arbitration Rules</p>
			<p>The arbitration will be administered by the American Arbitration Association (“AAA”) in accordance with the Commercial Arbitration Rules and the Supplementary Procedures for Consumer Related Disputes (the “AAA Rules”) then in effect, except as modified by this “Dispute Resolution” section. (The AAA Rules are available at www.adr.org/arb_med or by calling the AAA at 1-800-778-7879.) The Federal Arbitration Act will govern the interpretation and enforcement of this Section.</p>
			<p>Arbitration Process</p>
			<p>A party who desires to initiate arbitration must provide the other party with a written Demand for Arbitration as specified in the AAA Rules. AAA provides a general form for a Demand for Arbitration and a separate form for Demand for Arbitration for California residents. The arbitrator will be either a retired judge or an attorney licensed to practice law and will be selected by the parties from the AAA’s roster of arbitrators. If the parties are unable to agree upon an arbitrator within seven (7) days of delivery of the Demand for Arbitration, then the AAA will appoint the arbitrator in accordance with the AAA Rules.</p>
			<p>Arbitration Location and Procedure</p>
			<p>Unless you and the Companies otherwise agree, the arbitration will be conducted in the county where you reside. If your claim does not exceed $10,000, then the arbitration will be conducted solely on the basis of the documents that you and the Companies submit to the arbitrator, unless you request a hearing or the arbitrator determines that a hearing is necessary. If your claim exceeds $10,000, your right to a hearing will be determined by the AAA Rules. Subject to the AAA Rules, the arbitrator will have the discretion to direct a reasonable exchange of information by the parties, consistent with the expedited nature of the arbitration.</p>
			<p>Arbitrator’s Decision</p>
			<p>The arbitrator will render an award within the time frame specified in the AAA Rules. The arbitrator’s decision will include the essential findings and conclusions upon which the arbitrator based the award. Judgment on the arbitration award may be entered in any court having jurisdiction thereof. The arbitrator’s award of damages must be consistent with the terms of the “Limitation of Liability” section above as to the types and amounts of damages for which a party may be held liable. The arbitrator may award declaratory or injunctive relief only in favor of the claimant and only to the extent necessary to provide relief warranted by the claimant’s individual claim. If you prevail in arbitration you will be entitled to an award of attorneys’ fees and expenses, to the extent provided under applicable law. The Companies will not seek, and hereby waive all rights they may have under applicable law to recover, attorneys’ fees and expenses if they prevail in arbitration.</p>
			<p>Fees</p>
			<p>Your responsibility to pay any AAA filing, administrative and arbitrator fees will be solely as set forth in the AAA Rules. However, if your claim for damages does not exceed $75,000, the Companies will pay all such fees unless the arbitrator finds that either the substance of your claim or the relief sought in your Demand for Arbitration was frivolous or was brought for an improper purpose (as measured by the standards set forth in Federal Rule of Civil Procedure 11(b)).</p>
			<p>Changes</p>
			<p>Notwithstanding anything to the contrary in these Terms, if the Companies change this “Dispute Resolution” section after the date you first accepted these Terms (or accepted any subsequent changes to these Terms), you may reject any such change by sending us written notice (including by email to admin@medncures.com) within 30 days of the date such change became effective, as indicated in the “Last Updated” date above or in the date of the Companies’ email to you notifying you of such change. By rejecting any change, you are agreeing that you will arbitrate any Dispute between you and the Companies in accordance with the provisions of this “Dispute Resolution” section as of the date you first accepted these Terms (or accepted any subsequent changes to these Terms).</p>
			<p>Indemnity by You</p>
			<p>You agree to defend, indemnify and hold the Companies, their subsidiaries, affiliates, officers, agents, and other partners and employees, harmless from any loss, liability, claim, or demand, including, without limitation, reasonable attorney’s fees, made by any third party due to or arising out of your use of the Service in violation of these Terms and/or arising from a breach of these Terms and/or any breach of your representations and warranties set forth above.</p>
			<p>No Third-Party Beneficiaries</p>
			<p>You agree that, except as otherwise expressly provided in these Terms, there shall be no third-party beneficiaries to these Terms.</p>
			<p>Waiver and Severability</p>
			<p>The failure of the Companies to enforce any right or provision of these Terms will not constitute a waiver of future enforcement of that right or provision. The waiver of any such right or provision will be effective only if in writing and signed by a duly authorized representative of the Companies. Except as expressly set forth in these Terms, the exercise by any party of any of its remedies under these Terms will be without prejudice to its other remedies under these Terms or otherwise. If for any reason a court of competent jurisdiction or an arbitrator finds any provision of these Terms invalid or unenforceable, that provision will be enforced to the maximum extent permissible and the other provisions of these Terms will remain in full force and effect.</p>
			<p>Entire Agreement</p>
			<p>These Terms constitute the entire and exclusive understanding and agreement between the Companies and you regarding the Service, and these Terms supersede and replace any and all prior oral or written understandings or agreements between the Companies and you regarding the same.</p>
			<p>Assignment</p>
			<p>You may not assign or transfer these Terms, by operation of law or otherwise, without the Companies’ prior written consent. Any attempt by you to assign or transfer these Terms, without such consent, will be null and of no effect. The Companies may assign or transfer these Terms, at their sole discretion, without restriction. Subject to the foregoing, these Terms will bind and inure to the benefit of the parties, their successors and permitted assigns.</p>
			<p>Legal Disclosure</p>
			<p>The statements on these products have not been evaluated by FDA and the products are not intended to diagnose, treat, cure or prevent any disease or medical condition. Nor are the products intended to be rubbed, poured, or sprayed on, introduced into, or otherwise applied to the human body for cleansing, beautifying, promoting attractiveness, or altering the appearance. Rather, the products are meant to address energetic blockages that may have an impact on wellness and energetic balance, facilitating the body's natural ability to bring itself to homeostasis, which may have an impact on health and well-being.</p>
			<p>The contents of this website are for informational purposes only and do not render medical or psychological advice, opinion, diagnosis, or treatment. The information provided through this website should not be used for diagnosing or treating a health problem or disease. It is not a substitute for professional care. If you have or suspect you may have a medical or psychological problem, you should consult your appropriate health care provider. Never disregard professional medical advice or delay in seeking it because of something you have read on this website. Links on this website are provided only as an informational resource, and it should not be implied that we recommend, endorse or approve of any of the content at the linked sites, nor are we responsible for their availability, accuracy or content.</p>
			<p>Any review or other matter that could be regarded as a testimonial or endorsement does not constitute a guarantee, warranty, or prediction regarding the outcome of any consultation. The testimonials on this website represent the anecdotal experience of individual consumers. Individual experiences are not a substitute for scientific research.</p>
			<p>Notices</p>
			<p>Any notices or other communications permitted or required hereunder, including those regarding modifications to these Terms, will be in writing and given by the Companies (i) via email (in each case to the address that you have provided); or (ii) by posting to the Website or the App or via the Service. For notices made by e-mail, the date of receipt will be deemed the date on which such notice is transmitted.</p>
			<p>Contact Information</p>
			<p>If you have any questions about these Terms or the Service, please contact the Companies at admin@medncures.com.</p>
			<p>I HAVE READ THIS AGREEMENT AND AGREE TO ALL OF THE PROVISIONS CONTAINED ABOVE.</p>
			
			
			</div>
		</div>
	</div>
</div>
<?php }  // function ends here 
?>
