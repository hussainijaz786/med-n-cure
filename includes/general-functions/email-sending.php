<?php 

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly 

/*
*	For Get Password Reset Link
*	=> $password_reset_url = password_reset_link($user_id);
*/


//	Example Arguments for function


/*
	$args = array(
		'call_by' => 'post-registeration-physician',
		'receiver' => $user_email, 
		'subject_name' => 'first_name (optinal)', 
		'body_name' => 'Full Name (optional)', 
		'body_part' => '(optional)', 
		'password_reset_url' => $password_reset_url (optional),
	);
	cdrmed_send_email($args);
*/

function cdrmed_send_email($args) {
	
	//receiver email
	$to = $args['receiver'];
	//subject name if pass in args
	$subject_name = ($args['subject_name'] != '') ? $args['subject_name'] : '';
	$subject = $subject_name;
	
	// select email body base on calling condition
	/*
		send three parameters
		1 => Call by required
		2 => Name of receiver (optional)
		3 => Password reset link (optional)
	*/
	$body = '';
	if($args['call_by'] == 'patient-product-selection-dispensary'){
		$body = cdrmed_email_body($args['call_by'], $args['body_name'], $args['password_reset_url'], $args['full_name'], $args['product_title']);
	}
	else{
		$body = cdrmed_email_body($args['call_by'], $args['body_name'], $args['password_reset_url']);
	}
	$body_part = ($args['body_part'] != '') ? "\n".$args['body_part'] : '';
	
	$body .= '<br><br>For additional help, reach out to our support team. '."<br>".
	'<strong>Sincerely,</strong>'. "<br>".
	'MednCures Team '."<br>".
	'<img src="http://medncures/cdrmed-email-logo.png">';
	
	$headers = 'From: Customer Support <support@medncures.com>'. "\r\n";
	$headers .= "MIME-Version: 1.0\r\n";
	$headers .= "Content-type: text/html\r\n";
	
	
	
	return wp_mail($to, $subject, $body, $headers );
}


/*
*	Email Body Function
*
*/

function cdrmed_email_body($call_by, $body_name = '', $password_reset_url = '', $full_name = '', $product_title = ''){
	$body = '';
	if($call_by == 'pre-registeration-physician'){
		$body = "Thank you for requesting access to MednCures. We are verifying your information and building your instance of MednCures.  ";
	}
	elseif($call_by == 'post-registeration-physician'){
		//Call from admin-functions/add-new-physician.php
		$body = "Welcome to MednCures. We have built your domain and portal at:"."<br><br>".
		$body_name."<br><br>".
		"Please click this link to setup your password:   ".$password_reset_url;
	}
	elseif($call_by == 'pre-registeration-dispensary'){
		$body = "Thank you for requesting access to MednCures. We are verifying your information and building your instance of MednCures. ";
	}
	elseif($call_by == 'post-registeration-dispensary'){
		//Call from admin-functions/add-new-dispensary.php
		$body = 'Welcome to MednCures. We have built your domain and portal at:'. "<br><br>".
		$body_name."<br><br>".
		'Please click this link to setup your password:   '.$password_reset_url;
	}
	elseif($call_by == 'pre-registeration-patient'){
		//Call from patient-functions/frontend-forms/pre-registeration-form.php
		$body = 'You are one step closer to connecting to MednCures! '. "<br><br>".
			'A link to set your password will be sent you shortly once we have verified your patient information.';
	}
	elseif($call_by == 'pre-registeration-patient-physician'){
		//Call from patient-functions/frontend-forms/pre-registeration-form.php
		$body = "Patient <strong>".$body_name."</strong> Signed Up in Pre-Registeration  "."<br>".$password_reset_url;
	}
	elseif($call_by == 'post-registeration-patient'){
		//Call from physician-functions/view-pending-registeration.php
		$body = 'Welcome to MednCures! Please click this link to setup your password: '."<br><br>".
		$password_reset_url.'<br><br>
		Please be sure to complete your Health Intake questions so you can schedule a health assessment from one of our associated medical care providers.';
	}
	elseif($call_by == 'appointment-request-physician'){
		//Call from appointments/appointment-ajax-calls.php
		$body = "Patient ".$body_name." would like to schedule a consultation with you at  ".$password_reset_url.".";
	}
	elseif($call_by == 'appointment-confirmation-patient'){
		//Call from appointments/appointment-ajax-calls.php
		$body = "Dr.".$body_name." is available for your requested appointment at ".$password_reset_url.". ";
	}
	elseif($call_by == 'shoop-alert-patient'){
		//Call from shoop-system/strain_physician.php
		//Call from dispensary-functions/display-all-patient-shoops.php
		//Call from dispensary-functions/display-all-current-patient-shoops.php
		$body = 'You have been assigned a new SHOOP treatment recommendation. ';
	}
	elseif($call_by == 'physician-note-notification-patient'){
		$body = "Dr.".$body_name." has left you a note in MednCures.  Click Here to see what they said.";
	}
	elseif($call_by == 'support-ticket-response'){
		$body = "Thank you for reaching out to our support team.  We will respond to your inquiry ASAP.  ";
	}
	elseif($call_by == 'patient-product-selection-dispensary'){
		$body = "Your MednCures Patient ".$full_name." has inquired about \r\n\n".$product_title."\r\n\n ".$body_name." will be coming to pick up their medicine with 48 hours.\n";
	}
	elseif($call_by == 'new-medication-admin'){
		$body = 'A new Medication: <strong>"'.$body_name.'"</strong> is added in system and needs approval'."<br><br>".
		'Please <a href="http://medncures.com/wp-admin/admin.php?page=medication_list">Click Here</a> to Visit Admin Area and Approve the Medicine or Alternatively you can click on the following link: '.$password_reset_url;
	}
	elseif($call_by == 'new-diagnosis-admin'){
		$body = 'A new Diagnosis: <strong>"'.$body_name.'"</strong> is added in system and needs approval'."<br><br>".
		'Please <a href="http://medncures.com/wp-admin/admin.php?page=diagnosis_list">Click Here</a> to Visit Admin Area and Approve the Medicine or Alternatively you can click on the following link: '.$password_reset_url;
	}
	elseif($call_by == 'medicine-notification-patient'){
		$body = 'Time to Medicate: '."\n".
		$password_reset_url;
	}
	elseif($call_by == 'import-patient'){
		$body = $body_name."\n".
		$password_reset_url;
	}
	elseif($call_by == 'email-to-patient'){
		$body = $body_name."\n";
	}
	elseif($call_by == 'health-journey-patient'){
		$body = $body_name."\n";
	}
	return $body;
}