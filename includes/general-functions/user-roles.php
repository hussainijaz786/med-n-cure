<?php

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly 

add_action('init', 'caregiver_role');

function caregiver_role()
{
    global $wp_roles;
    if ( ! isset( $wp_roles ) )
        $wp_roles = new WP_Roles();

    $adm = $wp_roles->get_role('editor');
    //Adding a 'new_role' with all admin caps
    $wp_roles->add_role('caregiver', 'Caregiver', $adm->capabilities);
}

add_action('init', 'physician_role');

function physician_role()
{
    global $wp_roles;
    if ( ! isset( $wp_roles ) )
        $wp_roles = new WP_Roles();

    $adm = $wp_roles->get_role('editor');
    //Adding a 'new_role' with all admin caps
    $wp_roles->add_role('physician', 'Physician', $adm->capabilities);
}

add_action('init', 'patient_role');

function patient_role()
{
    global $wp_roles;
    if ( ! isset( $wp_roles ) )
        $wp_roles = new WP_Roles();

    $adm = $wp_roles->get_role('subscriber');
    //Adding a 'new_role' with all admin caps
    $wp_roles->add_role('patient', 'Patient', $adm->capabilities);
}

add_action('init', 'collective_role');

function collective_role()
{
    global $wp_roles;
    if ( ! isset( $wp_roles ) )
        $wp_roles = new WP_Roles();

    $adm = $wp_roles->get_role('editor');
    //Adding a 'new_role' with all admin caps
    $wp_roles->add_role('collective', 'Collective', $adm->capabilities);
}

add_action('init', 'dispensary_role');

function dispensary_role()
{
    global $wp_roles;
    if ( ! isset( $wp_roles ) )
        $wp_roles = new WP_Roles();

    $adm = $wp_roles->get_role('subscriber');
    //Adding a 'new_role' with all admin caps
    $wp_roles->add_role('dispensary', 'Dispensary', $adm->capabilities);
}

add_action('init', 'testing_lab_role');

function testing_lab_role()
{
    global $wp_roles;
    if ( ! isset( $wp_roles ) )
        $wp_roles = new WP_Roles();

    $adm = $wp_roles->get_role('subscriber');
    //Adding a 'new_role' with all admin caps
    $wp_roles->add_role('testing_lab', 'Testing Lab', $adm->capabilities);
}
?>