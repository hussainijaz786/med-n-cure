<?php 
if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly 
function az_tabs_data() {
	
	global $add_my_script;
	$siteurl = get_site_url();
	$page_type = isset($_GET['page_type'])? $_GET['page_type'] : '';
	$tbl = '';
	$current_usid = get_current_user_id();
	$current_rel_uri = add_query_arg( NULL, NULL );
	$userrole = get_user_role();
	if($userrole == 'physician' || $userrole == 'caregiver' || $userrole == 'administrator') {
		
		$tbl = '<div class="outer-wrap"><ul class="nav nav-pills patient-tabs phytb main-nav-tabs" id="physician-side-tabs">';
			if($page_type != 'settings'){
				$tbl .= '<li><a href="#home" data-toggle="tab">HOME</a></li>';
			}
			else{
				$tbl .= '<li class="active"><a href="#home" data-toggle="tab">HOME</a></li>';
			}
			$tbl .= '<li><a href="#patients" data-toggle="tab">PATIENTS</a></li>
			<li><a href="#menu1" data-toggle="tab">PRE-REGISTRATIONS<span id="pending-counts" style="display:none;"></span></a></li>
			<li><a href="#profile" data-toggle="tab">Profile</a></li>
		</ul></div>
		<div class="tab-content patient-pills phypl" ng-app="app" ng-controller="Ctrl">';
			if($page_type != 'settings'){
				$tbl .= '<div id="home" class="tab-pane fade in active">';
			}
			else{
				$tbl .= '<div id="home" class="tab-pane fade in">';
			}
			$tbl .= '<div class="lightborder1">'.do_shortcode('[cdrmed-dashboard]').'</div>
			</div>
			<div id="patients" class="tab-pane fade physician-patients-tab">'.do_shortcode('[cdrmed_patients_list]').'</div>
			<div id="add-patient" class="tab-pane fade"><div class="add-new-patient-form">'.do_shortcode('[user-registeration-form-physician]').'</div></div>
			<div id="menu1" class="tab-pane fade">'.do_shortcode('[pending-registrations]').'</div>
			<div id="profile" class="tab-pane fade"><div class="journey topspace"><h3>My Profile Settings:</h3><br></div>'.do_shortcode('[editprofile]').'</div>';
			if($page_type == 'settings'){
				$tbl .= '<div id="settings" class="tab-pane fade in active">'.do_shortcode('[child_physician_dispensary_profile]').'</div>';
			}
			else{
				$tbl .= '<div id="settings" class="tab-pane fade">'.do_shortcode('[edit_child_profile]').'</div>';
			}
				
		$tbl .= '</div>';
	}else if($userrole == 'dispensary') {
 
		$tbl = '<div class="outer-wrap"><ul class="nav nav-pills patient-tabs phytb main-nav-tabs" >';
			if($page_type != 'settings' && $page_type != 'orders'){
				$tbl .= '<li class="active"><a href="#home" data-toggle="tab">HOME</a></li>';
			}
			else{
				$tbl .= '<li><a href="#home" data-toggle="tab">HOME</a></li>';
			}
			$tbl .= '<li><a href="#patients" data-toggle="tab">PATIENTS</a></li>
			<li><a href="'.$siteurl.'/inventory-list/">Inventory</a></li>
			<li><a href="'.$siteurl.'/dispensar-profile">Profile</a></li>
		</ul></div>
		<div class="tab-content patient-pills phypl dispensary-tabs">';
			//$tbl .= '<div id="home" class="tab-pane fade in active">';
			if($page_type != 'settings' && $page_type != 'orders'){
				$tbl .= '<div id="home" class="tab-pane fade in active">';
			}
			else{
				$tbl .= '<div id="home" class="tab-pane fade">';
			}
			$tbl .= '<div class="lightborder1">'.do_shortcode('[cdrmed-dashboard]').'</div>
			</div>
			<div id="patients" class="tab-pane fade physician-patients-tab">'.do_shortcode('[cdrmed_patients_list]').'</div>
			<div id="add-patient" class="tab-pane fade"><div class="add-new-patient-form">'.do_shortcode('[user-registeration-form-physician]').'</div></div>';
			if($page_type == 'settings'){
				$tbl .= '<div id="settings" class="tab-pane fade in active">'.do_shortcode('[child_physician_dispensary_profile]').'</div>';
			}
			else if($page_type == 'orders'){
				$tbl .= '<div id="orders" class="tab-pane fade in active">'.do_shortcode('[inventory_orders]').'</div>';
			}
			else{
				$tbl .= '<div id="settings" class="tab-pane fade">'.do_shortcode('[edit_child_profile]').'</div>';
			}
		$tbl .= '</div>';
	}else if($userrole == 'patient') {
		
		//$tbl = do_shortcode( "[loadtab]");
		if(isset($_GET['view'])  && @$_GET['view'] == 2){
			$feedback =  "[symptoms_trackers]";
		}
		else if(isset($_GET['view'])  && @$_GET['view'] == 3){
			$feedback =  "[view-symptoms]";
		}else{
			//$feedback = "[cdrmed-patient-dashboard]";
			$feedback = "[cdrmed-dashboard]"; 
		}
		$tbl .= '<div class="outer-wrap">
		<ul class="nav nav-pills patient-tabs patienttb">';
			if($page_type != 'settings' && $page_type != 'orders' && $page_type != 'dosing_history'){
				$tbl .= '<li class="active"><a href="#home" data-toggle="tab">HOME</a></li>';
			}
			else{
				$tbl .= '<li><a href="#home" data-toggle="tab">HOME</a></li>';
			}
			
			$tbl .= '<li><a href="#myhealth" data-toggle="tab">MY HEALTH</a></li>
			<li><a href="#documents" data-toggle="tab">MY DOCUMENTS</a></li>
			<li><a href="#shoop" data-toggle="tab">MY THERAPY</a></li>
			<li><a href="#profile" data-toggle="tab">My PROFILE</a></li>
		</ul></div>';

		$password_set_by = get_user_meta($current_usid, 'who_updated_password', true);

		if(!empty($password_set_by) && $current_usid != $password_set_by){
			$tbl .= '<div class="max-internal-width password-reset-alert-bar">
				<div class="container">
					<div class="row">
						<div class="col-sm-12">
							<div class="alert">
								<div class="message">Please Reset Your Password</div>
								<span id="close-palert-bar" class="close-palert-bar"><i class="fa fa-remove"></i></span>
							</div>
						</div>
					</div>
				</div>
			</div>';
		}
		/* $home = '';
		$view_symptoms_trackers = '';
		if($page_type == 'view_symptoms_trackers'){
			$home = '';
			$view_symptoms_trackers = 'in active';
		} */

		$tbl .= '<div class="tab-content patient-pills patientpl" ng-app="app" ng-controller="Ctrl">';
			if($page_type != 'settings' && $page_type != 'orders' && $page_type != 'dosing_history'){
				$tbl .= '<div id="home" class="tab-pane fade in active">';
			}
			else{
				$tbl .= '<div id="home" class="tab-pane fade">';
			}
				$tbl .= '<div class="lightborder1">'.do_shortcode($feedback).'</div>
			</div>
			<div id="myhealth" class="tab-pane fade">
				<div class="intake-patient-form">
					'.do_shortcode( "[intake_form]").'
				</div>
			</div>
		

			<div id="documents" class="tab-pane fade">
				<div class="patient-documents">'.do_shortcode('[patient-documents]').'</div>
				<div class="journey"><h3>Add a new document to your secure storage:</h3></div>
				<div class="patient-add-documents">'.do_shortcode('[add-document]').'</div>
			</div>';

			if($siteurl == "https://medncures.com" || $siteurl == "http://medncures.com") {
				$tbl .= '<div id="shoop" class="tab-pane fade">
					<div class="row"><center><h2 class="journey">Coming Soon</h2></center></div>
				</div>';
			} else {
				$product_shortcode_return = do_shortcode('[shoop-products]');
				$tbl .= '<div id="shoop" class="tab-pane fade">
						<div class="row">
							<div class="col-md-9">'.do_shortcode('[get-patient-shoop-data]').'</div>
							<div class="col-md-3">'.$product_shortcode_return.'</div>
						</div>
					<div class="clear"></div>
					<div class="row">'.do_shortcode('[patient-therapy-display]').'</div>
				</div>';
			}

			$tbl .= '<div id="shoop-history" class="tab-pane fade">
				'.do_shortcode('[strain_shoop_history]').'
			</div>
			<div id="profile" class="tab-pane fade" ng-app="app" ng-controller="Ctrl">
				<div class="pati-profile">
					<div class="journey topspace"><h3>My Profile Settings</h3><br>
					</div>
					'.do_shortcode('[editprofile]').'
				</div><br>
				<div id="mmjrec">
					<div class="journey"><h3>My Medical Marijuana Recommendation:</h3>
					</div>
					'.do_shortcode('[mmj-recommendation-form]').'
				</div>
			</div>';
			//shortcode from patient-functions/import-requests.php
			if($page_type == 'settings'){
				$tbl .= '<div id="settings" class="tab-pane fade in active">'.do_shortcode('[import_requests]').'</div>';
			}
			else if($page_type == 'orders'){
				$tbl .= '<div id="orders" class="tab-pane fade in active">'.do_shortcode('[inventory_orders]').'</div>';
			}
			else if($page_type == 'dosing_history'){
				$tbl .= '<div id="dosing_history" class="tab-pane fade in active">'.do_shortcode('[dosing_history]').'</div>';
			}
			else{
				$tbl .= '<div id="settings" class="tab-pane fade">'.do_shortcode('[import_requests]').'</div>';
			}
			
		$tbl .= '</div>';
	
	}
	return $tbl;
}


add_shortcode( 'cdrmed-tabs', 'az_tabs_data' );
?>