<?php

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly 


function global_navigation(){
	$userrole = get_user_role();
	$url = $html = '';
	
	$display_user_info = null;
	/*
	*
	*	Navigation for All User Roles
	*
	*/
	if(is_user_logged_in()){
		if(	$userrole == 'physician' || $userrole == 'caregiver' || $userrole == 'administrator') {
			$url = site_url( '/physicians-dashboard/', 'http' );
			$html = '<div class="outer-wrap"><ul class="nav nav-pills patient-tabs phytb">
				<li class="active"><a href="'.$url.'#home">HOME</a></li>
				<li><a href="'.$url.'#patients">PATIENTS</a></li>
				<li><a href="'.$url.'#menu1">PRE-REGISTRATIONS</a></li>
				<li><a href="'.$url.'#profile">PROFILE</a></li>
			</ul></div>';
			$html .= '<div class="clear"></div>';
			$display_user_info = true;
		}
		else if( $userrole == 'dispensary' ){
			$url = site_url( '/dispensary-dashboard/', 'http' );
			$inventry = is_page("inventory-list") ? 'class="active"' : '';
			$profile = is_page("dispensar-profile") ? 'class="active"' : '';
			$html = '<div class="outer-wrap"><ul class="nav nav-pills patient-tabs phytb">
				<li><a href="'.$url.'#home">HOME</a></li>
				<li><a href="'.$url.'#patients">PATIENTS</a></li>
				<li '.$inventry.'><a href="'.get_site_url().'/inventory-list">Inventory</a></li>
				<li '.$profile.'><a href="'.get_site_url().'/dispensar-profile">PROFILE</a></li>
			</ul></div>';
			$html .= '<div class="clear"></div>';
			$display_user_info = true;
		}
		else if( $userrole == 'patient' ){
			$url = site_url( '/patient-dashboard/', 'http' );
			$html = '<div class="outer-wrap"><ul class="nav nav-pills patient-tabs patienttb">
				<li class="active"><a href="'.$url.'#home">HOME</a></li>
				<li><a href="'.$url.'#myhealth">MY HEALTH</a></li>
				<li><a href="'.$url.'#documents">MY DOCUMENTS</a></li>
				<li><a href="'.$url.'#shoop">MY THERAPY</a></li>
				<li><a href="'.$url.'#profile">My Profile</a></li>
			</ul></div>';
			$html .= '<div class="clear"></div>';
		}
	}
	
	if($display_user_info){
		if(is_page(array('tabs-details','user-registrations','mmj-recommendation','view-complete-intake','pre-registration-data','advanced-search','advanced-search-results','patient-health-journey','shoop-for-patient','create-new-shoop','user-registrations','patient-detail','patient-documents'))){
			
		}
	}
	return $html;
}

add_shortcode('global-navigation', 'global_navigation');



/*
*	Place this shortcode in page
*
	[cdrmed-pages-filter-shortcode]
*
*
*/
function page_shortcode_filter(){
	
	$html = '';
	$userrole = get_user_role();
	$url = $html = '';
	
	$display_user_info = null;
	
	if(	($userrole == 'physician' || $userrole == 'caregiver' || $userrole == 'administrator' || $userrole == 'dispensary' || $userrole == 'patient' ) && is_user_logged_in()) {
		$display_user_info = true;
	}
	/*
	*
	*	Shortcode use in Default pages
	*	Shortcode depend on Page Slug
	*
	*/
	if($display_user_info){
		
		if(is_page(array('physicians-dashboard','dispensary-dashboard','patient-dashboard'))){
			$html .= do_shortcode('[cdrmed-tabs]');
		}
		else{
			/*
			*	Add Navigation in Every Page
			*	All pages for Physician, Dispensary
			*/
			$html .= do_shortcode('[global-navigation]');
			$html .= '<div class="cdrmed-pages-filter">';
			if(is_page(array('advanced-search'))){
				$html .= do_shortcode('[cdrmed_search_advanced_fields]');
				$html .= do_shortcode('[cdrmed-adv-search]');
			}
			else if(is_page(array('general-notes'))){
				
				$html .= '<div class="patient-details-outerwrap cus-pad">
					<div class="row">
						'.do_shortcode('[user_profile_dispaly]').'
						'.do_shortcode('[patient-intake-short-details]').do_shortcode('[patient-notes]').'
					</div>
				</div>';
			}
			else if(is_page(array('generate-patients'))){
				$html .= do_shortcode('[generate-patients]');
			}
			else if(is_page(array('inventory-list'))){
				$html .= do_shortcode('[list-all-wc-products]');
			}
			else if(is_page(array('patient-health-journey'))){
				$html .= do_shortcode('[user_profile_dispaly]');
				$html .= do_shortcode('[patient_health_feedback_list]');
			}
			else if(is_page(array('patient-documents'))){
				$html .= do_shortcode('[user_profile_dispaly]');
				$html .= '<div class="patient-details-outerwrap">
					<div class="journey">
						<h3>Add documents to patient storage:</h3>
					</div>
					<div class="patient-add-documents">'.do_shortcode('[add-document]').'</div>
					<div class="journey">
						<h3>Current files shared with you:</h3>
					</div>
					'.do_shortcode('[patient-documents]').'
				</div>';
			}
			else if(is_page(array('pre-registration-data'))){
				$html .= do_shortcode('[user_profile_dispaly]');
				$html .= do_shortcode('[prereg_patient_data]');
			}
			else if(is_page(array('preview-strain'))){
				$html .= do_shortcode('[user_profile_dispaly]');
				$html .= do_shortcode('[strain_preview_form]');
			}
			else if(is_page(array('shoop-for-patient'))){
				$html .= do_shortcode('[user_profile_dispaly]');
				$html .= do_shortcode('[shoop-for-patient]');
			}
			else if(is_page(array('create-new-shoop'))){
				$html .= do_shortcode('[user_profile_dispaly]');
				$html .= '<div class="patient-details-outerwrap">
					<div class="main">
						'.do_shortcode('[patient_strain]').'
				</div></div>';
			}
			else if(is_page(array('view-complete-intake'))){
				$html .= do_shortcode('[user_profile_dispaly]');
				$html .= do_shortcode('[intake_complete_patient_data]');
			}
			else if(is_page(array('mmj-recommendation'))){
				$html .= do_shortcode('[user_profile_dispaly]');
				$html .= do_shortcode('[mmj-recommendation-form]');
			}
			else if(is_page(array('add-document'))){
				$html .= do_shortcode('[add-document]');
			}
			else if(is_page(array('dispensar-profile'))){
				$html .= '<div class="journey topspace"><h3>My Profile Settings:</h3><br></div>';
				$html .= do_shortcode('[editprofile]');
			}
			/*
			*	Page for Patient
			*/
			else if(is_page(array('view-patient-history'))){
				$html .= do_shortcode('[patient_health_feedback_list]');
			}
			$html .= '</div>';
		}
	}
	/*
	*	Before Login, Pages are Here
	*/
	else{
		if(is_page(array('pre-registration'))){
			$html .= '<div class="outer-wrap">
				<div class="heading-prereg">Pre Registration</div>
			</div>
			<div class="cdrmed-pages-filter">
				<div class="patient-details-outerwrap">
					'.do_shortcode('[pre-registration-form]').'
				</div>
			</div>';
		}
	}
	//$html .= do_shortcode('[user_profile_dispaly]');
	return $html;
}

add_shortcode('cdrmed-pages-filter-shortcode', 'page_shortcode_filter');


// Add Shortcode
function tab_navigation_cdrmed() {
	global $add_my_script;
	
	$userrole = get_user_role();
	$url = $html = '';
	if(	$userrole == 'physician' || $userrole == 'caregiver' || $userrole == 'administrator') {
		$url = site_url( '/physicians-dashboard/', 'http' );
		$html = '<div class="outer-wrap"><ul class="nav nav-pills patient-tabs phytb">
		<li class="active"><a href="'.$url.'#home">HOME</a></li>
		<li><a href="'.$url.'#patients">PATIENTS</a></li>
		<li><a href="'.$url.'#menu1">PRE-REGISTRATIONS</a></li>
		<li><a href="'.$url.'#profile">PROFILE</a></li>
		</ul></div>';
	}
	else{
		$url = site_url( '/dispensary-dashboard/', 'http' );
		$inventry = is_page("inventory-list") ? 'class="active"' : '';
		$profile = is_page("dispensar-profile") ? 'class="active"' : '';
		$html = '<div class="outer-wrap"><ul class="nav nav-pills patient-tabs phytb">
		<li><a href="'.$url.'#home">HOME</a></li>
		<li><a href="'.$url.'#patients">PATIENTS</a></li>
		<li '.$inventry.'><a href="'.get_site_url().'/inventory-list">Inventory</a></li>
		<li '.$profile.'><a href="'.get_site_url().'/dispensar-profile">PROFILE</a></li>
		</ul></div>';
	}
	$html .= '<div class="clear"></div>';
	return $html;
}
add_shortcode( 'physicians-tab-nav', 'tab_navigation_cdrmed' );




// Add Shortcode
function tab_navigation_cdrmed_p() {
	global $add_my_script;
	$url = site_url( '/patient-dashboard/', 'http' );
	
	
	$html = '<div class="outer-wrap"><ul class="nav nav-pills patient-tabs patienttb">
    <li class="active"><a href="'.$url.'#home">HOME</a></li>
    <li><a href="'.$url.'#myhealth">MY HEALTH</a></li>
    <li><a href="'.$url.'#documents">MY DOCUMENTS</a></li>
    <li><a href="'.$url.'#shoop">MY THERAPY</a></li>
    <li><a href="'.$url.'#profile">My Profile</a></li>
</ul></div>';
	
	$html .= '<div class="clear"></div>';
	
	
	return $html;
	
}
add_shortcode( 'patients-tab-nav', 'tab_navigation_cdrmed_p' );



// Add Shortcode
function tab_nav_switch() {
	
		$pid = $_GET['pid'];

	    if($pid){
	    $url = site_url( '/physicians-dashboard/', 'http' );
		$html = '<div class="outer-wrap"><ul class="nav nav-pills patient-tabs phytb">
		<li class="active"><a href="'.$url.'#home">HOME</a></li>
		<li><a href="'.$url.'#patients">PATIENTS</a></li>
		<li><a href="'.$url.'#menu1">PRE-REGISTRATIONS</a></li>
		<li><a href="'.$url.'#profile">PROFILE</a></li>
		</ul></div>
		';
		
		$html .= '<div class="clear"></div>';
	    } else {
	    $url = site_url( '/patient-dashboard/', 'http' );
		$html = '<div class="outer-wrap"><ul class="nav nav-pills patient-tabs patienttb">
	    <li class="active"><a href="'.$url.'#home">HOME</a></li>
	</ul></div>';
		
		$html .= '<div class="clear"></div>';
	    }
	    
	    return $html;

}
add_shortcode( 'tab-navigation', 'tab_nav_switch' );



// Add Shortcode
function tab_navigation_cdrmed_dispensar() {
	
	$url = site_url( '/dispensary-dashboard/', 'http' );
	
	$inventry = is_page("inventory-list") ? 'class="active"' : '';
	$profile = is_page("dispensar-profile") ? 'class="active"' : '';
	
	$html = '<div class="outer-wrap"><ul class="nav nav-pills patient-tabs phytb">
	<li><a href="'.$url.'#home">HOME</a></li>
	<li><a href="'.$url.'#patients">PATIENTS</a></li>
	<li '.$inventry.'><a href="'.get_site_url().'/inventory-list">Inventory</a></li>
	<li '.$profile.'><a href="'.get_site_url().'/dispensar-profile">PROFILE</a></li>
	</ul></div>
	';
	
	$html .= '<div class="clear"></div>';
	
	return $html;
	
}
add_shortcode( 'dispensar-tab-nav', 'tab_navigation_cdrmed_dispensar' );



?>