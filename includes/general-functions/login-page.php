<?php

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly 

// Add Shortcode
function cdrmed_new_login() {
	
	$login = '';
	
	$url = site_url();
	
	global $wpdb;
	$dispensary_id = $wpdb->get_var( $wpdb->prepare( "select user_id from $wpdb->usermeta where meta_key = 'url' and meta_value = '%s' LIMIT 1", $url ) );
	
	$site_title = 'MednCures';
	if ( $dispensary_id ) {
		$post_id = get_user_meta($dispensary_id, 'dispensar_post_id', true);
		$site_title = get_the_title($post_id);
	}
	
	$login .= '<div class="auntzelda-login">
			<div class="left-col">
			<h2>Welcome to the <span>'.$site_title.'</span> member portal.</h2>
			<div class="features">
				<ul>
					<li>
						<div class="icon">
							<img src="'.CDRMED.'/includes/assets/img/i1.png" alt="icon">
						</div>
						<div class="text">
							<h2>Easy-to-Understand Dosing Caculator</h2>
							<p>Use our online dosing caculator to figure out your daily dose to match your custom therapy.</p>
						</div>
					</li>
					<li>
						<div class="icon">
							<img src="'.CDRMED.'/includes/assets/img/i2.png" alt="icon">
						</div>
						<div class="text">
							<h2>Secure Document Storage</h2>
							<p>Share vital records and health documents within a secure enviroment.</p>
						</div>
					</li>
					<li>
						<div class="icon">
							<img src="'.CDRMED.'/includes/assets/img/i3.png" alt="icon">
						</div>
						<div class="text">
							<h2>Track Your Health</h2>
							<p>Chart your health over time and contribute to research that can advance cannabis medicine for all.</p>
						</div>
					</li>
				</ul>
			</div>
			<div class="sign-up">
				<a href="'.$url.'/pre-registration"><img class="signup-btn" src="'.CDRMED.'/includes/assets/img/signup.png" alt=""></a>
				<p>or visit <span><a href="'.$url.'">medncures.com</a></span> to learn more</p>
			</div>
			</div>
		<div class="right-col">
			<h2>Already a member? Sign in here:</h2>
			<div class="login-form">
				'.do_shortcode('[theme-my-login]').'
			</div>
			<div class="terms">
				<p class="upper">By logging in you agree to the <span><a href="#" class="mypopup" data-target="#privacyPolicypp" data-toggle="modal">Privacy Policy</a></span> and <span><a href="#" class="mypopup" data-target="#termsServicepp" data-toggle="modal">Terms of Use</a></span>.</p>
				<p class="bottom"><span class="light">Member portal provided by MednCures - </span><span><a href="#" class="mypopup" data-target="#hippaPolicypp" data-toggle="modal">HIPAA STATEMENT</a></span></p>
			</div>
		</div>
	</div>';	
		
	
	//}
	
	return $login;
		
}




add_shortcode( 'cdrmed-login', 'cdrmed_new_login' );

?>