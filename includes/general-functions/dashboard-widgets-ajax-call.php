<?php
if (!defined('ABSPATH')) exit; // Exit if accessed directly

/*
*
*		User Notifications
*		
*
*/

add_action( 'wp_ajax_remove_cdrmed_notifications_ajax', 'remove_cdrmed_notifications_ajax');
add_action( 'wp_ajax_nopriv_remove_cdrmed_notifications_ajax', 'remove_cdrmed_notifications_ajax');

function remove_cdrmed_notifications_ajax() {
	$current_usid = get_current_user_id();
	$current_usid = is_dispensary_physician_child($current_usid);
	$notification = $_POST['notification'];
	update_user_meta($current_usid, $notification, 0);
	die();
}	
	
	
add_action( 'wp_ajax_load_cdrmed_notifications_ajax', 'load_cdrmed_notifications_ajax');
add_action( 'wp_ajax_nopriv_load_cdrmed_notifications_ajax', 'load_cdrmed_notifications_ajax');

function load_cdrmed_notifications_ajax() {
	
	$current_usid = get_current_user_id();
	$current_usid = is_dispensary_physician_child($current_usid);
	$userrole = get_user_role();
	
	$html = '<div class="notification-panel-area">
		<span class="notification-btn"><i class="fa fa-bell"></i></span>';
	$action = null;
	if( $userrole == 'physician'){
		$html .= '<div class="single-notification">';
			$pre_reg_notification = get_user_meta($current_usid, 'pre_registeration_notification', true);
			if($pre_reg_notification == 1){
				$action = true;
				$url = site_url( '/physicians-dashboard/', 'http' );
				
				$html .= '<a class="notification-click" data-notification="pre_registeration_notification" url="'.$url.'#menu1">PRE-REGISTRATIONS</a>';
			}
			
			$appointment_notification = get_user_meta($current_usid, 'appointment_notification', true);
			if($appointment_notification == 1){
				$action = true;
				$html .= '<a class="diary1 view-all load-cdrmed-modal-box appointment_view_modal notification-click" data-notification="appointment_notification" data-call_action="appointment_view_modal" modal-title="'.get_user_meta($current_usid, "first_name", true).' \'s All Appointments" modal-action="appointment" modal-footer="false" data-call=">=">Appointments</a>';
			}
		$html .= '</div>';
	}
	elseif( $userrole == 'dispensary'){
		$html .= '<div class="single-notification">';
			$order_notification = get_user_meta($current_usid, 'order_notification', true);
			if($order_notification == 1){
				$action = true;
				$url = site_url();
				$html .= '<a class="notification-click" url="'.$url.'/dispensary-dashboard?page_type=orders" data-notification="order_notification">Orders</a>';
			}
		$html .= '</div>';
	}
	elseif( $userrole == 'patient'){
		$html .= '<div class="single-notification">';
			
			$import_patient_notification = get_user_meta($current_usid, 'import_patient_notification', true);
			if($import_patient_notification == 1){
				$action = true;
				$url = site_url();
				$html .= '<a class="notification-click" data-notification="import_patient_notification" url="'.$url.'/patient-dashboard?page_type=settings">Import Requests</a>';
			}
			
			$order_notification = get_user_meta($current_usid, 'order_notification', true);
			if($order_notification == 1){
				$action = true;
				$url = site_url();
				$html .= '<a class="notification-click" url="'.$url.'/patient-dashboard?page_type=orders" data-notification="order_notification">Orders</a>';
			}
			
			$shoop_notification = get_user_meta($current_usid, 'shoop_notification', true);
			if($shoop_notification == 1){
				$action = true;
				$url = site_url( '/patient-dashboard/', 'http' );
				$html .= '<a class="notification-click" url="'.$url.'#shoop" data-notification="shoop_notification">Therapy</a>';
			}
			
		$html .= '</div>';
	}
	
	$html .= '</div>';
	echo json_encode(array('action' => $action, 'output' => $html));
	die();
}

/*
*
*		Get 3 orders for  Dispensary Dashboard
*		
*
*/

add_action( 'wp_ajax_get_dashboard_orders_ajax', 'get_dashboard_orders_ajax');
add_action( 'wp_ajax_nopriv_get_dashboard_orders_ajax', 'get_dashboard_orders_ajax');

function get_dashboard_orders_ajax() {
	
	$current_usid = get_current_user_id();
	$current_usid = is_dispensary_physician_child($current_usid);
	$url = site_url();
	$html = '';
	$args = array(
		'post_type' => 'shop_order',
		'posts_per_page' => 3,
		'order' => 'DESC',
		'post_status' => 'wc-on-hold',
		'meta_query' => array(
			'relation' => 'AND',
			array(
				'key' => 'dispensary_order_id',
				'value' => $current_usid,
				'compare' => '='
			),
		),
	);
	$post = new WP_Query( $args );
	
	$total_orders = $post->found_posts;
	$child_counter = 1;
	while ( $post->have_posts() ) : $post->the_post();
		$order_id = get_the_ID();
		$date = get_the_date( "Y-m-d", $order_id );
		$patient_id = get_post_meta($order_id, '_customer_user', true);
		$name = get_user_meta($patient_id, 'first_name', true).' '.get_user_meta($patient_id, 'last_name', true);
		$html .= '<a class="child'.$child_counter.'">'.$name.'<span class="date-hover">'.$date.'</span></a>';
		$child_counter++;
	endwhile;
	if($html != ''){
		$html .= '<a class="view-all" href="'.$url.'/dispensary-dashboard?page_type=orders"">View All</a>';
	}
	else{
		$html .= '<span class="no-data">No New Order!</span>';
	}
	
	
	
	$total_orders = ($total_orders < 1) ? '<span class="no-data">No Order</span>' : 'On Hand: '.$total_orders;
	
	echo json_encode(array('active' => $total_orders, 'other' => $html));
	die();
}

/*
*
*		Get 3 Appointments for Physician Dashboard
*		
*
*/

add_action( 'wp_ajax_get_dashboard_appointment_ajax', 'get_dashboard_appointment_ajax');
add_action( 'wp_ajax_nopriv_get_dashboard_appointment_ajax', 'get_dashboard_appointment_ajax');

function get_dashboard_appointment_ajax() {
	
	$current_usid = get_current_user_id();
	$current_usid = is_dispensary_physician_child($current_usid);
	$userrole = get_user_role();
	$current_user_fname = get_user_meta($current_usid, "first_name", true);
	
	if( $userrole == 'physician' || $userrole == 'administrator'){
		
		
		$args = array(
			'post_type' => 'appointment',
			'posts_per_page' => 3,
			'order' => 'DESC',
			'post_status' => 'publish',
			'meta_query' => array(
				'relation' => 'AND',
				array(
					'key' => 'appointment_physician_id',
					'value' => $current_usid,
					'compare' => '='
				),
				array(
					'key' => 'appointment_status',
					'value' => array ( 'pending', 'confirm'),
					'compare' => 'IN'
				),
				array(
					'key' => 'date',
					'value' => date('m/d/Y'),
					'compare' => '>=',
				),
			),
		);
		
		$post = new WP_Query( $args );
		
		$appointments = '';
		$appointment_no = 1;
		$today_counter = 0;
		
		while ( $post->have_posts() ) : $post->the_post();
			$post_id = get_the_ID();
			$appointment_day = '';
			$appointment_date = get_post_meta(get_the_ID(), 'date', true);
			$name = get_post_meta(get_the_ID(), 'first_name', true).' '.get_post_meta(get_the_ID(), 'last_name', true);
			$date = get_the_date( "Y-m-d", $post_id );
			if($appointment_date == date('m/d/Y')){
				$appointment_day = 'Today';
				$today_counter++;
			}
			else{
				$appointment_date = DateTime::createFromFormat('m/d/Y', $appointment_date);
				$appointment_day = $appointment_date->format('D') ;
			}
			$appointments .= '<a class="child'.$appointment_no.' load-cdrmed-modal-box appointment_view_modal" data-call_action="appointment_view_modal" data-call=">=" modal-title="'.$current_user_fname.' \'s All Appointments" modal-action="appointment" modal-footer="false">'.$appointment_day.': '.$name.' <span class="date-hover">'.$date.'</span></a>';
			$appointment_no++;
			
		endwhile;
		
		$today_appointments = ($today_counter > 0) ? '<a class="load-cdrmed-modal-box appointment_view_modal" data-call_action="appointment_view_modal" data-call="=" modal-title="'.$current_user_fname.' \'s All Appointments" modal-action="appointment" modal-footer="false"><span class="total-appointments" style="cursor: pointer;">Today Appts '.$today_counter.'</span></a>' : '<span class="no-data">No Appointments!</span>';
		
		if($appointments != ''){
			$appointments .= '<a class="diary1 view-all load-cdrmed-modal-box appointment_view_modal"  data-call_action="appointment_view_modal"modal-title="'.$current_user_fname.' \'s All Appointments" modal-action="appointment" modal-footer="false" data-call=">=">View All</a>';
		}
		else{
			$appointment_system = get_user_meta( $current_usid, 'appointment_system', true );
			if($appointment_system == 'Calendly Appointment System'){
				$calendly_url = get_user_meta( $current_usid, 'calendly_url', true );
				$url = "Calendly.showPopupWidget('".$calendly_url."');return false;";
				//$appointments = '<a class="btn btn-default" href="#" onclick="'+$url+'">Click Here for Appointment</a>';
				$appointments = '<span class="no-data">No New Appointment</span>';
			}
			else{
				$appointments = '<span class="no-data">No New Appointment</span>';
			}
		}
		
		$response = array('active' => $today_appointments, 'other' => $appointments);
	}
	else{
		$response = array('active' => null, 'other' => '');
	}
	echo json_encode($response);
	die();
}

/*
*
*		Dashboard widgets init call
*		
*
*/

add_action( 'wp_ajax_get_dashboard_therapy_ajax', 'get_dashboard_therapy_ajax');
add_action( 'wp_ajax_nopriv_get_dashboard_therapy_ajax', 'get_dashboard_therapy_ajax');

function get_dashboard_therapy_ajax() {
	global $wpdb;
	
	$current_usid = get_current_user_id();
	$current_usid = is_dispensary_physician_child($current_usid);
	$args = array(
		'post_type'        => 'patient_therapy',
		'posts_per_page'   => 5,
		'orderby'          => 'date',
		'order'            => 'DESC',
		'meta_query' => array(
			'relation' => 'AND',
			array(
				'key' => 'therapy_by',
				'value' => $current_usid,
				'compare' => '='
			),
		),
	);
	$posts_array = new WP_Query( $args );
	$current_therapy = null;
	$html = '';
	$child_counter = 1;
	$shoopurl = site_url( '/shoop-for-patient/', 'http' );
	if($posts_array) {
		while ( $posts_array->have_posts() ) : $posts_array->the_post();
			$post_id = get_the_ID();
			$patient_id = get_the_author_meta( 'ID' );
			$name = get_user_meta($patient_id, 'first_name', true).' '.get_user_meta($patient_id, 'last_name', true);
			$date = get_the_date( "Y-m-d", $post_id );
			if(!$current_therapy){
				$current_therapy = '<a href="'.$shoopurl.'?pid='.$patient_id.'">'.$name.' <span class="date-hover">'.$date.'</span></a>';
			}
			else{
				$html .= '<a class="child'.$child_counter.'" href="'.$shoopurl.'?pid='.$patient_id.'">'.$name.' <span class="date-hover">'.$date.'</span></a>';
				$child_counter++;
			}
		endwhile;
	}
	
	if(!$current_therapy){
		$current_therapy = '<span class="no-data">No Therapy!</span>';
		$html = '<span class="no-data">Patients have no Therapy</span>';
	}
	
	$output = array('active' => $current_therapy, 'other' => $html);
	echo json_encode($output);
	die();
}



/*
*
*		Get Patients Appointments
*		
*
*/

add_action( 'wp_ajax_get_patient_appointments_ajax', 'get_patient_appointments_ajax');
add_action( 'wp_ajax_nopriv_get_patient_appointments_ajax', 'get_patient_appointments_ajax');

function get_patient_appointments_ajax() {
	
	$current_usid = get_current_user_id();
	$current_usid = is_dispensary_physician_child($current_usid);
	$args = array(
		'post_type' => 'appointment',
		'posts_per_page' => -1,
		'order' => 'DESC',
		'post_status' => 'publish',
		'author' => $current_usid,
		'meta_query' => array(
			'relation' => 'AND',
			array(
				'key' => 'appointment_status',
				'value' => array ( 'pending', 'confirm', 'cancel'),
				'compare' => 'IN'
			),
			array(
				'key' => 'date',
				'value' => date('m/d/Y'),
				'compare' => '>=',
			),
		),
	);
	
	$post = new WP_Query( $args );
	
	$response = (intval($post->post_count) > 0)? true : null;

	$html = '';
	$counter = 1;
	
	while ( $post->have_posts() ) : $post->the_post();
		
		$appointment_day = '';
		$appointment_date = get_post_meta(get_the_ID(), 'date', true);
		$name = '';
		
		$physician_id = get_post_meta(get_the_ID(), 'appointment_physician_id', true);
			$name = get_user_meta($physician_id, 'first_name', true).' '.get_user_meta($physician_id, 'last_name', true);
		
		$appointment_date = DateTime::createFromFormat('m/d/Y', $appointment_date);
		if($appointment_date == date('m/d/Y')){
			$appointment_day = 'Today';
		}
		else{
			$appointment_day = $appointment_date->format('l');
		}
		
		$status = get_post_meta(get_the_ID(), 'appointment_status', true);

		$status_html =  get_post_meta(get_the_ID(), 'appointment_cancel_by', true) ;
		
		if($status == 'cancel'){
			if($userrole == 'patient'){
				$status_html = ($status_html == 'patient') ? 'Cancel By You' : 'Cancel by '.$name;
			}
			else{
				$status_html = ($status_html == 'physician') ? 'Cancel By You' : 'Cancel by '.$name;
			}
		}
		else{
			$status_html = 'Status '.ucfirst($status);
		}
		
		$html .= '<div class="physician-single-appointment app'.$counter.'">
			<div class="date">'.$appointment_day.', '.$appointment_date->format('F').' '.$appointment_date->format('d').', '.$appointment_date->format('Y').'</div>
			<div class="panel panel-default appointment-collapse appointment_id'.get_the_ID().'">
				<div class="panel-heading panel-collapsed panel-opened '.$status.'">
						<span class="pull-left time">'.get_post_meta(get_the_ID(), 'time', true).'</span>
						<span class="info">'.$name.'</span>
						<span class="pull-right detail">Details <i class="fa fa-chevron-down"></i></span>
						<span class="pull-right closed">Close <i class="fa fa-remove"></i></span>
				</div>
				<div class="panel-body" style="display: none;">
					<div class="col-md-6">
						<label for="email">Email</label><br>
						<span class="email">'.get_post_meta(get_the_ID(), 'email', true).'</span>
					</div>
					<div class="col-md-6">
						<label for="phone">Phone</label><br>
						<span class="phone">'.get_post_meta(get_the_ID(), 'phone', true).'</span>
					</div>
					<div class="col-md-6">
						<span class="created">created '.get_the_date().'</span>
					</div>
					<div class="col-md-6">
						<span class="created">'.$status_html.'</span>
					</div>
						
					
					<div class="action">';
						
						$html .= '<a class="appointment-action-btn" data-action="cancel" data-id="'.get_the_ID().'"><span>Cancel</span><i class="fa fa-remove"></i></a>';
						
					$html .= '</div>
					
				</div>
			</div>
		</div>';
		$counter++;
		
	endwhile;
		
	$output = array('action' => $response, 'output' => $html);
	echo json_encode($output);
	die();
	
}




/*
*
*		Get 3 Patients
*		
*
*/

add_action( 'wp_ajax_get_dashboard_patients_ajax', 'get_dashboard_patients_ajax');
add_action( 'wp_ajax_nopriv_get_dashboard_patients_ajax', 'get_dashboard_patients_ajax');

function get_dashboard_patients_ajax() {
	
	$user_role = get_user_role();
	$current_usid = get_current_user_id();
	$current_usid = is_dispensary_physician_child($current_usid);
	$find_by = ($user_role == 'physician') ? 'patient_physician_id' : 'patient_dispensary_id';
	
	$params = array(
		'role' => 'patient',
		'number' => 4,
		'order' => 'DESC',
		'orderby' => 'ID',
		'date_query' 	=> array(
			array(
				'year' 	=> date('Y'),
				'month'	=> date('m'),
				'day'	=> date('d'),
			),
		),
		'meta_query' => array(
			'relation' => 'AND',
			array(
				'key' => $find_by,
				'value' => $current_usid,
				'compare' => 'like',
			)
		),
	);
	
	$users = new WP_User_Query($params);
	$getphysician = $users->get_results();
	
	$total_patients = (sizeof($getphysician) > 0)? 'Today: '.sizeof($getphysician) : '<span class="no-data">No Patient!</span>';
	$patients = '';
	foreach ($getphysician as $key => $value) {
		$patient_id = $value->ID;
		
		$name = get_user_meta( $patient_id, 'first_name', true ).' '.get_user_meta( $patient_id, 'last_name', true );
		
		$patients .= '<span>'.$name.'</span>';
		
	}
	
	$patients = ($patients == '') ? '<span class="no-data">Today: No New Patient!</span>' : $patients;
	
	echo json_encode(array('active' => $total_patients, 'other' => $patients));
	die();
	
}



/*
*
*		Get 3 products
*		
*
*/

add_action( 'wp_ajax_get_dashboard_products_ajax', 'get_dashboard_products_ajax');
add_action( 'wp_ajax_nopriv_get_dashboard_products_ajax', 'get_dashboard_products_ajax');

function get_dashboard_products_ajax() {
	$params = array(
		'post_type' => 'product',
		'posts_per_page' => 3,
		'date_query' 	=> array(
			array(
				'year' 	=> date('Y'),
				'month'	=> date('m'),
				'day'	=> date('d'),
			),
		),
		'meta_query' => array(
			'relation' => 'AND',
			array(
				'key' => 'product_dispensary_id',
				'value' => $patient_dispensary_id,
				'compare' => '=',
			)
		),
	);
	$wc_query = new WP_Query($params);
	$total_products = $wc_query->found_posts;
	
	$products = '';
	while ($wc_query->have_posts()) :
		$wc_query->the_post();
		
		$product_title = get_the_title();
		$products .= '<a href="'.get_permalink().'" target="_blank">'.$product_title.'</a>';
		
	endwhile;
	
	$total_products = ($total_products < 1) ? '<span class="no-data">No Product</span>' : 'Today: '.$total_products;
	$products = ($products == '') ? '<span class="no-data">Today: No New Product!</span>' : $products;
	
	echo json_encode(array('active' => $total_products, 'other' => $products));
	die();
	
}
?>