<?php

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly 

// Add Shortcode
function edit_profiles_all() {
	acf_form_head();
	ob_start();
// Bail if not logged in or able to post
	if ( ! ( is_user_logged_in() ) ) {
		echo '<p>You Must Be Logged In To Update Your Profile</p>';
		return;
	}
	
	$current_user = wp_get_current_user();
	$usid = get_current_user_id();
	$useremail = $current_user->user_email;
	if($usid) {
	acf_form(array(
					'post_id' => 'user_'.$usid, // Get the post ID
					'field_groups' => array('group_56de057b82a1f'),
					'label_placement' => 'left',
					'updated_message' => __("Profile Updated Successfully.", 'acf'),
					'submit_value'		=> 'Update Profile'
				));
	}
	$html = ob_get_contents();
		ob_get_clean();
		ob_end_flush();
		return $html;
}
add_shortcode( 'editprofile', 'edit_profiles_all' );


// Add Shortcode for Updating Health Updates
function edit_health_updates() {
	ob_start();
	if ( ! ( is_user_logged_in() ) ) {
		echo '<p>You Must Be Logged In To Update Your Profile</p>';
		return;
	}
	
	$current_user = wp_get_current_user();
	$usid = get_current_user_id();
	$useremail = $current_user->user_email;
	if($usid) {
	acf_form(array(
					'post_id' => 'user_' . $usid, // Get the post ID
					'field_groups' => array('group_576701d6a09f4'),
					'label_placement' => 'left',
					'updated_message' => __("We will keep you updated.", 'acf'),
					'submit_value'		=> 'Update Notifications'
				));
	}
	$html = ob_get_contents();
		ob_get_clean();
		ob_end_flush();
		return $html;
}
add_shortcode( 'edit-health-update', 'edit_health_updates' );


// Filter to just deal with the Password Field
function my_acf_update_value_userpass( $value, $post_id, $field  ) {
    $new_password = $_POST['acf']['field_56de05bdba5c0']; // update this
	if($new_password) {
	$who = str_replace('user_', '', $post_id);
	wp_set_password( $new_password, $who );
	wp_clear_auth_cookie();
	wp_set_current_user ( $who );
	wp_set_auth_cookie  ( $who );
	}
}
add_filter('acf/update_value/key=field_56de05bdba5c0', 'my_acf_update_value_userpass', 10, 3);


?>