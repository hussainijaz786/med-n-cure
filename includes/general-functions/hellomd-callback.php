<?php

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly 

// Add Shortcode
function hello_md_callback_function() {		
	$html = '';
// Bail if not logged in or able to post
	if ( ! ( is_user_logged_in() ) ) {
		$html .= '<p>You Must Be Logged In To Access</p>';
		return;
	}
	
	$current_user = wp_get_current_user();
	$usid = get_current_user_id();
	$useremail = $current_user->user_email;
	
	$hmd_userid = $_GET['user_id'];
	update_user_meta($usid, 'patient_hellomd_recommendation', $hmd_userid);

	if($hmd_userid) {
	$html .= '<center><h2>Thank You, Your records have been successfully submitted and will be updated as well accordingly.</h2></center>';
	} else {
	$html .= '<center><h2>Sorry, something\'s wrong, we will try to correct the issue and will update your details accordingly.</h2></center>';	
	}
	//echo $hmd_userid;
	
		$curl = curl_init();
		curl_setopt_array($curl, array(
		  CURLOPT_URL => "https://www.mixtology.com/api/v1/users/56fd9fc6f37ba00003000009",
		  CURLOPT_SSL_VERIFYHOST => false,
		  CURLOPT_RETURNTRANSFER => true,
		  CURLOPT_ENCODING => "",
		  CURLOPT_MAXREDIRS => 10,
		  CURLOPT_TIMEOUT => 30,
		  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		  CURLOPT_CUSTOMREQUEST => "GET",
		  CURLOPT_HTTPHEADER => array(
			"authorization: Basic NTM5Y2Q3ZTYwMjY2NjUxNTI0Y2E0N2Y5NDQ3YzliYWIwOWNlZmE2OmU1NTEyOTRhZDIxNGNmNzFhZjQxYzk5NTBkMjYzMjRjNTU1NjJmYg==",
			"cache-control: no-cache",
			"postman-token: bf110ecb-f298-c8ff-9591-b9e4581a4987"
		  ),
		));
		
		$response = curl_exec($curl);

		$err = curl_error($curl);
		
		curl_close($curl);
		
		if ($err) {
		  echo "cURL Error #:" . $err;
		} else {
		  $hellomd = $response;
		}
		
	$result = json_decode ($hellomd,true);
	
	/*echo "<pre/>";
	print_r($result);*/
	$document_url = $result['user']['document_url'];
	$ext = explode("?", $document_url);
	$url_ext = explode(".", $ext[0]);

	$phone = $result['user']['phone'];
	$recommendation_status = $result['user']['recommendation']['status'];
	$recommendation_expires = $result['user']['recommendation']['expires_at'];
	$recommendation_published = $result['user']['recommendation']['published_at'];
	$recommendation_url = $result['user']['recommendation']['url'];
	$doctor_firstname = $result['user']['doctor']['first_name'];
	$doctor_lastname = $result['user']['doctor']['last_name'];
	$doctor_license = $result['user']['doctor']['doctor_license'];
    $post_id = get_user_meta( $usid, 'pre-registration-id',true);
    $name = $doctor_firstname." ".$doctor_lastname;
   $date = date("Ymd", strtotime($recommendation_expires));
    update_field('field_570aa910f91b5',$name , $post_id);
    update_field('field_570aa92af91b6', $phone, $post_id);
    update_field('field_570aa9d7f91bb', $doctor_license, $post_id);
    update_field('field_570aa95ff91b8', $date, $post_id);
   
   
    //update_field('', '', $post_id);
    $filename = 'doc_'.substr(time(), 0,6).'.'.end($url_ext);
	$uploaddir = wp_upload_dir();
	$uploadfile = $uploaddir['path'] . '/'.$filename;

	$contents= file_get_contents( $document_url);
	$savefile = fopen($uploadfile, 'w');
	fwrite($savefile, $contents);
	fclose($savefile);
//
$wp_filetype = wp_check_filetype(basename($filename), null );

$attachment = array(
    'post_mime_type' => $wp_filetype['type'],
    'post_title' => $filename,
    'post_content' => '',
    'post_status' => 'inherit'
);

$attach_id = wp_insert_attachment( $attachment, $uploadfile);
update_field('field_570aa9f2f91bc',$attach_id,$post_id);

//second image 

	$ext2 = explode("?", $recommendation_url);
	$url_ext2 = explode(".", $ext2[0]);
  $filename = 'recomend_'.substr(time(), 0,6).'.'.end($url_ext2);
	$uploadfile = $uploaddir['path'] . '/'.$filename;
	$contents= file_get_contents( $recommendation_url);
	$savefile = fopen($uploadfile, 'w');
	fwrite($savefile, $contents);
	fclose($savefile);

	$wp_filetype = wp_check_filetype(basename($filename), null );

$attachment = array(
    'post_mime_type' => $wp_filetype['type'],
    'post_title' => $filename,
    'post_content' => '',
    'post_status' => 'inherit'
);

$attach_id = wp_insert_attachment( $attachment, $uploadfile);
update_field('field_570aa99cf91ba',$attach_id,$post_id);

/*$fullsizepath = get_attached_file( $imagenew->ID );
$attach_data = wp_generate_attachment_metadata( $attach_id, $fullsizepath );
wp_update_attachment_metadata( $attach_id, $attach_data );*/
	//$html .= '<img src="'.$document_url.'">';
	
	return $html;

}

add_shortcode( 'hellomd-callback', 'hello_md_callback_function' );


?>