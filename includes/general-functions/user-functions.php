<?php
if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

add_action( 'wp_ajax_count_user_age_ajax', 'count_user_age_ajax');
add_action( 'wp_ajax_nopriv_count_user_age_ajax', 'count_user_age_ajax');

function count_user_age_ajax() {
	
	$dob = isset($_POST['dob']) ? $_POST['dob'] : '';
	
	$db = date("M-d-Y", strtotime($dob));
	$age = date_diff(date_create($db), date_create('now'))->y;
	echo $age;
	die();
}
?>