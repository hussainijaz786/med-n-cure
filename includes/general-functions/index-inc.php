<?php 

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly 
// Include Custom Post Types Files
include('dashboard.php');
include('tabs.php');
include('login-page.php');
include('different-popups.php');
include('reassignuser.php');
include('tabs-nav.php');
include('security.php');
include('user-roles.php');
include('login-logout-filters.php');
include('dashboard-widgets-ajax-call.php');
include('email-sending.php');
include('user-functions.php');

?>