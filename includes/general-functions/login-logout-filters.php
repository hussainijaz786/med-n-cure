<?php

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly 

/*
*	Check Current user is child or not
*/
function is_dispensary_physician_child($current_usid){
	$parent_id = get_user_meta($current_usid, 'user_parent_id', true);
	if($parent_id){
		return $parent_id;
	}
	else{
		return $current_usid;
	}
}


/*
*
*	Geenrate Password reset link
*
*/
function password_reset_link($user_id){
	$user = new WP_User( (int) $user_id );
	$adt_rp_key = get_password_reset_key( $user );
	$user_login = $user->user_login;
	$rp_link = '<a href="' . network_site_url("wp-login.php?action=rp&key=$adt_rp_key&login=" . rawurlencode($user_login), 'login') . '">' . network_site_url("wp-login.php?action=rp&key=$adt_rp_key&login=" . rawurlencode($user_login), 'login') . '</a>';
	return $rp_link;
}

function get_user_role() {
	
	//error_reporting(E_ALL);
	////ini_set('display_errors', 0);
	
	global $current_user;
	$user_roles = $current_user->roles;
	$user_role = array_shift($user_roles);
	return $user_role;
}

function cdrmed_get_user_role( $user = null ) {
	$user = $user ? new WP_User( $user ) : '';
	return $user->roles ? $user->roles[0] : false;
}

function cdrmed_get_user_email( $user = null ) {
	$user = $user ? $user : '';
	$user_info = get_userdata($user);
	return $user_info->user_email ;
}

add_action ('init','access_control_admin');
function access_control_admin(){
	
	$userrole = get_user_role();
	
	$med_diag_offline_request = isset($_GET['med_diag_offline_request']) ? $_GET['med_diag_offline_request'] : '';
	
	if(!is_user_logged_in() && $med_diag_offline_request == 'offline'){
		$page = isset($_GET['page']) ? $_GET['page'] : '';
		$id = isset($_GET['id']) ? $_GET['id'] : '';
		$current_request = admin_url('admin.php?page='.$page.'&id='.$id);
		wp_cache_set('med_diag_offline_request', $current_request, 'med_diag_offline', 3600);
	}
	else if(is_user_logged_in() && $userrole == 'administrator' && $med_diag_offline_request == 'offline'){
		$current_request = wp_cache_get('med_diag_offline_request', 'med_diag_offline');
		
		if($current_request != false){
			wp_cache_delete('med_diag_offline_request');
			wp_redirect($current_request);
			exit;
		}
		else if(isset($_GET['page']) && isset($_GET['id'])){
			$page = isset($_GET['page']) ? $_GET['page'] : '';
			$id = isset($_GET['id']) ? $_GET['id'] : '';
			$current_request = admin_url('admin.php?page='.$page.'&id='.$id);
			wp_redirect($current_request);
			exit;
		}
	}
	
}


add_action ('wp_head','access_control_physician');

function access_control_physician(){
	
	$url = site_url();
	
	global $wp_query;
	$userrole = get_user_role();
	
	// Check If User is Not Logged in and tries to access any of the following page
	if(is_page(array('physicians-dashboard','dispensary-dashboard','patient-dashboard','tabs-details','user-registrations','mmj-recommendation','view-complete-intake','pre-registration-data','advanced-search','advanced-search-results','patient-health-journey','shoop-for-patient','create-new-shoop','user-registrations','patient-detail','patient-documents')) && !is_user_logged_in()){
		 wp_die('<center><img src="'.CDRMED.'/includes/assets/img/cdrmedlogo.png"><br>Sorry, you must be logged in to access dashboard.<br><br><a href="'.$url.'/login/">Click Here to Login</a>   or   <a href="'.$url.'">Click Here to go back</a></center>', 'Access Denied', array( 
        'response' => 401,
        'back_link' => false
        ));
	} // Check If User is Logged In and Has Correct Permissions to Access Specific Page
	else if(is_page(array('physicians-dashboard','tabs-details','user-registrations','mmj-recommendation','view-complete-intake','pre-registration-data','advanced-search','advanced-search-results','patient-health-journey','shoop-for-patient','create-new-shoop','user-registrations','patient-detail','patient-documents')) && is_user_logged_in()){
		if($userrole == 'physician' || $userrole == 'caregiver' || $userrole == 'administrator' || $userrole == 'dispensary') {
			// Perform No Action and Let User Use the Page.
		}
		else {
		wp_die('<center><img src="'.CDRMED.'/includes/assets/img/cdrmedlogo.png"><br><strong>Sorry, you must be a Physician or Caregiver to access this area.<strong><br><br><a href="'.$url.'/patient-dashboard/">Click Here to go back</a></center>', 'Access Denied', array( 
        'response' => 401,
        'back_link' => false
        ));				
		}
	}
	else if(is_page(array('dispensary-dashboard')) && is_user_logged_in()) {
		// Check if Logged In User is Admin or Dispensar
		if(	$userrole == 'dispensary' || $userrole == 'administrator' ) {
			// Perform No Action and Let User Use the Page.
		} else {
			wp_die('<center><img src="'.CDRMED.'/includes/assets/img/cdrmedlogo.png"><br><strong>Sorry, you must have Dispensary Capabilities to access this area.<strong><br><br><a href="'.$url.'/patient-dashboard/">Click Here to go back</a></center>', 'Access Denied', array( 
			'response' => 401,
			'back_link' => false
			));
		}
	} 
	else if(is_page(array('inventory-list','cart','checkout',)) && is_user_logged_in()){
		
		if(	$userrole == 'patient' || $userrole == 'dispensary'){
		}
		else{
			wp_die('<center><img src="'.CDRMED.'/includes/assets/img/cdrmedlogo.png"><br>Sorry, you must be logged in to access dashboard.<br><br><a href="'.$url.'/login/">Click Here to Login</a>   or   <a href="'.$url.'">Click Here to go back</a></center>', 'Access Denied', array( 
			'response' => 401,
			'back_link' => false
			));
		}
	}
	else if(is_page(array('inventory-list','cart','checkout',)) && !is_user_logged_in()){
		wp_die('<center><img src="'.CDRMED.'/includes/assets/img/cdrmedlogo.png"><br>Sorry, you must be logged in to access dashboard.<br><br><a href="'.$url.'/login/">Click Here to Login</a>   or   <a href="'.$url.'">Click Here to go back</a></center>', 'Access Denied', array( 
		'response' => 401,
		'back_link' => false
		));
	}
	/*
	* If Dispensary Role User access themes pages then show message
	*/
	if(is_page(array('patient-documents', 'create-new-shoop')) && $userrole == 'dispensary'){
		wp_die('<center><img src="'.CDRMED.'/includes/assets/img/cdrmedlogo.png"><br><strong>Sorry, you must be a Physician or Caregiver to access this area.<strong><br><br><a href="'.$url.'/dispensary-dashboard/">Click Here to go back</a></center>', 'Access Denied', array( 
        'response' => 401,
        'back_link' => false
        ));	
	}
	
	// Check If Anybody tries to use registration while being logged in
	if ( is_page(array('pre-registration','pre-registration-test')) && is_user_logged_in() ){
    wp_die('<center><img src="'.CDRMED.'/includes/assets/img/cdrmedlogo.png"><br><strong>Sorry, you are already a registered user and logged in as well.<strong><br><br><a href="'.$url.'/">Click Here to go back</a></center>', 'Access Denied', array( 
        'response' => 401,
        'back_link' => false
        ));	
	}
	
	
}

// This Shortcode is helpful in loading scripts on pages
add_shortcode( 'loadtab' , 'loadbstab' );

function loadbstab($atts)
{
	global $wpdb, $add_my_script;
	$add_my_script= true;
	
}


add_filter('wp_nav_menu_items', 'add_login_logout_link', 10, 2); 
function add_login_logout_link($items, $args) {
	global $current_user;
	//Freshdesk Support Login Quick Code
	$userrole = get_user_role();
	$url = site_url();
	ob_start(); 
	wp_loginout();
	$loginoutlink = ob_get_contents();
	ob_end_clean();
	
	if (!is_user_logged_in()) {
		$items .= '<li class="single-login-link">'. $loginoutlink .'</li>';
	} else {
		$items .= '<li class="menu-item menu-item-type-custom menu-item-object-custom" data-depth="0"><a href="#">Welcome, '.$current_user->user_firstname.'</a></li>';
		
		if(	$userrole == 'physician' || $userrole == 'caregiver' || $userrole == 	'administrator' ) {
			$items .= '<li><a href="'.$url.'/physicians-dashboard/">My Dashboard</a></li>';
			$items .= '<li><a href="'.$url.'/physicians-dashboard?page_type=settings">Sub User</a></li>';
			$items .= '<li><a href="'.$url.'/?action=freshdesk-login">Support</a></li>';
		} else if( $userrole == 'dispensary' ) {
			$items .= '<li><a href="'.$url.'/dispensary-dashboard/">My Dashboard</a></li>';
			$items .= '<li><a href="'.$url.'/dispensary-dashboard?page_type=settings">Sub User</a></li>';
			$items .= '<li><a class="load-cdrmed-modal-box api_view_modal" data-call_action="api_view_modal" modal-title="Setting '.$current_user->pos_system.'" modal-action="api" modal-footer="true" style="cursor: pointer;">APIs</a></li>';
			$items .= '<li><a href="'.$url.'/dispensary-dashboard?page_type=orders">Orders</a></li>';
			$items .= '<li><a href="'.$url.'/?action=freshdesk-login">Support</a></li>';
		} else if( $userrole == 'patient' ) {
			$items .= '<li><a href="'.$url.'/patient-dashboard/">My Dashboard</a></li>';
			$items .= '<li><a href="'.$url.'/view-patient-history">Health Journey</a></li>';
			$items .= '<li><a href="'.$url.'/patient-dashboard?view=3">View Symptoms</a></li>';
			$items .= '<li><a href="'.$url.'/patient-dashboard?page_type=settings">Import Requests</a></li>';
			$items .= '<li><a href="'.$url.'/patient-dashboard?page_type=orders">Orders</a></li>';
		}
		
		$items .= '<li>'.$loginoutlink.'</li>';
	}
	return $items;
}

add_filter('loginout','loginout_text_change');
function loginout_text_change($text) {
	
	$login_text_before = 'Log in';
	$login_text_after = 'Login';

	$logout_text_before = 'Log out';
	$logout_text_after = 'Log Out';

	$text = str_replace($login_text_before, $login_text_after ,$text);
	$text = str_replace($logout_text_before, $logout_text_after ,$text);
	return $text;
}


?>