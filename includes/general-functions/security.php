<?php

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly 

add_action('after_setup_theme', 'remove_admin_bar');

function remove_admin_bar() {
if (!current_user_can('administrator') && !is_admin()) {
  show_admin_bar(false);
}
}
function themeblvd_redirect_admin(){
	if ( !defined('DOING_AJAX') && !current_user_can('edit_posts') ) {
		wp_redirect( site_url() );
		exit;		
	}
}
add_action( 'admin_init', 'themeblvd_redirect_admin' );


// Extra Fields Auto Add Blank Rows in Medications and Supplements
//add_filter('acf/load_value/key=field_56c2ecf138705',  'afc_load_my_repeater_value1', 10, 3);
function afc_load_my_repeater_value1($value, $post_id, $field) {
	if(is_page(array( 'patient-dashboard','view-complete-intake'))){
	if(!$value){
		$value	= array();
		$value[] = array(
			field_56c2ed2038706 => ' ',
			field_56c2ed4138707 => ' ',
			field_56c2ed5038708 => ' '
		);
	}
	return $value;
	}
}

// Extra Fields Auto Add Blank Rows in Treatment History Repeater
add_filter('acf/load_value/key=field_56c2d72d0d663',  'afc_load_my_repeater_value2', 10, 3);
function afc_load_my_repeater_value2($value, $post_id, $field) {
	if(is_page(array( 'patient-dashboard','view-complete-intake'))){
	if(!$value){	
		$value	= array();
		$value[] = array(
			'field_56c2d73d0d664' => ' ',
			'field_56c2d7480d665' => ' ',
			'field_56c2d7650d666' => ' '
		);
	}
	return $value;
	}
}
// Show Extra Field in Medication Allergy Description
add_filter('acf/load_value/key=field_57b2a5a102b18',  'afc_load_my_repeater_value7', 10, 3);
function afc_load_my_repeater_value7($value, $post_id, $field) {
	if(is_page(array( 'patient-dashboard','view-complete-intake'))){
	if(!$value){	
		$value	= array();
		$value[] = array(
			'field_57b2a5b902b19' => ' '
		);
	}
	return $value;
	}
}

// Extra Fields Auto Add Blank Rows in Nutrition and Excercise
add_filter('acf/load_value/key=field_56c2f6c80c16c',  'afc_load_my_repeater_value3', 10, 3);
function afc_load_my_repeater_value3($value, $post_id, $field) {
	if(is_page(array( 'patient-dashboard','view-complete-intake'))){
	if(!$value){
		$value	= array();
		$value[] = array(
			'field_56c2f6e00c16d' => ' ',
			'field_56c2f6ef0c16e' => ' ',
			'field_56c2f7020c16f' => ' '
		);
	}
	return $value;
	}
}

// Extra Fields Auto Add Blank Rows in Other health concerns
add_filter('acf/load_value/key=field_56ca93b748d2f',  'afc_load_my_repeater_value4', 10, 3);
function afc_load_my_repeater_value4($value, $post_id, $field) {
	if(is_page(array( 'patient-dashboard','view-complete-intake'))){
	if(!$value){
		$value	= array();
		$value[] = array(
			'field_56ca93b748d31' => ' '
		);
	}
	return $value;
	}
}

//add_filter('acf/load_value/key=field_56da814d8b2ba',  'afc_load_my_repeater_value5', 10, 3);
function afc_load_my_repeater_value5($value, $post_id, $field) {
	if(is_page(array( 'pre-registration'))){
	if(!$value){
		$value	= array();
		$value[] = array(
			'field_56da815e8b2bb' => ' ',
			'field_56da818e8b3bb' => ' ',
			'field_56da81fe8b701' => ' '
		);
	}
	return $value;
	}
}

// Add Patient Page Repeater Fields Blank Rows
add_filter('acf/load_value/key=field_56da814d8b2ba1',  'afc_load_my_repeater_value9', 10, 3);
function afc_load_my_repeater_value9($value, $post_id, $field) {
	if(is_page(array( 'dispensary-dashboard','physicians-dashboard'))){
	if(!$value){
		$value	= array();
		$value[] = array(
			'field_56da815e8b2bb1' => ' ',
			'field_56da818e8b3bb1' => ' ',
			'field_56da81fe8b7011' => ' '
		);
	}
	return $value;
	}
}


//Email Validation on Pre-Registration Form
add_filter('acf/validate_value/key=field_56da7ab78b29f', 'my_acf_validate_value1', 10, 4);

function my_acf_validate_value1( $valid, $value, $field, $input ){
	// bail early if value is already invalid
	if( !$valid ) {
		return $valid;
	}
	
	// Check if email is already in registration queue
	/*$args = array(
	'posts_per_page'   => -1,
	'post_type'        => 'pre-registrations',
	'author'	   => '',
	'suppress_filters' => true 
	);
	$myposts = get_posts( $args );
	
	foreach($myposts as $postcheck) {
	$pid = get_the_ID();
	$email_already = get_field('email',$pid);
	if($value == $email_already) {
		$valid = 'We have already recieved your registration against this email ID, please wait for its approval or contact us. Thanks!';
	}
		
	}*/
	
	$exists = email_exists($value);
	if($exists) {
		$valid = 'This Email Already Exists in the System and Cannot be used again for Registration';
	}
	// return
	return $valid;
	
	
}
?>