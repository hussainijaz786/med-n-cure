<?php



add_shortcode( 'cdrmed-dashboard', 'cdrmed_dashboard');
function cdrmed_dashboard() {
	
	//error_reporting(E_ALL);
	////ini_set('display_errors', 0);
	
	$current_user = wp_get_current_user();
	$current_usid = get_current_user_id();
	$siteurl = site_url();
	$userrole = get_user_role();
	$dashboard_type = '';
	if($userrole == 'patient'){
		$dashboard_type = 'patient-dashboard';
	}
	$html = '';
	$html .= '<div class="cdrmed-dashboard '.$dashboard_type .'">
				
		
		
		<div class="container-fluid">';
		
		$current_rel_uri = add_query_arg( NULL, NULL );
		
		if($userrole == 'physician' || $userrole == 'caregiver' || $userrole == 'administrator' || $userrole == 'dispensary' ) {
			$url = site_url( '/', 'http' );
			$html .= '<div class="max-internal-width physician-dashboard-topbar">
				<div class="row first-row">
				
					<div class="col-xs-12 col-sm-6 col-md-3">
						<div class="dashboard-widget first-widget">
							<h1 id="current_date" class="current_date"></h1>
							<div id="user_calendar" class="datepicker-here datepicker-promo"></div>
						</div>
					</div>
					<div class="col-xs-12 col-sm-6 col-md-3">
						<div class="dashboard-widget second-widget">
							<div class="single-box">
								<h1 class="title">Patients</h1>
								<span class="active-box"></span>
							</div>
							<div class="other">
								<div class="other-data"></div>
							</div>
						</div>
					</div>
					<div class="col-xs-12 col-sm-6 col-md-3">
						<div class="dashboard-widget third-widget">
							<div class="single-box">
								<h1 class="title"></h1>
								<span class="active-box"></span>
							</div>
							<div class="other">
								<div class="other-data"></div>
							</div>
						</div>
					</div>
					<div class="col-xs-12 col-sm-6 col-md-3">
						<div class="dashboard-widget fourth-widget">
							<div class="single-box">
								<h1 class="title"></h1>
								<span class="active-box"></span>
							</div>
							<div class="other">
								<div class="other-data"></div>
							</div>
						</div>
					</div>
					
				</div>
			</div>';
		}else if($userrole == 'patient') {
			
			$intake_filled_tabs = $GLOBALS['intake_complete'];
			$url = site_url( '/patient-dashboard/', 'https' );
			if(count($intake_filled_tabs) > 1){
				$html .= '<div class="max-internal-width darkbluec client-alert-content-bar">
					<div class="container">
						<div class="row">
							<div class="col-sm-12 col-md-12">
								<div class="patient-dashboard-alert client-alert-content">
									<span class="heart"><i class="fa fa-heart"></i></span>
									<div class="message">health intake incomplete</div>
									<button href="'.$url.'#myhealth" id="update_my_health" name="submit" class="button">UPDATE</button>
									<span id="close-alert-bar" class="close-alert-bar"><i class="fa fa-remove"></i></span>
								</div>
							</div>
						</div>
					</div>
				</div>';
			}
			
			
			$html .= '<div class="max-internal-width patient-dashboard-feedback-bar">
					<div class="container">
						<div class="row">
							<div class="col-sm-12 col-md-12">
								<div class="patient-dashboard-feedback">
									'.do_shortcode("[feedback_form]").'
								</div>
							</div>
						</div>
					</div>
				</div>';
			
			
		}
		
			
			$html .= '<div class="main-dashboard">
				<div class="">
					<div class="">
						<div class="col-xs-12 col-sm-12 col-md-6">
							<div class="row">
								<div class="sidebar">
									<div class="col-xs-12 col-sm-6 col-md-6">
										<div class="tab-content buttons" style="margin-top: 0 !important;">';
											if($userrole == 'physician' || $userrole == 'caregiver' || $userrole == 'administrator') {
												$html .= '<a href="#patients" data-toggle="tab">Patient List</a>
												<a class="diary1 view-all load-cdrmed-modal-box appointment_view_modal" data-call_action="appointment_view_modal" modal-title="'.get_user_meta($current_usid, "first_name", true).' \'s All Appointments" modal-action="appointment" modal-footer="false" data-call=">=">Appointments</a>';
											}else if($userrole == 'dispensary') {
												$html .= '<a href="#add-patient" data-toggle="tab">Add Patient</a>
												<a href="#patients" data-toggle="tab">Patient List</a>
												<a href="'.$url.'inventory-list">Inventory</a>';
											}else if($userrole == 'patient') {
												$html .= '<a href="'.$url.'#profile" data-toggle="tab">My Profile</a>
												<a href="'.$url.'#shoop" data-toggle="tab">My Therapy</a>
												<a href="'.$url.'#myhealth" data-toggle="tab">Update Health</a>
												<a class="diary1 view-all load-cdrmed-modal-box physician_appointment_view_modal" data-call_action="physician_appointment_view_modal" modal-title="MednCures All Physicians" modal-action="patient-appointment" modal-footer="true">Schedule</a>
												<!--<a class="send-health-journey">Health journey</a>-->
												<!--<a class="send-medicine-notification">Med Notification</a>-->
												';
											}
											$html .= '<a class="educational_modal_box_show" data-toggle="modal">Learn More</a>';
										$html .= '</div>
									</div>
									<div class="col-xs-12 col-sm-6 col-md-6">
										<div class="profile" ng-app="app" ng-controller="Ctrl">
										
											<div class="dashboard-profile-img" class="user-custom-profile" id="user-custom-profile">
											
											
								
											<div class="user-avatar-group">
											
											<label for="profile_img_file1" id="profile_img_file_label1" class="profile_img_file_label">
											
											<span id="change-profile-btn1" class="change-profile-btn"><i class="fa fa-edit"></i></span>
											
											<span class="button button-primary profile_img_file_span" id="profile_img_file_span1">Choose Image</span>
												<img alt="" src="'.$current_user->user_avatar.'" class="user-avatar" id="user-avatar1" height="100" width="100"/>
											
											</label>
											
												<span id="remove-profile-btn1" class="remove-profile-btn"><i class="fa fa-remove"></i></span>
												
											</div>
											
											<input type="file" name="profile_img_file1" id="profile_img_file1" class="profile_img_file1" value="Choose Image" style="visibility: hidden; position: absolute;"/>
											<div class="profile-img-crop" id="profile-img-crop1">
												<div class="cropArea" id="cropArea1">
													<img-crop image="myImage" result-image="myCroppedImage" id="myImage1" class="myImage"></img-crop>
												</div>
												<input class="acf-button button button-primary clear-profile-img-btn" id="clear-profile-img-btn1" value="Clear" type="submit">
												<a class="profile-change-btn" id="profile-change-btn"><i class="fa fa-pencil"></i></a>
												<img ng-src="{{myCroppedImage}}" name="imgCropped" id="imgCropped1" class="imgCropped" height="100" width="100"/>
												<input type="hidden" id="profile_img_change_flag1" class="profile_img_change_flag">
											</div>
											
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="col-xs-12 col-sm-12 col-md-6">';
						
							if($userrole == 'patient') {
								$html .= '<div class="contents all-appointments patinet-side-appointments">
									<h3 class="heading">My Appointments</h3>
									<div class="appointments">
									</div>
								</div>';
							}
							else{
								
								
								$news_feed_link = (get_user_meta($current_usid, 'news_feed_link', true)!= '') ? get_user_meta($current_usid, 'news_feed_link', true) : 'http://feeds.denverpost.com/dp-business-healthcare';
								
								$news_feed_link_convert = simplexml_load_file($news_feed_link);
								$news_feed_link_arr = json_decode( json_encode($news_feed_link_convert) , 1);
								//echo "<pre>";print_r($news_feed_link_arr);echo "</pre>";
								$html .= '<div class="contents">
									<h3 class="heading">My News Feed</h3>';
								$counter = 0;
								foreach($news_feed_link_arr['channel']['item'] as $key=>$value){
									if($counter == 3){
										break;
									}
									$html .= '<div class="news-feed">
										<a href="'.$value["link"].'"  target="_blank"><h4 class="title">'.$value["title"].'</h4></a>';
									if(!is_array($value["category"])){
										$html .= '<h6 class="category">'.$value["category"].'</h6>';
									}
									if(!is_array($value["description"])){
									$html .= '
										<p>'.$value["description"].'</p>';
									}
									$html .= '
										<a href="'.$value["link"].'" target="_blank">+ MORE</a>
									</div>';
									$counter++;
								}
								
								$html .= '</div>';
								
							}
						$html .= '</div>
					</div>
				</div>
			</div>
			
		</div>
	</div>
	';
	
	
	
	return $html;
	
}