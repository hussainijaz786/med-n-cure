<?php
//Include ACF
include_once('includes/assets/acf/acf.php');

//WP Advanced Search Framework
require_once('includes/assets/wp-advanced-search/wpas.php');

//Custom Tables Auto Population
include_once('includes/db-tables/index-inc.php');

//Custom Post Types Registration Include
include('includes/cpts/index-inc.php');

//Custom Post Types Custom Fields Include
include('includes/cpts-custom-fields/index-inc.php');

//MednCures Appointment System for Physician and Patient
include_once('includes/appointments/index-inc.php');

//General Functions such as Tabs Navigation, Tabs etc include
include('includes/general-functions/index-inc.php');

//Physician Functions such as Tabs Navigation, Tabs etc include
include('includes/physician-functions/index-inc.php');

//Patient Functions for Patient Dashboard
include('includes/patient-functions/index-inc.php');

//Complete New SHOOP System Include
include('includes/shoop-system/index-inc.php');

//WooComerce Backend Tabs
include('includes/woocommerce-backend/woo-commerce-tabs.php');

//QR Code Library
//include('includes/assets/lib/qr/qrlib.php');

//Actual Function Files for Advanced Search
include('includes/advanced-search/index-inc.php');

//Actual Function Files for Inventory Management at Frontend
include('includes/frontend-product-management/index-inc.php');

//Actual Function Files for Inventory APIs
include('includes/inventory-apis/index-inc.php');

//Admin Functions
include('includes/admin-functions/index-inc.php');

//Actual Function Files for Dispensary Dashboard
include('includes/dispensary-functions/index-inc.php');

//MednCures API Authentication System
include('includes/cdrmed-api/index-inc.php');

//MednCures Modal Boxes
include('includes/cdrmed-modal-boxes/index-inc.php');

//Patient Notes Functions
include('includes/patient-notes/index-inc.php');

//MednCures Education System
include('includes/education-functions/index-inc.php');

?>