<?php

/*
  Plugin Name: Med & Cures
  Plugin URI:  http://geniusenterprises.co.uk/
  Description: 
  Author: TekFold
  Version: 1.0
  Author URI: tekfold.com 
 */
 
error_reporting(E_ALL);
//ini_set('display_errors', 0);

// Physicians Code

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly 

define('CDRMED', untrailingslashit(plugins_url('', __FILE__)));
include('functions.php');
register_activation_hook( __FILE__, 'create_notes_table' );
register_activation_hook( __FILE__, 'create_disease_table' );
add_action('init', 'register_my_script_wpd');

function register_my_script_wpd() {

	wp_register_script('my_custom_script1-wpd', plugins_url('includes/assets/js/html2canvas.js', __FILE__), array('jquery'), '1.0', true);
	wp_register_script('my_custom_script2-wpd', plugins_url('includes/assets/js/jquery.base64.js', __FILE__), array('jquery'), '1.0', true);
	wp_register_script('my_custom_script3-wpd', plugins_url('includes/assets/js/tableExport.js', __FILE__), array('jquery'), '1.0', true);
	wp_register_script('my_custom_script4-wpd', plugins_url('includes/assets/js/table/bootstrap-table-all.js', __FILE__), array('jquery'), '1.0', false);
	wp_register_script('cdremed-table-filter', plugins_url('includes/assets/js/bootstrap-table-filter.min.js', __FILE__), array('jquery'), '1.0', false);
	wp_register_script('my_custom_script5-wpd', plugins_url('includes/assets/js/jspdf/libs/sprintf.js', __FILE__), array('jquery'), '1.0', true);
	wp_register_script('my_custom_script6-wpd', plugins_url('includes/assets/js/jspdf/jspdf.js', __FILE__), array('jquery'), '1.0', true);
	wp_register_script('my_custom_script7-wpd', plugins_url('includes/assets/js/jspdf/libs/base64.js', __FILE__), array('jquery'), '1.0', true);
	wp_register_script('my_custom_script8-wpd', plugins_url('includes/assets/js/bootstrap-lightbox.min.js', __FILE__));
	//wp_register_script('cdrmedjs', plugins_url('includes/assets/js/cdrmedjs.js', __FILE__));
	wp_register_script('bsselectjs', plugins_url('includes/assets/js/bootstrap-select.js', __FILE__));
	wp_register_script('mcustomsb', plugins_url('includes/assets/js/jquery.mCustomScrollbar.js', __FILE__));
	//wp_register_script('disease-js', plugins_url('includes/assets/js/az-disease.js', __FILE__));
	//wp_register_script('cdrmed-ajax-functions', plugins_url('includes/assets/js/cdrmed-ajax-functions.js', __FILE__));
	//wp_register_script('maskedinput-js', plugins_url('includes/assets/js/jquery.maskedinput.min.js', __FILE__));
	wp_register_script('date-format-js', plugins_url('includes/assets/js/date.format.js', __FILE__));
	wp_register_script('input-mask-js', plugins_url('includes/assets/js/jquery.inputmask.bundle.js', __FILE__));
	//wp_register_script('jquery-inputmask-extensions', plugins_url('includes/assets/js/inputmask.extensions.js', __FILE__));

	//Auto Complete Scripts
	wp_register_script('modernizr', plugins_url('includes/assets/js/modernizr.js', __FILE__));
	wp_register_script('tokenize', plugins_url('includes/assets/js/jquery.tokenize.js', __FILE__));
	wp_register_script('jui', plugins_url('includes/assets/js/jquery-ui.js', __FILE__));
	
	//Auto Complete Styles
	wp_register_style('jui-css', plugins_url('includes/assets/css/jquery.ui.css', __FILE__));
	wp_register_style('jtoken-css', plugins_url('includes/assets/css/jquery.tokenize.css', __FILE__));

	//slick slider plugin 
	wp_register_script('slick-js', plugins_url('includes/assets/js/slick.min.js', __FILE__));
	wp_register_style('slick-css', plugins_url('includes/assets/css/slick.css', __FILE__));
	wp_register_style('slick-theme', plugins_url('includes/assets/css/slick-theme.css', __FILE__));
	
	//SHOOP CSS
	wp_register_style('bootstrapselect', plugins_url('includes/assets/css/bootstrap-select.css', __FILE__));
	wp_register_style('fontawesome', plugins_url('includes/assets/css/font-awesome.css', __FILE__));
	wp_register_style('customscroll', plugins_url('includes/assets/css/jquery.mCustomScrollbar.css', __FILE__));
	
	// Bootstrap Tabs
    //wp_register_style('mybscss', plugins_url('includes/assets/css/bootstrap.min.css', __FILE__));
	wp_register_style('cdrmedcss', plugins_url('includes/assets/css/cdrmedcss.css', __FILE__));
    wp_register_style('bootStrapMinCSS-wpd', 'http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css');
    wp_register_style('bootStrapTable-wpd', plugins_url('includes/assets/css/bootstrap-table.css', __FILE__));
	wp_register_style('jquery-style-wpd', 'http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.2/themes/smoothness/jquery-ui.css');
	wp_register_style('cdrmedow', plugins_url('includes/assets/css/cssoverwrite.css', __FILE__));
	wp_register_style('cdrmed-dispensar', plugins_url('includes/assets/css/cdrmed-dispensar.css', __FILE__));
	
	//Bootstrap Multiselect
	wp_register_script('bs-multiselect-js', plugins_url('includes/assets/js/bootstrap-multiselect.js', __FILE__));
	wp_register_style('bs-multiselect-css', plugins_url('includes/assets/css/bootstrap-multiselect.css', __FILE__));
	
	
	//WooCommerce Tabs Js + CSS
	wp_register_style('custom-tabs', plugins_url('includes/assets/css/custom-tabs.css', __FILE__));
	wp_register_script('woo-tabs-js', plugins_url('includes/assets/js/woocommerce-tabs.js', __FILE__));
	wp_register_script( 'jquery-bootstrap',  '//netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js', false, '1.0.5' );
	
	//Angular JS files for Upload and Crop	
	wp_register_script('angular-js', plugins_url('includes/assets/minified/angular.js', __FILE__), array('jquery'), '1.0', false);
	wp_register_script('ng-img-crop-js', plugins_url('includes/assets/minified/ng-img-crop.js', __FILE__), array('jquery'), '1.0', false);
	wp_register_style('ng-img-crop-css', plugins_url('includes/assets/minified/ng-img-crop.css', __FILE__));
	
	// Weather JS
	wp_register_script('weather-js', plugins_url('includes/assets/js/jquery.simpleWeather.min.js', __FILE__), array('jquery'), '1.0', false);
	//Determines the time zone of the browser client
	wp_register_script('timezonedetect-js', '//cdnjs.cloudflare.com/ajax/libs/jstimezonedetect/1.0.4/jstz.min.js');
	
	//Dashboard Calendar
	wp_register_style('calendar-css', plugins_url('includes/assets/css/datepicker.css', __FILE__));
	wp_register_script('calendar-js', plugins_url('includes/assets/js/datepicker.js', __FILE__), array('jquery'), '1.0', false);
	
	//Physician Dashboard
	wp_register_style('dashboard-css', plugins_url('includes/assets/css/dashboard.css', __FILE__));
	//wp_register_script('dashboard-js', plugins_url('includes/assets/js/dashboard.js', __FILE__), array('jquery'), '1.0', false);
	
	//Auto Complete Fields
	wp_register_script('medication-autocomplete-js', plugins_url('includes/assets/js/medication_autocomplete.js', __FILE__), array('jquery'), '1.0', false);
	wp_register_script('diagnosis-autocomplete-js', plugins_url('includes/assets/js/diagnosis_autocomplete.js', __FILE__), array('jquery'), '1.0', false);
	
	//MednCures Appointment System
	wp_register_style('cdrmed-appointment-css', plugins_url('includes/assets/css/appointment.css', __FILE__));
	//wp_register_script('cdrmed-appointment-js', plugins_url('includes/assets/js/appointment.js', __FILE__), array('jquery'), '1.0', false);
	
	//Datepicker for Appointment
	wp_register_style('appointment-datepicker-css', plugins_url('includes/assets/css/appointment-datepicker.css', __FILE__));
	wp_register_script('appointment-moment-js', plugins_url('includes/assets/js/moment.js', __FILE__), array('jquery'), '1.0', false);
	wp_register_script('appointment-datepicker-js', plugins_url('includes/assets/js/daterangepicker.js', __FILE__), array('jquery'), '1.0', false);
	//wp_register_script('appointment-datepicker-custom-call-js', plugins_url('includes/assets/js/appointment-datepicker-custom-call.js', __FILE__), array('jquery'), '1.0', true);
	
	
	
	/*
	*
	*	MednCures Feature functions
	*	like js validation, Show hide contents on page load etc
	*
	*/
	wp_register_script('cdrmed-feature-functons-js', plugins_url('includes/assets/js/cdrmed-feature-functons.js', __FILE__), array('jquery'), '1.0', true);
	/*
	*
	*	MednCures AJAX Calls
	*
	*/
	wp_register_script('cdrmed-ajax-functions-js', plugins_url('includes/assets/js/cdrmed-ajax-functions.js', __FILE__), array('jquery'), '1.0', true);
	
	//Admin side Medication & Diagnosis File
	wp_register_script('cdrmed-admin-functions-js', plugins_url('includes/assets/js/cdrmed-admin-functions.js', __FILE__), array('jquery'), '1.0', true);
	
	//Calendaly Appointment
	wp_register_style('calendly-cs', 'https://calendly.com/assets/external/widget.css');
	wp_register_script('calendly-js', 'https://calendly.com/assets/external/widget.js');
	
	//Admin Side CSS
	wp_register_style('cdrmed-admin-side-css', plugins_url('includes/assets/css/cdrmed-admin-side.css', __FILE__));
	
	
	
	
	
}

function my_assets() {
	
	//wp_enqueue_style( 'mybscss' );
	
	wp_enqueue_style( 'bootStrapMinCSS-wpd' );
	wp_enqueue_style( 'bootStrapTable-wpd' );
	wp_enqueue_style( 'jquery-style-wpd' );
	wp_enqueue_style( 'slick-css' );
	wp_enqueue_style( 'slick-theme' );
	wp_enqueue_style( 'bootstrapselect' );
	wp_enqueue_style( 'fontawesome' );
	wp_enqueue_style( 'customscroll' );
	wp_enqueue_style( 'cdrmedcss' );
	wp_enqueue_style( 'cdrmed-dispensar' );
	
	wp_enqueue_script( 'my_custom_script4-wpd');
	
	wp_enqueue_script( 'cdremed-table-filter');
	wp_enqueue_script( 'slick-js');
	wp_enqueue_script( 'angular-js' );
	wp_enqueue_script( 'ng-img-crop-js' );
	wp_enqueue_style( 'ng-img-crop-css' );

	wp_enqueue_script( 'bsselectjs');
	wp_enqueue_script( 'mcustomsb');
	//wp_enqueue_script( 'disease-js');
	//wp_enqueue_script( 'cdrmed-ajax-functions');
	wp_enqueue_script( 'date-format-js');
	wp_enqueue_script( 'input-mask-js');
	wp_enqueue_script( 'bs-multiselect-js');
	wp_enqueue_style( 'bs-multiselect-css');
	//wp_enqueue_script( 'cdrmedjs');
	
	
	//wp_enqueue_script( 'weather-js');
	//wp_enqueue_script( 'timezonedetect-js');
	
	wp_enqueue_style( 'calendar-css' );
	wp_enqueue_script( 'calendar-js');
	
	wp_enqueue_style( 'dashboard-css' );
	//wp_enqueue_script( 'dashboard-js');
	
	wp_enqueue_style( 'cdrmed-appointment-css' );
	//wp_enqueue_script( 'cdrmed-appointment-js');
	
	wp_enqueue_script( 'appointment-moment-js');
	wp_enqueue_style( 'appointment-datepicker-css' );
	wp_enqueue_script( 'appointment-datepicker-js');
	//wp_enqueue_script( 'appointment-datepicker-custom-call-js');
	
	
	/*
	*
	*	MednCures Feature functions
	*	like js validation, Show hide contents on page load etc
	*
	*/
	wp_enqueue_script( 'cdrmed-feature-functons-js');
	
	/*
	*
	*	MednCures AJAX Calls
	*
	*/
	wp_enqueue_script( 'cdrmed-ajax-functions-js');
	
	
	//$inventory_url = get_site_url().'/inventory-list/';
	
	$dashboard_widgets_run = null;
	if(is_page(array('physicians-dashboard','dispensary-dashboard', 'patient-dashboard'))){
		$dashboard_widgets_run = true;
	}
	$userrole = get_user_role();
	
	$save_therapy = '';
	
	$character0 = $character1 = $character2 = $character3 = null;
	 
	if($userrole == 'administrator'){
		$character0 = true;
	}
	elseif(	$userrole == 'physician' || $userrole == 'caregiver'){
		$character1 = true;
	}
	elseif(	$userrole == 'dispensary'){
		$character2 = true;
		$pid = isset($_GET['pid'])? $_GET['pid'] : '';
		$save_therapy = get_site_url().'/shoop-for-patient/?pid='.$pid;
	}
	elseif(	$userrole == 'patient'){
		$character3 = true;
	}
	
	wp_localize_script( 'cdrmed-feature-functons-js', 'cdrmedajax', array(
		'ajax_url' => admin_url( 'admin-ajax.php' ),
		'img_url' => CDRMED.'/includes/assets/img' ,
		'site_url' => get_site_url(),
		'save_therapy' => $save_therapy,
		'inventory_url' => get_site_url().'/inventory-list/',
		'shoop_for_patient' => get_site_url().'/shoop-for-patient/',
		'user_role' => $userrole,
		'character0' => $character0,
		'character1' => $character1,
		'character2' => $character2,
		'character3' => $character3,
		'dashboard_widgets_run' => $dashboard_widgets_run,
		'lab_result_pre_fix1' => '_azcl_',
		'lab_result_pre_fix2' => '_tests',
	));
	
	if(is_page(array('physicians-dashboard','dispensary-dashboard', 'patient-dashboard','pre-registration','view-complete-intake','advanced-search'))){
		wp_enqueue_script( 'medication-autocomplete-js');
		wp_enqueue_script( 'diagnosis-autocomplete-js');
	}
	
	/* if(is_page(array('patient-dashboard'))){
		wp_enqueue_style( 'calendly-css');
		wp_enqueue_script( 'calendly-js');
	} */
	
	
	
	
}

add_action( 'wp_enqueue_scripts', 'my_assets' );

add_action( 'get_footer', 'ge_med_care_prefix_add_footer_styles' );
function ge_med_care_prefix_add_footer_styles() {
	//Med & Cure Custom CSS
	wp_enqueue_style('med-cure-style-css', plugins_url('includes/assets/css/med-cure-style.css', __FILE__));
};

add_action('wp_head', 'print_my_styles', 40);

function print_my_styles(){
	// CSS Printed and Loaded in Footer to Overwrite Elements
	wp_print_styles('cdrmedow');
	
	// Conditionally Loaded Scripts For Auto Suggest Etc.
	if(is_page(array( 'physicians-dashboard','dispensary-dashboard', 'patient-dashboard','pre-registration','view-complete-intake','advanced-search'))){
		
		wp_print_scripts('modernizr');
		wp_print_scripts('tokenize');
		wp_print_scripts('jui');	
		wp_print_styles('jui-css');
		wp_print_styles('jtoken-css');
	}
}

function my_enqueue($hook) {
    if ( strpos('http://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'], 'page=export_intakes') !== false) {
        wp_enqueue_style( 'ui-style', '//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css' );
        wp_enqueue_script( 'jquery', '//code.jquery.com/jquery-1.10.2.js' );
        wp_enqueue_script( 'jq-ui', '//code.jquery.com/ui/1.11.4/jquery-ui.js' );
    }
	else if ( strpos('http://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'], 'page=medication_create') !== false 
	|| strpos('http://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'], 'page=medication_update') !== false 
	|| strpos('http://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'], 'page=diagnosis_create') !== false 
	|| strpos('http://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'], 'page=diagnosis_update') !== false
	|| strpos('http://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'], 'page=medication_list') !== false
	|| strpos('http://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'], 'page=diagnosis_list') !== false
	|| strpos('http://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'], 'page=activity_log_list') !== false
	|| strpos('http://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'], 'page=single_user_activity_log') !== false	) {
		// bootstrap files for medicine table sorting
		
		wp_enqueue_style( 'bootStrapMinCSS-wpd' );
		wp_enqueue_style( 'bootStrapTable-wpd' );
		wp_enqueue_script( 'my_custom_script4-wpd');
		
		if(strpos('http://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'], 'page=single_user_activity_log') !== false){
			wp_enqueue_script( 'appointment-moment-js');
			wp_enqueue_style( 'appointment-datepicker-css' );
			wp_enqueue_script( 'appointment-datepicker-js');
		}
		
		//custom js for admin side
		wp_enqueue_style( 'cdrmed-admin-side-css' );
		wp_enqueue_script( 'cdrmed-admin-functions-js');
		
	}
	else if ( (strpos('http://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'], 'post_type=autoshoop') !== false) 
	|| (isset($_GET['post']) && get_post_type($_GET['post']) == 'autoshoop')){
		
		wp_enqueue_script('diagnosis-autocomplete-js', plugins_url('includes/assets/js/diagnosis_autocomplete.js', __FILE__), array('jquery'), '1.0', false);
		
		wp_localize_script( 'diagnosis-autocomplete-js', 'cdrmedajax', array(
			'ajax_url' => admin_url( 'admin-ajax.php' ),
		));
		
		wp_enqueue_script('modernizr', plugins_url('includes/assets/js/modernizr.js', __FILE__));
		wp_enqueue_script('tokenize', plugins_url('includes/assets/js/jquery.tokenize.js', __FILE__));
		wp_enqueue_script('jui', plugins_url('includes/assets/js/jquery-ui.js', __FILE__));
		
		wp_enqueue_style('jui-css', plugins_url('includes/assets/css/jquery.ui.css', __FILE__));
		wp_enqueue_style('jtoken-css', plugins_url('includes/assets/css/jquery.tokenize.css', __FILE__));
	}
	
	wp_enqueue_style('admin-side-css', plugins_url('includes/assets/css/admin-side-css.css', __FILE__));
}
add_action( 'admin_enqueue_scripts', 'my_enqueue' );
//WooCommerce Tabs Scripts Admin Side

function thisScreen() {

    $currentScreen = get_current_screen();

   
    if( $currentScreen->post_type === "product" ) { 

           wp_enqueue_script( 'jquery-bootstrap' );
   			wp_enqueue_script( 'woo-tabs-js' );
    		wp_enqueue_style( 'custom-tabs' );

    }
    
}
add_action( 'current_screen', 'thisScreen' );


function woo_tabs_enqueue_scripts() {

	
    $current_url = $_SERVER["HTTP_HOST"] . $_SERVER["REQUEST_URI"];
	if ( strpos($current_url,'?post_type=product') !== false ) {
        
    		wp_enqueue_script( 'jquery-bootstrap' );
   			wp_enqueue_script( 'woo-tabs-js' );
    		wp_enqueue_style( 'custom-tabs' );

    	}
}

add_action( 'init', 'woo_tabs_enqueue_scripts' );


function my_acf_admin_enqueue_scripts() {
    // register script
    wp_register_script( 'my-acf-input-js', plugins_url('includes/assets/js/acf-js.js', __FILE__), false, '1.0.0');
    wp_enqueue_script( 'my-acf-input-js' );
}

//add_action( 'acf/input/admin_enqueue_scripts', 'my_acf_admin_enqueue_scripts' );

?>